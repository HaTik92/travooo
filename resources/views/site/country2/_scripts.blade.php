<script src="https://rawgit.com/jackmoore/autosize/master/dist/autosize.min.js"></script>

<script type="text/javascript">
    
    autosize(document.getElementById("AddReview"));

    
    $(document).on('click', '.place-comment-btn', function(){
        var report_id = $(this).data('id')
         Comment_Show.init($('.pp-report-'+report_id));
    })
    
    $(document).on('click', '.comment-filter-type', function(){
       var filter_type = $(this).data('type');
        var report_id = $(this).data('report_id');
        commentSort(filter_type, $(this), $('.pp-report-' + report_id));
    })
    
//----START---- comments scripts
    function commentSort(type, obj, reload_obj){
        if($(obj).hasClass('active'))
            return;
        
        $(obj).parent().find('button').removeClass('comments__filter-btn--active');
        $(obj).addClass('comments__filter-btn--active');
        var list, i, switching, shouldSwitch, switchcount = 0;
        switching = true;
        parent_obj = $(reload_obj).find('.comments__items');
        while (switching) {
            switching = false;
            list = parent_obj.find('>div');
            shouldSwitch = false;
            for (i = 0; i < list.length - 1; i++) {
                shouldSwitch = false;
                if(type == "Top")
                {
                    if ($(list[i]).attr('topsort') < $(list[i + 1]).attr('topsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "New")
                {
                    if ($(list[i]).attr('newsort') < $(list[i + 1]).attr('newsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                $(list[i]).before(list[i + 1]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && list.length > 1 && i == 0) {
                    switching = true;
                }
            }
        }
            Comment_Show.reload(reload_obj); 
    
    }
    
var Comment_Show = (()=>{
    var total_rows = 0;
    var display_unit = 3;
    function init(obj){
        var target = $(obj).find('.sortBody');
        if(target.attr('travooo-comment-rows'))
        {
            return;
        }
        else
        {
           if(target.find(">div").hasClass('comment-not-fund-info')){
               return;
           }else{
            target.find(">div").hide();
            get_totalrows(target, true)
        }   
        }
    }

    function reload(obj)
    {
        var target = $(obj).find('.sortBody');
        target.find(">div").hide();
        target.find(">div").removeClass('displayed');
        var check_count = (target.find(">div").length == display_unit+1) ? true : false;

        get_totalrows(target, check_count);
    }

    function get_totalrows(obj, check_type)
    {
        this.total_rows = obj.find(">div").length;
        
        if(this.total_rows>display_unit && check_type && obj.parent().find('a.comments__load-more').length == 0){
            var btn = $('<a href="javascript:;"/>').addClass('comments__load-more').text('Load more...').click(function(){
                Comment_Show.inc(obj)
            });
            obj.after(btn);
       }
       
        obj.attr('travooo-comment-rows', this.total_rows);
        if(!obj.attr('travooo-current-page'))
            obj.attr('travooo-current-page', 1);
        show_rows(obj, obj.attr('travooo-current-page'));
    }

    function show_rows(obj, page)
    {
        this.total_rows = parseInt(obj.attr('travooo-comment-rows'));
        var showing_rows = parseInt(page) * display_unit;
        showing_rows = this.total_rows > showing_rows ? showing_rows : this.total_rows;
        for(var i = 0; i < showing_rows; i++)
        {
            obj.find(">div").eq(i).show().addClass("displayed");
        }

        var disnum = obj.find(".displayed").length;
        obj.parent().find('.comm-count-info').find('strong').html( disnum );
    }

    function increase_show(obj)
    {
        var current_page = parseInt(obj.attr('travooo-current-page'));
        obj.attr('travooo-current-page', (current_page + 1));
        get_totalrows(obj, false);
    }


    return  {
        init: init,
        inc: increase_show,
        reload: reload
    }
})();

  //Report comment like
        $('body').on('click', '.report_comment_like', function (e) {
            obj = $(this);
            commentId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('report.commentlike') }}",
                data: {comment_id: commentId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (res.status == 'yes') {

                    } else if (res == 'no') {

                    }
                    obj.html(result.count + " @lang('home.likes')");
                });
            e.preventDefault();
        });
        
    //Report comment delete
    $('body').on('click', '.report-comment-delete', function (e) {
        
        var comment_id = $(this).attr('id');
        var report_id = $(this).attr('reportid');
        var commentNode = $(this).parent().parent().parent().parent();
        var commentContent = commentNode.parent().parent();
        var allReportCommentCount = $('.'+ report_id +'-all-comments-count').html()

        $.confirm({
        title: 'Delete Comment!',
        content: 'Are you sure you want to delete this comment? <div class="mb-3"></div>',
        columnClass: 'col-md-5 col-md-offset-5',
        closeIcon: true,
        offsetTop: 0,
        offsetBottom: 500,
        buttons: {
            cancel: function () {},
            confirm: {
                text: 'Confirm',
                btnClass: 'btn-red',
                keys: ['enter', 'shift'],
                action: function(){
                    $.ajax({
                        method: "POST",
                        url: "{{ route('report.commentdelete') }}",
                        data: {comment_id: comment_id}
                    })
                        .done(function (res) {
                            if(res){
                                $('.'+ report_id +'-all-comments-count').html(parseInt(allReportCommentCount) - 1)
                                $('.'+ report_id +'-post-comments-count strong').html(parseInt(allReportCommentCount) - 1)
                                commentNode.remove();
                               
                                Comment_Show.reload(commentContent);
                            }
                    });
                }
            }
        }
    });      
            e.preventDefault();
    });


//----END---- comments scripts
    
    
    $('#add_post_permission_button').click(function(e){
        if ( !$(this).find('button').hasClass('hided') )
        {
            $(".permissoin_show").removeClass('hided');
            $("#add_post_permission_button").addClass('hided');
            $(".white_mark").removeClass('hided');
        }
    });
    $(".add_post_permission").click(function(e){
        $(".permissoin_show").addClass('hided');
        $("#add_post_permission_button").removeClass('hided');
        $("#add_post_permission_button").html($(this).find('span').html());
        $(".white_mark").addClass('hided');
    });
    $(".white_mark").click( function() {
        $(".permissoin_show").addClass('hided');
        $("#add_post_permission_button").removeClass('hided');
        $(".white_mark").addClass('hided');
    });

    $('.datepicker').datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: '+5d',
        changeMonth: true,
        changeYear: true,
        altField: "#idTourDateDetailsHidden",
        altFormat: "yy-mm-dd"
    });
    
    $(document).ready(function () {
        $(".morelink").click(function(){
            var moretext = "see more";
            var lesstext = "see less";
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        }); 
        
        $.ajax({
            method: "POST",
            url: "{{ route('country.ajax_happening_today', $country->id) }}",
            data: {name: "test"}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                if(result.success)
                {
                    $("#modal-about").find(".map__users").html('');
                    var marker = new Array();
                    for(var i in result.happenings)
                    {
                        var name = result.happenings[i].name != null ? result.happenings[i].name : "";
                        var map__user    = '<div class="map__user" style="cursor: pointer" onclick="setCenter_usermap(today_map, ' + result.happenings[i].lat + ',' + result.happenings[i].lng + ')">';
                        map__user       += '<div class="map__user-content"><img class="map__user-avatar" src="' + result.happenings[i].profile_picture + '" alt="#" title="" />';
                        map__user       += '<div class="map__user-text"><a class="map__user-name" href="#">' + name + '</a> checked-in to';
                        map__user       += '<a class="map__user-added" href="#">&nbsp;' + result.happenings[i].title + '</a></div>';
                        map__user       += '</div><div class="map__posted">' + result.happenings[i].date + '</div></div>';

                        var title = name + " checked-in to " + result.happenings[i].title;
                        $("#modal-about").find(".map__users").append(map__user);

                        var canvas = document.createElement("canvas");
                        var scale = 1;
                        var ctx = canvas.getContext("2d");
                        ctx.canvas.width = 34 * scale;
                        ctx.canvas.height = 34 * scale;
                        
                        var img= document.createElement("img");
                        img.src = result.happenings[i].profile_picture;
                        ctx.save();
                        ctx.beginPath();
                        ctx.arc(17, 17, 15, 0, 2 * Math.PI);
                        ctx.stroke();
                        ctx.clip();
                        ctx.drawImage(img, 2, 2, 30, 30);
                        ctx.restore();

                        ctx.strokeStyle = "#FFFFFF";
                        ctx.beginPath();
                        ctx.lineWidth = 2;
                        ctx.arc(17, 17, 15, 0, 2 * Math.PI);
                        ctx.stroke();

                        var img = {
                            url: canvas.toDataURL(),
                        }
                        var latLng = new google.maps.LatLng(result.happenings[i].lat, result.happenings[i].lng);
                        var marker = new google.maps.Marker({
                            position: latLng,
                            icon: img,
                            map: today_map,
                            title: title
                        });
                    }
                }

            });

        var photo_position_calc = function(content) {
            var col_height = new Array(0, 0, 0), i = 0;

            $("#box-photos").find(".gallery>.gallery__col").each(function(){
                $(this).find('img').each(function(){
                    col_height[i] += this.height;
                });
                i++;
            });

            var col_index = col_height.indexOf(Math.min.apply(null, col_height));
            $("#box-photos").find(".gallery>.gallery__col").eq(col_index).append(content);
        }
        // about_photo_init();

        $(".about_more_photo").click(function(e){
            var num = parseInt($(this).attr("data-page"));
            $.post(
                '{{route("country.ajax_more_photo", $country->id)}}',
                {pagenum: num},
                function(res)
                {
                    if(res.success)
                    {
                        $(".about_more_photo").attr("data-page", (num+1));
                        for(var i in res.data)
                            photo_position_calc(res.data[i]);
                        // $("#box-photos").find(".gallery").append(res.data);
                    }
                },"json"
            );
        });
        $(".about_more_photo").click();

        $('body').on('click', '.report_like_button', function (e) {
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('report.like') }}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (res.status == 'yes') {


                    } else if (res == 'no') {

                    }
                    //console.log('#post_like_count_' + postId);
                    $('#report_like_count_' + postId).html('<strong>' + result.count + '</strong> Likes');
                });
            e.preventDefault();
        });
        $('body').on('click', '.report_share_button', function (e) {
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('report.share') }}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (res.status == 'yes') {


                    } else if (res == 'no') {

                    }
                    //console.log('#post_like_count_' + postId);
                    $('#report_share_count_' + postId).html('<strong>' + result.count + '</strong> Shares');
                });
            e.preventDefault();
        });

        $('body').on('click', '.followbtn4user', function (e) {
            var obj = $(this);
            var id = obj.parent().attr('data-id');
            var followurl = "{{  route('profile.follow', ':user_id')}}";
            var unfollowurl = "{{  route('profile.unfolllow', ':user_id')}}";
            var type = obj.hasClass('btn-light-grey') ? 'unfollow' : 'follow' ;
            if(type == "follow")
            {
                url = followurl.replace(':user_id', id);
            }
            else if(type == "unfollow")
            {
                url = unfollowurl.replace(':user_id', id);
            }
            $.ajax({
                method: "POST",
                url: url,
                data: {name: "test"}
            })
            .done(function (res) {
                if (res.success == true) {
                    if(type == "follow")
                    {
                        obj.removeClass('btn-light-primary');
                        obj.addClass('btn-light-grey');
                        obj.html('UnFollow');
                    }
                    else if(type == "unfollow")
                    {
                        obj.removeClass('btn-light-grey');
                        obj.addClass('btn-light-primary');
                        obj.html('Follow');
                    }
                } else if (res.success == false) {
                    
                }
            });
        });

        $('body').on('click', '.discussion_like_button', function (e) {
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('discussion.likeunlike') }}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    
                    if (res.status == 'yes') {


                    } else if (res == 'no') {

                    }
                    
                    $('#discussion_like_count_' + postId).html('<strong>' + result.count + '</strong> Likes');
                });
            e.preventDefault();
        });
        $('body').on('click', '.discussion_share_button', function (e) {
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('discussion.shareunshare') }}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    
                    if (res.status == 'yes') {


                    } else if (res == 'no') {

                    }
                    
                    $('#discussion_share_count_' + postId).html('<strong>' + result.count + '</strong> Shares');
                });
            e.preventDefault();
        });
        $('body').on('click', '.post_like_button', function (e) {
            var obj = $(this);
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('post.likeunlike') }}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (res.status == 'yes') {


                    } else if (res == 'no') {

                    }
                    //console.log('#post_like_count_' + postId);
                    $(obj).find('strong').html(result.count);
                });
            e.preventDefault();
        });
        $('body').on('click', '.post_share_button', function (e) {
            var obj = $(this);
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('post.shareunshare') }}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (res.status == 'yes') {


                    } else if (res == 'no') {

                    }
                    //console.log('#post_like_count_' + postId);
                    $(obj).find('strong').html(result.count);
                });
            e.preventDefault();
        });
        $('body').on('click', '.plan_share_button', function (e) {
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('trip.shareunshare') }}",
                data: {trip_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (res.status == 'yes') {


                    } else if (res == 'no') {

                    }
                    //console.log('#post_like_count_' + postId);
                    $('#plan_share_count_' + postId).html('<strong>' + result.count + '</strong> Shares');
                });
            e.preventDefault();
        });

        $('body').on('click', '.post__tip-upvotes', function(e) {

            var btn = $(this);
            replies_id = $(this).attr('reply-id');

            $.ajax({
                method: "POST",
                url: "{{ route('discussion.upvote') }}",
                data: {replies_id: replies_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.status == 1) {

                        btn.find('strong').html( result.count_upvotes );

                    }
                    //console.log('#post_like_count_' + postId);
                });
            e.preventDefault();
        });

        $('body').on('click', '.post__tip-loadmore', function(e) {

            var btn         = $(this);
            var replies_id  = btn.attr('reply-id');
            var page_num    = btn.attr('page_num');

            $.ajax({
                method: "POST",
                url: "{{ route('place.ajax_discuss_loadmore') }}",
                data: { dis_id: replies_id, page_num: page_num }
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.data != "") {

                        btn.parent().prev().append( result.data );

                    }
                    if ( result.has_next )
                    {
                        btn.parent().show();
                        btn.attr('page_num', 1 * page_num + 1);
                    }
                    else
                    {
                        btn.parent().hide();
                    }

                });
            e.preventDefault();
        });

        $('body').on('submit', '.dicussioncomment', function(e) {

            var form            = $(this);
            var discussion_id    = form.attr('data-id');
            var text            = form.find("textarea").val().trim();
            if (text.trim() == ""){
                return false;
            }
            $.ajax({
                method: "POST",
                url: "{{ route('discussion.ajaxreply') }}",
                data: { discussion_id: discussion_id, text: text }
            })
                .done(function (res) {
                    
                    if (res != "error") {

                        form.find("textarea").val('');
                        form.closest('.post').find(".post__tips").html(res);
                        var c = form.closest('.post').find('.post__footer').find('.post__comment-btn').find('strong').html() * 1;
                        form.closest('.post').find('.post__footer').find('.post__comment-btn').find('strong').html( c + 1 );
                        form.closest('.post').find('.post__tip-loadmore').attr('page_num', 1);

                    }
                    else
                    {
                        alert("error");
                    }

                });
            return false;
        })
        
       var scrollTimeout = null;
       
        // $(window).mousemove(function(){
        //     $('.hero__user-list').fadeIn('slow');
        //     if (scrollTimeout) clearTimeout(scrollTimeout);
        //     scrollTimeout = setTimeout(function(){
        //         $('.hero__user-list').fadeOut('slow');
        //     },2000);
        // });
    });

    function discuss_loadmore(obj)
    {
        $(obj).closest('.post').find('.post__tip-loadmore').click();
        $(obj).hide();
    }

    function review_share(id, e) {
        $.ajax({
                method: "POST",
                url: "{{ route('country.share-review') }}",
                data: {id: id},
                dataType: 'json'
            })
                .done(function (res) {
                                        
                    alert(res.msg);
                    
                });
            e.preventDefault();
    }

</script>

<script>
    

    $(document).ready(function (e) {
        $.ajax({
            method: "POST",
            url: "{{ route('country.check_follow', $country->id) }}",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    $('#followContainer1').html('<a class="btn btn--main button_unfollow" style="font-family: inherit;" href="#"><svg class="icon icon--follow"><use xlink:href="{{asset('assets3/img/sprite.svg#follow')}}"></use></svg>@lang("place.unfollow")</a>');
                } else if (res.success == false) {
                    $('#followContainer1').html('<a class="btn btn--main button_follow" style="font-family: inherit;" href="#"><svg class="icon icon--follow"><use xlink:href="{{asset('assets3/img/sprite.svg#follow')}}"></use></svg>@lang("place.follow")</a>');
                }
            });

    });

    $(document).ready(function () {
    
    $.ajax({
            method: "POST",
            url: "{{ route('country.check_checkin', $country->id) }}",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    $('#checkinContainer1').html('<a class="btn btn--secondary button_checkin" style="font-family: inherit;" href="#"><svg class="icon icon--location"><use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use></svg>Check-in</a>');
                } else if (res.success == false) {
                    $('#checkinContainer1').html('<a class="btn btn--secondary button_checkin" style="font-family: inherit;" href="#"><svg class="icon icon--location"><use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use></svg>Check-in</a>');
                }
            });
    
    $('body').on('click', '.button_checkin', function (e) {
            $.ajax({
                method: "POST",
                url: "{{ route('country.checkin', $country->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#checkinContainer1').html('<a class="btn btn--secondary button_checkin" style="font-family: inherit;" href="#"><svg class="icon icon--location"><use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use></svg>Check-in</a>');
                    } else if (res.success == false) {
                        $('#checkinContainer1').html('<a class="btn btn--secondary button_checkin" style="font-family: inherit;" href="#"><svg class="icon icon--location"><use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use></svg>Check-in</a>');
                    }
                });
            e.preventDefault();
        });
        
        
        $('body').on('click', '.button_follow', function (e) {
            $.ajax({
                method: "POST",
                url: "{{ route('country.follow', $country->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#followContainer1').html('<a class="btn btn--main button_unfollow" style="font-family: inherit;" href="#"><svg class="icon icon--follow"><use xlink:href="{{asset('assets3/img/sprite.svg#follow')}}"></use></svg>@lang("place.unfollow")</a>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
            e.preventDefault();
        });

        $('body').on('click', '.button_unfollow', function (e) {
            $.ajax({
                method: "POST",
                url: "{{ route('country.unfollow', $country->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#followContainer1').html('<a class="btn btn--main button_follow" style="font-family: inherit;" href="#"><svg class="icon icon--follow"><use xlink:href="{{asset('assets3/img/sprite.svg#follow')}}"></use></svg>@lang("place.follow")</a>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
            e.preventDefault();
        });
    });

    function reviewsSort(type, obj)
    {
        if($(obj).hasClass('comments__filter-btn--active'))
            return;
        
        $(obj).parent().find('button').removeClass('comments__filter-btn--active');
        $(obj).addClass('comments__filter-btn--active');
        var list, i, switching, shouldSwitch, switchcount = 0;
        switching = true;
        parent_obj = $(obj).closest('.comments').find('.comments__items');
        while (switching) {
            switching = false;
            list = parent_obj.find('>div');
            shouldSwitch = false;
            for (i = 0; i < list.length - 1; i++) {
                shouldSwitch = false;
                if(type == "Top")
                {
                    if ($(list[i]).attr('topsort') < $(list[i + 1]).attr('topsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "New")
                {
                    if ($(list[i]).attr('newsort') < $(list[i + 1]).attr('newsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "Worse")
                {
                    if ($(list[i]).attr('topsort') > $(list[i + 1]).attr('topsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                $(list[i]).before(list[i + 1]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && list.length > 1 && i == 0) {
                    switching = true;
                }
            }
        }
        Reviews_Show.reload($("#modal-review").find(".comments"));
    }
    
    var Reviews_Show = (()=>{
        var total_rows = 0;
        var display_unit = 5;

        function init(obj){
            var target = $(obj).find('.sortBody');
            if(target.attr('travoo-comment-rows'))
            {
                return;
            }
            else
            {
                target.find(">div").hide();
                var btn = $('<button/>').addClass('review__load-more').text('Load more...').click(function(){
                    Reviews_Show.inc(target)
                });
                target.after(btn);
                get_totalrows(target)

            }
        }

        function init_sort(obj, sortkey){

            var list, i, switching, shouldSwitch, switchcount = 0;
            switching = true;
            parent_obj = $(obj).find('.sortBody');
            while (switching) {
                switching = false;
                list = parent_obj.find('>div');
                shouldSwitch = false;
                for (i = 0; i < list.length - 1; i++) {
                    shouldSwitch = false;
                    if(sortkey == "desc")
                    {
                        if ($(list[i]).attr('sortval')*1 < $(list[i + 1]).attr('sortval')*1) {
                            shouldSwitch = true;
                            break;
                        }
                    }
                    else if(sortkey == "asc")
                    {
                        if ($(list[i]).attr('sortval')*1 > $(list[i + 1]).attr('sortval')*1) {
                            shouldSwitch = true;
                            break;
                        }
                    }
                }
                if (shouldSwitch) {
                    $(list[i]).before(list[i + 1]);
                    switching = true;
                    switchcount ++;
                } else {
                    if (switchcount == 0 && list.length > 1 && i == 0) {
                        switching = true;
                    }
                }
            }
            
            init(obj);
        }

        function reload(obj)
        {
            var target = $(obj).find('.sortBody');
            target.find(">div").hide();
            target.find(">div").removeClass('displayed');
            get_totalrows(target);
        }
        
        function get_totalrows(obj)
        {
            this.total_rows = obj.find(">div").length;
            obj.attr('travoo-comment-rows', this.total_rows);
            if(!obj.attr('travoo-current-page'))
                obj.attr('travoo-current-page', 1);
            show_rows(obj, obj.attr('travoo-current-page'));
        }

        function show_rows(obj, page)
        {
            this.total_rows = parseInt(obj.attr('travoo-comment-rows'));
            var showing_rows = parseInt(page) * display_unit;
            showing_rows = this.total_rows > showing_rows ? showing_rows : this.total_rows;
            for(var i = 0; i < showing_rows; i++)
            {
                obj.find(">div").eq(i).show().addClass("displayed");
            }

            if( showing_rows == this.total_rows ) 
            {
                obj.next().hide();
            }
            var disnum = obj.find(".displayed").length;
            obj.parent().find('.comments__number').find('strong').html( disnum );
        }

        function increase_show(obj)
        {
            var current_page = parseInt(obj.attr('travoo-current-page'));
            obj.attr('travoo-current-page', (current_page + 1));
            get_totalrows(obj);
        }


        return  {
            init: init,
            init_sort: init_sort,
            inc: increase_show,
            reload: reload
        }
    })();
    new Reviews_Show.init($("#modal-review").find(".comments"));
    new Reviews_Show.init_sort($("#box-ratings").find(".comments"), 'desc');
    new Reviews_Show.init_sort($("#box-qa"), 'desc');
    
  function tripslike(id, obj)
  {
      $.ajax({
          url: "{{route('trip.likeunlike')}}",
          type: "POST",
          data: {id: id},
          dataType: "json",
          success: function(data, status){
              $(obj).find('strong').html(data.count);
          },
          error: function(){}
      });
  }

  function tripcomment_delete(id, obj, e)
  {
    // commentId = $(obj).attr('id');
      e.preventDefault();
      if(!confirm("Are you sure delete this comment?"))
        return;
      $.ajax({
          method: "POST",
          url: "{{ route('trip.commentdelete') }}",
          data: {comment_id: id}
      })
          .done(function (res) {
              console.log('done');
              $('#tripcommentRow'+id).fadeOut();
              var cnt = parseInt($("#plan_post_comments").find('strong').html());
              $("#plan_post_comments").find('strong').html(cnt-1);
          });
          
  }

  // for spam reporting
  function injectData(id, obj)
  {
      // var button = $(event.relatedTarget);
      var posttype = $(obj).parent().attr("posttype");
      $("#spamReportDlg").find("#dataid").val(id);
      $("#spamReportDlg").find("#posttype").val(posttype);
  }
    // photo comment start
    $(document).on('keydown', '#mediacommenttext', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '#mediacommenttext', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            $(this).closest('form').submit();
        }
    });

    $(document).on('submit', '.media_comment_form', function(e){
        var form = $(this);
        var text = form.find('#mediacommenttext').val().trim();

        if(text == "")
        {
            return false;
        }
        var values = $(this).serialize();

        form.trigger('reset');
        var postId = form.find('input[name="medias_id"]').val();
        var url = '{{ route("media.comment", ":postId") }}';
        url = url.replace(':postId', postId);
       
        var all_comment_count = $('.'+ postId +'-all-comments-count').html();
        
        $.ajax({
            method: "POST",
            url: url,
            data: values
        })
            .done(function (res) {
                form.closest('.gallery-comment-wrap').find('.post-comment-wrapper').prepend(res);
                var comments = form.closest('.gallery-comment-wrap').find('.post-comment-wrapper').find('.post-comment-row').length;
                form.closest('.gallery-comment-wrap').find('.comm-count-info').find('strong').html(comments);
                
                $('.'+ postId +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                $("#" + postId + ".comment-not-fund-info").remove();
                 
                Comment_Show.reload($('.photo-comment-' + postId));
                form.trigger('reset');
            });
        e.preventDefault();
    });

    $('body').on('click', '.postmediaCommentDelete', function (e) {
        
        if( !confirm('Are you sure delete this comment?') )
            return false;

        var commentId = $(this).attr('id');
        var postid = $(this).attr('postid');
        var commentNode = $(this).parent().parent().parent().parent().parent();
        
        var commentMediaId = commentNode.parent().attr('data-id');
        var allMediaCommentCount = $('.'+ commentMediaId +'-all-comments-count').html()
        
        //alert(commentId);
        $.ajax({
            method: "POST",
            url: "{{ route('media.commentdelete') }}",
            data: {comment_id: commentId}
        })
            .done(function (res) {
                var comments = commentNode.closest('.gallery-comment-wrap').find('.post-comment-wrapper').find('.post-comment-row').length;
                commentNode.closest('.gallery-comment-wrap').find('.comm-count-info').find('strong').html(comments - 1);
                $('.'+ commentMediaId +'-all-comments-count').html(parseInt(allMediaCommentCount) - 1)
               
                
                commentNode.remove();
                Comment_Show.reload($('.photo-comment-' + commentMediaId));
            });
            
            e.preventDefault();
    });

    $('body').on('click', '.postmediaCommentLikes', function (e) {
        obj = $(this);
        commentId = $(this).attr('id');
        $.ajax({
            method: "POST",
            url: "{{ route('media.commentlikeunlike') }}",
            data: {comment_id: commentId}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                
                if (res.status == 'yes') {

                } else if (res == 'no') {

                }
                obj.html(result.count + " @lang('home.likes')");
            });
        e.preventDefault();
    });
    
    // Media comment sort

    $(document).on('click', '.media-comment-filter-type', function(){
       var filter_type = $(this).data('type');
        var media_id = $(this).data('id');
        mediaCommentSort(filter_type, $(this), $('.photo-comment-' + media_id));
    })
    
       function mediaCommentSort(type, obj, reload_obj){
        if($(obj).hasClass('active'))
            return;
        
        $(obj).parent().find('li').removeClass('active');
        $(obj).addClass('active');
        var list, i, switching, shouldSwitch, switchcount = 0;
        switching = true;
        parent_obj = $(reload_obj).find('.post-comment-wrapper');
        while (switching) {
            switching = false;
            list = parent_obj.find('>div');
            shouldSwitch = false;
            for (i = 0; i < list.length - 1; i++) {
                shouldSwitch = false;
                if(type == "Top")
                {
                    if ($(list[i]).attr('topsort') < $(list[i + 1]).attr('topsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "New")
                {
                    if ($(list[i]).attr('newsort') < $(list[i + 1]).attr('newsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                $(list[i]).before(list[i + 1]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && list.length > 1 && i == 0) {
                    switching = true;
                }
            }
        }
            Comment_Show.reload(reload_obj); 
    
    }
    
    // Media Comments votes
    $('body').on('click', '.media-comment-vote.up', function (e) {
      var comment_id = $(this).attr('id');
      $.ajax({
          method: "POST",
          url: "{{route('media.comment_updownvote')}}",
          data: {comment_id: comment_id, vote_type:1}
      })
          .done(function (res) {
               var up_result = JSON.parse(res);
              $('.upvote-'+comment_id+' b').html(up_result.count_upvotes);
              $('.downvote-'+comment_id+' b').html(up_result.count_downvotes);
          });
      e.preventDefault()
     });

     $('body').on('click', '.media-comment-vote.down', function (e) {
      var comment_id = $(this).attr('id');
      $.ajax({
          method: "POST",
          url: "{{route('media.comment_updownvote')}}",
          data: {comment_id: comment_id, vote_type:2}
      })
          .done(function (res) {
              var down_result = JSON.parse(res);
              $('.upvote-'+comment_id+' b').html(down_result.count_upvotes);
              $('.downvote-'+comment_id+' b').html(down_result.count_downvotes);
          });
      e.preventDefault()
     }); 
    
    //photo commenting end

    // like & unlike photo
    $('body').on('click', '.photo_like_button', function (e) {
        mediaId = $(this).attr('id');
        $.ajax({
            method: "POST",
            url: "{{ route('media.likeunlike') }}",
            data: {media_id: mediaId}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                if (result.status == 'yes') {
                    alert("You have just liked this photo.");

                } else if (result.status == 'no') {
                    alert("You have just unliked this photo.");
                }
            });
        e.preventDefault();
    });
</script>

<script>

$('#tripscommenttext').keydown(function(e){
    if(e.keyCode == 13 && !e.shiftKey){
        e.preventDefault();
    }
});
$('#tripscommenttext').keyup(function(e){
    if(e.keyCode == 13 && !e.shiftKey){
        $('.tripscomment').find('button[type=submit]').click();
    }
});
$('body').on('submit', '.tripscomment', function (e) {
    var form = $(this);
    var text = form.find('textarea').val().trim();
    // var files = $('.tripscomment').find('.medias').find('div').length;

    if(text == "")
    {
        // alert('Please input text or select files.');
        return false;
    }
    var values = form.serialize();
    form.find('button[type=submit]').attr('disabled', true);
    $.ajax({
        method: "POST",
        url: "{{ route('trip.comment') }}",
        data: values
    })
        .done(function (res) {
            $('#tripscomment').prepend(res);
            $('.tripscomment').find('textarea').val('');
            form.find('button[type=submit]').removeAttr('disabled');
            var cnt = parseInt($("#plan_post_comments").find('strong').html());
            $("#plan_post_comments").find('strong').html(cnt+1);
        });
    e.preventDefault();
});


$(document).on('click', '[data-tab]', function (e) {
    var Item = $(this).attr('data-tab');
    $('[data-content]').css('display', 'none');
    $('[data-content=' + Item + ']').css('display', 'block');
    e.preventDefault();
});
$(document).on('click', '[data-tab_reply]', function (e) {
    var Item = $(this).attr('data-tab_reply');
    $('[data-content_reply]').css('display', 'none');
    $('[data-content_reply=' + Item + ']').fadeIn();
    e.preventDefault();
});


</script>

<script>
    $(window).resize(function(){
        $(".fake__card").width(window.innerWidth - $(".hero__cards").width() - $(".hero__content").width());
        $(".fake__card").css('margin-left',($(".hero__content").width() + 67)+"px");
      });
      $(document).ready(function(){
        $(".fake__card").width(window.innerWidth - $(".hero__cards").width() - $(".hero__content").width()-67);
        $(".fake__card").css('margin-left',($(".hero__content").width() + 67)+"px");
    });

    $('#modal-share').find('.place_share_btn').click(function(){

        $.ajax({
                method: "POST",
                url: "{{ route('place.share', $country->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    alert(res.msg);
                    $("#modal-share").find('.modal__close').click();
                });
    });

    $('.mediaModalTrigger').on('click', function () {
        var slideNum = $(this).attr('data-id');
        let $lg = $(this).lightGallery({
            dynamic: true,
            index: parseInt(slideNum),
            dynamicEl: [
                    @foreach($country->getMedias AS $photo)
                {

                    "src": 'https://s3.amazonaws.com/travooo-images2/th1100/{{$photo->url}}',
                    'thumb': 'https://s3.amazonaws.com/travooo-images2/th180/{{$photo->url}}',
                    'subHtml': `
            <div class='cover-block' id='{{ $photo->id }}'>
              <div class='cover-block-inner comment-block'>
                <ul class='modal-outside-link-list white-bg'>
                  <li class='outside-link'>
                    <a href='#'>
                    @if(true)
                      <div class='round-icon'>
                        <i class='trav-share-icon'></i>
                      </div>
                    @endif
                      <span>Share</span>
                    </a>
                  </li>
                  <li class='outside-link lg-close' posttype="PlaceImage">
                    <a href='#' data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{ $photo->id }},this)">
                      <div class='round-icon'>
                        <i class='trav-flag-icon'></i>
                      </div>
                      <span>@lang("place.report")</span>
                    </a>
                  </li>
                </ul>
                <div class='gallery-comment-wrap'>
                  <div class='gallery-comment-inner mCustomScrollbar'>
                    <div class='top-gallery-content gallery-comment-top'>
                        <div class='top-info-layer pt-3 pb-3'>
                                <div class='top-info-txt'>
                                    <div class='preview-txt'>
                                        <p class='dest-place' style="font-size:17px">{{$country->trans[0]->title}}</p>
                                </div>
                                <div class='preview-txt comment-info'>
                                    <i class="trav-comment-icon" dir="auto"></i>
                                    <span style="color:#1a1a1a"><strong class="{{$photo->id}}-all-comments-count" >{{@count($photo->comments)}}</strong> @lang('comment.comments')</span>     
                                </div>
                            </div>
                        </div>
                        <div class='gal-com-footer-info'>
                          <div class='post-foot-block post-comment-place'>
                            <i class='trav-location'></i>
                            <div class='place-name'>{{$country->trans[0]->address}}</div>
                          </div>
                        </div>
                    </div>                  
                        <div class='post-comment-layer photo-comment-{{$photo->id}}'>
                             <div class='post-comment-top-info'>
                                <ul class="comment-filter">
                                    <li class="media-comment-filter-type" data-type="Top" data-id="{{$photo->id}}">@lang('other.top')</li>
                                    <li class="media-comment-filter-type active" data-type="New" data-id="{{$photo->id}}">@lang('other.new')</li>
                                </ul>
                                <div class='comm-count-info'>
                                    <strong>0</strong> / <span class="{{$photo->id}}-all-comments-count">{{@count($photo->comments)}}</span>
                                </div>
                            </div>
                            <div class='post-comment-wrapper sortBody' data-id="{{$photo->id}}"></div>
                        </div>
                    </div>
                        @if(Auth::user())
                            <form class='media_comment_form' method='post' action='#' autocomplete='off'>
                                <div class='post-add-comment-block'>
                                    <div class='avatar-wrap'>
                                      <img src='{{check_profile_picture(Auth::user()->profile_picture)}}' style='width:45px;height:45px;'>
                                    </div>
                                    <div class='post-add-com-input'>
                                      <input type='text' id='mediacommenttext' name='text' placeholder='@lang("comment.write_a_comment")'>
                                    </div>
                                    <input type='hidden' name='medias_id' value='{{$photo->id}}' />
                                    <input type='hidden' name='users_id' value='{{Auth::user()->id}}' />
                              </div>
                            </form>
                        @endif
                        </div>
                      </div>
                    </div>
`},
                    @endforeach
            {

                    "src": '',
                    {{--'thumb': 'https://s3.amazonaws.com/travooo-images2/th180/{{$photo->url}}',--}}
                    'subHtml': ``
            },
            ],
            addClass: 'main-gallery-block',
            share: true,
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
            

        });
        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });
        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');
            let currentCommentIs = $(subTtl).hasClass('comment-block');
            let currentImgPos = $(currentImg).position().top;
            setTimeout(function () {
                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                if (currentCommentIs) {
                    // console.log('yes');
                    $(currentImgWrap).css('padding-right', commentWidth);
                    $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    $(subTtl).css('width', currentWidth);
                }
                // $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();
            }, 250);
        }
        $lg.on('onBeforeSlide.lg',  function (e) {
            
        }) ;
        $lg.on('onSlideItemLoad.lg', function (e) {

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            });
        });
        $lg.on('onAfterSlide.lg', function () {
            setWidth();
            
            let currentItem = $('.main-gallery-block .lg-current');
            let currentCover = currentItem.find(".cover-block");
           
            if(currentCover.find('.post-comment-wrapper').html() != "")
                return;
            
            currentCover.find('.post-comment-wrapper').html('');
            mediaId = currentCover.attr('id');
            
            $.ajax({
                method: "POST",
                url: "{{ route('place.ajax_media_comment') }}",
                data: {media_id: mediaId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if(result.data != ''){
                         currentCover.find('.post-comment-wrapper').html(result.data);
                    }else{
                        currentCover.find('.post-comment-wrapper').html('<div class="post-comment-top-info comment-not-fund-info" id="'+ mediaId +'">'+
                                                                            '<ul class="comment-filter pl-4">'+
                                                                                '<li>No comments yet ..</li>'+
                                                                                '<li></li>'+
                                                                            '</ul>'+
                                                                        '</div>'); 
                    }
                    currentCover.find('.comm-count-info').find('strong').html(result.totalrows);
                    Comment_Show.init($('.photo-comment-' + mediaId));
                });
        });
        
    });

    $('body').click(function(evt){    
       if(evt.target.className == "lg-inner")
          $('.lg-close.lg-icon').click();

        if(evt.target.className.includes("custom-modal"))
        {
            $(evt.target).find('.modal__close').click();
        }
    });

    $('.tripPlanTriggerrrr').on('click', function () {
        var slideNum = $(this).attr('data-id');
        let $lg = $(this).lightGallery({
            dynamic: true,
            index: parseInt(slideNum),
            dynamicEl: [
                    @foreach($country->versions()->get() AS $tp)
                {

                    "src": 'https://s3.amazonaws.com/travooo-images2/th1100/{{@$photo->url}}',
                    'thumb': 'https://s3.amazonaws.com/travooo-images2/th180/{{@$photo->url}}',
                    'subHtml': `
            <div class='cover-block' id='{{ @$photo->id }}'>
              <div class='cover-block-inner comment-block'>
                <ul class='modal-outside-link-list white-bg'>
                  <li class='outside-link'>
                    <a href='#'>
                      <div class='round-icon'>
                        <i class='trav-angle-left'></i>
                      </div>
                      <span>@lang("other.back")</span>
                    </a>
                  </li>
                  <li class='outside-link'>
                    <a href='#'>
                    @if(true)
                      <div class='round-icon'>
                        <i class='trav-share-icon'></i>
                      </div>
                    @endif
                      <span>Share</span>
                    </a>
                  </li>
                  <li class='outside-link lg-close' posttype="PlaceImage">
                    <a href='#' data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{ $photo->id }},this)">
                      <div class='round-icon'>
                        <i class='trav-flag-icon'></i>
                      </div>
                      <span>@lang("place.report")</span>
                    </a>
                  </li>
                </ul>
                <div class='gallery-comment-wrap'>
                  <div class='gallery-comment-inner mCustomScrollbar'>
                    <div class='top-gallery-content gallery-comment-top'>
                        <div class='top-info-layer pt-3 pb-3'>
                                <div class='top-info-txt'>
                                    <div class='preview-txt'>
                                        <p class='dest-place' style="font-size:17px">{{$country->trans[0]->title}}</p>
                                </div>
                                <div class='preview-txt comment-info'>
                                    <i class="trav-comment-icon" dir="auto"></i>
                                    <span style="color:#1a1a1a"><strong class="{{$photo->id}}-all-comments-count" >{{@count($photo->comments)}}</strong> @lang('comment.comments')</span>     
                                </div>
                            </div>
                        </div>
                        <div class='gal-com-footer-info'>
                          <div class='post-foot-block post-comment-place'>
                            <i class='trav-location'></i>
                            <div class='place-name'>{{$country->trans[0]->address}}</div>
                          </div>
                        </div>
                    </div>                  
                        <div class='post-comment-layer photo-comment-{{$photo->id}}'>
                             <div class='post-comment-top-info'>
                                <ul class="comment-filter">
                                    <li class="media-comment-filter-type" data-type="Top" data-id="{{$photo->id}}">@lang('other.top')</li>
                                    <li class="media-comment-filter-type active" data-type="New" data-id="{{$photo->id}}">@lang('other.new')</li>
                                </ul>
                                <div class='comm-count-info'>
                                    <strong>0</strong> / <span class="{{$photo->id}}-all-comments-count">{{@count($photo->comments)}}</span>
                                </div>
                            </div>
                            <div class='post-comment-wrapper sortBody' data-id="{{$photo->id}}"></div>
                        </div>
                    </div>
                        @if(Auth::user())
                            <form class='media_comment_form' method='post' action='#' autocomplete='off'>
                                <div class='post-add-comment-block'>
                                    <div class='avatar-wrap'>
                                      <img src='{{check_profile_picture(Auth::user()->profile_picture)}}' style='width:45px;height:45px;'>
                                    </div>
                                    <div class='post-add-com-input'>
                                      <input type='text' id='mediacommenttext' name='text' placeholder='@lang("comment.write_a_comment")'>
                                    </div>
                                    <input type='hidden' name='medias_id' value='{{$photo->id}}' />
                                    <input type='hidden' name='users_id' value='{{Auth::user()->id}}' />
                              </div>
                            </form>
                        @endif
                        </div>
                      </div>
                    </div>
`},
                    @endforeach
            
            ],
            addClass: 'main-gallery-block',
            share: true,
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
            

        });
        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });
        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');
            let currentCommentIs = $(subTtl).hasClass('comment-block');
            let currentImgPos = $(currentImg).position().top;
            setTimeout(function () {
                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                if (currentCommentIs) {
                    // console.log('yes');
                    $(currentImgWrap).css('padding-right', commentWidth);
                    $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    $(subTtl).css('width', currentWidth);
                }
                // $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();
            }, 250);
        }
        $lg.on('onBeforeSlide.lg',  function (e) {
            
        }) ;
        $lg.on('onSlideItemLoad.lg', function (e) {

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            });
        });
        $lg.on('onAfterSlide.lg', function () {
            setWidth();
            
            let currentItem = $('.main-gallery-block .lg-current');
            let currentCover = currentItem.find(".cover-block");
           
            if(currentCover.find('.post-comment-wrapper').html() != "")
                return;
            
            currentCover.find('.post-comment-wrapper').html('');
            mediaId = currentCover.attr('id');
            
            $.ajax({
                method: "POST",
                url: "{{ route('place.ajax_media_comment') }}",
                data: {media_id: mediaId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if(result.data != ''){
                         currentCover.find('.post-comment-wrapper').html(result.data);
                    }else{
                        currentCover.find('.post-comment-wrapper').html('<div class="post-comment-top-info comment-not-fund-info" id="'+ mediaId +'">'+
                                                                            '<ul class="comment-filter pl-4">'+
                                                                                '<li>No comments yet ..</li>'+
                                                                                '<li></li>'+
                                                                            '</ul>'+
                                                                        '</div>'); 
                    }
                    currentCover.find('.comm-count-info').find('strong').html(result.totalrows);
                    Comment_Show.init($('.photo-comment-' + mediaId));
                });
        });
        
    });
    $('.nearbyEventTrigger').on('click', function () {
        var slideNum = $(this).attr('data-id');
        let $lg = $(this).lightGallery({
            dynamic: true,
            index: parseInt(slideNum),
            dynamicEl: [
                    @foreach($places_nearby AS $event)
                {

                    'src': '{{"https://maps.googleapis.com/maps/api/staticmap?center=".@$event->lat.",".@$event->lng."&language=en&zoom=16&size=750x506&scale=2&maptype=roadmap&markers=color:green%7Clabel:".substr(@$event->title,0,1)."%7C".@$event->lat.",".@$event->lng. "&key={{env('GOOGLE_MAPS_KEY')}}"}}',
                    "thumb": '{{"https://maps.googleapis.com/maps/api/staticmap?center=".@$event->lat.",".@$event->lng."&language=en&zoom=8&size=180x115&scale=2&maptype=roadmap&markers=color:green%7Clabel:".substr(@$event->title,0,1)."%7C".@$event->lat.",".@$event->lng. "&key={{env('GOOGLE_MAPS_KEY')}}"}}',
                },
                    @endforeach
            ],
            addClass: 'main-gallery-block',
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

    });
    $('.reportsTrigger').on('click', function () {
        var slideNum = $(this).attr('data-id');
        let $lg = $(this).lightGallery({
            dynamic: true,
            index: parseInt(slideNum),
            dynamicEl: [
                    @foreach(@$reports AS $report)
                    @php
                    $event = $report;
                    @endphp
                {

                    'src': '{{"https://maps.googleapis.com/maps/api/staticmap?center=".$event->lat.",".$event->lng."&language=en&zoom=16&size=750x506&scale=2&maptype=roadmap&markers=color:green%7C%7C".$event->lat.",".$event->lng. "&key={{env('GOOGLE_MAPS_KEY')}}"}}',
                    "thumb": '{{"https://maps.googleapis.com/maps/api/staticmap?center=".$event->lat.",".$event->lng."&language=en&zoom=8&size=180x115&scale=2&maptype=roadmap&markers=color:green%7C%7C".$event->lat.",".$event->lng. "&key={{env('GOOGLE_MAPS_KEY')}}"}}',
                },
                    @endforeach
            ],
            addClass: 'main-gallery-block',
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

    });
    
    $('.travel_mate_cards').slick({
        centerPadding: "20px",
        prevArrow: '',
        nextArrow: ''
    });
    $('.travel_mate_cards').find('img').css('display', 'block')
    .click(function(e) {
        var slideno = $(this).index();
        $('.travel_mate_cards').slick('slickGoTo', slideno);
    });
    $('.page-content__add-post-btn').click(function() {
        $('html, body').animate({
            scrollTop: ($('#add-post').offset().top-70)
        },500);
    });
    
    
    
    
    </script>
