<style>
  .modal__close {
    position: fixed !important;
  }

</style>

<!-- TRIP PLANS modal -->
<div class="about-modal custom-modal" id="modal-trips">
    <div class="about-modal__inner" style="height:95%;width:90%;max-width:90%;">
        <button class="modal__close" type="button"><svg class="icon icon--close">
            <use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use>
            </svg>
        </button>
        <div class="about-modal__content" style="padding: 0!important;">

            <div id="example3" class="slider-pro" style="background-color:white;">
                <div class="sp-slides">
                    @foreach($country->versions AS $pv)
                    <div class="sp-slide main-gallery-block-lg-outer-lg-item-clone">
                        <?php
                        $plan = \App\Models\TripPlans\TripPlans::find($pv->trips_id);
                        ?>
                        @include('site.country2.partials.carousel_plan')
                        
                        
                    </div>
                    @endforeach
                    
                    
                </div>

                <div class="sp-thumbnails">
                    @foreach($country->versions AS $pv)
                    <?php
                        $plan = \App\Models\TripPlans\TripPlans::find($pv->trips_id);
                        $mapindex = rand(1, 500);
  
                    ?>
                    <div class="sp-thumbnail" style="border: 1px solid lightgray;border-radius: 10px;overflow: hidden;box-shadow: 1px 1px 1px 1px #e1e1e18f;margin: 0 3px 0 3px;">
                        <img src="{{get_plan_map($plan, 200, 160)}}" style='width:200px;height:160px;' />
                        <div style="padding: 8px;font-family: inherit;font-size: 1.1em;">{{$plan->title}}</div>
                    </div>
                    
                    @endforeach
                    
                </div>
            </div>


        </div>
    </div>

    <script type="text/javascript">
        $( document ).ready(function( $ ) {
                $( '#example3').sliderPro({
                width: 1300,
                height: 450,
                fade: true,
                arrows: false,
                buttons: false,
                fullScreen: true,
                shuffle: false,
                smallSize: 500,
                mediumSize: 1000,
                largeSize: 3000,
                thumbnailArrows: true,
                autoplay: false,
                thumbnailWidth: 200,
                thumbnailHeight: 220
            });
            $( '#example3' ).sliderPro( 'gotoSlide', 1 );

        });
    </script>
</div>



<!-- EXPERTS modal -->
<div class="about-modal custom-modal" id="ExpertsModal">
    <div class="about-modal__inner"><button class="modal__close" type="button"><svg class="icon icon--close">
            <use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use>
            </svg></button>
        <div class="about-modal__links">
            <a class="about-modal__link about-modal__link--active" id="tab-1">Top Experts {{count($experts_top)}}</a>
            <a class="about-modal__link" id="tab-2">Local Experts {{count($experts_local)}}</a>
            <a class="about-modal__link" id="tab-3">My Friends {{count($experts_friends)}}</a>
            <a class="about-modal__link" id="tab-4">Live Check-ins {{count($live_checkins)}}</a>
        </div>
        <div class="about-modal__content" style="padding: 0!important;">

            <div class="about-modal__main">
                <div id="box-1" style='display:none;padding-bottom: 10px;'>
                    <div class="about-modal__item">
                        <div class="about-modal__questions" style='max-height: 500px;overflow: auto;margin-bottom: 0px !important;'>
                            
                            @forelse($experts_top AS $pce)
                            @if(is_object($pce->user))
                            <div class="question" style="padding:10px;">
                                <img class="question__avatar" src="{{check_profile_picture($pce->user->profile_picture)}}" alt="{{$pce->user->name}}" title="{{$pce->user->name}}" style="width:46px;height:46px;"/>
                                <div class="question__content" style='width:50%'>
                                    <div class="question__title">{{$pce->user->name}} @if(is_friend($pce->user->id))<span style='background-color: #e6e6e6;padding: 2px 5px;font-size: 13px;font-weight: 100;-moz-border-radius: 3px;border-radius: 4px;'>FRIEND</span>@endif</div>
                                    <div class="question__total" style="color:darkgrey;font-size:13px;">{{diffForHumans($pce->created_at)}}</div>
                                </div>
                                <div style='width:100%'>
                                    <div id="followContainer" style='float: right;padding-left: 10px;'><a class="btn btn--main button_unfollow" href="#" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Follow</a></div>&nbsp;
                                    @if(Auth::user()->id != $pce->user->id)
                                      <div id="messageContainer" style='float: right;'><a class="btn btn--main" href="{{url_with_locale('chat/new/'.$pce->user->id)}}" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Message</a></div>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @empty
                            <div class="question" style='padding:10px;'>
                            No Users found
                            </div>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div id="box-2" style='display:none;padding-bottom: 10px;'>
                    <div class="about-modal__item">
                        <div class="about-modal__questions" style='max-height: 500px;overflow: auto;margin-bottom: 0px !important;'>
                            
                            @forelse($experts_local AS $pce)
                            @if(is_object($pce->user))
                            <div class="question" style="padding:10px;">
                                <img class="question__avatar" src="{{check_profile_picture($pce->user->profile_picture)}}" alt="{{$pce->user->name}}" title="{{$pce->user->name}}" style="width:46px;height:46px;"/>
                                <div class="question__content" style='width:50%'>
                                    <div class="question__title">{{$pce->user->name}}</div>
                                    <div class="question__total" style="color:darkgrey;font-size:13px;">{{diffForHumans($pce->created_at)}}</div>
                                </div>
                                <div style='width:100%'>
                                    <div id="followContainer" style='float: right;padding-left: 10px;'><a class="btn btn--main button_unfollow" href="#" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Follow</a></div>&nbsp;
                                    <div id="messageContainer" style='float: right;'><a class="btn btn--main" href="{{url_with_locale('chat/new/'.$pce->user->id)}}" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Message</a></div>
                                </div>
                            </div>
                            @endif
                            @empty
                            <div class="question" style='padding:10px;'>
                            No Users found
                            </div>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div id="box-3" style='display:none;padding-bottom: 10px;'>
                    <div class="about-modal__item">
                        <div class="about-modal__questions" style='max-height: 500px;overflow: auto;margin-bottom: 0px !important;'>
                            
                            @forelse($experts_friends AS $pce)
                            @if(is_object($pce->user))
                            <div class="question" style="padding:10px;">
                                <img class="question__avatar" src="{{check_profile_picture($pce->user->profile_picture)}}" alt="{{$pce->user->name}}" title="{{$pce->user->name}}" style="width:46px;height:46px;"/>
                                <div class="question__content" style='width:50%'>
                                    <div class="question__title">{{$pce->user->name}}</div>
                                    <div class="question__total" style="color:darkgrey;font-size:13px;">{{diffForHumans($pce->created_at)}}</div>
                                </div>
                                <div style='width:100%'>
                                    <div id="followContainer" style='float: right;padding-left: 10px;'><a class="btn btn--main button_unfollow" href="#" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Follow</a></div>&nbsp;
                                    <div id="messageContainer" style='float: right;'><a class="btn btn--main" href="{{url_with_locale('chat/new/'.$pce->user->id)}}" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Message</a></div>
                                </div>
                            </div>
                            @endif
                            @empty
                            <div class="question" style='padding:10px;'>
                            No Users found
                            </div>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div id="box-4" style='display:none;padding-bottom: 10px;'>
                    <div class="about-modal__item">
                        <div class="about-modal__questions" style='max-height: 500px;overflow: auto;margin-bottom: 0px !important;'>
                            
                            @forelse($live_checkins AS $key=>$pci)
                            @if(is_object($pci->user))
                            <div class="question" style="padding:10px;">
                                <img class="question__avatar" src="{{check_profile_picture($pci->user->profile_picture)}}" alt="{{$pci->user->name}}" title="{{$pci->user->name}}" style="width:46px;height:46px;"/>
                                <div class="question__content" style='width:50%'>
                                    <div class="question__title">{{$pci->user->name}}</div>
                                    <div class="question__total" style="color:darkgrey;font-size:13px;">{{diffForHumans($pci->created_at)}}</div>
                                </div>
                                <div style='width:100%'>
                                    <div id="followContainer" style='float: right;padding-left: 10px;'><a class="btn btn--main button_unfollow" href="#" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Follow</a></div>&nbsp;
                                    <div id="messageContainer" style='float: right;'><a class="btn btn--main" href="{{url_with_locale('chat/new/'.$pci->user->id)}}" style="width:85px;height:38px;font-family: inherit;font-size:14px;">Message</a></div>
                                </div>
                            </div>
                            @endif
                            @empty
                            <div class="question" style='padding:10px;'>
                            No Users found
                            </div>
                            @endforelse
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<!-- ABOUT modal -->
<div class="about-modal custom-modal" id="modal-about">
    <div class="about-modal__inner"><button class="modal__close" type="button"><svg class="icon icon--close">
            <use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use>
            </svg></button>
        <div class="about-modal__links">
            <a class="about-modal__link about-modal__link--active" id="tab-about">About</a>
            <a class="about-modal__link" id="tab-ratings">Ratings</a>
            <a class="about-modal__link" id="tab-photos">Photos</a>
            <a class="about-modal__link" id="tab-qa">Q&A</a>
            <a class="about-modal__link" id="tab-people">People</a>
            <a class="about-modal__link" id="tab-today">Today</a>

        </div>
        <div class="about-modal__content" style="padding-top: 0!important;">

            <div class="about-modal__main">
                <div id="box-about">   
                    <div class="map map--about">
                        <div class="map__inner" style="background-image: url(https://maps.googleapis.com/maps/api/staticmap?maptype=satellite&center={{$country->lat}},{{$country->lng}}&markers=color:red%7Clabel:C%7C{{$country->lat}},{{$country->lng}}&zoom=14&size=595x360&key={{env('GOOGLE_MAPS_KEY')}})">
                            <div class="map__info">
                                <div class="map__info-item"><svg class="icon icon--user">
                                    <use xlink:href="{{asset('assets3/img/sprite.svg#user')}}"></use>
                                    </svg>
                                    <div class="map__info-wrap">
                                        <div class="map__info-label">#{{$country->id}}</div>
                                        <div class="map__info-value">Popularity</div>
                                    </div>
                                </div>

                            </div>
                            <div class="map__flag"><svg class="icon icon--usa">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#usa')}}"></use>
                                </svg></div>
                        </div>
                    </div>
                    <div class="about-modal__meta">
                        <span class="about-modal__meta-label">
                            <svg class="icon icon--location"><use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use></svg>
                            Address:
                        </span>
                        <span class="about-modal__meta-value">{{@$country->trans[0]->address}}</span>
                    </div>
                    <div class="about-modal__meta">
                        <div class="about-modal__meta-row">
                            @if(@$country->trans[0]->working_days!='')
                            <div class="about-modal__meta-item"><svg class="icon icon--time-2">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#time-2')}}"></use>
                                </svg>
                                <div class="about-modal__meta-content">
                                    <div class="about-modal__meta-label">Working times</div>
                                    <div class="about-modal__meta-value">
                                        {!!@$country->trans[0]->working_days!!}
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(@$country->trans[0]->description!='')
                            <div class="about-modal__meta-item"><svg class="icon icon--chain">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#chain')}}"></use>
                                </svg>
                                <div class="about-modal__meta-content">
                                    <div class="about-modal__meta-label">Website</div><a class="about-modal__meta-value" href="{{@$country->trans[0]->description}}" target="_blank">{{@$country->trans[0]->description}}</a>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div id="box-ratings" style="display:none;">
                    <div class="about-modal__meta about-modal__meta--rating" style='padding-top:15px;padding-bottom:15px;'>
                        <div class="about-modal__meta-item">
                            <div class="about-modal__meta-content">
                                <div class="about-modal__meta-label">User Rating</div>
                                <div class="rating">
                                    <div class="star-rating" style="margin-right: 0px;">
                                        <span style="width: 0%"></span>

                                    </div>
                                    <div class="rating__total"><strong></strong> from <a href="#"></a></div>
                                </div>
                            </div>
                            <a class="btn js-show-modal" href="#modal-review">Write a Review</a>
                        </div>
                    </div>
                    <div class="comments">
                        <div class="comments__items sortBody">
                            

                        </div>
                    </div>
                </div>
                <div id="box-photos" style="display:none;padding-top:15px;">
                    <div class="about-modal__item">
                        <div class="about-modal__item-header">
                            <div class="about-modal__item-title">Photos of {{@$country->trans[0]->title}} <span class="about-modal__item-total">{{count($country->getMedias)}}</span></div><a href="#" class="about_more_photo" data-page="0">More photos</a>
                        </div>
                        <div class="gallery">
                          <div class="gallery__col"></div>
                          <div class="gallery__col"></div>
                          <div class="gallery__col"></div>
                        </div>
                    </div>
                </div>
                <div id="box-qa" style="display:none;padding-top:15px;">
                    <div class="about-modal__item" style="padding-bottom: 10px">
                        <div class="about-modal__item-header" style="margin-bottom: 15px;">
                            <div class="about-modal__item-title">Q&A <span class="about-modal__item-total">{{count($country_discussions)}}</span></div>
                            <div class="user-list"></div>
                            @if(count($country_discussions)>0)
                            <div class="btn">
                                <a href="{{url('discussions?do=place&id='.$country->id)}}">See all {{count($country_discussions)}} Q&A</a></div>
                            @endif
                        </div>
                        <div class="about-modal__questions sortBody" style="display: contents;">
                            @forelse($country_discussions AS $dis)
                            <div class="question" sortval="{{ceil(count($dis->replies) + count($dis->likes) + count($dis->shares))}}">
                                <img class="question__avatar" src="{{check_profile_picture($dis->author->profile_picture)}}" alt="#" title="" />
                                <div class="question__content" style="overflow: hidden;word-break: break-word;">
                                    <div class="question__title"><a href="{{url_with_locale('profile/'.$dis->author->id)}}">{{$dis->author->name}}</a> Asked: {{$dis->question}}<span class="question__posted">{{diffForHumans($dis->created_at)}}</span></div>
                                    <div class="question__total"><strong>{{count($dis->replies)}} Answers</strong>
                                        @if(count($dis->replies))
                                        from 
                                        @if(count($dis->replies)>3)
                                        @for($i=0;$i<3;$i++)
                                        <a href="{{url_with_locale('profile/'.$dis->replies[$i]->author->id)}}">{{$dis->replies[$i]->author->name}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif 
                                        @endfor
                                        and more...
                                        @else
                                        @foreach($dis->replies AS $reply)
                                        <a href="{{url_with_locale('profile/'.$reply->author->id)}}">{{$reply->author->name}}</a>
                                        @if(!$loop->last)
                                            ,
                                        @endif 
                                        @endforeach
                                        @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @empty
                            <div class="question">
                            No Q&A related to {{@$country->trans[0]->title}}
                            </div>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div id="box-people" style="display:none;padding-top:15px;">
                    <div class="about-modal__item">
                        <div class="about-modal__item-header">
                            <div class="about-modal__item-title">People following this place <span class="about-modal__item-total">{{count($country->followers)}}</span></div>
                        </div>
                        <?php
                            $followers = [];
                            $following = \App\Models\UsersFollowers\UsersFollowers::where('followers_id', Auth::user()->id)->get();
                            foreach($following as $f):
                                $followers[] = $f->users_id;
                            endforeach;
                        ?>
                        <div class="about-modal__follow-cards">
                            @forelse($country->followers AS $fol)
                            {{var_dump($fol)}}
                            <div class="follow-card" data-id="{{$fol->user->id}}">
                                <img class="follow-card__avatar" src="{{check_profile_picture($fol->user->profile_picture)}}" alt="#" title="" />
                                <div class="follow-card__name">{{$fol->user->name}}</div>
                                <div class="follow-card__info">{{$fol->user->display_name}}</div>

                                @if(in_array($fol->user->id, $followers))
                                <div class="btn btn--sm btn-light-grey followbtn4user">UnFollow</div>
                                @elseif($fol->user->id == Auth::user()->id)
                                <div class="btn btn--sm btn-light-grey">Follow</div>
                                @else
                                <div class="btn btn--sm btn-light-primary followbtn4user">Follow</div>
                                @endif

                                <div class="follow-card__followers">{{count($fol->user->get_followers)}} Followers</div>
                            </div>
                            @empty
                            No users are following this place.
                            @endforelse

                        </div>
                    </div>
                </div>
                <div id="box-today" style="display:none;padding-top:15px;">
                    <div class="about-modal__item">
                        <div class="about-modal__item-header" style="margin-bottom: 10px;">
                            <div class="about-modal__item-title">Happening Today in "{{@$country->trans[0]->title}}"</div>
                        </div>  `
                        <div class="map">
                            <div class="map__inner" id="today_map"></div>
                            <div class="map__users-wrap">
                                <div class="map__users" data-simplebar="data-simplebar">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
</div>
<!-- REVIEW MODAL -->
<div class="custom-modal add-review" id="modal-review">
    <style>
      .add-review__img
      {
        object-fit: cover;
        object-position: center;
      }
    </style>
    <div class="modal__inner">
        <div class="modal__header">
            <h2 class="modal__title">Reviews for {{@$country->trans[0]->title}}</h2>
            <button class="modal__close" id="review-modal-close" type="button">
                <svg class="icon icon--close">
                  <use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use>
                </svg>
            </button>
        </div>
      <div class="add-review__main">
        <div class="add-review__header"><img class="add-review__img" src="@if(isset($country->getMedias[0]->url)) https://s3.amazonaws.com/travooo-images2/th1100/{{$country->getMedias[0]->url}} @else {{asset('assets2/image/placeholders/pattern.png')}} @endif" alt="" role="presentation" width="100%" style="height:294px" />
          <h2 class="add-review__title" style="padding-top: 25px;">{{@$country->trans[0]->title}}</h2>
          <div class="add-review__desc">{{do_placetype($country->place_type)}} in {{@$country->city->trans[0]->title}}</div><img class="add-review__rating" src="img/rating.svg" alt="" role="presentation" />
        </div>
        <form class="add-review__form" action="/place/save-review" method="post" id="writeReviewForm" autocomplete="off">
            {{ csrf_field() }}
            <input type="hidden" name="place_id" value="{{ $country->id }}">
              <div class="add-review__form-header"><img class="add-review__avatar" src="{{ check_profile_picture($me->profile_picture) }}" alt="" role="presentation" style="object-fit: cover; object-position: center;" />
                  <textarea class="add-review__input" id="AddReview" name="text" placeholder="Write a review..." type="text"></textarea>
              </div>
              <div class="add-review__form-footer">
                  <!-- Rating -->

                  <div class="rating-wrap">

                      <div class="rating-star-click">
                          <input type="range" min="0" max="5" step="0.1" value="" readonly>

                          <div class="rating-stars">
                              <div class="placehold">
                                  <input type="radio" name="score" id="5-stars" value="5"><label for="5-stars"></label>
                                  <input type="radio" name="score" id="4-stars" value="4"><label for="4-stars"></label>
                                  <input type="radio" name="score" id="3-stars" value="3"><label for="3-stars"></label>
                                  <input type="radio" name="score" id="2-stars" value="2"><label for="2-stars"></label>
                                  <input type="radio" name="score" id="1-star" value="1" checked><label for="1-star"></label>
                              </div>

                              <div class="slider">
                                  <svg class="svg" viewBox="0 0 500 100"><rect height="100" fill="url(#rating)" x="0" y="0"/></svg>
                              </div>
                          </div>
                      </div>

                  </div>

                  <!-- Star svg -->
                  <svg width="0" height="0" viewBox="0 0 100 100">
                      <clipPath id="star">
                          <path d="M 44.675463,5.8046818 32.56075,30.368057 5.4557026,34.319708 c -4.86072673,0.705003 -6.8087272,6.697428 -3.2837723,10.1296 L 21.781811,63.558262 17.14371,90.551989 c -0.834855,4.879315 4.304156,8.534111 8.608312,6.252184 L 49.999998,84.058654 74.247975,96.804173 c 4.304156,2.263397 9.44317,-1.372869 8.608312,-6.252184 L 78.21819,63.558262 97.828066,44.449308 c 3.524964,-3.432172 1.576954,-9.424597 -3.283772,-10.1296 L 67.439248,30.368057 55.324534,5.8046818 c -2.170626,-4.378365 -8.459887,-4.434032 -10.649071,0 z"/>
                      </clipPath>

                      <symbol viewBox="0 0 100 100" id="star-base">
                          <path d="M 44.675463,5.8046818 32.56075,30.368057 5.4557026,34.319708 c -4.86072673,0.705003 -6.8087272,6.697428 -3.2837723,10.1296 L 21.781811,63.558262 17.14371,90.551989 c -0.834855,4.879315 4.304156,8.534111 8.608312,6.252184 L 49.999998,84.058654 74.247975,96.804173 c 4.304156,2.263397 9.44317,-1.372869 8.608312,-6.252184 L 78.21819,63.558262 97.828066,44.449308 c 3.524964,-3.432172 1.576954,-9.424597 -3.283772,-10.1296 L 67.439248,30.368057 55.324534,5.8046818 c -2.170626,-4.378365 -8.459887,-4.434032 -10.649071,0 z"/>
                      </symbol>

                      <pattern id="rating" patternUnits="userSpaceOnUse" width="100" height="100" viewBox="0 0 100 100">
                          <use x="0" y="0" xlink:href="#star-base" width="100" height="100"/>
                      </pattern>

                      <pattern id="empty-star" patternUnits="userSpaceOnUse" width="100" height="100">
                          <use x="0" y="0" xlink:href="#star-base" fill="#ccc" width="100" height="100"/>
                      </pattern>

                      <pattern id="full-star" patternUnits="userSpaceOnUse" width="100" height="100">
                          <use x="0" y="0" xlink:href="#star-base" fill="#00b8d4" width="100" height="100"/>
                      </pattern>
                  </svg>


                  {{--            <div class="rating"><svg class="icon icon--star icon icon--star-active">--}}
    {{--                <use xlink:href="img/sprite.svg#star"></use>--}}
    {{--              </svg><svg class="icon icon--star icon icon--star-active">--}}
    {{--                <use xlink:href="img/sprite.svg#star"></use>--}}
    {{--              </svg><svg class="icon icon--star icon icon--star-active">--}}
    {{--                <use xlink:href="img/sprite.svg#star"></use>--}}
    {{--              </svg><svg class="icon icon--star icon icon--star-active">--}}
    {{--                <use xlink:href="img/sprite.svg#star"></use>--}}
    {{--              </svg><svg class="icon icon--star">--}}
    {{--                <use xlink:href="img/sprite.svg#star"></use>--}}
    {{--              </svg></div>--}}
                <div class="add-review__buttons">
                    <button class="add-review__btn-link" onclick="javascript:document.getElementById('review-modal-close').click();" type="button">Cancel</button>
                    <button class="btn btn--sm btn--main" type="submit">Post</button>
                </div>
              </div>
        </form>
      </div>
      <div class="add-review__footer">
        <div class="add-review__comment-btn">
          <div class="add-review__footer-icon"><svg class="icon icon--star">
              <use xlink:href="img/sprite.svg#star"></use>
            </svg></div>
          <div class="user-list">
            <div class="user-list__item">
              <div class="user-list__user"><img class="user-list__avatar" src="img/avatar-2.png" alt="" role="presentation" /></div>
              <div class="user-list__user"><img class="user-list__avatar" src="img/avatar-3.png" alt="" role="presentation" /></div>
              <div class="user-list__user"><img class="user-list__avatar" src="img/avatar-4.png" alt="" role="presentation" /></div>
            </div>
          </div><span><strong>{{ @count($country->reviews) }}</strong> Reviews</span>
        </div>
      </div>
      <div class="comments">
        <div class="comments__header">
          <div class="comments__filter">
            <button class="comments__filter-btn" type="button" onclick="reviewsSort('Top', this)">Top</button>
            <button class="comments__filter-btn comments__filter-btn--active" type="button"  onclick="reviewsSort('New', this)">New</button>
            <button class="comments__filter-btn" type="button"  onclick="reviewsSort('Worse', this)">Worse</button>
          </div>
          <div class="comments__number"><strong>0</strong> / <span>{{ @count($country->reviews) }}</span></div>
        </div>
        <div class="comments__items sortBody">
                            
          
          
        </div>
      </div>
    </div>
  </div>

<!-- FLY TO MODAL -->
<div class="modal bootstrap" id="FlyToModal" tabindex="-1" role="dialog" aria-labelledby="FlyToModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Please select your dates</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
          <label for="idTourDateDetails">From:</label>
          <div class="form-group">
              <div class="input-group">
                  <input type="text" name="date_from" id="date_from" class="form-control datepicker"> 
                  <span class="input-group-addon"><i id="calIconTourDateDetails" class="glyphicon glyphicon-th"></i></span>
              </div>
          </div>
          
          <label for="idTourDateDetails">To:</label>
          <div class="form-group">
              <div class="input-group">
                  <input type="text" name="date_to" id="date_to" class="form-control datepicker"> 
                  <span class="input-group-addon"><i id="calIconTourDateDetails" class="glyphicon glyphicon-th"></i></span>
              </div>
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save">Save changes</button>
      </div>
    </div>
  </div>
</div>


<!-- SHARE MODAL -->
<div class="custom-modal check-in-modal" id="modal-share">
    <div class="modal__inner">
      <div class="modal__header"><svg class="icon icon--location">
          <use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use>
        </svg>
        <h2 class="modal__title">Share on Travooo</h2><button class="modal__close" type="button"><svg class="icon icon--close">
            <use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use>
          </svg></button>
      </div>
      <div class="check-in-modal__content"><img class="check-in-modal__img" src="@if(isset($country->getMedias[0]->url)) https://s3.amazonaws.com/travooo-images2/th1100/{{$country->getMedias[0]->url}} @else {{asset('assets2/image/placeholders/pattern.png')}} @endif" alt="#" title="" />
        <div class="check-in-modal__footer">
          <div class="check-in-modal__city">{{$country->transsingle->title}}</div>
          <div class="check-in-modal__dropdown"></div>
        </div>
      </div>
      <div class="modal__footer"><button class="btn btn--sm btn--main place_share_btn">Share</button></div>
    </div>
  </div>


<!-- ADD TO PLAN MODAL -->
<div class="modal bootstrap" id="AddToPlanModal" tabindex="-1" role="dialog" aria-labelledby="AddToPlanModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Plese select your plan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="radio" name="plans_id" id="plans_id" class="form-control"> Add to a new Plan
          @foreach($my_plans AS $mp)
            <input type="radio" name="plans_id" id="plans_id" class="form-control"> {{$mp->title}}
          @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save">Add to selected Plan</button>
      </div>
    </div>
  </div>
</div>


<!-- MEDIA MODAL -->
<div class="img-modal custom-modal modal--light" id="modal-media"><button class="modal__close" type="button"><svg class="icon icon--close">
        <use xlink:href="img/sprite.svg#close"></use>
      </svg></button>
    <div class="img-modal__inner">
      <div class="img-modal__wrap">
        <div class="img-modal__carousel">
            @foreach($country->getMedias AS $pm)
            <img class="img-modal__img" src="https://s3.amazonaws.com/travooo-images2/{{@$pm->url}}" alt="#" title="" />
            @endforeach
        </div>
        <div class="img-modal__header">
            <img class="img-modal__avatar" src="img/user-avatar.png" alt="#" title="" />
          <div class="img-modal__user-info"><a class="img-modal__username" href="#">Julie</a>
            <div class="img-modal__posted">uploaded a photo 2 hours ago</div>
          </div>
          <div class="img-modal__likes">
            <div class="emoji">❤</div>165
          </div>
        </div>
        <div class="img-modal__side"><button class="img-modal__side-btn" type="button">
            <div class="img-modal__side-btn-icon"><svg class="icon icon--comment">
                <use xlink:href="img/sprite.svg#comment"></use>
              </svg>221</div>
            <div class="img-modal__side-btn-text">Comments</div>
          </button><button class="img-modal__side-btn" type="button">
            <div class="img-modal__side-btn-icon"><svg class="icon icon--flag">
                <use xlink:href="img/sprite.svg#flag"></use>
              </svg></div>
            <div class="img-modal__side-btn-text">Report</div>
          </button></div>
      </div>
      <div class="img-modal__thumbs">
          @foreach($country->getMedias AS $pm)
            <img class="img-modal__thumb" src="https://s3.amazonaws.com/travooo-images2/th230/{{@$pm->url}}" alt="#" title="" />
          @endforeach
      </div>
    </div>
  </div>


<!-- TOP EXPERTS MODAL -->
<div class="modal bootstrap" id="TopExpertsModal" tabindex="-1" role="dialog" aria-labelledby="TopExpertsModalModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Top Experts in {{$country->transsingle->title}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          @foreach($country->experts AS $key=>$pce)
                        <a class="user-list__user" href="{{url_with_locale('profile/'.$pce->user->id)}}"><img class="user-list__avatar" src="{{check_profile_picture($pce->user->profile_picture)}}" alt="{{$pce->user->name}}" title="{{$pce->user->name}}" role="presentation" style="width: 46px;height: 46px;" /></a>
                        @endforeach
      </div>
      
    </div>
  </div>
</div>


<!-- LOCAL EXPERTS MODAL -->
<div class="modal bootstrap" id="LocalExpertsModal" tabindex="-1" role="dialog" aria-labelledby="LocalExpertsModalModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Local Experts in {{$country->transsingle->title}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          @foreach($country->experts AS $key=>$pce)
                        <a class="user-list__user" href="{{url_with_locale('profile/'.$pce->user->id)}}"><img class="user-list__avatar" src="{{check_profile_picture($pce->user->profile_picture)}}" alt="{{$pce->user->name}}" title="{{$pce->user->name}}" role="presentation" style="width: 46px;height: 46px;" /></a>
                        @endforeach
      </div>
      
    </div>
  </div>
</div>

<!-- FRIENDS EXPERTS MODAL -->
<div class="modal bootstrap" id="FriendsExpertsModal" tabindex="-1" role="dialog" aria-labelledby="FriendsExpertsModalModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Friends Experts in {{$country->transsingle->title}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          @foreach($country->experts AS $key=>$pce)
                        <a class="user-list__user" href="{{url_with_locale('profile/'.$pce->user->id)}}"><img class="user-list__avatar" src="{{check_profile_picture($pce->user->profile_picture)}}" alt="{{$pce->user->name}}" title="{{$pce->user->name}}" role="presentation" style="width: 46px;height: 46px;" /></a>
                        @endforeach
      </div>
      
    </div>
  </div>
</div>


<!-- LIVE EXPERTS MODAL -->
<div class="modal bootstrap" id="LiveExpertsModal" tabindex="-1" role="dialog" aria-labelledby="LiveExpertsModalModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Live Experts in {{$country->transsingle->title}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          @foreach($country->experts AS $key=>$pce)
                        <a class="user-list__user" href="{{url_with_locale('profile/'.$pce->user->id)}}"><img class="user-list__avatar" src="{{check_profile_picture($pce->user->profile_picture)}}" alt="{{$pce->user->name}}" title="{{$pce->user->name}}" role="presentation" style="width: 46px;height: 46px;" /></a>
                        @endforeach
      </div>
      
    </div>
  </div>
</div>
