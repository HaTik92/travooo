<style>
.fake__card
{
    position: absolute;
    bottom: 0;
    height: 100%;
    cursor: pointer;
}
</style>
<div class="hero" data-id="0" style="background-image: url(@if(isset($country->getMedias[0]->url)) @if($country->getMedias[0]->thumbs_done==1)https://s3.amazonaws.com/travooo-images2/th1100/{{$country->getMedias[0]->url}} @else https://s3.amazonaws.com/travooo-images2/{{$country->getMedias[0]->url}} @endif @else {{asset('assets2/image/placeholders/pattern.png')}} @endif);width: 1060px;margin-left: auto;margin-right: auto;height: 550px;">
    <div class="hero__content">
        <h1 class="hero__title" style="font-size:30px;">{{$country->trans[0]->title}}</h1>
        <div class="hero__rating">Country in {{$country->region->transsingle->title}}
        </div>

        
        <div class="hero__meta">
            <div class="hero__meta-header">
                <h2 class="hero__meta-title">About</h2><a class="hero__see-all js-show-modal" href="#modal-about">See All</a>
            </div>
            <div class="hero__meta-items">
                <div class="hero__meta-item">
                    <div class="hero__meta-value">{{count($country->followers)}} is following this country</div>
                </div>
                <div class="hero__meta-item">
                    <img src="{{asset('assets3/img/clock.png?ver=2')}}" style="width: 24px;margin-right: 6px;margin-top: -3px;">
                    <div class="hero__meta-value">{{count($country->timezones)}} Time zones</div>
                </div>
                
            </div>
        </div>
        <div class="hero__buttons">
            <span id="followContainer1">
                
            </span>
            <span id="checkinContainer1">
                <a class="btn btn--secondary" href="#" style='font-family: inherit;'>
                    <svg class="icon icon--location button_checkin"><use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use>
                    </svg>Check-in
                </a>
            </span>
        </div>
        <div class="hero__links">
            <a class="hero__link" href="#"><svg class="icon icon--add-plan">
                <use xlink:href="{{asset('assets3/img/sprite.svg#add-plan')}}"></use>
                </svg>
                <span class="hero__link-text" data-toggle="modal" data-target="#AddToPlanModal"><strong>Add</strong> to a trip plan</span>
            </a>
            {{--
            Not a Hotel , remove the check prices button
            <a class="hero__link" href="#"><svg class="icon icon--tag">
                <use xlink:href="{{asset('assets3/img/sprite.svg#tag')}}"></use>
                </svg>
                <span class="hero__link-text"><strong>Check Prices</strong> of this country</span>
            </a>
            --}}
            <a class="hero__link" href="#"><svg class="icon icon--plane">
                <use xlink:href="{{asset('assets3/img/sprite.svg#plane')}}"></use>
                </svg>
                <span class="hero__link-text" data-toggle="modal" data-target="#FlyToModal"><strong>Fly To</strong> this country</span>
            </a>
            <a class="hero__link" href="#"><svg class="icon icon--share">
                <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
                </svg>
                <span class="hero__link-text js-show-modal" href="#modal-share"><strong>Share</strong> this country</span>
            </a>
        </div>
    </div>
    <div class="fake__card mediaModalTrigger" data-id="0"></div>
    <div class="hero__cards">
       
        
        
    </div>
    
    <div class="user-list hero__user-list" style="position:fixed;z-index:1000;-webkit-box-shadow: 0px -2px 6px 0px rgba(0,0,0,0.23);-moz-box-shadow: 0px -2px 6px 0px rgba(0,0,0,0.23);box-shadow: 0px -2px 6px 0px rgba(0,0,0,0.23);">
        
            
            <?php $pce0_count = 0; $output_0 = '';?>
            @foreach($country->experts AS $key=>$pce)
                @if($pce->user->nationality!=$me->nationality && !is_friend($pce->user->id))
                <?php
                $output_0 .= '<a class="user-list__user js-show-modal" href="#ExpertsModal" data-id="1"><img class="user-list__avatar" src="'.check_profile_picture($pce->user->profile_picture).'" alt="'.$pce->user->name.'" title="'.$pce->user->name.'" role="presentation" style="width: 46px;height: 46px;" /></a>';
                ?>
                    @if($loop->index==4)
                    @break
                    @endif
                <?php $pce0_count++; ?>
                @endif
            @endforeach
            @if($pce0_count>5)
            <?php
            $output_0 .= '<a class="user-list__more js-show-modal" href="#ExpertsModal" data-id="1">+'.($pce0_count-5).'</a>';
            ?>
            @endif
            
            @if($output_0!=='')
            <div class="user-list__item">
             <h2 class="user-list__title js-show-modal" href="#ExpertsModal" data-id="1" style="cursor:pointer;">Top <br/> Experts</h2>
            {!!$output_0!!}
            </div>
            @endif
            
        
        
            <?php $pce1_count = 0; $output_1 = '';?>
            
            @foreach($country->experts AS $key=>$pce)
                @if($pce->user->nationality==$me->nationality)
                <?php
                $output_1 .= '<a class="user-list__user js-show-modal" href="#ExpertsModal" data-id="2"><img class="user-list__avatar" src="'.check_profile_picture($pce->user->profile_picture).'" alt="'.$pce->user->name.'" title="'.$pce->user->name.'" role="presentation" style="width: 46px;height: 46px;" /></a>';
                ?>
                    @if($loop->index==2)
                    @break
                    @endif
                <?php $pce1_count++; ?>
                @endif
            @endforeach
            @if($pce1_count>3)  
            <?php
            $output_1 .= '<a class="user-list__more js-show-modal" href="#ExpertsModal" data-id="2">+'.($pce1_count-3).'</a>';
            ?>
            @endif
            @if($output_1!=='')
            <div class="user-list__item">
            <h2 class="user-list__title js-show-modal" href="#ExpertsModal" data-id="2" style="cursor:pointer;">Local <br/> Experts</h2>
            {!!$output_1!!}
            </div>
            @endif
            
            
        
        
            <?php $pce2_count = 0; $output_2 = '';?>
            
            
            @foreach($country->experts AS $key=>$pce)
                @if(is_friend($pce->user->id))
                <?php
                $output_2 .= '<a class="user-list__user js-show-modal" href="#ExpertsModal" data-id="3"><img class="user-list__avatar" src="'.check_profile_picture($pce->user->profile_picture).'" alt="'.$pce->user->name.'" title="'.$pce->user->name.'" role="presentation" style="width: 46px;height: 46px;" /></a>';
                ?>
                
                    @if($loop->index==2)
                    @break
                    @endif
                    <?php $pce2_count++; ?>
                @endif
            @endforeach
            @if($pce2_count>3)   
            <?php
                $output_2 .= '<a class="user-list__more js-show-modal" href="#ExpertsModal" data-id="3">+'.($pce2_count-3).'</a>';
            ?>
            @endif
            @if($output_2!=='')
            <div class="user-list__item">
            <h2 class="user-list__title js-show-modal" href="#ExpertsModal" data-id="3" style="cursor:pointer;">My <br/> Friends</h2>
            {!!$output_2!!}
             </div>
            @endif
       
        
            <?php $pce3_count = 0; $output_3 = ''; ?>
            @foreach($country->checkins()->groupBy('users_id')->get() AS $key=>$pci)
                @if(is_object($pci->user))
                <?php
                $output_3 .= '<a class="user-list__user js-show-modal" href="#ExpertsModal" data-id="4"><img class="user-list__avatar" src="'.check_profile_picture($pci->user->profile_picture).'" alt="'.$pci->user->name.'" title="'.$pci->user->name.'" role="presentation" style="width: 46px;height: 46px;" /></a>';
                ?>
                    @if($loop->index==3)
                    @break
                    @endif
                    <?php $pce3_count++; ?>
                @endif
            @endforeach
                @if(count($country->checkins()->groupBy('users_id')->get())>4)    
                <?php
                $output_3 .= '<a class="user-list__more js-show-modal" href="#ExpertsModal" data-id="4">+'.(count($place->checkins()->groupBy('users_id')->get())-4).'</a>';
                ?>
                @endif
            
                @if($output_3!=='')
                <div class="user-list__item">
                <h2 class="user-list__title js-show-modal" href="#ExpertsModal" data-id="4" style="cursor:pointer;">Live <br/> Check-ins</h2>
                {!!$output_3!!}
                </div>
                @endif
        
    </div>
</div>