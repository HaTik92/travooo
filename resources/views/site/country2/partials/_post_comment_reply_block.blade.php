<div class="comment" style="padding-left: 45px">
    <img class="comment__avatar" src="{{check_profile_picture($reply->author->profile_picture)}}" alt="" role="presentation" />
    <div class="comment__content">
        <div class="comment__header"><a class="comment__username" href="{{url_with_locale('profile/'.$reply->author->id)}}">{{$reply->author->name}}</a>

        </div>
        <div class="comment__text">{{$reply->text}}</div>
        <div class="comment__footer">
            <button class="comment__like-btn comment__like-button" type="button" id="{{$reply->id}}">
                <svg class="icon icon--like">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
                </svg>
                <strong>{{count($reply->likes)}}</strong>
            </button>
            <button class="comment__like-btn " type="button" onclick="injectData({{$reply->id}},this)" data-toggle="modal" data-target="#spamReportDlg">
                <svg class="icon icon--like" style="color: #4080ff">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#flag')}}"></use>
                </svg>
            </button>
            <!-- <button class="comment__reaction-btn" type="button"><span class="emoji">😆</span>5</button> -->
            @if($reply->author->id==Auth::user()->id)
            <button class="comment__reaction-btn" type="button" onclick="post_comment_delete({{ $reply->id }}, this)" style="color:red">Delete</button>
            @endif
            <div class="comment__posted">{{diffForHumans($reply->created_at)}}</div>
        </div>
    </div>
</div>