<?php
    $mapindex = rand(1, 500);
    $plan_comments = $plan->comments()->whereNull('places_id')->orderBy('created_at', 'DESC')->get();
?>
<div class="post" filter-tab="#tripplan">
    <div class="post__header">
        <img class="post__avatar" src="{{check_profile_picture($plan->author->profile_picture)}}" alt="" role="presentation">
        <div class="post__title-wrap">
            <a class="post__username" href="{{url_with_locale('profile/'.$plan->author->id)}}">{{$plan->author->name}}</a>
            <div class="post__posted">Planned a <strong>trip</strong> to {{$country->transsingle->title}} at {{diffForHumans($plan->created_at)}}</div>
        </div>
        <div class="dropdown">
            <button class="post__menu" type="button" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg class="icon icon--angle-down">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#angle-down')}}"></use>
                </svg>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Trip">
                <a class="dropdown-item plan_share_button" href="#" id="{{$plan->id}}">
                    <span class="icon-wrap">
                        <i class="trav-share-icon"></i>
                    </span>
                    <div class="drop-txt">
                        <p><b>@lang('home.share')</b></p>
                        <p>@lang('home.spread_the_word')</p>
                    </div>
                </a>
                @if(Auth::user()->id && Auth::user()->id!==$review->by_users_id)
                    @include('site.home.partials._info-actions', ['post'=>$plan])
                @endif
            </div>
        </div>
    </div>
    <div class="carousel">
        <div class="carousel__items">
            <div class="carousel__item">
                <div id="plan_map{{$mapindex}}" style='width:590px;height:400px;'></div>
                <div class="carousel-cards" style="overflow: hidden">
                    @foreach($plan->places AS $pplace)
                    <div class="carousel-cards__item" style="display: table-cell;cursor:pointer;">
                        <span class="carousel-cards__card carousel-cards__card--active" onclick="setCenter_usermap(map{{$mapindex}},{{$pplace->lat}}, {{$pplace->lng}})">
                            <img class="carousel-cards__card-img" src="{{check_place_photo($pplace)}}" alt="" role="presentation" style="width:68px;height:68px;"/>
                            <span class="carousel-cards__card-content">
                                <div class="carousel-cards__card-name">{{$pplace->transsingle->title}}</div>
                                <div class="carousel-cards__card-label">{{do_placetype($pplace->place_type)}}</div>
                            </span>
                        </span>
                    </div>
                    @endforeach
                    
                </div>
            </div>
            
        </div>
    </div>
    <div class="post__footer">
        <button class="post__like-btn" style="outline:none;" type="button" onclick="tripslike({{$plan->id}}, this)">
            <svg class="icon icon--like">
                <use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
            </svg>
            <span><strong>{{@count($plan->likes()->whereNull('places_id')->get())}}</strong> Likes</span>
        </button>
        <button class="post__like-btn plan_share_button" style="outline:none;" type="button" id="{{$plan->id}}">
            <svg class="icon icon--like" style="color: #4080ff">
            <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
            </svg>
            <span id="plan_share_count_{{$plan->id}}"><strong>{{@count($plan->shares)}}</strong> Shares</span>
        </button>
        <button class="post__comment-btn" id="plan_post_comments" type="button" data-tab="tripscomment{{ $plan->id }}" style="outline:none;">
            <svg class="icon icon--comment">
                <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
            </svg>
            <div class="user-list">
                <div class="user-list__item">
                    @if(count($plan_comments) > 0)
                        @foreach($plan->comments()->whereNull('places_id')->groupBy('users_id')->get() as $k=>$com)
                            @if($k < 3)
                             <div class="user-list__user"><img class="user-list__avatar" src="{{check_profile_picture($com->author->profile_picture)}}" alt="" role="presentation"></div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div><span><strong>{{@count($plan_comments)}}</strong> Comments</span>
        </button>
    </div>
    <div class="post-comment-layer" data-content="tripscomment{{ $plan->id }}" style='display:none; padding: 20px 30px 18px;'>
        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <!-- <li onclick="commentSort('Top', this)">@lang('comment.top')</li>
                <li class="active" onclick="commentSort('New', this)">@lang('home.new')</li> -->
            </ul>
            <div class="comm-count-info">
            </div>
        </div>
        <div class="post-comment-wrapper sortBody" id="tripscomment">
            @if(@count($plan_comments)>0)
                @foreach($plan_comments AS $comment)
                    @include('site.home.partials.trip_comment_block')
                @endforeach
            @endif
        </div>
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-input">
                <form class="tripscomment" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="post-block post-create-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" id="tripscommenttext" class="textarea-customize"
                                style="display:inline;vertical-align: top;height:40px;" placeholder="@lang('comment.write_a_comment')"></textarea>
                            <div class="medias">
                                <!-- <div class="plus-icon" style="display: none;">
                                    <div>
                                        <span class="close" onclick="javascript:$('#commentfile').click();"><span style="font-size:110px;">+</span></span>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <!-- <li class="post-options">
                                        <input type="file" name="file[]" id="commentfile" style="display:none" multiple>
                                        <i class="trav-camera click-target" data-target="commentfile"></i>
                                    </li> -->
                                </ul>
                            </div>

                            <button type="submit" class="btn btn--sm btn--main">@lang('buttons.general.send')</button>
                            
                        </div>
                        
                    </div>
                    <input type="hidden" name="post_id" value="{{$plan->id}}"/>
                </form>

                </div>
            </div>
            
        @endif
    </div>
</div>
<script>

    
    map_var += `var map{{$mapindex}}, center_lat{{$mapindex}}, center_lng{{$mapindex}};`;

    map_func += `map{{$mapindex}} = new google.maps.Map(document.getElementById('plan_map{{$mapindex}}'), {
            zoom: 4,
            center: new google.maps.LatLng(28.36539800, - 81.52126420),
            mapTypeId: 'satellite'
        });
        @foreach(get_plan_map_data($plan) AS $checkin)
            @if ($checkin['lat'] && $checkin['lng'])
                center_lat{{$mapindex}} = {{$checkin['lat']}};
                center_lng{{$mapindex}} = {{$checkin['lng']}};
                var marker{{$mapindex}}{{$loop->index}} = new google.maps.Marker({
                    position: {lat: {{$checkin['lat']}}, lng: {{$checkin['lng']}}},
                    map: map{{$mapindex}},
                    title: "{{$checkin['title']}}",
                    color: "{{$checkin['color']}}",
                    label: "{{$checkin['label']}}"
                });

                marker{{$mapindex}}{{$loop->index}}.addListener('click', function() {
                    map{{$mapindex}}.setZoom(16);
                    map{{$mapindex}}.setCenter(marker{{$mapindex}}{{$loop->index}}.getPosition());
                });
            @endif
        @endforeach
        map{{$mapindex}}.setCenter(new google.maps.LatLng(center_lat{{$mapindex}}, center_lng{{$mapindex}}));
    `;
</script>