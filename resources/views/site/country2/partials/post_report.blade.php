<div class="post" filter-tab="#report">
    <div class="post__header">
        <img class="post__avatar" src="{{check_profile_picture($report->author->profile_picture)}}" alt="" role="presentation">
        <div class="post__title-wrap">
            <a class="post__username" href="{{@url_with_locale('profile/'.$report->author->id)}}">{{$report->author->name}}</a>
            <div class="post__posted">Wrote a 
                <strong>travelog</strong> about
                {{$place->transsingle->title}} at 
                {{diffForHumans($report->created_at)}}
            </div>
        </div>
        <div class="dropdown">
            <button class="post__menu" type="button" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg class="icon icon--angle-down">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#angle-down')}}"></use>
                </svg>
            </button>
            @if($report->author->id !== auth()->id())
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Report">
                    @include('site.home.partials._info-actions', ['post'=>$report])
                </div>
            @endif

        </div>
    </div>
    <div class="post__content">
        <div class="post-media">
            <div class="post-media__content" style="width: 96%;">
                <h2 class="post-media__title">{{$report->title}}</h2>
                <div class="post-media__text" style="overflow-wrap: break-word;">{{$report->description}}</div>
            </div>
        </div>
    </div>
    <div class="post__footer">
        <button class="post__like-btn report_like_button" style="outline:none;" type="button" id="{{$report->id}}">
            <svg class="icon icon--like">
            <use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
            </svg>
            <span id="report_like_count_{{$report->id}}"><strong>{{@count($report->likes)}}</strong> Likes</span>
        </button>

        <button class="post__comment-btn place-comment-btn" style="outline:none;" type="button" data-tab="report__commenttext_{{$report->id}}" data-id="{{$report->id}}">
            <svg class="icon icon--comment">
            <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
            </svg>
            <div class="user-list">
                <div class="user-list__item">
                    @foreach(@$report->comments()->groupBy('users_id')->get() AS $pco)
                    <div class="user-list__user"><img class="user-list__avatar" src="{{check_profile_picture($pco->author->profile_picture)}}" alt="{{$pco->author->name}}" title="{{$pco->author->name}}" role="presentation" /></div>
                    @endforeach
                </div>
            </div><span class="{{$report->id}}-post-comments-count"><strong>{{@count($report->comments)}}</strong> Comments</span>
        </button>

        <button class="post__like-btn report_share_button" type="button" id="{{$report->id}}">
            <svg class="icon icon--like" style="color: #4080ff">
            <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
            </svg>
            <span id="report_share_count_{{$report->id}}"><strong>{{@count($report->shares)}}</strong> Shares</span>
        </button>
    </div>
    <div class="comments pp-report-{{$report->id}}" style="display:none;" data-content="report__commenttext_{{$report->id}}">
        <div class="comments__header" id="{{$report->id}}" {{(count($report->comments)>0)?"":'style=display:none'}}>
            <div class="comments__filter">
                <button class="comments__filter-btn comment-filter-type" type="button" data-type="Top" data-report_id="{{$report->id}}">Top</button>
                <button class="comments__filter-btn comments__filter-btn--active comment-filter-type" type="button" data-type="New" data-report_id="{{$report->id}}">New</button></div>
                <div class="comments__number comm-count-info">
                    <strong>0</strong> / <span class="{{$report->id}}-all-comments-count">{{count($report->comments)}}</span>
                </div>
        </div>
        <div class="comments__items sortBody">
            @if(count($report->comments)>0)
                @foreach($report->comments()->orderBy('created_at', 'desc')->get() AS $ppc)
                <div class="comment" topsort="{{count($ppc->likes)}}" newsort="{{strtotime($ppc->created_at)}}">
                    <img class="comment__avatar" src="{{check_profile_picture($ppc->author->profile_picture)}}" alt="" role="presentation" />
                    <div class="comment__content" style="width: 89%;">
                        <div class="comment__header" style="position: relative;"><a class="comment__username" href="{{url_with_locale('profile/'.$ppc->author->id)}}">{{$ppc->author->name}}</a>
                            <div class="comment__posted com-time" style="position: absolute;right: 0;">{{diffForHumans($ppc->created_at)}}</div>
                        </div>
                        <div class="comment__text" style="overflow-wrap: break-word;padding: 12px 0;">
                            @php
                                $showChar = 100;
                                $ellipsestext = "...";
                                $moretext = "see more";
                                $lesstext = "see less";

                                $content = $ppc->comment;

                                if(strlen($content) > $showChar) {

                                    $c = substr($content, 0, $showChar);
                                    $h = substr($content, $showChar, strlen($content) - $showChar);

                                    $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                                    $content = $html;
                                }
                            @endphp
                            {!!$content!!}
                        </div>
                        <div class="comment__footer">
                            <div class="com-time report-comment-actions" posttype="Reportcomment">
                                <a href="#" class="report_comment_like" id="{{$ppc->id}}">{{count($ppc->likes)}} @lang('home.likes')</a>
                                @if($ppc->users_id==Auth::user()->id)
                                - <a href="#" reportid="{{$report->id}}" class="report-comment-delete" id="{{$ppc->id}}" type="2" style="color:red">Delete</a>
                                @endif
                                - <a href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$ppc->id}},this)"> Report</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
             @else
                <div class="post-comment-top-info comment-not-fund-info" id="{{$report->id}}">
                    <ul class="comment-filter">
                        <li>No comments yet ..</li>
                        <li></li>
                    </ul>
                    <div class="comm-count-info">
                    </div>
                </div>
            @endif
        </div>
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-input">
                <form class="reportscomment" method="POST" action="{{url("new-comment") }}" id="{{$report->id}}" autocomplete="off" enctype="multipart/form-data">
                    <div class="post-block post-create-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" id="postscommenttext" class="textarea-customize"
                                style="display:inline;vertical-align: top;height:40px;" placeholder="@lang('comment.write_a_comment')"></textarea>
                            <div class="medias">
                                <!-- <div class="plus-icon" style="display: none;">
                                    <div>
                                        <span class="close" onclick="javascript:$('#commentfile').click();"><span style="font-size:110px;">+</span></span>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <!-- <li class="post-options">
                                        <input type="file" name="file[]" id="commentfile" style="display:none" multiple>
                                        <i class="trav-camera click-target" data-target="commentfile"></i>
                                    </li> -->
                                </ul>
                            </div>

                            <button type="submit" class="btn btn--sm btn--main">@lang('buttons.general.send')</button>
                            
                        </div>
                        
                    </div>
                    <input type="hidden" name="report_id" value="{{$report->id}}"/>
                </form>

                </div>
            </div>
            
        @endif
    </div>
</div>