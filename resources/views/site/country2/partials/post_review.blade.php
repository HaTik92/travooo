<div class="user-card post" filter-tab="#reviews">
    <div class="dropdown">
        <button class="post__menu" type="button" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <svg class="icon icon--angle-down">
                <use xlink:href="{{asset('assets3/img/sprite.svg#angle-down')}}"></use>
            </svg>
        </button>
        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Review">
            @if (Auth::user()->id )
                @if(Auth::user()->id==$review->by_users_id)
                    <a class="dropdown-item" href="#" onclick="post_delete('{{$review->id}}','Review', this, event)">
                        <span class="icon-wrap">
                            <!--<i class="trav-flag-icon-o"></i>-->
                        </span>
                        <div class="drop-txt">
                            <p><b>@lang('buttons.general.crud.delete')</b></p>
                            <p style="color:red">@lang('home.remove_this_post')</p>
                        </div>
                    </a>
                @else
                    @include('site.home.partials._info-actions', ['post'=>$review])
                @endif
            @endif
            <a class="dropdown-item" href="#" onclick="review_share('{{$review->id}}', event)">
                <span class="icon-wrap">
                    <i class="trav-share-icon"></i>
                </span>
                <div class="drop-txt">
                    <p><b>@lang('home.share')</b></p>
                    <p>@lang('home.spread_the_word')</p>
                </div>
            </a>
        </div>

    </div>
    <div class="user-card__main" style="width: 80%;"><img class="user-card__avatar" src="{{$review->google_profile_photo_url ? $review->google_profile_photo_url : check_profile_picture($review->author->profile_picture)}}" alt="" role="presentation">
        <div class="user-card__content" style="word-wrap: break-word;overflow: hidden;">
        <div class="user-card__header"><a class="user-card__name" href="{{@url_with_locale('profile/'.$review->author->id)}}">{{$review->google_author_name ? $review->google_author_name : $review->author->name}}</a> reviewed this place at {{$review->google_time ? diffForHumans(date("Y-m-d", $review->google_time)) : diffForHumans($review->created_at)}}</div>
        <div class="user-card__text">{{$review->text}}</div>
        <div class="user-card__meta"><span class="user-card__meta-item"> </span></div>
        </div>
    </div>
    <div class="user-card__btn-wrap" style="width: 20%;">
        <svg class="icon icon--star @if($review->score>=1) icon--star-active @endif"><use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use></svg>
        <svg class="icon icon--star @if($review->score>=2) icon--star-active @endif"><use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use></svg>
        <svg class="icon icon--star @if($review->score>=3) icon--star-active @endif"><use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use></svg>
        <svg class="icon icon--star @if($review->score>=4) icon--star-active @endif"><use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use></svg>
        <svg class="icon icon--star @if($review->score>=5) icon--star-active @endif"><use xlink:href="{{asset('assets3/img/sprite.svg#star')}}"></use></svg>
        <div class="rating__value"><strong>{{$review->score}}</strong> / 5</div>
    </div>
    <div class="mt-2 updownvote" id="rating_updownvote">
        <a href="#" class="upvote-link review-vote up disabled" id="{{$review->id}}">
            <span class="arrow-icon-wrap"><i class="trav-angle-up"></i></span>
        </a>
        <span class="upvote-{{$review->id}}"><b>{{count($review->updownvotes()->where('vote_type', 1)->get())}}</b> Upvotes</span>
        &nbsp;&nbsp;
        <a href="#" class="upvote-link review-vote down disabled" id="{{$review->id}}">
            <span class="arrow-icon-wrap"><i class="trav-angle-down"></i></span>
        </a>
        <span class="downvote-{{$review->id}}"><b>{{count($review->updownvotes()->where('vote_type', 2)->get())}}</b> Downvotes</span>
    </div>
</div>
