<!-- MEDIA WITH TEXT -->

<div class="post" filter-tab="#media">
    <div class="post__header"><img class="post__avatar" src="{{check_profile_picture($post->author->profile_picture)}}" alt="" role="presentation">
        <div class="post__title-wrap"><a class="post__username" href="{{url_with_locale('profile/'.$post->author->id)}}">{{$post->author->name}}</a>
            <div class="post__posted">Uploaded a <strong>photo</strong> {{diffForHumans($post->created_at)}}</div>
        </div>
        <div class="dropdown">
            <button class="post__menu" type="button" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg class="icon icon--angle-down">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#angle-down')}}"></use>
                </svg>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post">
                @if(Auth::user()->id)
                    @if(Auth::user()->id==$post->users_id)
                        <a class="dropdown-item" href="#" onclick="post_delete('{{$post->id}}','Post', this, event)">
                            <span class="icon-wrap">
                                <!--<i class="trav-flag-icon-o"></i>-->
                            </span>
                            <div class="drop-txt">
                                <p><b>@lang('buttons.general.crud.delete')</b></p>
                                <p style="color:red">@lang('home.remove_this_post')</p>
                            </div>
                        </a>
                    @else
                        @include('site.home.partials._info-actions', ['post'=>$post])
                    @endif
                @endif
</div>
</div>
</div>
<div class="post__content">
<div class="post-media">
<div class="post-media__caption">{!! $post->text !!}</div>
@foreach($post->medias as $pma)
@php
    $file_url = $pma->media->url;
    $file_url_array = explode(".", $file_url);
    $ext = end($file_url_array);
    $allowed_video = array('mp4');
@endphp
@if(in_array($ext, $allowed_video))
<video class="post-media__img" controls>
    <source src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" type="video/mp4">
    Your browser does not support the video tag.
</video>
@else
<img class="post-media__img" src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" alt="" role="presentation">
@endif
@endforeach
</div>
</div>
<div class="post__footer">
<button class="post__like-btn post_like_button" style="outline:none;" type="button" id="{{$post->id}}">
<svg class="icon icon--like">
<use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
</svg>
<span id="post_like_count_{{$post->id}}"><strong>{{count($post->likes)}}</strong> Likes</span>
</button>
<button class="post__comment-btn" style="outline:none;" type="button" data-tab="post__media_{{$post->id}}">
<svg class="icon icon--comment">
<use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
</svg>
<div class="user-list">
<div class="user-list__item">
    @foreach($post->comments()->groupBy('users_id')->get() AS $pco)
    <div class="user-list__user"><img class="user-list__avatar" src="{{check_profile_picture($pco->author->profile_picture)}}" alt="{{$pco->author->name}}" title="{{$pco->author->name}}" role="presentation" /></div>
    @endforeach
</div>
</div>
<span><strong>{{count($post->comments)}}</strong> Comments</span>
</button>
</div>
<div class="comments" style="display:none" data-content="post__media_{{$post->id}}">
<div class="comments__header">
<div class="comments__filter"><button class="comments__filter-btn comments__filter-btn--active" type="button">Top</button><button class="comments__filter-btn" type="button">New</button></div>
<div class="comments__number">3 / 20</div>
</div>
<div class="comments__items">
@foreach($post->comments AS $ppc)
@include('site.place2.partials._post_comment_block')
@endforeach
<button class="comments__load-more" type="button">Load more...</button>
</div>
@if(Auth::user())
<div class="post-add-comment-block">
<div class="avatar-wrap">
    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
            style="width:45px;height:45px;">
</div>
<div class="post-add-com-input">
<form class="postscomment" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
    <div class="post-block post-create-block" id="createPostBlock" tabindex="0">
        <div class="post-create-input">
            <textarea name="text" id="postscommenttext" class="textarea-customize"
                style="display:inline;vertical-align: top;height:40px;" placeholder="@lang('comment.write_a_comment')"></textarea>
            <div class="medias">
                <!-- <div class="plus-icon" style="display: none;">
                    <div>
                        <span class="close" onclick="javascript:$('#commentfile').click();"><span style="font-size:110px;">+</span></span>
                    </div>
                </div> -->
            </div>
        </div>

        <div class="post-create-controls">
            <div class="post-alloptions">
                <ul class="create-link-list">
                    <!-- <li class="post-options">
                        <input type="file" name="file[]" id="commentfile" style="display:none" multiple>
                        <i class="trav-camera click-target" data-target="commentfile"></i>
                    </li> -->
                </ul>
            </div>

            <button type="submit" class="btn btn--sm btn--main">@lang('buttons.general.send')</button>

        </div>

    </div>
    <input type="hidden" name="post_id" value="{{$post->id}}"/>
</form>

</div>
</div>

@endif
</div>
</div>