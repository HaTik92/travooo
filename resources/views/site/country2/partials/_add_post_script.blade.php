<script>
    $(document).ready(function() {
        $('.add-post__emoji').emojiInit({
            fontSize:20,
            success : function(data){

            },
            error : function(data,msg){
            }
        });
    });
    $(".add-post__post-type").click(function(){
        $(".add-post__post-type").removeClass("add-post__post-type--active");
        $(this).addClass("add-post__post-type--active");
    });

    $(document).on('click', '.add-post__post-type', function(){
        var data_type = $(this).attr('data-type');
        if( data_type == "image" )
        {
            $("#_add_post_imagefile").click();
        }
        else if( data_type == "video" )
        {
            $("#_add_post_videofile").click();
        }
    });

    $("#_add_post_imagefile").change(function(){

        var file = this.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];

        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
        {
            $('#cover').css('background-image', 'url(noimage.png)');
            $("#message").html("<p style='color:red'>Please Select A valid Image File</p>");
            $("#_add_post_imagefile").val("");
            return false;
        } 
        else
        {
            var fRead = new FileReader();
            fRead.onload = (function(file){
                return function(e) {
                    var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element
                    var button = $('<span/>').addClass('close').addClass('removeFile').click(function(){     //create close button element
                        $(this).parent().remove();
                    }).append('<span>&times;</span>');

                    var inputitem = $('<input type="hidden" name="mediafiles[]" />').val(e.target.result);

                    var imgitem = $('<div>').append(img); //create Image Wrapper element
                    imgitem = $('<div/>').addClass('img-wrap-newsfeed').append(imgitem).append(button).append(inputitem);
                    
                    $(".add-post__media").append(imgitem);
                };
            })(file);
            fRead.readAsDataURL(file); //URL representing the file's data.
            $("#_add_post_imagefile").val("");
        }
    });

    $("#_add_post_videofile").change(function(){

        var file = this.files[0];
        
        if ( !/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type) )
        {
            $('#cover').css('background-image', 'url(noimage.png)');
            $("#message").html("<p style='color:red'>Please Select A valid Image File</p>");
            $("#_add_post_videofile").val("");
            return false;
        } 
        else
        {
            var fRead = new FileReader();
            fRead.onload = (function(file){
                return function(e) {

                    var video = $('<video style="object-fit:cover;" width="151" height="130" controls>'+
                    '<source src='+e.target.result+' type="video/mp4">'+
                    '<source src="movie.ogg" type="video/ogg">'+
                    '</video>');
                    
                    var button = $('<span/>').addClass('close').addClass('removeFile').click(function(){     //create close button element
                        $(this).parent().remove();
                    }).append('<span>&times;</span>');

                    var inputitem = $('<input type="hidden" name="mediafiles[]" />').val(e.target.result);

                    var videoitem = $('<div/>').append(video);
                    videoitem = $('<div/>').addClass('img-wrap-newsfeed').append(videoitem).append(button).append(inputitem);

                    $(".add-post__media").append(videoitem);
                };
            })(file);
            fRead.readAsDataURL(file); //URL representing the file's data.
            $("#_add_post_videofile").val("");
        }
    });

    $(document).on('click', '.comment__like-button', function (e) {
        var commentId = $(this).attr('id');
        var obj = $(this);
        $.ajax({
            method: "POST",
            url: "{{ route('post.commentlikeunlike') }}",
            data: {comment_id: commentId}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                if (res.status == 'yes') {


                } else if (res == 'no') {

                }
                obj.find('strong').html(result.count);
            });
        e.preventDefault();
    });

    $(document).on('click', '.report_comment__like-button', function (e) {
        var commentId = $(this).attr('id');
        var obj = $(this);
        $.ajax({
            method: "POST",
            url: "{{ route('report.commentlike') }}",
            data: {comment_id: commentId}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                if (res.status == 'yes') {


                } else if (res == 'no') {

                }
                obj.find('strong').html(result.count);
            });
        e.preventDefault();
    });

    $(document).on('keydown', '#postscommenttext', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '#postscommenttext', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            $(this).closest('form').find('button[type=submit]').click();
        }
    });

    $(document).on('submit', '.postscomment', function(e){
        var form = $(this);
        var text = form.find('textarea').val().trim();

        if(text == "")
        {
            return false;
        }
        var values = $(this).serialize();
        form.find('button[type=submit]').attr('disabled', true);
        $.ajax({
            method: "POST",
            url: "{{ route('place.postcomment') }}",
            data: values
        })
            .done(function (res) {
                form.closest('.comments').find('.comments__items').append(res);
                var comments = form.closest('.comments').find('.comments__items').find('.comment').length;
                form.closest('.post').find('.post__comment-btn').find('strong').html(comments);
                form.find('.medias').empty();
                form.find('textarea').val('');
                form.find('button[type=submit]').removeAttr('disabled');
            });
        e.preventDefault();
    });
    $(document).on('submit', '.reportscomment', function(e){
        var form = $(this);
        var text = form.find('textarea').val().trim();

        if(text == "")
        {
            return false;
        }
        
        var report_id = form.attr('id');
        var all_comment_count = $('.'+ report_id +'-all-comments-count').html(); 
        var values = $(this).serialize();
        form.find('button[type=submit]').attr('disabled', true);
        $.ajax({
            method: "POST",
            url: "{{ route('report.postcomment') }}",
            data: values
        })
            .done(function (res) {
                $("#" + report_id + ".comment-not-fund-info").remove();
                $("#" + report_id + ".comments__header").show();
                $('.'+ report_id +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                
                form.closest('.comments').find('.comments__items').prepend(res);
                var comments = form.closest('.comments').find('.comments__items').find('.comment').length;
                form.closest('.post').find('.post__comment-btn').find('strong').html(comments);
                form.find('.medias').empty();
                form.find('textarea').val('');
                form.find('button[type=submit]').removeAttr('disabled');
                
                Comment_Show.reload($('.pp-report-'+report_id)); 
                
            });
        e.preventDefault();
    });
    $(document).on('submit', '.postscommentreply', function(e){
        var form = $(this);
        var text = form.find('textarea').val().trim();

        if(text == "")
        {
            return false;
        }
        var values = $(this).serialize();
        form.find('button[type=submit]').attr('disabled', true);
        $.ajax({
            method: "POST",
            url: "{{ route('place.postcommentreply') }}",
            data: values
        })
            .done(function (res) {
                form.closest('.post-add-comment-block').before(res);
                form.find('.medias').empty();
                form.find('textarea').val('');
                form.find('button[type=submit]').removeAttr('disabled');
            });
        e.preventDefault();
    });
</script>

<script>
    function post_comment_delete(id, obj)
    {
        if(!confirm("Are you sure delete this comment?"))
            return;
        var commentId = id;
        $.ajax({
            method: "POST",
            url: "{{ route('post.commentdelete') }}",
            data: {comment_id: commentId}
        })
            .done(function (res) {

                $(obj).closest('.comment').remove();
            });
    }
    function report_comment_delete(id, obj)
    {
        if(!confirm("Are you sure delete this comment?"))
            return;
        var commentId = id;
        $.ajax({
            method: "POST",
            url: "{{ route('report.commentdelete') }}",
            data: {comment_id: commentId}
        })
            .done(function (res) {

                $(obj).closest('.comment').remove();
            });
    }
    function _place_send_post(obj)
    {
        var type = $(".add-post__post-type--active").attr("data-type");
        if(!type)
            return;

        switch (type) {
            case 'text':
                if ($("#_place_add_post_form").find('textarea').val().trim() == "" ) return;
                break;
        
            case 'image':
            case 'video':
                if ($("#_place_add_post_form").find("input[name^=mediafiles]").length == 0) return;
                
                break;

            case 'info':
                return;
                break;
            case 'map':
                return;
                break;
            default:
                break;
        }
        
        $("#_place_add_post_form").find("input[name=type]").val(type);

        var buttons = $(obj).closest(".add-post__footer");
        var loading = buttons.next();
        buttons.hide();
        loading.show();
        $.post(
            "{{route('place.addpost', $country->id)}}",
            {data: $("#_place_add_post_form").serializeArray()},
            function(res)
            {
                if(res != "error")
                {
                    $(".page-content__main").find(".posts-filter").after(res);
                    $("#_place_add_post_form").find('textarea').val('');
                    $(".add-post__media").html('');
                    $("#_place_add_post_form").find("input[name=type]").val('text');
                    $( ".add-post__post-type" ).each(function() {
                        if($(this).attr("data-type")!='text') {
                            $( this ).removeClass( "add-post__post-type--active" );
                        }
                        else {
                             $( this ).addClass( "add-post__post-type--active" );
                        }
                      });
                    
  
                      
                      
                }

                buttons.show();
                loading.hide();
            }
        );
    }

    function post_delete(param, type, obj, e) 
    {
        if(confirm('Are you sure you want to delete this post?'))
        {
            $.ajax({
                method: "POST",
                url: "{{ route('post.delete') }}",
                data: {post_id: param, post_type: type} 
            })
            .done(function(res) {
                result = JSON.parse(res);
                if(result.status == "yes")
                {
                    $(obj).closest('.post').remove();
                }else{
                    //
                    alert(result.status);
                }
            });
        }

        e.preventDefault();
    }


</script>