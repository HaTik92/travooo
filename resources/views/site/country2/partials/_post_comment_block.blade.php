
<?php
    $comment_childs = \App\Models\Posts\PostsComments::where('parents_id', '=', $ppc->id)->orderby('created_at','DESC')->get();
?>
<div class="comment">
    <img class="comment__avatar" src="{{check_profile_picture($ppc->author->profile_picture)}}" alt="" role="presentation" />
    <div class="comment__content">
        <div class="comment__header"><a class="comment__username" href="{{url_with_locale('profile/'.$ppc->author->id)}}">{{$ppc->author->name}}</a>

        </div>
        <div class="comment__text">{{$ppc->text}}</div>
        <div class="comment__footer" posttype="Postcomment">
            <button class="comment__like-btn comment__like-button" type="button" id="{{$ppc->id}}">
                <svg class="icon icon--like">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
                </svg>
                <strong>{{count($ppc->likes)}}</strong>
            </button>
            <button class="comment__like-btn " type="button" onclick="injectData({{$ppc->id}},this)" data-toggle="modal" data-target="#spamReportDlg">
                <svg class="icon icon--like" style="color: #4080ff">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#flag')}}"></use>
                </svg>
            </button>
            <!-- <button class="comment__reaction-btn" type="button"><span class="emoji">😆</span>5</button> -->
            @if($ppc->author->id==Auth::user()->id)
            <button class="comment__reaction-btn" type="button" onclick="post_comment_delete({{ $ppc->id }}, this)" style="color:red">Delete</button>
            @endif
            <button class="comment__reply" type="button" data-tab_reply="comment__reply{{$ppc->id}}">Reply</button>
            <div class="comment__posted">{{diffForHumans($ppc->created_at)}}</div>
        </div>
    </div>
</div>

@foreach($comment_childs AS $reply)
    @include('site.place2.partials._post_comment_reply_block')
@endforeach
@if(Auth::user())
    <div class="post-add-comment-block" style="padding-left: 45px; display: none" data-content_reply="comment__reply{{$ppc->id}}">
        <div class="avatar-wrap">
            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                    style="width:45px;height:45px;">
        </div>
        <div class="post-add-com-input">
        <form class="postscommentreply" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
            <div class="post-block post-create-block" id="createPostBlock" tabindex="0">
                <div class="post-create-input">
                    <textarea name="text" id="postscommenttext" class="textarea-customize"
                        style="display:inline;vertical-align: top;height:40px;" placeholder="@lang('comment.write_a_comment')"></textarea>
                    <div class="medias">
                        <!-- <div class="plus-icon" style="display: none;">
                            <div>
                                <span class="close" onclick="javascript:$('#commentfile').click();"><span style="font-size:110px;">+</span></span>
                            </div>
                        </div> -->
                    </div>
                </div>

                <div class="post-create-controls">
                    <div class="post-alloptions">
                        <ul class="create-link-list">
                            <!-- <li class="post-options">
                                <input type="file" name="file[]" id="commentfile" style="display:none" multiple>
                                <i class="trav-camera click-target" data-target="commentfile"></i>
                            </li> -->
                        </ul>
                    </div>

                    <button type="submit" class="btn btn--sm btn--main">@lang('buttons.general.send')</button>
                    
                </div>
                
            </div>
            <input type="hidden" name="post_id" value="{{$post->id}}"/>
            <input type="hidden" name="comment_id" value="{{$ppc->id}}"/>
        </form>

        </div>
    </div>
    
@endif