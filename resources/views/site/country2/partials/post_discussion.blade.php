<style>
    .post__tip
    {
        border-top: #e6e6e6 1px solid;
        padding-bottom: 15px;
        padding-top: 5px;
    }
    .post__tip-loadmore
    {
        padding: 0;
        border: 0;
        background-color: transparent;
        cursor: pointer;
    }
</style>

<div class="post" filter-tab="#discussion">

    <div class="post__header"><img class="post__avatar" src="{{ check_profile_picture($discussion->author->profile_picture) }}" alt="" role="presentation">
        <div class="post__title-wrap"><a class="post__username" href="{{url_with_locale('profile/'.$discussion->author->id)}}">{{$discussion->author->name}}</a>
            <div class="post__posted">Asked for <strong>recommendations</strong> in {{$country->transsingle->title}} at {{diffForHumans($discussion->created_at)}}</div>
        </div>
        <div class="dropdown">
            <button class="post__menu" type="button" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg class="icon icon--angle-down">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#angle-down')}}"></use>
                </svg>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion">
                @if($discussion->author->id !== Auth::user()->id)
                    @include('site.home.partials._info-actions', ['post'=>$discussion])
                @endif
            </div>
        </div>
        
    </div>
    <div class="post__content">
        <div class="post-link">
            <div class="post-link__content" style="width:100%;">
                <h3 class="post-link__title dis_question_{{$discussion->id}}" style="overflow-wrap: break-word;">
                    @php
                        $showChar = 30;
                        $ellipsestext = "...";
                        $moretext = "see more";
                        $lesstext = "see less";

                        $question = $discussion->question;

                        if(strlen($question) > $showChar) {

                            $c = substr($question, 0, $showChar);
                            $h = substr($question, $showChar, strlen($question) - $showChar);

                            $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink" style="color: #4080ff; font-size: 23px;">' . $moretext . '</a></span>';

                            $question = $html;
                        }
                    @endphp
                    {!!$question!!}
                </h3>
                <div class="post-link__text dis_description_{{$discussion->id}}" style="overflow-wrap: break-word;">
                    @php
                        $showChar = 100;
                        $ellipsestext = "...";
                        $moretext = "see more";
                        $lesstext = "see less";

                        $description = $discussion->description;

                        if(strlen($description) > $showChar) {

                            $c = substr($description, 0, $showChar);
                            $h = substr($description, $showChar, strlen($description) - $showChar);

                            $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                            $description = $html;
                        }
                    @endphp
                    {!!$description!!}
                </div>
            </div>
        </div>
    </div>

    <div class="post__footer">
        <button class="post__like-btn discussion_like_button" style="outline:none;" type="button" id="{{$discussion->id}}">
            <svg class="icon icon--like">
            <use xlink:href="{{asset('assets3/img/sprite.svg#like')}}"></use>
            </svg>
            <span id="discussion_like_count_{{$discussion->id}}"><strong>{{count($discussion->likes)}}</strong> Likes</span>
        </button>

        <button class="post__comment-btn" style="outline:none;" type="button" data-tab="share__commenttext_{{$discussion->id}}">
            <svg class="icon icon--comment">
            <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
            </svg>
            <span><strong>{{count($discussion->replies)}}</strong> Reply</span>
        </button>

        <button class="post__like-btn discussion_share_button" type="button" id="{{$discussion->id}}">
            <svg class="icon icon--like" style="color: #4080ff">
            <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
            </svg>
            <span id="discussion_share_count_{{$discussion->id}}"><strong>{{count($discussion->shares)}}</strong> Shares</span>
        </button>
    </div>

    

    <div class="post__tips">
    @if(@$latest_tip = $discussion->replies()->orderBy('created_at', 'desc')->first())
        @if($discussion->type==1)
            <h3 class="post__tips-title">@lang('discussion.strings.top_recommendations_by_users')</h3>
        @elseif($discussion->type==2)
            <h3 class="post__tips-title">@lang('discussion.strings.top_tips_by_users')</h3>
        @elseif($discussion->type==3)
            <h3 class="post__tips-title">@lang('discussion.strings.top_planning_tips_by_users')</h3>
        @elseif($discussion->type==4)
            <h3 class="post__tips-title">@lang('discussion.strings.top_answers_by_users')</h3>
        @endif
        <div class="post__tip">
            <div class="post__tip-main">
                <img class="post__tip-avatar" src="{{ check_profile_picture($latest_tip->author->profile_picture) }}" alt="" role="presentation">
                <div class="post__tip-content">
                    <div class="post__tip-header">
                        <a href="#">{{ $latest_tip->author->name }}</a> said<span class="post__tip-posted">{{ diffForHumans($latest_tip->created_at) }}</span>
                    </div>
                    <div class="post__tip-text">{{ $latest_tip->reply }}</div>
                </div>
            </div>
            <div class="post__tip-footer">
                <button class="post__tip-upvotes" type="button" style="outline:none;" reply-id="{{ $latest_tip->id }}">
                    <svg class="icon icon--angle-up-solid">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#angle-up-solid')}}"></use>
                    </svg>
                    <span><strong>{{ @count($latest_tip->upvotes) }}</strong>  Upvotes</span>
                </button>
                @if( @count($discussion->replies) > 1 )
                <button class="post__tip-more" type="button" style="outline:none;" onclick="discuss_loadmore(this)">
                    <div class="user-list">
                        <div class="user-list__item">
                            @foreach( $discussion->replies as $rep )
                            @if( $loop->index == 3 ) @break @endif
                            <div class="user-list__user"><img class="user-list__avatar" src="{{ check_profile_picture($rep->author->profile_picture) }}" alt="" role="presentation"></div>
                            @endforeach
                        </div>
                    </div>
                    <span><strong>{{ @(count($discussion->replies) - 1) }} </strong> more tips</span>
                </button>
                @endif
            </div>
        </div>
        
    @endif
        
    </div>
    {{--@if( @count($discussion->replies) > 1 ) --}}
    <div style="padding-left: 30px; padding-bottom: 10px; display: none">
        <button class="post__tip-loadmore" type="button" reply-id="{{ $discussion->id }}" page_num="1">
            <svg class="icon icon--angle-up-solid">
                <use xlink:href="{{asset('assets3/img/sprite.svg#add')}}" style="color: rgb(64, 128, 255)"></use>
            </svg>
            <span>load more</span>
        </button>
    </div>
    {{--@endif--}}

    <div class="comments" style="display:none;" data-content="share__commenttext_{{$discussion->id}}">
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-input">
                <form class="dicussioncomment" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data" data-id="{{$discussion->id}}">
                    <div class="post-block post-create-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" id="discussioncommenttext" class="textarea-customize"
                                style="display:inline;vertical-align: top;height:40px;" placeholder="@lang('comment.write_a_reply')"></textarea>
                            <div class="medias">
                                <!-- <div class="plus-icon" style="display: none;">
                                    <div>
                                        <span class="close" onclick="javascript:$('#commentfile').click();"><span style="font-size:110px;">+</span></span>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <!-- <li class="post-options">
                                        <input type="file" name="file[]" id="commentfile" style="display:none" multiple>
                                        <i class="trav-camera click-target" data-target="commentfile"></i>
                                    </li> -->
                                </ul>
                            </div>

                            <button type="submit" class="btn btn--sm btn--main">@lang('buttons.general.send')</button>
                            
                        </div>
                        
                    </div>
                </form>

                </div>
            </div>
            
        @endif
    </div>
</div>
