<div class="user-card" filter-tab="#mate">
    <div class="user-card__main"><img class="user-card__avatar" src="{{check_profile_picture($travelmate_request->author->profile_picture)}}" alt="" role="presentation" />
        <div class="user-card__content">
            <div class="user-card__header"><a class="user-card__name" href="#">{{$travelmate_request->author->name}}</a>, {{$travelmate_request->author->display_name}}</div>
            <div class="user-card__text">Photography gear, travel, and inspiration. - You In?</div>
            <div class="user-card__meta"><a class="user-card__meta-item" href="#">#MomentGear</a><span class="user-card__meta-item"> hello@shopmoment.com</span></div><a class="user-card__link" href="#">View 86 contributions</a>
        </div>
    </div>
    <div class="user-card__btn-wrap"><button class="btn btn--md btn--main" type="button"><svg class="icon icon--add-user">
            <use xlink:href="img/sprite.svg#add-user"></use>
            </svg>Follow</button></div>
</div>