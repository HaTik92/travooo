<div class="picture-cards">
    <div class="picture-cards__inner">
        <div class="picture-cards__items">
            <div class="picture-cards__item">
                <div class="picture-card">
                    <span class="picture-card__header">
                        <h2 class="picture-card__title">Trending Media</h2>
                        <span class="picture-card__number">{{count($country->getMedias)}}</span>
                    </span>
                    <img class="picture-card__img mediaModalTrigger" style="width:200px;height:134px;cursor:pointer;" src="@if(isset($country->getMedias[0]->url)) https://s3.amazonaws.com/travooo-images2/th1100/{{$country->getMedias[0]->url}} @else {{asset('assets2/image/placeholders/pattern.png')}} @endif" alt="" role="presentation" />
                    <!--<span class="picture-card__icon js-show-modal" href="#modal-media">-->
                    <span class="picture-card__icon mediaModalTrigger" style="cursor:pointer">
                        <svg class="icon icon--multiple-items"><use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items')}}"></use>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="picture-cards__item">
                <div class="picture-card">
                    <span class="picture-card__header">
                        <h2 class="picture-card__title">Trip Plans</h2>
                        <span class="picture-card__number">@if(is_object($country->versions) && count($country->versions)) {{ count($country->versions) }} @endif</span>
                    </span>
                    @if(is_object($country->versions) && count($country->versions))
                    @foreach($country->versions()->take(3)->get() AS $pv)
                    @if ($loop->first)
                    <img class="picture-card__img js-show-modal" href="#modal-trips" style="cursor:pointer;" src="{{@get_plan_map($pv->trip, 185, 115, @$pv->trip->active_version)}}" alt="" role="presentation" />
                    <span class="picture-card__icon js-show-modal" href="#modal-trips" style="cursor:pointer"><svg class="icon icon--multiple-items"><use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items')}}"></use></svg>
                    </span>
                    @endif
                    @endforeach
                    @else
                    @lang('place.no_plans')
                    @endif

                </div>
            </div>
            <div class="picture-cards__item">
                <div class="picture-card">
                    <span class="picture-card__header" style="margin-bottom:0"><h2 class="picture-card__title">Travel Mates</h2>
                        <span class="picture-card__number">{{count($travelmates)}}</span>
                    </span>
                    <div class="" style="padding-left:3px;">
                    @foreach($travelmates AS $tm)
                    @if($loop->index<6)
                        <div style="width:62px;height:62px;object-fit:cover;overflow:hidden;cursor:grab;background-image:url('{{check_profile_picture($tm->author->profile_picture)}}');background-position:center center;background-size: 87px 87px;border:1px solid white;float:left;" alt="" role="presentation"></div>
                    @endif
                    @endforeach
                    </div>
                </div>
            </div>
            <div class="picture-cards__item">
                <div class="picture-card">
                    <span class="picture-card__header"><h2 class="picture-card__title">Nearby Events</h2>
                    <span class="picture-card__number">@if(isset($events)) {{count($events)}} @endif</span>
                                        </span>
            @if(isset($events))
            @foreach ($events AS $event)
            <?php
            $rand = rand(1, 10);
            ?>
            @if($loop->first)
            <img class="picture-card__img nearbyEventTrigger" src="{{asset('assets2/image/events/'.$event->category.'/'.$rand.'.jpg')}}" style="cursor:pointer;width:200px;height:135px;" role="presentation"/>
            <span class="picture-card__icon" style="cursor:pointer"><svg class="icon icon--multiple-items"><use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items')}}"></use></svg>
            </span>
            @endif
            @endforeach
            @endif 
            
            <span class="picture-card__icon" style="cursor:pointer"><svg class="icon icon--multiple-items"><use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items')}}"></use></svg>
            </span>
        </div>
    </div>
    <div class="picture-cards__item">
        <div class="picture-card">
            <span class="picture-card__header">
                <h2 class="picture-card__title">Reports</h2>
                <span class="picture-card__number">{{count($reports)}}</span>
            </span>
            @foreach($reports as $report)
            @if(isset($report->cover[0]) && is_object($report->cover[0]) && $report->cover[0]->val!='')
            <img class="picture-card__img reportsTrigger" data-id="0" src="{{@$report->cover[0]->val}}" alt="" style="cursor:pointer;width:200px;height:135px;" role="presentation" />
            @break
            @endif
            @endforeach
            <span class="picture-card__icon" style="cursor:pointer"><svg class="icon icon--multiple-items"><use xlink:href="{{asset('assets3/img/sprite.svg#multiple-items')}}"></use></svg>
            </span>
        </div>
    </div>
</div>