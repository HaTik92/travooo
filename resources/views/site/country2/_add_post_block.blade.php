<style>
.add-post__media
{
    width: 100%;
}
.img-wrap-newsfeed
{
    width: 151px;
    height: 130px;
    float: left;
    margin: 10px;
}
.img-wrap-newsfeed>div
{

    overflow: hidden;
    justify-content: center;
    display: flex;
}

.img-wrap-newsfeed .close
{
    margin-top: -133px;
    margin-left: 135px;
    position:absolute;
    margin-right: 5px;
    opacity: 1;
}
.thumb 
{
    object-fit: cover;
    height: 130px;
}
</style>


<div class="add-post" id="add-post">
    <h2 class="add-post__title">Add a post to <strong>{{$place->trans[0]->title}}</strong></h2>
    <div class="add-post__content">
        <form class="add-post__form" id="_place_add_post_form">
            <img class="add-post__avatar" src="{{check_profile_picture($me->profile_picture)}}" alt="" role="presentation" />
            <textarea class="add-post__textarea" name="text" placeholder="Speak your mind, {{@explode(" ", $me->name)[0]}}."></textarea>
            <div class="add-post__media">

            </div>
            <div class="add-post__form-footer">
                <button class="add-post__emoji" type="button">🙂</button>
                <a class="add-post__check-in" href="#">
                    <svg class="icon icon--location">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#location')}}"></use>
                    </svg>
                Check in</a>
            </div>
            <input type="file" name="file" id="_add_post_imagefile" style="display:none" multiple>
            <input type="hidden" name="type" value="" />
        </form>
        <div class="add-post__footer">
            <div class="add-post__footer-left">
                <button class="add-post__post-type add-post__post-type--active" type="button" data-type="text">T</button>
                <button class="add-post__post-type" type="button" data-type="image">
                    <svg class="icon icon--camera">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#camera')}}"></use>
                    </svg>
                </button>
                <button class="add-post__post-type" type="button" data-type="video">
                    <svg class="icon icon--play">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#play')}}"></use>
                    </svg>
                </button>
                <button class="add-post__post-type" type="button" data-type="info">
                    <svg class="icon icon--question-mark">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#question-mark')}}"></use>
                    </svg>
                </button>
                <button class="add-post__post-type" type="button" data-type="map">
                    <svg class="icon icon--map">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#map')}}"></use>
                    </svg>
                </button>
            </div>
            <div class="add-post__footer-right">
                <div class="dropdown">
                    <button class="btn btn--sm btn--outline dropdown__toggle">
                        <svg class="icon icon--earth">
                            <use xlink:href="{{asset('assets3/img/sprite.svg#earth')}}"></use>
                        </svg>
                    PUBLIC</button>
                </div>
                <button class="btn btn--sm btn--main" onclick="_place_send_post()">Post</button>
            </div>
        </div>
    </div>
    <div class="add-post__resize">
        <svg class="icon icon--resize">
            <use xlink:href="{{asset('assets3/img/sprite.svg#resize')}}"></use>
        </svg>
    </div>
</div>
