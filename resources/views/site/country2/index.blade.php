<!DOCTYPE html>
<html class="page" lang="en">
    <head>
        <title>{{@$country->trans[0]->title}} on Travooo</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">
        <meta name="format-detection" content="date=no">
        <meta name="format-detection" content="address=no">
        <meta name="format-detection" content="email=no">
        <meta content="notranslate" name="google">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
        <link rel="stylesheet" href="{{asset('assets2/css/style-15102019-3.css')}}">
        <link rel="stylesheet" href="{{asset('assets2/css/travooo.css?0.0.2')}}">
        <link rel="stylesheet" href="{{asset('assets3/css/style-13102019.css')}}">
        <link rel="stylesheet" href="{{asset('assets3/css/datepicker.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets3/css/slider-pro.min.css')}}" media="screen"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <style>
            .lg-outer #lg-share {
                display:none !important;
                }
                .lg-outer .lg-thumb-outer {
                    width: auto !important;
                }
        </style>
        <link rel="stylesheet" href="{{asset('assets3/css/star.css')}}">
        <style>
            .modal__close {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                position: absolute;
                top: 20px;
                right: 20px;
                width: 26px;
                height: 26px;
                background-color: rgba(255, 255, 255, .5);
                color: rgba(0, 0, 0, .5);
                border-radius: 50%;
            }
	    .textarea-customize {
                border: 1px solid #dcdcdc;
                border-radius: 3px;
                resize: none;
            }
            .comment {
                margin-bottom: 10px;
            }
            .comment-info{
                display: flex;
                align-items: center;
            }
            .comment-info i{
                margin-right: 9px;
                color: #4080ff;
                font-size: 18px;
            }
            
            .carousel-cards__control--prev {
                display:flex !important;
            }

            .icon--star {
                color:  #ccc;
            }

            .icon--star-active {
                color: #4080ff!important;
            }
            .user-card.post {
                position: relative;
                padding-bottom: 50px;
            }
            .user-card.post .dropdown {
                position: absolute;
                top: 8px;
                right: 12px;
            }
            .user-card.post .updownvote {
                position: absolute;
                bottom: 15px;
            }
            body {
                pointer-events:none;
            }
        </style>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    </head>

    <body class="page__body">
        @include('site.layouts._header')
        <div class="page-content page__content">
            @include('site.country2._hero_block')
            @include('site.country2._picture_card_block')

        </div>
    </div>
    <div class="page-content__inner">
        <div class="page-content__row">
            <div class="page-content__main">
                @include('site.country2.partials._add_post_block')
                <div class="posts">
                    
                    <div class="posts-filter">
                        <a class="posts-filter__link" href="#all">All</a>
                        <a class="posts-filter__link" href="#discussion" >Discussions</a>
                        <a class="posts-filter__link" href="#top" >Top</a>
                        <a class="posts-filter__link" href="#media" >Media</a>
                        <a class="posts-filter__link" href="#tripplan" >Trip Plans</a>
                        <a class="posts-filter__link" href="#mate" >Mates</a>
                        <a class="posts-filter__link" href="#event" >Events</a>
                        <a class="posts-filter__link" href="#report" >Reports</a>
                    </div>
                    
                    
                    <script>
                        var map_var = "";
                        var map_func = "";
                    </script>
                    @foreach($pposts AS $ppost)
                        @if($ppost['type']=='regular')
                            <?php
                            $post = App\Models\Posts\Posts::find($ppost['id']);
                            ?>
                            @if($post->text!='' && count($post->medias)>0)
                                @include('site.country2.partials.post_media_with_text')
                            @elseif($post->text=='' && count($post->medias)>0)
                                @include('site.country2.partials.post_media_without_text')
                            @else
                                @include('site.country2.partials.post_text')
                            @endif
                            
                        @elseif($ppost['type']=='discussion')

                        <?php
                        $discussion = App\Models\Discussion\Discussion::find($ppost['id']);
                        ?>
                        @include('site.country2.partials.post_discussion')
                        
                        
                        
                        
                        @elseif($ppost['type']=='plan')
                        <?php
                        $plan = \App\Models\TripPlans\TripPlans::find($ppost['id']);
                        ?>
                        @include('site.country2.partials.post_plan')
                        
                        
                        
                        
                        @elseif($ppost['type']=='report')
                        
                        <?php
                        $report = App\Models\Reports\Reports::find($ppost['id']);
                        if($report){
                        ?>
                        @include('site.country2.partials.post_report')
                        <?php }?>
                        
                        @elseif($ppost['type']=='travelmate')
                        
                        <?php
                        $travelmate_request = \App\Models\TravelMates\TravelMatesRequests::find($ppost['id']);
                        ?>
                        
                        @include('site.country2.partials.post_travelmate')


                        @elseif($ppost['type']=='checkin')
                        
                        <?php
                        $checkin = \App\Models\Posts\Checkins::find($ppost['id']);
                        ?>
                        
                        @include('site.country2.partials.post_checkin')
                        
                        @endif
                    @endforeach
                    
                    
                    
                    
                </div>
                <div id='pagination' style='text-align:center;'>
                    <input type="hidden" id="pageno" value="1">
                    <div id="feedloader" class="post-animation-content post">
                        <div class="post__header">
                                <div class="post-top-avatar-wrap animation post-animation-avatar"></div>
                                <div class="post-animation-info-txt">
                                    <div class="post-top-name animation post-animation-top-name"></div>
                                    <div class="post-info animation post-animation-top-info"></div>
                                </div>
                        </div>
                        <div class="post__content">
                            <div class="post-animation-text animation pw-2"></div>
                            <div class="post-animation-text animation pw-3"></div>
                        <div class="post-animation-footer">
                            <div class="post-animation-text animation pw-4"></div>
                        </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="page-content__side">
                <div class="card">
                    <div class="card__header">
                        <h2 class="card__title">Need Assistance?</h2><a class="card__link card__link--lg" href="{{url_with_locale('discussions')}}">Ask Experts</a>
                    </div>
                    <div class="assistance card__content" style="max-height: 540px;overflow: scroll;">
                    @if(count($country->experts)>0)
                         @foreach($country->experts AS $key=>$pcex)
                        <div class="assistance__item">
                            <div class="assistance__avatar-wrap"><img class="assistance__avatar" src="{{check_profile_picture($pcex->user->profile_picture)}}" alt="{{$pcex->user->name}}" title="{{$pcex->user->name}}" role="presentation" />
                                <div class="assistance__icon"><svg class="icon icon--fire">
                                    <use xlink:href="{{asset('assets3/img/sprite.svg#fire')}}"></use>
                                    </svg></div>
                            </div>
                            <div class="assistance__content">
                                <div class="assistance__username">{{$pcex->user->name}}</div><a class="assistance__link" href="#" onclick="update_newsfeed('top', {{$pcex->user->id}})">View {{@count($pcex->user->contributions)}} contributions</a>
                            </div>
                            <div class="assistance__views">
                                <div class="assistance__views-number">{{@count($pcex->user->postviews)}}</div>
                                <div class="assistance__views-label">Views</div>
                            </div>
                        </div>
                         @endforeach
                    @else
                    <div style="font-weight: 100;display: block;line-height: 1.3em;">
                        Looks like there is no Experts in this City/Country. <br />
                        Why don't you become an Expert by participating in the <a href="{{url_with_locale('discussions')}}">Discussion Forum</a> now?..
                    </div>
                    @endif
                        
                    </div>
                </div>
                <div class="card">
                    <div class="card__header">
                        <h2 class="card__title">Travel Mates</h2><a class="card__link" href="{{url_with_locale('travelmates')}}">See All</a>
                    </div>
                    <div class="card__content">
                        <div class="user-list">
                            <div class="user-list__item">
                                @foreach($travelmates AS $mate)
                                <a class="user-list__user" href="{{url_with_locale('profile/'.$mate->author->id)}}">
                                    <img class="user-list__avatar" src="{{check_profile_picture($mate->author->profile_picture)}}" alt="{{$mate->author->name}}" role="presentation" />
                                </a>
                                @endforeach
                                @if(count($travelmates)>8)
                                <a class="user-list__more" href="#">+{{count($travelmates)-8}}</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <ul class="nav nav-tabs" style="width: 100%;padding: 20px;">
                        <li class="active" style="width: 50%;text-align: center;">
                            <a data-toggle="tab" href="#flights" class="card__title">Flights</a>
                        </li>
                        <li style="width: 50%;text-align: center;">
                            <a data-toggle="tab" href="#hotels" class="card__title">Hotels</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="flights" class="tab-pane fade in active show" style="padding:20px;">
                            <div class="user-list">
                                <div data-skyscanner-widget="SearchWidget" 
                                     data-origin-geo-lookup="true" 
                                     data-currency="USD" 
                                     data-destination-phrase="'{{$country->transsingle->title}}'"
                                     data-target="_blank"
                                     data-button-colour="#db0000"
                                     data-flight-outbound-date="{{date("Y-n-d", strtotime('+1 day', time()))}}"
                                     data-flight-inbound-date="{{date("Y-n-d", strtotime('+8 day', time()))}}"></div>
                                <script src="https://widgets.skyscanner.net/widget-server/js/loader.js" async></script>
                            </div>
                        </div>
                        <div id="hotels" class="tab-pane fade" style="padding:20px;">
                            <div class="user-list">
                                
                                <div data-skyscanner-widget="HotelSearchWidget"
                                     data-currency="USD"
                                     data-destination-phrase="'{{$country->transsingle->title}}'"
                                     data-target="_blank"
                                     data-button-colour="#db0000"
                                     data-hotel-check-in-date="{{date("Y-n-d", strtotime('+1 day', time()))}}"
                                     data-hotel-check-out-date="{{date("Y-n-d", strtotime('+8 day', time()))}}"
                                     data-responsive="true"
                                     ></div>
                                <script src="https://widgets.skyscanner.net/widget-server/js/loader.js" async></script>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="side-nav">
                    <a class="side-nav__link" href="{{route('page.privacy_policy')}}">Privacy</a>
                    <a class="side-nav__link" href="{{route('page.terms_of_service')}}">Terms</a>
                    <a class="side-nav__link" href="{{url('/')}}">Advertising</a>
                    <a class="side-nav__link" href="{{url('/')}}">Cookies</a>
                    <a class="side-nav__link" href="{{url('/')}}">More</a>
                </div>
                <div class="page-content__copyright">Travooo © 2017</div>
                <button class="page-content__add-post-btn" type="button" style="position: fixed;bottom: 40px;right: 40px;">
                    <svg class="icon icon--add">
                    <use xlink:href="{{asset('assets3/img/sprite.svg#add')}}"></use>
                    </svg>Add a Post
                </button>
            </div>
        </div>
    </div>
</div>


<footer class="page-footer page__footer"></footer>

<script src="{{asset('assets3/js/tether.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="{{asset('assets3/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="{{asset('assets3/js/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
<script src="{{asset('assets3/js/jquery.barrating.min.js')}}"></script>
<script src="{{asset('assets3/js/star.js')}}"></script>

<script src="{{ asset('assets2/js/emoji/jquery.emojiFace.js?v=0.1') }}"></script>
<script src="{{ asset('assets3/js/jquery.sliderPro.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
@include('site.home.partials._spam_dialog')
</body>

@include('site.country2._modals')
<script>
    eval(map_var);
    var today_map, today_lat = {{ $country->lat }}, today_lng = {{ $country->lng }};
    function setCenter_usermap(map, lat, lng)
    {
        map.setZoom(16);
        map.setCenter(new google.maps.LatLng(parseFloat(lat), parseFloat(lng)));
    }
    function initMap() {
        eval(map_func);

        today_map = new google.maps.Map(document.getElementById('today_map'), {
                zoom: 4,
                center: new google.maps.LatLng(today_lat, today_lng),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            
    }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&callback=initMap&language=en">
</script>

<script>
    $(document).ready(function () {
          $('body').css('pointer-events', 'all') //activate all pointer-events on body
    $(document).on('click', '.morelink', function(){
       console.log('$(this)', $(this))
        var moretext = "see more";
        var lesstext = "see less";
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
       // return false;
    });
        $(".about-modal__link").click(function() {
            if( $(this).hasClass('about-modal__link--active') )
                return;

            $(".about-modal__link").removeClass('about-modal__link--active');
            $(this).addClass('about-modal__link--active');

            var attr = $(this).attr('id').split('-');
            $(".about-modal__content").find('[id^=box-]').hide();
            $(".about-modal__content").find( '#box-' + attr[1] ).fadeIn();
        });
        
        $(".posts-filter__link").click(function(e) {
            if( $(this).hasClass('posts-filter__link--active') )
                return;

            $(".posts-filter__link").removeClass('posts-filter__link--active');
            $(this).addClass('posts-filter__link--active');

            if( $(this).attr('href') == '#all' )
            {
                $('.posts').find('[filter-tab]').fadeIn();
            }
            else
            {
                $('.posts').find('[filter-tab]').fadeOut();
                $('.posts').find('[filter-tab="' + $(this).attr('href') + '"]').fadeIn();
            }
            
            e.preventDefault();
        });
        
        $("header").css({"width": "100%", "position": "fixed"});

        $('#feedloader').on('inview', function(event, isInView) {
            if (isInView) {
                var nextPage = parseInt($('#pageno').val())+1;
                $.ajax({
                    type: 'POST',
                    url: '{{route("place.update_post", $country->id )}}',
                    data: { pageno: nextPage },
                    success: function(data){
                        if(data != ''){							 
                            $('.posts').append(data);
                            $('#pageno').val(nextPage);
                            initMap();
                        } else {								 
                            $("#pagination").hide();
                        }
                    }
                });
            }
        });
    });
</script>
@include('site.country2._scripts')
@include('site.country2.partials._add_post_script')
@include('site.layouts._footer-scripts')
<script>
    $("*").not("script").not("style").not("link").attr("dir", "auto");
    $("*").not("script").not("style").not("link").each(function(){
        if(/[\u0600-\u06FF]/.test($(this).text()) && !/</.test($(this).html()))
            $(this).attr("dir", "");
    });
    $(".ql-direction-rtl").attr("dir", "rtl");
    $('body').on('DOMNodeInserted', function(e) {
        $(e.target).find("*").attr('dir', "auto");
        $(e.target).find("*").not("script").not("style").not("link").each(function(){
            if(/[\u0600-\u06FF]/.test($(this).text()) && !/</.test($(this).html()))
                $(this).attr("dir", "");
        });
    });
</script>
</html>