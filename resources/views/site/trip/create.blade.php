@php
    $title = 'Travooo - Trip Plan';
@endphp

@extends('site.trip.template.trip')

@section('before_site_style')
    @parent
    <link rel="stylesheet" href="{{asset('assets2/js/timepicker/jquery.timepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.css')}}">
@endsection

@section('after_styles')
    @if(isset($memory))    
    <link type="text/css" rel="stylesheet" href="{{asset('assets2/js/plupload-2.3.6/js/jquery.ui.plupload/css/jquery.ui.plupload.css')}}" media="screen" />

    @endif
    <style>
        .editCityIcon {
            display: none;
        }

        .trip-place-name:hover .editCityIcon {
            display: block;
        }

        .editPlanIcon {
            display: none;
        }

        .editPlanTitle:hover .editPlanIcon {
            display: block;
        }

        .editPlaceIcon {
            display: none;
        }

        .trip-side-row:hover .editPlaceIcon {
            display: block;
        }
    </style>
@endsection

@section('content')
  
@include('site.trip.partials._trip-plan-header')
    <div class="create-trip-wrapper">
        <div class="trip-side">
            <div class="trip-side-content">
                @php $trip_arr = [];$trip_cities_arr=[]; $existing_place_ids = []; $existing_city_ids = []; @endphp
                <!-- FIRST SHOW COMPLETED CITIES, AND PLACES INSIDE THEM -->
                @if(isset($trips_cities_complete))
                    @foreach($trips_cities_complete AS $cities_complete)
                    @php if(!array_key_exists($cities_complete->city->id, $trip_cities_arr)){$trip_cities_arr[$cities_complete->city->id] = ['lat'=>$cities_complete->city->lat, 'lng'=>$cities_complete->city->lng];}
                         
                         if(!in_array($cities_complete->city->id, $existing_city_ids)){$existing_city_ids[] = $cities_complete->city->id;}
                    @endphp
                        <div class="trip-side-block
                             @if($cities_complete->transportation=="PLANE")
                                airplane
                            @elseif($cities_complete->transportation=="DRIVING")
                                                            overland-trip
                            @elseif($cities_complete->transportation=="railway")
                                                            trav-railway-icon
                            @elseif($cities_complete->transportation=="bus")
                        @elseif($cities_complete->transportation=="bicycle")
                        @elseif($cities_complete->transportation=="walk")
                        @elseif($cities_complete->transportation=="ship")
                        @endif
                                ">
                            <div class="trip-country-name">{{$cities_complete->city->country->transsingle->title}}</div>
                            <div class="trip-place-name">
                                <span class="name">
                                    {{$cities_complete->city->transsingle->title}}

                                </span>
                                &nbsp;
                                <a href="#" class="drag-icon editCityIcon" data-id="{{$cities_complete->id}}"
                                   style="text-align: right;top:2px;right:2px;width:16px;height:16px;">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </div>
                            <div class="trip-side-layer-wrap">
                                <?php
                                $trip_places_inside_city = \App\Models\TripPlaces\TripPlaces::where('trips_id', $cities_complete->trips_id)
                                    ->where('cities_id', $cities_complete->cities_id)
                                    ->where('versions_id', $my_version_id)
                                    ->get();
                                ?>
                                 @if( count($trip_places_inside_city) == 0 && ((isset($trips_cities_incomplete) && count($trips_cities_incomplete) > 0) || count($trips_cities_complete) > 1))
                                            @php 
                                            $trip_arr['city-'.$cities_complete->city->id] = ['lat'=>$cities_complete->city->lat, 'lng'=>$cities_complete->city->lng]; 
                                            @endphp
                                @endif
                                @foreach($trip_places_inside_city AS $trip_place_inside_city)
                                     <!-- Add places in map -->
                                     @php  if(!in_array($trip_place_inside_city->place_id, $existing_place_ids)){$existing_place_ids[] = $trip_place_inside_city->place_id;} @endphp
                                    @if(count($trip_places_inside_city) ==1)
                                         @if((isset($trips_cities_incomplete) && count($trips_cities_incomplete) > 0) ||  count($trips_cities_complete) > 1)
                                            @php $trip_arr[$cities_complete->city->id.'-place-'.$trip_place_inside_city->places_id] = ['lat'=>$trip_place_inside_city->place->lat, 'lng'=>$trip_place_inside_city->place->lng] @endphp
                                         @endif
                                    @else
                                     @php $trip_arr[$cities_complete->city->id.'-place-'.$trip_place_inside_city->places_id] = ['lat'=>$trip_place_inside_city->place->lat, 'lng'=>$trip_place_inside_city->place->lng] @endphp
                                    @endif
                                    
                                    @if($trip_place_inside_city->active)
                                    
                                        <div class="trip-side-layer" id="TripPlace{{$trip_place_inside_city->id}}">
                                            <div class="trip-side-row">

                                                <a href="#" class="drag-icons editPlaceIcon"
                                                   data-id="{{$trip_place_inside_city->id}}"
                                                   style="position: absolute; text-align: right;top:2px;right:2px;width:16px;height:16px;">
                                                    <i class="fa fa-edit"></i>
                                                </a>

                                                <div class="img-wrap">
                                                    <img src="{{check_place_photo($trip_place_inside_city->place)}}"
                                                         alt="photo" style="width:64px;height:60px;">
                                                </div>
                                                <div class="trip-content">
                                                    <div class="trip-txt-line">
                                                        <div class="line-txt place-info">
                                                            <p class="dest-name"
                                                               style="max-width: 200px;">{{@$trip_place_inside_city->place->transsingle->title}}</p>
                                                        </div>
                                                        <div class="line-time">
                                                            {{weatherDate($trip_place_inside_city->time)}}&nbsp;
                                                            @ {{weatherTime($trip_place_inside_city->time)}}
                                                        </div>
                                                    </div>
                                                    <div class="trip-txt-line">
                                                        <div class="line-txt">
                                                            @lang('trip.will_spend')
                                                            <b>${{$trip_place_inside_city->budget}}</b>
                                                            <span class="dot">·</span>
                                                            @lang('trip.planning_to_stay')
                                                            <b>{{$trip_place_inside_city->duration}}
                                                                @lang('time.min')</b>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="adding-place-block">
                                            <form method='post' id='activatePlace' autocomplete="off">
                                                <div class="add-form">
                                                    <div class="form-row">
                                                        <div class="field-style flex-field">
                                                            <div class="img-wrap">
                                                                <img src="{{check_place_photo($trip_place_inside_city->place)}}"
                                                                     alt="photo" style="width:54px;height:54px;">
                                                            </div>
                                                            <div class="field-txt">
                                                                {{@$trip_place_inside_city->place->transsingle->title}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="field-style">
                                                            <div class="field-label">
                                                                @if(isset($memory))
                                                                    When did you check-in?
                                                                @else
                                                                    @lang('trip.when_you_will_check_in')
                                                                @endif
                                                            </div>
                                                            <div class="flex-field">
                                                                <div class="flex-item">
                                                                    <div class="field-ttl">@lang('time.date')</div>
                                                                    <div class="date-inner">
                                                                        <input type='text' name='date'
                                                                               class='datepicker_place' required
                                                                               value="{{$trip_place_inside_city->date!='0000-00-00' ? date('d F Y', strtotime($trip_place_inside_city->date)) : ''}}"/>
                                                                        <input type='hidden' name='actual_place_date'
                                                                               id='actual_place_date'
                                                                               value="{{$trip_place_inside_city->date ? $trip_place_inside_city->date : ''}}"/>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-item">
                                                                    <div class="field-ttl">@lang('time.time')</div>
                                                                    <div class="date-inner">
                                                                        <input type='text' name='time'
                                                                               class='timepicker'
                                                                               value="{{$trip_place_inside_city->time!='' ? date('g:ia', strtotime($trip_place_inside_city->time)) : ''}}"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="field-style">
                                                            <div class="field-label">
                                                                @if(isset($memory))
                                                                    You stayed
                                                                @else
                                                                    @lang('trip.planning_to_stay')
                                                                @endif
                                                            </div>
                                                            <div class="flex-field">
                                                                <div class="flex-item">
                                                                    <div class="field-ttl">@lang('time.hours')</div>
                                                                    <div class="time-count">
                                                                        <span class="click"
                                                                              onclick="doMinus('duration_hours')">-</span>
                                                                        <input type="text" name="duration_hours"
                                                                               id="duration_hours"
                                                                               value="{{@durationInHours($trip_place_inside_city->duration)}}">
                                                                        <span class="click"
                                                                              onclick="doPlus('duration_hours')">+</span>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-item">
                                                                    <div class="field-ttl">@lang('time.minutes')</div>
                                                                    <div class="time-count">
                                                                        <span class="click"
                                                                              onclick="doMinus('duration_minutes')">-</span>
                                                                        <input type="text" name="duration_minutes"
                                                                               id="duration_minutes"
                                                                               value="{{@durationInMinutes($trip_place_inside_city->duration)}}">
                                                                        <span class="click"
                                                                              onclick="doPlus('duration_minutes')">+</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="field-style">
                                                            <div class="field-label">
                                                                @if(isset($memory))
                                                                    How much did you spend?
                                                                @else
                                                                    @lang('trip.how_much_you_are_expecting_to_spend')
                                                                @endif
                                                            </div>
                                                            <div class="flex-field">
                                                                <div class="flex-item amount-input">
                                                                    <input type="text" name="budget" id="budget_custom"
                                                                           placeholder="@lang('other.enter_amount')"
                                                                           value="{{@$trip_place_inside_city->budget}}">
                                                                </div>
                                                                <div class="flex-item radio-field">
                                                                    <input type="radio" name="budget" id="radio-1"
                                                                           value="50"
                                                                           @if(isset($trip_place_inside_city->budget) AND $trip_place_inside_city->budget==50) checked @endif>
                                                                    <label class="check-field" for="radio-1">
                                                                        50$
                                                                    </label>
                                                                </div>
                                                                <div class="flex-item radio-field">
                                                                    <input type="radio" name="budget" id="radio-2"
                                                                           value="100"
                                                                           @if(isset($trip_place_inside_city->budget) AND $trip_place_inside_city->budget==100) checked @endif>
                                                                    <label class="check-field" for="radio-2">
                                                                        100$
                                                                    </label>
                                                                </div>
                                                                <div class="flex-item radio-field">
                                                                    <input type="radio" name="budget" id="radio-3"
                                                                           value="200"
                                                                           @if(isset($trip_place_inside_city->budget) AND $trip_place_inside_city->budget==200) checked @endif>
                                                                    <label class="check-field" for="radio-3">
                                                                        200$
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="field-subtext">
                                                                @lang('trip.spending_so_far') <b class="spending_amount">${{isset($trip_place_inside_city->budget)? $trip_place_inside_city->budget:0}}</b>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if(isset($memory))
                                                    <div class="form-row">
                                                        <div class="field-style">
                                                            <div class="field-label">
                                                                Upload photos & videos
                                                            </div>
                                                            <div class="uploader">
                                                                
                                                            </div>
                                                            <div class="field-subtext">
                                                                @lang('trip.spending_so_far') <b>${{isset($trip_place_inside_city->budget)? $trip_place_inside_city->budget:0}}</b>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                                <div class="add-footer">
                                                    <input type='hidden' name='trip_place_id'
                                                           value='{{$trip_place_inside_city->id}}'/>
                                                    <input type='hidden' name='trip_id'
                                                           value='{{$trip_place_inside_city->trips_id}}'/>
                                                    <input type='hidden' name='version_id'
                                                           value='{{$trip_place_inside_city->trip->myversion()[0]->id}}'/>
                                                    <input type='hidden' name='city_id'
                                                           value='{{$trip_place_inside_city->cities_id}}'/>
                                                    <input type='hidden' name='place_id' id='place_id'
                                                           value='{{$trip_place_inside_city->places_id}}'/>
                                                    <input type='hidden' name='version_id' value='{{$my_version_id}}'/>
                                                    <button type="button" class="btn btn-transp btn-clear"
                                                            id="removePlace"
                                                            data-id="{{@$trip_place_inside_city->place->id}}">@lang('buttons.general.crud.delete')
                                                    </button>
                                                    <button type="submit" name='submit'
                                                            class="btn btn-light-primary btn-bordered">@lang('buttons.general.save')
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                @endif

            <!-- THEN SHOW INCOMPLETE CITIES, AND PLACES INSIDE THEM -->
                @if(isset($trips_cities_incomplete) && count($trips_cities_incomplete)>0)
                    @php $j=0; $city_location = ['lat'=>$trips_cities_incomplete[0]->city->lat,'lng'=>$trips_cities_incomplete[0]->city->lng];@endphp
                    @foreach($trips_cities_incomplete AS $cities_incomplete)
                        @php $i=0; if(isset($city) && $cities_incomplete->city->id == $city->id){
                        $city_location = ['lat'=>$cities_incomplete->city->lat,'lng'=>$cities_incomplete->city->lng];
                        }
                        if(!array_key_exists($cities_incomplete->city->id, $trip_cities_arr)){$trip_cities_arr[$cities_incomplete->city->id] = ['lat'=>$cities_incomplete->city->lat, 'lng'=>$cities_incomplete->city->lng];}
                        
                        if(!in_array($cities_incomplete->city->id, $existing_city_ids)){$existing_city_ids[] = $cities_incomplete->city->id;}
                        @endphp
                        <div class="trip-side-block add-place">
                            <div class="trip-side-layer-wrap">
                                <div class="adding-place-block arrow-left">
                                    <div class="add-form">
                                        <div class="form-row">
                                            <div class="field-style flex-field city-content" data-city_id="{{$cities_incomplete->city->id}}" style="cursor: pointer;">
                                                <div class="img-wrap">
                                                    <img src="{{(count($cities_incomplete->city->getMedias) > 0)?check_city_photo($cities_incomplete->city->getMedias[0]->url):check_city_photo('')}}"
                                                         alt="photo" style='width:54px;height:54px;'>
                                                </div>
                                                <div class="field-txt">
                                                    {{$cities_incomplete->city->transsingle->title}}
                                                </div>
                                            </div>
                                        </div>
                                        @php  $trip_city_places = get_trip_places_by_city_id($cities_incomplete->trips_id, $cities_incomplete->versions_id, $cities_incomplete->cities_id);@endphp
                                       
                                    @if($j == 0 && (count($trip_city_places) > 0 || count($trips_cities_incomplete) > 1))
                                         <div class="form-row">
                                             <div class="field-style">
                                                 <div class="field-label">
                                                     @if(isset($memory))
                                                     How did you arrive here?
                                                     @else
                                                     @lang('trip.how_you_will_arrive_here')
                                                     @endif
                                                 </div>
                                                 <div class="flex-field">
                                                     <div class="flex-item radio-field">
                                                         <input type="radio" name="transportation" id="arrive_radio-2" class="travel-type" data-mode="DRIVING"
                                                                @if($cities_incomplete->transportation=="DRIVING" OR $cities_incomplete->transportation=="") checked
                                                                @endif value='DRIVING'>
                                                                <label class="check-field" for="arrive_radio-2">
                                                             <i class="trav-car-icon"></i>
                                                         </label>
                                                     </div>
                                                     <div class="flex-item radio-field">
                                                         <input type="radio" name="transportation" id="arrive_radio-3" class="travel-type" data-mode="WALKING"
                                                                @if($cities_incomplete->transportation=="RAILWAY") checked
                                                                @endif value='WALKING'>
                                                                <label class="check-field" for="arrive_radio-3">
                                                             <i class="trav-railway-icon"></i>
                                                         </label>
                                                     </div>
                                                     <div class="flex-item radio-field">
                                                         <input type="radio" name="transportation" id="arrive_radio-4" class="travel-type" data-mode="TRANSIT"
                                                                @if($cities_incomplete->transportation=="TRANSIT") checked
                                                                @endif value='TRANSIT'>
                                                                <label class="check-field" for="arrive_radio-4">
                                                             <i class="trav-bus-icon"></i>
                                                         </label>
                                                     </div>
                                                     <div class="flex-item radio-field">
                                                         <input type="radio" name="transportation" id="arrive_radio-5" class="travel-type" data-mode="BICYCLING"
                                                                @if($cities_incomplete->transportation=="BICYCLING") checked
                                                                @endif value='BICYCLING'>
                                                                <label class="check-field" for="arrive_radio-5">
                                                             <i class="trav-cycle-icon"></i>
                                                         </label>
                                                     </div>
                                                     <div class="flex-item radio-field">
                                                         <input type="radio" name="transportation" id="arrive_radio-6" class="travel-type" data-mode="WALKING"
                                                                @if($cities_incomplete->transportation=="WALKING") checked
                                                                @endif value='WALKING'>
                                                                <label class="check-field" for="arrive_radio-6">
                                                             <i class="trav-walk-icon-2"></i>
                                                         </label>
                                                     </div>
                                                    <div class="flex-item radio-field">
                                                        <input type="radio" name="transportation" id="arrive_radio-1" class="travel-type" data-mode="PLANE"
                                                               @if($cities_incomplete->transportation=="PLANE") checked
                                                               @endif value='PLANE'>
                                                        <label class="check-field" for="arrive_radio-1">
                                                            <i class="trav-angle-plane-icon"></i>
                                                        </label>
                                                    </div>
                                                 </div>
                                             </div>
                                         </div>
                                       @endif
                                                
                                        @if(count($trip_city_places) > 0)
                                            @if(count($trip_city_places) == 1)
                                                @foreach($trip_city_places as $trip_places)
                                                    @if( count($trips_cities_incomplete) > 1 || (isset($trips_cities_complete) && count($trips_cities_complete)>0))
                                                      @php $trip_arr[$cities_incomplete->city->id.'-place-'.$trip_places->places_id] = ['lat'=>$trip_places->place->lat, 'lng'=>$trip_places->place->lng] @endphp
                                                    @endif

                                                    @php $place_location = ['lat'=>$trip_places->place->lat, 'lng'=>$trip_places->place->lng];
                                                          if(!in_array($trip_places->place->id, $existing_place_ids)){$existing_place_ids[] = $trip_places->place->id;}
                                                    @endphp
                                                    <div class="trip-side-block">
                                                        <div class="trip-place-name form-row place-row" style="display: block !important;">
                                                            <div class="field-style flex-field">
                                                                <div class="img-wrap">
                                                                    <img src="{{check_place_photo($trip_places->place)}}"
                                                                         alt="photo" style='width:54px;height:54px;'>
                                                                </div>
                                                                <div class="field-txt">
                                                                    {{$trip_places->place->transsingle->title}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if($j < count($trips_cities_incomplete) - 1 && count($trips_cities_incomplete)>1)
                                                            <div class="trip-place-name form-row place-row pl-5 d-block">
                                                                <div class="{{$cities_incomplete->city->id}}-place-{{$trip_places->places_id}}">
                                                                    <div class="trip-txt-info d-flex" >
                                                                        <div class="trip-info w-50 d-flex" style="align-items: center;">
                                                                            <div class="icon-wrap">
                                                                                <i class="trav-clock-icon" style="font-size:23px;color:#ccc;"></i>
                                                                            </div>
                                                                            <div class="trip-info-inner">
                                                                                <p class="mb-0"><b class="duration-{{$cities_incomplete->city->id}}-place-{{$trip_places->places_id}}"></b>
                                                                                </p>
                                                                                <p class="mb-0">@lang('book.duration')</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="trip-info w-50 d-flex" style="align-items: center;">
                                                                            <div class="icon-wrap">
                                                                                <i class="trav-distance-icon" style="font-size:23px;color:#ccc;"></i>
                                                                            </div>
                                                                            <div class="trip-info-inner">
                                                                                <p class="mb-0"><b class="distance-{{$cities_incomplete->city->id}}-place-{{$trip_places->places_id}}"></b></p>
                                                                                <p class="mb-0">@lang('profile.distance')</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                       @else
                                                        <div class="trip-place-name" style="display: block !important;">
                                                            <div class="field-style flex-field">
                                                                What is your next Place?
                                                            </div>
                                                        </div>
                                                    @endif
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class='trip-side-block'>
                                                    @foreach($trip_city_places as $trip_places)
                                                        <div class="trip-place-name form-row place-row" style="display: block !important;">
                                                            <div class="field-style flex-field">
                                                                <div class="img-wrap">
                                                                    <img src="{{check_place_photo($trip_places->place)}}"
                                                                         alt="photo" style='width:54px;height:54px;'>
                                                                </div>
                                                                <div class="field-txt">
                                                                    {{$trip_places->place->transsingle->title}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if($i < count($trip_city_places)-1 || ($j < count($trips_cities_incomplete) - 1))
                                                            <div class="form-row place-row pl-5">
                                                                <div class="{{$cities_incomplete->city->id}}-place-{{$trip_places->places_id}}">
                                                                    <div class="trip-txt-info d-flex" >
                                                                        <div class="trip-info w-50 d-flex" style="align-items: center;">
                                                                            <div class="icon-wrap">
                                                                                <i class="trav-clock-icon" style="font-size:23px;color:#ccc;"></i>
                                                                            </div>
                                                                            <div class="trip-info-inner">
                                                                                <p class="mb-0"><b class="duration-{{$cities_incomplete->city->id}}-place-{{$trip_places->places_id}}"></b>
                                                                                </p>
                                                                                <p class="mb-0">@lang('book.duration')</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="trip-info w-50 d-flex" style="align-items: center;">
                                                                            <div class="icon-wrap">
                                                                                <i class="trav-distance-icon" style="font-size:23px;color:#ccc;"></i>
                                                                            </div>
                                                                            <div class="trip-info-inner">
                                                                                <p class="mb-0"><b class="distance-{{$cities_incomplete->city->id}}-place-{{$trip_places->places_id}}"></b></p>
                                                                                <p class="mb-0">@lang('profile.distance')</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @php $i++; $trip_arr[$cities_incomplete->city->id.'-place-'.$trip_places->places_id] = ['lat'=>$trip_places->place->lat, 'lng'=>$trip_places->place->lng];
                                                    if(!in_array($trip_places->place->id, $existing_place_ids)){$existing_place_ids[] = $trip_places->place->id;}
                                                    @endphp
                                                    @endforeach
                                                </div>
                                             @endif
                                        @else
                                         <div class="trip-side-block">
                                                @if( count($trips_cities_incomplete) > 1 && $j< count($trips_cities_incomplete)-1)
                                                    <div class="trip-place-name form-row place-row pl-5 d-block">
                                                        <div class="city-{{$cities_incomplete->city->id}}">
                                                            <div class="trip-txt-info d-flex" >
                                                                <div class="trip-info w-50 d-flex" style="align-items: center;">
                                                                    <div class="icon-wrap">
                                                                        <i class="trav-clock-icon" style="font-size:23px;color:#ccc;"></i>
                                                                    </div>
                                                                    <div class="trip-info-inner">
                                                                        <p class="mb-0"><b class="duration-city-{{$cities_incomplete->city->id}}"></b>
                                                                        </p>
                                                                        <p class="mb-0">@lang('book.duration')</p>
                                                                    </div>
                                                                </div>
                                                                <div class="trip-info w-50 d-flex" style="align-items: center;">
                                                                    <div class="icon-wrap">
                                                                        <i class="trav-distance-icon" style="font-size:23px;color:#ccc;"></i>
                                                                    </div>
                                                                    <div class="trip-info-inner">
                                                                        <p class="mb-0"><b class="distance-city-{{$cities_incomplete->city->id}}"></b></p>
                                                                        <p class="mb-0">@lang('profile.distance')</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                 @php if(count($trips_cities_incomplete) > 1 || (isset($trips_cities_complete) && count($trips_cities_complete)>0)){
                                                 $trip_arr['city-'.$cities_incomplete->city->id] = ['lat'=>$cities_incomplete->city->lat, 'lng'=>$cities_incomplete->city->lng];
                                                 } @endphp
                                                <div class="trip-place-name form-row place-row">
                                                    <div class="field-style">
                                                       <div class="field-label mb-0">
                                                           Add Places in  {{$cities_incomplete->city->transsingle->title}}
                                                       </div>
                                                   </div>
                                               </div>
                                            </div>
                                        @endif
                                        <div class="flight-citys d-none">
                                             <div class="form-row place-row pl-5">
                                                <div class="flight-city-{{$cities_incomplete->city->id}}">
                                                      <div class="trip-txt-info d-flex" >
                                                          <div class="trip-info w-50 d-flex" style="align-items: center;">
                                                              <div class="icon-wrap">
                                                                  <i class="trav-clock-icon" style="font-size:23px;color:#ccc;"></i>
                                                              </div>
                                                              <div class="trip-info-inner">
                                                                  <p class="mb-0"><b class="flight-duration-city-{{$cities_incomplete->city->id}}"></b>
                                                                  </p>
                                                                  <p class="mb-0">@lang('book.duration')</p>
                                                              </div>
                                                          </div>
                                                          <div class="trip-info w-50 d-flex" style="align-items: center;">
                                                              <div class="icon-wrap">
                                                                  <i class="trav-distance-icon" style="font-size:23px;color:#ccc;"></i>
                                                              </div>
                                                              <div class="trip-info-inner">
                                                                  <p class="mb-0"><b class="flight-distance-city-{{$cities_incomplete->city->id}}"></b></p>
                                                                  <p class="mb-0">@lang('profile.distance')</p>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                    </div>
                                    <div class="add-footer">
                                        <button type="button" class="btn btn-transp btn-clear removeCity"
                                                data-city_id="{{$cities_incomplete->city->id}}">@lang('buttons.general.crud.delete')
                                        </button>
                                        <button type="button" class="btn btn-light-primary btn-bordered activateCity"
                                                data-id='{{$cities_incomplete->id}}'>@lang('buttons.general.save')
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @php $j++; @endphp
                    @endforeach
                @endif

            </div>

            <div class="trip-side-button-block">
                <button class="btn btn-transp btn-bordered" data-toggle="modal" data-target="#addPlacePopup"
                        @if(!isset($city) OR !is_object($city)) disabled @endif>
                    <i class="trav-add-place-icon"></i>
                    @isset($city)
                        <span>@lang('trip.add_a_place_in_name', ['name' => $city->transsingle->title])</span>
                    @endif
                </button>
                <button class="btn btn-transp btn-bordered" data-toggle="modal" data-target="#addCityPopup">
                    <i class="trav-move-new-icon"></i>
                    <span>@lang('trip.move_to_a_new_country_or_city')</span>
                </button>
            </div>
        </div>
        <div class="trip-map-block">
            <div class="trip-map-layer">
                <div id="maps" style="width:100%"></div>
                
            </div>
        </div>
    </div>

	<!--Delete trip city confirmation  modal-->
	<div class="modal fade white-style" data-backdrop="false" id="deleteTripCityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
			<i class="trav-close-icon"></i>
		</button>
		<div class="modal-dialog modal-custom-style " role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h6 class="modal-title">@lang("profile.are_you_sure_want_to_delete")</h6>

				</div>
				<div class="modal-footer">
					<a href="" data-dismiss="modal">Close</a>
					<button type="button" class="btn btn-danger confirm-trip-city-delete" data-id=""  style="padding:.5rem 1rem;">Confirm</button>

				</div>
			</div>
		</div>
	</div>
@endsection

@section('before_scripts')
    <!-- modals -->
    @include('site/trip/partials/select-city-popup')
    @include('site/trip/partials/select-place-popup')
    @include('site/trip/partials/first-step-popup')
@endsection

@section('before_site_script')
    @parent
    <script src="{{asset('assets2/js/timepicker/jquery.timepicker.min.js')}}"></script>
    <script src="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.js')}}"></script>
    <script src="{{asset('assets2/js/html2canvas/html2canvas.min.js')}}"></script>
@endsection

@section('after_scripts')
    <script>
        @if (isset($goto_first_step) AND $goto_first_step)
        $(document).ready(function () {
            $('#createTripPlanStep').modal('show');

        });
        @endif
        @if (isset($trip_id) AND $trip_id>0 AND isset($do) AND $do=="add-city")
        $(document).ready(function () {
            $('#addCityPopup').modal('show');

        });
        @endif
        @if (isset($city) AND isset($do) AND $do=="add-place")
        $(document).ready(function () {
            $('#addPlacePopup').modal('show');

        });
        @endif


        $('input[name=budget]').on('click', function (e) {
            $('#budget_custom').val($(this).val());
             $('.spending_amount').html('$' + $(this).val()); 

        });
        $('body').on('click', '#save_trip', function (e) {
            var trip_id = $('#trip_id').val();
            var version_id = $('#version_id').val();
            if ($('#send_back').is(":checked")) {
                var send_back = $('#send_back').val();
            }

            $.ajax({
                method: "POST",
                url: "{{url('trip/ajaxActivateTrip')}}",
                data: {"trip_id": trip_id, "version_id": version_id, "send_back": send_back}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (result.status == 'success') {
                        //console.log(result.trip_city_id);
                        window.location.replace("{{url('trip/view')}}" + "/" + trip_id);
                    } else if (result.status == 'error_unknow') {
                        alert('{{ __("trip.unknown_error_while_deleting_trip_plan") }}');
                    } else if (result.status == 'error_not_enough_data') {
                        alert('{{ __("trip.you_need_to_add_at_least_one_place_to_publish_your_trip") }}');
                    }

                });
            e.preventDefault();
        });

        $(document).ready(function () {
            $('body').on('click', '.privacy-button', function (e) {
               $($(this).find("input[type=radio]")).prop("checked", true);
               $('.privacy-button').each(function(i, obj) {
                    $(obj).removeClass('btn-transp')
                    $(obj).removeClass('btn-light-primary')
                   if($($(obj).find("input[name='privacy']:checked")).val()){
                         $(obj).addClass('btn-light-primary')
                   }else{
                        $(obj).addClass('btn-transp')
                   }
                });
            });
             
            $('body').on('keyup', '#budget_custom', function (e) {
                if(!$.isNumeric($(this).val())){
                    $(this).val('')
                }else{
                   $('.spending_amount').html('$' + $(this).val()); 
                }
                $('input[name=budget]').prop('checked', false);
            });
            
             $('body').on('click', '.city-content', function (e) {
                 window.location.href="?city_id=" + $(this).data('city_id')
             });
             
            $('body').on('submit', '#createTrip', function (e) {
                var values = $(this).serialize();
                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxFirstStep')}}",
                    data: values
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        console.log(result);
                        if (result.status == 'success') {
                            console.log(result.trip_id);
                            @if(isset($memory))
                                window.location.replace("{{url('trip/plan')}}" + "/" + result.trip_id + "?do=add-city&memory=1");
                            @else
                                window.location.replace("{{url('trip/plan')}}" + "/" + result.trip_id + "?do=add-city");
                            @endif
                        } else {

                        }


                    });
                e.preventDefault();
            });
            
            // Delete trip city and place
            $('body').on('click', '.removeCity', function (e) {
                $('.confirm-trip-city-delete').attr('data-id', $(this).data('city_id'))
              $('#deleteTripCityModal').modal('show')
            });
            
          
          @if(isset($trip) && is_object($trip))
            $('body').on('click', '.confirm-trip-city-delete', function (e) {
                var trip_id = {{$trip->id}};
                var city_id = $(this).data('id');
                var version_id = {{$trip->myversion()[0]->id}};
                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxRemoveCityFromTrip')}}",
                     data: {"city_id": city_id, "trip_id": trip_id, "version_id": version_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        console.log(result);
                        if (result.status == 'success') {
                            if(result.last_city_id != 0){
                                 window.location.replace("{{url('trip/plan')}}/{{$trip->id}}?city_id=" + result.last_city_id);
                            }else{
                                @if(isset($memory))
                                    window.location.replace("{{url('trip/plan')}}/{{$trip->id}}?do=add-city&memory=1");
                                @else
                                    window.location.replace("{{url('trip/plan')}}/{{$trip->id}}?do=add-city");
                                @endif
                            }
                        } else {

                        }


                    });
                e.preventDefault();
            });
        @endif

            @if(isset($city) && is_object($city))
            $('body').on('submit', '#activatePlace', function (e) {
                var values = $(this).serialize();
                console.log(values);
                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxActivateTripPlace')}}",
                    data: values
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        console.log(result);
                        if (result.status == 'success') {
                            console.log(result.trip_id);
                            window.location.replace("{{url('trip/plan')}}" + "/"+{{$trip_id}}+
                            "?city_id=" +{{$city->id}})
                            ;
                        } else {

                        }


                    });
                e.preventDefault();
            });

            @endif


            function delay(callback, ms) {
                var timer = 0;
                return function () {
                    var context = this, args = arguments;
                    clearTimeout(timer);
                    timer = setTimeout(function () {
                        callback.apply(context, args);
                    }, ms || 0);
                };
            }

            $('body').on('keyup', '#citySearchInput', delay(function (e) {
                var value = $(this).val();
                var selected_cities = '{{json_encode($existing_city_ids)}}';
                if (value.length >= 2) {
                    $.ajax({
                        method: "POST",
                        url: "{{url('trip/ajaxSearchCities')}}",
                        data: {"query": value, selected_cities: selected_cities}
                    })
                        .done(function (res) {
                            var result = JSON.parse(res);
                            //console.log(result);
                            $('#sugg-inner').html(result);

                        });
                }
                e.preventDefault();
            }, 500));


            @if(isset($trip))
            $('body').on('click', '.doAddCity', function (e) {
                var city_id = $(this).attr('data-id');
                var trip_id = {{$trip->id}};
                var version_id = {{$trip->myversion()[0]->id}};

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxAddCityToTrip')}}",
                    data: {"city_id": city_id, "trip_id": trip_id, "version_id": version_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            console.log(result.trip_city_id);
                            @if(isset($memory))
                                window.location.replace("{{url('trip/plan')}}" + "/" + trip_id + "?do=add-place&city_id=" + city_id + "&memory=1");
                            @else
                                window.location.replace("{{url('trip/plan')}}" + "/" + trip_id + "?do=add-place&city_id=" + city_id);
                            @endif
                        } else {

                        }

                    });
                e.preventDefault();
            });
            @endif


            $('body').on('keyup', '#placeSearchInput', delay(function (e) {
                var value = $(this).val();
                var selected_places = '{{json_encode($existing_place_ids)}}';
                if (value.length >= 2) {
                            @if(isset($city))
                    var city_id = {{$city->id}};
                    @endif
                    $.ajax({
                        method: "POST",
                        url: "{{url('trip/ajaxSearchPlaces')}}",
                        data: {"query": value, "city_id": city_id, selected_places: selected_places}
                    })
                        .done(function (res) {
                            var result = JSON.parse(res);
                            console.log(result);
                            $('#place-sugg-inner').html(result);

                        });
                    e.preventDefault();
                }
            }, 500));
            @if(isset($city) AND isset($trip))
            $('body').on('click', '.doAddPlace', function (e) {
                $(this).attr("disabled", true);
                var place_id = $(this).attr('data-id');
                var trip_id = {{$trip->id}};
                var city_id = {{$city->id}};
                var version_id = {{$trip->myversion()[0]->id}};

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxAddPlaceToTrip')}}",
                    data: {"city_id": city_id, "trip_id": trip_id, "place_id": place_id, "version_id": version_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            console.log(result.trip_city_id);
                            @if(isset($memory))
                                window.location.replace("{{url('trip/plan')}}" + "/" + trip_id + "?city_id=" + city_id);
                            @else
                                window.location.replace("{{url('trip/plan')}}" + "/" + trip_id + "?city_id=" + city_id + "&memory=1");
                            @endif
                        } else {

                        }

                    });
                e.preventDefault();
            });

            $('body').on('click', '#removePlace', function (e) {
                var place_id = $(this).attr('data-id');
                var trip_id = {{$trip->id}};
                var city_id = {{$city->id}};
                var version_id = {{$trip->myversion()[0]->id}};

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxRemovePlaceFromTrip')}}",
                    data: {"city_id": city_id, "trip_id": trip_id, "place_id": place_id, "version_id": version_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            console.log(result.trip_city_id);
                            window.location.replace("{{url('trip/plan')}}" + "/" + trip_id + "?city_id=" + city_id);
                        } else {

                        }

                    });
                e.preventDefault();
            });
            @endif

            @if(isset($trip) && isset($city) && is_object($city))
            $('body').on('click', '.activateCity', function (e) {
                var trip_city_id = $(this).attr('data-id');
                var transportation = $("input[name='transportation']:checked").val();
                var version_id = {{$trip->myversion()[0]->id}};

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxActivateTripCity')}}",
                    data: {"trip_city_id": trip_city_id, "transportation": transportation, "version_id": version_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        console.log(result);
                        if (result.status == 'success') {
                            //console.log(result.trip_city_id);
                            window.location.replace("{{url('trip/plan')}}" + "/"+{{$trip_id}}+
                            "?city_id=" +{{$city->id}})
                            ;
                        } else {

                        }

                    });
                e.preventDefault();
            });
            @endif

            @if(isset($trip) && is_object($trip->myversion()[0]) && isset($city))
            $('body').on('click', '.editPlaceIcon', function (e) {
                var trip_place_id = $(this).attr('data-id');
                var version_id = {{$trip->myversion()[0]->id}};

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxDeActivateTripPlace')}}",
                    data: {"trip_place_id": trip_place_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        console.log(result);
                        if (result.status == 'success') {
                            console.log(result.trip_id);
                            window.location.replace("{{url('trip/plan')}}" + "/"+{{$trip_id}}+
                            "?city_id=" +{{$city->id}})
                            ;
                        } else {

                        }


                    });
                e.preventDefault();
            });
            @endif

            @if(isset($trip) && is_object($trip->myversion()[0]) && isset($city))

            $('body').on('click', '.editCityIcon', function (e) {
                var trip_city_id = $(this).attr('data-id');
                var version_id = {{$trip->myversion()[0]->id}};

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxDeActivateTripCity')}}",
                    data: {"trip_city_id": trip_city_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        console.log(result);
                        if (result.status == 'success') {
                            console.log(result.trip_id);
                            window.location.replace("{{url('trip/plan')}}" + "/"+{{$trip_id}}+
                            "?city_id=" +{{$city->id}})
                            ;
                        } else {

                        }


                    });
                e.preventDefault();
            });
            @endif



            $(".datepicker").datepicker({
                dateFormat: "d MM yy",
                altField: "#actual_trip_start_date",
                altFormat: "yy-mm-dd",
                @if(!isset($memory))
                     minDate: new Date()
                @endif

            });


            @if(isset($trip) && isset($trip->myversion()[0]) AND is_object($trip->myversion()[0]))
            $(".datepicker_place").datepicker({
                minDate: '{{date("d F Y", strtotime($trip->myversion()[0]->start_date))}}',
                dateFormat: "d MM yy",
                altField: "#actual_place_date",
                altFormat: "yy-mm-dd",
                defaultDate: ""
            });
            @endif

            $('.timepicker').timepicker();

        });

        function doPlus(fieldId) {
            $('#' + fieldId).val(parseInt($('#' + fieldId).val()) + 1);
        }

        function doMinus(fieldId) {
            if (parseInt($('#' + fieldId).val()) > 0) {
                $('#' + fieldId).val(parseInt($('#' + fieldId).val()) - 1);
            }
        }


    </script>
     @include('site.trip.partials._maps-scripts')
    <script>
        // confirmation
        $('.confirm-trip-deletion').on('click', function () {
            $.confirm({
                title: '{{ __('other.are_you_sure') }}',
                content: '{{ __('trip.if_you_canceled_this_trip_you_will') }}',
                icon: 'fa fa-question-circle',
                animation: 'scale',
                closeAnimation: 'scale',
                opacity: 0.5,
                buttons: {
                    'confirm': {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function () {

                            $.ajax({
                                method: "POST",
                                url: "{{route('trip.ajax_delete_trip')}}",
                                data: {"trip_id": {{$trip_id}}}
                            })
                                .done(function (res) {
                                    var result = JSON.parse(res);
                                    if (result.status == 'success') {
                                        //console.log(result.trip_city_id);
                                        window.location.replace("{{url('home')}}");
                                    } else {
                                        alert('{{ __("trip.unknown_error_while_deleting_trip_plan") }}');
                                    }


                                });
                        }
                    },
                    cancel: function () {

                    },

                }
            });
        });

    </script>
    
    @if(isset($memory) && isset($trip))
        <script type="text/javascript" src="{{asset('assets2/js/plupload-2.3.6/js/plupload.full.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets2/js/plupload-2.3.6/js/jquery.ui.plupload/jquery.ui.plupload.min.js')}}"></script>
    <script type="text/javascript">
// Initialize the widget when the DOM is ready
$(function() {
    var place_id = $('#place_id').val();
    console.log(place_id);
    $(".uploader").plupload({
        // General settings
        runtimes : 'html5,flash,silverlight,html4',
        url : "{{url('trip/plan/'.$trip->id.'/media')}}?trip_id={{$trip->id}}&place_id="+place_id,
 
        // Maximum file size
        max_file_size : '10mb',
 
        //chunk_size: '1mb',
 
        // Resize images on clientside if we can
        //resize : {
        //    width : 200,
        //    height : 200,
        //    quality : 90,
        //    crop: true // crop to exact dimensions
        //},
 
        // Specify what files to browse for
        filters : [
            {title : "Image files", extensions : "jpeg,jpg,gif,png"},
            {title : "Video files", extensions : "mp4"}
        ],
 
        // Rename files by clicking on their titles
        rename: false,
         
        // Sort files
        sortable: false,
 
        // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
        dragdrop: true,
 
        // Views to activate
        views: {
            thumbs: true, // Show thumbs
            active: 'thumbs'
        },
 
        // Flash settings
        flash_swf_url : '/plupload/js/Moxie.swf',
     
        // Silverlight settings
        silverlight_xap_url : '/plupload/js/Moxie.xap'
    });
});

</script>
    @endif
 
@endsection