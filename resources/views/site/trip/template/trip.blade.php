@extends('site.layouts.site')

@section('before_site_style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('base')
    @yield('content')
@endsection

@section('before_site_script')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection