<div class="modal fade white-style"  data-backdrop="static" data-keyboard="false" id="createTripPlanStep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    
    <div class="modal-dialog modal-custom-style modal-750" role="document">
        <form method="post" id="createTrip" autocomplete="off">
            <div class="modal-custom-block">
                <div class="post-block post-modal-trip-planner">
                    <div class="plan-head">
                        <h4 class="ttl">
                            @if(isset($memory))
                                Create a Memory Trip
                            @else
                                @lang('trip.trip_planner')
                            @endif
                        </h4>
                        <div class="head-btn-wrap">
                        <!--<button type="button" class="btn btn-transp btn-clear">@lang('buttons.general.save')</button>-->
                            <button type="submit"
                                    class="btn btn-light-primary btn-bordered">@lang('other.next_step')</button>
                        </div>
                    </div>
                    <div class="create-trip-block">
                        <div class="form-row">
                            <div class="field-label">@lang('trip.what_do_you_want_to_call_your_trip')</div>
                            <div class="field-item">
                                <input type="text" name="title"
                                       placeholder="@lang('trip.please_enter_a_descriptive_name_for_your_trip')"
                                       required maxlength="100">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="field-label">
                                @if(isset($memory))
                                    When did you start your trip?
                                @else
                                    @lang('trip.when_do_you_intend_to_go_on_this_trip')
                                @endif
                            </div>
                            <div class="field-item">
                                <div class="field-ttl">@lang('time.date')</div>
                                <input class="datepicker" type="text" name="date" placeholder="" required>
                                <input type="hidden" name="actual_start_date" id="actual_trip_start_date" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="field-label">@lang('trip.privacy_options')</div>
                            <p>@lang('trip.you_decide_who_can_see_your_trip_plan')</p>
                        </div>
                        <div class="create-footer">
                            <button type="button" class="btn btn-light-primary btn-bordered privacy-button">
                                <div class="icon-wrap">
                                    <input type="radio" name="privacy" class="d-none" value="1" required checked />
                                    <i class="trav-earth"></i>

                                </div>
                                <div class="btn-text">
                                    <div class="ttl">@lang('profile.public')</div>
                                    <div class="sub-ttl">@lang('profile.everyone_can_see_this')</div>
                                </div>
                            </button>
                            <button type="button" class="btn btn-transp btn-bordered privacy-button">
                                <div class="icon-wrap">
                                    <input type="radio" name="privacy" class="d-none" value="2" required />
                                    <i class="trav-friends-icon"></i>
                                </div>
                                <div class="btn-text">
                                    <div class="ttl">@lang('profile.friends')</div>
                                    <div class="sub-ttl">@lang('profile.only_visible_to_friends')</div>
                                </div>
                            </button>
                            <button type="button" class="btn btn-transp btn-bordered privacy-button">
                                <div class="icon-wrap">
                                    <input type="radio" name="privacy" class="d-none" value="0" required />
                                    <i class="trav-lock-icon"></i>
                                </div>
                                <div class="btn-text">
                                    <div class="ttl">@lang('profile.private')</div>
                                    <div class="sub-ttl">@lang('profile.visible_only_to_me')</div>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>