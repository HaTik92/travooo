<!-- invite popup -->
        <div class="modal fade white-style" data-backdrop="false" id="invitePopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                <i class="trav-close-icon"></i>
            </button>
            <div class="modal-dialog modal-custom-style modal-700" role="document">
                <div class="modal-custom-block">
                    <div class="post-block post-going-block post-mobile-full">
                        <div class="post-top-layer">
                            <div class="post-top-info">
                                <h3 class="info-title">{{$trip_info->title}}</h3>
                                <ul class="info-list">
                                    <li class="info-inner">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="info-txt">
                                            <p class="title">{{calculate_duration($trip_info->id, 'all')}}</p>
                                            <p class="sub-title">@lang('trip.duration')n</p>
                                        </div>
                                    </li>
                                    <li class="info-inner">
                                        <div class="icon-wrap">
                                            <i class="trav-budget-icon"></i>
                                        </div>
                                        <div class="info-txt">
                                            <p class="title">$ {{$budget}}</p>
                                            <p class="sub-title">@lang('trip.budget')</p>
                                        </div>
                                    </li>
                                    <li class="info-inner">
                                        <div class="icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="info-txt">
                                            <p class="title">{{calculate_distance($trip_info->id)}}</p>
                                            <p class="sub-title">@lang('trip.distance')</p>
                                        </div>
                                    </li>
                                    <li class="info-inner">
                                        <div class="icon-wrap">
                                            <i class="trav-map-marker-icon"></i>
                                        </div>
                                        <div class="info-txt">
                                            <p class="title">{{count($trip_info->cities)}}</p>
                                            <p class="sub-title">@choice('trip.place', 2)</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="post-photo-wrap">
                                
                            </div>
                        </div>
                        <div class="post-sub-layer">
                            @lang('trip.invite_people_to_organize_this_trip_plan_together')
                        </div>
                        <div class="post-people-block-wrap mCustomScrollbar">
                            @foreach($users AS $user)
                            <div class="people-row">
                                <div class="main-info-layer">
                                    <div class="img-wrap">
                                        <img class="ava" src="{{check_profile_picture($user->profile_picture)}}" alt="ava" style="width:50px;height:50px;">
                                    </div>
                                    <div class="txt-block">
                                        <div class="name">{{$user->name}}</div>
                                        <div class="info-line">
                                            <div class="info-part">{{$user->display_name}}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="button-wrap">
                                    <button class="btn btn-light-grey btn-bordered btnInviteFriend" data-id="{{$user->id}}">Select</button>
                                    <!--<button class="btn btn-light-primary btn-bordered" data-id="{{$user->id}}">Unselect</button>-->
                                </div>
                            </div>
                            @endforeach
                            
                        </div>
                        <form method="post" id="InviteFriendsForm">
                        <div class="post-people-txt">
                            <ul class="people-ava-list" id="people-ava-list">
                                <!--<li><a href="#"><img src="http://placehold.it/50X50" alt="avatar"></a></li>
                                <li><a href="#"><img src="http://placehold.it/50X50" alt="avatar"></a></li>
                                <li><a href="#"><img src="http://placehold.it/50X50" alt="avatar"></a></li>
                                <li><a href="#"><img src="http://placehold.it/50X50" alt="avatar"></a></li>
                                <li><a href="#"><img src="http://placehold.it/50X50" alt="avatar"></a></li>
                                <li><a href="#"><img src="http://placehold.it/50X50" alt="avatar"></a></li>
                                <li><a href="#"><img src="http://placehold.it/50X50" alt="avatar"></a></li>
                                -->
                            </ul>
                            <div class="form-row">
                                <input class="field-item" type='text' name='budget' style='border:1px;'/>
                            </div>
                            <div class="people-txt-wrap">
                                <textarea class="people-txt" name="notes" id="notes" cols="30" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="post-btn-foot">
                            <input type="hidden" name="trip_id" value="{{$trip_info->id}}" />
                            <button type="button"
                                    class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                            <button type="submit" name="submit"
                                    class="btn btn-light-primary btn-bordered">@lang('buttons.general.invite')</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>