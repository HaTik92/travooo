<style>
    /*maps custon style*/
    .create-trip-wrapper .trip-map-block .trip-map-layer img{
            object-fit: unset !important;
    }
</style>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&callback=initMap&language=en"></script>
<script>
    var infowindows = [], map, marker, flightPath = [], markers = [];
    var trip_cities_arr = <?= json_encode($trip_cities_arr);?>;
        // Initialize and add the map
        function initMap() {
              var directionsService = new google.maps.DirectionsService;
              var directionsDisplay = new google.maps.DirectionsRenderer;
              @if(isset($city_location))
                  @if(isset($place_location))
                      
                       var world = {lat: parseFloat('{{$place_location["lat"]}}'), lng: parseFloat('{{$place_location["lng"]}}')};  
                  @else
                       var world = {lat: parseFloat('{{$city_location["lat"]}}'), lng: parseFloat('{{$city_location["lng"]}}')};  
                  @endif
              @else
                var world = {lat: 25.204849, lng: 55.270782};  
              @endif

            
            map = new google.maps.Map(
                document.getElementById('maps'), {zoom: 13, center: world, mapTypeId: 'satellite', pixelRatio: window.devicePixelRatio || 1});
         marker = new google.maps.Marker({position: world, map: map});
        directionsDisplay.setMap(map);
   
        
var origin = '';
var direction = '';
var trip_ids = [];
var points = [];


@if (isset($trip_arr) && count($trip_arr) > 0)
    @php $i = 0; $number_count = count($trip_arr); @endphp
    @foreach($trip_arr as $current_trip_id => $trip)
        @if ($i == 0)
            origin = {lat:'{{$trip["lat"]}}', lng:'{{$trip["lng"]}}'};
        @endif
        @if (++$i == $number_count)
            direction = {lat:'{{$trip["lat"]}}', lng:'{{$trip["lng"]}}'};
        @endif
        @if ($i != $number_count && $i != 1)
            points.push({lat:'{{$trip["lat"]}}', lng:'{{$trip["lng"]}}'})
        @endif
        @if ($i < $number_count)
            trip_ids.push('{{$current_trip_id}}')
        @endif
    @endforeach
@endif

if(origin != '' && direction !=''){
    var travel_mode;
    if(typeof $("input[name='transportation']:checked").val() != 'undefined'){
        travel_mode = $("input[name='transportation']:checked").val();
    }else{
        travel_mode = 'DRIVING';
    }
    getDisplayRoute(directionsService, directionsDisplay, origin, direction, points, trip_ids, travel_mode);
}

$(document).on('click', '.travel-type', function(){
    if(infowindows){
        for (var i = 0; i < infowindows.length; i++) {
            infowindows[i].close();
        }
    }
    getDisplayRoute(directionsService, directionsDisplay, origin, direction, points, trip_ids, $(this).data('mode'));
})

        }
        
function getDisplayRoute(directionsService, directionsDisplay, origin, direction, points, place_ids, mode) {
        if(markers){
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
        }
        if(Object.keys(trip_cities_arr).length > 1 && mode == 'PLANE'){
        $('.flight-citys').removeClass('d-none');
        var flightPlanCoordinates = [], i = 0, cities_id = [], flight_trip_cities = [];
        $.each( trip_cities_arr, function( k, val ) {
            cities_id[i] = k;
            markers[i] = new google.maps.Marker({position:{lat: parseFloat(val.lat), lng: parseFloat(val.lng)}, map: map});
            flightPlanCoordinates.push({lat: parseFloat(val.lat), lng: parseFloat(val.lng)})
            i++;
        })
       
        if(directionsDisplay){
            directionsDisplay.setMap(null);
            directionsDisplay = null;
        }
        
         for (var j = 1; j < flightPlanCoordinates.length; j++) {
            var flight_obj = {};
             
                flightPath[j-1] = new google.maps.Polyline({
                path: geodesicPolyline(flightPlanCoordinates[j-1], flightPlanCoordinates[j]),
                geodesic: true,
                strokeColor: '#4080ff',
                strokeOpacity: 1.0,
                strokeWeight: 5,
                });
        
            flightPath[j-1].setMap(map);
             
            infowindows[j-1] = new google.maps.InfoWindow({
                    content: getInfoWindowContent('', distanceFrom(flightPlanCoordinates[j-1], flightPlanCoordinates[j], 1), 'PLANE'),
                    map: map,
                    position: GetPointsAtDistance(flightPath[j-1].getPath(), flightPlanCoordinates[j-1], flightPlanCoordinates[j], distanceFrom(flightPlanCoordinates[j-1], flightPlanCoordinates[j], 1)/2)
                });
                
            $('.flight-duration-city-'+cities_id[j-1]).html(secondsTimeSpanToHMS(distanceFrom(flightPlanCoordinates[j-1], flightPlanCoordinates[j], 1)/222))
            $('.flight-distance-city-'+cities_id[j-1]).html((distanceFrom(flightPlanCoordinates[j-1], flightPlanCoordinates[j], 1)/1000).toFixed(2) + ' km');
            
            flight_obj[cities_id[j-1]] = {'duration': Math.floor(distanceFrom(flightPlanCoordinates[j-1], flightPlanCoordinates[j], 1)/222),'distance':  Math.floor(distanceFrom(flightPlanCoordinates[j-1], flightPlanCoordinates[j], 1))}; 
                flight_trip_cities.push(flight_obj);     
                
         }
        for (var l = 0; l < place_ids.length; l++) {
            $('.'+place_ids[l]).addClass('d-none');
        }
     
          postDistance(flight_trip_cities, place_ids, 'PLANE', 1);
       
    }else{
        if(flightPath){
            for(k = 0; k<flightPath.length; k++){
                 flightPath[k].setMap(null);
            }
              directionsDisplay.setMap(map);
        }
    
        var waypts = [];
         for (var i = 0; i < points.length; i++) {
            waypts.push({
              location: new google.maps.LatLng(parseFloat(points[i].lat), parseFloat(points[i].lng)),
              stopover: true
            })  
         };

        directionsService.route({
         origin: new google.maps.LatLng(parseFloat(origin.lat), parseFloat(origin.lng)),
        destination: new google.maps.LatLng(parseFloat(direction.lat), parseFloat(direction.lng)),
          waypoints: waypts,
          optimizeWaypoints: false,
          travelMode: google.maps.TravelMode[mode],
          transitOptions: {
            departureTime: new Date(),
            modes: ['BUS'],
            routingPreference: 'FEWER_TRANSFERS'
          },
        }, function(response, status) {
          if (status === 'OK') {
               marker.setMap(null);
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            postDistance(route.legs, place_ids, google.maps.TravelMode[mode], 2);
             $('.flight-citys').addClass('d-none');
              for (var i = 0; i < route.legs.length; i++) {
                  $('.'+place_ids[i]).removeClass('d-none');
                  $('.duration-'+place_ids[i]).html(route.legs[i].duration.text)
                  $('.distance-'+place_ids[i]).html(route.legs[i].distance.text);
                  
                var content = getInfoWindowContent(route.legs[i].duration.text, route.legs[i].distance.text, mode);
                var point = distanceAlongPath(null, route.legs[i].distance.value, route.legs[i].steps, .2);
               
                infowindows[i] = new google.maps.InfoWindow({
                    content: content,
                    map: map,
                    position: point
                });
              }
          } else {
                 // alert an error message when the route could nog be calculated.
                if (status == 'ZERO_RESULTS') {
                  //alert('No route could be found between the origin and destination.');
                } else if (status == 'UNKNOWN_ERROR') {
                  alert('A directions request could not be processed due to a server error. The request may succeed if you try again.');
                } else if (status == 'REQUEST_DENIED') {
                  alert('This webpage is not allowed to use the directions service.');
                } else if (status == 'OVER_QUERY_LIMIT') {
                  alert('The webpage has gone over the requests limit in too short a period of time.');
                } else if (status == 'NOT_FOUND') {
                  alert('At least one of the origin, destination, or waypoints could not be geocoded.');
                } else if (status == 'INVALID_REQUEST') {
                  alert('The DirectionsRequest provided was invalid.');         
                } else {
                  alert("There was an unknown error in your request. Requeststatus: nn"+status);
                }
          }
        });
    }
      }
      
function distanceAlongPath(distanceFromOrigin, distences, steps, ratioFromOrigin) {
    var totalDistance = distences;
    var tempDistanceSum = 0;
    
    if (ratioFromOrigin) {
      distanceFromOrigin = ratioFromOrigin * totalDistance;
    }

        for (var i in steps) {
            tempDistanceSum += steps[i].distance.value;

            if (tempDistanceSum > distanceFromOrigin) {
                return steps[i].end_point;
            }
        }
    }
    
    function getInfoWindowContent(duration, distance, mode){
        var image, content;
        if(mode == 'DRIVING'){
            image = '<img src="{{asset('assets2/image/map-DRIVING-icon.png')}}" style="width:26px !important;" class="pr-2">';
        }else if(mode == 'WALKING'){
             image = '<img src="{{asset('assets2/image/map-WALKING-icon.png')}}" style="width:38px !important;" class="pr-2">';
        }else if(mode == 'PLANE'){
            duration = '<span style="padding-left:30px;">' + secondsTimeSpanToHMS(distance/222) + '</span>';
            distance = (distance/1000).toFixed(2) + ' km';
             image = '<img src="{{asset('assets2/image/map-PLANE-icon.png')}}" style="width:31px !important;" class="pr-2">';
        }else if(mode == 'TRANSIT'){
             image = '<img src="{{asset('assets2/image/map-TRANSIT-icon.png')}}" style="width:38px !important;" class="pr-2">';
        }
        
        content = image + distance + '<br>' + duration;
        
        return content;
    }
    
    function postDistance(distance, trip_info, travelMode, type){
     var trip_places = [];
     var trip_cities = [];
     if(type == 1){
         trip_cities = distance;
     }else{
        for (var i = 0; i < distance.length; i++) {
            var obj = {}; 
           if(trip_info[i].split('-')[1] != 'place'){
               obj[trip_info[i].split('-')[1]] = {'duration': distance[i].duration.value,'distance':  distance[i].distance.value}; 
               trip_cities.push(obj); 
           }else{
               if(i < distance.length-1 && trip_info[i].split('-')[0] != trip_info[i+1].split('-')[0]){
                   obj[trip_info[i].split('-')[0]] = {'duration': distance[i].duration.value,'distance':  distance[i].distance.value}; 
                   trip_cities.push(obj); 
               }else{
                   obj[trip_info[i].split('-')[2]] = {'duration': distance[i].duration.value,'distance':  distance[i].distance.value}; 
                   trip_places.push(obj); 
               }
           }

       }
    }

        var trip_id = '{{$trip_id}}';
        $.ajax({
           method: "POST",
           url: "{{url('trip/postTripDistance')}}",
                data: {"trip_places": trip_places, "trip_cities": trip_cities, 'trip_id':trip_id, 'traval_mode': travelMode}
        })
                .done(function (res) {
                });
       
    }
  
 // === A method which returns an float 2 point distance meters === 
 function distanceFrom (start, end, type) {
        var EarthRadiusMeters = 6378137.0; // meters
        if(type == 1){
            var lat1 = start.lat;
            var lon1 = start.lng;
            var lat2 = end.lat;
            var lon2 = end.lng; 
        }else{
            var lat1 = start.lat();
            var lon1 = start.lng();
            var lat2 = end.lat();
            var lon2 = end.lng();
        }
        var dLat = (lat2-lat1) * Math.PI / 180;
        var dLon = (lon2-lon1) * Math.PI / 180;
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
          Math.sin(dLon/2) * Math.sin(dLon/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = EarthRadiusMeters * c;
        return d;
}

// === A method which returns an array of GLatLngs of points a given interval along the path ===
function GetPointsAtDistance (distnce, start, end, metres) {
  if (metres == 0) return this.getPath().getAt(0);
  if (metres < 0) return null;
  if (distnce.getLength() < 2) return null;
  var dist=0;
  var olddist=0;
  for (var i=1; (i < distnce.getLength() && dist < metres); i++) {
    olddist = dist;
    dist += distanceFrom(distnce.getAt(i), distnce.getAt(i-1), 2);
  }
  if (dist < metres) {
    return null;
  }
  var p1= distnce.getAt(i-2);
  var p2= distnce.getAt(i-1);
  var m = (metres-olddist)/(dist-olddist);
  return new google.maps.LatLng( p1.lat() + (p2.lat()-p1.lat())*m, p1.lng() + (p2.lng()-p1.lng())*m);
}

// === A method which returns an 50 points between 2 points ===
function geodesicPolyline(start, end) {
  var points = 50;

  var geodesicPoints = new Array();
  with (Math) {
    var lat1 = start.lat * (PI/180);
    var lon1 = start.lng * (PI/180);
    var lat2 = end.lat * (PI/180);
    var lon2 = end.lng * (PI/180);

    var d = 2*asin(sqrt( pow((sin((lat1-lat2)/2)),2) + cos(lat1)*cos(lat2)*pow((sin((lon1-lon2)/2)),2)));

    for (var n = 0 ; n < points+1 ; n++ ) {
      var f = (1/points) * n;
      var A = sin((1-f)*d)/sin(d)
      var B = sin(f*d)/sin(d)
      var x = A*cos(lat1)*cos(lon1) +  B*cos(lat2)*cos(lon2)
      var y = A*cos(lat1)*sin(lon1) +  B*cos(lat2)*sin(lon2)
      var z = A*sin(lat1)           +  B*sin(lat2)

      var latN = atan2(z,sqrt(pow(x,2)+pow(y,2)))
      var lonN = atan2(y,x)
      var p = new google.maps.LatLng(latN/(PI/180), lonN/(PI/180));
      geodesicPoints.push(p);
    }
  }
  return geodesicPoints;
}
// === A method which returns an hours and minuets from seconds ===
function secondsTimeSpanToHMS(s) {
    var h = Math.floor(s/3600);
    s -= h*3600;
    var m = Math.floor(s/60);
    s -= m*60;
    return h+"h "+(m < 10 ? '0'+m : m) +"m"; 
}

        
    </script>
   
    
            
            
            
