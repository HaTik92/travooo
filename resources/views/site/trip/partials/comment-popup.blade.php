 <!-- comment popup -->
    <div class="modal fade white-style" data-backdrop="false" id="commentPopup{{$trip_place->places_id}}" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
            <i class="trav-close-icon"></i>
        </button>
        <div class="modal-dialog modal-custom-style modal-650" role="document">
            <div class="modal-custom-block">
                <div class="post-block post-modal-comment">
                    <div class="post-comment-head">
                        <div class="c-head-txt-wrap">
                            <div class="c-head-txt">
                                <div class="dest-name">
                                    <span><?php echo @\App\Models\Place\Place::find($trip_place->places_id)->trans[0]->title;?></span>
                                    <span class="block-tag"><?php echo @str_replace("_", " ", @explode(",", \App\Models\Place\Place::find($trip_place->places_id)->place_type)[0]);?></span></div>
                                <div class="dest-story">
                                    <b>@lang('time.day_value', ['value' => $i-1])</b> / {{count($trip_places)}} <span
                                            class="dot">&nbsp;·&nbsp;</span>
                                    {{date("D, j M Y", strtotime($trip_place->date))}}<span class="dot">&nbsp;·&nbsp;</span>@ {{date("h:i a", strtotime($trip_place->time))}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-comment-main">
                        <div class="dest-pic pt-2 post-txt" style="font-size: 18px;">
                            @if($trip_place->trip_description)
                                {!!$trip_place->trip_description->description!!}
                            @else
                                <p class="post-txt">
                                     “No Description”
                                </p>
                            @endif
                        </div>
                        <div class="desc-info">
                            @if($trip_place->budget>0)<i class="trav-budget-icon"></i><span class="info-block"> @lang('trip.will_spend') <b>${{ $trip_place->budget }}</b></span>@endif
                            @if($trip_place->duration>0)<i class="trav-clock-icon"></i><span class="info-block"> @lang('trip.planning_to_stay') <b>{{ calculate_duration($trip_place->places_id, 'place')}}</b></span>@endif
                        </div>
                        @if(count($trip_place->trip_comments)>0)
                            <div class="discussion-block">
                                <span class="disc-ttl">Comments</span>
                                <span class="disc-count">{{count($trip_place->trip_comments)}}</span>
                                <ul class="disc-ava-list">
                                    @foreach($trip_place->trip_comments()->orderBy('created_at', 'desc')->groupBy('users_id')->get() as $k=>$com)
                                        @if($k < 5)
                                            <li><a href="#"><img src="{{check_profile_picture($com->author->profile_picture)}}" alt="avatar"></a></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="post-add-comment-block">
                        <div class="avatar-wrap">
                            <img src="{{check_profile_picture(Auth::guard('user')->user()->profile_picture)}}" alt=""
                                 style="width:45px;height:45px;">
                        </div>
                        <div class="post-add-com-input">
                            <form method='post' class='tripAddCommentForm' id="{{$trip_place->places_id}}">
                                <textarea name="comment" class="comment-text" placeholder="@lang('comment.write_a_comment')" onkeyup="auto_grow(this)" style=" min-height: 50px;max-height: 150px;padding:8px;display:inline;resize: none;border: 1px solid #dcdcdc;"></textarea>
                                <input type="hidden" name="trip_id" value="{{$trip_info->id}}"/>
                                <input type="hidden" name="place_id" value="{{$trip_place->places_id}}"/>
                                <input type="hidden" name="description_id" value="{{($trip_place->trip_description)?$trip_place->trip_description->id:''}}"/>
                                <input type="submit" name="submit" value="send" style="cursor: pointer;display:inline;width:100px;vertical-align: top;height:40px;"/>
                            </form>
                        </div>
                    </div>
                    <div class="post-comment-layer">
                        <div class="post-comment-top-info">
                            <ul class="comment-filter">
                                <li class="comment-filter-type" data-type="Top">@lang('other.top')</li>
                                <li class="comment-filter-type active" data-type="New">@lang('other.new')</li>
                            </ul>
                            <div class="comm-count-info">
                                <strong>0</strong> / <span class="{{$trip_place->places_id}}-all-comments-count">{{count($trip_place->trip_comments)}}</span>
                            </div>
                        </div>
                        <div class="post-comment-wrapper sortBody" id="{{$trip_place->places_id}}_post_comment">
                            @if(count($trip_place->trip_comments)>0)
                                @foreach($trip_place->trip_comments()->orderBy('created_at', 'desc')->get() as $trip_comment)
                                <div class="post-comment-row comment-content" topsort="{{count($trip_comment->updownvotes()->where('vote_type', 1)->get())}}" newsort="{{strtotime($trip_comment->created_at)}}">
                                    <div class="post-com-avatar-wrap">
                                        <img src="{{check_profile_picture($trip_comment->author->profile_picture)}}" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="{{url("profile/".$trip_comment->author->id)}}" class="comment-name">{{$trip_comment->author->name}}</a>
                                            <span class="com-time">{{diffForHumans($trip_comment->created_at)}}</span>
                                        </div>
                                        <div class="comment-txt">
                                            @php
                                                $showChar = 100;
                                                $ellipsestext = "...";
                                                $moretext = "see more";
                                                $lesstext = "see less";

                                                $content = $trip_comment->text;

                                                if(strlen($content) > $showChar) {

                                                    $c = substr($content, 0, $showChar);
                                                    $h = substr($content, $showChar, strlen($content) - $showChar);

                                                    $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                                                    $content = $html;
                                                }
                                            @endphp
                                            <p>{!!$content!!}</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <div class="tips-footer updownvote">
                                                    <a href="#" class="upvote-link trip-comment-vote up disabled" id="{{$trip_comment->id}}">
                                                        <span class="arrow-icon-wrap"><i class="trav-angle-up"></i></span>
                                                    </a>
                                                    <span class="upvote-{{$trip_comment->id}}" style="font-size:85%"><b>{{count($trip_comment->updownvotes()->where('vote_type', 1)->get())}}</b> Upvotes</span>
                                                    &nbsp;&nbsp;
                                                    <a href="#" class="upvote-link trip-comment-vote down disabled" id="{{$trip_comment->id}}">
                                                        <span class="arrow-icon-wrap"><i class="trav-angle-down"></i></span>
                                                    </a>
                                                    <span class="downvote-{{$trip_comment->id}}" style="font-size:85%"><b>{{count($trip_comment->updownvotes()->where('vote_type', 2)->get())}}</b> Downvotes</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                         
                            @else
                                <div class="comment-not-fund-info">
                                   <p>Not Comment for now...</p>
                                </div>
                            @endif
                        </div>
                         <!--<a  class="load-more-link">@lang('buttons.general.load_more_dots')</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>