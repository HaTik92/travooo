<?php
use App\Models\Posts\Checkins;
use App\Models\Place\Place;
?>
@if(isset($city))
<div class="modal fade white-style" data-backdrop="false" id="addPlacePopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-650" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-modal-add-place">
                <h3 class="place-title">
                    @if(isset($memory))
                        What was your next destination in
                    @else
                        @lang('trip.what_is_your_next_destination_in')
                    @endif
                    
                    <b>{{$city->transsingle->title}}</b>?</h3>
                <div class="search-block-wrap">
                    <div class="search-block-inner">
                        <div class="search-block">
                            <input type="text" class="" placeholder="@lang('buttons.general.search_dots')"
                                   id="placeSearchInput" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="suggestion-wrapper">
                    <p class="sugg-ttl">@lang('trip.suggestions')</p>
                    <div class="sugg-inner" id="place-sugg-inner">
                        @if(isset($place_suggestions))
                            @foreach($place_suggestions AS $ps)
                            <div class="suggestion-block">
                            <?php
                            $place = Place::find($ps->places_id);
                            ?>
                                <div class="img-wrap">
                                    <img src="{{check_place_photo($place)}}" alt="image" style="width:60px;height:60px;">
                                </div>
                                <div class="sugg-content">
                                    <div class="sugg-place">{{$place->transsingle->title}}</div>
                                    <div class="sugg-place-info">{{do_placetype($place->place_type)}}</div>
                                    <div class="com-star-block">
                                        <ul class="com-star-list">
                                            @for ($i = 0; $i < ceil($place->rating); $i++)
                                                <li><i class="trav-star-icon"></i></li>
                                            @endfor
                                        </ul>
                                        <span class="count">
                                            <b>{{$place->rating}}</b>
                                        </span>

                                    </div>
                                    @if(Checkins::where('place_id', $place->id)->get()->count())
                                         <div style='color:cadetblue;font-size:12px'>
                                            {{Checkins::where('place_id', $place->id)->get()->first()->post_checkin->post->author->name}}
                                             @lang('profile.checked_in_here')
                                            {{diffForHumans(Checkins::where('place_id', $place->id)->get()->first()->post_checkin->post->created_at)}}
                                             ,
                                             <a href="{{url('profile/'.Checkins::where('place_id', $place->id)->get()->first()->post_checkin->post->author->id)}}"
                                                target="_blank">@lang('profile.contact_him_now')</a> @lang('other.for_advice')
                                             .
                                         </div>
                                        @endif
                                </div>
                                <div class="sugg-btn-wrap">
                                    <button type="button" class="btn btn-light-primary btn-bordered doAddPlace"
                                            data-id='{{$place->id}}' {{(in_array($place->id, $existing_place_ids))?'disabled="disabled"':''}}><span>+</span> @lang('other.add')</button>
                                </div>
                            </div>
                            @endforeach
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif