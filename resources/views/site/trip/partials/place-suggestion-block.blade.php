<?php
use App\Models\Posts\Checkins;
?>
<div class="suggestion-block">
    <div class="img-wrap">
        <img src="{{check_place_photo($suggestion)}}" alt="image" style="width:60px;height:60px;">
    </div>
    <div class="sugg-content">
        <div class="sugg-place">{{$suggestion->transsingle->title}}</div>
        <div class="sugg-place-info">{{do_placetype($suggestion->place_type)}}</div>
        <div class="com-star-block">
            <ul class="com-star-list">
                @for ($i = 0; $i < ceil($suggestion->rating); $i++)
                    <li><i class="trav-star-icon"></i></li>
                @endfor
            </ul>
            <span class="count">
                <b>{{$suggestion->rating}}</b>
            </span>
            
        </div>
        @if(Checkins::where('place_id', $suggestion->id)->get()->count())
             <div style='color:cadetblue;font-size:12px'>
                {{Checkins::where('place_id', $suggestion->id)->get()->first()->post_checkin->post->author->name}}
                 @lang('profile.checked_in_here')
                {{diffForHumans(Checkins::where('place_id', $suggestion->id)->get()->first()->post_checkin->post->created_at)}}
                 ,
                 <a href="{{url('profile/'.Checkins::where('place_id', $suggestion->id)->get()->first()->post_checkin->post->author->id)}}"
                    target="_blank">@lang('profile.contact_him_now')</a> @lang('other.for_advice').
             </div>
            @endif
    </div>
    <div class="sugg-btn-wrap">
        <button type="button" class="btn btn-light-primary btn-bordered doAddPlace" data-id='{{$suggestion->id}}' {{(in_array($suggestion->id, $selected_places))?'disabled="disabled"':''}}><span>+</span> @lang('other.add')
        </button>
    </div>
</div>