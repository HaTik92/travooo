  <header class="main-header">
        <div class="container-fluid">
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="trav-bars"></i>
                </button>
                <a class="navbar-brand" href="{{url('home')}}">
                    <img src="{{url('assets2/image/main-circle-logo.png')}}" alt="">
                    </a>
                     <a class="navbar-brand" href="javascript:;">
                    <span>Travooo</span>
                    <span class="blue">
                        @if(isset($memory))
                            Memory Trip
                        @else
                            @lang('trip.trip_planner')
                        @endif
                    </span>
                </a>

                <div class="collapse navbar-collapse create-navbar" id="navbarSupportedContent">
                    <form method="post" id="updateTripForm">
                        <ul class="navbar-nav create-menu">

                            @if(!$im_invited)
                                <li class="nav-item">
                                    <a class="nav-link confirm-trip-deletion"
                                       href="#">@lang('buttons.general.cancel')</a>
                                </li>
                            @else
                                <li class="nav-item" style='font-size:13px;'>
                                    <input type='checkbox' name='send_back' id='send_back' value='1'/>
                                    @lang('trip.send_your_version_back_to_name', ['name' => $me->name])
                                </li>
                            @endif


                            <li class="nav-item">
                                <a class="nav-link btn btn-light-primary" href="#"
                                   id="save_trip">@lang('trip.save_plan')</a>

                            </li>
                            <li class="nav-item dropdown">
                                <a class="profile-link" href="#" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                    <img src="{{check_profile_picture($me->profile_picture)}}" alt=""
                                         style="width:36px;height:36px;">
                                </a>
                                     <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-use-menu">
                                <a class="dropdown-item" href="{{url('profile')}}">
                                    <div class="drop-txt">
                                        <p>Profile</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="{{url('dashboard')}}">
                                    <div class="drop-txt">
                                        <p>My Dashboard</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="{{url('settings')}}">
                                    <div class="drop-txt">
                                        <p>Settings</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="{{url('activitylog')}}">
                                    <div class="drop-txt">
                                        <p>Activity Log</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="{{url('user/logout')}}">
                                    <div class="drop-txt">
                                        <p>Log Out</p>
                                    </div>
                                </a>
                            </div>
                            </li>
                        </ul>
                        <input type="hidden" name="trip_id" id="trip_id" value="{{$trip_id}}"/>
                        <input type="hidden" name="version_id" id="version_id" value="{{$my_version_id}}"/>
                    </form>
                </div>
            </nav>
        </div>
    </header>