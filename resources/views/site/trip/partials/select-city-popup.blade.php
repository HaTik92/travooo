<?php
use App\Models\Posts\Checkins;
?>
<div class="modal fade white-style" data-backdrop="false" id="addCityPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-650" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-modal-add-place">
                <h3 class="place-title">
                    @if(isset($memory))
                        Which city did you visit?
                    @else
                        @lang('trip.which_city_would_you_like_to_visit')
                    @endif
                </h3>
                <div class="search-block-wrap">
                    <div class="search-block-inner">
                        <div class="search-block">
                            <input type="text" class="" placeholder="@lang('buttons.general.search_dots')"
                                   id="citySearchInput" autocomplete="off">
                        </div>
                        
                    </div>
                </div>
                <div class="suggestion-wrapper">
                    <p class="sugg-ttl">@lang('trip.suggestions')</p>
                    <div class="sugg-inner" id="sugg-inner">
                        @if(isset($city_suggestions))
                        @foreach($city_suggestions AS $suggestion)
                        @if(isset($suggestion->city) && is_object($suggestion->city))
                        <div class="suggestion-block">
                            <div class="img-wrap">
                                <img src="{{check_city_photo(@$suggestion->city->getMedias[0]->url)}}" alt="image" style="width:60px;height:60px">
                            </div>
                            <div class="sugg-content">
                                <div class="sugg-place">{{@$suggestion->city->transsingle->title}}</div>
                                <div class="sugg-place-info">{{@$suggestion->city->country->transsingle->title}}</div>
                                <div class="com-star-block">
                                    @if(Checkins::where('city_id', $suggestion->city->id)->get()->count())
                                    <span style='color:cadetblue;'>
                                        {{Checkins::where('city_id', $suggestion->city->id)->get()->first()->post_checkin->post->author->name}}
                                        @lang('profile.checked_in_here')
                                        {{diffForHumans(Checkins::where('city_id', $suggestion->city->id)->get()->first()->post_checkin->post->created_at)}}
                                        , <a href="{{url('profile/'.Checkins::where('city_id', $suggestion->city->id)->get()->first()->post_checkin->post->author->id)}}"
                                             target="_blank">@lang('profile.contact_him_now')</a> @lang('other.for_advice').
                                    
                                    
                                    </span>
                                    @endif

                                    <!--<span class="count">
                                        <b>4.8</b>
                                    </span>
                                    <span>@lang('profile.from_count_reviews', ['count' => 26])</span>-->
                                </div>
                            </div>
                            <div class="sugg-btn-wrap">
                                <button type="button" class="btn btn-light-primary btn-bordered doAddCity"
                                        data-id='{{@$suggestion->city->id}}' data-tripid='{{@$trip->id}}' {{(in_array(@$suggestion->city->id, $existing_city_ids))?'disabled="disabled"':''}}>
                                    <span>+</span> @lang('other.add')</button>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @endif
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>