<?php

use \App\Models\Posts\Checkins;
?>
<div class="suggestion-block">
    <div class="img-wrap">
        <img src="{{check_city_photo(@$suggestion->getMedias[0]->url)}}" alt="image" style="width:60px;height:60px">
    </div>
    <div class="sugg-content">
        <div class="sugg-place">{{$suggestion->transsingle->title}}</div>
        <div class="sugg-place-info">{{$suggestion->country->transsingle->title}}</div>
        <div class="com-star-block">
            @if(Checkins::where('city_id', $suggestion->id)->get()->count())
             <span style='color:cadetblue;'>
                {{Checkins::where('city_id', $suggestion->id)->get()->first()->post_checkin->post->author->name}}
                 @lang('profile.checked_in_here')
                 {{diffForHumans(Checkins::where('city_id', $suggestion->id)->get()->first()->post_checkin->post->created_at)}}
                , <a href="{{url('profile/').Checkins::where('city_id', $suggestion->id)->get()->first()->post_checkin->post->author->id}}"
                     target="_blank">@lang('profile.contact_him_now')</a> @lang('other.for_advice').
             </span>
            @endif
            <!--<span class="count">
                <b>4.8</b>
            </span>
            <span>@lang('profile.from_count_reviews', ['count' => 26])</span>-->
        </div>
    </div>
    <div class="sugg-btn-wrap">
        <button type="button" class="btn btn-light-primary btn-bordered doAddCity" data-id='{{$suggestion->id}}'
                data-tripid='$trip_id' {{(in_array($suggestion->id, $selected_cities))?'disabled="disabled"':''}}><span>+</span> @lang('other.add')</button>
    </div>
</div>