<script>
    $(document).ready(function () {
    var image_element_flag = false;  
    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'], 
        // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'header': [3, 4, 5, 6, false] }],

        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'font': [] }],                                       // remove formatting button
    ];

    var quill = new Quill('#trip_description_text', {
            theme: 'snow',
            placeholder: 'Please enter your content here , it will be appearing just like this!',
            modules: {
                toolbar: toolbarOptions,

            }
    });
    quill.focus();
    
        $(".morelink").click(function(){
        var moretext = "see more";
        var lesstext = "see less";
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });

       $('#postTripDescription').submit(function(e) {
            $('.ql-editor').each(function (index, value) {
                var HTML = $(this).html();
                $(this).parent().siblings("input").each(function (index2, value2) {
                    $(this).val(HTML);
                });
            });
        });

    $(document).on('click', '#tripDescriptionBtn', function(){
        var place_id = $(this).data('place_id');
        var description_id = $(this).data('description_id');

        $('#places_id').val(place_id);
        if(description_id != ''){
            $.ajax({
                method: "POST",
                url: "{{url('trip/ajaxTripDescription')}}",
                data: {description_id: description_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.status == "success") {
                        $('#descriptions_id').val(description_id)
                    quill.root.innerHTML = result.description;
                    $('#image_video_content').html(result.result)
                         $('#tripDescriptionPopup').modal('show'); 
                    }
            });
        }else{
             $('#descriptions_id').val('')
             quill.root.innerHTML = '';
             $('#image_video_content').html('')
           $('#tripDescriptionPopup').modal('show'); 
        }
    })
    
    $(document).on('click', '#tripCommentPopup', function(){
        var place_id = $(this).data('place_id');
        Comment_Show.init($('#commentPopup'+place_id).find(".post-comment-layer"));
        $('#commentPopup'+place_id).modal('show')

    })
    
    $(document).on('click', '.comment-filter-type', function(){
        var filter_type = $(this).data('type');
        var reload_obj = $("#commentPopup{{$trip_place ? $trip_place->places_id : 0}}").find(".post-comment-layer");
        commentSort(filter_type, $(this), reload_obj);
    })
    
    $('body').on('submit', '.tripAddCommentForm', function (e) {
    var form = $(this);
    var values = form.serialize();
    var place_id = form.attr('id');
    var all_comment_count = $('.'+ place_id +'-all-comments-count').html();
    var comment_text = form.find('.comment-text');
    
    if(comment_text.val().trim() != ''){
        form.find('button[type=submit]').attr('disabled', true);
        $.ajax({
            method: "POST",
            url: "{{route('trip.add_comment')}}",
            data: values
        })
        .done(function (res) {
            var result = JSON.parse(res);
            form.find('button[type=submit]').removeAttr('disabled');
            
            $("#" + place_id + "_post_comment").prepend(result.trip_comment).fadeIn('slow');
            $("#" + place_id + "_new_comment").html(result.trip_comment);
            $("#" + place_id + "_post_comment .comment-not-fund-info").remove();
            $('.'+ place_id +'-all-comments-count').html(parseInt(all_comment_count) + 1);
            $('.'+ place_id +'-main__comment-count b').html(parseInt(all_comment_count) + 1);
            $('.'+ place_id +'-new-comment-info').html('1/' + (parseInt(all_comment_count) + 1));
            
            Comment_Show.reload($('#commentPopup'+place_id).find(".post-comment-layer"));
            comment_text.val('');
        });
    }
            e.preventDefault();
        });
        
    // Comments votes
    $('body').on('click', '.trip-comment-vote.up', function (e) {
      var comment_id = $(this).attr('id');
      $.ajax({
          method: "POST",
          url: "{{route('trip.comment_updownvote')}}",
          data: {comment_id: comment_id, vote_type:1}
      })
          .done(function (res) {
               var up_result = JSON.parse(res);
              $('.upvote-'+comment_id+' b').html(up_result.count_upvotes);
              $('.downvote-'+comment_id+' b').html(up_result.count_downvotes);
          });
      e.preventDefault()
  });

  $('body').on('click', '.trip-comment-vote.down', function (e) {
      var comment_id = $(this).attr('id');
      $.ajax({
          method: "POST",
          url: "{{route('trip.comment_updownvote')}}",
          data: {comment_id: comment_id, vote_type:2}
      })
          .done(function (res) {
              var down_result = JSON.parse(res);
              $('.upvote-'+comment_id+' b').html(down_result.count_upvotes);
              $('.downvote-'+comment_id+' b').html(down_result.count_downvotes);
          });
      e.preventDefault()
  }); 
  

$('#image_cont').click(function () {
    var num_images = $('.add_image_cont').length;
    var image_div_id = 'add_image_cont';
    var thisNum = parseInt(num_images)+1;
    image_div_id = 'add_image_cont'+(parseInt(thisNum));

        $('#image_video_content').append(`<div class="create-row ui-state-default add_image_cont" id="`+image_div_id+`" style="border:none;background:none;margin-top:20px;margin-bottom:20px;float:left;width:33%;">
    <div class="content">
        <input type="file" class="file-input tripDescImage" data-id="`+thisNum+`" id="tripDescImage`+thisNum+`" name="places_photo[]" style="display:none;">
                <img id="add_image_src`+thisNum+`" src="http://placehold.it/188x183" alt="image" width="188px" max-heght="183px" />
    </div>
    <div class="side right" style="left: 65%;top: 10px;">
        <ul class="video-upload-control">
            <li>
                <a href="#" onclick="document.getElementById('`+image_div_id+`').remove();">
                    <div class="close-handle red">
                        <i class="trav-close-icon"></i>
                    </div>
                </a>
            </li>
        </ul>
    </div>
                </div>`);

        if(image_element_flag === true)
        {
            document.getElementById('tripDescImage'+thisNum).files = drag_files;
            $('#tripDescImage'+thisNum).change(); 
            image_element_flag = false;
            drag_files = null;
        }
        else
        {
            $('#tripDescImage'+thisNum).click(); 

            document.body.onfocus = () => {
                setTimeout(_=>{
                    let file_input = document.getElementById('tripDescImage'+thisNum);
                    if (!file_input.value) $("#"+image_div_id).remove();
                    document.body.onfocus = null
                },100)
            }
        }

});

        $(document).on('change', '.tripDescImage', function(e) {
            //$("#message").empty(); // To remove the previous error message
            var file = this.files[0];
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var thisNum = $(this).attr('data-id');
            if(!this.files.length){
                $("#add_image_cont" + thisNum).remove();
                return;
            }

            if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
            {
                $("#message2").show();
                $("#message2").html("<p style='color:red'>Please Select A valid Image File</p>");
                $("#add_image_cont" + thisNum).remove();
                return false;
            } else
            {
                var reader = new FileReader();
                reader.onload = function (e) {
                                    $('#add_image_src'+thisNum).attr('src', e.target.result);
                                };

                reader.readAsDataURL(this.files[0]);
                $("#message2").hide();
            }
        });


        $('body').on('click', '.btnInviteFriend', function (e) {
            var user_id = $(this).attr('data-id');
            var trip_id = {{$trip_info->id}};

            $(this).html('Unselect');
            $(this).removeClass("btnInviteFriend");
            $(this).removeClass("btn-light-grey");
            $(this).addClass("btn-light-primary");
            $(this).addClass("btnUninviteFriend");

            $("#people-ava-list").append("<li id='user_" + user_id + "'><a href='#'><img src='{{check_profile_picture_id(1)}}' alt='avatar' style='width:50px;height:50px'></a><input type='hidden' name='user_id[]' value='" + user_id + "' /></li>");

            e.preventDefault()
        });

        $('body').on('click', '.btnUninviteFriend', function (e) {
            var user_id = $(this).attr('data-id');
            var trip_id = {{$trip_info->id}};

            $(this).html('Select');
            $(this).addClass("btnInviteFriend");
            $(this).addClass("btn-light-grey");
            $(this).removeClass("btn-light-primary");
            $(this).removeClass("btnUninviteFriend");

            $("#user_" + user_id).remove();


            e.preventDefault()
        });

        $('body').on('submit', '#InviteFriendsForm', function (e) {
            var trip_id = $("#trip_id").val();
            var values = $(this).serialize();
            //console.log(values);
            $.ajax({
                method: "POST",
                url: "{{url('trip/ajaxInviteFriends')}}",
                data: values
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.status == "success") {
                        console.log(result);
                        $('#invitePopup').modal('toggle');
                        alert(result.message);
                    }


                });
            e.preventDefault();
        });
    });
    
    function tripsDescLike(trip_id, place_id, obj)
      {
          $.ajax({
              url: "{{route('trip.likeunlike')}}",
              type: "POST",
              data: {id: trip_id, place_id:place_id},
              dataType: "json",
              success: function(data, status){
                 $(obj).find('b').html(data.count);
              },
              error: function(){}
          });
      }

    
function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
}
</script>