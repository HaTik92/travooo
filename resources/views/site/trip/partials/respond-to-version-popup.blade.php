<!-- invite popup -->
        <div class="modal fade white-style" data-backdrop="false" id="respondToVersionPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                <i class="trav-close-icon"></i>
            </button>
            <div class="modal-dialog modal-custom-style modal-700" role="document">
                <div class="modal-custom-block">
                    <div class="post-block post-going-block post-mobile-full">
                        <div class="post-top-layer">
                            <div class="post-top-info">
                                <h3 class="info-title">{{$trip_info->title}}</h3>
                                
                            </div>
                            <div class="post-photo-wrap">
                                
                            </div>
                        </div>
                        <div class="post-sub-layer">
                            @lang('trip.respond_to_suggestions_by_name', ['name' => $version->author->name])
                        </div>
                        <form method="post" action="{{url_with_locale('trip/version-respond')}}">
                        <div class="post-people-block-wrap mCustomScrollbar">
                            <div class="people-row">
                                <ul>
                                    <li style="padding:5px;"><input type="radio" name="action"
                                                                    value="accept"/> @lang('trip.accept_suggestions')
                                    </li>
                                    <li style="padding:5px;"><input type="radio" name="action"
                                                                    value="reject"/> @lang('trip.reject_suggestions')
                                    </li>
                                </ul>
                            </div>
                            
                        </div>
                        
                        
                        <div class="post-btn-foot">
                            <input type="hidden" name="version_id" value="{{$version_id}}" />
                            <input type="hidden" name="trip_id" value="{{$trip_info->id}}" />
                            <button type="button"
                                    class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                            <button type="submit" name="submit"
                                    class="btn btn-light-primary btn-bordered">@lang('buttons.general.save')</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>