@php
    $title = 'Trip Plan';
@endphp

@extends('site.trip.template.trip')

@section('before_site_style')
    @parent
    <link rel="stylesheet" type="text/css" href="{{asset('assets2/css/zabuto_calendar.min.css')}}">
    <link href="https://cdn.quilljs.com/1.2.6/quill.snow.css" rel="stylesheet">
@endsection

@section('content')
    <header class="main-header trip-plan-class">
        @include('site.layouts._header')

        <div class="head-trip-plan" id="headerTripPlan">
            <div class="head-trip-plan_wrapper">
                <div class="container-fluid">
                    <div class="head-trip-plan_inner">
                        <div class="plan-by-name">
                            <div class="ava-by-wrap">
                                <img src="{{check_profile_picture($trip_info->author->profile_picture)}}" alt="avatar"
                                     style="width:36px;height:36px">
                            </div>
                            <div class="plan-name">@lang('other.by') <a
                                        href="{{(Illuminate\Support\Facades\Auth::guard('user')->user()->id && $trip_info->author->id == Illuminate\Support\Facades\Auth::guard('user')->user()->id)?url('profile/'): url('profile/'.$trip_info->author->id)}}"
                                        class="name-link">{{$trip_info->author->name}}</a>
                                {!! get_exp_icon($trip_info->author) !!}
                            </div>
                        </div>
                        <div class="plan-title">
                            {{$trip_info->title}}
                        </div>
                        <div class="plan-btn-wrap">
                            @if(Illuminate\Support\Facades\Auth::guard('user')->user()->id &&  ($trip_info->author->id != Illuminate\Support\Facades\Auth::guard('user')->user()->id))
                                <a class="nav-link btn btn-light-primary" href="#">{{ __('Use plan')}}</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="head-trip-plan_trip-line">
                <div class="container-fluid">
                    <div class="trip-line" id="tripLineSlider">
                        <?php
                        $get_trip_cities = App\Models\TripCities\TripCities::where('trips_id', $trip_info->id)
                            ->where('versions_id', $version_id)
                            ->orderBy('id', 'ASC')
                            ->get();
                        $all_cities = array();
                        ?>
                        @foreach($get_trip_cities AS $gtc)
                            <?php

                            $country_id = \App\Models\City\Cities::find($gtc->cities_id)->countries_id;
                            $country = \App\Models\Country\Countries::find($country_id);
                            if (!isset($old_country) || $country->id != $old_country) {
                                $show_country_block = true;
                            } else {
                                $show_country_block = false;
                            }
                            $old_country = $country->id;


                            ?>

                            @if($show_country_block)
                                <div class="trip-line_slide">
                                    <div class="trip-line_slide__inner">
                                        <div class="trip-icon">
                                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($country->iso_code)}}.png"
                                                 alt="flag-icon" style="width:32px;height:32px;">
                                        </div>
                                        <div class="trip-content">
                                            <div class="trip-line-name">{{$country->trans[0]->title}}</div>
                                            <div class="trip-line-tag">
                                                <span class="place-tag">
                                                    <?php
                                                    echo @App\Models\TripCities\TripCities::leftJoin('cities', 'trips_cities.cities_id', '=', 'cities.id')
                                                        ->where('versions_id', $version_id)
                                                        ->where('trips_cities.trips_id', $trip_info->id)
                                                        ->where('cities.countries_id', $country->id)
                                                        ->count();
                                                    ?>
                                                    @lang('region.cities')
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="trip-line_slide">
                                <div class="trip-line_slide__inner">
                                    <div class="trip-icon blue-icon">
                                        <i class="trav-set-location-icon"></i>
                                    </div>
                                    <div class="trip-content">
                                        <div class="trip-line-name blue">
                                            {{\App\Models\City\Cities::find($gtc->cities_id)->trans[0]->title}}
                                        </div>
                                        <div class="trip-line-tag">
                                                <span class="place-tag">
                                                <?php
                                                    echo @App\Models\TripPlaces\TripPlaces::where('trips_id', $trip_info->id)
                                                        ->where('versions_id', $version_id)
                                                        ->where('cities_id', $gtc->cities_id)
                                                        ->count();
                                                    ?>
                                                    {{ __('places')}}
                                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </header>

    <div class="content-wrap">

        <button class="btn btn-mobile-side trip-sidebar-toggler" id="tripSidebarToggler">
            <i class="trav-cog"></i>
        </button>

        <div class="container-fluid">

            <div class="bottom-fixed-layer">
                @if(Illuminate\Support\Facades\Auth::guard('user')->user()->id &&  ($trip_info->author->id == Illuminate\Support\Facades\Auth::guard('user')->user()->id))
                    <button class="btn btn-light-primary" data-toggle="modal" data-target="#invitePopup">
                        <i class="trav-user-plus-icon"></i>
                        <span>{{ __('Invite to your trip')}}</span>
                    </button>
                    @if(count($trip_info->contribution_requests()->where('status', 1)->get()))
                        <ul class="bottom-ava-list">
                            @foreach($trip_info->contribution_requests()->where('status', 1)->get() AS $crequest)
                                <li>
                                    <a href="{{url('profile/'.$crequest->user->id)}}">
                                        <img src="{{check_profile_picture($crequest->user->profile_picture)}}" alt="ava"
                                             style="width:24px;height:24px;">
                                    </a>
                                </li>
                            @endforeach

                            <li>
                                <a href="#">+2 {{ __('more')}}</a>
                            </li>
                        </ul>
                    @endif
                @else
                    @if(isset($version))
                        <button class="btn btn-light-primary" data-toggle="modal" data-target="#respondToVersionPopup">
                            <span>{{ __('This version was suggested by:')}} {{$version->author->name}}</span>
                        </button>
                    @endif
                @endif
            </div>
            <?php
            $budget = 0;
            $get_places = App\Models\TripPlaces\TripPlaces::where('trips_id', $trip_info->id)
                ->get();
            foreach ($get_places AS $gp) {
                $budget += $gp->budget;
            }
            ?>

            <div class="trip-map">
                <div class="trip-map_wrapper">
                    <img src="{{get_plan_map($trip_info, 700, 600, $version_id)}}" alt="map">
                </div>
                <div class="trip-map_top-layer">
                    <div class="trip-map-left">
                        <div class="trip-map-info">
                            <div class="trip-map-info_block">
                                <div class="trip-icon-wrap">
                                    <i class="trav-clock-icon"></i>
                                </div>
                                <div class="trip-info-txt">
                                    <p class="label-txt">{{calculate_duration($trip_info->id, 'all')}}</p>
                                    <p class="info-txt">@lang('trip.duration')</p>
                                </div>
                            </div>
                            <div class="trip-map-info_block">
                                <div class="trip-icon-wrap">
                                    <i class="trav-budget-icon"></i>
                                </div>
                                <div class="trip-info-txt">
                                    <p class="label-txt">{{$budget}}$</p>
                                    <p class="info-txt">@lang('trip.budget')</p>
                                </div>
                            </div>
                            <div class="trip-map-info_block">
                                <div class="trip-icon-wrap">
                                    <i class="trav-distance-icon"></i>
                                </div>
                                <div class="trip-info-txt">
                                    <p class="label-txt">{{calculate_distance($trip_info->id)}}</p>
                                    <p class="info-txt">@lang('trip.distance')</p>
                                </div>
                            </div>
                            <div class="trip-map-info_block">
                                <div class="trip-icon-wrap">
                                    <i class="trav-map-marker-icon"></i>
                                </div>
                                <div class="trip-info-txt">
                                    <p class="label-txt">{{@count($trip_info->places)}}</p>
                                    <p class="info-txt">@choice('trip.place', 2)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="trip-map-right">
                        <div class="trip-map-right-inner">
                                    <span class="right-txt">
                                        <i class="trav-ok-icon"></i>
                                        {{@count($trip_info->users)}} {{ __('Used this plan')}}
                                    </span>
                            <ul class="trip-avatar-list">
                                @if(@count($trip_info->users))
                                    @foreach($trip_info->users AS $tiu)
                                        <li><img class="small-ava"
                                                 src="{{check_profile_picture($tiu->profile_picture)}}" alt="ava"
                                                 style='width:29px;height:29px;'></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="trip-destination-slider" id="tripDestSlider">
                    <?php
                    $get_countries = App\Models\TripCities\TripCities::leftJoin('cities', 'trips_cities.cities_id', '=', 'cities.id')
                        ->leftJoin('countries', 'cities.countries_id', '=', 'countries.id')
                        ->leftJoin('countries_trans', 'countries_trans.countries_id', '=', 'countries.id')
                        ->where('trips_cities.trips_id', $trip_info->id)
                        ->select(array('countries.id', 'countries.iso_code', 'countries_trans.title'))
                        ->get();

                    $countries = array();
                    foreach ($get_countries AS $gc) {

                        $countries[$gc->id] = array(
                            'id'           => $gc->id,
                            'iso_code'     => strtolower($gc->iso_code),
                            'title'        => $gc->title,
                            'destinations' => count($trip_info->places)
                        );
                    }
                    ?>
                    @foreach($countries AS $cnt)
                        <div class="trip-dest-slide">
                            <div class="trip-dest-slide_inner">
                                <div class="trip-dest-flag">
                                    <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{$cnt['iso_code']}}.png"
                                         alt="flag" style="width:67px;height:68px;">
                                </div>
                                <div class="trip-dest-info">
                                    <p class="trip-dest-name">{{$cnt['title']}}</p>
                                    <p class="trip-dest-count">
                                        <?php
                                        echo @App\Models\TripCities\TripCities::leftJoin('cities', 'trips_cities.cities_id', '=', 'cities.id')
                                            ->where('versions_id', $version_id)
                                            ->where('trips_cities.trips_id', $trip_info->id)
                                            ->where('cities.countries_id', $cnt['id'])
                                            ->count();
                                        ?>

                                        @choice('trip.destination', 2)
                                    </p>
                                </div>
                                <div class="trip-plane-info">
                                    <!--<i class="trav-plane-icon"></i>
                                        <span>Fly - 5,250 km</span>-->
                                </div>
                            </div>
                        </div>
                    @endforeach


                </div>
            </div>

            <div class="custom-row">

                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">


                    <div class="imageGallaryWrap">
                    </div>

                    <div class="post-block post-top-bordered post-view-block">
                        <div class="post-top-info-layer">
                            <ul class="view-link">
                                <li class="active" style="cursor:pointer;">
                                    <a class="tablinks" onclick="openTab(event, 'overview')">@lang('trip.overview')</a>
                                </li>
                                <li style="cursor:pointer;">
                                    <a class="tablinks" onclick="openTab(event, 'details')">{{ __('Details')}}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="post-view-inner tabcontent" id="overview" style="display:block;">
                            <?php
                          //  dd($trip_places_info);
                            $i = 0;
                            ?>
                            @foreach($trip_places_info AS $tpi)

                                <?php
                                $i++;
                                $ii = 0;
                                $c = 2;
                                ?>
                                @foreach($tpi AS $t)
                                    <?php
                                    $ii++;
                                    ?>
                                    <div class="post-view-wrap">
                                        @if($ii==1)
                                            <div class="story-date-layer">
                                                <div class="s-date-line">
                                                    <div class="s-date-badge">{{ __('Day')}} {{$i}}/ {{$num_days}}</div>
                                                </div>
                                                <div class="s-date-line">
                                                    <div class="s-date-badge"><?php echo date("D, j M Y", strtotime($t->date)) ?></div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(isset($transportation[$c]) AND $transportation[$c]=='plane')
                                            <div class="view-img-layer airplane">
                                        @else
                                                    <div class="view-img-layer">
                                        @endif

                                                        <div class="view-img">
                                                            {{@$transportation[$t->cities_id]}}
                                                            <img src="{{check_place_photo(\App\Models\Place\Place::find($t->places_id))}}"
                                                                 alt="view" style="width:50px;height:50px;">
                                                            <span class="round-count">{{$ii}}</span>
                                                        </div>
                                                    </div>

                                                    <div class="view-content">
                                                        <div class="view-txt-line">
                                                            <div class="line-txt place-info">
                                                                <a href="{{url('place').'/'.$t->places_id}}"
                                                                   class="dest-link">
                                                                    <?php
                                                                    echo @\App\Models\Place\Place::find($t->places_id)->trans[0]->title;
                                                                    ?>
                                                                </a>
                                                                <span class="block-tag">
                                                    <?php
                                                                    echo @str_replace("_", " ", @explode(",", \App\Models\Place\Place::find($t->places_id)->place_type)[0]);
                                                                    ?>
                                                    </span>
                                                                <i class="trav-set-location-icon"></i>
                                                                <span class="place-name">
                                                        <a href='{{url("city/")."/".$t->cities_id}}'>
                                                    <?php
                                                            echo \App\Models\City\Cities::find($t->cities_id)->trans[0]->title;
                                                            ?>
                                                        </a>
                                                    </span>
                                                                <?php
                                                                $iso_code = strtolower(\App\Models\Country\Countries::find($t->countries_id)->iso_code);
                                                                ?>
                                                                <img class="flag-img"
                                                                     src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{$iso_code}}.png"
                                                                     alt="flag" style="width:24px;height:17px;">
                                                                <span class="place-name">
                                                        <a href='{{url("country/")."/".$t->countries_id}}'>
                                                    <?php
                                                            echo \App\Models\Country\Countries::find($t->countries_id)->trans[0]->title;
                                                            ?>
                                                        </a>
                                                    </span>
                                                            </div>
                                                            <div class="line-time">
                                                                @ {{ $t->time }}
                                                            </div>
                                                        </div>
                                                        <div class="view-txt-line">
                                                            <div class="line-txt">
                                                                @if($t->budget>0) @lang('trip.will_spend')
                                                                <b>${{ $t->budget }}</b>@endif
                                                                @if($t->duration>0)
                                                                    <span class="dot">·</span>
                                                                    @lang('trip.planning_to_stay')
                                                                    <b>{{ calculate_duration($t->places_id, 'place')}}</b>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <?php
                                                        // check if one of friends checked in to this city or place before
                                                        $friend_checkin = false;
                                                        $friend_checkin = \App\Models\Posts\Posts::with('checkin')
                                                            ->whereHas('checkin', function ($q) use ($t) {
                                                                $q->where('city_id', $t->cities_id);
                                                                $q->orWhere('place_id', $t->places_id);
                                                            })
                                                            ->get()->first();
                                                        ?>
                                                        @if($friend_checkin)
                                                            <div class="view-txt-line">

                                                                <div class="line-txt" style="font-size:14px">
                                                                    <a href="{{url('profile/'.$friend_checkin->author->id)}}"><b>{{$friend_checkin->author->name}}</b></a>
                                                                    {{ __('Checked in here')}} {{diffForHumans($friend_checkin->created_at)}}
                                                                    , {{ __('contact him now to get some suggestions.')}}

                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                            </div>
                                            <?php
                                            $c++;
                                            ?>
                                            @endforeach
                                            @endforeach
                                    </div>
                        </div>

                        <div class="post-preview-wrap tabcontent" id="details">
                            @php $i = 1; @endphp
                            @foreach($trip_places as $trip_place)
                            <div class="post-block post-preview-mode airplane">
                                <div class="story-date-layer">
                                    <div class="s-date-line">
                                        <div class="s-date-badge">@lang('time.day_value', ['value' => $i.'/'.count($trip_places)])</div>
                                    </div>
                                    <div class="s-date-line">
                                        <div class="s-date-badge">{{date("D, j M Y", strtotime($trip_place->date))}}</div>
                                    </div>
                                    <div class="s-date-line">
                                        <div class="s-date-badge">@ {{date("h:i a", strtotime($trip_place->time))}}</div>
                                    </div>
                                </div>

                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="{{check_place_photo(\App\Models\Place\Place::find($trip_place->places_id))}}"
                                            alt="view" style="width:50px !important;height:50px;">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-preview-txt">
                                                        <div>
                                                            <a class="dest-link" href="{{url('place').'/'.$trip_place->places_id}}">
                                                                <?php echo @\App\Models\Place\Place::find($trip_place->places_id)->trans[0]->title;?>
                                                            </a>
                                                            <span class="block-tag">
                                                                <?php echo @str_replace("_", " ", @explode(",", \App\Models\Place\Place::find($trip_place->places_id)->place_type)[0]);?>
                                                            </span>
                                                        </div>
                                                        <div style="padding:5px 0;width: 363px;">
                                                            <a href='{{url("city/")."/".$trip_place->cities_id}}' class="dest-link">
                                                                <?php $iso_code = strtolower(\App\Models\Country\Countries::find($trip_place->countries_id)->iso_code);?>
                                                                <img class="flag-img"
                                                                     src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{$iso_code}}.png"
                                                                     alt="flag" style="width:24px;height:17px;">

                                                                <?php echo \App\Models\City\Cities::find($trip_place->cities_id)->trans[0]->title;?>
                                                            </a>,
                                                            <a href='{{url("country/")."/".$trip_place->countries_id}}' class="dest-link">
                                                                    <?php echo \App\Models\Country\Countries::find($trip_place->countries_id)->trans[0]->title; ?>
                                                                </a>
                                                        </div>
                                                        <div class="line-txt">
                                                            @if($trip_place->budget>0) @lang('trip.spend') <b>${{ $trip_place->budget }}</b>, @endif
                                                            @if($trip_place->duration>0) @lang('trip.stayed') <b>{{ calculate_duration($trip_place->places_id, 'place')}}</b> @endif
                                                        </div>
                                            </div>
                                        </div>
                                        @if($trip_info->author->id == Auth::user()->id)
                                                <div class="post-top-info-txt">
                                                        <button type="button" class="btn btn-light-primary btn-bordered" data-place_id="{{$trip_place->places_id}}" data-description_id="{{($trip_place->trip_description)?$trip_place->trip_description->id:''}}" id="tripDescriptionBtn">
                                                            <i class="trav-add-trip-icon"></i>
                                                            @if($trip_place->trip_description)
                                                                Update Report
                                                            @else
                                                                Add Report
                                                            @endif
                                                        </button>
                                                </div>

                                        @endif
                                    </div>
                                </div>
                                <div class="post-image-container">
                                    <div class="post-txt-wrap">
                                        <div class="txt-name pull-left plan-by-name" style="padding: 10px 22px 10px 0;">
                                            <div class="ava-by-wrap text-center">
                                            <img src="{{check_profile_picture($trip_info->author->profile_picture)}}" alt="avatar"
                                                 style="width:45px;height:45px;border-radius: 50%;">
                                            </div>
                                            <div class="plan-name"><a
                                                        href="{{(Auth::user()->id && $trip_info->author->id == Auth::user()->id)?url('profile/'): url('profile/'.$trip_info->author->id)}}"
                                                        class="name-link" style="color: #4080ff;font-family: 'Circular Std Med';">{{$trip_info->author->name}}</a>
                                            </div>
                                        </div>
                                        @if($trip_place->trip_description)
                                        <div class="post-txt" style="font-size:18px">
                                                 {!!$trip_place->trip_description->description!!}
                                            </div>
                                        @else
                                        <p class="post-txt" style="padding-bottom: 61px;border-bottom: 1px solid #e6e6e6;margin-bottom: 0;">
                                                 “No Description”
                                            </p>
                                        @endif
                                    </div>
                                     @if($trip_place->trip_description && $trip_place->trip_description->images)
                                     <ul class="post-image-list" style="margin-top:21px;">
                                            @foreach(explode(',', $trip_place->trip_description->images) as $description_photo)
                                                <li>
                                                    <img src="{{$description_photo}}" alt="" style="max-width:188px;max-height: 183px;">
                                                </li>
                                            @endforeach
                                        </ul>
                                     @endif
                                </div>
                                <div class="post-footer-info">
                                    <div class="post-foot-block post-reaction" onclick="tripsDescLike({{ $trip_info->id }}, {{ $trip_place->places_id }}, this)" style="cursor:pointer;">
                                        <i class="trav-heart-fill-icon"></i>
                                        <span><b>{{count($trip_place->trip_description_likes)}}</b> @lang('other.reactions')</span>
                                    </div>
                                    <div class="post-foot-block add-comment-btn" data-place_id="{{$trip_place->places_id}}" id="tripCommentPopup" style="cursor:pointer;">
                                        <i class="trav-comment-icon"></i>
                                        <span class="{{$trip_place->places_id}}-main__comment-count"><b>{{count($trip_place->trip_comments)}}</b> @lang('comment.comments')</span>
                                    </div>
<!--                                    <div class="post-foot-block">
                                        <i class="trav-light-icon"></i>
                                        <span><b>7</b> @lang('trip.tips')</span>
                                    </div>-->
                                </div>
                                @if(count($trip_place->trip_comments)>0)
                                @php $new_comment = $trip_place->trip_comments()->orderBy('created_at', 'desc')->get(); @endphp
                                <div class="post-comment-layer">
                                    <div class="post-comment-top-info">
                                        <ul class="comment-filter">
                                        </ul>
                                        <div class="comm-count-info {{$trip_place->places_id}}-new-comment-info">
                                            {{count($trip_place->trip_comments)>0?'1/'.count($trip_place->trip_comments):''}}
                                        </div>
                                    </div>
                                    <div class="post-comment-wrapper" id="{{$trip_place->places_id}}_new_comment">
                                        <div class="post-comment-row">
                                            <div class="post-com-avatar-wrap">
                                                <img src="{{check_profile_picture($new_comment[0]->author->profile_picture)}}" alt="">
                                            </div>
                                            <div class="post-comment-text">
                                                <div class="post-com-name-layer">
                                                    <a href="{{url("profile/".$new_comment[0]->author->id)}}" class="comment-name">{{$new_comment[0]->author->name}}</a>
                                                     <span class="com-time">{{diffForHumans($new_comment[0]->created_at)}}</span>
                                                </div>
                                                <div class="comment-txt">
                                                    @php
                                                        $showChar = 100;
                                                        $ellipsestext = "...";
                                                        $moretext = "see more";
                                                        $lesstext = "see less";

                                                        $content = $new_comment[0]->text;

                                                        if(strlen($content) > $showChar) {

                                                            $c = substr($content, 0, $showChar);
                                                            $h = substr($content, $showChar, strlen($content) - $showChar);

                                                            $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                                                            $content = $html;
                                                        }
                                                    @endphp
                                                    <p>{!!$content!!}</p>
                                                </div>
                                                <div class="comment-bottom-info">
                                                    <div class="com-reaction">
                                                        <div class="tips-footer updownvote">
                                                            <a href="#" class="upvote-link trip-comment-vote up disabled" id="{{$new_comment[0]->id}}">
                                                                <span class="arrow-icon-wrap"><i class="trav-angle-up"></i></span>
                                                            </a>
                                                            <span class="upvote-{{$new_comment[0]->id}}" style="font-size:85%"><b>{{count($new_comment[0]->updownvotes()->where('vote_type', 1)->get())}}</b> Upvotes</span>
                                                            &nbsp;&nbsp;
                                                            <a href="#" class="upvote-link trip-comment-vote down disabled" id="{{$new_comment[0]->id}}">
                                                                <span class="arrow-icon-wrap"><i class="trav-angle-down"></i></span>
                                                            </a>
                                                            <span class="downvote-{{$new_comment[0]->id}}" style="font-size:85%"><b>{{count($new_comment[0]->updownvotes()->where('vote_type', 2)->get())}}</b> Downvotes</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="post-comment-layer">
                                    <div class="post-comment-top-info">
                                        <ul class="comment-filter">
                                        </ul>
                                        <div class="comm-count-info {{$trip_place->places_id}}-new-comment-info">
                                        </div>
                                    </div>
                                    <div class="post-comment-wrapper" id="{{$trip_place->places_id}}_new_comment">
                                    </div>
                                </div>
                                @endif
                            </div>
                            @php $i++; @endphp
                            @include('site/trip/partials/comment-popup')
                            @endforeach
                        </div>
                    </div>

                    <!-- SIDEBAR -->
                    <div class="sidebar-layer trip-side" id="sidebarLayer">
                        <aside class="sidebar">

                            <div class="post-block post-side-block">
                                <div class="post-side-top">
                                    <h3 class="side-ttl">@choice('travelmate.travel_mate', 2)</h3>
                                </div>
                                <div class="post-suggest-inner">

                                    <div class="suggest-txt">
                                        <p>{{ __('Looking for someone to take along?')}}</p>
                                    </div>
                                    <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                                            onclick="location.href = '{{url('travelmates-newest')}}';">
                                        {{ __('Find Travel Mate')}}
                                    </button>
                                </div>
                            </div>

                            <div class="post-block post-side-block">
                                <div class="post-side-top timeline-top">
                                    <h3 class="side-ttl">{{ __('Calendar')}}</h3>
                                    <div class="side-right-control">
                                        <!--<a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                                            <span class="month-name">Jun</span>
                                            <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>-->
                                    </div>
                                </div>
                                <div class="post-calendar-inner">
                                    <div class="post-calendar">


                                        <!-- define the calendar element -->
                                        <div id="tripCalendar"></div>


                                    </div>
                                </div>
                            </div>


                            <ul class="sidebar-btn-list">
                                <li>
                                    <a href="#">
                                        <div class="round-icon">
                                            <i class="trav-story-icon"></i>
                                        </div>
                                        <span>{{ __('Story')}}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="round-icon">
                                            <i class="trav-map"></i>
                                        </div>
                                        <span>{{ __('Map')}}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="round-icon">
                                            <i class="trav-gallery-icon"></i>
                                        </div>
                                        <span>{{ __('Gallery')}}</span>
                                    </a>
                                </li>
                            </ul>


                            @include('site.layouts._sidebar')
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('before_scripts')
    <!-- modals -->
     <!-- description popup -->
<div class="modal fade white-style" data-backdrop="false" id="tripDescriptionPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                <form method='post' id="postTripDescription" action="{{url('/trip/post-trip-description')}}" enctype='multipart/form-data' autocomplete="off">
                    <div class="top-title-layer">
                        <h3 class="title">
                            <span class="txt">Trip Place Description</span>
                        </h3>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="travlog-main-block">
                        <div class="main-content" style="width:80%">
                            <div class="create-row">
                                <div class="content">
                                    <div id="message2"></div>
                                    <ul class="action-list">
                                        <li><a href="#" title="Insert Image" id="image_cont" class="pull-left"><i class="trav-camera"></i></a> <span class="pull-left pl-2" style="padding-top:12px">Add Photo</span></li>
                                    </ul>
                                </div>
                            </div>

                            <div id="sortable">
                                <div class="content">
                                    <div class="" id="trip_description_text">
                                    </div>
                                    <input type="hidden" name="description" id="" />
                                </div>
                                <div id="image_video_content" style="display:flow-root;">
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="post-foot-btn">
                        <input type="hidden" name="trips_id"  value='{{$trip_info->id}}' />
                        <input type="hidden" name="place_id" id="places_id" value="" />
                        <input type="hidden" name="description_id" id="descriptions_id" value="" />
                        <button class="btn btn-transp btn-clear" type="cancel" data-dismiss="modal" aria-label="Close">Cancel</button>
                        <button class="btn btn-light-primary btn-bordered" type="submit" onclick="javascript:$('#step2_flag').val('1');">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    @include('site/trip/partials/invite-friends-popup')
    @if(isset($version))
        @include('site/trip/partials/respond-to-version-popup')
    @endif
@endsection

@section('after_scripts')
    <script src="{{asset('assets2/js/zabuto_calendar.min.js')}}"></script>
    <script src="https://cdn.quilljs.com/1.2.6/quill.min.js"></script>
    <!-- initialize the calendar on ready -->
    <script type="application/javascript">

        $(document).ready(function () {
            var eventData = [
                    @foreach($trip_places_info AS $ctpi)
                {
                    "date": "{{$ctpi[0]->date}}", "badge": false, "title": "{{$ctpi[0]->city->transsingle->title}}"
                },
                @endforeach

            ];
            $("#tripCalendar").zabuto_calendar({
                data: eventData,
                today: false,
                show_days: false,
                nav_icon: {
                    prev: '<i class="fa fa-chevron-circle-left"></i>',
                    next: '<i class="fa fa-chevron-circle-right"></i>'
                }
            });
        });
    </script>
 @include('site.trip.partials._scripts')
    <style type="text/css">
        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        .post-txt p{
            margin-bottom: 0 !important;
        }

        .dest-pic p{
            line-height: 1.5;
        }

        /* Style the buttons that are used to open the tab content */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }

        .tabcontent {
            animation: fadeEffect 1s; /* Fading effect takes 1 second */
        }

        .com-time{
            float: right;
            color: #b2b2b2;
        }

        .comment-content{
            padding-top: 10px;
            border-bottom: 1px solid #e6e6e6;
        }

        .add-comment-btn:hover{
            border: 1px solid #6e96e4;
            border-radius: 3px;
            padding: 0px 5px 0px 3px;
        }

        .trip-comment-vote:hover, .trip-comment-vote:active, .trip-comment-vote:focus{
            text-decoration: none !important;
        }

        /* Go from zero to full opacity */
        @keyframes fadeEffect {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }
    </style>

    <script type="text/javascript">
        function openTab(evt, tabName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
//                tablinks[i].className = tablinks[i].parentElement.className.replace(" active", "");
                tablinks[i].parentElement.className = tablinks[i].parentElement.className.replace("active", "");
            }

            // Show the current tab, and add an "active" class to the button that opened the tab
            document.getElementById(tabName).style.display = "block";
          //  evt.currentTarget.className += " active";
            evt.currentTarget.parentElement.className = "active";
        }

    </script>
@endsection
