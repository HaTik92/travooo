<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets2/css/zabuto_calendar.min.css')}}">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>{{ __('Travooo - trip plan')}}</title>
</head>
<body>
<div class="main-wrapper">
    <header class="main-header trip-plan-class">
        @include('site.layouts.header')
        <div class="head-trip-plan" id="headerTripPlan">
            <div class="head-trip-plan_wrapper">
                <div class="container-fluid">
                    <div class="head-trip-plan_inner">
                        <div class="plan-by-name">
                            <div class="ava-by-wrap">
                                <img src="{{check_profile_picture($trip_info->author->profile_picture)}}" alt="avatar"
                                     style="width:36px;height:36px">
                            </div>
                            <div class="plan-name">@lang('other.by') <a
                                        href="{{url('profile/'.$trip_info->author->id)}}"
                                        class="name-link">{{$trip_info->author->name}}</a>
                            </div>
                        </div>
                        <div class="plan-title">
                            {{$trip_info->title}}
                        </div>
                        <div class="plan-btn-wrap">
                            @if(Illuminate\Support\Facades\Auth::guard('user')->user()->id &&  ($trip_info->author->id != Illuminate\Support\Facades\Auth::guard('user')->user()->id))
                                <a class="nav-link btn btn-light-primary" href="#">{{ __('Use plan')}}</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="head-trip-plan_trip-line">
                <div class="container-fluid">
                    <div class="trip-line" id="tripLineSlider">
                        <?php
                        $get_trip_cities = App\Models\TripCities\TripCities::where('trips_id', $trip_info->id)
                            ->where('versions_id', $version_id)
                            ->orderBy('id', 'ASC')
                            ->get();
                        $all_cities = array();
                        ?>
                        @foreach($get_trip_cities AS $gtc)
                            <?php

                            $country_id = \App\Models\City\Cities::find($gtc->cities_id)->countries_id;
                            $country = \App\Models\Country\Countries::find($country_id);
                            if (!isset($old_country) || $country->id != $old_country) {
                                $show_country_block = true;
                            } else {
                                $show_country_block = false;
                            }
                            $old_country = $country->id;


                            ?>

                            @if($show_country_block)
                                <div class="trip-line_slide">
                                    <div class="trip-line_slide__inner">
                                        <div class="trip-icon">
                                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($country->iso_code)}}.png"
                                                 alt="flag-icon" style="width:32px;height:32px;">
                                        </div>
                                        <div class="trip-content">
                                            <div class="trip-line-name">{{$country->trans[0]->title}}</div>
                                            <div class="trip-line-tag">
                                                <span class="place-tag">
                                                    <?php
                                                    echo @App\Models\TripCities\TripCities::leftJoin('cities', 'trips_cities.cities_id', '=', 'cities.id')
                                                        ->where('versions_id', $version_id)
                                                        ->where('trips_cities.trips_id', $trip_info->id)
                                                        ->where('cities.countries_id', $country->id)
                                                        ->count();
                                                    ?>
                                                    cities
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="trip-line_slide">
                                <div class="trip-line_slide__inner">
                                    <div class="trip-icon blue-icon">
                                        <i class="trav-set-location-icon"></i>
                                    </div>
                                    <div class="trip-content">
                                        <div class="trip-line-name blue">
                                            {{\App\Models\City\Cities::find($gtc->cities_id)->trans[0]->title}}
                                        </div>
                                        <div class="trip-line-tag">
                                                <span class="place-tag">
                                                <?php
                                                    echo @App\Models\TripPlaces\TripPlaces::where('trips_id', $trip_info->id)
                                                        ->where('versions_id', $version_id)
                                                        ->where('cities_id', $gtc->cities_id)
                                                        ->count();
                                                    ?>
                                                    {{ __('places')}}
                                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </header>

    <div class="content-wrap">

        <button class="btn btn-mobile-side trip-sidebar-toggler" id="tripSidebarToggler">
            <i class="trav-cog"></i>
        </button>

        <div class="container-fluid">

            <div class="bottom-fixed-layer">
                @if(Illuminate\Support\Facades\Auth::guard('user')->user()->id &&  ($trip_info->author->id == Illuminate\Support\Facades\Auth::guard('user')->user()->id))
                    <button class="btn btn-light-primary" data-toggle="modal" data-target="#invitePopup">
                        <i class="trav-user-plus-icon"></i>
                        <span>{{ __('Invite to your trip')}}</span>
                    </button>
                    @if(count($trip_info->contribution_requests()->where('status', 1)->get()))
                        <ul class="bottom-ava-list">
                            @foreach($trip_info->contribution_requests()->where('status', 1)->get() AS $crequest)
                                <li>
                                    <a href="{{url('profile/'.$crequest->user->id)}}">
                                        <img src="{{check_profile_picture($crequest->user->profile_picture)}}" alt="ava"
                                             style="width:24px;height:24px;">
                                    </a>
                                </li>
                            @endforeach

                            <li>
                                <a href="#">+2 {{ __('more')}}</a>
                            </li>
                        </ul>
                    @endif
                @else
                    @if(isset($version))
                        <button class="btn btn-light-primary" data-toggle="modal" data-target="#respondToVersionPopup">
                            <span>{{ __('This version was suggested by:')}} {{$version->author->name}}</span>
                        </button>
                    @endif
                @endif
            </div>
            <?php
            $budget = 0;
            $get_places = App\Models\TripPlaces\TripPlaces::where('trips_id', $trip_info->id)
                ->get();
            foreach ($get_places AS $gp) {
                $budget += $gp->budget;
            }
            ?>

            <div class="trip-map">
                <div class="trip-map_wrapper">
                    <img src="{{get_plan_map($trip_info, 700, 600, $version_id)}}" alt="map">
                </div>
                <div class="trip-map_top-layer">
                    <div class="trip-map-left">
                        <div class="trip-map-info">
                            <div class="trip-map-info_block">
                                <div class="trip-icon-wrap">
                                    <i class="trav-clock-icon"></i>
                                </div>
                                <div class="trip-info-txt">
                                    <p class="label-txt">{{dateDiffDays($trip_info->myversion->first()->start_date, $trip_info->myversion->first()->end_date)}}</p>
                                    <p class="info-txt">{{ __('Duration')}}</p>
                                </div>
                            </div>
                            <div class="trip-map-info_block">
                                <div class="trip-icon-wrap">
                                    <i class="trav-budget-icon"></i>
                                </div>
                                <div class="trip-info-txt">
                                    <p class="label-txt">{{$budget}}$</p>
                                    <p class="info-txt">@lang('trip.budget')</p>
                                </div>
                            </div>
                            <div class="trip-map-info_block">
                                <div class="trip-icon-wrap">
                                    <i class="trav-distance-icon"></i>
                                </div>
                                <div class="trip-info-txt">
                                    <p class="label-txt">{{$trip_info->distance>0 ? $trip_info->distance : 'n/a'}}</p>
                                    <p class="info-txt">@lang('trip.distance')</p>
                                </div>
                            </div>
                            <div class="trip-map-info_block">
                                <div class="trip-icon-wrap">
                                    <i class="trav-map-marker-icon"></i>
                                </div>
                                <div class="trip-info-txt">
                                    <p class="label-txt">{{@count($trip_info->places)}}</p>
                                    <p class="info-txt">@choice('trip.place', 2)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="trip-map-right">
                        <div class="trip-map-right-inner">
                                    <span class="right-txt">
                                        <i class="trav-ok-icon"></i>
                                        {{@count($trip_info->users)}} {{ __('Used this plan')}}
                                    </span>
                            <ul class="trip-avatar-list">
                                @if(@count($trip_info->users))
                                    @foreach($trip_info->users AS $tiu)
                                        <li><img class="small-ava"
                                                 src="{{check_profile_picture($tiu->profile_picture)}}" alt="ava"
                                                 style='width:29px;height:29px;'></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="trip-destination-slider" id="tripDestSlider">
                    <?php
                    $get_countries = App\Models\TripCities\TripCities::leftJoin('cities', 'trips_cities.cities_id', '=', 'cities.id')
                        ->leftJoin('countries', 'cities.countries_id', '=', 'countries.id')
                        ->leftJoin('countries_trans', 'countries_trans.countries_id', '=', 'countries.id')
                        ->where('trips_cities.trips_id', $trip_info->id)
                        ->select(array('countries.id', 'countries.iso_code', 'countries_trans.title'))
                        ->get();

                    $countries = array();
                    foreach ($get_countries AS $gc) {

                        $countries[$gc->id] = array(
                            'id'           => $gc->id,
                            'iso_code'     => strtolower($gc->iso_code),
                            'title'        => $gc->title,
                            'destinations' => count($trip_info->places)
                        );
                    }
                    ?>
                    @foreach($countries AS $cnt)
                        <div class="trip-dest-slide">
                            <div class="trip-dest-slide_inner">
                                <div class="trip-dest-flag">
                                    <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{$cnt['iso_code']}}.png"
                                         alt="flag" style="width:67px;height:68px;">
                                </div>
                                <div class="trip-dest-info">
                                    <p class="trip-dest-name">{{$cnt['title']}}</p>
                                    <p class="trip-dest-count">
                                        <?php
                                        echo @App\Models\TripCities\TripCities::leftJoin('cities', 'trips_cities.cities_id', '=', 'cities.id')
                                            ->where('versions_id', $version_id)
                                            ->where('trips_cities.trips_id', $trip_info->id)
                                            ->where('cities.countries_id', $cnt['id'])
                                            ->count();
                                        ?>
                                        {{ __('Destinations')}}</p>
                                </div>
                                <div class="trip-plane-info">
                                    <!--<i class="trav-plane-icon"></i>
                                        <span>Fly - 5,250 km</span>-->
                                </div>
                            </div>
                        </div>
                    @endforeach


                </div>
            </div>

            <div class="custom-row">

                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">


                    <div class="imageGallaryWrap">

                        <!-- <ul id="lightgallery" class="light-gallery-block">
                                  <li data-src="https://sachinchoolur.github.io/lightGallery/static/img/1-1600.jpg">
                                    <a href="">
                                      <img class="img-responsive" src="https://sachinchoolur.github.io/lightGallery/static/img/thumb-1.jpg">
                                    </a>
                                  </li>
                                  <li data-src="https://sachinchoolur.github.io/lightGallery/static/img/2-1600.jpg">
                                    <a href="">
                                      <img class="img-responsive" src="https://sachinchoolur.github.io/lightGallery/static/img/thumb-2.jpg">
                                    </a>
                                  </li>
                                  <li data-src="https://sachinchoolur.github.io/lightGallery/static/img/13-1600.jpg">
                                    <a href="">
                                      <img class="img-responsive" src="https://sachinchoolur.github.io/lightGallery/static/img/thumb-13.jpg">
                                    </a>
                                  </li>
                                  <li data-src="https://sachinchoolur.github.io/lightGallery/static/img/4-1600.jpg">
                                    <a href="">
                                      <img class="img-responsive" src="https://sachinchoolur.github.io/lightGallery/static/img/thumb-4.jpg">
                                    </a>
                                  </li>
                                </ul> -->
                    </div>

                    <div class="post-block post-top-bordered post-view-block">
                        <div class="post-top-info-layer">
                            <ul class="view-link">
                                <li class="active">
                                    <a class="tablinks" onclick="openTab(event, 'overview')">@lang('trip.overview')</a>
                                </li>
                                <li>
                                    <a class="tablinks" onclick="openTab(event, 'details')">{{ __('Details')}}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="post-view-inner tabcontent" id="overview">
                            <?php
                            $i = 0;
                            ?>
                            @foreach($trip_places_info AS $tpi)

                                <?php
                                $i++;
                                $ii = 0;
                                $c = 2;
                                ?>
                                @foreach($tpi AS $t)
                                    <?php
                                    $ii++;
                                    ?>
                                    <div class="post-view-wrap">
                                        @if($ii==1)
                                            <div class="story-date-layer">
                                                <div class="s-date-line">
                                                    <div class="s-date-badge">{{ __('Day')}} {{$i}}/ {{$num_days}}</div>
                                                </div>
                                                <div class="s-date-line">
                                                    <div class="s-date-badge"><?php echo date("D, j M Y", strtotime($t->date)) ?></div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(isset($transportation[$c]) AND $transportation[$c]=='plane')
                                            <div class="view-img-layer airplane">
                                                @else
                                                    <div class="view-img-layer">
                                                        @endif

                                                        <div class="view-img">
                                                            {{@$transportation[$t->cities_id]}}
                                                            <img src="{{check_place_photo(\App\Models\Place\Place::find($t->places_id))}}"
                                                                 alt="view" style="width:50px;height:50px;">
                                                            <span class="round-count">{{$ii}}</span>
                                                        </div>
                                                    </div>

                                                    <div class="view-content">
                                                        <div class="view-txt-line">
                                                            <div class="line-txt place-info">
                                                                <a href="{{url('place').'/'.$t->places_id}}"
                                                                   class="dest-link">
                                                                    <?php
                                                                    echo @\App\Models\Place\Place::find($t->places_id)->trans[0]->title;
                                                                    ?>
                                                                </a>
                                                                <span class="block-tag">
                                                    <?php
                                                                    echo @str_replace("_", " ", @explode(",", \App\Models\Place\Place::find($t->places_id)->place_type)[0]);
                                                                    ?>
                                                    </span>
                                                                <i class="trav-set-location-icon"></i>
                                                                <span class="place-name">
                                                        <a href='{{url("city/")."/".$t->cities_id}}'>
                                                    <?php
                                                            echo \App\Models\City\Cities::find($t->cities_id)->trans[0]->title;
                                                            ?>
                                                        </a>
                                                    </span>
                                                                <?php
                                                                $iso_code = strtolower(\App\Models\Country\Countries::find($t->countries_id)->iso_code);
                                                                ?>
                                                                <img class="flag-img"
                                                                     src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{$iso_code}}.png"
                                                                     alt="flag" style="width:24px;height:17px;">
                                                                <span class="place-name">
                                                        <a href='{{url("country/")."/".$t->countries_id}}'>
                                                    <?php
                                                            echo \App\Models\Country\Countries::find($t->countries_id)->trans[0]->title;
                                                            ?>
                                                        </a>
                                                    </span>
                                                            </div>
                                                            <div class="line-time">
                                                                @ {{ $t->time }}
                                                            </div>
                                                        </div>
                                                        <div class="view-txt-line">
                                                            <div class="line-txt">
                                                                @if($t->budget>0) @lang('trip.will_spend')
                                                                <b>${{ $t->budget }}</b>@endif
                                                                @if($t->duration>0)
                                                                    <span class="dot">·</span>
                                                                    @lang('trip.planning_to_stay')
                                                                    <b><?php echo date("H", $t->duration); ?> {{ __('hours')}} <?php echo date("i", $t->duration); ?> {{ __('minutes')}}</b>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <?php
                                                        // check if one of friends checked in to this city or place before
                                                        $friend_checkin = false;
                                                        $friend_checkin = \App\Models\Posts\Posts::with('checkin')
                                                            ->whereHas('checkin', function ($q) use ($t) {
                                                                $q->where('city_id', $t->cities_id);
                                                                $q->orWhere('place_id', $t->places_id);
                                                            })
                                                            ->get()->first();
                                                        ?>
                                                        @if($friend_checkin)
                                                            <div class="view-txt-line">

                                                                <div class="line-txt" style="font-size:14px">
                                                                    <a href="{{url('profile/'.$friend_checkin->author->id)}}"><b>{{$friend_checkin->author->name}}</b></a>
                                                                    {{ __('Checked in here')}} {{diffForHumans($friend_checkin->created_at)}}
                                                                    , {{ __('contact him now to get some suggestions.')}}

                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                            </div>
                                            <?php
                                            $c++;
                                            ?>
                                            @endforeach
                                            @endforeach
                                    </div>
                        </div>

                        <div class="post-preview-wrap tabcontent" id="details">

                            <div class="post-block post-preview-mode airplane">
                                <div class="story-date-layer">
                                    <div class="s-date-line">
                                        <div class="s-date-badge">@lang('time.day_value', ['value' => '1 / 7'])</div>
                                    </div>
                                    <div class="s-date-line">
                                        <div class="s-date-badge">Mon, 18 Jun 2017</div>
                                    </div>
                                    <div class="s-date-line">
                                        <div class="s-date-badge">@ 10:00am</div>
                                    </div>
                                </div>

                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/50x50" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-preview-txt">
                                                <a class="dest-link" href="#">Rabat sale airport</a>
                                                <span class="block-tag">Airport</span>
                                                <div class="line-txt">
                                                    @lang('trip.spend') <b>$240</b>,
                                                    @lang('trip.stayed') <b>@lang('time.count_min', ['count' => 40])</b>
                                                </div>
                                                <a href="#" class="place-name underline">Rabat, Morocco</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-image-container">
                                    <div class="post-txt-wrap">
                                        <p class="post-txt"><span class="txt-name">Suzanne: </span>“Lorem ipsum dolor
                                            sit amet, consectetur adipiscing elit nunc porta eu leo a porttitor integer
                                            lectus ligula, egestas non vitae, dignissim placerat purus volutpat varius
                                            blandit...” <a href="#" class="read-more-link">Read more</a></p>
                                    </div>
                                    <ul class="post-image-list">
                                        <li>
                                            <img src="http://placehold.it/188x183" alt="">
                                        </li>
                                        <li>
                                            <img src="http://placehold.it/188x183" alt="">
                                        </li>
                                        <li>
                                            <img src="http://placehold.it/188x183" alt="">
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-footer-info">
                                    <div class="post-foot-block post-reaction">
                                        <i class="trav-heart-fill-icon"></i>
                                        <span><b>6</b> @choice('other.reaction', 6, ['count' => 6])</span>
                                    </div>
                                    <div class="post-foot-block">
                                        <i class="trav-comment-icon"></i>
                                        <span><b>20</b> @lang('comment.comments')</span>
                                    </div>
                                    <div class="post-foot-block">
                                        <i class="trav-light-icon"></i>
                                        <span><b>7</b> @lang('trip.tips')</span>
                                    </div>
                                </div>

                                <div class="post-comment-layer">
                                    <div class="post-comment-top-info">
                                        <ul class="comment-filter">
                                            <li class="active">@lang('other.top')</li>
                                            <li>@lang('other.new')</li>
                                        </ul>
                                        <div class="comm-count-info">
                                            3 / 20
                                        </div>
                                    </div>
                                    <div class="post-comment-wrapper">
                                        <div class="post-comment-row">
                                            <div class="post-com-avatar-wrap">
                                                <img src="http://placehold.it/45x45" alt="">
                                            </div>
                                            <div class="post-comment-text">
                                                <div class="post-com-name-layer">
                                                    <a href="#" class="comment-name">Katherin</a>
                                                    <a href="#" class="comment-nickname">@katherin</a>
                                                </div>
                                                <div class="comment-txt">
                                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                        doloribus labore tenetur vel. Neque molestiae repellat culpa qui
                                                        odit delectus.</p>
                                                </div>
                                                <div class="comment-bottom-info">
                                                    <div class="com-reaction">
                                                        <img src="./assets/image/icon-smile.png" alt="">
                                                        <span>21</span>
                                                    </div>
                                                    <div class="com-time">6 hours ago</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-block post-preview-mode overland-trip">
                                <div class="story-date-layer">
                                    <div class="s-date-line">
                                        <div class="s-date-badge">@lang('time.day_value', ['value' => '1 / 7'])</div>
                                    </div>
                                    <div class="s-date-line">
                                        <div class="s-date-badge">Mon, 18 Jun 2017</div>
                                    </div>
                                    <div class="s-date-line">
                                        <div class="s-date-badge">@ 10:00am</div>
                                    </div>
                                </div>

                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/50x50" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-preview-txt">
                                                <a class="dest-link" href="#">Dubai international airport</a>
                                                <span class="block-tag">Airport</span>
                                                <div class="line-txt">
                                                    @lang('trip.spend') <b>$240</b>,
                                                    @lang('trip.stayed') <b>@lang('time.count_min', ['count' => 40])</b>
                                                </div>
                                                <a href="#" class="place-name underline">Dubai, United arab emirates</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-image-container">
                                    <div class="post-txt-wrap">
                                        <p class="post-txt"><span class="txt-name">Suzanne: </span>“Lorem ipsum dolor
                                            sit amet, consectetur adipiscing elit nunc porta eu leo a porttitor integer
                                            lectus ligula, egestas non vitae, dignissim placerat purus volutpat varius
                                            blandit...” <a href="#" class="read-more-link">Read more</a></p>
                                    </div>
                                    <ul class="post-image-list">
                                        <li>
                                            <img src="http://placehold.it/188x183" alt="">
                                        </li>
                                        <li>
                                            <img src="http://placehold.it/188x183" alt="">
                                        </li>
                                        <li>
                                            <img src="http://placehold.it/188x183" alt="">
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-footer-info">
                                    <div class="post-foot-block post-reaction">
                                        <i class="trav-heart-fill-icon"></i>
                                        <span><b>6</b> @choice('other.reaction', 6, ['count' => 6])</span>
                                    </div>
                                    <div class="post-foot-block">
                                        <i class="trav-comment-icon"></i>
                                        <span><b>20</b> @lang('comment.comments')</span>
                                    </div>
                                    <div class="post-foot-block">
                                        <i class="trav-light-icon"></i>
                                        <span><b>7</b> @lang('trip.tips')</span>
                                    </div>
                                </div>

                                <div class="post-comment-layer">
                                    <div class="post-comment-top-info">
                                        <ul class="comment-filter">
                                            <li class="active">@lang('other.top')</li>
                                            <li>@lang('other.new')</li>
                                        </ul>
                                        <div class="comm-count-info">
                                            3 / 20
                                        </div>
                                    </div>
                                    <div class="post-comment-wrapper">
                                        <div class="post-comment-row">
                                            <div class="post-com-avatar-wrap">
                                                <img src="http://placehold.it/45x45" alt="">
                                            </div>
                                            <div class="post-comment-text">
                                                <div class="post-com-name-layer">
                                                    <a href="#" class="comment-name">Katherin</a>
                                                    <a href="#" class="comment-nickname">@katherin</a>
                                                </div>
                                                <div class="comment-txt">
                                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                        doloribus labore tenetur vel. Neque molestiae repellat culpa qui
                                                        odit delectus.</p>
                                                </div>
                                                <div class="comment-bottom-info">
                                                    <div class="com-reaction">
                                                        <img src="./assets/image/icon-smile.png" alt="">
                                                        <span>21</span>
                                                    </div>
                                                    <div class="com-time">6 hours ago</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-block post-preview-mode">
                                <div class="story-date-layer">
                                    <div class="s-date-line">
                                        <div class="s-date-badge">@lang('time.day_value', ['value' => '1 / 7'])</div>
                                    </div>
                                    <div class="s-date-line">
                                        <div class="s-date-badge">Mon, 18 Jun 2017</div>
                                    </div>
                                    <div class="s-date-line">
                                        <div class="s-date-badge">@ 10:00am</div>
                                    </div>
                                </div>

                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <div class="post-top-avatar-wrap">
                                            <img src="http://placehold.it/50x50" alt="">
                                        </div>
                                        <div class="post-top-info-txt">
                                            <div class="post-preview-txt">
                                                <a class="dest-link" href="#">Dubai international airport</a>
                                                <span class="block-tag">Airport</span>
                                                <div class="line-txt">
                                                    @lang('trip.spend') <b>$240</b>,
                                                    @lang('trip.stayed') <b>@lang('time.count_min', ['count' => 40])</b>
                                                </div>
                                                <a href="#" class="place-name underline">Dubai, United arab emirates</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-image-container">
                                    <div class="post-txt-wrap">
                                        <p class="post-txt"><span class="txt-name">Suzanne: </span>“Lorem ipsum dolor
                                            sit amet, consectetur adipiscing elit nunc porta eu leo a porttitor integer
                                            lectus ligula, egestas non vitae, dignissim placerat purus volutpat varius
                                            blandit...” <a href="#" class="read-more-link">Read more</a></p>
                                    </div>
                                    <ul class="post-image-list">
                                        <li>
                                            <img src="http://placehold.it/188x183" alt="">
                                        </li>
                                        <li>
                                            <img src="http://placehold.it/188x183" alt="">
                                        </li>
                                        <li>
                                            <img src="http://placehold.it/188x183" alt="">
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-footer-info">
                                    <div class="post-foot-block post-reaction">
                                        <i class="trav-heart-fill-icon"></i>
                                        <span><b>6</b> @choice('other.reaction', 6, ['count' => 6])</span>
                                    </div>
                                    <div class="post-foot-block">
                                        <i class="trav-comment-icon"></i>
                                        <span><b>20</b> @lang('comment.comments')</span>
                                    </div>
                                    <div class="post-foot-block">
                                        <i class="trav-light-icon"></i>
                                        <span><b>7</b> @lang('trip.tips')</span>
                                    </div>
                                </div>

                                <div class="post-comment-layer">
                                    <div class="post-comment-top-info">
                                        <ul class="comment-filter">
                                            <li class="active">@lang('other.top')</li>
                                            <li>@lang('other.new')</li>
                                        </ul>
                                        <div class="comm-count-info">
                                            3 / 20
                                        </div>
                                    </div>
                                    <div class="post-comment-wrapper">
                                        <div class="post-comment-row">
                                            <div class="post-com-avatar-wrap">
                                                <img src="http://placehold.it/45x45" alt="">
                                            </div>
                                            <div class="post-comment-text">
                                                <div class="post-com-name-layer">
                                                    <a href="#" class="comment-name">Katherin</a>
                                                    <a href="#" class="comment-nickname">@katherin</a>
                                                </div>
                                                <div class="comment-txt">
                                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                        doloribus labore tenetur vel. Neque molestiae repellat culpa qui
                                                        odit delectus.</p>
                                                </div>
                                                <div class="comment-bottom-info">
                                                    <div class="com-reaction">
                                                        <img src="./assets/image/icon-smile.png" alt="">
                                                        <span>21</span>
                                                    </div>
                                                    <div class="com-time">6 hours ago</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- SIDEBAR -->
                    <div class="sidebar-layer trip-side" id="sidebarLayer">
                        <aside class="sidebar">

                            <div class="post-block post-side-block">
                                <div class="post-side-top">
                                    <h3 class="side-ttl">@choice('travelmate.travel_mate', 2)</h3>
                                </div>
                                <div class="post-suggest-inner">

                                    <div class="suggest-txt">
                                        <p>{{ __('Looking for someone to take along?')}}</p>
                                    </div>
                                    <button type="button" class="btn btn-light-primary btn-bordered btn-full"
                                            onclick="location.href = '{{url('travelmates-newest')}}';">
                                        {{ __('Find Travel Mate')}}
                                    </button>
                                </div>
                            </div>

                            <div class="post-block post-side-block">
                                <div class="post-side-top timeline-top">
                                    <h3 class="side-ttl">{{ __('Calendar')}}</h3>
                                    <div class="side-right-control">
                                        <!--<a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                                            <span class="month-name">Jun</span>
                                            <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>-->
                                    </div>
                                </div>
                                <div class="post-calendar-inner">
                                    <div class="post-calendar">


                                        <!-- define the calendar element -->
                                        <div id="tripCalendar"></div>


                                    </div>
                                </div>
                            </div>


                            <ul class="sidebar-btn-list">
                                <li>
                                    <a href="#">
                                        <div class="round-icon">
                                            <i class="trav-story-icon"></i>
                                        </div>
                                        <span>{{ __('Story')}}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="round-icon">
                                            <i class="trav-map"></i>
                                        </div>
                                        <span>{{ __('Map')}}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="round-icon">
                                            <i class="trav-gallery-icon"></i>
                                        </div>
                                        <span>{{ __('Gallery')}}</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="aside-footer">
                                <ul class="aside-foot-menu">
                                    <li><a href="#">{{ __('Privacy')}}</a></li>
                                    <li><a href="#">{{ __('Terms')}}</a></li>
                                    <li><a href="#">{{ __('Advertising')}}</a></li>
                                    <li><a href="#">{{ __('Cookies')}}</a></li>
                                    <li><a href="#">{{ __('More')}}</a></li>
                                </ul>
                                <p class="copyright">{{ __('Travooo © 2017')}}</p>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modals -->

    <!-- story mode popup -->
    <div class="modal fade white-style" data-backdrop="false" id="chatPopup" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">

        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
            <i class="trav-close-icon"></i>
        </button>

        <div class="modal-dialog modal-custom-style modal-1420" role="document">
            <div class="modal-custom-block">
                <div class="post-block post-invite-chat">
                    <div class="head-trip-plan">
                        <div class="head-trip-plan_wrapper">
                            <div class="head-trip-plan_inner">
                                <div class="plan-by-name">
                                    <div class="ava-by-wrap">
                                        <img src="http://placehold.it/36x36" alt="avatar">
                                    </div>
                                    <div class="plan-name">@lang('other.by') <a href="#" class="name-link">Suzanne</a>
                                    </div>
                                </div>
                                <div class="plan-title">
                                    From Morocco to Japan in 7 Days
                                </div>
                                <div class="plan-btn-wrap">
                                    <ul class="head-ava-list">
                                        <li>
                                            <a href="#">
                                                <img src="http://placehold.it/24x24" alt="ava">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="http://placehold.it/24x24" alt="ava">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="http://placehold.it/24x24" alt="ava">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="http://placehold.it/24x24" alt="ava">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="http://placehold.it/24x24" alt="ava">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">@lang('other.count_more', ['count' => '+2'])</a>
                                        </li>
                                    </ul>
                                    <a class="btn btn-light-primary btn-bordered" href="#">
                                        <i class="trav-user-plus-icon"></i>&nbsp;
                                        <span>@lang('profile.invite_people')</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chat-wrapper">
                        <div class="view-layer mCustomScrollbar">
                            <div class="post-block post-top-bordered post-view-block">
                                <div class="post-top-info-layer">
                                    <ul class="view-link">
                                        <li class="active">
                                            <a href="#">@lang('trip.overview')</a>
                                        </li>
                                        <li>
                                            <a href="#">Suggested Places 3</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-view-inner">
                                    <div class="post-view-wrap">
                                        <div class="story-date-layer">
                                            <div class="s-date-line">
                                                <div class="s-date-badge">@lang('time.day_value', ['value' => '1 / 7'])</div>
                                            </div>
                                            <div class="s-date-line">
                                                <div class="s-date-badge">Mon, 18 Jun 2017</div>
                                            </div>
                                        </div>

                                        <div class="view-img-layer airplane">
                                            <div class="view-img">
                                                <img src="http://placehold.it/50x50" alt="view">
                                                <span class="round-count">1</span>
                                            </div>
                                        </div>
                                        <div class="view-content">
                                            <div class="view-txt-line">
                                                <div class="line-txt place-info">
                                                    <a href="#" class="dest-link">Rabat sale airport</a>
                                                    <span class="block-tag">Airport</span>
                                                    <i class="trav-set-location-icon"></i>
                                                    <span class="place-name">Rabat</span>
                                                    <img class="flag-img" src="http://placehold.it/24x17" alt="flag">
                                                    <span class="place-name">Morocco</span>
                                                </div>
                                                <div class="line-time">
                                                    @ 10:00am
                                                </div>
                                            </div>
                                            <div class="view-txt-line">
                                                <div class="line-txt">
                                                    @lang('trip.will_spend') <b>$240</b>
                                                    <span class="dot">·</span>
                                                    @lang('trip.planning_to_stay')
                                                    <b>@lang('time.count_min', ['count' => 40])</b>
                                                </div>
                                            </div>
                                            <div class="view-txt-line">
                                                <div class="line-txt">
                                                    <b>@lang('profile.have_something_to_tell_name', ['name' => 'Suzanne'])</b>
                                                    @lang('profile.discussed_by')
                                                    <ul class="foot-avatar-list">
                                                        <li><img class="small-ava" src="http://placehold.it/20x20"
                                                                 alt="ava"></li><!--
                                                            -->
                                                        <li><img class="small-ava" src="http://placehold.it/20x20"
                                                                 alt="ava"></li><!--
                                                            -->
                                                        <li><img class="small-ava" src="http://placehold.it/20x20"
                                                                 alt="ava"></li>
                                                    </ul>
                                                    @lang('profile.count_users', ['count' => 128])
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-view-wrap">
                                        <div class="view-img-layer airplane">
                                            <div class="view-img">
                                                <img src="http://placehold.it/50x50" alt="view">
                                                <span class="round-count">2</span>
                                            </div>
                                        </div>
                                        <div class="view-content">
                                            <div class="view-txt-line">
                                                <div class="line-txt place-info">
                                                    <a href="#" class="dest-link">Dubai international airport</a>
                                                    <span class="block-tag">Airport</span>
                                                    <i class="trav-set-location-icon"></i>
                                                    <span class="place-name">Dubai</span>
                                                    <img class="flag-img" src="http://placehold.it/24x17" alt="flag">
                                                    <span class="place-name">United arab emirates</span>
                                                </div>
                                                <div class="line-time">
                                                    @ 3:00pm
                                                </div>
                                            </div>
                                            <div class="view-txt-line">
                                                <div class="line-txt">
                                                    @lang('trip.will_spend') <b>$510</b>
                                                    <span class="dot">·</span>
                                                    @lang('trip.planning_to_stay')
                                                    <b>@choice('time.count_hours', 2, ['count' => 2])</b>
                                                </div>
                                            </div>
                                            <div class="view-txt-line">
                                                <div class="line-txt">
                                                    <b>@lang('profile.have_something_to_tell_name', ['name' => 'Suzanne'])</b>
                                                    @lang('profile.discussed_by')
                                                    <ul class="foot-avatar-list">
                                                        <li><img class="small-ava" src="http://placehold.it/20x20"
                                                                 alt="ava"></li><!--
                                                            -->
                                                        <li><img class="small-ava" src="http://placehold.it/20x20"
                                                                 alt="ava"></li><!--
                                                            -->
                                                        <li><img class="small-ava" src="http://placehold.it/20x20"
                                                                 alt="ava"></li>
                                                    </ul>
                                                    @lang('profile.count_users', ['count' => 128])
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-view-wrap current">
                                        <div class="story-date-layer">
                                            <div class="s-date-line">
                                                <div class="s-date-badge">@lang('time.day_value', ['value' => '2 / 7'])</div>
                                            </div>
                                            <div class="s-date-line">
                                                <div class="s-date-badge">Mon, 19 Jun 2017</div>
                                            </div>
                                        </div>

                                        <div class="view-img-layer overland-trip">
                                            <div class="view-img">
                                                <img src="http://placehold.it/50x50" alt="view">
                                                <span class="round-count">3</span>
                                            </div>
                                        </div>
                                        <div class="view-content">
                                            <div class="view-txt-line">
                                                <div class="line-txt place-info">
                                                    <a href="#" class="dest-link">Haneda airport</a>
                                                    <span class="block-tag">Airport</span>
                                                    <i class="trav-set-location-icon"></i>
                                                    <span class="place-name">Tokyo</span>
                                                    <img class="flag-img" src="http://placehold.it/24x17" alt="flag">
                                                    <span class="place-name">Japan</span>
                                                </div>
                                                <div class="line-time">
                                                    @ 12:00am
                                                </div>
                                            </div>
                                            <div class="view-txt-line">
                                                <div class="line-txt">
                                                    @lang('trip.will_spend') <b>$240</b>
                                                    <span class="dot">·</span>
                                                    @lang('trip.planning_to_stay')
                                                    <b>@lang('time.count_min', ['count' => 40])</b>
                                                </div>
                                                <div class="btn-wrap">
                                                    <button class="btn btn-success btn-bordered">@lang('buttons.general.approve')</button>
                                                </div>
                                            </div>
                                            <div class="view-txt-line">
                                                <div class="line-txt">
                                                    <b>@lang('profile.suggested_by')</b>&nbsp;
                                                    <ul class="foot-avatar-list">
                                                        <li><img class="small-ava" src="http://placehold.it/20x20"
                                                                 alt="ava"></li>
                                                    </ul>&nbsp;
                                                    <a href="#" class="name-link">Amine</a>
                                                </div>
                                                <div class="btn-wrap">
                                                    <button class="btn btn-trip-red btn-bordered">@lang('buttons.general.disapprove')</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-view-wrap">
                                        <div class="view-img-layer airplane">
                                            <div class="view-img">
                                                <img src="http://placehold.it/50x50" alt="view">
                                                <span class="round-count">4</span>
                                            </div>
                                        </div>
                                        <div class="view-content">
                                            <div class="view-txt-line">
                                                <div class="line-txt place-info">
                                                    <a href="#" class="dest-link">Bunka</a>
                                                    <span class="block-tag">Hotel</span>
                                                    <i class="trav-set-location-icon"></i>
                                                    <span class="place-name">Tokyo</span>
                                                    <img class="flag-img" src="http://placehold.it/24x17" alt="flag">
                                                    <span class="place-name">Japan</span>
                                                </div>
                                                <div class="line-time">
                                                    @ 10:00am
                                                </div>
                                            </div>
                                            <div class="view-txt-line">
                                                <div class="line-txt">
                                                    @lang('trip.will_spend') <b>$510</b>
                                                    <span class="dot">·</span>
                                                    @lang('trip.planning_to_stay')
                                                    <b>@choice('time.count_hours', 2, ['count' => 2])</b>
                                                </div>
                                            </div>
                                            <div class="view-txt-line">
                                                <div class="line-txt">
                                                    <b>@lang('profile.have_something_to_tell_name', ['name' => 'Suzanne'])</b>
                                                    @lang('profile.discussed_by')
                                                    <ul class="foot-avatar-list">
                                                        <li><img class="small-ava" src="http://placehold.it/20x20"
                                                                 alt="ava"></li><!--
                                                            -->
                                                        <li><img class="small-ava" src="http://placehold.it/20x20"
                                                                 alt="ava"></li><!--
                                                            -->
                                                        <li><img class="small-ava" src="http://placehold.it/20x20"
                                                                 alt="ava"></li>
                                                    </ul>
                                                    @lang('profile.count_users', ['count' => 128])
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="chat-layer">
                            <div class="chat-inner">
                                <div class="chat-ttl-layer">
                                    <h4 class="chat-ttl-messages">@lang('trip.trip_plan_messages')</h4>
                                </div>
                                <div class="chat-txt-block">
                                    <div class="chat-group-wrap mCustomScrollbar">
                                        <div class="chat-group">
                                            <div class="avatar-block">
                                                <img src="http://placehold.it/32x32" alt="avatar">
                                            </div>
                                            <div class="text-block">
                                                Hi, how are you going?
                                            </div>
                                        </div>
                                        <div class="chat-group right">
                                            <div class="avatar-block">
                                                <img src="http://placehold.it/32x32" alt="avatar">
                                            </div>
                                            <div class="text-block">
                                                I'm good and you?
                                            </div>
                                        </div>
                                        <div class="chat-group">
                                            <div class="avatar-block">
                                                <img src="http://placehold.it/32x32" alt="avatar">
                                            </div>
                                            <div class="text-block">
                                                Great! what should we talk about here?
                                            </div>
                                        </div>
                                        <div class="chat-group right">
                                            <div class="avatar-block">
                                                <img src="http://placehold.it/32x32" alt="avatar">
                                            </div>
                                            <div class="text-block">
                                                IDK, what do you think? <img class="smile"
                                                                             src="./assets/image/smile-eek.svg"
                                                                             alt="smile">
                                            </div>
                                        </div>
                                        <div class="chat-group">
                                            <div class="avatar-block">
                                                <img src="http://placehold.it/32x32" alt="avatar">
                                            </div>
                                            <div class="text-block">
                                                Great! what should we talk about here?
                                            </div>
                                        </div>
                                        <div class="chat-group right">
                                            <div class="avatar-block">
                                                <img src="http://placehold.it/32x32" alt="avatar">
                                            </div>
                                            <div class="text-block">
                                                IDK, what do you think? <img class="smile"
                                                                             src="./assets/image/smile-eek.svg"
                                                                             alt="smile">
                                            </div>
                                        </div>
                                        <div class="chat-group right">
                                            <div class="avatar-block">
                                                <img src="http://placehold.it/32x32" alt="avatar">
                                            </div>
                                            <div class="text-block">
                                                I'll be happy to know what is in your mind
                                            </div>
                                        </div>
                                        <div class="chat-group">
                                            <div class="avatar-block">
                                                <img src="http://placehold.it/32x32" alt="avatar">
                                            </div>
                                            <div class="card-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/82x82" alt="photo">
                                                </div>
                                                <div class="card-txt">
                                                    <div class="card-ttl">
                                                        <span class="name">Asakusa view hotel</span>
                                                    </div>
                                                    <div class="card-info-wrap">
                                                        <div class="info-block">
                                                            <span>@lang('profile.check_in')</span>&nbsp;
                                                            <b>19 october</b>&nbsp;
                                                            <span>at</span>&nbsp;
                                                            <b>9h20</b>
                                                        </div>
                                                    </div>
                                                    <div class="card-info-wrap">
                                                        <div class="info-block">
                                                            <span>@lang('trip.will_spend')</span>&nbsp;
                                                            <b>$1,200</b>
                                                        </div>
                                                        <div class="dot">·</div>
                                                        <div class="info-block">
                                                            <span>@lang('trip.planning_to_stay')</span>&nbsp;
                                                            <b>@choice('time.count_hours', 2, ['count' => 2])</b>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="chat-group right">
                                            <div class="avatar-block">
                                                <img src="http://placehold.it/32x32" alt="avatar">
                                            </div>
                                            <div class="card-block">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/82x82" alt="photo">
                                                </div>
                                                <div class="card-txt">
                                                    <div class="card-ttl">
                                                        <span class="name">Asakusa view hotel</span>
                                                    </div>
                                                    <div class="card-info-wrap">
                                                        <div class="info-block">
                                                            <span>@lang('profile.check_in')</span>&nbsp;
                                                            <b>19 october</b>&nbsp;
                                                            <span>at</span>&nbsp;
                                                            <b>9h20</b>
                                                        </div>
                                                    </div>
                                                    <div class="card-info-wrap">
                                                        <div class="info-block">
                                                            <span>@lang('trip.will_spend')</span>&nbsp;
                                                            <b>$1,200</b>
                                                        </div>
                                                        <div class="dot">·</div>
                                                        <div class="info-block">
                                                            <span>@lang('trip.planning_to_stay')</span>&nbsp;
                                                            <b>@choice('time.count_hours', 2, ['count' => 2])</b>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add-input-block">
                                        <div class="input-wrap">
                                            <input type="text" placeholder="@lang('chat.write_a_message')">
                                        </div>
                                        <div class="add-input-action">
                                            <ul class="action-list">
                                                <li>
                                                    <a href="#">
                                                        <i class="trav-input-image-icon"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="trav-input-h-icon"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="trav-input-cutlery-icon"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="trav-input-map-star-icon"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="trav-input-map-around-icon"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="trav-input-video-icon"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- story mode popup -->
    <div class="modal fade white-style" data-backdrop="false" id="storyModePopup" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">

        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
            <i class="trav-close-icon"></i>
        </button>

        <div class="head-trip-plan" id="modalHeadTripPlan">
            <div class="head-trip-plan_wrapper">
                <div class="container-fluid">
                    <div class="head-trip-plan_inner">
                        <div class="plan-by-name">
                            <div class="ava-by-wrap">
                                <img src="http://placehold.it/36x36" alt="avatar">
                            </div>
                        </div>
                        <div class="head-trip-timeline">
                            <div class="timeline-layer">
                                <img src="http://placehold.it/800x50?text=timeline+place:https://timeline.knightlab.com"
                                     alt="timeline">
                            </div>
                            <div class="day-slider">
                                <ul class="calendar-slider" id="calendarSlider">
                                    <li>
                                        <div class="number-day">16</div>
                                        <div class="month">jun</div>
                                    </li>
                                    <li>
                                        <div class="number-day">17</div>
                                        <div class="month">jun</div>
                                    </li>
                                    <li class="active">
                                        <div class="number-day">18</div>
                                        <div class="month">jun</div>
                                    </li>
                                    <li>
                                        <div class="number-day">19</div>
                                        <div class="month">jun</div>
                                    </li>
                                    <li>
                                        <div class="number-day">20</div>
                                        <div class="month">jun</div>
                                    </li>
                                    <li>
                                        <div class="number-day">21</div>
                                        <div class="month">jun</div>
                                    </li>
                                </ul>
                                <ul class="calendar-link-list">
                                    <li>
                                        <a href="#" class="prevDay"><i class="trav-angle-left"></i></a>
                                    </li>
                                    <li>
                                        <span class="dot"></span>
                                    </li>
                                    <li>
                                        <a href="#" class="nextDay"><i class="trav-angle-right"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="head-trip-plan_trip-line">
                <div class="container-fluid">
                    <div class="trip-line" id="tripLineSliderModal">
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon">
                                    <img src="./assets/image/flag-img-1.png" alt="flag-icon">
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">Morocco</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('region.count_city', 1, ['count' => 1])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon blue-icon">
                                    <i class="trav-set-location-icon"></i>
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name blue">Rabat-Sale</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('place.count_place', 1, ['count' => 1])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon">
                                    <img src="http://placehold.it/32x32" alt="flag-icon">
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">United arab emirates</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('region.count_city', 1, ['count' => 1])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon disabled-icon">
                                    <i class="trav-radio-checked2"></i>
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">Dubai</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('place.count_place', 1, ['count' => 1])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon">
                                    <img src="http://placehold.it/32x32" alt="flag-icon">
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">Japan</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('region.count_city', 3, ['count' => 3])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon disabled-icon">
                                    <i class="trav-radio-checked2"></i>
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">Tokyo</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('trip.count_place', 2, ['count' => 8])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon disabled-icon">
                                    <i class="trav-radio-checked2"></i>
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">Nagoya</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('trip.count_place', 2, ['count' => 3])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-line_slide">
                            <div class="trip-line_slide__inner">
                                <div class="trip-icon disabled-icon">
                                    <i class="trav-radio-checked2"></i>
                                </div>
                                <div class="trip-content">
                                    <div class="trip-line-name">Osaka</div>
                                    <div class="trip-line-tag">
                                        <span class="place-tag">@choice('trip.count_place', 2, ['count' => 5])</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-dialog modal-custom-style modal-1070" role="document">
            <div class="modal-custom-block">
                <div class="post-block post-story-block">
                    <div class="story-header-map start-map-layer">
                        <img src="http://placehold.it/1070x420?text=map+layer" alt="">
                        <button class="btn btn-light-primary btn-use-plan">@lang('trip.use_plan')</button>
                    </div>
                    <div class="story-info-block">
                        <div class="story-by">
                            <div class="avatar-wrap">
                                <img src="http://placehold.it/64x64" alt="avatar">
                            </div>
                            <div class="by-info">
                                <span>@lang('other.by')</span>
                                <span class="name">Suzanne</span>
                                <span class="posted">&nbsp;·&nbsp; Posted on 15 July 2017</span>
                            </div>
                        </div>
                        <div class="story-info-head">
                            <h2 class="start-ttl">From Morocco to Japan in 7 Days</h2>
                            <div class="story-head-bottom">
                                <div class="story-avatar-block">
                                    <ul class="avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/34x34" alt="ava"></li><!--
                                            -->
                                        <li><img class="small-ava" src="http://placehold.it/34x34" alt="ava"></li><!--
                                            -->
                                        <li><img class="small-ava" src="http://placehold.it/34x34" alt="ava"></li>
                                    </ul>
                                    <span><b>23</b><br>@lang('trip.used_this_trip_plan')</span>
                                </div>
                                <div class="map-info-list">
                                    <div class="trip-map-info_block">
                                        <div class="trip-icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="trip-info-txt">
                                            <p class="label-txt">7 Days</p>
                                            <p class="info-txt">Duration</p>
                                        </div>
                                    </div>
                                    <div class="trip-map-info_block">
                                        <div class="trip-icon-wrap">
                                            <i class="trav-budget-icon"></i>
                                        </div>
                                        <div class="trip-info-txt">
                                            <p class="label-txt">$ 5000</p>
                                            <p class="info-txt">@lang('trip.budget')</p>
                                        </div>
                                    </div>
                                    <div class="trip-map-info_block">
                                        <div class="trip-icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="trip-info-txt">
                                            <p class="label-txt">2.3 km</p>
                                            <p class="info-txt">@lang('trip.distance')</p>
                                        </div>
                                    </div>
                                    <div class="trip-map-info_block">
                                        <div class="trip-icon-wrap">
                                            <i class="trav-map-marker-icon"></i>
                                        </div>
                                        <div class="trip-info-txt">
                                            <p class="label-txt">12</p>
                                            <p class="info-txt">@choice('trip.place', 2)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="post-block post-story-block">
                    <div class="info-between-block">
                        <span class="arrow"></span>
                        <div class="trip-info">
                            <i class="trav-flag-icon"></i>
                            <span>
                                    <b>@lang('other.start')</b>
                                </span>
                        </div>
                        <div class="flag-wrap">
                            <img src="http://placehold.it/50x50" alt="flag">
                        </div>
                    </div>
                    <div class="story-header-map">
                        <img src="http://placehold.it/1070x365?text=map+layer" alt="">
                        <div class="caption-inner">
                            <div class="post-map-info-caption map-black">
                                <div class="map-avatar">
                                    <img src="http://placehold.it/25x25" alt="ava">
                                </div>
                                <div class="map-label-txt">@lang('trip.stayed')
                                    <b>@choice('time.count_hours', 2, ['count' => 2])</b> in <b>Morocco</b>, spent
                                    arround <b>$250</b>
                                    and have visited <b>1 place</b> in <b>Rabat-Salé</b></div>
                            </div>
                        </div>
                    </div>
                    <div class="story-place-layer">
                        <div class="story-place">
                            <div class="place-name">
                                <ul class="place-name-list">
                                    <li class="current">Rabat-sale</li>
                                </ul>
                            </div>
                            <div class="story-arrow-block">
                                <span class="arrow"></span>
                                <div class="block-tag">@lang('time.day_value', ['value' => '1'])</div>
                                <h2 class="info-ttl">Monday 18 Jun 2017 at 10:00 am</h2>
                            </div>
                        </div>
                    </div>
                    <div class="story-content-block">
                        <div class="place-over">
                            <div class="place-over_top">
                                <div class="place-over-txt">
                                    <b>Rabat-Salé Airport</b>
                                    <i class="trav-set-location-icon"></i>
                                    <span class="place-txt">Airport in Rabat-Salé, Moroco</span>
                                </div>
                                <div class="follow-btn-wrap">
                                    <button type="button"
                                            class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                </div>
                            </div>
                            <div class="trip-photos">
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365" alt="photo">
                                </div>
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365/f5f5f5" alt="photo">
                                </div>
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365" alt="photo">
                                </div>
                                <a href="#" class="see-more-link">@lang('place.see_more_photos')</a>
                            </div>
                        </div>
                        <div class="story-content-inner">
                            <div class="story-date-layer">
                                <div class="follow-avatar">
                                    <img src="http://placehold.it/68x68" alt="avatar">
                                </div>
                                <div class="follow-by">
                                    <span>@lang('other.by')</span>
                                    <b>Suzanne</b>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">@lang('time.day_value', ['value' => '1 / 7'])</div>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">Mon, 18 Jun 2017</div>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">@ 10:00am</div>
                                </div>
                            </div>
                            <div class="post-block post-side-preview">
                                <div class="post-side-top">
                                    <h3 class="side-ttl">Rabat-Sale airport</h3>
                                </div>
                                <div class="post-map-inner">
                                    <img src="http://placehold.it/234x175?text=map_layer" alt="map">
                                    <button class="btn btn-light-primary fullscreen-btn">
                                        <i class="trav-fullscreen-icon"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="info-block">
                                <div class="info-top-layer">
                                    <ul class="info-list">
                                        <li>
                                            <i class="trav-clock-icon"></i><span
                                                    class="info-txt"> @lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></span>
                                        </li>
                                        <li>
                                            <i class="trav-budget-icon"></i><span
                                                    class="info-txt"> @lang('trip.spent') <b>$310</b></span>
                                        </li>
                                        <li>
                                            <i class="trav-weather-cloud-icon"></i><span class="info-txt"> @lang('place.weather') <b>12° C</b></span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="info-content">
                                    <span class="good-label">@lang('place.good_for')</span>
                                    <ul class="block-tag-list">
                                        <li><span class="block-tag">@lang('place.food')</span></li>
                                        <li><span class="block-tag">@lang('place.relaxation')</span></li>
                                        <li><span class="block-tag">@lang('place.shopping')</span></li>
                                        <li><span class="block-tag">@lang('place.photography')</span></li>
                                    </ul>
                                </div>
                                <div class="info-bottom-layer">
                                    <p>“It was an amazing experience, I’m happy I selected this place to be in my
                                        trip”</p>
                                </div>
                            </div>
                            <div class="info-main-layer">
                                <p>Cras ac tortor orci a liquam fermentum leo faucibus tellus blandit dapibus ras
                                    convallis placerat est, tempor volutpat urna sempernec.</p>
                                <h3>Headline Here</h3>
                                <p>Consectetur adipiscing elit nam consequat ligula non mi rutrum suscipit cras ac
                                    tortor orci.</p>
                                <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est,
                                    tempor volutpat urna sempernec.</p>
                                <ul class="img-list">
                                    <li><img src="http://placehold.it/700x460" alt="image"></li>
                                </ul>
                                <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est,
                                    tempor volutpat urna sempernec.</p>
                                <div class="flex-image-wrap fh-365">
                                    <div class="image-column col-60">
                                        <div class="image-inner"
                                             style="background-image:url(http://placehold.it/410x365)"></div>
                                    </div>
                                    <div class="image-column col-40">
                                        <div class="image-inner"
                                             style="background-image:url(http://placehold.it/290x365)"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-tips">
                                <div class="post-tips-top">
                                    <h4 class="post-tips_ttl">Tips about Rabat Sale Airport</h4>
                                    <a href="#" class="see-all-tip">@lang('trip.see_all_tips_count', ['count' => 7])</a>
                                </div>
                                <div class="post-tips_block-wrap">
                                    <div class="post-tips_block">
                                        <div class="tips-content">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                                architecto vel magnam accusamus nemo odio unde autem, provident eos
                                                itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                        </div>
                                        <div class="tips-footer">
                                            <div class="tip-by-info">
                                                <div class="tip-by-img">
                                                    <img src="http://placehold.it/25x25" alt="">
                                                </div>
                                                <div class="tip-by-txt">
                                                    @lang('other.by') <span class="tag blue">Suzanne</span> <span
                                                            class="date-info">5 days ago</span>
                                                </div>
                                            </div>
                                            <ul class="tip-right-info-list">
                                                <li class="round">
                                                    <i class="trav-bookmark-icon"></i>
                                                </li>
                                                <li class="round">
                                                    <i class="trav-flag-icon"></i>
                                                </li>
                                                <li class="round blue">
                                                    <i class="trav-chevron-up"></i>
                                                </li>
                                                <li class="count">
                                                    <span>6</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="foot-reaction">
                                <ul class="post-round-info-list">
                                    <li>
                                        <div class="map-reaction">
                                                <span class="dropdown">
                                                    <i class="trav-heart-fill-icon" data-toggle="dropdown"
                                                       aria-haspopup="true" aria-expanded="false"></i>
                                                    <div class="dropdown-menu dropdown-menu-middle-right dropdown-arrow dropdown-emoji-wrap">
                                                        <ul class="dropdown-emoji-list">
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap like-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.like')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap haha-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.haha')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap wow-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.wow')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap sad-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.sad')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap angry-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.angry')</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </span>
                                            <span class="count">6</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="round-icon">
                                            <i class="trav-comment-icon"></i>
                                        </div>
                                        <span class="count">3</span>
                                    </li>
                                    <li>
                                        <div class="round-icon">
                                            <i class="trav-light-icon"></i>
                                        </div>
                                        <span class="count">37</span>
                                    </li>
                                    <li>
                                        <a href="#" class="info-link">
                                            <div class="round-icon">
                                                <i class="trav-share-fill-icon"></i>
                                            </div>
                                            <span>@lang('profile.share')</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-comment-layer-wrap">
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li class="active">@lang('other.top')</li>
                                        <li>@lang('other.new')</li>
                                    </ul>
                                    <div class="comm-count-info">
                                        3 / 20
                                    </div>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-smile.png" alt="">
                                                    <span>21</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-like.png" alt="">
                                                    <span>19</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-smile.png" alt="">
                                                    <span>15</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                                </div>
                                <div class="post-add-comment-block">
                                    <div class="avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-add-com-input">
                                        <input type="text" placeholder="@lang('comment.write_a_comment')">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="post-block post-story-block">
                    <div class="info-between-block">
                        <span class="arrow"></span>
                        <div class="trip-info">
                            <i class="trav-plane-icon"></i>
                            <span>
                                    @lang('trip.flying_to')
                                    <b>United Arab Emirates</b>
                                </span>
                        </div>
                        <div class="flag-wrap">
                            <img src="http://placehold.it/50x50" alt="flag">
                        </div>
                    </div>
                    <div class="story-header-map">
                        <img src="http://placehold.it/1070x365?text=map+layer" alt="">
                        <div class="caption-inner">
                            <div class="post-map-info-caption map-black">
                                <div class="map-avatar">
                                    <img src="http://placehold.it/25x25" alt="ava">
                                </div>
                                <div class="map-label-txt">@lang('trip.stayed') <b>3 hours</b> in <b>United Arab
                                        Emirates</b>, spent
                                    arround <b>$520</b> and have visited <b>1 place</b> in <b>Dubai</b></div>
                            </div>
                        </div>
                    </div>
                    <div class="story-place-layer">
                        <div class="story-place">
                            <div class="place-name">
                                <ul class="place-name-list">
                                    <li class="current">Dubai</li>
                                </ul>
                            </div>
                            <div class="story-arrow-block">
                                <span class="arrow"></span>
                                <div class="block-tag">@lang('time.day_value', ['value' => '1'])</div>
                                <h2 class="info-ttl">Monday 18 Jun 2017 at 10:00 am</h2>
                            </div>
                        </div>
                    </div>
                    <div class="story-content-block">
                        <div class="place-over">
                            <div class="place-over_top">
                                <div class="place-over-txt">
                                    <b>@lang('place.name_airport', ['name' => 'Dubai international'])</b>
                                    <i class="trav-set-location-icon"></i>
                                    <span class="place-txt">@lang('place.airport_in_place', ['place' => 'Dubai, United Arab Emirates'])</span>
                                </div>
                                <div class="follow-btn-wrap">
                                    <button type="button"
                                            class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                </div>
                            </div>
                            <div class="trip-photos">
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365" alt="photo">
                                </div>
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365/f5f5f5" alt="photo">
                                </div>
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365" alt="photo">
                                </div>
                                <a href="#" class="see-more-link">@lang('place.see_more_photos')</a>
                            </div>
                        </div>
                        <div class="story-content-inner">
                            <div class="story-date-layer">
                                <div class="follow-avatar">
                                    <img src="http://placehold.it/68x68" alt="avatar">
                                </div>
                                <div class="follow-by">
                                    <span>@lang('other.by')</span>
                                    <b>Suzanne</b>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">@lang('time.day_value', ['value' => '1 / 7'])</div>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">Mon, 18 Jun 2017</div>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">@ 10:00am</div>
                                </div>
                            </div>
                            <div class="info-block">
                                <div class="info-top-layer">
                                    <ul class="info-list">
                                        <li>
                                            <i class="trav-clock-icon"></i><span
                                                    class="info-txt"> @lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></span>
                                        </li>
                                        <li>
                                            <i class="trav-budget-icon"></i><span
                                                    class="info-txt"> @lang('trip.spent') <b>$310</b></span>
                                        </li>
                                        <li>
                                            <i class="trav-weather-cloud-icon"></i><span class="info-txt"> @lang('place.weather') <b>12° C</b></span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="info-content">
                                    <span class="good-label">@lang('place.good_for')</span>
                                    <ul class="block-tag-list">
                                        <li><span class="block-tag">@lang('place.food')</span></li>
                                        <li><span class="block-tag">@lang('place.relaxation')</span></li>
                                        <li><span class="block-tag">@lang('place.shopping')</span></li>
                                        <li><span class="block-tag">@lang('place.photography')</span></li>
                                    </ul>
                                </div>
                                <div class="info-bottom-layer">
                                    <p>“It was an amazing experience, I’m happy I selected this place to be in my
                                        trip”</p>
                                </div>
                            </div>
                            <div class="info-main-layer">
                                <p>Cras ac tortor orci a liquam fermentum leo faucibus tellus blandit dapibus ras
                                    convallis placerat est, tempor volutpat urna sempernec.</p>
                                <h3>Headline Here</h3>
                                <p>Consectetur adipiscing elit nam consequat ligula non mi rutrum suscipit cras ac
                                    tortor orci.</p>
                                <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est,
                                    tempor volutpat urna sempernec.</p>
                                <ul class="img-list">
                                    <li><img src="http://placehold.it/700x460" alt="image"></li>
                                </ul>
                                <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est,
                                    tempor volutpat urna sempernec.</p>
                                <div class="flex-image-wrap fh-460">
                                    <div class="image-column col-66">
                                        <div class="image-inner"
                                             style="background-image:url(http://placehold.it/475x460)"></div>
                                    </div>
                                    <div class="image-column col-33">
                                        <div class="image-inner img-h-66"
                                             style="background-image:url(http://placehold.it/225x310)"></div>
                                        <div class="image-inner img-h-33"
                                             style="background-image:url(http://placehold.it/225x150)"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-tips">
                                <div class="post-tips-top">
                                    <h4 class="post-tips_ttl">@lang('trip.tips_about_place', ['place' => 'Dubai Airport'])</h4>
                                    <a href="#" class="see-all-tip">@lang('trip.see_all_tips_count', ['count' => 7])</a>
                                </div>
                                <div class="post-tips_block-wrap">
                                    <div class="post-tips_block">
                                        <div class="tips-content">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                                architecto vel magnam accusamus nemo odio unde autem, provident eos
                                                itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                        </div>
                                        <div class="tips-footer">
                                            <div class="tip-by-info">
                                                <div class="tip-by-img">
                                                    <img src="http://placehold.it/25x25" alt="">
                                                </div>
                                                <div class="tip-by-txt">
                                                    @lang('other.by') <span class="tag blue">Suzanne</span> <span
                                                            class="date-info">5 days ago</span>
                                                </div>
                                            </div>
                                            <ul class="tip-right-info-list">
                                                <li class="round">
                                                    <i class="trav-bookmark-icon"></i>
                                                </li>
                                                <li class="round">
                                                    <i class="trav-flag-icon"></i>
                                                </li>
                                                <li class="round blue">
                                                    <i class="trav-chevron-up"></i>
                                                </li>
                                                <li class="count">
                                                    <span>6</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="foot-reaction">
                                <ul class="post-round-info-list">
                                    <li>
                                        <div class="map-reaction">
                                                <span class="dropdown">
                                                    <i class="trav-heart-fill-icon" data-toggle="dropdown"
                                                       aria-haspopup="true" aria-expanded="false"></i>
                                                    <div class="dropdown-menu dropdown-menu-middle-right dropdown-arrow dropdown-emoji-wrap">
                                                        <ul class="dropdown-emoji-list">
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap like-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.like')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap haha-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.haha')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap wow-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.wow')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap sad-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.sad')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap angry-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.angry')</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </span>
                                            <span class="count">6</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="round-icon">
                                            <i class="trav-comment-icon"></i>
                                        </div>
                                        <span class="count">3</span>
                                    </li>
                                    <li>
                                        <div class="round-icon">
                                            <i class="trav-light-icon"></i>
                                        </div>
                                        <span class="count">37</span>
                                    </li>
                                    <li>
                                        <a href="#" class="info-link">
                                            <div class="round-icon">
                                                <i class="trav-share-fill-icon"></i>
                                            </div>
                                            <span>@lang('profile.share')</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-comment-layer-wrap">
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li class="active">@lang('other.top')</li>
                                        <li>@lang('other.new')</li>
                                    </ul>
                                    <div class="comm-count-info">
                                        3 / 20
                                    </div>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-smile.png" alt="">
                                                    <span>21</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-like.png" alt="">
                                                    <span>19</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-smile.png" alt="">
                                                    <span>15</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                                </div>
                                <div class="post-add-comment-block">
                                    <div class="avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-add-com-input">
                                        <input type="text" placeholder="@lang('comment.write_a_comment')">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="post-block post-story-block">
                    <div class="info-between-block">
                        <span class="arrow"></span>
                        <div class="trip-info">
                            <i class="trav-plane-icon"></i>
                            <span>
                                    @lang('trip.flying_to')
                                    <b>Japan</b>
                                </span>
                        </div>
                        <div class="flag-wrap">
                            <img src="http://placehold.it/50x50" alt="flag">
                        </div>
                    </div>
                    <div class="story-header-map">
                        <img src="http://placehold.it/1070x365?text=map+layer" alt="">
                        <div class="caption-inner">
                            <div class="post-map-info-caption map-black">
                                <div class="map-avatar">
                                    <img src="http://placehold.it/25x25" alt="ava">
                                </div>
                                <div class="map-label-txt">@lang('trip.stayed') <b>4 days</b> in <b>Japan</b>, spent
                                    arround
                                    <b>$840</b> and have visited <b>5 places</b> in <b>Tokyo</b>, <b>Nagoya</b> and <b>+1
                                        more city</b></div>
                            </div>
                        </div>
                    </div>
                    <div class="story-place-layer">
                        <div class="story-place">
                            <div class="place-name">
                                <ul class="place-name-list">
                                    <li class="current">Tokyo</li>
                                    <li>Nagoya</li>
                                    <li>Niigata</li>
                                </ul>
                            </div>
                            <div class="story-arrow-block">
                                <span class="arrow"></span>
                                <div class="block-tag">@lang('time.day_value', ['value' => '2'])</div>
                                <h2 class="info-ttl">Tuesday 19 Jun 2017 at 10:00 am</h2>
                            </div>
                        </div>
                    </div>
                    <div class="story-content-block">
                        <div class="place-over">
                            <div class="place-over_top">
                                <div class="place-over-txt">
                                    <b>Haneda Airport</b>
                                    <i class="trav-set-location-icon"></i>
                                    <span class="place-txt">@lang('place.airport_in_place', ['place' => 'Tokyo, Japan'])</span>
                                </div>
                                <div class="follow-btn-wrap">
                                    <button type="button"
                                            class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                </div>
                            </div>
                            <div class="trip-photos">
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365" alt="photo">
                                </div>
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365/f5f5f5" alt="photo">
                                </div>
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365" alt="photo">
                                </div>
                                <a href="#" class="see-more-link">@lang('place.see_more_photos')</a>
                            </div>
                        </div>
                        <div class="story-content-inner">
                            <div class="story-date-layer">
                                <div class="follow-avatar">
                                    <img src="http://placehold.it/68x68" alt="avatar">
                                </div>
                                <div class="follow-by">
                                    <span>@lang('other.by')</span>
                                    <b>Suzanne</b>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">@lang('time.day_value', ['value' => '1 / 7'])</div>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">Mon, 18 Jun 2017</div>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">@ 10:00am</div>
                                </div>
                            </div>
                            <div class="info-block">
                                <div class="info-top-layer">
                                    <ul class="info-list">
                                        <li>
                                            <i class="trav-clock-icon"></i><span
                                                    class="info-txt"> @lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></span>
                                        </li>
                                        <li>
                                            <i class="trav-budget-icon"></i><span
                                                    class="info-txt"> @lang('trip.spent') <b>$310</b></span>
                                        </li>
                                        <li>
                                            <i class="trav-weather-cloud-icon"></i><span class="info-txt"> @lang('place.weather') <b>12° C</b></span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="info-content">
                                    <span class="good-label">@lang('place.good_for')</span>
                                    <ul class="block-tag-list">
                                        <li><span class="block-tag">@lang('place.food')</span></li>
                                        <li><span class="block-tag">@lang('place.relaxation')</span></li>
                                        <li><span class="block-tag">@lang('place.shopping')</span></li>
                                        <li><span class="block-tag">@lang('place.photography')</span></li>
                                    </ul>
                                </div>
                                <div class="info-bottom-layer">
                                    <p>“It was an amazing experience, I’m happy I selected this place to be in my
                                        trip”</p>
                                </div>
                            </div>
                            <div class="info-main-layer">
                                <p>Cras ac tortor orci a liquam fermentum leo faucibus tellus blandit dapibus ras
                                    convallis placerat est, tempor volutpat urna sempernec.</p>
                                <h3>Headline Here</h3>
                                <p>Consectetur adipiscing elit nam consequat ligula non mi rutrum suscipit cras ac
                                    tortor orci.</p>
                                <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est,
                                    tempor volutpat urna sempernec.</p>
                                <ul class="img-list">
                                    <li><img src="http://placehold.it/700x460" alt="image"></li>
                                </ul>
                                <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est,
                                    tempor volutpat urna sempernec.</p>
                                <div class="flex-image-wrap fh-460">
                                    <div class="image-column col-33">
                                        <div class="image-inner img-h-40"
                                             style="background-image:url(http://placehold.it/225x180)"></div>
                                        <div class="image-inner img-h-60"
                                             style="background-image:url(http://placehold.it/225x280)"></div>
                                    </div>
                                    <div class="image-column col-33">
                                        <div class="image-inner img-h-55"
                                             style="background-image:url(http://placehold.it/225x250)"></div>
                                        <div class="image-inner img-h-45"
                                             style="background-image:url(http://placehold.it/225x210)"></div>
                                    </div>
                                    <div class="image-column col-33">
                                        <div class="image-inner"
                                             style="background-image:url(http://placehold.it/225x460)"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-tips">
                                <div class="post-tips-top">
                                    <h4 class="post-tips_ttl">Tips about Haneda Airport</h4>
                                    <a href="#" class="see-all-tip">@lang('trip.see_all_tips_count', ['count' => 7])</a>
                                </div>
                                <div class="post-tips_block-wrap">
                                    <div class="post-tips_block">
                                        <div class="tips-content">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                                architecto vel magnam accusamus nemo odio unde autem, provident eos
                                                itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                        </div>
                                        <div class="tips-footer">
                                            <div class="tip-by-info">
                                                <div class="tip-by-img">
                                                    <img src="http://placehold.it/25x25" alt="">
                                                </div>
                                                <div class="tip-by-txt">
                                                    @lang('other.by') <span class="tag blue">Suzanne</span> <span
                                                            class="date-info">5 days ago</span>
                                                </div>
                                            </div>
                                            <ul class="tip-right-info-list">
                                                <li class="round">
                                                    <i class="trav-bookmark-icon"></i>
                                                </li>
                                                <li class="round">
                                                    <i class="trav-flag-icon"></i>
                                                </li>
                                                <li class="round blue">
                                                    <i class="trav-chevron-up"></i>
                                                </li>
                                                <li class="count">
                                                    <span>6</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="foot-reaction">
                                <ul class="post-round-info-list">
                                    <li>
                                        <div class="map-reaction">
                                                <span class="dropdown">
                                                    <i class="trav-heart-fill-icon" data-toggle="dropdown"
                                                       aria-haspopup="true" aria-expanded="false"></i>
                                                    <div class="dropdown-menu dropdown-menu-middle-right dropdown-arrow dropdown-emoji-wrap">
                                                        <ul class="dropdown-emoji-list">
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap like-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.like')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap haha-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.haha')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap wow-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.wow')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap sad-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.sad')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap angry-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.angry')</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </span>
                                            <span class="count">6</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="round-icon">
                                            <i class="trav-comment-icon"></i>
                                        </div>
                                        <span class="count">3</span>
                                    </li>
                                    <li>
                                        <div class="round-icon">
                                            <i class="trav-light-icon"></i>
                                        </div>
                                        <span class="count">37</span>
                                    </li>
                                    <li>
                                        <a href="#" class="info-link">
                                            <div class="round-icon">
                                                <i class="trav-share-fill-icon"></i>
                                            </div>
                                            <span>@lang('profile.share')</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-comment-layer-wrap">
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li class="active">@lang('other.top')</li>
                                        <li>@lang('other.new')</li>
                                    </ul>
                                    <div class="comm-count-info">
                                        3 / 20
                                    </div>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-smile.png" alt="">
                                                    <span>21</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-like.png" alt="">
                                                    <span>19</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-smile.png" alt="">
                                                    <span>15</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                                </div>
                                <div class="post-add-comment-block">
                                    <div class="avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-add-com-input">
                                        <input type="text" placeholder="@lang('comment.write_a_comment')">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="story-place-layer">
                        <div class="story-place">
                            <div class="place-name">
                                <ul class="place-name-list">
                                    <li class="current">Tokyo</li>
                                    <li>Nagoya</li>
                                    <li>Niigata</li>
                                </ul>
                            </div>
                            <div class="story-arrow-block">
                                <span class="arrow"></span>
                                <div class="block-tag">@lang('time.day_value', ['value' => '2'])</div>
                                <h2 class="info-ttl">Tuesday 19 Jun 2017 at 10:00 am</h2>
                            </div>
                        </div>
                    </div>
                    <div class="story-content-block">
                        <div class="place-over">
                            <div class="place-over_top">
                                <div class="place-over-txt">
                                    <b>Bunka</b>
                                    <i class="trav-set-location-icon"></i>
                                    <span class="place-txt">@lang('place.hotel_in_place', ['place' => 'Tokyo, Japan'])</span>
                                </div>
                                <div class="follow-btn-wrap">
                                    <button type="button"
                                            class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                </div>
                            </div>
                            <div class="trip-photos">
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365" alt="photo">
                                </div>
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365/f5f5f5" alt="photo">
                                </div>
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365" alt="photo">
                                </div>
                                <a href="#" class="see-more-link">@lang('place.see_more_photos')</a>
                            </div>
                        </div>
                        <div class="story-content-inner">
                            <div class="story-date-layer">
                                <div class="follow-avatar">
                                    <img src="http://placehold.it/68x68" alt="avatar">
                                </div>
                                <div class="follow-by">
                                    <span>@lang('other.by')</span>
                                    <b>Suzanne</b>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">@lang('time.day_value', ['value' => '1 / 7'])</div>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">Mon, 18 Jun 2017</div>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">@ 10:00am</div>
                                </div>
                            </div>
                            <div class="info-block">
                                <div class="info-top-layer">
                                    <ul class="info-list">
                                        <li>
                                            <i class="trav-clock-icon"></i><span
                                                    class="info-txt"> @lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></span>
                                        </li>
                                        <li>
                                            <i class="trav-budget-icon"></i><span
                                                    class="info-txt"> @lang('trip.spent') <b>$310</b></span>
                                        </li>
                                        <li>
                                            <i class="trav-weather-cloud-icon"></i><span class="info-txt"> @lang('place.weather') <b>12° C</b></span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="info-content">
                                    <span class="good-label">@lang('place.good_for')</span>
                                    <ul class="block-tag-list">
                                        <li><span class="block-tag">@lang('place.food')</span></li>
                                        <li><span class="block-tag">@lang('place.relaxation')</span></li>
                                        <li><span class="block-tag">@lang('place.shopping')</span></li>
                                        <li><span class="block-tag">@lang('place.photography')</span></li>
                                    </ul>
                                </div>
                                <div class="info-bottom-layer">
                                    <p>“It was an amazing experience, I’m happy I selected this place to be in my
                                        trip”</p>
                                </div>
                            </div>
                            <div class="info-main-layer">
                                <p>Cras ac tortor orci a liquam fermentum leo faucibus tellus blandit dapibus ras
                                    convallis placerat est, tempor volutpat urna sempernec.</p>
                                <h3>Headline Here</h3>
                                <p>Consectetur adipiscing elit nam consequat ligula non mi rutrum suscipit cras ac
                                    tortor orci.</p>
                                <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est,
                                    tempor volutpat urna sempernec.</p>
                                <ul class="img-list">
                                    <li><img src="http://placehold.it/700x460" alt="image"></li>
                                </ul>
                                <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est,
                                    tempor volutpat urna sempernec.</p>
                                <ul class="img-list">
                                    <li><img src="http://placehold.it/405x360" alt="image"></li>
                                    <li><img src="http://placehold.it/290x360" alt="image"></li>
                                </ul>
                            </div>
                            <div class="post-tips">
                                <div class="post-tips-top">
                                    <h4 class="post-tips_ttl">Tips about Bunka Hotel</h4>
                                    <a href="#" class="see-all-tip">@lang('trip.see_all_tips_count', ['count' => 7])</a>
                                </div>
                                <div class="post-tips_block-wrap">
                                    <div class="post-tips_block">
                                        <div class="tips-content">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                                architecto vel magnam accusamus nemo odio unde autem, provident eos
                                                itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                        </div>
                                        <div class="tips-footer">
                                            <div class="tip-by-info">
                                                <div class="tip-by-img">
                                                    <img src="http://placehold.it/25x25" alt="">
                                                </div>
                                                <div class="tip-by-txt">
                                                    @lang('other.by') <span class="tag blue">Suzanne</span> <span
                                                            class="date-info">5 days ago</span>
                                                </div>
                                            </div>
                                            <ul class="tip-right-info-list">
                                                <li class="round">
                                                    <i class="trav-bookmark-icon"></i>
                                                </li>
                                                <li class="round">
                                                    <i class="trav-flag-icon"></i>
                                                </li>
                                                <li class="round blue">
                                                    <i class="trav-chevron-up"></i>
                                                </li>
                                                <li class="count">
                                                    <span>6</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="foot-reaction">
                                <ul class="post-round-info-list">
                                    <li>
                                        <div class="map-reaction">
                                                <span class="dropdown">
                                                    <i class="trav-heart-fill-icon" data-toggle="dropdown"
                                                       aria-haspopup="true" aria-expanded="false"></i>
                                                    <div class="dropdown-menu dropdown-menu-middle-right dropdown-arrow dropdown-emoji-wrap">
                                                        <ul class="dropdown-emoji-list">
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap like-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.like')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap haha-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.haha')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap wow-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.wow')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap sad-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.sad')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap angry-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.angry')</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </span>
                                            <span class="count">6</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="round-icon">
                                            <i class="trav-comment-icon"></i>
                                        </div>
                                        <span class="count">3</span>
                                    </li>
                                    <li>
                                        <div class="round-icon">
                                            <i class="trav-light-icon"></i>
                                        </div>
                                        <span class="count">37</span>
                                    </li>
                                    <li>
                                        <a href="#" class="info-link">
                                            <div class="round-icon">
                                                <i class="trav-share-fill-icon"></i>
                                            </div>
                                            <span>@lang('profile.share')</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-comment-layer-wrap">
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li class="active">@lang('other.top')</li>
                                        <li>@lang('other.new')</li>
                                    </ul>
                                    <div class="comm-count-info">
                                        3 / 20
                                    </div>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-smile.png" alt="">
                                                    <span>21</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-like.png" alt="">
                                                    <span>19</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-smile.png" alt="">
                                                    <span>15</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                                </div>
                                <div class="post-add-comment-block">
                                    <div class="avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-add-com-input">
                                        <input type="text" placeholder="@lang('comment.write_a_comment')">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="story-place-layer">
                        <div class="story-place">
                            <div class="place-name">
                                <ul class="place-name-list">
                                    <li class="current">Tokyo</li>
                                    <li>Nagoya</li>
                                    <li>Niigata</li>
                                </ul>
                            </div>
                            <div class="story-arrow-block">
                                <span class="arrow"></span>
                                <div class="block-tag">@lang('time.day_value', ['value' => '2'])</div>
                                <h2 class="info-ttl">Tuesday 19 Jun 2017 at 10:00 am</h2>
                            </div>
                        </div>
                    </div>
                    <div class="story-content-block">
                        <div class="place-over">
                            <div class="place-over_top">
                                <div class="place-over-txt">
                                    <b>Tokyo Sushi</b>
                                    <i class="trav-set-location-icon"></i>
                                    <span class="place-txt">Restaurant in Tokyo, Japan</span>
                                </div>
                                <div class="follow-btn-wrap">
                                    <button type="button"
                                            class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                </div>
                            </div>
                            <div class="trip-photos">
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365" alt="photo">
                                </div>
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365/f5f5f5" alt="photo">
                                </div>
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365" alt="photo">
                                </div>
                                <a href="#" class="see-more-link">@lang('place.see_more_photos')</a>
                            </div>
                        </div>
                        <div class="story-content-inner">
                            <div class="story-date-layer">
                                <div class="follow-avatar">
                                    <img src="http://placehold.it/68x68" alt="avatar">
                                </div>
                                <div class="follow-by">
                                    <span>@lang('other.by')</span>
                                    <b>Suzanne</b>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">@lang('time.day_value', ['value' => '1 / 7'])</div>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">Mon, 18 Jun 2017</div>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">@ 10:00am</div>
                                </div>
                            </div>
                            <div class="info-block">
                                <div class="info-top-layer">
                                    <ul class="info-list">
                                        <li>
                                            <i class="trav-clock-icon"></i><span
                                                    class="info-txt"> @lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></span>
                                        </li>
                                        <li>
                                            <i class="trav-budget-icon"></i><span
                                                    class="info-txt"> @lang('trip.spent') <b>$310</b></span>
                                        </li>
                                        <li>
                                            <i class="trav-weather-cloud-icon"></i><span class="info-txt"> @lang('place.weather') <b>12° C</b></span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="info-content">
                                    <span class="good-label">@lang('place.good_for')</span>
                                    <ul class="block-tag-list">
                                        <li><span class="block-tag">@lang('place.food')</span></li>
                                        <li><span class="block-tag">@lang('place.relaxation')</span></li>
                                        <li><span class="block-tag">@lang('place.shopping')</span></li>
                                        <li><span class="block-tag">@lang('place.photography')</span></li>
                                    </ul>
                                </div>
                                <div class="info-bottom-layer">
                                    <p>“It was an amazing experience, I’m happy I selected this place to be in my
                                        trip”</p>
                                </div>
                            </div>
                            <div class="info-main-layer">
                                <p>Cras ac tortor orci a liquam fermentum leo faucibus tellus blandit dapibus ras
                                    convallis placerat est, tempor volutpat urna sempernec.</p>
                                <h3>Headline Here</h3>
                                <p>Consectetur adipiscing elit nam consequat ligula non mi rutrum suscipit cras ac
                                    tortor orci.</p>
                                <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est,
                                    tempor volutpat urna sempernec.</p>
                                <ul class="img-list">
                                    <li><img src="http://placehold.it/700x460" alt="image"></li>
                                </ul>
                                <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est,
                                    tempor volutpat urna sempernec.</p>
                                <ul class="img-list">
                                    <li><img src="http://placehold.it/405x360" alt="image"></li>
                                    <li><img src="http://placehold.it/290x360" alt="image"></li>
                                </ul>
                            </div>
                            <div class="post-tips">
                                <div class="post-tips-top">
                                    <h4 class="post-tips_ttl">Tips about Bunka Hotel</h4>
                                    <a href="#" class="see-all-tip">@lang('trip.see_all_tips_count', ['count' => 7])</a>
                                </div>
                                <div class="post-tips_block-wrap">
                                    <div class="post-tips_block">
                                        <div class="tips-content">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                                architecto vel magnam accusamus nemo odio unde autem, provident eos
                                                itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                        </div>
                                        <div class="tips-footer">
                                            <div class="tip-by-info">
                                                <div class="tip-by-img">
                                                    <img src="http://placehold.it/25x25" alt="">
                                                </div>
                                                <div class="tip-by-txt">
                                                    @lang('other.by') <span class="tag blue">Suzanne</span> <span
                                                            class="date-info">5 days ago</span>
                                                </div>
                                            </div>
                                            <ul class="tip-right-info-list">
                                                <li class="round">
                                                    <i class="trav-bookmark-icon"></i>
                                                </li>
                                                <li class="round">
                                                    <i class="trav-flag-icon"></i>
                                                </li>
                                                <li class="round blue">
                                                    <i class="trav-chevron-up"></i>
                                                </li>
                                                <li class="count">
                                                    <span>6</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="foot-reaction">
                                <ul class="post-round-info-list">
                                    <li>
                                        <div class="map-reaction">
                                                <span class="dropdown">
                                                    <i class="trav-heart-fill-icon" data-toggle="dropdown"
                                                       aria-haspopup="true" aria-expanded="false"></i>
                                                    <div class="dropdown-menu dropdown-menu-middle-right dropdown-arrow dropdown-emoji-wrap">
                                                        <ul class="dropdown-emoji-list">
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap like-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.like')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap haha-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.haha')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap wow-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.wow')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap sad-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.sad')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap angry-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.angry')</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </span>
                                            <span class="count">6</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="round-icon">
                                            <i class="trav-comment-icon"></i>
                                        </div>
                                        <span class="count">3</span>
                                    </li>
                                    <li>
                                        <div class="round-icon">
                                            <i class="trav-light-icon"></i>
                                        </div>
                                        <span class="count">37</span>
                                    </li>
                                    <li>
                                        <a href="#" class="info-link">
                                            <div class="round-icon">
                                                <i class="trav-share-fill-icon"></i>
                                            </div>
                                            <span>@lang('profile.share')</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-comment-layer-wrap">
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li class="active">@lang('other.top')</li>
                                        <li>@lang('other.new')</li>
                                    </ul>
                                    <div class="comm-count-info">
                                        3 / 20
                                    </div>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-smile.png" alt="">
                                                    <span>21</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-like.png" alt="">
                                                    <span>19</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-smile.png" alt="">
                                                    <span>15</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                                </div>
                                <div class="post-add-comment-block">
                                    <div class="avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-add-com-input">
                                        <input type="text" placeholder="@lang('comment.write_a_comment')">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="post-block modal-post-slider">
                    <div class="post-side-top">
                        <h3 class="side-ttl">More stories</h3>
                        <div class="side-right-control">
                            <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                            <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
                        </div>
                    </div>
                    <div class="post-side-inner">
                        <div class="post-slide-wrap slide-hide-right-margin">
                            <ul id="moreStorySlider" class="post-slider">
                                <li class="post-card">
                                    <div class="image-wrap">
                                        <img src="http://placehold.it/290x200" alt="">
                                    </div>
                                    <div class="post-slider-caption">
                                        <p class="post-card-name">What to See in New York City</p>
                                        <p class="post-card-by">
                                            <span>@lang('other.by') <b>Michell</b></span>
                                        </p>
                                    </div>
                                </li>
                                <li class="post-card">
                                    <div class="image-wrap">
                                        <img src="http://placehold.it/290x200" alt="">
                                    </div>
                                    <div class="post-slider-caption">
                                        <p class="post-card-name">My Trip to Tokyo</p>
                                        <p class="post-card-by">
                                            <span>@lang('other.by') <b>Tameka</b></span>
                                        </p>
                                    </div>
                                </li>
                                <li class="post-card">
                                    <div class="image-wrap">
                                        <img src="http://placehold.it/290x200" alt="">
                                    </div>
                                    <div class="post-slider-caption">
                                        <p class="post-card-name">Going to Morocco for the First Time</p>
                                        <p class="post-card-by">
                                            <span>@lang('other.by') <b>Emily</b></span>
                                        </p>
                                    </div>
                                </li>
                                <li class="post-card">
                                    <div class="image-wrap">
                                        <img src="http://placehold.it/290x200" alt="">
                                    </div>
                                    <div class="post-slider-caption">
                                        <p class="post-card-name">What to See in New York</p>
                                        <p class="post-card-by">
                                            <span>@lang('other.by') <b>Patrick</b></span>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- map mode popup -->
    <div class="modal fade modal-child white-style" data-backdrop="false" id="mapModePlacePopup" tabindex="-1"
         role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-modal-parent="#mapModePopup">
        <div class="modal-dialog modal-custom-style modal-1070" role="document">
            <div class="modal-custom-block">
                <div class="post-block post-mode-map-place">
                    <div class="story-content-block">
                        <div class="place-over">
                            <div class="place-over_top">
                                <div class="place-over-txt">
                                    <b>Rabat-Salé Airport</b>
                                    <i class="trav-set-location-icon"></i>
                                    <span class="place-txt">Airport in Rabat-Salé, Moroco</span>
                                </div>
                                <div class="follow-btn-wrap">
                                    <button type="button"
                                            class="btn btn-white-bordered">@lang('buttons.general.follow')</button>
                                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                                        <i class="trav-close-icon"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="trip-photos">
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365" alt="photo">
                                </div>
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365/f5f5f5" alt="photo">
                                </div>
                                <div class="photo-wrap">
                                    <img src="http://placehold.it/356x365" alt="photo">
                                </div>
                                <a href="#" class="see-more-link">@lang('place.see_more_photos')</a>
                            </div>
                        </div>
                        <div class="story-content-inner">
                            <div class="story-date-layer">
                                <div class="follow-avatar">
                                    <img src="http://placehold.it/68x68" alt="avatar">
                                </div>
                                <div class="follow-by">
                                    <span>@lang('other.by')</span>
                                    <b>Suzanne</b>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">@lang('time.day_value', ['value' => '1 / 7'])</div>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">Mon, 18 Jun 2017</div>
                                </div>
                                <div class="s-date-line">
                                    <div class="s-date-badge">@ 10:00am</div>
                                </div>
                            </div>
                            <div class="info-block">
                                <div class="info-top-layer">
                                    <ul class="info-list">
                                        <li>
                                            <i class="trav-clock-icon"></i><span
                                                    class="info-txt"> @lang('trip.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></span>
                                        </li>
                                        <li>
                                            <i class="trav-budget-icon"></i><span
                                                    class="info-txt"> @lang('trip.spent') <b>$310</b></span>
                                        </li>
                                        <li>
                                            <i class="trav-weather-cloud-icon"></i><span class="info-txt"> @lang('place.weather') <b>12° C</b></span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="info-content">
                                    <span class="good-label">@lang('place.good_for')</span>
                                    <ul class="block-tag-list">
                                        <li><span class="block-tag">@lang('place.food')</span></li>
                                        <li><span class="block-tag">@lang('place.relaxation')</span></li>
                                        <li><span class="block-tag">@lang('place.shopping')</span></li>
                                        <li><span class="block-tag">@lang('place.photography')</span></li>
                                    </ul>
                                </div>
                                <div class="info-bottom-layer">
                                    <p>“It was an amazing experience, I’m happy I selected this place to be in my
                                        trip”</p>
                                </div>
                            </div>
                            <div class="info-main-layer">
                                <p>Cras ac tortor orci a liquam fermentum leo faucibus tellus blandit dapibus ras
                                    convallis placerat est, tempor volutpat urna sempernec.</p>
                                <h3>Headline Here</h3>
                                <p>Consectetur adipiscing elit nam consequat ligula non mi rutrum suscipit cras ac
                                    tortor orci.</p>
                                <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est,
                                    tempor volutpat urna sempernec.</p>
                                <ul class="img-list">
                                    <li><img src="http://placehold.it/700x460" alt="image"></li>
                                </ul>
                                <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est,
                                    tempor volutpat urna sempernec.</p>
                                <div class="flex-image-wrap fh-365">
                                    <div class="image-column col-60">
                                        <div class="image-inner"
                                             style="background-image:url(http://placehold.it/410x365)"></div>
                                    </div>
                                    <div class="image-column col-40">
                                        <div class="image-inner"
                                             style="background-image:url(http://placehold.it/290x365)"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-tips">
                                <div class="post-tips-top">
                                    <h4 class="post-tips_ttl">Tips about Rabat Sale Airport</h4>
                                    <a href="#" class="see-all-tip">@lang('trip.see_all_tips_count', ['count' => 7])</a>
                                </div>
                                <div class="post-tips_block-wrap">
                                    <div class="post-tips_block">
                                        <div class="tips-content">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                                architecto vel magnam accusamus nemo odio unde autem, provident eos
                                                itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                        </div>
                                        <div class="tips-footer">
                                            <div class="tip-by-info">
                                                <div class="tip-by-img">
                                                    <img src="http://placehold.it/25x25" alt="">
                                                </div>
                                                <div class="tip-by-txt">
                                                    @lang('other.by') <span class="tag blue">Suzanne</span> <span
                                                            class="date-info">5 days ago</span>
                                                </div>
                                            </div>
                                            <ul class="tip-right-info-list">
                                                <li class="round">
                                                    <i class="trav-bookmark-icon"></i>
                                                </li>
                                                <li class="round">
                                                    <i class="trav-flag-icon"></i>
                                                </li>
                                                <li class="round blue">
                                                    <i class="trav-chevron-up"></i>
                                                </li>
                                                <li class="count">
                                                    <span>6</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="foot-reaction">
                                <ul class="post-round-info-list">
                                    <li>
                                        <div class="map-reaction">
                                                <span class="dropdown">
                                                    <i class="trav-heart-fill-icon" data-toggle="dropdown"
                                                       aria-haspopup="true" aria-expanded="false"></i>
                                                    <div class="dropdown-menu dropdown-menu-middle-right dropdown-arrow dropdown-emoji-wrap">
                                                        <ul class="dropdown-emoji-list">
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap like-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.like')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap haha-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.haha')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap wow-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.wow')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap sad-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.sad')</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="emoji-link">
                                                                    <div class="emoji-wrap angry-icon"></div>
                                                                    <span class="emoji-name">@lang('chat.emoticons.angry')</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </span>
                                            <span class="count">6</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="round-icon">
                                            <i class="trav-comment-icon"></i>
                                        </div>
                                        <span class="count">3</span>
                                    </li>
                                    <li>
                                        <div class="round-icon">
                                            <i class="trav-light-icon"></i>
                                        </div>
                                        <span class="count">37</span>
                                    </li>
                                    <li>
                                        <a href="#" class="info-link">
                                            <div class="round-icon">
                                                <i class="trav-share-fill-icon"></i>
                                            </div>
                                            <span>@lang('profile.share')</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-comment-layer-wrap">
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li class="active">@lang('other.top')</li>
                                        <li>@lang('other.new')</li>
                                    </ul>
                                    <div class="comm-count-info">
                                        3 / 20
                                    </div>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-smile.png" alt="">
                                                    <span>21</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-like.png" alt="">
                                                    <span>19</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-reaction">
                                                    <img src="./assets/image/icon-smile.png" alt="">
                                                    <span>15</span>
                                                </div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                                </div>
                                <div class="post-add-comment-block">
                                    <div class="avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-add-com-input">
                                        <input type="text" placeholder="@lang('comment.write_a_comment')">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- map mode popup -->
    <div class="modal fade white-style" data-backdrop="false" id="mapModePopup" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
            <i class="trav-close-icon"></i>
        </button>
        <div class="modal-dialog modal-custom-style modal-1420" role="document">
            <div class="modal-custom-block">
                <div class="post-block post-mode-map">

                    <div class="map-mode-slide-wrap" id="mapModeSlider">
                        <div class="map-mode-layer">

                            <div class="trip-map-slider" id="tripMapSlider">
                                <div class="trip-map">
                                    <div class="post-map-breadcrumb">
                                        <ul class="breadcrumb-list">
                                            <li><a href="#">@lang('trip.trip_overview')</a></li>
                                            <li><a href="#">Japan</a></li>
                                            <li>@choice('count_day_in', 1, ['count' => 1]) <b>Tokyo</b></li>
                                        </ul>
                                    </div>
                                    <ul class="post-time-trip">
                                        <li class="day">@lang('time.day')</li>
                                        <li class="count">6</li>
                                        <li class="all">@lang('other.all')</li>
                                    </ul>
                                    <div class="trip-map_wrapper">
                                        <img src="./assets/image/popup-map.jpg" alt="map">
                                        <div class="destination-point" style="top:80px;left: 480px;">
                                            <img class="map-marker" src="./assets/image/marker.png" alt="marker">
                                            <img src="http://placehold.it/31x31" alt="dest" class="dest-img">
                                        </div>
                                    </div>
                                    <div class="trip-destination-slider" id="tripDestSlider1">
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-plane-icon"></i>
                                                    <span>Fly - 5,250 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner checked-item">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                    <a href="#" class="show-place-info" data-toggle="modal"
                                                       data-target="#mapModePlacePopup">
                                                        <i class="trav-enlarge2"></i>
                                                    </a>
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Bulgari Tokyo</p>
                                                    <p class="trip-dest-name">Restaurant</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-plane-icon"></i>
                                                    <span>Fly - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-subway-icon"></i>
                                                    <span>Subway - 5,250 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-walk-icon"></i>
                                                    <span>Walk - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-car-icon"></i>
                                                    <span>Drive - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-car-icon"></i>
                                                    <span>Drive - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-plane-icon"></i>
                                                    <span>Fly - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-subway-icon"></i>
                                                    <span>Subway - 5,250 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-walk-icon"></i>
                                                    <span>Walk - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-car-icon"></i>
                                                    <span>Drive - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="trip-map">
                                    <div class="post-map-breadcrumb">
                                        <ul class="breadcrumb-list">
                                            <li><a href="#">@lang('trip.trip_overview')</a></li>
                                            <li><a href="#">Japan</a></li>
                                            <li>@choice('count_day_in', 1, ['count' => 1]) <b>Tokyo</b></li>
                                        </ul>
                                    </div>
                                    <ul class="post-time-trip">
                                        <li class="day">@lang('time.day')</li>
                                        <li class="count">6</li>
                                        <li class="all">@lang('other.all')</li>
                                    </ul>
                                    <div class="trip-map_wrapper">
                                        <img src="./assets/image/popup-map.jpg" alt="map">
                                    </div>
                                    <div class="trip-destination-slider" id="tripDestSlider2">
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-plane-icon"></i>
                                                    <span>Fly - 5,250 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner checked-item">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                    <a href="#" class="show-place-info" data-toggle="modal"
                                                       data-target="#mapModePlacePopup">
                                                        <i class="trav-enlarge2"></i>
                                                    </a>
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Bulgari Tokyo</p>
                                                    <p class="trip-dest-name">Restaurant</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-plane-icon"></i>
                                                    <span>Fly - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-subway-icon"></i>
                                                    <span>Subway - 5,250 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-walk-icon"></i>
                                                    <span>Walk - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-car-icon"></i>
                                                    <span>Drive - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-car-icon"></i>
                                                    <span>Drive - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-plane-icon"></i>
                                                    <span>Fly - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-subway-icon"></i>
                                                    <span>Subway - 5,250 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-walk-icon"></i>
                                                    <span>Walk - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-car-icon"></i>
                                                    <span>Drive - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="trip-map">
                                    <div class="post-map-breadcrumb">
                                        <ul class="breadcrumb-list">
                                            <li><a href="#">@lang('trip.trip_overview')</a></li>
                                            <li><a href="#">Japan</a></li>
                                            <li>@choice('count_day_in', 1, ['count' => 1]) <b>Tokyo</b></li>
                                        </ul>
                                    </div>
                                    <ul class="post-time-trip">
                                        <li class="day">@lang('time.day')</li>
                                        <li class="count">6</li>
                                        <li class="all">@lang('other.all')</li>
                                    </ul>
                                    <div class="trip-map_wrapper">
                                        <img src="./assets/image/popup-map.jpg" alt="map">
                                        <div class="post-map-info-caption map-blue">
                                            <div class="map-avatar">
                                                <img src="http://placehold.it/25x25" alt="ava">
                                            </div>
                                            <div class="map-label-txt">
                                                <b>Suzanne</b> checked on <b>2 Sep</b> at <b>8:30 am</b> and stayed <b>30
                                                    min</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="trip-destination-slider" id="tripDestSlider3">
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-plane-icon"></i>
                                                    <span>Fly - 5,250 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner checked-item">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                    <a href="#" class="show-place-info" data-toggle="modal"
                                                       data-target="#mapModePlacePopup">
                                                        <i class="trav-enlarge2"></i>
                                                    </a>
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Bulgari Tokyo</p>
                                                    <p class="trip-dest-name">Restaurant</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-plane-icon"></i>
                                                    <span>Fly - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-subway-icon"></i>
                                                    <span>Subway - 5,250 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-walk-icon"></i>
                                                    <span>Walk - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-car-icon"></i>
                                                    <span>Drive - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-car-icon"></i>
                                                    <span>Drive - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-plane-icon"></i>
                                                    <span>Fly - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-subway-icon"></i>
                                                    <span>Subway - 5,250 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-walk-icon"></i>
                                                    <span>Walk - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Nadaman Shangri</p>
                                                </div>
                                                <div class="trip-plane-info">
                                                    <i class="trav-car-icon"></i>
                                                    <span>Drive - 7,280 km</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-slide">
                                            <div class="trip-dest-slide_inner">
                                                <div class="trip-dest-flag">
                                                    <img src="http://placehold.it/67x68?text=flag" alt="flag">
                                                </div>
                                                <div class="trip-dest-info">
                                                    <p class="trip-dest-name">Courtyard by</p>
                                                    <p class="trip-dest-name">Marriott Tokyo</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- map popup -->
    <div class="modal fade white-style" data-backdrop="false" id="mapPopup" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-custom-style modal-1080" role="document">
            <div class="modal-custom-block">
                <div class="post-block post-map-modal">
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>

                    <div class="post-MM-block">
                        <div class="post-MM-map" id="modalMapSlider">
                            <div class="post-MM-layer">
                                <div class="map-wrap">
                                    <img src="http://placehold.it/1070x690/f2f2f2?text=map_layer" alt="map">
                                    <div class="map-info-block">
                                        <div class="dest-flag">
                                            <img src="http://placehold.it/51x40" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">Morocco <i class="trav-set-location-icon"></i>
                                                Rabat-sale
                                            </div>
                                            <div class="dest-place">@lang('region.country_in_name', ['name' => 'North Africa'])</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-MM-caption">
                                    <div class="dest-side">
                                        <div class="side-ttl">@lang('trip.previous_destination')</div>
                                        <div class="dest-slide-info">
                                            <p class="dest-home-pos">
                                                <span>@lang('navs.general.home')</span>
                                                <img src="./assets/image/home-flag.svg" alt="flag">
                                            </p>
                                        </div>
                                    </div>
                                    <div class="post-dest-info">
                                        <div class="dest-img">
                                            <img src="http://placehold.it/51x51" alt="dest-image">
                                        </div>
                                        <div class="dest-name">
                                            Rabat-Sale Airport
                                        </div>
                                        <div class="dest-date">
                                            <span class="block-tag">@lang('time.day_value', ['value' => '1'])</span>
                                            <span>Sunday 18 Jun 2017 at 3:00 PM</span>
                                        </div>
                                    </div>
                                    <div class="dest-side">
                                        <div class="side-ttl">Next destination</div>
                                        <div class="dest-slide-info">
                                            <a href="#" class="dest-slide-vector" data-direct="right">
                                                <span>Dubai international airport</span>
                                                <i class="trav-long-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-MM-layer">
                                <div class="map-wrap">
                                    <img src="http://placehold.it/1070x690/f2f2f2?text=map_layer" alt="map">
                                    <div class="map-info-block">
                                        <div class="dest-flag">
                                            <img src="http://placehold.it/51x40" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">Japan <i class="trav-set-location-icon"></i> tokyo
                                            </div>
                                            <div class="dest-place">@lang('region.country_in_name', ['name' => 'East Asia'])</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-MM-caption">
                                    <div class="dest-side">
                                        <div class="side-ttl">@lang('trip.previous_destination')</div>
                                        <div class="dest-slide-info">
                                            <a href="#" class="dest-slide-vector" data-direct="left">
                                                <i class="trav-long-arrow-left"></i>
                                                <span>Tokyo tower</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-dest-info">
                                        <div class="dest-img">
                                            <img src="http://placehold.it/51x51" alt="dest-image">
                                        </div>
                                        <div class="dest-name">
                                            Han no Daidokoro Kadochika
                                            <span class="kind">Restaurant</span>
                                        </div>
                                        <div class="dest-date">
                                            <span class="block-tag">Day 5</span>
                                            <span>Sunday 18 Jun 2017 at 3:00 PM</span>
                                        </div>
                                    </div>
                                    <div class="dest-side">
                                        <div class="side-ttl">Next destination</div>
                                        <div class="dest-slide-info">
                                            <a href="#" class="dest-slide-vector" data-direct="right">
                                                <span>Hilton tokyo</span>
                                                <i class="trav-long-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- place popup -->
    <div class="modal fade white-style" data-backdrop="false" id="placePopup" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
            <i class="trav-close-icon"></i>
        </button>
        <div class="modal-dialog modal-custom-style modal-615" role="document">
            <div class="modal-custom-block">
                <div class="post-block post-modal-place">
                    <div class="top-place-img">
                        <img src="http://placehold.it/615x250" alt="place image">
                    </div>
                    <div class="place-general-block">
                        <div class="place-review-block">
                            <div class="place-info">
                                <div class="place-name">@lang('place.name_airport', ['name' => 'Rabat-sale'])</div>
                                <div class="place-dist">@lang('place.airport_in_place', ['place' => 'Sale, Morocco'])</div>
                            </div>
                            <div class="place-review-layer">
                                <ul class="star-list">
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-half-star-icon"></i></li>
                                </ul>
                                &nbsp;
                                <span class="review-count">547</span>
                                &nbsp;
                                <span>@lang('place.reviews')</span>
                            </div>
                        </div>
                        <div class="place-review-info-main">
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, commodi nobis
                                        quaerat nam impedit deleniti?</p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <address>2145 Hamilton Avenue, San Jose CA 95125</address>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.working_times')</div>
                                <div class="info-txt">
                                    <p>All Week, <b>8:30AM</b> – <b>4PM</b></p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.website')</div>
                                <div class="info-txt">
                                    <a href="www.disneyland.com" target="blank" class="web-site">www.disneyland.com</a>
                                </div>
                            </div>
                            <div class="review-info-block">
                                <div class="info-ttl">@lang('place.popular_times')</div>
                                <div class="info-txt">
                                    <div class="chart-layer">
                                        <img src="http://placehold.it/585x105?text=chart_of_popular_times" alt="chart">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="post-footer-info foot-btn-wrap">
                            <div class="btn-layer">
                                <button class="nav-link btn btn-white-bordered">@lang('trip.book_a_tour')</button>
                            </div>
                            <div class="btn-layer">
                                <button class="nav-link btn btn-light-primary">follow</button>
                                <div class="post-foot-block">
                                    <ul class="foot-avatar-list">
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                            -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                                            -->
                                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                    </ul>
                                    <span>@lang('profile.count_followers', ['count' => 642])</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- comment popup -->
    <div class="modal fade white-style" data-backdrop="false" id="commentPopup" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
            <i class="trav-close-icon"></i>
        </button>
        <div class="modal-dialog modal-custom-style modal-650" role="document">
            <div class="modal-custom-block">
                <div class="post-block post-modal-comment">
                    <div class="post-comment-head">
                        <div class="c-head-txt-wrap">
                            <div class="c-head-txt">
                                <div class="dest-name">
                                    <span>@lang('place.name_airport', ['name' => 'Rabat-sale'])</span>
                                    <span class="block-tag">Airport</span></div>
                                <div class="dest-story">
                                    <b>@lang('time.day_value', ['value' => '1'])</b> / 7 <span
                                            class="dot">&nbsp;·&nbsp;</span>
                                    Mon, 18 Jun 2017<span class="dot">&nbsp;·&nbsp;</span>@ 10:00 PM
                                </div>
                            </div>
                        </div>
                        <div class="c-head-btn-wrap">
                            <button type="button" class="btn btn-light-primary">
                                @lang('buttons.general.follow')
                            </button>
                        </div>
                    </div>
                    <div class="post-comment-main">
                        <div class="dest-pic">
                            <img src="http://placehold.it/590x320" alt="">
                        </div>
                        <div class="desc-info">
                            <i class="trav-clock-icon"></i><span
                                    class="info-block"> @lang('trip.will_spend') <b>$240</b></span>
                            <i class="trav-budget-icon"></i><span
                                    class="info-block"> @lang('trip.planning_to_stay') <b>@lang('time.count_min', ['count' => 40])</b></span>
                        </div>
                        <div class="discussion-block">
                            <span class="disc-ttl">@lang('place.discussion')</span>
                            <span class="disc-count">129</span>
                            <ul class="disc-ava-list">
                                <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                                <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                                <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                                <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                                <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                                <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="post-add-comment-block">
                        <div class="avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-add-com-input">
                            <input type="text" placeholder="@lang('comment.write_a_comment')">
                        </div>
                    </div>
                    <div class="post-comment-layer">
                        <div class="post-comment-top-info">
                            <ul class="comment-filter">
                                <li class="active">@lang('other.top')</li>
                                <li>@lang('other.new')</li>
                            </ul>
                            <div class="comm-count-info">
                                3 / 20
                            </div>
                        </div>
                        <div class="post-comment-wrapper">
                            <div class="post-comment-row">
                                <div class="post-com-avatar-wrap">
                                    <img src="http://placehold.it/45x45" alt="">
                                </div>
                                <div class="post-comment-text">
                                    <div class="post-com-name-layer">
                                        <a href="#" class="comment-name">Katherin</a>
                                        <a href="#" class="comment-nickname">@katherin</a>
                                    </div>
                                    <div class="comment-txt">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore
                                            tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                                    </div>
                                    <div class="comment-bottom-info">
                                        <div class="com-reaction">
                                            <img src="./assets/image/icon-smile.png" alt="">
                                            <span>21</span>
                                        </div>
                                        <div class="com-time">6 hours ago</div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-comment-row">
                                <div class="post-com-avatar-wrap">
                                    <img src="http://placehold.it/45x45" alt="">
                                </div>
                                <div class="post-comment-text">
                                    <div class="post-com-name-layer">
                                        <a href="#" class="comment-name">Amine</a>
                                        <a href="#" class="comment-nickname">@ak0117</a>
                                    </div>
                                    <div class="comment-txt">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                                    </div>
                                    <div class="comment-bottom-info">
                                        <div class="com-reaction">
                                            <img src="./assets/image/icon-like.png" alt="">
                                            <span>19</span>
                                        </div>
                                        <div class="com-time">6 hours ago</div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-comment-row">
                                <div class="post-com-avatar-wrap">
                                    <img src="http://placehold.it/45x45" alt="">
                                </div>
                                <div class="post-comment-text">
                                    <div class="post-com-name-layer">
                                        <a href="#" class="comment-name">Katherin</a>
                                        <a href="#" class="comment-nickname">@katherin</a>
                                    </div>
                                    <div class="comment-txt">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore
                                            tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                                    </div>
                                    <div class="comment-bottom-info">
                                        <div class="com-reaction">
                                            <img src="./assets/image/icon-smile.png" alt="">
                                            <span>15</span>
                                        </div>
                                        <div class="com-time">6 hours ago</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- add to the chat popup -->
    <div class="modal fade white-style" data-backdrop="false" id="addPlacePopup" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
            <i class="trav-close-icon"></i>
        </button>
        <div class="modal-dialog modal-custom-style modal-650" role="document">
            <div class="modal-custom-block">
                <div class="post-block post-modal-add-place">
                    <h3 class="place-title">@lang('chat.add_content_to_the_chat')</h3>
                    <!-- step 1 -->
                    <div class="search-block-wrap">
                        <div class="search-block-inner">
                            <div class="search-block">
                                <input type="text" class="" placeholder="@lang('trip.search_places_hotels_cities')"
                                       id="placeSearchInput2" autocomplete="off">
                            </div>
                            <div class="search-drop-place">
                                <div class="results-block-wrap">
                                    <div class="sugg-inner">
                                        <div class="suggestion-block">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/60x60" alt="image">
                                            </div>
                                            <div class="sugg-content">
                                                <div class="sugg-place">Osaka</div>
                                                <div class="sugg-place-info">@lang('region.city_in_name', ['name' => 'Japan'])</div>
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                                                            <b>4.8</b>
                                                        </span>
                                                    <span>@lang('profile.from_count_reviews', ['count' => 26])</span>
                                                </div>
                                            </div>
                                            <div class="sugg-btn-wrap">
                                                <button type="button" class="btn btn-light-primary btn-bordered">
                                                    <span>+</span> @lang('other.add')
                                                </button>
                                            </div>
                                        </div>
                                        <div class="suggestion-block">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/60x60" alt="image">
                                            </div>
                                            <div class="sugg-content">
                                                <div class="sugg-place">@lang('place.name_view_hotel', ['name' => 'Asakusa'])</div>
                                                <div class="sugg-place-info">@lang('place.hotel_in_place', ['place' => 'Tokyo, Japan'])</div>
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                                                            <b>4.8</b>
                                                        </span>
                                                    <span>@lang('profile.from_count_reviews', ['count' => 26])</span>
                                                </div>
                                            </div>
                                            <div class="sugg-btn-wrap">
                                                <button type="button" class="btn btn-light-primary btn-bordered">
                                                    <span>+</span> @lang('other.add')
                                                </button>
                                            </div>
                                        </div>
                                        <div class="suggestion-block">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/60x60" alt="image">
                                            </div>
                                            <div class="sugg-content">
                                                <div class="sugg-place">Morocco</div>
                                                <div class="sugg-place-info">@lang('region.country_in_name', ['name' => 'North Africa'])</div>
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                                                            <b>4.8</b>
                                                        </span>
                                                    <span>@lang('profile.from_count_reviews', ['count' => 26])</span>
                                                </div>
                                            </div>
                                            <div class="sugg-btn-wrap">
                                                <button type="button" class="btn btn-light-primary btn-bordered">
                                                    <span>+</span> @lang('other.add')
                                                </button>
                                            </div>
                                        </div>
                                        <div class="suggestion-block">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/60x60" alt="image">
                                            </div>
                                            <div class="sugg-content">
                                                <div class="sugg-place">Paris</div>
                                                <div class="sugg-place-info">@lang('region.capital_of_place', ['place' => 'France'])</div>
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                                                            <b>4.8</b>
                                                        </span>
                                                    <span>@lang('profile.from_count_reviews', ['count' => 26])</span>
                                                </div>
                                            </div>
                                            <div class="sugg-btn-wrap">
                                                <button type="button" class="btn btn-light-primary btn-bordered">
                                                    <span>+</span> @lang('other.add')
                                                </button>
                                            </div>
                                        </div>
                                        <div class="suggestion-block">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/60x60" alt="image">
                                            </div>
                                            <div class="sugg-content">
                                                <div class="sugg-place">Nagoya city science museum</div>
                                                <div class="sugg-place-info">Museum</div>
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                                                            <b>4.8</b>
                                                        </span>
                                                    <span>@lang('profile.from_count_reviews', ['count' => 26])</span>
                                                </div>
                                            </div>
                                            <div class="sugg-btn-wrap">
                                                <button type="button" class="btn btn-light-primary btn-bordered">
                                                    <span>+</span> @lang('other.add')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="suggestion-wrapper">
                        <p class="sugg-ttl">@lang('trip.suggestions')</p>
                        <div class="sugg-inner">
                            <div class="suggestion-block">
                                <div class="img-wrap">
                                    <img src="http://placehold.it/60x60" alt="image">
                                </div>
                                <div class="sugg-content">
                                    <div class="sugg-place">Nagoya castle</div>
                                    <div class="sugg-place-info">Monument</div>
                                    <div class="com-star-block">
                                        <ul class="com-star-list">
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                        </ul>
                                        <span class="count">
                                                <b>4.8</b>
                                            </span>
                                        <span>@lang('profile.from_count_reviews', ['count' => 26])</span>
                                    </div>
                                </div>
                                <div class="sugg-btn-wrap">
                                    <button type="button" class="btn btn-light-primary btn-bordered">
                                        <span>+</span> @lang('other.add')
                                    </button>
                                </div>
                            </div>
                            <div class="suggestion-block">
                                <div class="img-wrap">
                                    <img src="http://placehold.it/60x60" alt="image">
                                </div>
                                <div class="sugg-content">
                                    <div class="sugg-place">Port of nagoya public aquarium</div>
                                    <div class="sugg-place-info">Aquarium</div>
                                    <div class="com-star-block">
                                        <ul class="com-star-list">
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                        </ul>
                                        <span class="count">
                                                <b>4.8</b>
                                            </span>
                                        <span>@lang('profile.from_count_reviews', ['count' => 26])</span>
                                    </div>
                                </div>
                                <div class="sugg-btn-wrap">
                                    <button type="button" class="btn btn-light-primary btn-bordered">
                                        <span>+</span> @lang('other.add')
                                    </button>
                                </div>
                            </div>
                            <div class="suggestion-block">
                                <div class="img-wrap">
                                    <img src="http://placehold.it/60x60" alt="image">
                                </div>
                                <div class="sugg-content">
                                    <div class="sugg-place">Nagoya TV tower</div>
                                    <div class="sugg-place-info">Tower</div>
                                    <div class="com-star-block">
                                        <ul class="com-star-list">
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                        </ul>
                                        <span class="count">
                                                <b>4.8</b>
                                            </span>
                                        <span>@lang('profile.from_count_reviews', ['count' => 26])</span>
                                    </div>
                                </div>
                                <div class="sugg-btn-wrap">
                                    <button type="button" class="btn btn-light-primary btn-bordered">
                                        <span>+</span> @lang('other.add')
                                    </button>
                                </div>
                            </div>
                            <div class="suggestion-block">
                                <div class="img-wrap">
                                    <img src="http://placehold.it/60x60" alt="image">
                                </div>
                                <div class="sugg-content">
                                    <div class="sugg-place">Nagoya city science museum</div>
                                    <div class="sugg-place-info">Museum</div>
                                    <div class="com-star-block">
                                        <ul class="com-star-list">
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                            <li><i class="trav-star-icon"></i></li>
                                        </ul>
                                        <span class="count">
                                                <b>4.8</b>
                                            </span>
                                        <span>@lang('profile.from_count_reviews', ['count' => 26])</span>
                                    </div>
                                </div>
                                <div class="sugg-btn-wrap">
                                    <button type="button" class="btn btn-light-primary btn-bordered">
                                        <span>+</span> @lang('other.add')
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- step 2 -->
                    <div class="adding-place-block">
                        <div class="add-form">
                            <div class="form-row">
                                <div class="field-style flex-field">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/54x54" alt="photo">
                                    </div>
                                    <div class="field-txt">
                                        Nagoya TV Tower
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="field-style">
                                    <div class="field-label">When you suggest to check-in?</div>
                                    <div class="flex-field">
                                        <div class="flex-item">
                                            <div class="field-ttl">@lang('time.date')</div>
                                            <div class="date-inner">
                                                19 October 2017
                                            </div>
                                        </div>
                                        <div class="flex-item">
                                            <div class="field-ttl">@lang('time.time')</div>
                                            <div class="date-inner">
                                                9:20 PM
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="field-style">
                                    <div class="field-label">@lang('trip.planning_to_stay')</div>
                                    <div class="flex-field">
                                        <div class="flex-item">
                                            <div class="field-ttl">@lang('time.hours')</div>
                                            <div class="time-count">
                                                <span class="click">-</span>
                                                <input type="text" name="hour" value="2">
                                                <span class="click">+</span>
                                            </div>
                                        </div>
                                        <div class="flex-item">
                                            <div class="field-ttl">@lang('time.minutes')</div>
                                            <div class="time-count">
                                                <span class="click">-</span>
                                                <input type="text" name="minute" value="30">
                                                <span class="click">+</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="field-style">
                                    <div class="field-label">@lang('trip.how_much_you_are_expecting_to_spend')</div>
                                    <div class="flex-field">
                                        <div class="flex-item amount-input">
                                            <input type="text" name="enter_amount"
                                                   placeholder="@lang('other.enter_amount')">
                                        </div>
                                        <div class="flex-item radio-field">
                                            <input type="radio" name="expect_spend" id="radio-1">
                                            <label class="check-field" for="radio-1">
                                                50$
                                            </label>
                                        </div>
                                        <div class="flex-item radio-field">
                                            <input type="radio" name="expect_spend" checked="checked" id="radio-2">
                                            <label class="check-field" for="radio-2">
                                                100$
                                            </label>
                                        </div>
                                        <div class="flex-item radio-field">
                                            <input type="radio" name="expect_spend" id="radio-3">
                                            <label class="check-field" for="radio-3">
                                                200$
                                            </label>
                                        </div>
                                    </div>
                                    <div class="field-subtext">
                                        @lang('trip.spending_so_far') <b>$500</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="add-footer">
                            <button type="button"
                                    class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                            <button type="button" class="btn btn-light-primary btn-bordered">@lang('other.add')</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('site/trip/partials/invite-friends-popup')
    @if(isset($version))
        @include('site/trip/partials/respond-to-version-popup')
    @endif


    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>

    <script src="{{url('assets2/js/script.js')}}"></script>

    <script src="{{asset('assets2/js/zabuto_calendar.min.js')}}"></script>
    <!-- initialize the calendar on ready -->
    <script type="application/javascript">

        $(document).ready(function () {
            var eventData = [
                    @foreach($trip_places_info AS $ctpi)
                {
                    "date": "{{$ctpi[0]->date}}", "badge": false, "title": "{{$ctpi[0]->city->transsingle->title}}"
                },
                @endforeach

            ];
            $("#tripCalendar").zabuto_calendar({
                data: eventData,
                today: false,
                show_days: false,
                nav_icon: {
                    prev: '<i class="fa fa-chevron-circle-left"></i>',
                    next: '<i class="fa fa-chevron-circle-right"></i>'
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {


            $('body').on('click', '.btnInviteFriend', function (e) {
                var user_id = $(this).attr('data-id');
                var trip_id = {{$trip_info->id}};

                $(this).html('Unselect');
                $(this).removeClass("btnInviteFriend");
                $(this).removeClass("btn-light-grey");
                $(this).addClass("btn-light-primary");
                $(this).addClass("btnUninviteFriend");

                $("#people-ava-list").append("<li id='user_" + user_id + "'><a href='#'><img src='{{check_profile_picture_id(1)}}' alt='avatar' style='width:50px;height:50px'></a><input type='hidden' name='user_id[]' value='" + user_id + "' /></li>");

                e.preventDefault()
            });

            $('body').on('click', '.btnUninviteFriend', function (e) {
                var user_id = $(this).attr('data-id');
                var trip_id = {{$trip_info->id}};

                $(this).html('Select');
                $(this).addClass("btnInviteFriend");
                $(this).addClass("btn-light-grey");
                $(this).removeClass("btn-light-primary");
                $(this).removeClass("btnUninviteFriend");

                $("#user_" + user_id).remove();


                e.preventDefault()
            });

            $('body').on('submit', '#InviteFriendsForm', function (e) {
                var trip_id = $("#trip_id").val();
                var values = $(this).serialize();
                //console.log(values);
                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxInviteFriends')}}",
                    data: values
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == "success") {
                            console.log(result);
                            $('#invitePopup').modal('toggle');
                            alert(result.message);
                        }


                    });
                e.preventDefault();
            });
        });
    </script>
    <style type="text/css">
        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Style the buttons that are used to open the tab content */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }

        .tabcontent {
            animation: fadeEffect 1s; /* Fading effect takes 1 second */
        }

        /* Go from zero to full opacity */
        @keyframes fadeEffect {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }
    </style>

    <script type="text/javascript">
        function openTab(evt, tabName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the button that opened the tab
            document.getElementById(tabName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        openTab(event, 'overview');
    </script>

</body>
</html>