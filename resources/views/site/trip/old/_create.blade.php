<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-j8y0ITrvFafF4EkV1mPW0BKm6dp3c+J9Fky22Man50Ofxo2wNe5pT1oZejDH9/Dt" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('assets2/js/timepicker/jquery.timepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets2/css/style.css')}}">
    <title>Travooo - create trip plan</title>
    <style>
        .editCityIcon {
            display: none;
        }

        .trip-place-name:hover .editCityIcon {
            display: block;
        }

        .editPlanIcon {
            display: none;
        }

        .editPlanTitle:hover .editPlanIcon {
            display: block;
        }

        .editPlaceIcon {
            display: none;
        }

        .trip-side-row:hover .editPlaceIcon {
            display: block;
        }
    </style>
</head>

<body>

<div class="main-wrapper">
    <header class="main-header">
        <div class="container-fluid">
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="trav-bars"></i>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="{{url('assets2/image/main-circle-logo.png')}}" alt="">
                    <span>Travooo</span>
                    <span class="blue">@lang('trip.trip_planner')</span>
                </a>

                <div class="collapse navbar-collapse create-navbar" id="navbarSupportedContent">
                    <form method="post" id="updateTripForm">
                        <ul class="navbar-nav create-menu">

                            @if(!$im_invited)
                                <li class="nav-item">
                                    <a class="nav-link confirm-trip-deletion"
                                       href="#">@lang('buttons.general.cancel')</a>
                                </li>
                            @else
                                <li class="nav-item" style='font-size:13px;'>
                                    <input type='checkbox' name='send_back' id='send_back' value='1'/> Send your version
                                    back to {{$trip->author->name}}
                                </li>
                            @endif


                            <li class="nav-item">
                                <a class="nav-link btn btn-light-primary" href="#"
                                   id="save_trip">@lang('trip.save_plan')</a>

                            </li>
                            <li class="nav-item">
                                <a class="profile-link" href="#">
                                    <img src="{{check_profile_picture($me->profile_picture)}}" alt=""
                                         style="width:36px;height:36px;">
                                </a>
                            </li>
                        </ul>
                        <input type="hidden" name="trip_id" id="trip_id" value="{{$trip_id}}"/>
                        <input type="hidden" name="version_id" id="version_id" value="{{$my_version_id}}"/>
                    </form>
                </div>
            </nav>
        </div>
    </header>

    <div class="create-trip-wrapper">
        <div class="trip-side">
            <div class="trip-side-content">

                <!-- FIRST SHOW COMPLETED CITIES, AND PLACES INSIDE THEM -->
                @if(isset($trips_cities_complete))
                    @foreach($trips_cities_complete AS $cities_complete)
                        <div class="trip-side-block
                             @if($cities_complete->transportation=="plane")
                                airplane
@elseif($cities_complete->transportation=="car")
                                overland-trip
@elseif($cities_complete->transportation=="railway")
                                trav-railway-icon
@elseif($cities_complete->transportation=="bus")
                        @elseif($cities_complete->transportation=="bicycle")
                        @elseif($cities_complete->transportation=="walk")
                        @elseif($cities_complete->transportation=="ship")
                        @endif
                                ">
                            <div class="trip-country-name">{{$cities_complete->city->country->transsingle->title}}</div>
                            <div class="trip-place-name">
                                <span class="name">
                                    {{$cities_complete->city->transsingle->title}}

                                </span>
                                &nbsp;
                                <a href="#" class="drag-icon editCityIcon" data-id="{{$cities_complete->id}}"
                                   style="text-align: right;top:2px;right:2px;width:16px;height:16px;">
                                    <i class="fal fa-edit"></i>
                                </a>
                            </div>
                            <div class="trip-side-layer-wrap">
                                <?php
                                $trip_places_inside_city = \App\Models\TripPlaces\TripPlaces::where('trips_id', $cities_complete->trips_id)
                                    ->where('cities_id', $cities_complete->cities_id)
                                    ->where('versions_id', $my_version_id)
                                    ->get();
                                ?>
                                @foreach($trip_places_inside_city AS $trip_place_inside_city)
                                    @if($trip_place_inside_city->active)

                                        <div class="trip-side-layer" id="TripPlace{{$trip_place_inside_city->id}}">
                                            <div class="trip-side-row">

                                                <a href="#" class="drag-icon editPlaceIcon"
                                                   data-id="{{$trip_place_inside_city->id}}"
                                                   style="text-align: right;top:2px;right:2px;width:16px;height:16px;">
                                                    <i class="fal fa-edit"></i>
                                                </a>

                                                <div class="img-wrap">
                                                    <img src="{{check_place_photo($trip_place_inside_city->place)}}"
                                                         alt="photo" style="width:64px;height:60px;">
                                                </div>
                                                <div class="trip-content">
                                                    <div class="trip-txt-line">
                                                        <div class="line-txt place-info">
                                                            <p class="dest-name"
                                                               style="max-width: 200px;">{{@$trip_place_inside_city->place->transsingle->title}}</p>
                                                        </div>
                                                        <div class="line-time">
                                                            {{weatherDate($trip_place_inside_city->time)}}&nbsp;
                                                            @ {{weatherTime($trip_place_inside_city->time)}}
                                                        </div>
                                                    </div>
                                                    <div class="trip-txt-line">
                                                        <div class="line-txt">
                                                            @lang('trip.will_spend')
                                                            <b>${{$trip_place_inside_city->budget}}</b>
                                                            <span class="dot">·</span>
                                                            @lang('trip.planning_to_stay')
                                                            <b>{{$trip_place_inside_city->duration}}
                                                                @lang('time.min')</b>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="adding-place-block">
                                            <form method='post' id='activatePlace' autocomplete="off">
                                                <div class="add-form">
                                                    <div class="form-row">
                                                        <div class="field-style flex-field">
                                                            <div class="img-wrap">
                                                                <img src="{{check_place_photo($trip_place_inside_city->place)}}"
                                                                     alt="photo" style="width:54px;height:54px;">
                                                            </div>
                                                            <div class="field-txt">
                                                                {{@$trip_place_inside_city->place->transsingle->title}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="field-style">
                                                            <div class="field-label">@lang('trip.when_you_will_check_in')</div>
                                                            <div class="flex-field">
                                                                <div class="flex-item">
                                                                    <div class="field-ttl">@lang('time.date')</div>
                                                                    <div class="date-inner">
                                                                        <input type='text' name='date'
                                                                               class='datepicker_place' required
                                                                               value="{{$trip_place_inside_city->date!='0000-00-00' ? date('d F Y', strtotime($trip_place_inside_city->date)) : ''}}"/>
                                                                        <input type='hidden' name='actual_place_date'
                                                                               id='actual_place_date'
                                                                               value="{{$trip_place_inside_city->date ? $trip_place_inside_city->date : ''}}"/>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-item">
                                                                    <div class="field-ttl">@lang('time.time')</div>
                                                                    <div class="date-inner">
                                                                        <input type='text' name='time'
                                                                               class='timepicker'
                                                                               value="{{$trip_place_inside_city->time!='' ? date('g:ia', strtotime($trip_place_inside_city->time)) : ''}}"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="field-style">
                                                            <div class="field-label">@lang('trip.planning_to_stay')</div>
                                                            <div class="flex-field">
                                                                <div class="flex-item">
                                                                    <div class="field-ttl">@lang('time.hours')</div>
                                                                    <div class="time-count">
                                                                        <span class="click"
                                                                              onclick="doMinus('duration_hours')">-</span>
                                                                        <input type="text" name="duration_hours"
                                                                               id="duration_hours"
                                                                               value="{{@durationInHours($trip_place_inside_city->duration)}}">
                                                                        <span class="click"
                                                                              onclick="doPlus('duration_hours')">+</span>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-item">
                                                                    <div class="field-ttl">@lang('time.minutes')</div>
                                                                    <div class="time-count">
                                                                        <span class="click"
                                                                              onclick="doMinus('duration_minutes')">-</span>
                                                                        <input type="text" name="duration_minutes"
                                                                               id="duration_minutes"
                                                                               value="{{@durationInMinutes($trip_place_inside_city->duration)}}">
                                                                        <span class="click"
                                                                              onclick="doPlus('duration_minutes')">+</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="field-style">
                                                            <div class="field-label">How much you are expecting to
                                                                spend
                                                            </div>
                                                            <div class="flex-field">
                                                                <div class="flex-item amount-input">
                                                                    <input type="text" name="budget" id="budget_custom"
                                                                           placeholder="@lang('other.enter_amount')"
                                                                           value="{{@$trip_place_inside_city->budget}}">
                                                                </div>
                                                                <div class="flex-item radio-field">
                                                                    <input type="radio" name="budget" id="radio-1"
                                                                           value="50"
                                                                           @if(isset($trip_place_inside_city->budget) AND $trip_place_inside_city->budget==50) checked @endif>
                                                                    <label class="check-field" for="radio-1">
                                                                        50$
                                                                    </label>
                                                                </div>
                                                                <div class="flex-item radio-field">
                                                                    <input type="radio" name="budget" id="radio-2"
                                                                           value="100"
                                                                           @if(isset($trip_place_inside_city->budget) AND $trip_place_inside_city->budget==100) checked @endif>
                                                                    <label class="check-field" for="radio-2">
                                                                        100$
                                                                    </label>
                                                                </div>
                                                                <div class="flex-item radio-field">
                                                                    <input type="radio" name="budget" id="radio-3"
                                                                           value="200"
                                                                           @if(isset($trip_place_inside_city->budget) AND $trip_place_inside_city->budget==200) checked @endif>
                                                                    <label class="check-field" for="radio-3">
                                                                        200$
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="field-subtext">
                                                                @lang('trip.spending_so_far') <b>${{$budget_so_far}}</b>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="add-footer">
                                                    <input type='hidden' name='trip_place_id'
                                                           value='{{$trip_place_inside_city->id}}'/>
                                                    <input type='hidden' name='trip_id'
                                                           value='{{$trip_place_inside_city->trips_id}}'/>
                                                    <input type='hidden' name='version_id'
                                                           value='{{$trip_place_inside_city->trip->myversion()[0]->id}}'/>
                                                    <input type='hidden' name='city_id'
                                                           value='{{$trip_place_inside_city->cities_id}}'/>
                                                    <input type='hidden' name='place_id'
                                                           value='{{$trip_place_inside_city->places_id}}'/>
                                                    <input type='hidden' name='version_id' value='{{$my_version_id}}'/>
                                                    <button type="button" class="btn btn-transp btn-clear"
                                                            id="removePlace"
                                                            data-id="{{@$trip_place_inside_city->place->id}}">@lang('buttons.general.crud.delete')
                                                    </button>
                                                    <button type="submit" name='submit'
                                                            class="btn btn-light-primary btn-bordered">@lang('buttons.general.save')
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                @endif

            <!-- THEN SHOW INCOMPLETE CITIES, AND PLACES INSIDE THEM -->
                @if(isset($trips_cities_incomplete))
                    @foreach($trips_cities_incomplete AS $cities_incomplete)

                        <div class="trip-side-block add-place">
                            <div class="trip-side-layer-wrap">
                                <div class="adding-place-block arrow-left">
                                    <div class="add-form">
                                        <div class="form-row">
                                            <div class="field-style flex-field">
                                                <div class="img-wrap">
                                                    <img src="@if(isset($cities_incomplete->city->medias[0]->url)) {{$cities_incomplete->city->medias[0]->url}} @else {{asset('assets2/image/placeholders/place.png')}} @endif"
                                                         alt="photo" style='width:54px;height:54px;'>
                                                </div>
                                                <div class="field-txt">
                                                    {{$cities_incomplete->city->transsingle->title}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="field-style">
                                                <div class="field-label">@lang('trip.how_you_will_arrive_here')</div>
                                                <div class="flex-field">
                                                    <div class="flex-item radio-field">
                                                        <input type="radio" name="transportation" id="arrive_radio-1"
                                                               @if($cities_incomplete->transportation=="plane" OR $cities_incomplete->transportation=="") checked
                                                               @endif value='plane'>
                                                        <label class="check-field" for="arrive_radio-1">
                                                            <i class="trav-angle-plane-icon"></i>
                                                        </label>
                                                    </div>
                                                    <div class="flex-item radio-field">
                                                        <input type="radio" name="transportation" id="arrive_radio-2"
                                                               @if($cities_incomplete->transportation=="car") checked
                                                               @endif value='car'>
                                                        <label class="check-field" for="arrive_radio-2">
                                                            <i class="trav-car-icon"></i>
                                                        </label>
                                                    </div>
                                                    <div class="flex-item radio-field">
                                                        <input type="radio" name="transportation" id="arrive_radio-3"
                                                               @if($cities_incomplete->transportation=="railway") checked
                                                               @endif value='railway'>
                                                        <label class="check-field" for="arrive_radio-3">
                                                            <i class="trav-railway-icon"></i>
                                                        </label>
                                                    </div>
                                                    <div class="flex-item radio-field">
                                                        <input type="radio" name="transportation" id="arrive_radio-4"
                                                               @if($cities_incomplete->transportation=="bus") checked
                                                               @endif value='bus'>
                                                        <label class="check-field" for="arrive_radio-4">
                                                            <i class="trav-bus-icon"></i>
                                                        </label>
                                                    </div>
                                                    <div class="flex-item radio-field">
                                                        <input type="radio" name="transportation" id="arrive_radio-5"
                                                               @if($cities_incomplete->transportation=="bicycle") checked
                                                               @endif value='bicycle'>
                                                        <label class="check-field" for="arrive_radio-5">
                                                            <i class="trav-cycle-icon"></i>
                                                        </label>
                                                    </div>
                                                    <div class="flex-item radio-field">
                                                        <input type="radio" name="transportation" id="arrive_radio-6"
                                                               @if($cities_incomplete->transportation=="walk") checked
                                                               @endif value='walk'>
                                                        <label class="check-field" for="arrive_radio-6">
                                                            <i class="trav-walk-icon-2"></i>
                                                        </label>
                                                    </div>
                                                    <div class="flex-item radio-field">
                                                        <input type="radio" name="transportation" id="arrive_radio-7"
                                                               @if($cities_incomplete->transportation=="ship") checked
                                                               @endif value='ship'>
                                                        <label class="check-field" for="arrive_radio-7">
                                                            <i class="trav-ship-icon"></i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add-footer">
                                        <button type="button" class="btn btn-transp btn-clear"
                                                id="removeCity">@lang('buttons.general.crud.delete')
                                        </button>
                                        <button type="button" class="btn btn-light-primary btn-bordered activateCity"
                                                data-id='{{$cities_incomplete->id}}'>@lang('buttons.general.save')
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                @endif

            </div>

            <div class="trip-side-button-block">
                <button class="btn btn-transp btn-bordered" data-toggle="modal" data-target="#addPlacePopup"
                        @if(!isset($city) OR !is_object($city)) disabled @endif>
                    <i class="trav-add-place-icon"></i>
                    <span>Add a Place in {{@$city->transsingle->title}}</span>
                </button>
                <button class="btn btn-transp btn-bordered" data-toggle="modal" data-target="#addCityPopup">
                    <i class="trav-move-new-icon"></i>
                    <span>Move to a new Country or City</span>
                </button>
            </div>
        </div>
        <div class="trip-map-block">
            <!--
            <div class="trip-top-timeline">
                <div class="timeline-layer">
                    <img src="http://placehold.it/800x50?text=timeline+place:https://timeline.knightlab.com" alt="timeline">
                </div>
                <div class="day-slider">
                    <ul class="calendar-slider" id="createTripCalendarSlider">
                        <li>
                            <div class="number-day">16</div>
                            <div class="month">oct</div>
                        </li>
                        <li>
                            <div class="number-day">17</div>
                            <div class="month">oct</div>
                        </li>
                        <li class="active">
                            <div class="number-day">18</div>
                            <div class="month">oct</div>
                        </li>
                        <li>
                            <div class="number-day">19</div>
                            <div class="month">oct</div>
                        </li>
                        <li>
                            <div class="number-day">20</div>
                            <div class="month">oct</div>
                        </li>
                        <li>
                            <div class="number-day">21</div>
                            <div class="month">oct</div>
                        </li>
                    </ul>
                    <ul class="calendar-link-list">
                        <li>
                            <a href="#" class="prevDay"><i class="trav-angle-left"></i></a>
                        </li>
                        <li>
                            <span class="dot"></span>
                        </li>
                        <li>
                            <a href="#" class="nextDay"><i class="trav-angle-right"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="timeline-btn-wrap">
                    <button class="btn btn-light-bg-grey btn-bordered">Next Day</button>
                </div>
            </div>
            -->

            <div class="trip-map-layer">
                <div id="map" style="width:100%"></div>

            </div>
        </div>
    </div>
</div>

<!-- modals -->
@include('site.trip.partials.select-city-popup')
@include('site/trip/partials/select-place-popup')
@include('site/trip/partials/first-step-popup')

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqTXNqPdQlFIV5QNvYQeDrJ5vH0y9_D-M&callback=initMap&language=en"></script>
<script src="{{asset('assets2/js/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.js')}}"></script>
<script src="{{asset('assets2/js/html2canvas/html2canvas.min.js')}}"></script>
<script src="{{asset('assets2/js/script.js')}}"></script>
<script>
    @if (isset($goto_first_step) AND $goto_first_step)
    $(document).ready(function () {
        $('#createTripPlanStep').modal('show');

    });
    @endif
    @if (isset($do) AND $do=="add-city")
    $(document).ready(function () {
        $('#addCityPopup').modal('show');

    });
    @endif
    @if (isset($city) AND isset($do) AND $do=="add-place")
    $(document).ready(function () {
        $('#addPlacePopup').modal('show');

    });
    @endif


    $('input[name=budget]').on('click', function (e) {
        $('#budget_custom').val($(this).val());

    });
    $('body').on('click', '#save_trip', function (e) {
        var trip_id = $('#trip_id').val();
        var version_id = $('#version_id').val();
        if ($('#send_back').is(":checked")) {
            var send_back = $('#send_back').val();
        }

        $.ajax({
            method: "POST",
            url: "{{url('trip/ajaxActivateTrip')}}",
            data: {"trip_id": trip_id, "version_id": version_id, "send_back": send_back}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                console.log(result);
                if (result.status == 'success') {
                    //console.log(result.trip_city_id);
                    window.location.replace("{{url('trip/view')}}" + "/" + trip_id);
                } else if (result.status == 'error_unknow') {
                    alert('{{ __("trip.unknown_error_while_deleting_trip_plan") }}');
                } else if (result.status == 'error_not_enough_data') {
                    alert('{{ __("trip.you_need_to_add_at_least_one_place_to_publish_your_trip") }}');
                }

            });
        e.preventDefault();
    });

    $(document).ready(function () {

        $('body').on('submit', '#createTrip', function (e) {
            var values = $(this).serialize();
            //console.log(values);
            $.ajax({
                method: "POST",
                url: "{{url('trip/ajaxFirstStep')}}",
                data: values
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (result.status == 'success') {
                        console.log(result.trip_id);
                        window.location.replace("{{url('trip/plan')}}" + "/" + result.trip_id + "?do=add-city");
                    } else {

                    }


                });
            e.preventDefault();
        });

        @if(isset($city) && is_object($city))
        $('body').on('submit', '#activatePlace', function (e) {
            var values = $(this).serialize();
            console.log(values);
            $.ajax({
                method: "POST",
                url: "{{url('trip/ajaxActivateTripPlace')}}",
                data: values
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (result.status == 'success') {
                        console.log(result.trip_id);
                        window.location.replace("{{url('trip/plan')}}" + "/"+{{$trip_id}}+
                        "?city_id=" +{{$city->id}})
                        ;
                    } else {

                    }


                });
            e.preventDefault();
        });

        @endif


        function delay(callback, ms) {
            var timer = 0;
            return function () {
                var context = this, args = arguments;
                clearTimeout(timer);
                timer = setTimeout(function () {
                    callback.apply(context, args);
                }, ms || 0);
            };
        }

        $('body').on('keyup', '#citySearchInput', delay(function (e) {
            var value = $(this).val();
            if (value.length >= 2) {
                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxSearchCities')}}",
                    data: {"query": value}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        //console.log(result);
                        $('#sugg-inner').html(result);

                    });
            }
            e.preventDefault();
        }, 500));


        @if(isset($trip))
        $('body').on('click', '.doAddCity', function (e) {
            var city_id = $(this).attr('data-id');
            var trip_id = {{$trip->id}};
            var version_id = {{$trip->myversion()[0]->id}};

            $.ajax({
                method: "POST",
                url: "{{url('trip/ajaxAddCityToTrip')}}",
                data: {"city_id": city_id, "trip_id": trip_id, "version_id": version_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.status == 'success') {
                        console.log(result.trip_city_id);
                        window.location.replace("{{url('trip/plan')}}" + "/" + trip_id + "?do=add-place&city_id=" + city_id);
                    } else {

                    }

                });
            e.preventDefault();
        });
        @endif


        $('body').on('keyup', '#placeSearchInput', delay(function (e) {
            var value = $(this).val();
            if (value.length >= 2) {
                        @if(isset($city))
                var city_id = {{$city->id}};
                @endif
                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxSearchPlaces')}}",
                    data: {"query": value, "city_id": city_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        console.log(result);
                        $('#place-sugg-inner').html(result);

                    });
                e.preventDefault();
            }
        }, 500));
        @if(isset($city) AND isset($trip))
        $('body').on('click', '.doAddPlace', function (e) {
            var place_id = $(this).attr('data-id');
            var trip_id = {{$trip->id}};
            var city_id = {{$city->id}};
            var version_id = {{$trip->myversion()[0]->id}};

            $.ajax({
                method: "POST",
                url: "{{url('trip/ajaxAddPlaceToTrip')}}",
                data: {"city_id": city_id, "trip_id": trip_id, "place_id": place_id, "version_id": version_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.status == 'success') {
                        console.log(result.trip_city_id);
                        window.location.replace("{{url('trip/plan')}}" + "/" + trip_id + "?city_id=" + city_id);
                    } else {

                    }

                });
            e.preventDefault();
        });

        $('body').on('click', '#removePlace', function (e) {
            var place_id = $(this).attr('data-id');
            var trip_id = {{$trip->id}};
            var city_id = {{$city->id}};
            var version_id = {{$trip->myversion()[0]->id}};

            $.ajax({
                method: "POST",
                url: "{{url('trip/ajaxRemovePlaceFromTrip')}}",
                data: {"city_id": city_id, "trip_id": trip_id, "place_id": place_id, "version_id": version_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.status == 'success') {
                        console.log(result.trip_city_id);
                        window.location.replace("{{url('trip/plan')}}" + "/" + trip_id + "?city_id=" + city_id);
                    } else {

                    }

                });
            e.preventDefault();
        });
        @endif

        @if(isset($city) && is_object($city))
        $('body').on('click', '.activateCity', function (e) {
            var trip_city_id = $(this).attr('data-id');
            var transportation = $("input[name='transportation']:checked").val();
            var version_id = {{$trip->myversion()[0]->id}};

            $.ajax({
                method: "POST",
                url: "{{url('trip/ajaxActivateTripCity')}}",
                data: {"trip_city_id": trip_city_id, "transportation": transportation, "version_id": version_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (result.status == 'success') {
                        //console.log(result.trip_city_id);
                        window.location.replace("{{url('trip/plan')}}" + "/"+{{$trip_id}}+
                        "?city_id=" +{{$city->id}})
                        ;
                    } else {

                    }

                });
            e.preventDefault();
        });
        @endif

        @if(isset($trip) && is_object($trip->myversion()[0]) && isset($city))
        $('body').on('click', '.editPlaceIcon', function (e) {
            var trip_place_id = $(this).attr('data-id');
            var version_id = {{$trip->myversion()[0]->id}};

            $.ajax({
                method: "POST",
                url: "{{url('trip/ajaxDeActivateTripPlace')}}",
                data: {"trip_place_id": trip_place_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (result.status == 'success') {
                        console.log(result.trip_id);
                        window.location.replace("{{url('trip/plan')}}" + "/"+{{$trip_id}}+
                        "?city_id=" +{{$city->id}})
                        ;
                    } else {

                    }


                });
            e.preventDefault();
        });
        @endif

        @if(isset($trip) && is_object($trip->myversion()[0]) && isset($city))

        $('body').on('click', '.editCityIcon', function (e) {
            var trip_city_id = $(this).attr('data-id');
            var version_id = {{$trip->myversion()[0]->id}};

            $.ajax({
                method: "POST",
                url: "{{url('trip/ajaxDeActivateTripCity')}}",
                data: {"trip_city_id": trip_city_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (result.status == 'success') {
                        console.log(result.trip_id);
                        window.location.replace("{{url('trip/plan')}}" + "/"+{{$trip_id}}+
                        "?city_id=" +{{$city->id}})
                        ;
                    } else {

                    }


                });
            e.preventDefault();
        });
        @endif



        $(".datepicker").datepicker({
            dateFormat: "d MM yy",
            altField: "#actual_trip_start_date",
            altFormat: "yy-mm-dd"

        });


        @if(isset($trip->myversion()[0]) AND is_object($trip->myversion()[0]))
        $(".datepicker_place").datepicker({
            minDate: '{{date("d F Y", strtotime($trip->myversion()[0]->start_date))}}',
            dateFormat: "d MM yy",
            altField: "#actual_place_date",
            altFormat: "yy-mm-dd",
            defaultDate: ""
        });
        @endif

        $('.timepicker').timepicker();

    });

    function doPlus(fieldId) {
        $('#' + fieldId).val(parseInt($('#' + fieldId).val()) + 1);
    }

    function doMinus(fieldId) {
        if (parseInt($('#' + fieldId).val()) > 0) {
            $('#' + fieldId).val(parseInt($('#' + fieldId).val()) - 1);
        }
    }


</script>
<script>
    // Initialize and add the map
    function initMap() {
        var world = {lat: 0, lng: 0};
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 2, center: world, mapTypeId: 'roadmap'});

                @if(isset($trips_cities_complete))
        var flightPlanCoordinates = [
                        @foreach($trips_cities_complete AS $dest)
                {
                    lat: {{$dest->city->lat}}, lng: {{$dest->city->lng}}},
                    @endforeach

            ];
        var flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });

        flightPath.setMap(map);


                @foreach($trips_cities_complete AS $dest)
        var Item_{{$dest->city->id}} = new google.maps.LatLng({{$dest->city->lat}}, {{$dest->city->lng}});
                @endforeach

        var bounds = new google.maps.LatLngBounds();

        @foreach($trips_cities_complete AS $dest)
        bounds.extend(Item_{{$dest->city->id}});
        @endforeach
        map.fitBounds(bounds);
        @endif


    }
</script>

<script>
    // confirmation
    $('.confirm-trip-deletion').on('click', function () {
        $.confirm({
            title: '{{ __('other.are_you_sure') }}',
            content: '{{ __('trip.if_you_canceled_this_trip_you_will') }}',
            icon: 'fa fa-question-circle',
            animation: 'scale',
            closeAnimation: 'scale',
            opacity: 0.5,
            buttons: {
                'confirm': {
                    text: 'Confirm',
                    btnClass: 'btn-orange',
                    action: function () {

                        $.ajax({
                            method: "POST",
                            url: "{{route('trip.ajax_delete_trip')}}",
                            data: {"trip_id": {{$trip_id}}}
                        })
                            .done(function (res) {
                                var result = JSON.parse(res);
                                if (result.status == 'success') {
                                    //console.log(result.trip_city_id);
                                    window.location.replace("{{url('home')}}");
                                } else {
                                    alert('{{ __("trip.unknown_error_while_deleting_trip_plan") }}');
                                }


                            });
                    }
                },
                cancel: function () {

                },

            }
        });
    });

</script>
</body>

</html>