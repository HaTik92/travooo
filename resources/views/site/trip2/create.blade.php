<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" dir="auto">
        <meta name="description" content="">
        <title>@if(isset($trip) && is_object($trip)){{$trip->title}} @else Travooo @endif</title>
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-j8y0ITrvFafF4EkV1mPW0BKm6dp3c+J9Fky22Man50Ofxo2wNe5pT1oZejDH9/Dt" crossorigin="anonymous">
        @include('site/trip2/partials/_styles')

        @php  $existing_place_ids = []; $existing_city_ids = []; @endphp
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha256-uKEg9s9/RiqVVOIWQ8vq0IIqdJTdnxDMok9XhiqnApU=" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-typeahead.css" integrity="sha256-kEEuwzcrMTLlrazS39JQIwiHUCxx2vRJlZcJ0Tj6MNU=" crossorigin="anonymous" />
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"> -->
        <link rel="stylesheet" href="{{asset('assets2/css/toastr.css')}}">
        <link rel="stylesheet" href="{{asset('assets2/css/plan.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

        <link rel="stylesheet" href="{{asset('assets2/css/travooo.css?0.6.8'.time())}}" />
        <link href="{{ asset('assets2/js/summernote/summernote.css') }}" rel="stylesheet">
        <link href="{{ asset('/plugins/summernote-master/tam-emoji/css/emoji.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{asset('assets2/css/trip-planner-custom.css')}}">
    </head>

    <body>
        <div class="wrapper landscape-trip-tablet @if (isset($goto_first_step) AND $goto_first_step) mobile-hide @endif">
            @if(Auth::check())
            <header class="navbar">
                @if (!isset($do))
                    <!-- Mobile view - invited user -->
                    <button class="btn-back-mobile"><i class="trav-angle-left"></i></button>
                    <button class="side-menu-btn right" data-toggle="modal" data-target="#modal-mobile_side_menu"><i class="trav-bars"></i><span class="counter" style="display: none;">0</span></button>
                    <!-- Mobile view - invited user -->
                @else
                    <button class="side-menu-btn" data-toggle="modal" data-dismiss="modal" data-target="#modal-mobile_side_menu"><i class="trav-bars"></i><span class="counter" style="display: none;">0</span></button>
                @endif

                <a href="{{url_with_locale('home')}}" class="navbar-brand"><img src="{{asset('trip_assets/images/logo.png')}}" alt=""></a>
                <div class="line-grey"></div>
                <div class="header-title tooltip-title tooltip--remove"><span class="navbar-text mobile-hide">@if(isset($trip) && is_object($trip)){{$trip->title}} @endif</span>
                    @if(isset($do) && $do=='edit')
                        <span class="navbar-text desktop-hide" style="font-family: CircularStd;">Create Trip</span>
                    @endif
                    <span class="tooltiptext">@if(isset($trip) && is_object($trip)){{$trip->title}} @endif</span>
                    @if(isset($do) && $do=='edit')
                        @if (($is_admin))
                            <button type="button" class="btn btn-secondary btn-edit-sm" data-toggle="modal" data-target="#modal-edit_title"></button>
                        @endif
                    @endif
                </div>

                @if(isset($trip) && (!isset($do) || $do!='edit'))
                    <div class="line-grey"></div>
                    <div class="header-subtitle"><p><b>Trip</b> By <a href="{{url_with_locale('profile/'.$trip->author->id)}}">{{$trip->author->name}}</a></p></div>

                    @if (!$show_accept_buttons && $trip->privacy !== 2)
                    <button type="button" class="btn btn-secondary plan-share-button" data-toggle="modal" data-target="#modal-share_popup"><i class="fa fa-share-square" aria-hidden="true"></i></button>
                    @endif
                @endif

                @if (!isset($do))
                    <!-- Mobile view - invited user -->
                    <div class="header-subtitle-invited">
                        @if(isset($trip) && is_object($trip))
                        <a href="{{url_with_locale('profile/'.$trip->author->id)}}"><img src="{{check_profile_picture($trip->author->profile_picture)}}"> {{$trip->author->name}}</a>
                        <h4 class="text-overflow-prevent">{{$trip->title}}</h4>
                        @endif
                    </div>
                    <!-- Mobile view - invited user -->
                @endif

                @if(!$is_admin || (!isset($do) || $do != 'edit'))
                    <button type="button" class="btn btn-secondary plan-details-button" data-toggle="modal" data-target="#modal-plan_details"><img src="{{asset('trip_assets/images/info-button-icon.svg')}}" alt="" /></button>
                @endif
                <span class="mode-info" style="margin: auto; left: 0; right: 0; text-align: center; width: fit-content; pointer-events: none;">
                    @if (!isset($do))
                            <button type="button" class="mode-btn"><i class="fa fa-eye" aria-hidden="true"></i>view mode</button>
                        @elseif ($do === 'edit' && $is_admin)
                            <button type="button" class="mode-btn"><i class="fa fa-pencil-alt" aria-hidden="true"></i>Creation Mode</button>
                        @elseif ($do === 'preview' && $is_admin)
                            <button type="button" class="mode-btn"><i class="fa fa-pencil-alt" aria-hidden="true"></i>Preview Mode</button>
                        @else
                            <button type="button" class="mode-btn"><i class="fa fa-pencil-alt" aria-hidden="true"></i>Suggestion Mode</button>
                        @endif
                </span>
                <div class="header-control ml-auto">
                    @if(isset($do) && $do=='edit' && ($is_admin || $is_editor))
                        @if ($show_draft_buttons)
                            <button type="button" class="btn btn-transparent btn-undo" data-toggle="modal" data-target="#modal-cancel">Undo</button>
                        @endif
                            @if(isset($do) && $do=='edit')
                            <div class="line-grey"></div>
                            <a href="?do=preview" class="preview">PREVIEW</a>
                            @endif
                        @if ($is_admin)
                            <button type="button" class="btn btn-primary mobile-hide" data-toggle="modal" data-target="#modal-final_step">Review & publish</button>
                            <button type="button" class="btn btn-primary desktop-hide" data-toggle="modal" data-target="#modal-final_step">Next</button>
                        @endif
                    @elseif (!isset($do) && ($is_editor || $is_admin))
                        <a href="?do=edit" class="mobile-hide"><button type="button" class="btn btn-primary edit-trip">Edit</button></a>
                        @if ($log_unread_count)
                            <div class="unsaved edit-trip" style="color: #fff;
    position: relative;
    right: 12px;
    bottom: 16px;
    font-size: 14px;
    line-height: 18px;
    font-family: 'CircularAirPro Light';
    background: #fc225a;
    width: auto;
    min-width: 22px;
    height: 22px;
    padding: 0 5px;
    text-align: center;
    border-radius: 22px;
    border: 2px solid #fff;">{{$log_unread_count}}</div>
                        @endif
                    @endif

                    @if(isset($do) && $do=='edit' && $is_editor)
                        <button type="button" class="btn btn-primary send-suggestions mobile-hide" @if (!\App\Services\Trips\TripsSuggestionsService::isNotSendingSuggestions($trip->id)) disabled @endif>Send suggestions</button>
                        <button type="button" class="btn btn-primary send-suggestions desktop-hide" @if (!\App\Services\Trips\TripsSuggestionsService::isNotSendingSuggestions($trip->id)) disabled @endif>Send</button>
                    @endif

                    <button style="border: none;" type="button" class="btn-user"><a href="{{route('profile')}}"><img class="rounded-circle" src="{{check_profile_picture(Auth::user()->profile_picture)}}" style='width:36px;height:36px;'></a></button>
                </div>
            </header>
            @else
                <header class="navbar">
                    <link rel="stylesheet" href="{{asset('assets2/css/sign-up.css?v='.time())}}">
                    <div class="non-logged-header-block pt-0">
                        <div style="display: flex;">
                            <a href="{{url_with_locale('home')}}" class="navbar-brand"><img src="{{asset('frontend_assets/image/main-logo.png')}}" alt=""></a>
                            <div style="display: flex; align-items: center;">
                                <div class="line-grey"></div>
                                <div class="header-title tooltip-title"><span class="navbar-text mobile-hide">@if(isset($trip) && is_object($trip)){{$trip->title}} @endif</span>
                                    <span class="navbar-text desktop-hide" style="font-family: CircularStd;">Create Trip</span>
                                    <span class="tooltiptext">@if(isset($trip) && is_object($trip)){{$trip->title}} @endif</span>
                                </div>

                                @if(isset($trip) && (!isset($do) || $do!='edit'))
                                    <div class="line-grey"></div>
                                    <div class="header-subtitle"><p><b>Trip</b> By <a href="{{url_with_locale('profile/'.$trip->author->id)}}">{{$trip->author->name}}</a></p></div>
                                @endif
                            </div>
                        </div>
                        <div class="non-logged-action-block">
                            <a href="javascript:;" class="btn non-logged-btn login-btn">Log In</a>
                            <a href="{{url('/signup?dt=regular')}}" class="btn non-logged-btn signup-btn">Sign Up</a>
                        </div>
                    </div>
                </header>
            @endif

            <!-- Mobile view - invited user -->
            @if (!isset($do) && isset($trip))
            <div class="mobile-trip-view-mode @if (!$show_accept_buttons) fullheight @endif" style=" @if(!$trip->cover) background: #aaa; @endif @if (!auth()->check()) height: calc(100% - 90px) !important; @endif">
                @if ($trip->cover)
                <img class="mobile-trip-view-mode-bg" src="{{$trip->cover}}" alt="">
                @else
                <img class="mobile-trip-view-mode-bg" src="https://travooo.com/trip_assets/images/cover-placeholder.svg" alt="" style="margin: auto;width: 100px;height: 100px;left: 0;right: 0;top: 0;bottom: 0;">
                @endif
                <div class="mobile-trip-view-mode-top">
                    <button class="back" style="opacity: 0;"><i class="trav-angle-left"></i></button>
                    <h4>Trip Plan</h4>
                    <button class="side-menu-btn" data-toggle="modal" data-target="#modal-mobile_side_menu"><i class="trav-bars"></i><span class="counter" style="display: none;">0</span></button>
{{--                    <div class="reactions">--}}
{{--                        <a href="#"><i class="trav-heart-fill-icon"></i> 0</a>--}}
{{--                        <a href="#"><i class="trav-comment-icon"></i> 0</a>--}}
{{--                    </div>--}}
                </div>
                <div class="mobile-trip-view-mode-bottom">
                    <a class="author" href="{{url_with_locale('profile/'.$trip->author->id)}}"><img src="{{check_profile_picture($trip->author->profile_picture)}}"> {{$trip->author->name}}</a>
                    <h4 class="trip-title">{{$trip->title}}</h4>
                    <div class="trip-stats">
                        <?php
                        $placesIds = [];

                        foreach ($trip_places_arr as $trip_place_arr) {
                            $placesIds[] = $trip_place_arr->trip_place_id;
                        }

                        ?>
                        <span>
                            @if(isset($trip_places) && is_array($trip_places) && count($trip_places)>0)
                                <?php
                                $dStart = new DateTime(min(array_keys($trip_places)));
                                $dEnd  = new DateTime(max(array_keys($trip_places)));
                                $dDiff = $dStart->diff($dEnd);
                                echo $dDiff->format('%r%a');
                                ?>
                            @endif
                        </span> Days
                        <b>&middot;</b>
                        $ <span><?php echo Illuminate\Support\Facades\DB::table('trips_places')->whereIn('id', $placesIds)->sum('budget');?></span> Budget
                        <b>&middot;</b>
                        <span>{{count($placesIds)}}</span> Destinations</div>
                    <div class="trip-description">{{$trip->description}}</div>
                    <div class="trip-photos">
                        <ul class="post-image-list">
                        </ul>
                    </div>
                    <div class="actions">
                        <button class="story-mode" style="opacity:0;"><i class="fas fa-file-alt"></i> Story Mode</button>
                        <button class="view-plan"><i class="fas fa-map"></i> View Trip Plan →</button>
                    </div>
                </div>
            </div>
            @endif
            <!-- Mobile view - invited user -->

            <input type="checkbox" id="send_back" @if($im_invited) checked @endif style="display: none" />
            <div class="page-holder">
                <div class="page">
                    <div class="page-planner">
                        <!-- If user == invited => add class invited to the next sidebar div -->
                        <div class="sidebar @if (!isset($do)) invited @endif @if ($show_accept_buttons) acceptable @endif">
                            <div class="sidebar-holder" id="planContents">
                            </div>
                            @if ($show_accept_buttons)
                            <div class="invitation-popup">
                                <div class="info">
                                    You have been invited to this trip plan as <b>{{$invite_desc_role}}</b> by
                                    <span><img src="{{check_profile_picture($inviter->profile_picture)}}">{{$inviter->name}}</span>
                                </div>
                                <div class="actions">
                                    <button data-id="{{$invite_id}}" type="button" class="deny-invite">Reject</button>
                                    <button type="button" data-id="{{$invite_id}}" class="accept-invite">Accept</button>
                                </div>
                            </div>
                            @endif
                        </div>
                            <div class="page-map">
                                <div class="page-map-header">
                                    <div class="timeline mt-auto">
                                        <div class="timeline-holder">
                                            <div class="timeline-design">
                                                <div class="line line-long"><span>12 AM</span></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-long"><span>6 AM</span></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-long"><span>12 PM</span></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-long"><span>6 PM</span></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-short"></div>
                                                <div class="line line-long"><span>12 AM</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="time-control">
                                        <div class="swiper-days">
                                            <div class="swiper-wrapper">
                                            </div>
                                            <div class="swiper-days-control">
                                                <div class="swiper-prev">
                                                    <svg id="SVGDoc" width="8" height="12" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 8 12">
                                                    <defs></defs>
                                                    <path d="M6.29259,0.28561l1.4142,1.41556l-4.29346,4.29265l4.29346,4.29309l-1.4142,1.41346l-6.00004,-6.00057l1.4142,-1.41241l0.29254,0.29251z" fill="#e6e6e6" fill-opacity="1"></path>
                                                    </svg>
                                                </div>
                                                <div class="swiper-next">
                                                    <svg id="SVGDoc" width="8" height="12" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 8 12">
                                                    <defs></defs>
                                                    <path d="M0.29258,1.70117l1.4142,-1.41556l4.29325,4.29425l0.29249,-0.29247l1.4143,1.41241l-6.00004,6.00057l-1.4142,-1.41346l4.29343,-4.29312z" fill="#e6e6e6" fill-opacity="1"></path>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Mobile elements -->
                                @if(isset($do) && $do=='edit')
                                <div class="mobile-map-view-controls">
                                    <button class="story-view active">
                                        <i class="fas fa-file-alt"></i>
                                    </button>
                                    <button class="map-view">
                                        <i class="fas fa-map"></i>
                                    </button>
                                </div>
                                @endif
                                <!-- Mobile elements END -->
                                <div class="map-search modal-place" style="width: 300px;">
                                    <div class="input-group input-dropdown" id="dropCont">
                                        <div class="input-group-prepend"><img src="{{asset('trip_assets/images/ico-search.svg')}}" alt=""></div>
                                        <input type="text" class="map-place-search form-control" placeholder="Search on map...">
                                    </div>
                                    <div class="input-group-dropdown">
                                        <div class="input-group-dropdown-body select select-map-place">
                                            <div class="map-select map-select-map-place" style="overflow: hidden;">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @if(isset($do) && $do=='edit')
                                    <div class="add-place-map desktop-hide">+</div>
                                @endif

                                @if ($planMember)
                                    @include('site/trip2/partials/plan-chats')
                                @endif

                                <div class="map-placeholder-block map-placeholder-background mobile-hide" style="display: none;">
                                </div>
                                <div class="map-placeholder-block map-placeholder mobile-hide" style="display: none;">
                                    <img src="{{asset('assets2/image/map_click.png')}}" alt=""><br>
                                    Click the map to select a location or place
                                </div>
                                <div class="map-wrap" id="maps">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="offline-status">
                Reconnecting...
            </div>

            <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.1/js.cookie.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>

            @include('site/trip2/partials/_scripts')
            @include('site.home.partials._spam_dialog')
            @if(isset($trip) && is_object($trip))
            @include('site/trip2/partials/modal-edit_title')
            @include('site/trip2/partials/modal-final_step')
            @include('site/trip2/partials/modal-plan_details')
            @include('site/trip2/partials/modal-invite')
            @include('site/trip2/partials/modal-cancel')
            @include('site/trip2/partials/modal-mobile_side_menu')
            @endif


            @include('site/trip2/partials/modal-checkins')
            @include('site/trip2/partials/modal-skyscanner')
            @include('site/trip2/partials/modal-create_trip')
            @include('site/trip2/partials/modal-add_place')
            @include('site/trip2/partials/modal-edit_place')
            @include('site/trip2/partials/modal-show_place_suggestions')
            @include('site/trip2/partials/modal-edit_memory_place')
            @include('site/trip2/partials/modal-add_suggestion')
            @include('site/trip2/partials/modal-suggestions')
            @include('site/trip2/partials/modal-affiliate_hint')
            @include('site/trip2/partials/modal-activity_logs')
            @include('site/home/partials/modal_comments_like')
            @include('site/trip2/partials/modal-leave_popup')
            @include('site/trip2/partials/modal-decline_suggestion')

            @if(Auth::check() && isset($trip) && is_object($trip))
            @include('site/trip2/partials/modal-share')
            @include('site/trip2/partials/modal-share_popup')
            @include('site/trip2/partials/_activity_logs_script')
            @include('site/trip2/partials/_suggestions_script')
            @endif

        @if(!Auth::check())
            @include('site/layouts/_non-loggedin-user-footer')
        @endif
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" integrity="sha256-tQ3x4V2JW+L0ew/P3v2xzL46XDjEWUExFkCDY0Rflqc=" crossorigin="anonymous"></script>
    <script>
        var swiper = new Swiper('.swiper-media', {
            slidesPerView: 'auto',
            spaceBetween: 12,
            observer: true,
            observeParents: true,
            navigation: {
                nextEl: '.swiper-media .swiper-button-next',
                prevEl: '.swiper-media .swiper-button-prev'
            }
        });
    </script>
    <script src="{{asset('assets2/js/pages/trip-planner/trip-planner-select.js')}}"></script>
</html>
