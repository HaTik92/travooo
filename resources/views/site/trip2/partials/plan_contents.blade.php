<style>
    .thumbnail {
        position: relative;
        width: 200px;
        height: 200px;
        overflow: hidden;
    }
    .thumbnail img {
        position: absolute;
        left: 50%;
        top: 50%;
        height: 100%;
        width: auto;
        -webkit-transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
        transform: translate(-50%,-50%);
    }
    .thumbnail img.portrait {
        width: 100%;
        height: auto;
    }


    .media-comment-footer a{
        text-decoration: none !important;
    }

    .lg-outer .lg-thumb-outer {
        /* width:auto !important; */
    }

    a.plan-place {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        max-width: 300px;
        cursor: pointer;
        display: block;
        font-family: 'Circular Std Book',-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
        font: weight: 500;
    }

    a.plan-place:hover {
        white-space: unset;
        overflow: unset;
        text-overflow: unset;
        max-width: unset;
        cursor: pointer;
        display: block;
    }
</style>

<div class="plan-days">
    <label class="btn-toggle group-toggle-title @if(count($trip_places) > 6) fader @endif" style="cursor:initial; min-width: 62px;">Day</label>
    <div class="btn-group btn-group-toggle btn-group-toggle-days @if(count($trip_places) > 6) short @endif" data-toggle="buttons">
        @foreach($trip_places AS $day=>$tp)
        <label class="btn btn-toggle @if($loop->first) active @endif" data-date="{{$day}}" data-day="{{\Carbon\Carbon::parse($day)->format('d')}}" data-month="{{\Carbon\Carbon::parse($day)->format('M')}}" onclick="moveSlider('{{$day}}')">
            <input type="radio" autocomplete="off"> {{$loop->index+1}}
        </label>
        @endforeach
    </div>
    @if (count($trip_places) > 6)
        <div class="dropdown" style="vertical-align: top;">
            <button class="btn btn-toggle choose-day" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                ...
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @php
                    $dayCity = null;
                @endphp
                @foreach($trip_places AS $day=>$tp)
                    @if (isset(array_values($tp)[0]) && array_values($tp)[0]['trip_place']->city->trans[0]->title != $dayCity)
                        @php
                            $dayCity = array_values($tp)[0]['trip_place']->city->trans[0]->title;
                        @endphp
                        <a class="dropdown-item item-city" href="#" onclick="moveSlider('{{$day}}')"><img src="{{asset('trip_assets/images/ico-location.svg')}}" alt="" />{{$dayCity}}</a>
                    @endif
                    <a class="dropdown-item" onclick="moveSlider('{{$day}}')" href="#">Day {{$loop->index+1}}</a>
                @endforeach
            </div>
        </div>
    @endif
    @if(isset($do) && $do=='edit' && $plan_member)
        <button type="button" class="btn btn-primary activity-logs" style="color: #404040; border: 1px solid #e0e0e0;background-color: #f2f2f2;height: 34px;width: 110px;text-align: center;padding: 0;float:right;margin-left: 10px;" ><i class="fas fa-list"></i> Activity Log</button>

        <div class="logs_unread mobile-hide" style="color: #fff;
    position: absolute;
    right: 10px;
    top: 12px;
    font-size: 14px;
    line-height: 18px;
    font-family: 'CircularAirPro Light';
    background: #fc225a;
    width: auto;
    min-width: 22px;
    height: 22px;
    padding: 0 5px;
    text-align: center;
    border-radius: 22px;
    @if (!$unread_logs_count)
            display: none;
    @endif
    border: 2px solid #fff;">{{$unread_logs_count}}</div>

    @endif
    @if ($is_admin)<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-invite" style="height: 34px;width: 78px;text-align: center;padding: 0;float:right;"><i class="trav-user-icon"></i> Invite</button>@endif
</div>

<div class="wrap-trip">
    <div class="wrap-trip-holder" @if($is_invited) style="padding-bottom: 86px;" @endif>
        <div class="trip">
            @php
                $beforeCity = null;
                $spentSoFar = 0;
            @endphp
            @if(isset($trip_places) && count($trip_places)>0)
            @foreach($trip_places AS $day=>$tps)
            <div class="trip-day filterDiv {{$day}} @if($loop->first) show active @else show @endif">
                <div class="trip-day_header">
                    <div class="trip-day_header-holder mobile-hide">
                        <span class="header-day">Day {{$loop->index+1}}</span> <span class="dot">•</span> <span class="day_header-date">{{\Carbon\Carbon::parse($day)->format('d F Y')}}</span>
                    </div>
                </div>
                <div class="trip-day_body">
                    @php
                        $beforeTpTime = \Carbon\Carbon::parse($day)->startOfDay();
                    @endphp

                    @foreach($tps as $tpId => $tp)
                    @php
                        $role = $tp['role'];
                        $isExpert = $tp['is_expert'];
                        $suggestion = $tp['suggestion'];
                        $toDismissSuggestion = $tp['trip_place']['to_dismiss'];
                        $tp = $tp['trip_place'];
                        $currentTpTime = \Carbon\Carbon::parse($tp->time)->startOfMinute();
                        $time = (clone $beforeTpTime)->addMinutes($currentTpTime->diffInMinutes($beforeTpTime)/2);
                        $order = $tp->order;
                        $isMyPendingSuggestion = $suggestion && $suggestion->users_id === $authUserId && $suggestion->status === \App\Services\Trips\TripsSuggestionsService::SUGGESTION_PENDING_STATUS;
                    @endphp

                    @if(isset($do) && $do=='edit' && $plan_member)
                        <div class="trip-add_place" style="position: relative; top: -7px; margin-bottom: 0px;">
                            <button data-spent="{{$spentSoFar}}" @if($beforeCity) data-cityimg="{{@check_city_photo($beforeCity->city_medias[0]->url, 180, true)}}" data-citytitle="{{$beforeCity->trans[0] ? $beforeCity->trans[0]->title : ''}}" data-cityid="{{$beforeCity->id}}" data-citylat="{{$beforeCity->lat}}" data-citylng="{{$beforeCity->lng}}" @endif data-dateformat="{{\Carbon\Carbon::parse($day)->format('d F Y')}}" data-date="{{$day}}" data-order="{{$order - 1}}" class="btn btn-transparent btn-add_place" data-toggle="modal" data-target="#modal-add_place">@if ($is_admin)Add a Place @else Suggest a Place @endif</button>
                        </div>
                    @endif
                        <div class="has-icon-text"><span class="image" style="background-image: url({{asset('trip_assets/images/ico-plane.svg')}});"></span> <span class="text"><span class="text text-start">Start</span><span class="text text-end">End</span></span></div>
                    @php
                        $beforeTpTime = $currentTpTime;
                        $beforeCity = $tp->city;
                    @endphp
                    @php
                        $allLikes = $tp->likes;
                        $userLikes = $allLikes->where('users_id', $authUserId);
                    @endphp
                    <div class="trip-place-wrap" data-spent="{{$spentSoFar}}" data-date="{{$day}}" data-time="{{\Carbon\Carbon::parse($day)->startOfDay()->diffInMinutes(\Carbon\Carbon::parse($tp->time))}}" data-placeid="{{$tp->id}}" data-origplaceid="{{$tp->place->id}}">
                        <div class="trip-place" data-id="{{$tp->id}}">
                            <div class="trip-place-country">
                                <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($tp->city->country->iso_code)}}.png" class="flag" style="width:36px;height:23px;">
                                <span><a href='{{url_with_locale("country/".$tp->city->country->id)}}' target='_blank'>{{$tp->city->country->trans[0]->title}}</a></span>
                            </div>
                            <div class="trip-place-city">
                                <a href='{{url_with_locale("city/".$tp->place->city->id)}}' target='_blank'>{{$tp->place->city->trans[0] ? $tp->place->city->trans[0]->title : ''}}</a>
                            </div>
                            <div class="seporator desktop-hide"></div>
                            <div class="place-time desktop-hide">
                                <span class="bold">{{\Carbon\Carbon::parse($tp->time)->format('d M')}}</span> <span class="light">{{\Carbon\Carbon::parse($tp->time)->format('Y')}}</span> @if (!$tp->without_time)<span class="dark-light">at</span> <span class="bold">{{\Carbon\Carbon::parse($tp->time)->format('h:i')}}</span> <span class="light">{{\Carbon\Carbon::parse($tp->time)->format('A')}}</span> @endif
                            </div>
                            <div class="card card-details card-toggle @if($isMyPendingSuggestion) pending @endif">
                                <div class="card-inner @if($toDismissSuggestion) disapproved @endif">
                                    <div class="card-img-holder">
                                        @if ($isExpert)
                                            <div class="by-expert" style="">BY EXPERT</div>
                                        @endif
                                        <img src="@if(isset($tp->place->getMedias[0]->url)) @if($tp->place->getMedias[0]->thumbs_done==1 || 1)https://s3.amazonaws.com/travooo-images2/th180/{{$tp->place->getMedias[0]->url}} @else https://s3.amazonaws.com/travooo-images2/{{$tp->place->getMedias[0]->url}} @endif @else {{asset('assets2/image/placeholders/pattern.png')}} @endif" alt="" class="card-img">
                                        <!-- Mobile view - invited user -->
                                        <div class="reactions" style="display: none;">
                                            <a href="#"><i class="@if ($userLikes->count()) trav-heart-fill-icon @else trav-heart-icon @endif"></i></a>
                                            <a href="#"><i class="trav-comment-icon"></i></a>
{{--                                            <a href="#"><i class="fas fa-lightbulb"></i> 7</a>--}}
                                        </div>
                                        <!-- Mobile view - invited user -->
                                        </div>
                                    <div class="card-activity" data-tripplaceid="{{$tp->id}}">
                                        <div class="location">
                                            <span class="activity-name" style="display: flex; align-items: center;">
                                                @if (in_array($tp->id, $updated_places))
                                                    <span class="new" style="box-shadow: 0 1px 4px rgba(0,0,0,0.3); height: 10px; width: 10px; border-radius: 7px; background: #fc225a; margin-right: 8px; display: flex;"></span>
                                                @endif
                                                <a class="plan-place" data-date="{{\Carbon\Carbon::parse($day)->greaterThan(\Carbon\Carbon::now()) ? \Carbon\Carbon::parse($day)->format('Y-n-d') : \Carbon\Carbon::now()->addDay()->format('Y-n-d')}}" data-city="{{$tp->city->trans[0] ? $tp->city->trans[0]->title : ''}}" data-src="@if(isset($tp->place->getMedias[0]->url)) @if($tp->place->getMedias[0]->thumbs_done==1)https://s3.amazonaws.com/travooo-images2/th1100/{{$tp->place->getMedias[0]->url}}@else https://s3.amazonaws.com/travooo-images2/{{$tp->place->getMedias[0]->url}}@endif @else{{asset('assets2/image/placeholders/pattern.png')}}@endif" data-id="{{$tp->place->id}}" data-address="{{$tp->place->transsingle->address}}" data-lat="{{$tp->place->lat}}" data-lng="{{$tp->place->lng}}">
                                                    {{@$tp->place->transsingle->title}}
                                                </a>
                                            </span>
                                                @if ($tp->place->place_type)
                                                    <span class="activity-place-type">{{@mb_strtoupper(str_replace('_', ' ', explode(',', $tp->place->place_type)[0]))}}</span>
                                                @endif

                                                @if (!$tp->without_time)
                                                    <span class="activity-time">@ {{\Carbon\Carbon::parse($tp->time)->format('g:ia')}}</span>
                                                @endif
                                        </div>
                                        <div class="info mobile-hide">
                                            @if (@$tp->budget)
                                                <span class="activity-cost">
                                                    @if($tp->time <= now()) Spent @else Will spend @endif
                                                    <b>${{@$tp->budget}}</b></span>
                                                <b class="dot">·</b>
                                            @endif

                                            @if (durationInHours($tp->duration) || durationInMinutes($tp->duration))
                                                <span class="activity-duration">
                                                @if($tp->time <= now()) Stayed @else Will stay @endif
                                                <b>@if (durationInHours($tp->duration) > 0) {{durationInHours($tp->duration)}} hours @endif {{durationInMinutes($tp->duration)}} minutes</b></span>
                                            @endif
                                        </div>
                                    </div>


                                    @if(isset($do) && $do=='edit')
                                        <div class="card-edit ml-auto">
                                            @if ($plan_member && !$toDismissSuggestion && (!$suggestion || $suggestion->status === \App\Services\Trips\TripsSuggestionsService::SUGGESTION_APPROVED_STATUS))
                                                <button type="button" class="btn btn-secondary btn-edit editPlaceTrigger" data-id="{{$tp->id}}" @if (\Carbon\Carbon::parse($day)->lessThan(\Carbon\Carbon::now())) data-memory="1" @else data-memory="0" @endif><img src="{{asset('trip_assets/images/ico-edit.svg')}}" class="ico-edit" alt=""> Edit</button>
                                            @endif
                                            {{--<p class="time-edit">Editing...</p>--}}
                                            @if($plan_member && isset($is_edited[$tp->id]) && $is_edited[$tp->id])<p class="time-edit">Last edited {{diffForHumans($tp->created_at, 'UTC')}}</p>@endif
                                        </div>

                                        @if(!$toDismissSuggestion && ($tp->trip->memory || \Carbon\Carbon::now()->greaterThan($tp->date)))
                                            <button class="btn-card-toggle testButton" id="testButton"></button>
                                        @endif
                                    @elseif($tp->trip->memory || \Carbon\Carbon::now()->greaterThan($tp->date))
                                        <button class="btn-card-toggle ml-auto testButton" id="testButton"></button>
                                    @endif
                                </div>
                                <div class="activity-info desktop-hide">
                                    @if (@$tp->budget)
                                        <span class="activity-cost">
                                                    @if($tp->time <= now()) Spent @else Will spend @endif
                                                    <b>${{@$tp->budget}}</b></span>
                                        <b class="dot">·</b>
                                    @endif

                                    @if (durationInHours($tp->duration) || durationInMinutes($tp->duration))
                                        <span class="activity-duration">
                                                @if($tp->time <= now()) Stayed @else Will stay @endif
                                                <b>@if (durationInHours($tp->duration) > 0) {{durationInHours($tp->duration)}} hours @endif {{durationInMinutes($tp->duration)}} minutes</b></span>
                                    @endif
                                </div>
                                @if (isset($not_approved_suggestions[$tp->id]) && $is_admin)
                                <div class="suggestion exist">
                                    <div class="author">
                                        <div class="author-img">
                                        @foreach($not_approved_suggestions[$tp->id] as $suggestionsUser)
                                                @if ($loop->index < 3)
                                                    <img src="{{check_profile_picture($suggestionsUser->profile_picture)}}" />
                                                @endif
                                        @endforeach
                                        </div>

                                        @php
                                            $suggestionsUsersCount = count($not_approved_suggestions[$tp->id]) - 2;
                                        @endphp

                                        <div class="info">
                                            <div>
                                                <span class="author-desc">
                                                    <a class="author-name" href="#">@foreach($not_approved_suggestions[$tp->id] as $suggestionsUser)@if($loop->index < 2)@if($loop->index > 0), @endif{{explode(' ', $suggestionsUser->name)[0]}}@endif{{''}}@endforeach
                                                    </a>
                                                    @if ($suggestionsUsersCount > 0)<a class="author-name" href="#">+{{$suggestionsUsersCount}} others </a>@endif
                                                    have made some suggestions
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="status">
                                        <img src="{{asset('trip_assets/images/arrow-suggest.svg')}}" alt="">
                                    </div>
                                </div>
                                @elseif (isset($not_approved_suggestions[$tp->id]) && !$is_admin && isset($not_approved_suggestions[$tp->id][$authUserId]))
                                <div class="suggestion exist">
                                    <div class="author">
                                        <div class="info">
                                            <div>
                                                <span class="author-desc"><a class="author-name" href="#">You</a> have made some suggestions</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="status">
                                        <img src="{{asset('trip_assets/images/arrow-suggest.svg')}}" alt="">
                                    </div>
                                </div>
                                @elseif ($suggestion && !$preview_mode)
                                <div class="suggestion">
                                    <div class="author">
                                        @php
                                            $suggestionType = '';
                                            $adminSuggestionType = '';
                                            $authorText = '';

                                            switch ($suggestion->type) {
                                                case \App\Services\Trips\TripsSuggestionsService::SUGGESTION_CREATE_TYPE:
                                                    $suggestionType = '';
                                                    $authorText = $suggestionType . 'Suggested by ';
                                                    $adminSuggestionType = 'Added';
                                                    break;
                                                case \App\Services\Trips\TripsSuggestionsService::SUGGESTION_EDIT_TYPE:
                                                    $suggestionType = 'Edits';
                                                    $adminSuggestionType = 'Edits done';
                                                    break;
                                                case \App\Services\Trips\TripsSuggestionsService::SUGGESTION_DELETE_TYPE:
                                                    $suggestionType = 'Removal';
                                                    $adminSuggestionType = 'Removed';
                                                    break;
                                                default:
                                                    break;
                                            }

                                            if (!$authorText) {
                                                $authorText = $suggestionType . ' suggested by ';
                                            }

                                            if ($suggestion->role === 'admin') {
                                                $authorText = $adminSuggestionType . ' by ';
                                            }
                                        @endphp
                                        @if ($suggestion->users_id !== $authUserId && !$toDismissSuggestion)
                                            <div class="author-img">
                                                <img src="{{check_profile_picture($suggestion->user->profile_picture)}}" />
                                            </div>
                                        @endif
                                        <div class="info">
                                            @if($toDismissSuggestion)
                                                <div>
                                                @if ($suggestion->users_id === $authUserId && $suggestion->status === \App\Services\Trips\TripsSuggestionsService::SUGGESTION_DISAPPROVED_STATUS)
                                                    <span class="author-desc">{{$authorText}}<span class="author-name">you</span> and it's removed by Admin</span>
                                                @else
                                                    <span class="author-desc">It's removed by <span class="author-name">{{$suggestion->user->name}}</span></span>
                                                @endif
                                                </div>
                                            @else
                                                <div>
                                                    <span class="author-desc">
                                                        {{$authorText}}
                                                        @if ($suggestion->users_id !== $authUserId)
                                                            <a class="author-name" href="/profile/{{$suggestion->user->id}}">{{$suggestion->user->name}}</a>
                                                        @else
                                                            <span class="author-name">you</span>
                                                        @endif
                                                        @if($suggestion->status === \App\Services\Trips\TripsSuggestionsService::SUGGESTION_APPROVED_STATUS && !$suggestion->is_published)
                                                            and it's not published yet
                                                        @endif
                                                    </span>
                                                </div>
                                                <span class="role" >@if ($isMyPendingSuggestion) Pending approval... @else{{ucfirst($role)}}@endif</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="status">
                                        @if ($suggestion->status !== \App\Services\Trips\TripsSuggestionsService::SUGGESTION_APPROVED_STATUS && !$toDismissSuggestion)
                                            @if ($is_admin)
                                                <div class="disapprove" data-id="{{$suggestion->id}}">✕</div>
                                                <div class="approve" data-id="{{$suggestion->id}}">✓</div>
                                            @elseif ($isMyPendingSuggestion)
                                                <div class="remove dismissPlaceTrigger" data-id="{{$suggestion->suggested_trips_places_id}}">✕</div>
                                            @elseif ($suggestion->status !== \App\Services\Trips\TripsSuggestionsService::SUGGESTION_DISAPPROVED_STATUS)
                                                <button type="button" style="margin-right: 9px;" class="btn btn-secondary btn-edit showPlaceTrigger" data-id="{{$suggestion->suggested_trips_places_id}}" @if (\Carbon\Carbon::parse($day)->lessThan(\Carbon\Carbon::now())) data-memory="1" @else data-memory="0" @endif>Show</button>
                                            @endif
                                        @elseif (!$suggestion->is_published && $is_admin && !$toDismissSuggestion)
                                            <button type="button" class="publish"><img src="{{asset('trip_assets/images/publish.svg')}}" alt=""> Publish</button>
                                        @endif

                                        @if ($toDismissSuggestion)
                                            <button type="button" class="dismissPlaceTrigger" data-id="{{$suggestion->suggested_trips_places_id}}">✓</button>
                                        @endif
                                    </div>
                                </div>
                                @endif
                                @if (!$toDismissSuggestion)
                                <div class="card-trip_info card-collapse collapse @if (\Carbon\Carbon::now()->greaterThan($tp->date) || !$editable) show @endif">
                                    <!-- Mobile view -->
                                    <button type="button" class="mobile-collapse-toggler" style="display: none !important;">
                                        <i class="trav-angle-left"></i>
                                    </button>
                                    <div class="place-summary" style="display: none;">
                                        <img src="@if(isset($tp->place->getMedias[0]->url)) @if($tp->place->getMedias[0]->thumbs_done==1 || 1)https://s3.amazonaws.com/travooo-images2/th180/{{$tp->place->getMedias[0]->url}} @else https://s3.amazonaws.com/travooo-images2/{{$tp->place->getMedias[0]->url}} @endif @else {{asset('assets2/image/placeholders/pattern.png')}} @endif" alt="">
                                        <div class="text">
                                            <div class="place-title">{{@$tp->place->transsingle->title}} <span class="arrival-time">
                                                    @if (!$tp->without_time)
                                                        <span class="activity-time">@ {{\Carbon\Carbon::parse($tp->time)->format('g:ia')}}</span>
                                                    @endif
                                                </span>
                                            </div>
                                            <div class="details">
                                            @if (@$tp->budget)
                                                    @if($tp->time <= now()) Spent @else Will spend @endif <strong>{{@$tp->budget}}$</strong> <strong class="dot">&middot;</strong>

                                            @endif
                                            @if (durationInHours($tp->duration) || durationInMinutes($tp->duration))
                                            @if($tp->time <= now()) Stayed @else Will stay @endif
                                                <strong>@if (durationInHours($tp->duration) > 0) {{durationInHours($tp->duration)}} hours @endif {{durationInMinutes($tp->duration)}} minutes</strong>
                                            @endif
                                            </div>

                                        </div>
                                    </div>
                                    <!-- Mobile view -->
                                    <div class="trip-photos">
                                        <div class="trip-photos-holder place-{{$tp->id}}" style="overflow: hidden;">
                                            <?php
                                                $trip_place_media = $trip_place_medias[$tp->id];
                                            ?>
                                            @if(count($trip_place_media) && \Carbon\Carbon::now()->greaterThan($tp->date))
                                                <div class="post-media-inner">
                                                    <div class="post-media-list sortBody" style="display: flex;">
                                                        @foreach($trip_place_media AS $photo)
                                                            <div class="media-wrap" datesort="{{$photo->media->uploaded_at?strtotime($photo->media->uploaded_at):0}}" style="cursor: pointer;">
                                                                <div class="media-inner" style="position: relative;">
                                                                    @if ($photo->media->type === \App\Models\ActivityMedia\Media::TYPE_VIDEO)
                                                                        <div class="thumb-play-icon" style="top:92px;"></div>
                                                                    @endif
                                                                    <div class="counters" data-mediaid={{$photo->media->id}} style="">
                                                                        <div class="trip-card__likes" dir="auto"><i class="fa fa-heart fill" aria-hidden="true" dir="auto" style="color: #ff5a79;font-size: 14px;padding-right:3px;"></i> <span>{{count($photo->media->likes)}}</span></div>
                                                                        <div class="trip-card__comments" dir="auto">
                                                                            <i class="trav-comment-icon" style="color: #4080ff;font-size: 16px;padding-right:3px;"></i>
                                                                            <span class="15-comment-count" dir="auto"> {{count($photo->media->comments)}}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div data-tripmediaid={{$photo->id}} data-mediaid={{$photo->media->id}} data-video={{$photo->media->type === \App\Models\ActivityMedia\Media::TYPE_VIDEO ? 1 : 0}} class="thumbnail lightbox_imageModal" data-likes="3" data-comments="5" data-alt="testtest" data-id="{{ $loop->index }}" style="width:218px;height:218px;">
                                                                        <img orig-url="{{$photo->media->url}}" src="{{$photo->media->type === \App\Models\ActivityMedia\Media::TYPE_VIDEO ? $photo->media->source_url : get_profile_media($photo->media->source_url)}}" style="width:300px;height:250px;object-fit: cover;" onerror="this.src = $(this).attr('orig-url')" />

                                                                        <div class="media-cover-txt">
                                                                            <div class="media-info media-foot">
                                                                                <div class="foot-layer {{$photo->media->id}}-media-liked-count liked-count">
                                                                                    <b>{{count($photo->media->likes)}}</b>
                                                                                    <span>@lang('profile.likes')</span>
                                                                                </div>
                                                                                <div class="foot-layer media-commented-count" style="cursor: pointer">
                                                                                    <b>{{count($photo->media->comments)}}</b>
                                                                                    <span>@lang('profile.comments')</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        @include('site.trip2.partials._trip_photos_scripts')
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        @if(count($trip_place_media) > 3)
                                            <a href="#" class="see-more">See more media</a>
                                        @endif
                                    </div>
                                    <?php
                                        $gf = $tp->comment ? explode(",", $tp->comment) : 0;
                                    ?>
                                    @if($gf && !empty($gf))
                                    <div class="trip-tags"><span class="title">Good for:</span>
                                        <ul class="tags-list">
                                            @foreach($gf AS $ggf)
                                            <li>{{$ggf}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif

                                    @if($tp->story)
                                        <div class="story">
                                            {!! $tp->story !!}
                                        </div>
                                    @endif

                                    @php
                                        $commentsCount = $tp->comments_count;
                                    @endphp

                                    @if ($tp->published && !$preview_mode)
                                        <div class="trip-place-reactions" style="display: flex; margin: 0 20px;">
                                                <div class="trip-place-reaction" style="margin: 17px 0;">
                                                        <a href="javascript:;" class="trip-place-like" style="text-decoration: none!important;">
                                                            <i class="@if ($userLikes->count()) trav-heart-fill-icon @else trav-heart-icon @endif like" style="color: #ff5a79; font-size: 18px;"></i>
                                                        </a>
                                                <span class="trip-place-like-count" style="vertical-align: top;"><b dir="auto">{{$allLikes->count()}}</b></span>
                                                </div>

                                                <div class="trip-place-reaction reaction-comments" style="display: flex; margin: 17px 0; margin-left: 25px;">
                                                    <div>
                                                        <a href="javascript:;" class="trip-place-like" style="text-decoration: none!important;display: flex;justify-content: center;align-items: center;height: 100%;">
                                                            <i class="trav-comment-icon"></i>
                                                        </a>
                                                    </div>
                                                    <span class="trip-place-like-count" style="vertical-align: top; margin-left: 5px;"><b dir="auto">{{$commentsCount}}</b></span>
                                                </div>
                                                @if(!$is_invited && $trip->privacy !== 2)
                                                <div class="trip-place-reaction share" style="display: flex; margin: 17px 0; margin-left: 25px;">
                                                    <div>
                                                        <a href="javascript:;" class="trip-place-like" style="text-decoration: none!important;display: flex;justify-content: center;align-items: center;height: 100%;">
                                                            <i class="trav-share-fill-icon"></i>
                                                        </a>
                                                    </div>
                                                    <span class="trip-place-like-count share" style="vertical-align: top; margin-left: 5px;font-size: 14px; padding-top: 2px;">Share</span>
                                                </div>
                                                @endif
                                        </div>
                                        <div class="comments">
                                        <div class="post-comment-layer-wrap">
                                            <div class="post-comment-layer report-comment-content">
                                                <div class="post-comment-top-info" id="{{$tp->id}}" {{($commentsCount > 0)?"":'style=display:none'}} >
                                                    <ul class="comment-filter">
                                                        <li class="comment-filter-type" data-type="Top" data-comment-type="1">@lang('other.top')</li>
                                                        <li class="comment-filter-type active" data-type="New" data-comment-type="1">@lang('other.new')</li>
                                                    </ul>
                                                    <div class="comm-count-info">
                                                        <span class="{{$tp->id}}-loading-count loading-count"><strong>0</strong></span> / <span class="{{$tp->id}}-all-comments-count all-comments-count">{{$commentsCount}}</span>
                                                    </div>
                                                </div>
                                                <div class="post-comment-wrapper sortBody report-comment-wraper trip-place-comments-wrapper" id="{{$tp->id}}">
                                                    @if($commentsCount > 0)
                                                        <div class="report-comment-main">
                                                        </div>
                                                        <div id="loadMore2" class="loadMore2" reportId="{{$tp->id}}" style="{{$commentsCount < 0?'display:none;':''}}">
                                                            <input type="hidden" id="pagenum" value="0">
                                                            <div id="reportloader" class="post-animation-content post-comment-row">
                                                                <div class="report-com-avatar-wrap">
                                                                    <div class="post-top-avatar-wrap animation post-animation-avatar"></div>
                                                                </div>
                                                                <div class="post-comment-text">
                                                                    <div class="report-top-name animation"></div>
                                                                    <div class="report-com-info animation"></div>
                                                                    <div class="report-com-sec-info animation"></div>
                                                                    <div id="hide_loader_2" class="report-com-thr-info animation"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="post-comment-top-info comment-not-fund-info" id="{{$tp->id}}">
                                                            <ul class="comment-filter">
                                                                <li>No comments yet ..</li>
                                                                <li></li>
                                                            </ul>
                                                            <div class="comm-count-info">
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                                @if($authUserId)
                                                    <div class="post-add-comment-block trip-place-comments-wrapper">
                                                        <div class="avatar-wrap">
                                                            <img src="{{$auth_picture}}" alt="" width="45px" height="45px">
                                                        </div>
                                                        <div class="post-add-com-inputs">
                                                            <form class="reportCommentForm" method="POST" data-id="{{$tp->id}}"  data-type="2" autocomplete="off" enctype="multipart/form-data">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" data-id="pair{{$tp->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                                                                <div class="post-create-block reports-comment-block" tabindex="0">
                                                                    <div class="post-create-input report-create-input">
                                        <textarea name="text" data-id="text" id="report_comment_text" class="textarea-customize report-comment-textarea report-comment-emoji"
                                                  placeholder="@lang('comment.write_a_comment')"></textarea>
                                                                    </div>
                                                                    <div class="post-create-controls d-none">
                                                                        <div class="post-alloptions">
                                                                            <ul class="create-link-list">
                                                                                <li class="post-options">
                                                                                    <input type="file" name="file[]" class="report-comment-media-input" data-report_id="{{$tp->id}}" data-id="commentfile{{$tp->id}}" style="display:none">
                                                                                    <i class="fa fa-camera click-target" aria-hidden="true" data-target="commentfile{{$tp->id}}"></i>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="comment-edit-action">
                                                                            <a href="javascript:;" class="report-comment-cancel-link">Cancel</a>
                                                                            <a href="javascript:;" class="post-report-comment-link report-comment-link">Post</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="medias report-media">
                                                                        <div class="spinner-border rep-spinner-border" role="status" style="display:none"><span class="sr-only">Loading...</span></div>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="report_id" value="{{$tp->id}}"/>
                                                                <button type="submit" class="btn btn-primary d-none"></button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                        </div>
                        @endif
                    </div>
                </div>
            </div>
                @php
                    $spentSoFar += $tp->budget;
                @endphp
            @endforeach

            @if(isset($do) && $do=='edit' && $plan_member)
                <div class="trip-add_place">
                    <button data-spent="{{$spentSoFar}}" @if($beforeCity) data-cityimg="{{@check_city_photo($beforeCity->city_medias[0]->url, 180, true)}}" data-citytitle="{{$beforeCity->trans[0] ? $beforeCity->trans[0]->title : ''}}" data-cityid="{{$beforeCity->id}}" data-citylat="{{$beforeCity->lat}}" data-citylng="{{$beforeCity->lng}}" @endif data-dateformat="{{\Carbon\Carbon::parse($day)->format('d F Y')}}" data-date="{{$day}}" data-order="{{$order}}"  class="btn btn-transparent btn-add_place" data-toggle="modal" data-target="#modal-add_place">@if ($is_admin)Add a Place @else Suggest a Place @endif</button>
                </div>
            @endif

            <div class="has-icon-text"><span class="image" style="background-image: url({{asset('trip_assets/images/ico-plane.svg')}});"></span> <span class="text"><span class="text text-start">Start</span><span class="text text-end">End</span></span></div>

        </div>
    </div>
    @endforeach
    @else
        @if(isset($do) && $do=='edit' && $plan_member)
            <div class="trip-add_place">
                @php
                    $addDay = \Carbon\Carbon::create();

                    if (!$trip->memory) {
                        $addDay->addDay();
                    }

                    $addDay = $addDay->format('Y-m-d');
                @endphp

                <button data-spent="{{$spentSoFar}}" data-dateformat="{{\Carbon\Carbon::parse($addDay)->format('d F Y')}}" data-date="{{$addDay}}" data-order="0" class="btn btn-transparent btn-add_place" data-toggle="modal" data-target="#modal-add_place">@if ($is_admin)Add a Place @else Suggest a Place @endif</button>
            </div>
        @else
            <div class="trip-add_place" style="position: relative; top: -7px; margin-bottom: 0px;">
                <button class="btn btn-transparent" disabled>The timeline is empty. Please check back when the creators have added something.</button>
            </div>
        @endif
    @endif
    <input type="hidden" id="overall-budget" value="{{$overall_budget}}">
</div>
</div>
</div>

<div class="debug" style="display: none;">{{json_encode($debug)}}</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="{{ asset('assets2/js/summernote/summernote.js') }}"></script>
<script src="{{ asset('/plugins/summernote-master/tam-emoji/js/config.js') }}"></script>
<script src="{{ asset('/plugins/summernote-master/tam-emoji/js/tam-emoji.js?v='.time()) }}"></script>
<script src="{{ asset('/plugins/summernote-master/tam-emoji/js/textcomplete.js?v='.time()) }}"></script>
<script src="{{url('assets2/js/script.js')}}"></script>
<script src="{{ asset('assets2/js/posts-script.js?v='.time()) }}"></script>

@include('site.trip2.partials._plan-share_script')

<script>
//photos sorting by type
function itemsSort(obj, type){
var list, i, switching, shouldSwitch, switchcount = 0;
switching = true;
parent_obj = $('.post-media-inner').find('.post-media-list');
while (switching) {
    switching = false;
    list = parent_obj.find('>div');
    shouldSwitch = false;
    for (i = 0; i < list.length - 1; i++) {
        shouldSwitch = false;
        if(type == "date_up")
        {
            if ($(list[i]).attr('datesort') > $(list[i + 1]).attr('datesort')) {
                shouldSwitch = true;
                break;
            }
        }
        else if(type == "date_down")
        {
            if ($(list[i]).attr('datesort') < $(list[i + 1]).attr('datesort')) {
                shouldSwitch = true;
                break;
            }
        }
        else if(type == "most_liked")
        {

            if ($(list[i]).find('.liked-count b').html() < $(list[i + 1]).find('.liked-count b').html()) {
                shouldSwitch = true;
                break;
            }
        }
        else if(type == "most_commented")
        {
            if ($(list[i]).find('.media-commented-count b').html() < $(list[i + 1]).find('.media-commented-count b').html()) {
                shouldSwitch = true;
                break;
            }
        }
    }
    if (shouldSwitch) {
        $(list[i]).before(list[i + 1]);
        switching = true;
        switchcount ++;
    } else {
        if (switchcount == 0 && list.length > 1 && i == 0) {
            switching = true;
        }
    }
}
LoadMore_Show.reload($('.post-media-inner'));
}

$('body').off('click', '.photo_like_button');
// photos like & unlike photo
$('body').on('click', '.photo_like_button', function (e) {
mediaId = $(this).attr('id');
$.ajax({
    method: "POST",
    url: "{{ route('media.trip-place.likeunlike') }}",
    data: {media_id: mediaId}
})
    .done(function (res) {
        var result = JSON.parse(res);

        $('.'+ mediaId +'-like-count b').html(result.count)
        $('.'+ mediaId +'-media-liked-count b').html(result.count)
        $('div.counters[data-mediaid=' + mediaId + '] div.trip-card__likes span').html(result.count)

        if (result.status == 'yes') {
            $('.'+ mediaId +'-media-like-icon').html('<i class="trav-heart-fill-icon" style="color:#ff5a79;"></i>')
            $('.'+ mediaId +'-media-like-icon').attr('data-type', 'like')
            $('i.photo_like_button[id=' + mediaId + ']').removeClass('trav-heart-icon');
            var classes = $('i.photo_like_button[id=' + mediaId + ']').attr('class');
            var classes = 'trav-heart-fill-icon ' + classes;
            $('i.photo_like_button[id=' + mediaId + ']').attr('class', classes);
        } else if (result.status == 'no') {
            $('.'+ mediaId +'-media-like-icon').html('<i class="trav-heart-icon"></i>')
            $('.'+ mediaId +'-media-like-icon').attr('data-type', 'unlike')
            $('i.photo_like_button[id=' + mediaId + ']').removeClass('trav-heart-fill-icon');
            var classes = $('i.photo_like_button[id=' + mediaId + ']').attr('class');
            var classes = 'trav-heart-icon ' + classes;
            $('i.photo_like_button[id=' + mediaId + ']').attr('class', classes);
        }

        updateMobileReactions();
    });
e.preventDefault();
});

$(document).find('div.time-control div.swiper-wrapper').html('');

$('div.btn-group-toggle-days .btn.btn-toggle').each(function(i, e) {
var day = $(e).data('day');
var month = $(e).data('month');
var date = $(e).data('date');

$(document).find('div.time-control div.swiper-wrapper').append('<div class="swiper-slide" data-date="' + date + '">\n' +
    '<p class="day">' + day + '</p>\n' +
    '<p class="month">' + month + '</p>\n' +
    '</div>')
});

var swiperDays = new Swiper('.swiper-days', {
slidesPerView: 3,
spaceBetween: 10,
centeredSlides: true,
slideToClickedSlide: true,
preventInteractionOnTransition: true,
navigation: {
    nextEl: '.swiper-days-control .swiper-next',
    prevEl: '.swiper-days-control .swiper-prev'
}
});

swiperDays.off('slideChangeTransitionEnd');
swiperDays.on('slideChangeTransitionEnd', function (e) {
    var date = $(document).find('.swiper-slide.swiper-slide-active').data('date');
    $('div.btn-group-toggle-days .btn.btn-toggle[data-date="' + date + '"').trigger('click');
    generatePortion();
});

$('.swiper-next, .swiper-prev').off('click');
$('.swiper-next, .swiper-prev').on('click', function() {
    // $('.mobile-map-view-controls button.active').removeClass('active');
    // $('.mobile-map-view-controls button.map-view').addClass('active');
    // $('.page-planner .sidebar').addClass('mobile-hide');
});

var positions = [];
var positionsHorizontal = [];
updatePositions();

function updatePositions() {
    var position = $('div.trip-day:first').position();

    if (!position) {
        return;
    }

    var top = position.top;
    var left = position.left;

    $('div#planContents div.trip-day').each(function(i, e) {
        positions[i] = $(e).position().top - top;
        positionsHorizontal[i] = $(e).position().left - left;
    });
}

function moveSlider(date, time = false) {
    $('div.trip-day').removeClass('active');
    $('div.trip-day.' + date).addClass('active');

    var slideIndex = $(document).find('.swiper-slide[data-date="' + date + '"').index();

    if (time !== false) {
        swiperDays.slideTo(slideIndex, 100, false);
        generatePortion();
        $('div.curent-time[data-time=' + time + ']').addClass('active');
        return;
    }

    swiperDays.slideTo(slideIndex);

    var position = positions[$('div.trip-day.' + date).index()];
    var positionHorizontal = positionsHorizontal[$('div.trip-day.' + date).index()];

    $('div.trip').animate({
        scrollTop: position
    }, 500);

    var isRow = $('div.trip').css('flex-direction') == 'row';

    if (isRow) {
        $('div.trip').animate({
            scrollLeft: positionHorizontal
        }, 500);
    }

    $('div.trip-day.' + date).find('div.card-inner:first').trigger('click');
}

function generatePortion()
{
$(document).find('div.timeline-holder div.curent-time').remove();

$('div.trip-day.active').find('div.trip-place-wrap').each(function(i, e) {
   var time = $(e).data('time');

   var value = time * 100 / 1440;

   $(document).find('div.timeline-holder').append('<div data-time="' + time + '" class="curent-time" style="left: ' + value + '%;"></div>')

});
}

generatePortion();

$(document).off('click', 'div.curent-time');
$(document).on('click', 'div.curent-time', function() {
    $(document).find('div.curent-time.active').removeClass('active');
    $(this).addClass('active');

    var time = $(this).data('time');

    scrollSideBar($(document).find('div.trip-day.active').find('div.trip-place-wrap[data-time="' + time + '"]').attr('data-placeid'));

    $(document).find('div.trip-day.active').find('div.trip-place-wrap[data-time="' + time + '"]').find('a.plan-place').trigger('click');
});

function reloadComments(self = null, tripPlaceId = null, last = false)
{
    var page = 1;
    var perPage = '';

    if (self) {
        page = $(self).attr('next-page');
        perPage = '';
    }

    if (last) {
        page = 1;
        perPage = '&per-page=1';
    }

    $.ajax({
        method: "GET",
        url: '/trip-places/load-more-comments?page=' + page + perPage,
        data: {trip_place_id: tripPlaceId}
    })
        .done(function (res) {
            if (res.data.meta.to >= res.data.meta.total) {
                $(self).hide();
            } else if (!last) {
                $(self).show();
            }

            if (!last) {
                $(self).attr('next-page', res.data.meta.current_page + 1);
            }

            //$(self).parent().find('.post-comment-row').remove();
            $(self).parents('.post-comment-layer').find('.comm-count-info.pagination span:last').text(res.data.meta.total);

            res.data.comments.forEach(function (el, i) {
                var content = '<div class="post-comment-row">\n' +
                    '                        <div class="post-com-avatar-wrap">\n' +
                    '                        <img src="' + el.user.img + '" alt="">\n' +
                    '                        </div>\n' +
                    '                        <div class="post-comment-text">\n' +
                    '                        <div class="post-com-name-layer">\n' +
                    '                        <a href="#" class="comment-name">' + el.user.name + '</a>\n' +
                    '                        <a href="#" class="comment-nickname">@' + el.user.username + '</a>\n' +
                    '                        </div>\n' +
                    '                        <div class="comment-txt">\n' +
                    '                        <p>' + el.comment + '</p>\n' +
                    '                    </div>\n' +
                    '                    <div class="comment-bottom-info">\n' +
                    '                        <div class="com-reaction">\n' +
                    '                        <img src="/assets/image/icon-smile.png" alt="">\n' +
                    '                        <span>0</span>\n' +
                    '                        </div>\n' +
                    '                        <div class="com-time">' + el.created + '</div>\n' +
                    '                    </div>\n' +
                    '                    </div>\n' +
                    '                    </div>';

                if (!last) {
                    $(self).closest('div.post-comment-wrapper').prepend(content);
                } else {
                    $(self).before(content);
                }
            });

            $(self).parents('.post-comment-layer').find('.comm-count-info.pagination span:first').text($(self).parent().find('.post-comment-row').length);

        });
}

$(document).ready(function() {

// Get media - with autoplay disabled (audio or video)
var media = $('div.card-details div.large-images div.large-image video').not("[autoplay='autoplay']");
var placeWrapper = $('div.trip-place-wrap');
var tolerancePixel = 40;

function checkScroll(e) {
    updatePositions();

    if ($(this).is(':animated')) {
        return;
    }

    // Get current browser top and bottom
    var scrollTop = $(window).scrollTop() + tolerancePixel;
    var scrollLeft = $(window).scrollLeft() + tolerancePixel;
    var scrollBottom = $(window).scrollTop() + $(window).height() - tolerancePixel;

    media.each(function(index, el) {
        var yTopMedia = $(this).offset().top;
        var yBottomMedia = $(this).height() + yTopMedia;

        if(scrollTop < yBottomMedia && scrollBottom > yTopMedia) {
            $(this).get(0).play();
            $(el).prop('muted', true);
        } else {
            $(this).get(0).pause();
        }
    });

    var isRow = $('div.trip').css('flex-direction') == 'row';

    var divTripOffset = $('div.trip').scrollTop() + $('div.trip').outerHeight();
    var divTripHeight = $('div.trip').get(0).scrollHeight;

    var divTripOffsetHorizontal = $('div.trip').scrollLeft() + $('div.trip').outerWidth();
    var divTripWidth = $('div.trip').get(0).scrollWidth;

    if ((!isRow && divTripOffset >= divTripHeight) || (isRow && divTripOffsetHorizontal >= divTripWidth)) {
        var latestPlaceId = $('div.trip-place-wrap').last().data('placeid');
        var latestOrigPlaceId = $('div.trip-place-wrap').last().data('origplaceid');
        var latestDate = $('div.trip-place-wrap').last().data('date');

        if (opened != latestPlaceId) {
            $('div.btn-group-toggle-days .btn.btn-toggle').removeClass('active');
            $('div.btn-group-toggle-days .btn.btn-toggle[data-date="' + latestDate + '"').addClass('active');
            $('div.btn-group-toggle-days .btn.btn-toggle[data-date="' + latestDate + '"')[0].scrollIntoView({
                behavior: 'smooth'
            });
            moveToMarker(latestPlaceId);
            opened = latestPlaceId;

            if (e.originalEvent && e.originalEvent.isTrusted) {
                //$('div.trip-place-wrap').last().find(".report-comment-wraper.trip-place-comments-wrapper").trigger('scroll');
            }
        }

        return;
    }


    placeWrapper.each(function(index, el) {
        var yTopMedia = $(this).offset().top;
        var yBottomMedia = $(this).height() + yTopMedia;

        var xLeftMedia = $(this).offset().left;
        var xRightMedia = $(this).height() + xLeftMedia;

        var height = $(this).get(0).scrollHeight + 60;

        if ((!isRow && scrollTop < yBottomMedia && (scrollTop + height) > yTopMedia) || (isRow && scrollLeft < xRightMedia && (scrollLeft + 100) > xLeftMedia))  {
            if (opened != $(this).data('placeid')) {
                $('div.btn-group-toggle-days .btn.btn-toggle').removeClass('active');
                $('div.btn-group-toggle-days .btn.btn-toggle[data-date="' + $(this).data('date') + '"').addClass('active');
                $('div.btn-group-toggle-days .btn.btn-toggle[data-date="' + $(this).data('date') + '"')[0].scrollIntoView({
                    behavior: 'smooth'
                });
                moveToMarker($(this).data('placeid'));
                opened = $(this).data('placeid');

                if (e.originalEvent && e.originalEvent.isTrusted) {
                    //$(this).find(".report-comment-wraper.trip-place-comments-wrapper").trigger('scroll');
                }

                return false;
            }
        }
    });
}

$('div.trip').off('scroll', checkScroll);
$('div.trip').on('scroll', checkScroll);

$(document).off('click', 'div.comments a.load-more-link');
$(document).on('click', 'div.comments a.load-more-link', function (e) {
    var tripPlaceId = $(this).data('id');
    var self = $(this);

    $(this).parents('.csomments').find('div.post-add-comment-block').show();
    $(this).parents('.comments').find('div.post-add-comment-block').css('display', 'flex');

    reloadComments(self, tripPlaceId);
});

});

    @if(isset($trip_places_arr))
        trip_places_arr = <?= json_encode($trip_places_arr);?>;
        saved_trip_places_arr = [...trip_places_arr];
        usePreload = true;
    @else
        trip_places_arr = 0;
    @endif
</script>

<script>
    //----START---- comments scripts

    $(document).ready(function(){
        document.emojiSource = "{{ asset('plugins/summernote-master/tam-emoji/img') }}";
        document.textcompleteUrl = "{{ url('reports/get_search_info') }}";


        $('div.report-alert').delay(3000).slideUp(300);
        $(document).off('click', '.report-comment-cancel-link');
        $(document).on('click', '.report-comment-cancel-link', function(){
            var form = $(this).closest('form');
            form.find('.post-create-controls').addClass('d-none')
            form.find('.emojionearea-button').addClass('d-none');
            form.find('textarea').val('');
            form.find('.note-editable').trigger('blur');
            form.find(".note-editable").html('');
            form.find(".note-placeholder").addClass('custom-placeholder')
            form.find('.medias').find('.removeFile').each(function(){
                $(this).click();
            });
            form.find('.medias').empty();

        })

        $(document).off('click', '.comment-filter-type');
        $(document).on('click', '.comment-filter-type', function(){
            var filter_type = $(this).data('type');
            if($(this).data('comment-type') == 1){
                var reload_obj = $('.report-comment-contentss');
            }else{
                var reload_obj = $('.report-modal-comment-content');
            }

            reportCommentSort(filter_type, $(this), reload_obj);
        });

        $(document).off('click', '#commentsPopupBtn');
        $(document).on('click', '#commentsPopupBtn', function(){
            $('#commentsPopup').modal('show')
        });

        $(document).off('click', '.report-comment-cnt');
        $(document).on('click', '.report-comment-cnt', function(){
            $('html, body').animate({ scrollTop:$('.post-comment-layer-wrap').position().top + 600}, 400);
        });

        $(".comment-like-people-block").off( 'scroll');
        //load more comment like user
        $(".comment-like-people-block").on( 'scroll', function(){

            if(visibleY(document.getElementById('hide_comlike_loader'))){
                loadMoreCommentLikeUsers();
            }
        });


        $(".report-like-users").off( 'scroll');
        //load more comment like user
        $(".report-like-users").on( 'scroll', function(){
            if(visibleY(document.getElementById('hide_replike_loader'))){
                loadMoreReportLikeUsers();
            }
        });


        $(".add-report-plan-inner").off( 'scroll');
        //load more plan
        $(".add-report-plan-inner").on( 'scroll', function(){
            if(visibleY(document.getElementById('hide_repPlan_loader'))){
                loadMorePlan();
            }
        });
        // Enable edit button
        setTimeout(function() {
            $('#editReport').removeAttr('disabled');
        }, 2000);

        $(".report-comment-wraper.trip-place-comments-wrapper").each(function() {
            loadMoreComment(2, $(this));
        });
    });


    $(document).off('click', ".read-more-link");
    // More/less content
    $(document).on('click', ".read-more-link", function(){
        $(this).closest('.comment-txt').find('.less-content').hide()
        $(this).closest('.comment-txt').find('.more-content').show()
        $(this).hide()
    });


    $(document).off('click', ".read-less-link");
    $(document).on('click', ".read-less-link", function(){
        $(this).closest('.more-content').hide()
        $(this).closest('.comment-txt').find('.less-content').show()
        $(this).closest('.comment-txt').find('.read-more-link').show()
    });

    //$(document).on('click', ".morelink", function(){
    //    var moretext = "see more";
    //    var lesstext = "see less";
    //    if($(this).hasClass("less")) {
    //        $(this).removeClass("less");
    //        $(this).html(moretext);
    //    } else {
    //        $(this).addClass("less");
    //        $(this).html(lesstext);
    //    }
    //    $(this).parent().prev().toggle();
    //    $(this).prev().toggle();
    //    return false;
    //});

    $('body').off('click', '.report_comment_like');
    //Report comment like
    $('body').on('click', '.report_comment_like', function (e) {
        var _this = $(this);
        commentId = $(this).attr('id');

        if ($(this).parents('.trip-place-comments-wrapper').length) {
            var url = "{{ route('trip_places.commentlikeunlike') }}"
        } else {
            var url = "{{ route('media.commentlikeunlike') }}"
        }

        $.ajax({
            method: "POST",
            url: url,
            data: {comment_id: commentId}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                if (result.status == 'yes') {
                    _this.find('i').addClass('fill');
                } else if (result.status == 'no') {
                    _this.find('i').removeClass('fill');
                    _this.parent().find('.comment-likes-block span').each(function () {
                        if ($(this).text() == result.name)
                            $(this).remove();
                    });
                }
                _this.parent().find('.'+ commentId +'-comment-like-count').html(result.count);
                updateMobileReactions();
            });
        e.preventDefault();
    });

    $('body').off('click', '.report-comment-likes-block');
    $('body').on('click', '.report-comment-likes-block', function (e) {
        var _this = $(this);
        commentId = $(this).attr('data-id');
        var like_user_count = $('.'+commentId +'-comment-like-count').html();

        if ($(this).parents('.trip-place-comments-wrapper').length) {
            var url = "{{ route('trip_places.commentlikeusers') }}";
        } else {
            var url = "{{ route('media.commentlikeusers') }}";
        }

        $.ajax({
            method: "POST",
            url: url,
            data: {comment_id: commentId}
        })
            .done(function (res) {
                $('.post-comment-like-users').html(res)
                if(like_user_count > 7){
                    $('#loadMoreUsers').removeClass('d-none');
                    $('#loadMoreUsers').attr('commentId', commentId);
                }
                $('#commentslikePopup').find('.comment-like-count').html(like_user_count+ ' Likes')
                $('#commentslikePopup').modal('show')
            });
        e.preventDefault();
    });

    $('body').off('mouseover', '.news-feed-comment');
    $('body').on('mouseover', '.news-feed-comment', function () {
        $(this).find('.post-com-top-action .dropdown').show()
    });

    $('body').off('mouseout', '.news-feed-comment');
    $('body').on('mouseout', '.news-feed-comment', function () {
        $(this).find('.post-com-top-action .dropdown').hide()
    });

    $('body').off('mouseover', '.doublecomment');
    $('body').on('mouseover', '.doublecomment', function () {
        $(this).find('.post-com-top-action .dropdown').show()
    });

    $('body').off('mouseout', '.doublecomment');
    $('body').on('mouseout', '.doublecomment', function () {
        $(this).find('.post-com-top-action .dropdown').hide()
    });

    $('body').off('click', '.remove-media-comment-file');
    $('body').on('click', '.remove-media-comment-file', function (e) {
        var media_id = $(this).attr('data-media_id');
        $(this).closest('.medias').append('<input type="hidden" name="existing_medias[]" value="' + media_id + '">')
        $(this).parent().remove();
    });

    $('body').off('click', '.post-report-comment-link');
    $('body').off('submit', '.reportCommentForm');

    $('body').on('click', '.post-report-comment-link', function (e) {
        $(this).closest('.reportCommentForm').find('button[type=submit]').click();
    });

    $('body').on('submit', '.reportCommentForm', function (e) {
        e.preventDefault();
        var form = $(this);
        var type = form.data('type');
        var entity_id = form.attr('data-id');
        var values = form.serialize();
        var comment_text = form.find('textarea').val().trim();
        var all_comment_count = $('.'+ entity_id +'-all-comments-count').html();
        var loading_count =  $('.'+ entity_id +'-loading-count strong').html();
        var files = form.find('.medias').find('div').length;
        var _this = this;

        if(comment_text == '' && files == 0){
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }

        form.find('.report-comment-link').hide();
        form.find('.report-comment-cancel-link').hide();

        if ($(this).parents('.trip-place-comments-wrapper').length) {
            var url ='/trip-places/' + entity_id + '/comment';
        } else {
            var url ='/medias/' + entity_id + '/comment';
        }

        $.ajax({
            method: "POST",
            url: url,
            data: values
        })
            .done(function (res) {
                form.find('.medias').find('.removeFile').each(function(){
                    $(this).click();
                });
                form.find('.medias').empty();
                form.find(".note-editable").html('');
                form.find('textarea').val('')
                form.find('.report-comment-link').show()
                form.find('.report-comment-cancel-link').show()
                form.find(".note-placeholder").addClass('custom-placeholder')

                form.find('.post-create-controls').addClass('d-none');
                form.find('.emojionearea-button').addClass('d-none');

                $("#" + entity_id + ".comment-not-fund-info").remove();
                $("#" + entity_id + ".post-comment-top-info").show();
                $('.'+ entity_id +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                form.parents('.card-trip_info').find('div.reaction-comments span b').html(parseInt(all_comment_count) + 1);
                $('.'+ entity_id +'-loading-count strong').html(parseInt(loading_count) + 1);


                if ($(_this).parents('.trip-place-comments-wrapper').length) {
                    if(all_comment_count == 0){
                        form.parents('.report-comment-content').find("#" + entity_id + ".post-comment-wrapper").prepend('<div class="report-comment-main">'+res+'</div>').fadeIn('slow');
                    }else{
                        form.parents('.report-comment-content').find(".report-comment-main").prepend(res).fadeIn('slow');
                    }
                } else {
                    if(all_comment_count == 0){
                        form.parents('.gallery-comment-wrap').find("#" + entity_id + ".report-comment-wraper").append('<div class="report-comment-main">'+res+'</div>').fadeIn('slow');
                    }else{
                        form.parents('.gallery-comment-wrap').find(".report-comment-main").prepend(res).fadeIn('slow');
                    }
                }

                updateMobileReactions();

                form.find('div.note-editable').blur();
            });

    });


    $('body').off('click', '.report-edit-comment');
    // Edit comment
    $('body').on('click', '.report-edit-comment', function (e) {
        var _this = $(this);

        var commentId = _this.attr('data-id');
        editReportCommentMedia(commentId)
        var postId = _this.attr('data-report');
        _this.closest('.post-comment-text').find('.commentEditForm' + commentId).removeClass('d-none')
        _this.closest('.post-comment-text').find('.comment-text-' + commentId + ' p').addClass('d-none')

        CommentEmoji($('.commentEditForm' + commentId));
        $('.commentEditForm' + commentId).find(".note-placeholder").removeClass('custom-placeholder')
        e.preventDefault();
    });

    $('body').off('click', '.edit-cancel-link');
    $('body').on('click', '.edit-cancel-link', function (e) {
        var _this = $(this);

        var commentId = _this.attr('data-comment_id');
        $('.commentEditForm' + commentId).addClass('d-none')
        $('.comment-text-' + commentId + ' p').removeClass('d-none')

        e.preventDefault();
    });


    $(document).off('change', '.report-comment-media-input');
    $(document).on('change', '.report-comment-media-input', function (e) {
        var report_id = $(this).attr('data-report_id')
        $(this).closest('form').find('.medias').find('.removeFile').each(function(){
            $(this).click();
        });
        commentMediaUploadFile($(this), $(this).closest(".reportCommentForm"), report_id);

    })


    $(document).off('change', '.report-modal-media-input');
    $(document).on('change', '.report-modal-media-input', function (e) {
        var report_id = $(this).attr('data-report_id')
        $(this).closest('form').find('.medias').find('.removeFile').each(function(){
            $(this).click();
        });
        commentMediaUploadFile($(this), $(this).closest(".reportCommentForm"), report_id);

    })

    $('body').off('click', '.edit-report-comment-link');
    $('body').on('click', '.edit-report-comment-link', function (e) {
        var _this = $(this);
        var commentId = _this.attr('data-comment_id');
        var form = _this.closest('.commentEditForm' + commentId);

        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;
        var comment_type = form.find("input[name='comment_type']").val();

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        form.find('.report-comment-link').hide()
        var values = form.serialize();

        if ($(this).parents('.trip-place-comments-wrapper').length) {
            var url = "{{ route('trip_places.commentedit') }}"
        } else {
            var url = "{{ route('media.commentedit') }}"
        }

        $.ajax({
            method: "POST",
            url: url,
            data: values
        })
            .done(function (res) {
                form.find('.medias').find('.removeFile').each(function () {
                    $(this).click();
                });
                form.find('.report-comment-link').show()
                if (comment_type == 1) {
                    $('.commentRow' + commentId).parent().html(res);
                } else {
                    $('.commentRow' + commentId).replaceWith(res);
                }
            });
        e.preventDefault();
    });

    var check_comment_id;
    $('body').off('click', '.mediaCommentReply');
    $('body').on('click', '.mediaCommentReply', function (e) {
        commentId = $(this).attr('id');
        $('.'+ commentId +'-comment-reply-block').toggle();
        $('.replyForm' + commentId).show();

        if ( $('.'+ commentId +'-comment-reply-block').is(':visible')) {
            //$('.report-comment-wraper').animate({ scrollTop: $('.commentRow' + commentId).position().top}, 400);
        }

        if (commentId != check_comment_id) {
            check_comment_id = commentId;
            CommentEmoji($('.replyForm' + commentId));
        }

        e.preventDefault();
    });

    // Reply comment
    $(document).off('change', '.report-comment-reply-media-input');
    $(document).on('change', '.report-comment-reply-media-input', function (e) {
        var comment_id = $(this).attr('data-comment-id')
        $(this).closest('form').find('.medias').find('.removeFile').each(function(){
            $(this).click();
        });
        commentMediaUploadFile($(this), $(this).closest(".commentReplyForm"+comment_id), comment_id);

    })

    $(document).off('change', '.report-modal-reply-media-input');
    $(document).on('change', '.report-modal-reply-media-input', function (e) {
        var comment_id = $(this).attr('data-comment-id')
        $(this).closest('form').find('.medias').find('.removeFile').each(function(){
            $(this).click();
        });
        commentMediaUploadFile($(this), $(this).closest(".commentModalReplyForm"+comment_id), comment_id);

    })

    $('body').off('click', '.report-reply-link');
    $('body').on('click', '.report-reply-link', function(e){
        var comment_id = $(this).attr('data-id');
        var form = $(this).closest('.commentReplyForm' + comment_id);
        replyReportComment(form, comment_id);
        $(this).val('')
    });


    function replyReportComment(form, parent_id) {
        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;
        var replies_count = $('.'+ parent_id+'-rep-reply span').text();
        var reply_int_count = (replies_count !='')?parseInt(replies_count):0;
        var entity_id = form.attr('data-id');

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        var values = form.serialize();
        form.find('.report-comment-link').hide();

        if ($(form).parents('.trip-place-comments-wrapper').length) {
            var url ='/trip-places/' + entity_id + '/comment';
        } else {
            var url ='/medias/' + entity_id + '/comment';
        }

        $.ajax({
            method: "POST",
            url: url,
            data: values
        })
            .done(function (res) {
                form.parent().parent().before(res);
                form.find('.medias').find('.removeFile').each(function () {
                    $(this).click();
                });
                form.find('.report-comment-link').show()
                form.find('textarea').val('');
                form.find(".note-editable").html('');
                form.find(".note-placeholder").addClass('custom-placeholder')
                form.find('.post-create-controls').addClass('d-none');
                form.find('.emojionearea-button').addClass('d-none');

                form.find('.medias').empty();
                $('.'+ parent_id+'-rep-reply').html('<span>'+ (reply_int_count+1) +'</span> Replies')
                updateMobileReactions();
            });
    }

    //delete comment
    $('body').off('click', '.report-comment-delete');
    $('body').on('click', '.report-comment-delete', function (e) {
        var _this = $(this);
        var comment_id = _this.attr('id');
        var report_id = _this.attr('reportid');
        var allReportCommentCount = $('.'+ report_id +'-all-comments-count').html();
        var reload_obj = _this.closest('.report-comment-content');
        var type = _this.attr('data-del-type');
        var parent_id = _this.data('parent');
        var loading_count =  $('.'+ report_id +'-loading-count strong').html();
        var replies_count = $('.'+ parent_id+'-rep-reply span').text();

        if ($(this).parents('.trip-place-comments-wrapper').length) {
            var url = "{{ route('trip_places.commentdelete') }}"
        } else {
            var url = "{{ route('media.commentdelete') }}"
        }

        $.confirm({
            title: 'Delete Comment!',
            content: 'Are you sure you want to delete this comment? <div class="mb-3"></div>',
            columnClass: 'col-md-5 col-md-offset-5',
            closeIcon: true,
            offsetTop: 0,
            offsetBottom: 500,
            scrollToPreviousElement:false,
            scrollToPreviousElementAnimate:false,
            buttons: {
                cancel: function () {},
                confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'shift'],
                    action: function(){
                        $.ajax({
                            method: "POST",
                            url: url,
                            data: {comment_id: comment_id}
                        })
                            .done(function (res) {
                                if(res){
                                    $('.commentRow' + comment_id).fadeOut();
                                    $('.'+ report_id +'-all-comments-count').html(parseInt(allReportCommentCount) - 1)
                                    _this.parents('.card-trip_info').find('div.reaction-comments span b').html(parseInt(allReportCommentCount) - 1);
                                    $('.'+ report_id +'-loading-count strong').html(parseInt(loading_count) - 1);
                                    if ($(_this).parents('.trip-place-comments-wrapper').length) {
                                        loadMoreComment(2, $(_this).parents('.report-comment-wraper'));
                                    } else {
                                        loadMoreComment(1, $(_this).parents('.report-comment-wraper'));
                                    }

                                    if (type == 1) {
                                        $('.commentRow' + comment_id).parent().remove();

                                    } else {
                                        if (_this.closest('.report-comment-content').find('*[data-parent_id="' + parent_id + '"]:visible').length == 1) {
                                            $('.commentRow' + parent_id).find('.report-comment-delete').removeClass('d-none')
                                            $('.commentRow' + parent_id).find('.delete-line').removeClass('d-none')
                                        }

                                        if(replies_count == 1){
                                            $('.'+ parent_id+'-rep-reply').html('<span></span> Reply')
                                        }else{
                                            $('.'+ parent_id+'-rep-reply').html('<span>'+ (parseInt(replies_count)-1) +'</span> Replies')
                                        }
                                    }
                                }
                            });
                    }
                }
            }
        });
        e.preventDefault();
    });



    // For spam reporting
    function injectData(id, obj)
    {
        // var button = $(event.relatedTarget);
        var posttype = $(obj).parent().attr("posttype");
        $("#spamReportDlg").find("#dataid").val(id);
        $("#spamReportDlg").find("#posttype").val(posttype);
    }


    function editReportCommentMedia(id) {
        $('.commenteditfile' + id).off('change');
        $('.commenteditfile' + id).on('change', function () {
            $(this).closest('form').find('.medias').find('.removeFile').each(function(){
                $(this).click();
            });
            $(this).closest('form').find('.medias').find('.remove-media-comment-file').each(function(){
                $(this).click();
            });
            commentMediaUploadFile($(this), $(".commentEditForm" + id), id);
        });
    }


    //Sort comment
    function reportCommentSort(type, obj, reload_obj){
        if($(obj).hasClass('active'))
            return;

        $(obj).parent().find('li').removeClass('active');
        $(obj).addClass('active');
        var list, i, switching, shouldSwitch, switchcount = 0;
        switching = true;
        parent_obj = $(obj).closest('.post-comment-layer').find('.report-comment-main');
        while (switching) {
            switching = false;
            list = parent_obj.find('>div');
            shouldSwitch = false;
            for (i = 0; i < list.length - 1; i++) {
                shouldSwitch = false;
                if(type == "Top")
                {
                    if ($(list[i]).attr('topsort') < $(list[i + 1]).attr('topsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "New")
                {
                    if ($(list[i]).attr('newsort') < $(list[i + 1]).attr('newsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                $(list[i]).before(list[i + 1]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && list.length > 1 && i == 0) {
                    switching = true;
                }
            }
        }
    }

    var loadMoreCommentsRequest = [];

    //load more comment
    function loadMoreComment(type, elem, nextPage = null) {
        if (!nextPage) {
            nextPage = parseInt($(elem).find('#pagenum').val()) + 1;
        }

        if (!nextPage) {
            return;
        }

        if (type === 1) {
            var report_id = $(elem).find('.loadMore1').attr('reportId');
            var all_comment_count = $('.'+ report_id +'-all-comments-count').html();
            var load_count = $('.'+ report_id + '-loading-count strong').html();
            var url = "{{route('media.load_more_comment')}}";
            var data = {pagenum: nextPage, media_id: report_id}
        } else {
            var report_id = $(elem).find('.loadMore2').attr('reportId');
            var all_comment_count = $('.'+ report_id +'-all-comments-count').html();
            var load_count = $('.'+ report_id + '-loading-count strong').html();
            var url = "{{route('trip_places.load_more_comment')}}";
            var data = {pagenum: nextPage, trip_place_id: report_id}
        }

        if (data.media_id === undefined && data.trip_place_id === undefined) {
            return;
        }

        if (loadMoreCommentsRequest[report_id]) {
            loadMoreCommentsRequest[report_id].abort();
        }

        loadMoreCommentsRequest[report_id] = $.ajax({
            type: 'POST',
            url: url,
            data: data,
            async: true,
            success: function (data) {
                if(data != ''){
                    $(elem).find('.report-comment-main').append(data);

                    var append_count = parseInt(load_count) + 5;
                    if(append_count < parseInt(all_comment_count)){
                        $('.'+ report_id + '-loading-count strong').html(append_count)
                    }else{
                        $('.'+ report_id + '-loading-count strong').html(parseInt(all_comment_count))

                        if (type === 1) {
                            $(elem).find(".loadMore1").hide();
                        } else {
                            $(elem).find(".loadMore2").hide();
                        }
                    }
                    $(elem).find('#pagenum').val(nextPage);
                } else {
                    if (type === 1) {
                        $(elem).find(".loadMore1").hide();
                    } else {
                        $(elem).find(".loadMore2").hide();
                    }
                }
            }
        });
    }

    //Comment summernote emoji
    function CommentEmoji(element, focus = false) {
        element.find(".report-comment-emoji").summernote({
            airMode: false,
            focus: focus,
            disableDragAndDrop: true,
            placeholder: 'Write a comment',
            toolbar: [
                ['insert', ['emoji']],
                ['custom', ['textComplate']],
            ],
            callbacks: {
                onFocus: function(e) {
                    $(e.target).closest('form').find(".note-placeholder").removeClass('custom-placeholder')
                    $(e.target).closest('form').find('.post-create-controls').removeClass('d-none');
                    $(e.target).closest('form').find('.emojionearea-button').removeClass('d-none');

                },
                onInit: function(e) {
                    e.editingArea.find(".note-placeholder").addClass('custom-placeholder')
                }
            }
        });
    }

    $(document).off('click', 'i.fa-camera');
    $(document).on('click', 'i.fa-camera', function () {
        $("#createPosTxt").click();
        var target = $(this).attr('data-target');
        var target_el = $(this).parent().find('*[data-id="'+target+'"]');
        target_el.trigger('click');
    })

    $('body').off('click', '.reportCommentForm');
    $('body').on('click', '.reportCommentForm', function(e) {
        CommentEmoji($(this), true);
    });

    //load more comment
    $(".report-comment-wraper.trip-place-comments-wrapper").off('scroll');
    $(".report-comment-wraper.trip-place-comments-wrapper").on( 'scroll', function() {
        var loadingCount = $(this).parent().find('.post-comment-top-info span.loading-count strong').html();
        var allCount = $(this).parent().find('.post-comment-top-info span.all-comments-count').html();

        if(loadingCount === allCount){
            $(this).find('.loadMore2').hide();
        }

        if($(this).find('#hide_loader_2')[0] && visibleY($(this).find('#hide_loader_2')[0])) {
            loadMoreComment(2, $(this));
        }
    });

    $('body').off('click', 'div.trip-place-reaction i.like');

    $('body').on('click', 'div.trip-place-reaction i.like', function (e) {
        var _this = $(this);
        var placeId = $(this).parents('.trip-place-wrap').data('placeid');

        $.ajax({
            method: "POST",
            url: "{{ route('trip_places.likeunlike') }}",
            data: {place_id: placeId}
        })
            .done(function (res) {
                var result = res.data;
                if (result.status == 'yes') {
                    _this.removeClass('trav-heart-icon');
                    _this.addClass('trav-heart-fill-icon');
                } else if (result.status == 'no') {
                    _this.addClass('trav-heart-icon');
                    _this.removeClass('trav-heart-fill-icon');
                }

                _this.parents('.trip-place-reaction').find('span.trip-place-like-count b').html(result.count);
                updateMobileReactions();
            });
        e.preventDefault();
    });

    if ($('.btn-add_place').length === 1 && !add) {
        $('.btn-add_place').trigger('click');
        uploader.unbindAll();
        editUploader.unbindAll();
        suggestUploader.unbindAll();
        uploader.destroy();
        editUploader.destroy();
        suggestUploader.destroy();
        initUploaders();
    }

    if (add && addType == 'place') {
        $('.btn-add_place:first').trigger('click');

        var modal = $('#modal-add_place');

        $(modal).find('input#places_id').val(addData.title);
        $(modal).find('input#cities_id').val(addData.city_title);
        $(modal).find('input#places_real_id').val(addData.id);
        $(modal).find('input#cities_real_id').val(addData.city_id);
        $(modal).find('div.input-group-img.place').css('background-image', 'url(' + addData.image + ')');
        $(modal).find('div.input-group-img.place').css('background-size', 'cover');

        $(modal).find('div.input-group-img.city').css('background-image', 'url(' + addData.city_img + ')');
        $(modal).find('div.input-group-img.city').css('background-size', 'cover');

        $(modal).find('div.place.disabled').removeClass('disabled');
        $(modal).find('input#places_id').removeClass('disabled');
        $(modal).find('input#places_id').prop('disabled', false);

        var lnglat = [addData.lng, addData.lat];

        $('div.map-placeholder-block').hide();

        var el = $(document.createElement('div'));
        var style = 'background: url(\' ' + addData.image + '\');' +
            'background-size: cover;' +
            'background-repeat: no-repeat;' +
            'background-position: center center;' +
            'width: 50px;' +
            'height: 50px;' +
            'border-radius: 50px;' +
            'border: 2px solid #fff !important;' +
            'cursor: pointer;';

        $(el).addClass('place-marker marker');
        $(el).attr('style', style);

        var marker = new mapboxgl.Marker($(el)[0])
            .setLngLat(lnglat)
            .addTo(window.plan_map);

        searchMarkers.push(marker);

        map.flyTo({
            center: lnglat,
            essential: true,
            speed: 0.7,
            curve: 1,
            zoom: 15
        });

        var source = window.plan_map.getSource('route');

        if (source) {
            coordinates = source._options.data.geometry.coordinates;
            savedCoords = coordinates;
            tempCoords = lnglat;
        }
    }

    if (add && addType == 'city') {
        $('.btn-add_place:first').trigger('click');

        var modal = $('#modal-add_place');

        $(modal).find('input#cities_id').val(addData.title + ', ' + addData.country);
        $(modal).find('input#cities_real_id').val(addData.id);

        $(modal).find('div.place.disabled').removeClass('disabled');
        $(modal).find('input#places_id').removeClass('disabled');
        $(modal).find('input#places_id').prop('disabled', false);

        var lnglat = [addData.lng, addData.lat];

        $('div.map-placeholder-block').hide();

        window.selected_city_id = addData.id;
        window.selected_city_title = addData.title;

        selected_city_name = ', ' + addData.title;

        map.flyTo({
            center: lnglat,
            essential: true,
            speed: 0.7,
            curve: 1,
            zoom: 12
        });

        var source = window.plan_map.getSource('route');

        if (source) {
            coordinates = source._options.data.geometry.coordinates;
            savedCoords = coordinates;
            tempCoords = lnglat;
        }
    }

    if (add && addType == 'country') {
        $('.btn-add_place:first').trigger('click');

        var modal = $('#modal-add_place');

        $(modal).find('input#cities_id').val(addData.title);
        $(modal).find('input#cities_id').trigger('keyup');
    }

    if (add) {
        var url = removeParam('add', window.location.href);
        url = removeParam('add-type', url);

        window.history.pushState({}, document.title, url);

        add = undefined;
        addType = undefined;
    }

    var distance = parseInt('{{$distance}}');
    var budget = parseInt('{{$budget}}');
    var destinations = parseInt('{{$destinations}}');
    var duration = parseInt('{{$duration}}');

    $('div.card-trip_data.distance div.card-body').each(function() {
        $(this).find('p:first').text(distance + ' km')
    });

    $('div.card-trip_data.budget div.card-body').each(function() {
        $(this).find('p:first').text('$' + budget)
    });

    $('div.card-trip_data.destinations div.card-body').each(function() {
        $(this).find('p:first').text(destinations)
    });

    $('div.card-trip_data.duration div.card-body').each(function() {
        $(this).find('p:first').text(duration + ' Days')
    });

    function removeParam(key, sourceURL) {
        var rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        return rtn;
    }

    updateSharePlanPopup();

    if (!trip_places_arr.length) {
        $('button#btnPublishTrip').attr('disabled', 'disabled');
    } else {
        $('button#btnPublishTrip').removeAttr('disabled');
    }

    var approvedSuggestions = [];

    @if (isset($approved_suggestions))
        approvedSuggestions = <?= json_encode($approved_suggestions);?>;
    @endif

    $('div#modal-leave_popup div.changes').html('');

    if (!approvedSuggestions.length) {
        $('div#modal-leave_popup div.changes').remove();
        $('div#modal-leave_popup div.description').remove();
    } else {
        approvedSuggestions.forEach(function(suggestion, index) {
            if (!suggestion.suggested_place) {
                return;
            }

            var time = '';

            if (!suggestion.suggested_place.without_time) {
                var date = new Date(suggestion.suggested_place.time);
                time = '<span>@' + moment(date).format('HH:mma') + '</span>';
            }

            var budget = '$0';

            if (suggestion.suggested_place.budget) {
                budget = '$' + suggestion.suggested_place.budget;
            }

            var duration = '0 Minutes';

            if (suggestion.suggested_place.duration) {
                duration = renderDuration(suggestion.suggested_place.duration);
            }

            var spend = 'Will spend';
            var stay = 'Will stay';

            if (moment(suggestion.suggested_place.time) <= moment()) {
                spend = 'Spent';
                stay = 'Stayed';
            }

            var details = '';

            if (suggestion.suggested_place.budget > 0) {
                details += '<div class="budget">' + spend + ' <span>' + budget + '</span></div>';
            }

            if (suggestion.suggested_place.duration > 0) {
                details += '<div class="duration">' + spend + ' <span>' + duration + '</span></div>';
            }

            $('div#modal-leave_popup div.changes').append('<div class="change">\n' +
                '                            <div class="img">\n' +
                '                                <img src="' + suggestion.place_img + '" alt="" />\n' +
                '                            </div>\n' +
                '                            <div class="info">\n' +
                '                                <div class="title text-overflow-prevent">' + suggestion.name + time + '</div>\n' +
                '                                <div class="details">' + details + '</div>\n' +
                '                            </div>\n' +

                                                `<div class="info info-mobile">
                                                    <div class="title text-overflow-prevent">${suggestion.name}</div>
                                                    <div class="location text-overflow-prevent">
                                                        <span class="location-label">POI</span>
                                                        <span class="text-overflow-prevent">City, Country</span>
                                                    </div>
                                                    <div class="details">
                                                        <div class="details-item budget"><span class="text-overflow-prevent">${budget}</span></div>
                                                        <div class="details-item duration"><span class="text-overflow-prevent">${duration}</span></div>
                                                        <div class="details-item time"><span class="text-overflow-prevent">${time}</span></div>
                                                    </div>
                                                </div>` +
                '                        </div>')
        });

        $('div#modal-leave_popup span.changes-count').html(approvedSuggestions.length + ' changes');
    }

    function renderDuration(duration) {
        if(duration > (60 * 24)) {
            var days = Math.trunc(duration / (60 * 24));
        }
        else {
            var days = 0;
        }

        duration = duration - days * (60 * 24);

        if(duration > 60) {
            var hours = Math.trunc(duration / 60);
        }
        else {
            var hours = 0;
        }

        var minutes = duration - (hours*60);

        var result = '';

        if (days > 0) {
            result += days + ' Days';
        }

        if (hours > 0) {
            if (days > 0) {
                result += ' ';
            }

            result +=  hours + ' Hours';
        }

        if (minutes > 0) {
            if (days > 0 || hours > 0) {
                result += ' ';
            }

            result += minutes + ' Minutes';
        }

        return result;
    }

    var send_suggestions_available = '{{$send_suggestions_available}}';

    if (send_suggestions_available) {
        $('button.send-suggestions').removeAttr('disabled');
    } else {
        $('button.send-suggestions').attr('disabled', 'disabled');
    }

    $(document).ready(function() {
        setTimeout(function() {
            var step = location.href.split('#step-').pop();

            if (step && !step.includes('http')) {
                scrollSideBar(step);
                moveToMarker(step);
            }
        }, 1000);

        renderPlaceMarkers(true);
        setRouteSources();

        @if(isset($do) && $do=='edit' && ($is_admin || $is_editor))
            @if ($is_undo)
                addUndoButton();
            @else
                $('button.btn-undo').remove();
            @endif
        @endif
    });

</script>

<script
        type="text/javascript"
        async defer
        src="//assets.pinterest.com/js/pinit.js"
></script>

<script>
    function updateMobileReactions()
    {
        var overallLikes = 0;
        var overallComments = 0;

        $('.trip-place-reactions').each(function(e) {
            var likesCount = $(this).find('.trip-place-reaction span b').first().text();
            var commentsCount = $(this).find('.trip-place-reaction span b').last().text();

            var savedLikes = $(this).parents('.trip-place').find('div.reactions a i').first();
            var savedComments = $(this).parents('.trip-place').find('div.reactions a i').last();
            $(this).parents('.trip-place').find('div.reactions a').first().html('');
            $(this).parents('.trip-place').find('div.reactions a').last().html('');

            $(this).parents('.trip-place').find('div.reactions a').first().append(savedLikes);
            $(this).parents('.trip-place').find('div.reactions a').last().append(savedComments);

            $(this).parents('.trip-place').find('div.reactions a i').first().after(' ' + likesCount);
            $(this).parents('.trip-place').find('div.reactions a i').last().after(' ' + commentsCount);

            $(this).parents('.trip-place').find('div.reactions a i').first().attr('class', $(this).find('.trip-place-reaction a i').first().attr('class'));
            $(this).parents('.trip-place').find('div.reactions a i').last().attr('class', $(this).find('.trip-place-reaction a i').last().attr('class'));

            overallLikes += parseInt(likesCount);
            overallComments += parseInt(commentsCount);
        });

        $('div#modal-mobile_side_menu div.reactions a.likes-count span').text(overallLikes);
        $('div#modal-mobile_side_menu div.reactions a.comments-count span').text(overallComments);
    }

    $(document).ready(function() {
        updateMobileReactions();
        $(".btn-group-toggle-days").scroll(function() {
            if($(this).scrollLeft() > 0) {
                $(this).siblings('.group-toggle-title').addClass('show-fader')
            } else {
                $(this).siblings('.group-toggle-title').removeClass('show-fader')
            }
        });

        $(document).off('click', '.mobile-map-view-controls button');
        $(document).on('click', '.mobile-map-view-controls button', function (e) {
            e.preventDefault();
            $('.mapboxgl-popup').remove();
            $(this).siblings('button').removeClass('active');
            $(this).addClass('active');

            if($(this).hasClass('map-view')) {
                $('.page-planner .sidebar').hide();
            } else {
                $('.page-planner .sidebar').show();
                $('.page-planner .sidebar').removeClass('mobile-hide');
                moveSlider($('.swiper-slide-active').attr('data-date'));
                $('.map-search .input-group').removeClass("active");
            }
        });

        $(document).off('click', '.modal-place .select-from-map');
        $(document).on('click', '.modal-place .select-from-map', function (e) {
            e.preventDefault();
            $('.mobile-map-view-controls .map-view').trigger("click");
            $('.map-search .input-group').addClass("active");


        });

        $(document).off('click', '.map-search .input-group-prepend');
        $(document).on('click', '.map-search .input-group-prepend', function (e) {
            $('.map-search .input-group').addClass("active");
        });

        $(document).off('click', '.plan-chats .title');
        $(document).on('click', '.plan-chats .title', function (e) {
            if($('.modal-backdrop.chat-bg').length) {
                if($(this).hasClass('active')) {
                    if($('.page-map .places-chats.window.opened') || $('.page-map .all-plan-chats.window.opened')) {
                        $('.modal-backdrop.chat-bg').remove();
                        $('.plan-chats').removeClass('active');
                    }
                }
            } else {
                $('body').append("<div class='modal-backdrop chat-bg show'></div>");
                $('.plan-chats').addClass('active');
            }
        });

        $(document).off('click', '.mobile-trip-view-mode .actions button');
        $(document).on('click', '.mobile-trip-view-mode .actions button', function (e) {
            e.preventDefault();
            $('.mobile-trip-view-mode').hide();
            $('.mobile-map-view-controls .story-view').trigger("click");
            // if($(this).hasClass('view-plan')) {
            //     $('.mobile-map-view-controls .map-view').trigger("click");
            // } else {
            //     $('.mobile-map-view-controls .story-view').trigger("click");
            // }
        });

        var unreadLogsCount = parseInt($('div.logs_unread').text());

        if (unreadLogsCount !== 0 && !isNaN(unreadLogsCount)) {
            $('button.side-menu-btn span').text(unreadLogsCount);
            $('button.side-menu-btn span').show();

            $('div#modal-mobile_side_menu button.activity-logs span').text(unreadLogsCount);
            $('div#modal-mobile_side_menu button.activity-logs span').show();
        }

        $('.post-image-list').html('');

        $('div.thumbnail').each(function(i, e) {
            if (i > 4) {
                return;
            }

            var src = $(e).find('img').attr('src');

            var loadMore = '';
            var countLeft = $('div.thumbnail').length - 5;
            if (i === 4 && countLeft > 0) {
                loadMore = '<a class="more-photos" href="#" data-lightbox="media__post199366" style="cursor: default;pointer-events: none;text-decoration: none;">+' + countLeft + '</a>';
            }

            $('.post-image-list').append('<li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">\n' + loadMore +
                '                                                        <img data-cfsrc="' + src + '" alt="" data-cfstyle="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" src="' + src + '">\n' +
                '                                                </li>');

        })

        $('div#modal-mobile_side_menu div.flags').html('');
        $('div#modal-final_step div.flags').html('');

        var flags = [];

        $('div.trip-place-country img').each(function(i, e) {
            var src = $(e).attr('src');

            if (flags[src] !== undefined) {
                return;
            }

            flags[src] = 1;

            if (Object.keys(flags).length > 8) {
                return;
            }

            $('div#modal-mobile_side_menu div.flags').append('<img src="' + src + '">');
            $('div#modal-final_step div.flags').append('<img src="' + src + '">');
        });

        if (Object.keys(flags).length > 8) {
            $('div#modal-mobile_side_menu div.flags').append('<span class="flags-add-count">+ ' + (Object.keys(flags).length - 8) + '</span>');
            $('div#modal-final_step div.flags').append('<span class="flags-add-count">+ ' + (Object.keys(flags).length - 8) + '</span>');
        }

        updatePositions();
    })
</script>