<div class="modal fade vertical-center" backdrop="false" id="modal-affiliate_hint" tabindex="-1" role="dialog" aria-labelledby="Affiliate Tip" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 509px; display: block;">
        <div class="modal-content">
            <div class="modal-header">
                <div class="title">
                    Affiliate Program
                </div>
            </div>
            <div class="modal-body">
                <div class="affiliate-hint">
                    <img class="" src="{{asset('assets2/image/affiliate-hint.png')}}" alt="">
                    <div class="desc">
                        As an expert, when you add place to this trip plan you will automatically receive an affiliate commission when someone booked in that place.
                    </div>
                    <div class="actions">
                        <div class="ok">
                            OK, GOT IT!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>