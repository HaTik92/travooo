<div class="modal modal-sidebar logs" backdrop="false" id="modal-activity_logs" role="dialog" aria-labelledby="Activity Logs" style="z-index: 1052 !important;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
{{--                <button type="button" class="filter"><i class="trav-filter-icon"></i></button>--}}
                <h5 class="modal-title">Activity Log</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="trav-angle-left"></i></button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>