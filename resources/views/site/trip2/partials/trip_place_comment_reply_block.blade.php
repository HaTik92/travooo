<div class="post-comment-row doublecomment post-comment-reply commentRow{{$child->id}}" data-parent_id="{{$child->reply_to}}" >
    <div class="post-com-avatar-wrap">
        <img src="{{check_profile_picture($child->user->profile_picture)}}" alt="">
    </div>
    <div class="post-comment-text">
        <div class="post-com-name-layer">
            <a href="{{route('profile.show', ['id' => $child->user_id])}}" class="comment-name">{{$child->user->name}}</a>
            {!! get_exp_icon($child->user) !!}
            <div class="post-com-top-action">
                <div class="dropdown">
                    <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Mediacomment">
                        @if($child->user->id==$authUserId)
                        <a href="#" class="dropdown-item report-edit-comment" data-id="{{$child->id}}" data-report="{{$tp->id}}">
                            <span class="icon-wrap">
                                <i class="trav-pencil" aria-hidden="true"></i>
                            </span>
                            <div class="drop-txt comment-edit__drop-text">
                                <p><b>@lang('profile.edit')</b></p>
                            </div>
                        </a>

                        <a href="javascript:;" class="dropdown-item report-comment-delete" id="{{$child->id}}" data-parent="{{$child->reply_to}}" data-del-type="2">
                            <span class="icon-wrap rep-delete-icon">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </span>
                            <div class="drop-txt comment-delete__drop-text">
                                <p><b>Delete</b></p>
                            </div>
                        </a>
                        @endif
                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$child->id}},this)">
                            <span class="icon-wrap">
                                <i class="trav-flag-icon-o"></i>
                            </span>
                            <div class="drop-txt comment-report__drop-text">
                                <p><b>@lang('profile.report')</b></p>
                            </div>
                        </a>

                    </div>
                </div>
            </div>
        </div>
        <div class="comment-txt comment-text-{{$child->id}}">
            @php
                $showCommentChar = 100;
                $ellipsestext = "...";
                $commentmoretext = "see more";
                $commentlesstext = "see less";

                $comment_content = $child->comment;
                $convert_content = strip_tags($comment_content);

                if(mb_strlen($convert_content, 'UTF-8') > $showCommentChar) {
                 
                    $l = mb_substr($comment_content, 0, $showCommentChar, 'UTF-8');

                    $html = '<span class="less-content">'.$l . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span> <a href="javascript:;" class="read-more-link">' . $commentmoretext . '</a></span>  <span class="more-content" style="display:none">' . $comment_content . ' &nbsp;&nbsp;<a href="javascript:;" class="read-less-link">' . $commentlesstext . '</a></span>';

                    $comment_content = $html;
                }
            @endphp
            <p>{!!$comment_content!!}</p>
            <form class="commentEditForm{{$child->id}} comment-edit-form d-none" data-id="{{$tp->id}}"  method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
                {{ csrf_field() }}<input type="hidden" data-id="pair{{$child->id}}" name="pair" value="{{uniqid()}}"/>
                <div class="post-create-block reports-comment-block post-edit-block" tabindex="0">
                    <div class="post-create-input report-create-input w-490">
                        <textarea name="text" data-id="text{{$child->id}}" class="textarea-customize report-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="@lang('comment.write_a_comment')">{!!$child->comment!!}</textarea>
                    </div>
                    <div class="post-create-controls d-none">
                        <div class="post-alloptions">
                            <ul class="create-link-list">
                                <li class="post-options">
                                    <input type="file" name="file[]" class="commenteditfile commenteditfile{{$child->id}}" data-id="commenteditfile{{$child->id}}"  style="display:none" multiple>
                                    <i class="fa fa-camera click-target" data-target="commenteditfile{{$child->id}}"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="comment-edit-action">
                            <a href="javascript:;" class="edit-cancel-link"  data-comment_id="{{$child->id}}">Cancel</a>
                            <a href="javascript:;" class="edit-report-comment-link" data-comment_id="{{$child->id}}">Post</a>
                        </div>
                    </div>
                    <div class="medias report-media">
                          @if(is_object($child->medias))

                              @foreach($child->medias AS $media)
                                  @php
                                  $file_url = $media->media->url;
                                  $file_url_array = explode(".", $file_url);
                                  $ext = end($file_url_array);
                                  $allowed_video = array('mp4');
                                  @endphp
                                  <div class="img-wrap-newsfeed">
                                      <div>
                                          @if(in_array($ext, $allowed_video))
                                          <video style="object-fit: cover" class="thumb" controls>
                                              <source src="{{$file_url}}" type="video/mp4">
                                          </video>
                                      @else
                                      <img class="thumb" src="{{$file_url}}" alt="" >
                                      @endif
                                      </div>
                                      <span class="close remove-media-comment-file" data-media_id="{{$media->media->id}}">
                                          <span>×</span>
                                      </span></div>
                              @endforeach
                          @endif
                    </div>
                </div>
                <input type="hidden" name="trip_place_id" value="{{$tp->id}}"/>
                <input type="hidden" name="comment_id" value="{{$child->id}}">
                 <input type="hidden" name="comment_type" value="2">
                <button type="submit"  class="d-none"></button>
            </form>
        </div>
        <div class="post-image-container">
            @if(is_object($child->medias))
                @php
                    $index = 0;

                @endphp
                @foreach($child->medias AS $media)
                    @php
                        $index++;
                        $file_url = $media->media->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                    @endphp
                    @if($index % 2 == 1)
                    <ul class="post-image-list" style=" display: flex;margin-bottom: 20px;">
                    @endif
                        <li style="overflow: hidden;margin:1px;">
                            @if(in_array($ext, $allowed_video))
                            <video style="object-fit: cover" width="192" height="210" controls>
                                <source src="{{$file_url}}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            @else
                            <a href="{{$file_url}}" data-lightbox="comment__replymedia{{$child->id}}">
                                <img src="{{$file_url}}" alt="" style="width:192px;height:210px;object-fit: cover;">
                            </a>
                            @endif
                        </li>
                    @if($index % 2 == 0)
                    </ul>
                    @endif
                @endforeach
            @endif

        </div>
        <div class="comment-bottom-info">
            <div class="comment-info-content">
                <div class="dropdown">
                    <a href="#" class="report_comment_like like-link" id="{{$child->id}}"><i class="fa fa-heart {{($child->likes()->where('user_id', $authUserId)->first())?'fill':''}}" aria-hidden="true"></i> <span class="{{$child->id}}-comment-like-count">{{count($child->likes)}}</span></a>
                    @if(count($child->likes))
                    <div class="dropdown-content comment-likes-block report-comment-likes-block" data-id="{{$child->id}}">
                        @foreach($child->likes()->orderBy('created_at', 'DESC')->take(7)->get() as $like)
                        <span>{{$like->author->name}}</span>
                        @endforeach
                        @if(count($child->likes)>7)
                        <span>...</span>
                        @endif
                    </div>
                    @endif
                </div>
                <span class="com-time"><span class="comment-dot report-com-dot"> · </span>{{diffForHumans($child->created_at)}}</span>
            </div>
        </div>
    </div>
</div>
