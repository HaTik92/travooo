<div class="modal fade vertical-center" id="modal-skyscanner" tabindex="-1" role="dialog" aria-labelledby="Cancel Edit" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
    <div class="modal-dialog" role="document" style="width: 400px; display: block;">
        <div class="modal-content">
            <div class="modal-body">
                <div class="user-list" style="border: none; box-shadow: none;">
                </div>
            </div>
        </div>
    </div>
</div>