<div class="modal fade plan-popup vertical-center" id="modal-invite" tabindex="-1" role="dialog" aria-labelledby="Edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form>
            <div class="modal-content">
                <div class="modal-header">
                    <div class="title">
                        <i class="trav-user-plus-icon"></i>
                        <h5 class="modal-title">Invite people</h5>
                        <input type="hidden" name="trip_id" id="trip_id" value="{{$trip->id}}" />
                    </div>
                    <img class="invite-icon-cross" alt="" src="{{asset('assets2/image/plans/invite-top-icon-cross.png')}}" data-dismiss="modal" aria-label="Close"/>
                </div>
                <div class="modal-inner-trip_info">
                    <span class="bold">
                        Invite other users to your trip plan for collaboration
                    </span>
                </div>
                <div class="modal-body invite">
                    <div class="search-block">
                        <div class="search">
                            <input placeholder="Search by…">
                            <div class="tabs">
                                <div data-type="experts" class="experts tab active">Experts <span>0</span></div>
                                <div data-type="locals" class="locals tab">Locals <span>0</span></div>
                                <div data-type="friends" class="friends tab">Friends <span>0</span></div>
                                <div data-type="followers" class="followers tab">Followers <span>0</span></div>
                            <!--<div data-type="email" class="emails tab" style="align-items: center; display: flex;">
                                    <img class="invite-email-icon-active" alt="" src="{{asset('assets2/image/plans/invite-email-icon-active.png')}}" /><img class="invite-email-icon" alt="" src="{{asset('assets2/image/plans/invite-email-icon.png')}}" />Email
                                </div>-->
                            </div>
                            <div class="styles-checkins">
                                Match your Travel Styles?
                                <input type="checkbox" name="with_styles" value="1" />
                            </div>
                        </div>
                        <div class="search-results">
                            <div class="spinner-border rep-spinner-border" role="status" style="position: absolute; right: 305px; top: 250px;"><span class="sr-only">Loading...</span></div>
                            <div class="result-rows">
                            </div>
                        </div>
                        <div class="email-invite">
                            <div class="desc">
                                An email will be sent to your friend with a link to signup to Travooo.
                                Once your friend joins he can start planning with you.
                            </div>
                            <div class="actions">
                                <div class="email-input">
                                    <input type="email" name="email" placeholder="Email address…">
                                </div>
                                <div class="role">
                                    <select name="" id="">
                                        <option selected value="">Admin</option>
                                        <option value="">Editor</option>
                                        <option value="">Viewer</option>
                                    </select>
                                </div>
                                <div class="action send">Send</div>
                            </div>
                        </div>
                    </div>
                    <div class="role-block pending">
                        <h1>Pending</h1>
                        <div class="spinner-border rep-spinner-border" role="status"><span class="sr-only">Loading...</span></div>
                    </div>

                    <div class="role-block admin">
                        <h1>Admins</h1>
                        <div class="spinner-border rep-spinner-border" role="status"><span class="sr-only">Loading...</span></div>
                    </div>

                    <div class="role-block editor">
                        <h1>Editors</h1>
                        <div class="spinner-border rep-spinner-border" role="status"><span class="sr-only">Loading...</span></div>
                    </div>

                    <div class="role-block viewer">
                        <h1>Viewers</h1>
                        <div class="spinner-border rep-spinner-border" role="status"><span class="sr-only">Loading...</span></div>
                    </div>

                    <div class="role-block travel-mate">
                        <h1>Travel Mates</h1>
                        <div class="spinner-border rep-spinner-border" role="status"><span class="sr-only">Loading...</span></div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>