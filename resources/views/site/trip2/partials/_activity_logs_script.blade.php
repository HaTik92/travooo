<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
    var authId = "{{auth()->id()}}";
    var currentSession = false;
    var editable = parseInt('{{$editable}}');

    function renderLog(date, currentValue, forNotification = false)
    {
        var newNot = '';

        if (currentValue.unread && currentValue.user_id != authId) {
            newNot = '<span class="new" style="display: inline-block; height: 10px; width: 10px; border-radius: 5px; background: #fc225a; margin-right: 5px;"></span>';
        }

        var entity = '';
        var action = '<button data-id="' + currentValue.entity.id + '" class="logs-place"><i style="" class="fa fa-eye"></i></button>';

        switch(currentValue.type) {
            case 0:
            case 1:
            case 10:
            case 11:
            case 12:
                currentValue.entity = {};
            case 13:
            case 14:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
                entity = currentValue.entity.name;
                action = '<button></button>';
                break;
            case 4:
                entity = currentValue.entity.name + ' in ' + currentValue.entity.country_name;
                action = '<button></button>';
                break;
            default:
                var link = 'href="/places/' + currentValue.entity.place_id + '"';

                if (forNotification) {
                    link = '';
                }

                entity = '<a target="_blank" ' + link + '>' + currentValue.entity.name + '</a> in ' + currentValue.entity.country_name;
                break;

        }

        var user = '';

        if (currentValue.user) {
            if (currentValue.user.id == authId) {
                user = '<span class="activity-log-name">You</span>';
            } else {
                var link = 'href="/profile/' + currentValue.user.id + '"';

                if (forNotification) {
                    link = '';
                }

                user = '<a class="activity-log-name text-overflow-prevent" ' + link + '>' + currentValue.user.name + '</a>';
                if (currentValue.user.role) {
                    user += '<span class="activity-log-name"> [' + currentValue.user.role + ']</span>';
                }
            }
        }
        var entityLine = '';
        if (currentValue.entity.place_image) {
            entityLine = '<span class="activity-log-entity">' + '<img src="' + currentValue.entity.place_image + '"><span class="entity-place">' + entity + '</span></span>'
        }

        if (!forNotification) {
            return '<div class="logs-row" data-id="'+ currentValue.id + '">' +
                '<div>' +
                    '<span class="activity-log-type">'
                        + newNot + user + ' ' + currentValue.mapped_type +
                    '</span>' +
                    entityLine +
                    '<span class="activity-log-time">' + date + '</span>' +
                '</div>' +
                '<div>' +
                    '<span class="activity-log-action">' + action + '</span>' +
                '</div>' +
            '</div>';
        }

        return user + ' ' + currentValue.mapped_type + ' ' + entity;
    }

    var logsRequest = null;

    $(document).ready(function () {
        $(document).on('click', 'button.activity-logs', function () {
            var planId = '{{$trip->getKey()}}';

            if (logsRequest) {
                logsRequest.abort();
            }

            logsRequest = $.ajax({
                method: "GET",
                url: "{{url('trip/activity-logs')}}",
                data: {"plan_id": planId}
            })
                .done(function (res) {
                    $('div#modal-activity_logs div.modal-body').html('');
                    $('div#modal-activity_logs div.modal-body').css('overflow', 'scroll');

                    $('div#modal-activity_logs').modal('show');
                    $('div.modal-backdrop').hide();
                    $('div.no-activity').remove();

                    for (date in res.data.logs) {
                        res.data.logs[date].forEach(function (currentValue, index) {
                            $('div#modal-activity_logs div.modal-body').append(renderLog(date, currentValue));
                        });
                    }

                    if (!res.data.logs) {
                        $('div#modal-activity_logs div.modal-body').append('<div class="no-activity" style="padding: 10px 20px;"><h6 style="margin: 0;">No activity yet...</h6></div>');
                    }

                    $('button.side-menu-btn span').hide();
                    $('div#modal-mobile_side_menu button.activity-logs span').hide();
                });
        });


        $(document).on('click', 'button.logs-place', function () {
            var placeId = $(this).data('id');
            scrollToPlace(placeId);
            $('#modal-mobile_side_menu').modal('hide');
        });

        $(document).on('click', 'div#modal-activity_logs button.close', function () {
            updatePlanContents();
        });

        function scrollToPlace(placeId) {
            openPlanContents();
            setTimeout(function() {
                scrollSideBar(placeId)
            }, 500);
        }

        function openPlanContents()
        {
            $('div#modal-activity_logs').modal('toggle');
            $('.mobile-map-view-controls button.story-view').trigger('click');
        }

        var tripId = "{{$trip_id}}";

        window.Echo.channel('test-channel').listen('.test-test', function (e) {
            console.log(`test-channel => ', '.test-test =>  ${e}`);
        });

        window.Echo.private('new-test-channel.'+authId).listen('.new-test-test', function (e) {
            console.log(`new-test-channel.${authId} => ', '.new-test-test => ${e}`);
        });

        window.Echo.private('plan-logs-channel.' + authId).listen('.App\\Events\\PlanLogs\\PlanLogAddedEvent', function (e) {
            autoSave();

            console.log('plan-logs-channel.{user_id} => ', '.App\\Events\\PlanLogs\\PlanLogAddedEvent => ', e);

            if (e.hard_refresh) {
                location.reload();
            }

            if (!editable) {
                return;
            }

            var log = e.log;

            if (log.trips_id == tripId && log.entity.id) {
                toastr.options = {
                    onclick: function () {
                        $('button.activity-logs').trigger('click');
                    },
                    timeOut: 3000,
                    extendedTimeOut: 0,
                    positionClass: "toast-bottom-right",
                };

                if (authId != log.user_id) {
                    toastr.info(renderLog(e.time, log, true));
                }

                $('div.toast.toast-info:nth-child(n+5)').remove();

                updatePlanData();

                if ([{{\App\Models\TripPlans\PlanActivityLog::TYPE_REJECT_INVITATION}}, {{\App\Models\TripPlans\PlanActivityLog::TYPE_INVITED_TO_PLAN}}, {{\App\Models\TripPlans\PlanActivityLog::TYPE_REMOVED_FROM_PLAN}}, {{\App\Models\TripPlans\PlanActivityLog::TYPE_JOINED_THE_PLAN}}, {{\App\Models\TripPlans\PlanActivityLog::TYPE_LEFT_THE_PLAN}}].includes(log.type)) {
                    renderInvited();
                    search();
                }

                updatePlanContents();

                $('div#modal-activity_logs div.modal-body div.logs-row:first').before(renderLog(e.time, log));

                if ($('button.edit-trip').length) {
                    if ($('div.unsaved.edit-trip').length) {
                        $('div.unsaved.edit-trip').text(parseInt($('div.unsaved.edit-trip').text()) + 1);
                    } else {
                        $('button.edit-trip').after('<div class="unsaved edit-trip" style="color: #fff;\n' +
                            '    position: relative;\n' +
                            '    right: 12px;\n' +
                            '    bottom: 16px;\n' +
                            '    font-size: 14px;\n' +
                            '    background: #ff5a79;\n' +
                            '    width: 20px;\n' +
                            '    height: 20px;\n' +
                            '    text-align: center;\n' +
                            '    border-radius: 20px;\n' +
                            '    border: 2px solid #fff;">1</div>');
                    }
                }

                if ($('div#modal-activity_logs').is(':visible')) {
                    $('button.activity-logs').trigger('click');
                }

                autoSave(true);
            }
        });

        window.Echo.private('plan-logs-channel.' + authId).listen('.App\\Events\\PlanLogs\\PlanLogRemovedEvent', function (e) {
            autoSave();
            var log = e.log;
            console.log('plan-logs-channel => ', 'App\\Events\\PlanLogs\\PlanLogRemovedEvent => ', e);

            if (log.trips_id == tripId) {
                updatePlanData();
                updatePlanContents();
                $('div#modal-activity_logs div.modal-body div.logs-row[data-id=' + log.id + ']').remove();

                autoSave(true);
            }

            if ($('button.edit-trip').length) {
                if ($('div.unsaved.edit-trip').length) {
                    $('div.unsaved.edit-trip').text(parseInt($('div.unsaved.edit-trip').text()) - 1);

                    if (parseInt($('div.unsaved.edit-trip').text()) == 0) {
                        $('div.unsaved.edit-trip').remove();
                    }
                }
            }
        });

        window.Echo.private('plan-logs-channel.' + authId).listen('.App\\Events\\PlanLogs\\PlanLogReadEvent', function (e) {
            updatePlanContents();
        });

        @if((isset($is_invited) && !$is_invited && !$is_admin) || !$editable)
            window.Echo.channel('plan-logs-public-channel').listen('.App\\Events\\PlanLogs\\PlanPublicEvent', function (e) {
                var log = e.log;
                console.log('plan-logs-public-channel=> ', 'App\\Events\\PlanLogs\\PlanPublicEvent => ', e);

                if (log.trips_id == tripId && log.entity.id) {
                    toastr.options = {
                        onclick: function () {
                            $('button.activity-logs').trigger('click');
                        },
                        timeOut: 300,
                        extendedTimeOut: 0,
                        positionClass: "toast-bottom-right",
                    };

                    if (authId != log.user_id) {
                        toastr.info(renderLog(e.time, log, true));
                    }

                    $('div.toast.toast-info:nth-child(n+5)').remove();

                    updatePlanData();
                    updatePlanContents();
                }
            });
        @endif

        window.Echo.channel('plan-media-comment-added').listen('.App\\Events\\PlanMedia\\TripMediaCommentAddedEvent', function (e) {
            var comment = e.comment;
            var commentView = e.comment_view;
            var mediaId = e.comment.medias_id;

            console.log('plan-media-comment-added => ', 'App\\Events\\PlanMedia\\TripMediaCommentAddedEvent => ', e);

            if (!comment || !mediaId || !commentView) {
                return;
            }

            setTimeout(function () {
                var parentSelector = 'div.photo-comment-' + mediaId;

                if ($(parentSelector).find('.commentRow' + comment.id).length) {
                    return;
                }

                var all_comment_count = $(parentSelector).find('.'+ mediaId +'-all-comments-count').html();
                var loading_count =  $(parentSelector).find('.'+ mediaId +'-loading-count strong').html();

                $(parentSelector).find("#" + mediaId + ".comment-not-fund-info").remove();
                $(parentSelector).find("#" + mediaId + ".post-comment-top-info").show();
                $(parentSelector).find('.'+ mediaId +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                $(parentSelector).find('div.reaction-comments span b').html(parseInt(all_comment_count) + 1);
                $(parentSelector).find('.'+ mediaId +'-loading-count strong').html((parseInt(loading_count) + 1));

                if (all_comment_count == 0){
                    $(parentSelector).parents('.gallery-comment-wrap').find("#" + mediaId + ".report-comment-wraper").append('<div class="report-comment-main">'+commentView+'</div>').fadeIn('slow');
                } else {
                    $(parentSelector).parents('.gallery-comment-wrap').find(".report-comment-main").prepend(commentView).fadeIn('slow');
                }
            }, 500);
        });


        window.Echo.channel('plan-trip-place-comment-added').listen('.App\\Events\\Plan\\TripPlaceCommentAddedEvent', function (e) {
            var comment = e.comment;
            var commentView = e.comment_view;
            var tripPlaceId = e.trip_place_id;


            console.log('plan-media-comment-added => ', 'App\\Events\\PlanMedia\\TripPlaceCommentAddedEvent => ', e);

            if (!comment || !tripPlaceId || !commentView) {
                return;
            }

            setTimeout(function () {
                var parentSelector = '.trip-place-wrap[data-placeid="' + tripPlaceId + '"]';

                if ($(parentSelector).find('.commentRow' + comment.id).length) {
                    return;
                }

                var all_comment_count = $(parentSelector).find('.'+ tripPlaceId +'-all-comments-count').html();
                var loading_count =  $(parentSelector).find('.'+ tripPlaceId +'-loading-count strong').html();

                $(parentSelector).find("#" + tripPlaceId + ".comment-not-fund-info").remove();
                $(parentSelector).find("#" + tripPlaceId + ".post-comment-top-info").show();
                $(parentSelector).find('.'+ tripPlaceId +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                $(parentSelector).find('div.reaction-comments span b').html(parseInt(all_comment_count) + 1);
                $(parentSelector).find('.'+ tripPlaceId +'-loading-count strong').html((parseInt(loading_count) + 1));

                if (all_comment_count == 0) {
                    $(parentSelector).find('.report-comment-content').find("#" + tripPlaceId + ".post-comment-wrapper").prepend('<div class="report-comment-main">'+commentView+'</div>').fadeIn('slow');
                } else {
                    $(parentSelector).find('.report-comment-content').find(".report-comment-main").prepend(commentView).fadeIn('slow');
                }
            }, 500);
        });

        window.Echo.channel('plan-media-comment-removed').listen('.App\\Events\\PlanMedia\\TripMediaCommentRemovedEvent', function (e) {
            var commentId = e.comment_id;
            var mediaId = e.media_id;

            console.log('plan-media-comment-removed => ', 'App\\Events\\PlanMedia\\TripMediaCommentRemovedEvent => ', e);

            if (!commentId || !mediaId) {
                return;
            }

            var parentSelector = 'div.photo-comment-' + mediaId;

            if (!$(parentSelector).find('.commentRow' + commentId).length) {
                return;
            }

            var all_comment_count = $(parentSelector).parents('.gallery-comment-wrap').find('.'+ mediaId +'-all-comments-count').html();
            var loading_count =  $(parentSelector).parents('.gallery-comment-wrap').find('.'+ mediaId +'-loading-count strong').html();

            $(parentSelector).parents('.gallery-comment-wrap').find('.'+ mediaId +'-all-comments-count').html(parseInt(all_comment_count) - 1);
            $(parentSelector).parents('.gallery-comment-wrap').find('.card-trip_info').find('div.reaction-comments span b').html(parseInt(all_comment_count) - 1);
            $('.'+ mediaId +'-loading-count strong').html(parseInt(loading_count) - 1);
            $(parentSelector).parents('.gallery-comment-wrap').find('.commentRow' + commentId).parent().remove();
        });

        window.Echo.channel('plan-trip-place-comment-removed').listen('.App\\Events\\Plan\\TripPlaceCommentRemovedEvent', function (e) {
            var commentId = e.comment_id;
            var tripPlaceId = e.trip_place_id;

            console.log('plan-trip-place-comment-removed => ', 'App\\Events\\Plan\\TripPlaceCommentRemovedEvent => ', e);

            if (!commentId || !tripPlaceId) {
                return;
            }

            var parentSelector = '.trip-place-wrap[data-placeid="' + tripPlaceId + '"]';

            if (!$(parentSelector).find('.commentRow' + commentId).length) {
                return;
            }

            var all_comment_count = $(parentSelector).find('.'+ tripPlaceId +'-all-comments-count').html();
            var loading_count =  $(parentSelector).find('.'+ tripPlaceId +'-loading-count strong').html();

            $(parentSelector).find('.'+ tripPlaceId +'-all-comments-count').html(parseInt(all_comment_count) - 1);
            $(parentSelector).find('.card-trip_info').find('div.reaction-comments span b').html(parseInt(all_comment_count) - 1);
            $(parentSelector).find('.'+ tripPlaceId +'-loading-count strong').html(parseInt(loading_count) - 1);
            $(parentSelector).find('.commentRow' + commentId).parent().remove();
        });

        var getMediaCommentRequest = [];

        window.Echo.channel('plan-media-comment-changed').listen('.App\\Events\\PlanMedia\\TripMediaCommentChangedEvent', function (e) {
            var commentId = e.comment.id;
            var mediaId = e.comment.medias_id;

             console.log('plan-media-comment-changed => ', '.App\\Events\\PlanMedia\\TripMediaCommentChangedEvent => ', e);

            if (!commentId || !mediaId) {
                return;
            }

            if (getMediaCommentRequest[commentId]) {
                getMediaCommentRequest[commentId].abort();
            }

            getMediaCommentRequest[commentId] = $.ajax({
                method: "GET",
                url: "{{url('medias/comments')}}/" + commentId,
            })
                .done(function (res) {
                    var parentSelector = 'div.photo-comment-' + mediaId;
                    var isRepliesOpen = $(parentSelector).find('.commentRow' + commentId).parent().find('div.comment-reply-block:visible').length;

                    $(parentSelector).find('.commentRow' + commentId).parent().replaceWith(res);

                    if (isRepliesOpen) {
                        $(parentSelector).find('.commentRow' + commentId).parent().find('div.comment-reply-block').show();
                        $(parentSelector).find('.replyForm' + commentId).show();
                        CommentEmoji($(parentSelector).find('.replyForm' + commentId));
                    }
                });
        });

        var getStepCommentRequest = [];

        window.Echo.channel('plan-trip-place-comment-changed').listen('.App\\Events\\Plan\\TripPlaceCommentChangedEvent', function (e) {
            var commentId = e.comment.id;
            var tripPlaceId = e.trip_place_id;

            console.log('plan-trip-place-comment-changed => ', '.App\\Events\\Plan\\TripPlaceCommentChangedEvent => ', e);

            if (!commentId || !tripPlaceId) {
                return;
            }

            if (getStepCommentRequest[commentId]) {
                getStepCommentRequest[commentId].abort();
            }

            getStepCommentRequest[commentId] = $.ajax({
                method: "GET",
                url: "{{url('trip-places/comments')}}/" + commentId,
            })
                .done(function (res) {
                    var parentSelector = '.trip-place-wrap[data-placeid="' + tripPlaceId + '"]';
                    var isRepliesOpen = $(parentSelector).find('.commentRow' + commentId).parent().find('div.comment-reply-block:visible').length;

                    $(parentSelector).find('.commentRow' + commentId).parent().replaceWith(res);

                    if (isRepliesOpen) {
                        $(parentSelector).find('.commentRow' + commentId).parent().find('div.comment-reply-block').show();
                        $(parentSelector).find('.replyForm' + commentId).show();
                        CommentEmoji($(parentSelector).find('.replyForm' + commentId));
                    }
                });
        });

        var updateStepLikesRequests = [];

        window.Echo.channel('plan-step-like').listen('.App\\Events\\Plan\\TripPlaceStepLikeEvent', function (e) {
            var tripPlaceId = e.trip_place_id;

            console.log('plan-step-like => ', '.App\\Events\\Plan\\TripPlaceStepLikeEvent => ', e);

            if (!tripPlaceId) {
                return;
            }

            var _this = $('.trip-place-wrap[data-placeid="' + tripPlaceId + '"] div.trip-place-reaction i.like');

            if (updateStepLikesRequests[tripPlaceId]) {
                updateStepLikesRequests[tripPlaceId].abort();
            }

            updateStepLikesRequests[tripPlaceId] = $.ajax({
                method: "GET",
                url: "{{url('trip-places/likes')}}/" + tripPlaceId,
            })
                .done(function (result) {
                    if (result.is_my_like) {
                        _this.removeClass('trav-heart-icon');
                        _this.addClass('trav-heart-fill-icon');
                    } else {
                        _this.addClass('trav-heart-icon');
                        _this.removeClass('trav-heart-fill-icon');
                    }

                    _this.parents('.trip-place-reaction').find('span.trip-place-like-count b').html(result.count);
                });
        });

        var updateMediaLikesRequests = [];

        window.Echo.channel('plan-media-like').listen('.App\\Events\\PlanMedia\\TripMediaLikeEvent', function (e) {
            var mediaId = e.media_id;

            console.log('plan-media-like => ', '.App\\Events\\PlanMedia\\TripMediaLikeEvent => ', e);


            if (!mediaId) {
                return;
            }

            if (updateMediaLikesRequests[mediaId]) {
                updateMediaLikesRequests[mediaId].abort();
            }

            updateMediaLikesRequests[mediaId] = $.ajax({
                method: "GET",
                url: "{{url('medias/likes')}}/" + mediaId,
            })
                .done(function (result) {
                    $('.'+ mediaId +'-like-count b').html(result.count);
                    $('.'+ mediaId +'-media-liked-count b').html(result.count);
                    $('div.counters[data-mediaid=' + mediaId + '] div.trip-card__likes span').html(result.count)

                    if (result.is_my_like) {
                        $('.'+ mediaId +'-media-like-icon').html('<i class="trav-heart-fill-icon" style="color:#ff5a79;"></i>');
                        $('.'+ mediaId +'-media-like-icon').attr('data-type', 'like');

                    } else {
                        $('.'+ mediaId +'-media-like-icon').html('<i class="trav-heart-icon"></i>');
                        $('.'+ mediaId +'-media-like-icon').attr('data-type', 'unlike');
                    }
                });
        });


        window.Echo.channel('plan-deleted').listen('.App\\Events\\Plan\\PlanDeletedEvent', function (e) {
            var deletedPlanId = e.plan_id;

            console.log('plan-deleted.{user_id} => ', '.App\\Events\\Plan\\PlanDeletedEvent => ', e);

            if (!deletedPlanId || tripId != deletedPlanId) {
                return;
            }

            window.location.href = '/home';
        });
    });


    @if(isset($trip) && is_object($trip))
    function updatePlanData() {
        var planId = parseInt('{{$trip->id}}');

        $.ajax({
            method: "GET",
            url: '{{url('trip/ajax-get-plan-data')}}/' + planId,
        })
            .done(function (res) {
                var editable = parseInt('{{$editable}}');

                if (res.status == 'success') {
                    var data = res.data.actual;

                    if (editable) {
                        data = res.data.draft;
                    }

                    $('span.navbar-text.mobile-hide').text(data.title);
                    $('span.tooltiptext').text(data.title);

                    $('div#modal-final_step').find('input[name=title]').val(data.title);
                    $('div#modal-final_step').find('textarea[name=description]').val(data.description);
                    $('div#modal-final_step').find('input[name=privacy_create_trip][value='+data.privacy+']').prop("checked", true);

                    $('div#modal-edit_title').find('input[name=title]').val(data.title);
                    $('div#modal-edit_title').find('textarea[name=description]').val(data.description);
                    $('div#modal-edit_title').find('input[name=privacy][value='+data.privacy+']').prop("checked", true);

                    if (data.cover) {
                        $('div#modal-edit_title div.cover-upload').attr('style', 'background: url("' + data.cover + '")');
                        $('div#modal-edit_title div.cover-upload').addClass('with-image');

                        $('div#modal-final_step').find('div.cover').attr('style', 'background: url("' + data.cover + '");');
                        $('div#modal-final_step').find('div.cover').addClass('with-image');
                    }

                    document.title = data.title;
                }
            });
    }
    @endif
</script>