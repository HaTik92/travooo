<div class="modal modal-sidebar modal-place" backdrop="false" id="modal-add_place" role="dialog" aria-labelledby="Edit Place">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Add a Place</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method='post' id='form_trip_place' autocomplete="off">
                <div class="modal-body pb-0">

                    <div class="form-row">
                        <div class="col-6 mobile-fullwidth">
                            <div class="form-group add-city">
                                <label for="location">City</label>
                                <div class="input-group input-dropdown cities">
                                    <div class="input-group-img city" style="width: 20%; max-width: 54px;"></div>
                                    <input type="text" name="cities_id" id="cities_id" data-placeholder="Type to search for a City..." class="input groupOne cities_id" style="width: 80%;height: 51px;border: 0;padding: 5px;" autocomplete="off" >
                                </div>
                                <div class="input-group-dropdown">
                                    <div class="input-group-dropdown-body select select-city">
                                        <div class="map-select map-select-city" style="overflow: auto;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Mobile element -->
                                <button type="button" class="btn btn-light-primary select-from-map" data-dismiss="modal" aria-label="Close">Select from the map</button>
                            <!-- Mobile element END-->
                        </div>
                        <div class="col-6 mobile-fullwidth">
                            <div class="form-group add-place">
                                <label for="location">Place</label>
                                <div class="input-group input-dropdown suggestedClass" id="dropCont">
                                    <div class="input-group-img place disabled" style="width: 20%; max-width: 54px;"></div>
                                        <input disabled type="text" name="places_id" id="places_id" data-placeholder="Type to search for a Place..." class="disabled input groupOne places_id" style="width: 80%;height: 51px;border: 0;padding: 5px;" autocomplete="off" >
                                </div>
                                <div class="input-group-dropdown">
                                    <div class="input-group-dropdown-body select select-place">
                                        <div class="map-select map-select-place" style="overflow: auto;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row mt-4">
                        <div class="col-12">
                            @if(isset($trip) && is_object($trip) && $trip->memory)
                            <label for="location">When did you arrive?</label>
                            @else
                            <label for="location">When you will arrive?</label>
                            @endif
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="input-group input-group-time">
                                    <div class="input-group-img" style="background-image: url({{asset('trip_assets/images/ico-calendar.svg')}});"></div>
                                    <p class="input-name">Date</p>
                                    <input @if (isset($trip) && is_object($trip) && !$trip->memory) value="{{date('d M Y')}}" @endif readonly="readonly" type="text" name='date' class="datepicker_place"  required placeholder="19 October 2017" style='border: none;margin-top: 20px;'>
                                    <input type='hidden' name='actual_place_date' id='actual_place_date' @if (isset($trip) && is_object($trip) && !$trip->memory) value="{{date('Y-m-d')}}" @else value="" @endif/>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="input-group input-group-time">
                                    <div class="input-group-img" style="background-image: url({{asset('trip_assets/images/ico-clock.svg')}});"></div>
                                    <p class="input-name">Time</p>
                                    <input onkeydown="event.preventDefault()" inputmode='none' type="text" name='time' onCut="return false" id='time' class="timepicker form-control" placeholder="9:20 PM" @if (isset($trip) && is_object($trip) && !$trip->memory) value="{{date('g:ia')}}" @endif>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row optional memory-only mt-4">
                        <div class="col-12">
                            <div class="form-group memory-only">
                                <label for="">Add media (optional)</label>
                                <div class="swiper-media swiper-container">
                                    <div class="swiper-wrapper" id="media-container">
                                        <div class="swiper-slide" id="img-upload">
                                            <button type="button" class="img-upload"></button>
                                        </div>
                                    </div>
                                    <div class="swiper-button-next"><img src="{{asset('trip_assets/images/ico-chevron_right.svg')}}" alt=""></div>
                                    <div class="swiper-button-prev"><img src="{{asset('trip_assets/images/ico-chevron_left.svg')}}" alt=""></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row memory-only mt-4 mb-3 pb-3">
                        <div class="col-12">
                            <button onclick="$('div#modal-story div.story').html('');" type="button" class="add-story"><img src="{{asset('trip_assets/images/white-edit-icon.svg')}}" class="mr-2" alt=""> Write your story (optional)<span></span></button>
                            <div id="modal-story">
                                <label for="">Write your story</label>
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="editor">
                                                <span class="bold">B</span> <span class="separator">|</span>
                                                <span class="italic">I</span> <span class="separator">|</span>
                                                <span class="link">L</span>
                                                <input class="link" placeholder="https://example.com"/>
                                                <span class="separator">|</span>
                                                <span class="unlink" style="font-family: 'Arial';">X</span>
                                            </div>
                                            <div data-placeholder='Enter your story…' contenteditable="true" class="story"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hr mt-4"></div>
                    <div class="open-optional mb-3">
                        Add more details <i class='fa fa-caret-down'></i>
                    </div>
                    <div class="optional-block mb-4">
                        <div class="form-row mt-2">
                            <div class="col-6 mobile-fullwidth">
                                <div class="form-group mb-0 optional">
                                    @if(isset($trip) && is_object($trip) && $trip->memory)
                                    <label for="">How long did you stay? <span class="optional">(optional)</span></label>
                                    @else
                                    <label for="">Planning to stay <span class="optional">(optional)</span></label>
                                    @endif

                                    <div class="form-row">
                                        <div class="col-4">
                                            <div class="input-group input-group-couunter">
                                                <p class="input-name">Days</p>
                                                <div class="counter"><span class="minus" data-before="0"></span>
                                                    <input onchange="this.value = parseInt(this.value);" onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" class="count" name="duration_days" id="duration_days" value="0" max="1000" min="0"> <span class="plus" data-before="0"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="input-group input-group-couunter">
                                                <p class="input-name">Hours</p>
                                                <div class="counter"><span class="minus" data-before="0"></span>
                                                    <input onchange="this.value = parseInt(this.value);" onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" class="count" name="duration_hours" id="duration_hours" value="0" max="23" min="0"> <span class="plus" data-before="0"></span></div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="input-group input-group-couunter">
                                                <p class="input-name">Minutes</p>
                                                <div class="counter"><span class="minus" data-before="0"></span>
                                                    <input onchange="this.value = parseInt(this.value);" onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" class="count" name="duration_minutes" id="duration_minutes" value="0" max="59" min="0"> <span class="plus" data-before="0"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mobile-fullwidth">
                                <div class="form-group mb-0 optional">
                                    <label class="d-flex" for="">
                                        @if(isset($trip) && is_object($trip) && $trip->memory)
                                        <span>How much did you spend? <span class="optional">(optional)</span></span>
                                        @else
                                        <span>How much you will spend? <span class="optional">(optional)</span></span>
                                        @endif
                                            @if(isset($trip) && is_object($trip) && $trip->memory)<span class="label-sub ml-auto">Spent so far <b class="overall-budget">$500</b></span>@else<span class="label-sub ml-auto">Spent so far <b class="overall-budget">$500</b></span>@endif</label>
                                    <div class="form-row">
                                        <div class="col-4">
                                            <div class="input-group">
                                                <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control form-control-amount count" name="budget" id="budget_custom" placeholder="Amount" max="2000000000" min="0">
                                                <div class="input-group-append"><span class="input-group-text">$</span></div>
                                            </div>
                                        </div>
                                        <div class="col-8">
                                            <div class="group-radio-form group-radio-form-cards group-radio-form-amount d-flex justify-content-between w-100">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="editAmountMoney1" name="budget" value='50' class="custom-control-input">
                                                    <label class="custom-control-label" for="editAmountMoney1">50$</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="editAmountMoney2" name="budget" value='100' class="custom-control-input">
                                                    <label class="custom-control-label" for="editAmountMoney2">100$</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="editAmountMoney3" name="budget" value='200' class="custom-control-input">
                                                    <label class="custom-control-label" for="editAmountMoney3">200$</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-3">
                            <div class="col-12">
                                <div class="form-group optional memory-only">
                                        <label for="">What is this place known for? (optional)</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="known_for" name="known_for" placeholder="Food, Photography, History...">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type='hidden' name='places_real_id' id='places_real_id' value=""/>
                    <input type='hidden' name='cities_real_id' id='cities_real_id' />
                    <input type='hidden' name='order' id='order' />
                    <button type="button" class="btn btn-color-gray ml-auto" data-dismiss="modal">CANCEL</button>
                    <button type="button" class="btn btn-primary ml-1 doAddPlace" data-id=''>ADD</button>
                </div>
            </form>
        </div>
    </div>
</div>