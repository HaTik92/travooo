<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('assets2/js/timepicker/jquery.timepicker.min.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets2/js/plupload-2.3.6/js/jquery.ui.plupload/css/jquery.ui.plupload.css')}}" media="screen" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
<link href="{{asset('/plugins/croppie/croppie.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets2/js/lightbox2/src/css/lightbox.css') }}" rel="stylesheet"/>
<link href="{{asset('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
<link href="{{asset('assets2/css/profile-post.css')}}" rel="stylesheet"/>
<link rel="stylesheet" href="{{asset('trip_assets/styles/vendor.css')}}">
<link rel="stylesheet" href="{{asset('assets2/css/style-15102019-9.css')}}" />
<link rel="stylesheet" href="{{asset('trip_assets/styles/main.css')}}">
<link rel="stylesheet" href="{{asset('assets2/css/trip-planner.css')}}">
<link rel="stylesheet" href="{{asset('assets2/css/trip-planner-suggestions.css')}}">
<link rel="stylesheet" href="{{asset('assets2/css/trip-planner-share.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<style>
.drop-row {
    padding: 10px 20px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
}

.drop-wrap .drop-row .img-wrap {
    position: relative;
}

.drop-wrap .drop-row .img-wrap.rounded.place-img img {
    border: 1px solid #e5e5e5;
    width: 60px !important;
    height: 60px;
    border-radius: 50%;
    -o-object-fit: cover;
    object-fit: cover;
}

.drop-content {
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    padding: 0 15px 0 10px;
}
.drop-wrap .drop-row .drop-content p.place-name {
    margin-bottom: 0;
}


.drop-wrap .drop-row .drop-content p.place-name span.follow-tag {
    display: inline-block;
    border-radius: 2px;
    background: #b3b3b3;
    font-family: 'Circular Std Book';
    color: white;
    font-size: 12px;
    line-height: 20px;
    text-transform: uppercase;
    padding: 0 10px;
}
.rounded {
    border-radius: .25rem;
}

.drop-row .img-wrap.rounded.place-img img {
    border: 1px solid #e5e5e5;
    width: 60px !important;
    height: 60px;
    border-radius: 50%;
    -o-object-fit: cover;
    object-fit: cover;
}

.drop-row .drop-content p.place-name {
    margin-bottom: 0;
}

    .show {
  display: block;
}
    .hide {
  display: none;
}
.select2-input {
        height: 53px !important;
    padding: 8px !important;
    border: none !important;
    }  
    .select2-selection__choice {
        background: #f7f7f7 !important;
    padding: 5px 10px !important;
    position: relative !important;
    border-radius: 0 !important;
        color: #4080ff !important;
        margin-top:0px !important;
    }
    .select2-selection__arrow{
        top:11px !important;
    }
    .ui-state-highlight {
        height:25px;
        background-color:#c4d9ff;
    }
    .padding-10{
        padding: 10px 0 !important;
    }
    em {
        font-style: italic;
    }
    
    .latest-link{
        padding-left: 10px;
    }
    .invite-user {
      float: left;
      text-align: center;
      margin: 3px;
    }
</style>
<script>
    function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("filterDiv");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    w3AddClass(x[i], "hide");
    if (x[i].className.indexOf(c) > -1)  {
        w3AddClass(x[i], "show")
        w3RemoveClass(x[i], "hide")
    }
  }
}

// Show filtered elements
function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}

// Hide elements that are not selected
function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(" ");
}

// Add active class to the current control button (highlight it)
if(null!==document.getElementById("myBtnContainer")) {
var btnContainer = document.getElementById("myBtnContainer");
var btns = btnContainer.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}
}
</script>