@if(isset($trip) && is_object($trip))
<div class="modal fade modal-sm vertical-center decline-suggestion warning-popup" id="modal-decline_suggestion" tabindex="-1" role="dialog" aria-labelledby="Share" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Decline Suggestion</h5>
                </div>
                <div class="modal-body">
                    <div class="confirm-label">Are you sure you want to decline this suggestion?</div>
                    <div class="description">
                        This suggestion will be removed and you will not be able to undo this action.
                    </div>
                </div>
                <div class="footer">
                    <button class="back" data-dismiss="modal" aria-label="Close">GO BACK</button>
                    <button class="decline">DECLINE NOW</button>
                </div>
            </div>
        </div>
    </div>
@endif