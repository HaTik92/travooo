<script>
    var suggestions = null;
    var lastTab = null;
    var meta = null;

    function getSuggestions() {
        var placeId = $('div#modal-suggestions').attr('data-id');

        $.ajax({
            method: "GET",
            url: "{{url('/suggestions')}}",
            data: {
                plan_id: $('#trip_id').val(),
                place_id: placeId
            }
        })
            .done(function (res) {
                suggestions = res.data.suggestions;
                meta = res.data.meta;

                if (!lastTab) {
                    $('div#modal-suggestions div.navigation div.tab:first-child').trigger('click');
                } else {
                    $('div#modal-suggestions div.navigation div.tab[data-type=' + lastTab + ']').trigger('click');
                }

                $('div#modal-suggestions').modal('show');

                $('div#modal-suggestions div.navigation div.tab div.unread_logs').remove();

                for (var type in meta) {
                    if (meta[type].unread_count > 0) {
                        $('div#modal-suggestions div.navigation div.tab[data-type=' + type + ']').append('<div class="unread_logs">' + meta[type].unread_count + '</div>');
                    }
                }

            }).fail(function(data) {
            if (data.status == 403) {
                location.reload();
            }
        });
    }

    function readTabLogs(type) {
        var placeId = $('div#modal-suggestions').attr('data-id');

        $.ajax({
            method: "POST",
            url: "{{url('/suggestions/read')}}",
            data: {
                plan_id: $('#trip_id').val(),
                place_id: placeId,
                type: type
            }
        })
            .done(function (res) {
                $('div#modal-suggestions div.navigation div.tab[data-type=' + type + '] div.unread_logs').remove();
                if (suggestions[type] !== undefined) {
                    for (var i in suggestions[type]) {
                        suggestions[type][i].unread = 0;
                    }
                }

            }).fail(function(data) {
            if (data.status == 403) {
                location.reload();
            }
        });
    }

    $(document).ready(function() {
        $('div#modal-add_suggestion input[name=suggest_budget]').on('click', function (e) {
            $('#suggest_budget_custom').val($(this).val());
            $('#suggest_budget_custom').change();
        });

        $("div#modal-add_suggestion .datepicker_place").datepicker({
            dateFormat: "d MM yy",
            altField: "div#modal-add_suggestion #suggest_actual_place_date",
            altFormat: "yy-mm-dd",
            defaultDate: ""
            @if(isset($trip) && $trip->memory)
            ,maxDate: 0
            @endif
        });

        $(document).on('click', 'div#modal-edit_place.for-editor div.suggestable', function() {
            $('div#modal-add_suggestion h5').hide();
            $('div#modal-add_suggestion div.attribute').hide();
            $('div#modal-add_suggestion textarea.reason').val('');
            $('div#modal-add_suggestion textarea.reason').trigger('change');
            $('div#modal-add_suggestion').removeClass('story');

            var parent = $(this).parents('.modal-place');
            var type = $(this).data('type');
            var placeId = parent.find('input#trip_places_real_id').val();

            if (type == 'story') {
                $('div#modal-add_suggestion').addClass('story');
            }

            renderSuggestPopupData(parent, type);

            $('div#modal-add_suggestion h5.' + type).show();
            $('div#modal-add_suggestion div.attribute.' + type).show();

            $('div#modal-add_suggestion').modal('show');
            $('div#modal-add_suggestion').attr('data-type', type);
            $('div#modal-add_suggestion').attr('data-place-id', placeId);
        });

        $(document).on('click', 'div#modal-add_suggestion button.suggest', function() {
            if ($('div#modal-add_suggestion div.swiper-slide.temp:visible').length) {
                alert('File is uploading');
                return;
            }

            //disableSuggestButton();

            var type = $('div#modal-add_suggestion').attr('data-type');
            var placeId = $('div#modal-add_suggestion').attr('data-place-id');
            var reason = $('div#modal-add_suggestion textarea.reason').val().trim();
            var values = getSuggestionValues(type);

            $.ajax({
                method: "POST",
                contentType: 'application/json',
                url: "{{url('/suggestions')}}",
                data: JSON.stringify({
                    suggestion_edit_type: type,
                    values: values,
                    reason: reason,
                    plan_id: $('#trip_id').val(),
                    place_id: placeId
                })
            })
                .done(function (res) {
                    $('div#modal-add_suggestion').modal('hide');
                    $('div.modal-place').modal('hide');
                    updatePlanContents();
                    addButtonsAfterEdits();
                }).fail(function(data) {
                if (data.status == 403) {
                    location.reload();
                }
            });
        });

        $(document).on('change keyup', 'div#modal-add_suggestion textarea.reason', function() {
            var value = $(this).val().trim();

            if (value.length > 0) {
                releaseSuggestButton();
            } else {
                //disableSuggestButton();
            }
        });

        $(document).on('click', 'div.trip-place div.suggestion.exist', function() {
            var parent = $(this).parents('div.trip-place');
            var img = $(parent).find('div.card-img-holder img').attr('src');
            var title = $(parent).find('a.plan-place').text();
            var city = $(parent).find('a.plan-place').attr('data-city');
            var time = $(parent).find('span.activity-time').last().text();
            var placeId = $(parent).attr('data-id');

            $('div#modal-suggestions div.modal-body div.header').attr('style', 'background-image: url("' + img + '")');
            $('div#modal-suggestions div.modal-body div.header div.name').text(title);
            $('div#modal-suggestions div.modal-body div.header div.location').text(city);
            $('div#modal-suggestions div.modal-body div.header div.data').text(time);

            $('div#modal-suggestions').attr('data-id', placeId);

            lastTab = null;

            getSuggestions();
        });

        $(document).on('click', 'div#modal-suggestions div.navigation div.tab', function() {
            renderTab($(this).data('type'));
        });

        $(document).on('click', 'div#modal-suggestions button.approve', function() {
            var suggestionId = $(this).attr('data-id');

            $.ajax({
                method: "POST",
                url: "{{route('trip.ajax_approve_suggestion')}}",
                data: {suggestion_id: suggestionId}
            })
                .done(function (res) {
                    updatePlanContents();
                    addButtonsAfterEdits();
                    getSuggestions();
                }).fail(function(data) {
                if (data.status == 403) {
                    location.reload();
                }
            });
        });

        $(document).on('click', 'div#modal-suggestions button.disapprove', function() {
            var suggestionId = $(this).attr('data-id');
            $('#modal-decline_suggestion').find('button.decline').attr('data-id', suggestionId);
            $('#modal-decline_suggestion').modal('show');
        });

        $(document).on('click', '#modal-decline_suggestion button.decline', function() {
            var suggestionId = $(this).attr('data-id');

            $.ajax({
                method: "POST",
                url: "{{route('trip.ajax_disapprove_suggestion')}}",
                data: {suggestion_id: suggestionId}
            })
                .done(function (res) {
                    updatePlanContents();
                    getSuggestions();
                    $('#modal-decline_suggestion').modal('hide');
                }).fail(function(data) {
                if (data.status == 403) {
                    location.reload();
                }
            });
        });
    });

    function getSuggestionValues(type) {
        var values = {};

        switch (type)
        {
            case 'duration':
                values.duration_days = $('input#suggest_duration_days').val();
                values.duration_hours = $('input#suggest_duration_hours').val();
                values.duration_minutes = $('input#suggest_duration_minutes').val();
                break;

            case 'budget':
                values.budget = $('input#suggest_budget_custom').val();
                break;

            case 'media':
                var medias = [];

                $('div#modal-add_suggestion .swiper-wrapper .swiper-slide[data-mediaid]').each(function (e) {
                    medias.push($(this).attr('data-mediaid'));
                });

                values.medias = medias;
                break;

            case 'date':
                values.date = $('input#suggest_actual_place_date').val();
                values.time = $('input#suggest_time').val();
                break;

            case 'story':
                values.story = $('div#modal-add_suggestion div#modal-story div.story').html();
                break;

            case 'tags':
                values.tags = $('div#modal-add_suggestion input#suggest_known_for').val();
                break;

            default:
                break;
        }

        return values;
    }

    function disableSuggestButton() {
        $('div#modal-add_suggestion button.suggest').addClass('disabled');
        $('div#modal-add_suggestion button.suggest').attr('disabled', 'disabled');
    }

    function releaseSuggestButton() {
        $('div#modal-add_suggestion button.suggest').removeClass('disabled');
        $('div#modal-add_suggestion button.suggest').removeAttr('disabled');
    }

    function renderSuggestPopupData(parent, type) {
        switch (type)
        {
            case 'duration':
                $('input#suggest_duration_days').val($(parent).find('input[name=duration_days]').val());
                $('input#suggest_duration_hours').val($(parent).find('input[name=duration_hours]').val());
                $('input#suggest_duration_minutes').val($(parent).find('input[name=duration_minutes]').val());
                break;

            case 'budget':
                $('input#suggest_budget_custom').val($(parent).find('input.form-control-amount').val());
                break;

            case 'media':
                $('div#modal-add_suggestion .swiper-wrapper .swiper-slide[data-mediaid]').remove();
                $(parent).find('.swiper-wrapper .swiper-slide[data-mediaid]').clone().prependTo($('div#modal-add_suggestion .swiper-wrapper'));
                break;

            case 'date':
                $('input#suggest_time').val($(parent).find('input#edit_time').val());
                $('input#suggest_actual_place_date').val($(parent).find('input#edit_actual_place_date').val());
                $('input#suggest_date').val($(parent).find('input.datepicker_place').val());
                break;

            case 'story':
                $('div#modal-add_suggestion div.attribute.story').html('');
                $(parent).find('div#modal-story').clone().prependTo($('div#modal-add_suggestion div.attribute.story'));
                $('div#modal-add_suggestion div.attribute.story div#modal-story').show();
                break;

            case 'tags':
                $('div#modal-add_suggestion input#suggest_known_for').tagsinput('removeAll');
                $(parent).find('input#known_for').val().split(',').forEach(function (e, i) {
                    $('input#suggest_known_for').tagsinput('add', e);
                });
                break;

            default:
                break;
        }
    }

    function renderTab(type) {
        readTabLogs(type);
        $('div#modal-suggestions div.navigation div.tab').removeClass('active');
        $('div#modal-suggestions div.navigation div.tab[data-type=' + type + ']').addClass('active');
        $('div#modal-suggestions div.modal-body div.content').html('');

        lastTab = type;

        var isAdmin = '{{$is_admin}}';

        if (suggestions[type]) {
            suggestions[type].forEach(function(suggestion, i) {
                var actions = '';

                if (isAdmin && suggestion.status === 0) {
                    actions = '                            <div class="actions">\n' +
                        '                                <button data-id="' + suggestion.id + '" class="disapprove">✕</button>\n' +
                        '                                <button data-id="' + suggestion.id + '" class="approve">✓</button>\n' +
                        '                            </div>\n';
                } else if (suggestion.status !== 0) {
                    actions = suggestion.status === 1 ? 'Approved' : 'Disapproved';
                } else {
                    actions = 'Pending';
                }

                var role = suggestion.user.role ? '[' + suggestion.user.role + ']' : '';

                var unreadDot = '';

                if (suggestion.unread) {
                    unreadDot = '<span class="new" style="display: inline-block; height: 10px; width: 10px; border-radius: 5px; background: #fc225a; margin-right: 10px;"></span>'
                }

                if (suggestion.reason == null) {
                    var reason = '<span style="' +
                        '    font-family: inherit;' +
                        '    font-style: italic;' +
                        '    opacity: 0.5;' +
                        '">“The editor has made no comment.”</span>'
                } else {
                    var reason = '“' + suggestion.reason + '”';
                }

                var row = '<div class="row">\n' +
                    '                        <div class="row-header">\n' +
                    '                            <div class="author">\n' +
                                                        unreadDot +
                    '                                <div class="img">\n' +
                    '                                    <img src="' + suggestion.user.img + '" alt="" />\n' +
                    '                                </div>\n' +
                    '                                <div class="info">\n' +
                    '                                    <div class="name text-overflow-prevent">' + suggestion.user.name + '<span>' + role + '</span></div>\n' +
                    '                                    <div class="date">' + suggestion.date + '</div>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' + actions +
                    '                        </div>\n' +
                    '                        <div class="row-content">\n' +
                    '                            <div class="value">' + getValueDependsOnType(suggestion.suggested_place, type) + '</div>\n' +
                    '                            <div class="reason">' + reason + '</div>\n' +
                    '                        </div>\n' +
                    '                    </div>'    ;

                $('div#modal-suggestions div.modal-body div.content').append(row);
            });
        }
    }

    function getValueDependsOnType(place, type) {
        switch (type)
        {
            case 'duration':
                var duration = place.duration;

                if(duration > (60 * 24)) {
                    var days = Math.trunc(duration / (60 * 24));
                }
                else {
                    var days = 0;
                }

                duration = duration - days * (60 * 24);

                if(duration > 60) {
                    var hours = Math.trunc(duration / 60);
                }
                else {
                    var hours = 0;
                }

                var minutes = duration - (hours*60);

                var result = '';

                if (days > 0) {
                    result += days + ' Days';
                }

                if (hours > 0) {
                    if (days > 0) {
                        result += ' : ';
                    }

                    result +=  hours + ' Hours';
                }

                if (minutes > 0) {
                    if (days > 0 || hours > 0) {
                        result += ' : ';
                    }

                    result += minutes + ' Minutes';
                }

                if (days === 0 && hours === 0 && minutes === 0) {
                    return 'Suggested no duration';
                }

                return result;
                break;

            case 'budget':
                return place.budget ? '$' + place.budget : '$0';
                break;

            case 'media':
                var result = '';

                if (!place.medias.length) {
                    return '<div class="value-empty">Suggested no media</div>';
                }

                place.medias.forEach(function(media, i) {
                    if (media.media.type == 1) {
                        result += '<img src=' + media.media.url + ' alt=""/>';
                    } else {
                        result += '<video width="100%" controls>\n' +
                        '  <source src="' + media.media.url + '" type="video/mp4">\n' +
                        '  Your browser does not support the video tag.\n' +
                        '</video>';
                    }
                });

                return result;
                break;

            case 'date':
                var date = moment(place.time);
                var result = date.format('DD MMM yy');

                if (!place.without_time) {
                    result += ' <span>at</span> ' + date.format('HH:mm A');
                }

                return result;
                break;

            case 'story':
                var checkOnlyEmptyElements = $('<div>' + place.story + '</div>').text().trim();

                return checkOnlyEmptyElements ? place.story : 'Suggested no story';
                break;

            case 'tags':
                return place.comment ? place.comment.split(',').join(', ') : 'Suggested no tags';
                break;

            default:
                break;
        }
    }

    // Media tab style
    $('.modal.suggestions .navigation .tab').on('click', function (e) {
        $(this).parent().siblings('.content').removeClass('media-content');
        if($(this).data('type') == 'media') { $(this).parent().siblings('.content').addClass('media-content'); }
    });
</script>