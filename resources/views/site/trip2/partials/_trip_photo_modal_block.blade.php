<div class='cover-block' id='{{ $photo->media->id }}' type="{{$photo->media->type}}" url="{{$photo->media->url}}" trip-media-id="{{$photo->id}}" trip-place-id="{{$photo->trip_place_id}}">
    <div class='cover-block-inner comment-block'>
        @if ($tp->published)
        <div class="trip__side-buttons" posttype="Media">
            @if(!$is_invited)
            <button class="trip__side-btn plan_share_button" type="button" id="{{ $photo->media->id }}">
                <div class="trip__side-btn-icon"><svg class="icon icon--share">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
                    </svg></div>
                <div class="trip__side-btn-text">Share</div>
            </button>
            @endif
            <button class="trip__side-btn" type="button" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{ $photo->media->id }},this)">
                <div class="trip__side-btn-icon"><svg class="icon icon--flag">
                        <use xlink:href="{{asset('assets3/img/sprite.svg#flag')}}"></use>
                    </svg></div>
                <div class="trip__side-btn-text">Report</div>
            </button>
        </div>
        @endif
        <div class='gallery-comment-wrap'>
            <div class='gallery-comment-inner mCustomScrollbar'>
                @if(isset($photo->media->users[0]) && is_object($photo->media->users[0]))
                <div class='top-gallery-content gallery-comment-top'>
                    <div class='top-info-layer'>
                        <div class='top-avatar-wrap'>
                            @if(isset($photo->media->users[0]) && is_object($photo->media->users[0]))
                            <img src='{{check_profile_picture($photo->media->users[0]->profile_picture)}}' alt='' style='width:50px;hright:50px;'>
                            @endif
                        </div>
                        <div class='top-info-txt'>
                            <div class='preview-txt'>
                                @if(isset($photo->media->users[0]) && is_object($photo->media->users[0]) && $photo->media->users[0]->name != '')
                                <a class='dest-name' href='#'>{{$photo->media->users[0]->name}}</a>
                                    {!! get_exp_icon($photo->media->users[0]) !!}
                                @endif

                                <p class='dest-place'>@lang("place.uploaded_a")
                                    <b>
                                        @if ($photo->media->type === \App\Models\ActivityMedia\Media::TYPE_VIDEO)
                                            @lang("place.video")
                                        @else
                                            @lang("place.photo")
                                        @endif
                                    </b>
                                    <span class="date">{{$photo->media->uploaded_at}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class='gallery-comment-txt'>
                        @if($photo->media->title)
                            @php

                            $dom = new \DOMDocument;
                            @$dom->loadHTML($photo->media->title);
                            $elements = @$dom->getElementsByTagName('div');

                            if(count($elements)> 0){
                                $content = $elements[0]->nodeValue;
                            }else{
                                $content = $photo->media->title;
                            }
                                $showChar = 100;
                                $ellipsestext = "...";

                                if(strlen($content) > $showChar) {

                                    $c = substr($content, 0, $showChar);
                                    $h = substr($content, $showChar, strlen($content) - $showChar);

                                    $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span>';

                                    $content = $html;
                                }
                            @endphp
                            <p style="word-wrap: break-word;">{!!$content!!}</p>
                        @else
                            <p>No Description ...</p>
                        @endif
                    </div>
                    <div class='gal-com-footer-info'>
                        @if ($tp->published)
                        <div class='post-foot-block post-reaction'>
                            <i @if ($photo->media->likes->where('users_id', $authUserId)->count()) class="trav-heart-fill-icon @else class="trav-heart-icon @endif photo_like_button" id="{{$photo->media->id}}"></i>
                            <span class="{{$photo->media->id}}-like-count"><b>{{count($photo->media->likes)}}</b></span>
                        </div>
                        <div class='post-foot-block'>
                            <i class="trav-comment-icon" dir="auto"></i>
                            <span style="color: #2b2b2b"><b class="{{$photo->media->id}}-all-comments-count">{{@count($photo->media->comments)}}</b>&nbsp;@lang('comment.comments')</span>
                        </div>

                        <div class="trip__side-buttons mobile" posttype="Media">
                            @if(!$is_invited && $trip->privacy !== 2)
                                <button class="trip__side-btn plan_share_button" type="button" id="{{ $photo->media->id }}">
                                    <div class="trip__side-btn-icon"><svg class="icon icon--share">
                                            <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
                                        </svg></div>
                                </button>
                            @endif
                            <button class="trip__side-btn" type="button" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{ $photo->media->id }},this)">
                                <div class="trip__side-btn-icon"><svg class="icon icon--flag">
                                        <use xlink:href="{{asset('assets3/img/sprite.svg#flag')}}"></use>
                                    </svg></div>
                            </button>
                        </div>
                        @endif
                    </div>
                </div>
                @endif
                <div class='post-comment-layer photo-comment-{{$photo->media->id}}'>
                    <div class="post-comment-top-info" id="{{$photo->media->id}}" {{(isset($photo->media->comments) && count($photo->media->comments) > 0)?"":'style=display:none'}} >
                        <ul class="comment-filter">
                            <li class="comment-filter-type" data-type="Top" data-comment-type="1">@lang('other.top')</li>
                            <li class="comment-filter-type active" data-type="New" data-comment-type="1">@lang('other.new')</li>
                        </ul>
                        <div class="comm-count-info">
                            <span class="{{$photo->media->id}}-loading-count loading-count" data-id="{{$photo->media->id}}"><strong>0</strong></span> / <span class="{{$photo->media->id}}-all-comments-count all-comments-count" data-id="{{$photo->media->id}}">{{count($photo->media->comments)}}</span>
                        </div>
                    </div>
                    <div class="post-comment-wrapper sortBody report-comment-wraper" id="{{$photo->media->id}}">
                        @if(isset($photo->media->comments) && count($photo->media->comments) > 0)
                            <div class="report-comment-main">
                            </div>
                            <div id="loadMore1" class="loadMore1" reportId="{{$photo->media->id}}">
                                <input type="hidden" id="pagenum" value="0">
                                <div id="reportloader" class="post-animation-content post-comment-row">
                                    <div class="report-com-avatar-wrap">
                                        <div class="post-top-avatar-wrap animation post-animation-avatar"></div>
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="report-top-name animation"></div>
                                        <div class="report-com-info animation"></div>
                                        <div class="report-com-sec-info animation"></div>
                                        <div id="hide_loader_1" class="report-com-thr-info animation"></div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="post-comment-top-info comment-not-fund-info" id="{{$photo->media->id}}">
                                <ul class="comment-filter">
                                    <li style="margin-left: 20px;">No comments yet ..</li>
                                    <li></li>
                                </ul>
                                <div class="comm-count-info">
                                </div>
                            </div>
                            <div class="report-comment-main">
                            </div>
                            <div id="loadMore1" class="loadMore1" reportId="{{$photo->media->id}}" style="display: none;">
                                <input type="hidden" id="pagenum" value="0">
                                <div id="reportloader" class="post-animation-content post-comment-row">
                                    <div class="report-com-avatar-wrap">
                                        <div class="post-top-avatar-wrap animation post-animation-avatar"></div>
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="report-top-name animation"></div>
                                        <div class="report-com-info animation"></div>
                                        <div class="report-com-sec-info animation"></div>
                                        <div id="hide_loader_1" class="report-com-thr-info animation"></div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @if($authUserId && $tp->published)
                <div class="post-add-comment-block">
                    <div class="avatar-wrap">
                        <img src="{{$auth_picture}}" alt="" width="45px" height="45px">
                    </div>
                    <div class="post-add-com-inputs">
                        <form class="reportCommentForm mediaCommentForm" method="POST" data-id="{{$photo->media->id}}"  data-type="2" autocomplete="off" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" data-id="pair{{$photo->media->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                            <div class="post-create-block reports-comment-block" tabindex="0">
                                <div class="post-create-input report-create-input w-530">
                                        <textarea name="text" data-id="text" id="report_comment_text" class="textarea-customize report-comment-textarea report-comment-emoji"
                                                  placeholder="@lang('comment.write_a_comment')"></textarea>
                                </div>
                                <div class="post-create-controls d-none">
                                    <div class="post-alloptions">
                                        <ul class="create-link-list">
                                            <li class="post-options">
                                                <input type="file" name="file[]" class="report-comment-media-input" data-report_id="{{$photo->media->id}}" data-id="commentfile{{$photo->media->id}}" style="display:none">
                                                <i class="fa fa-camera click-target" aria-hidden="true" data-target="commentfile{{$photo->media->id}}"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="comment-edit-action">
                                        <a href="javascript:;" class="report-comment-cancel-link">Cancel</a>
                                        <a href="javascript:;" class="post-report-comment-link report-comment-link">Post</a>
                                    </div>
                                </div>
                                <div class="medias report-media">
                                    <div class="spinner-border rep-spinner-border" role="status" style="display:none"><span class="sr-only">Loading...</span></div>
                                </div>
                            </div>
                            <input type="hidden" name="report_id" value="{{$photo->media->id}}"/>
                            <button type="submit" class="btn btn-primary d-none"></button>
                        </form>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>