@if(isset($trip) && is_object($trip))
<div class="modal fade modal-sm vertical-center leave-popup warning-popup" id="modal-leave_popup" tabindex="-1" role="dialog" aria-labelledby="Share" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Leave Trip Plan</h5>
                </div>
                <div class="modal-body">
                    <div class="confirm-label">Are you sure you want to leave this trip plan?</div>
                    <div class="description">
                        You made <span class="changes-count"></span> to the following trip. After you leave this plan all your contributions will be assigned to the admin
                        <span class="admin"><img src="{{check_profile_picture($trip->author->profile_picture)}}" alt=""/>{{$trip->author->name}}</span>
                    </div>
                    <div class="changes">
                    </div>
                </div>
                <div class="footer">
                    <button class="back" data-dismiss="modal" aria-label="Close">GO BACK</button>
                    <button class="leave">LEAVE NOW</button>
                </div>
            </div>
        </div>
    </div>
@endif