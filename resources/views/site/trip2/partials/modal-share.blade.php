@if(isset($trip) && is_object($trip))
<div class="modal fade modal-sm vertical-center landscape-fix" id="modal-share" tabindex="-1" role="dialog" aria-labelledby="Share" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Share</h5>
                    <button type="button" class="btn btn-primary ml-auto" data-dismiss="modal">Done</button>
                </div>
                <div class="modal-body">
                    <p class="text-center"><img src="{{asset('trip_assets/images/start.svg')}}"></p>
                    <h3>Congratulations!</h3>
                    <p class="mb-5"><b>Your trip plan is live now</b>, spread the word and show the world your adventure</p>
                    <div class="share-type share-type-link">
                        <p>Trip plan link</p>
                        <input class="share-link" type="text" placeholder="https://www.travooo.com/trip/plan/{{$trip->id}}" readonly="readonly">
                    </div>
                    <div class="share-type mb-0">
                        <p>Share on</p>
                        <ul class="share-list">
                            <li>
                                <a href="#"><img class="share-published-plan" data-toggle="modal" data-target="#modal-share_popup" src="{{asset('trip_assets/images/ico-share-travoo.png')}}" alt=""></a>
                            </li>
                            <li>
                                <a target="_blank" href="https://twitter.com/intent/tweet?text=https://www.travooo.com/trip/plan/{{$trip->id}}"><img src="{{asset('trip_assets/images/ico-share-twitter.png')}}" alt=""></a>
                            </li>
                            <li>
                                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.travooo.com/trip/plan/{{$trip->id}}"><img src="{{asset('trip_assets/images/ico-share-facebook.png')}}" alt=""></a>
                            </li>
                            <li>
                                <a target="_blank" href="https://pinterest.com/pin/create/link/?url=https://www.travooo.com/trip/plan/{{$trip->id}}&media={{urlencode(get_plan_map($trip, 613, 378, 1, null, 10))}}&description=plan"><img src="{{asset('trip_assets/images/ico-share-pinterest.png')}}" alt=""></a>
                            </li>
                            <li>
                                <a target="_blank" href="https://tumblr.com/widgets/share/tool?canonicalUrl=https://www.travooo.com/trip/plan/{{$trip->id}}"><img src="{{asset('trip_assets/images/ico-share-tumblr.png')}}" alt=""></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif