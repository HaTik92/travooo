<div class="plan-chats">
    <div class="places section">
        <div class="previews">
            @foreach(array_slice($trip_places_arr, 0, 3) as $tp)
                <img class="preview" src="{{$tp->thumb}}" alt="{{$tp->name}}" title="{{$tp->name}}">
            @endforeach
        </div>
        <div class="title active">Places</div>
    </div>
    <div class="separator"></div>
    <div class="chats section">
        <div class="previews">
            @if ($plan_chats['unread_count'])
                <div class="notification-badge">{{$plan_chats['unread_count']}}</div>
            @endif
            @php
                $addedSrc = [];
            @endphp
            @foreach($plan_chats['data'] as $key => $chat)
                @php
                    $cnt = 0;
                @endphp
                @while(isset($chat['participants'][$cnt]) && !in_array($chat['participants'][$cnt]['src'], $addedSrc) && count($addedSrc) < 3)
                    <img class="preview" src="{{$chat['participants'][$cnt]['src']}}" alt="{{$chat['participants'][$cnt]['name']}}" title="{{$chat['participants'][$cnt]['name']}}">
                    @php
                        $addedSrc[] = $chat['participants'][$cnt]['src'];
                        $cnt++;
                    @endphp
                @endwhile
            @endforeach
        </div>
        <div class="title active">Chats</div>
    </div>
</div>
<div class="places-chats window">
    <div class="header">
        <div class="title">Places Chats</div>
        <div class="desc">List of places included in the trip plan</div>
    </div>
    <div class="content">
        @foreach($trip_places_arr as $tp)
            <div class="place-chat" data-id="{{$tp->id}}">
                <div class="place-block">
                    <div class="image">
                        <img src="{{$tp->thumb}}">
                    </div>
                    <div class="info">
                        <div class="title light">
                            {{$tp->name}}
                        </div>
                        <div class="desc">
                            {{$tp->city_name}}, {{$tp->country->transsingle->title}} &middot; @if (!$tp->without_time) {{\Carbon\Carbon::parse($tp->time)->format('D g:i A')}} @else {{\Carbon\Carbon::parse($tp->time)->format('D')}} @endif
                        </div>
                    </div>
                </div>
                <div class="place-chat-members">
                    @php
                        $cnt = 0;
                    @endphp
                    @foreach($tp->chat_users as $chuRoles)
                        @foreach($chuRoles as $role => $chu)
                            @php
                                $cnt++;

                                if ($cnt > 3) {
                                    continue;
                                }
                            @endphp
                            <img class="member-image" src="{{$chu['src']}}" alt="{{$chu['name']}}" title="{{$chu['name']}}">
                        @endforeach
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
</div>

<div class="all-plan-chats window">
    <div class="header">
        <div class="title">Chats</div>
        <div class="desc">List of all plan chats</div>
    </div>
    <div class="content">
        @foreach($plan_chats['data'] as $chat)
            <div data-id="{{$chat['id']}}" class="place-chat">
                <div class="place-block">
                    <div class="image">
                        @if ($chat['unread_count'])
                            <div class="notification-badge">{{$chat['unread_count']}}</div>
                        @endif
                        @if ($chat['type'] === \App\Services\PlaceChat\PlaceChatsService::CHAT_TYPE_MAIN)
                            @foreach($chat['participants'] as $key => $participant)
                                @if ($key < 3)
                                    <img src="{{$participant['src']}}">
                                @endif
                            @endforeach
                        @else
                            @foreach($chat['participants'] as $key => $participant) @if ($participant['id'] == $chat['created_by_id']) <img src="{{$participant['src']}}"> @endif @endforeach
                        @endif
                    </div>
                    <div class="info">
                        <div class="title @if (!$chat['unread_count'] && $chat['type'] !== \App\Services\PlaceChat\PlaceChatsService::CHAT_TYPE_MAIN) light @endif">
                            @if ($chat['type'] === \App\Services\PlaceChat\PlaceChatsService::CHAT_TYPE_MAIN)
                                Trip Chat: <span>@foreach($chat['participants'] as $key => $participant)@if ($key > 0), @endif {{$participant['name']}}@endforeach</span>
                            @else
                                @foreach($chat['participants'] as $key => $participant) @if ($participant['id'] == $chat['created_by_id']) {{$participant['name']}} @endif @endforeach
                            @endif
                        </div>
                        <div class="desc plan-chat-desc">
                            @if (is_object($chat['last_message']))
                                <span>{{$chat['last_message']->message}}</span> &middot; {{\Carbon\Carbon::parse($chat['last_message']->created_at)->format('D g:i A')}}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

<div class="place-chats window">
    <div class="header">
        <div class="back">
            <img src="{{asset('trip_assets/images/chats-back-arrow.svg')}}" alt="">
        </div>
        <div>
            <div class="title"></div>
            <div class="desc"></div>
        </div>
    </div>
    <div class="content">
    </div>
</div>

<div class="place-chat window">
    <div class="header">
        <div class="back">
            <img src="{{asset('trip_assets/images/chats-back-arrow.svg')}}" alt="">
        </div>
        <div>
            <div class="title"></div>
            <div class="desc">@if(isset($trip)){{$trip->title}}@endif</div>
        </div>
        <div class="dropdown">
            <div class="plan-users-thumbs">
            </div>
            <img src="{{asset('trip_assets/images/chat-dropdown.svg')}}" alt="">
            <div class="chat-users-block">
            </div>
        </div>

    </div>
    <div class="content">
        <div class="place-chats-dialog">
        </div>
        <div class="place-chats-send">
            <textarea placeholder="Say something..."></textarea>
            <button>Send</button>
        </div>
    </div>
</div>