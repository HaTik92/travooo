<div class="modal modal-sidebar fade modal-place @if ($is_editor) for-editor @endif" id="modal-edit_place" tabindex="-1" role="dialog" aria-labelledby="Edit Place" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Edit Place</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method='post' id='form_edit_trip_place' autocomplete="off">
                <div class="form-row">
                    <div class="col-6 mobile-fullwidth">
                        <div class="form-group add-city">
                            <label for="location" class="cities_id">City</label>
                            <div class="input-group input-dropdown cities">
                                <div class="input-group-img city" style="width: 20%; max-width: 54px;"></div>
                                <input type="text" name="cities_id" id="cities_id" data-placeholder="Type to search for a City..." class="input groupOne cities_id" style="width: 80%;height: 51px;border: 0;padding: 5px;" autocomplete="off" >
                            </div>
                            <div class="input-group-dropdown">
                                <div class="input-group-dropdown-body select select-city">
                                    <div class="map-select map-select-city" style="overflow: auto;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 mobile-fullwidth">
                        <div class="form-group add-place">
                            <label for="location" class="places_id">Place</label>
                            <div class="input-group input-dropdown" id="dropCont">
                                <div class="input-group-img place" style="width: 20%; max-width: 54px;"></div>
                                <input type="text" name="places_id" id="places_id" data-placeholder="Type to search for a Place..." class="input groupOne places_id" style="width: 80%;height: 51px;border: 0;padding: 5px;" autocomplete="off" >
                            </div>
                            <div class="input-group-dropdown">
                                <div class="input-group-dropdown-body select select-place">
                                    <div class="map-select map-select-place" style="overflow: auto;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($is_editor)
                    <div class="hr"></div>
                    <div class="form-row">
                        <div class="col-12 edit-suggestion">
                            <div class="icon">
                                <img src="{{asset('trip_assets/images/edit-suggestion.svg')}}" alt="">
                            </div>
                            <div class="desc">You can send edit suggestions to the trip plan admin by tapping on one of the fields below.</div>
                        </div>
                    </div>
                @endif
                <div class="form-row mb-3 pb-3 mt-4 suggestable" data-type="date">
                    <div class="col-12">
                        @if(isset($trip) && is_object($trip))
                            <label for="location" class="time date">When you will arrive?</label>
                        @endif
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div class="input-group input-group-time">
                                <div class="input-group-img" style="background-image: url({{asset('trip_assets/images/ico-calendar.svg')}});"></div>
                                <p class="input-name">Date</p>
                                <input readonly="readonly" type="text" name='date' class="datepicker_place" id="edit_date" required placeholder="19 October 2017" style='border: none;margin-top: 20px;'>
                                <input type='hidden' name='edit_actual_place_date' id='edit_actual_place_date' value=""/>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div class="input-group input-group-time">
                                <div class="input-group-img" style="background-image: url({{asset('trip_assets/images/ico-clock.svg')}});"></div>
                                <p class="input-name">Time</p>
                                <input onkeydown="event.preventDefault()" type="text" name='time' id='edit_time' onCut="return false" inputmode='none' class="timepicker form-control" placeholder="9:20 PM" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hr mt-2"></div>
                <div class="open-optional">
                    Add more details <i class='fa fa-caret-down'></i>
                </div>
                <div class="optional-block">
                    <div class="form-row mt-2">
                        <div class="col-6 mobile-fullwidth">
                            <div class="form-group mb-0 optional suggestable" data-type="duration">
                                @if(isset($trip) && is_object($trip))
                                    <label for="" class="duration">Planning to stay (optional)</label>
                                    @endif
                                <div class="form-row">
                                    <div class="col-6">
                                        <div class="input-group input-group-couunter">
                                            <p class="input-name">Hours</p>
                                            <div class="counter"><span class="minus"></span>
                                                <input type="number" class="count" name="duration_hours" id="edit_duration_hours" value="0"> <span class="plus"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="input-group input-group-couunter">
                                            <p class="input-name">Minutes</p>
                                            <div class="counter"><span class="minus"></span>
                                                <input type="number" class="count" name="duration_minutes" id="edit_duration_minutes" value="0"> <span class="plus"></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 mobile-fullwidth">
                            <div class="form-group mb-0 optional suggestable" data-type="budget">
                                <label class="d-flex budget" for="">
                                    @if(isset($trip) && is_object($trip))
                                        <span>How much you will spend? (optional)</span>
                                        @endif
                                        @if(isset($trip) && is_object($trip))<span class="label-sub ml-auto">Spent so far <b class="overall-budget">$500</b></span>@endif</label>
                                <div class="form-row">
                                    <div class="col-4">
                                        <div class="input-group">
                                            <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control form-control-amount" name="edit_budget" id="edit_budget_custom" placeholder="Amount" min="0" max="2000000000">
                                            <div class="input-group-append"><span class="input-group-text">$</span></div>
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="group-radio-form group-radio-form-cards group-radio-form-amount d-flex justify-content-between w-100">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="amountMoney1"  name="edit_budget" value='50' class="custom-control-input amountMoney1">
                                                <label class="custom-control-label" for="amountMoney1">50$</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="amountMoney2"  name="edit_budget" value='100' class="custom-control-input amountMoney2">
                                                <label class="custom-control-label" for="amountMoney2">100$</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="amountMoney3"  name="edit_budget" value='200' class="custom-control-input amountMoney3">
                                                <label class="custom-control-label" for="amountMoney3">200$</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" @if ($is_editor) style="display: none;" @endif>
                <input type='hidden' name='places_real_id' id='places_real_id' />
                <input type='hidden' name='trip_places_real_id' id='trip_places_real_id' />
                <input type='hidden' name='cities_real_id' id='cities_real_id' />
                <input type='hidden' name='order' id='order' />
                <button type="button" class="btn btn-color-danger ml-auto" id="removePlace" data-id="">DELETE</button>
                <button type="submit" class="btn btn-primary ml-4 doEditPlace">SAVE</button>
            </div>
            </form>
        </div>
    </div>
</div>