<div class="modal modal-sidebar suggestions" backdrop="false" id="modal-suggestions" role="dialog" aria-labelledby="Suggestions">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Place Suggestions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="trav-angle-left"></i></button>
            </div>
            <div class="modal-body">
                <div class="header">
                    <div class="name"></div>
                    <div class="location"></div>
                    <div class="data"></div>
                </div>
                <div class="navigation">
                    <div class="tab active" data-type="date">
                        Time
                    </div>
                    <div class="tab" data-type="duration">
                        Duration
                    </div>
                    <div class="tab" data-type="budget">
                        Budget
                    </div>
                    <div class="tab" data-type="media">
                        Media
                    </div>
                    <div class="tab" data-type="story">
                        Story
                    </div>
                    <div class="tab" data-type="tags">
                        Tags
                    </div>
                </div>
                <div class="content">
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>