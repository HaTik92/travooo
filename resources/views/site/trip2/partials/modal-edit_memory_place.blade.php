<div class="modal modal-sidebar fade modal-place memory @if ($is_editor) for-editor @endif" id="modal-edit_place" tabindex="-1" role="dialog" aria-labelledby="Edit Place" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Edit Place</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method='post' id='form_edit_trip_place' autocomplete="off">
                <div class="form-row">
                    <div class="col-6 mobile-fullwidth">
                        <div class="form-group">
                            <label for="location" class="cities_id">City</label>
                            <div class="input-group input-dropdown cities">
                                <div class="input-group-img city" style="width: 20%; max-width: 54px;"></div>
                                <input type="text" name="cities_id" id="cities_id" data-placeholder="Type to search for a City..." class="input groupOne cities_id" style="width: 80%;height: 51px;border: 0;padding: 5px;" autocomplete="off" >
                            </div>
                            <div class="input-group-dropdown">
                                <div class="input-group-dropdown-body select select-city">
                                    <div class="map-select map-select-city" style="overflow: auto;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 mobile-fullwidth">
                        <div class="form-group">
                            <label for="location" class="places_id">Place</label>
                            <div class="input-group suggestedClass input-dropdown" id="dropCont">
                                <div class="input-group-img place" style="width: 20%; max-width: 54px;"></div>
                                <input type="text" name="places_id" id="places_id" data-placeholder="Type to search for a Place..." class="input groupOne places_id" style="width: 80%;height: 51px;border: 0;padding: 5px;" autocomplete="off" >
                            </div>
                            <div class="input-group-dropdown">
                                <div class="input-group-dropdown-body select select-place">
                                    <div class="map-select map-select-place" style="overflow: auto;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($is_editor)
                    <div class="hr"></div>
                    <div class="form-row">
                        <div class="col-12 edit-suggestion">
                            <div class="icon">
                                <img src="{{asset('trip_assets/images/edit-suggestion.svg')}}" alt="">
                            </div>
                            <div class="desc">You can send edit suggestions to the trip plan admin by tapping on one of the fields below.</div>
                        </div>
                    </div>
                @endif
                <div class="form-row mt-4 suggestable" data-type="date">
                    <div class="col-12">
                        @if(isset($trip) && is_object($trip))
                            <label for="location" class="time date">When did you arrive?</label>
                        @endif
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div class="input-group input-group-time">
                                <div class="input-group-img" style="background-image: url({{asset('trip_assets/images/ico-calendar.svg')}});"></div>
                                <p class="input-name">Date</p>
                                <input readonly="readonly" type="text" name='date' class="datepicker_place" id="edit_date_memory" required placeholder="19 October 2017" style='border: none;margin-top: 20px;'>
                                <input type='hidden' name='edit_actual_place_date' id='edit_actual_place_date' value=""/>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div class="input-group input-group-time">
                                <div class="input-group-img" style="background-image: url({{asset('trip_assets/images/ico-clock.svg')}});"></div>
                                <p class="input-name">Time</p>
                                <input onkeydown="event.preventDefault()" type="text" name='time' id='edit_time' onCut="return false" inputmode='none' class="timepicker form-control" placeholder="9:20 PM" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row optional memory-only mt-4 suggestable" data-type="media">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="">Add media</label>
                            <div class="swiper-media swiper-container">
                                <div class="swiper-wrapper" id="edit-media-container">
                                    <div class="swiper-slide" id="edit-img-upload">
                                        <button type="button" class="img-upload"></button>
                                    </div>
                                </div>
                                <div class="swiper-button-next"><img src="{{asset('trip_assets/images/ico-chevron_right.svg')}}" alt=""></div>
                                <div class="swiper-button-prev"><img src="{{asset('trip_assets/images/ico-chevron_left.svg')}}" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row memory-only mt-4 mb-3 pb-3 suggestable" data-type="story">
                    <div class="col-12">
                        <button type="button" class="add-story">
                            <img src="{{asset('trip_assets/images/white-edit-icon.svg')}}" class="mr-2" alt=""> Write your story<span></span>
                        </button>
                        <div id="modal-story">
                            <label for=""><span>Write your story</span></label>
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="editor disabled-input-toggle">
                                            <div class='disable--overlay'></div>
                                            <span class="bold">B</span> <span class="separator">|</span>
                                            <span class="italic">I</span> <span class="separator">|</span>
                                            <span class="link">L</span>
                                            <input class="link disable--input" placeholder="https://example.com"/>
                                            <span class="separator">|</span>
                                            <span class="unlink" style="font-family: 'Arial';">X</span>
                                        </div>
                                        <div data-placeholder='Enter your story…' contenteditable="true" class="story input-disable-opacity"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hr mt-4"></div>
                <div class="open-optional">
                    Add more details <i class='fa fa-caret-down'></i>
                </div>
                <div class="optional-block">
                    <div class="form-row mt-2">
                        <div class="col-6 mobile-fullwidth">
                            <div class="form-group mb-0 optional suggestable" data-type="duration">
                                @if(isset($trip) && is_object($trip))
                                    <label for="" class="duration">How long did you stay?</label>
                                @endif
                                <div class="form-row">
                                    <div class="col-4">
                                        <div class="input-group input-group-couunter">
                                            <p class="input-name">Days</p>
                                            <div class="counter"><span class="minus"></span>
                                            <input onchange="this.value = parseInt(this.value);" onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" class="count" name="duration_days" id="edit_duration_days" value="0" max="1000" min="0"> <span class="plus"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="input-group input-group-couunter">
                                            <p class="input-name">Hours</p>
                                            <div class="counter"><span class="minus"></span>
                                            <input onchange="this.value = parseInt(this.value);" onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" class="count" name="duration_hours" id="edit_duration_hours" value="0" max="23" min="0"> <span class="plus"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="input-group input-group-couunter">
                                            <p class="input-name">Minutes</p>
                                            <div class="counter"><span class="minus"></span>
                                            <input onchange="this.value = parseInt(this.value);" onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" class="count" name="duration_minutes" id="edit_duration_minutes" value="0" max="59" min="0"> <span class="plus"></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 mobile-fullwidth">
                            <div class="form-group mb-0 optional suggestable" data-type="budget">
                                <label class="d-flex budget" for="">
                                    @if(isset($trip) && is_object($trip))
                                        <span>How much did you spend?</span>
                                    @endif
                                    @if(isset($trip) && is_object($trip))<span class="label-sub ml-auto">Spent so far <b class="overall-budget">$500</b></span>@endif</label>
                                <div class="form-row">
                                    <div class="col-4">
                                        <div class="input-group">
                                            <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control form-control-amount count" name="edit_budget" id="edit_budget_custom" placeholder="Amount" min="0" max="2000000000">
                                            <div class="input-group-append"><span class="input-group-text">$</span></div>
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="group-radio-form group-radio-form-cards group-radio-form-amount d-flex justify-content-between w-100">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="memory-amountMoney1"  name="edit_budget" value='50' class="custom-control-input amountMoney1">
                                                <label class="custom-control-label" for="memory-amountMoney1">50$</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="memory-amountMoney2"  name="edit_budget" value='100' class="custom-control-input amountMoney2">
                                                <label class="custom-control-label" for="memory-amountMoney2">100$</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="memory-amountMoney3"  name="edit_budget" value='200' class="custom-control-input amountMoney3">
                                                <label class="custom-control-label" for="memory-amountMoney3">200$</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    @if(isset($trip) && is_object($trip))
                    <div class="form-row mt-4 suggestable mb-4" style="margin-top: 30px;" data-type="tags">
                        <div class="col-12">
                            <div class="form-group optional memory-only">
                                    <label for="" class="comment">What is this place known for?</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="known_for" name="known_for" placeholder="Food, Photography, History...">
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                </form>
            </div>

            <div class="modal-footer" @if ($is_editor) style="display: none;" @endif>
                <input type='hidden' name='places_real_id' id='places_real_id' />
                <input type='hidden' name='trip_places_real_id' id='trip_places_real_id' />
                <input type='hidden' name='cities_real_id' id='cities_real_id' />
                <input type='hidden' name='order' id='order' />
                <button type="button" class="btn btn-color-danger ml-auto" id="removePlace" data-id="">DELETE</button>
                <button type="submit" class="btn btn-primary ml-4 doEditPlace">SAVE</button>
            </div>
        </div>
    </div>
</div>