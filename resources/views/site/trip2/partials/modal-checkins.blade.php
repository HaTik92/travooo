<div class="modal fade vertical-center" id="modal-checkins" tabindex="-1" role="dialog" aria-labelledby="checkins" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 509px; display: block;">
        <div class="modal-content">
            <div class="modal-header">
                <div class="navigation">
                    <div class="tab active" data-type="friend">Friends<span>0</span></div>
                    <div class="tab" data-type="follower">Followers<span>0</span></div>
                    <div class="tab" data-type="expert">Experts<span>0</span></div>
                </div>
                <img class="icon-cross" alt="" src="{{asset('assets2/image/plans/invite-top-icon-cross.png')}}" data-dismiss="modal" aria-label="Close"/>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>