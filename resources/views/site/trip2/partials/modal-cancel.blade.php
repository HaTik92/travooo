<div class="modal fade plan-popup vertical-center" id="modal-cancel" tabindex="-1" role="dialog" aria-labelledby="Edit" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
        <div class="modal-dialog" role="document">
            <form>
            <div class="modal-content">
                <div class="modal-header">
                    <div class="title" style="width: 100%; justify-content: space-between;">
                        <h5 class="modal-title">Undo Edit</h5>
                        <div>
                            <button type="button" class="btn btn-primary cancel" id="undo" style="text-align: center; padding: 0; float:right;">Undo the last edit</button>
                        </div>
                    </div>
                </div>
                <div class="modal-inner-trip_info">
                    <span class="bold">
                        Are you sure you want to undo the last edit?
                    </span>
                </div>
            </div>
            </form>
        </div>
    </div>