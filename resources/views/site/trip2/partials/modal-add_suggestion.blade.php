<div class="modal modal-sidebar add-suggestion" backdrop="false" id="modal-add_suggestion" role="dialog" aria-labelledby="Suggest">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center duration">Duration Suggestion</h5>
                <h5 class="modal-title text-center budget">Budget Suggestion</h5>
                <h5 class="modal-title text-center date">Time Suggestion</h5>
                <h5 class="modal-title text-center media">Add Media</h5>
                <h5 class="modal-title text-center story">Story Suggestion</h5>
                <h5 class="modal-title text-center tags">Place Known For Suggestion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="attribute duration">
                    <div class="col-12">
                        <div class="form-group mb-0 optional">
                            <label for="" class="duration">
                                <span>How long did you stay?</span>
                            </label>
                            <div class="form-row">
                                <div class="col-4">
                                    <div class="input-group input-group-couunter">
                                        <p class="input-name">Days</p>
                                        <div class="counter"><span class="minus"></span>
                                            <input onchange="this.value = parseInt(this.value);" onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" class="count" name="duration_days" id="suggest_duration_days" value="0" max="1000" min="0"> <span class="plus"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="input-group input-group-couunter">
                                        <p class="input-name">Hours</p>
                                        <div class="counter"><span class="minus"></span>
                                            <input onchange="this.value = parseInt(this.value);" onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" class="count" name="duration_hours" id="suggest_duration_hours" value="0" max="23" min="0"> <span class="plus"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="input-group input-group-couunter">
                                        <p class="input-name">Minutes</p>
                                        <div class="counter"><span class="minus"></span>
                                            <input onchange="this.value = parseInt(this.value);" onkeypress="return event.charCode >= 48 && event.charCode <= 57" type="number" class="count" name="duration_minutes" id="suggest_duration_minutes" value="0" max="59" min="0"> <span class="plus"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="attribute budget">
                    <div class="col-12">
                        <div class="form-group mb-0 optional">
                            <label class="d-flex budget" for="">
                                    <span>How much did you spend?</span>
                            </label>
                            <div class="form-row">
                                <div class="col-4">
                                    <div class="input-group">
                                        <input type="number" class="form-control form-control-amount count" onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="suggest_budget" id="suggest_budget_custom" placeholder="Amount" min="0" max="2000000000">
                                        <div class="input-group-append"><span class="input-group-text">$</span></div>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="group-radio-form group-radio-form-cards group-radio-form-amount d-flex justify-content-between w-100">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="suggest-amountMoney1"  name="suggest_budget" value='50' class="custom-control-input amountMoney1">
                                            <label class="custom-control-label" for="suggest-amountMoney1">50$</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="suggest-amountMoney2"  name="suggest_budget" value='100' class="custom-control-input amountMoney2">
                                            <label class="custom-control-label" for="suggest-amountMoney2">100$</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="suggest-amountMoney3"  name="suggest_budget" value='200' class="custom-control-input amountMoney3">
                                            <label class="custom-control-label" for="suggest-amountMoney3">200$</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="attribute story col-12">
                </div>

                <div class="attribute date col-12">
                    <div class="form-row">
                        <div class="col-12">
                            <label for="location" class="time date"><span>When you checked-in?</span></label>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="input-group input-group-time">
                                    <div class="input-group-img" style="background-image: url({{asset('trip_assets/images/ico-calendar.svg')}});"></div>
                                    <p class="input-name">Date</p>
                                    <input readonly="readonly" type="text" name='date' class="datepicker_place" id="suggest_date" required placeholder="19 October 2017" style='border: none; margin-top: 20px; max-width: 100px; font-size: 12px;'>
                                    <input type='hidden' name='suggest_actual_place_date' id='suggest_actual_place_date' value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="input-group input-group-time">
                                    <div class="input-group-img" style="background-image: url({{asset('trip_assets/images/ico-clock.svg')}});"></div>
                                    <p class="input-name">Time</p>
                                    <input onkeydown="event.preventDefault()" type="text" name='time' inputmode='none' id='suggest_time' class="timepicker form-control" placeholder="9:20 PM" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="attribute media form-row" style="padding: 0 16px;">
                    <div class="col-12">
                        <div class="form-group">
                            <label for=""><span>Upload photos & videos</span></label>
                            <div class="swiper-media swiper-container">
                                <div class="swiper-wrapper" id="suggest-media-container">
                                    <div class="swiper-slide" id="suggest-img-upload">
                                        <button type="button" class="img-upload"></button>
                                    </div>
                                </div>
                                <div class="swiper-button-next"><img src="{{asset('trip_assets/images/ico-chevron_right.svg')}}" alt=""></div>
                                <div class="swiper-button-prev"><img src="{{asset('trip_assets/images/ico-chevron_left.svg')}}" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="attribute tags col-12">
                    <div>
                        <div class="form-group optional">
                            <label for=""><span>What is this place known for?</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="suggest_known_for" name="suggest_known_for" placeholder="Food, Photography, History...">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="suggestion-general-block">
                    <div class="col-12">
                        <div class="form-group mb-0 optional">
                            <label>
                                <span>Why you made this suggestion?</span>
                            </label>
                            <textarea type="text" class="reason" placeholder="Why you made this suggestion?" maxlength="200"></textarea>
                            <span class="desc">Your suggestion will be reviewed by the trip plan admin.</span>
                        </div>
                    </div>

                    <div class="footer">
                        <button class="back" data-dismiss="modal" aria-label="Close">Go back</button>
                        <button class="suggest">Send</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>