<link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
<script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
<script src="https://npmcdn.com/@turf/turf@5.1.6/turf.min.js"></script>
<script src="{{ asset('assets2/js/chat.js') }}"></script>

<script>
    var mobileControlsVisible = $('.mobile-map-view-controls:visible').length;
    let trendingMarkerType = 1;
    var popups = [];
    var trendingPopups = [];
    var nearbyPopups = [];
    var showHint = true;
    var invisibleMarkers = [];

    var showOnlyChildren = null;

    var beforeCity, beforeCountry, beforePoint = null;

    var editable = parseInt("{{$editable}}");
    var convertedToMemory = parseInt("{{$convertedToMemory}}");
    var preloadTrends = <?= json_encode($trends);?>;
    var preloadTrendsTime = <?= json_encode($trends_time);?>;
    var countriesIso = <?= json_encode($countries_iso);?>;
    var usePreload = true;
    var nearbyTimeoutIndex = 0;
    var suggestedMarkers = [];
    var topSuggestedMarkers = [];
    var nearbyMarkers = [];
    var trendingMarkers = [];
    var savedTrendingMarkers = [];
    var searchMarkersCoordinates = [];
    var nearbyMarkersCoordinates = [];
    var trendingMarkersCoordinates = [];
    var activeMarker = null;
    var beforeClusters = [];

    $('div.map-placeholder-block').hide();

    var trip_places_arr = [];
    var saved_trip_places_arr = [];
    var placesCoordinates = [];
    var placesCoordinatesFlat = [];
    var placesMapping = [];
    var nearbyCoordinates = [];
    var trendingCoordinates = [];
    var mapBounds = [
        [-180, -85],
        [180, 85]
    ];
    var markers = [];
    var placeMarkers = [];
    var oldMarkers = [];

    var coordinates = {
        'city': [],
        'country': [],
        'world': []
    };

    var clusters = [];
    var markersClusters = [];
    var nearbyClusters = [];
    var trendingClusters = [];
    var nearbyMarkersClusters = [];
    var trendingMarkersClusters = [];
    var renderedClusters = [];
    var renderedTrendingClusters = [];
    var bounds = new mapboxgl.LngLatBounds();
    var trendingBounds = new mapboxgl.LngLatBounds();
    var nearbyBounds = new mapboxgl.LngLatBounds();
    var clustersBounds = [];
    var trendingClustersBounds = [];
    var allTrendingBounds = [];

    var savedLines = [];
    var savedLength = [];
    var savedClusters = [];

    var relations = [];

    var geoJsonPlaces = {
        "type": "FeatureCollection",
        "features": []
    };

    var countryZoomLevel = 4.5;
    var cityZoomLevel = 7;
    var useCountryTempZoom = false;
    var useCityTempZoom = false;
    var useWorldTempZoom = false;

    var isSuggestedPlaces = false;

    var elementsCount = 0;
    var forceNearby = false;

    var labels = ['country-label', 'state-label', 'settlement-minor-label', 'settlement-major-label'];

    @if (isset($trip_places_arr))
        trip_places_arr = <?= json_encode($trip_places_arr);?>;
        elementsCount = trip_places_arr.length;
        saved_trip_places_arr = [...trip_places_arr];
        generateTripPlacesRelations();
    @endif

    var lastNearbyCenter = null;
    var map = initMap();

    window.plan_map = map;

    map.on('load', function () {
        //fitBounds();
        setRouteSources();
        addZoomIconsToLabels();
    });

    var labelReq = null;

    function labelArrowClick() {
        var id = $(this).attr('data-id');
        var marker = labelMarkers[id];

        forceNearby = false;
        var features = map.queryRenderedFeatures(marker._pos, {layers: labels});

        if (!features.length) {
            return;
        }

        var code = features[0].properties.iso_3166_1;
        var code2 = features[0].properties.iso_3166_2;
        var title = features[0].properties.name_en;
        var type = features[0].properties.type;

        var url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + title + '.json?' + 'country=' + code + '&city=' + code2 + '&access_token=' + '{{config('mapbox.token')}}';

        if (labelReq !== null) {
            labelReq.abort();
        }

        labelReq = $.ajax({
            url: url,
            success: function(res) {
                var bounds = new mapboxgl.LngLatBounds();

                var maxZoom = 9;

                if (type === 'country' || res.features[0].bbox !== undefined) {
                    bounds.extend(res.features[0].bbox);
                    maxZoom = 15;
                } else {
                    bounds.extend(res.features[0].center);
                }

                forceNearby = true;

                map.fitBounds(bounds, {
                    maxZoom: maxZoom
                });

                useCountryTempZoom = true;

                updateTrendingData(1, countriesIso[code], 0, [], false, 1, true);
            }
        });
    }

    function fitBounds(maxZoom = 10, trending = false, nearby = false) {
        var fitBounds = bounds;

        if (trending) {
            fitBounds = trendingBounds;
        }

        if (nearby) {
            fitBounds = nearbyBounds;
            maxZoom = 20;
            if (!fitBounds.isEmpty()) {
                lastNearbyCenter = fitBounds.getCenter();
            }
        }

        if (!fitBounds.isEmpty()) {
            map.fitBounds(fitBounds, {
                padding: {top: 120, bottom:50, left: 50, right: 50},
                maxZoom: maxZoom
            });
        } else if (!trending && !nearby) {
            map.setZoom(1);
        }
    }

    function generateVistisBlock(id, locationType, checkinsData)
    {
        var checkins = '<div class="checkins" data-id="' + id + '" data-locationtype="' + locationType + '">';
        var count = 0;

        if (checkinsData && checkinsData.length) {
            checkinsData.forEach(function(user) {
                if (user.person_type === 'other') {
                    return;
                }

                if (count < 3) {
                    checkins += '<img width="25px" src="'+user.src+'"/>'
                }
                count++;
            });

            checkins += '<span class="bold">' + count + '</span>';
            checkins += '<span>Visited</span></div>';
        } else {
            checkins += '<span class="bold">0</span><span>Visited</span></div>';
        }

        return checkins;
    }

    function generatePopup(id, img, name, address, reviewsAvg, reviewsLength, time, cityId, cityName, checkinsData = [], addToTrip = false, isTrending = false, trendingType = null, trendingLocation = '', tempSrc = '', cityImg = '', locationType = 0) {
        if (isTrending && trendingType) {
            locationType = trendingType;
        }

        var route = '/place/' + id;
        var add = '';

        if (addToTrip) {
            switch (locationType) {
                case 0:
                    add = '<button data-cityimg="' + cityImg + '" data-cityid="'+cityId+'" data-citytitle="'+cityName+'" data-placeid="'+id+'" data-placesrc="'+img+'" data-title="'+name+'" class="add-map-place btn btn-primary">Add to your trip</button>';
                    route = '/place/' + id;
                    break;
                case 1:
                    add = '<button data-cityimg="' + cityImg + '" data-citytitle="'+name+'" data-cityid="'+id+'" class="add-map-place btn btn-primary">Add to your trip</button>';
                    route = '/city/' + id;
                    break;
                case 2:
                    add = '<button data-countrytitle="'+name+'" class="add-map-place btn btn-primary">Add to your trip</button>';
                    route = '/country/' + id;
                    break;
                default:
                    break;
            }
        }

        var checkins = generateVistisBlock(id, locationType, checkinsData);

        var reviews = '';

        if (trendingType === null) {
            if (reviewsLength) {
                reviews = '        <div class="hero__rating">\n' +
                    '            <div class="star-rating">\n' +
                    '                <span style="width: ' + (reviewsAvg * 20) + '%"></span>\n' +
                    '            </div>\n' +
                    '            <div class="hero__rating-value"><span>' + reviewsAvg + '</span> from ' + reviewsLength + ' Reviews</div>\n' +
                    '        </div>';
            } else {
                reviews = '        <div class="hero__rating">\n' +
                    '            <div class="hero__rating-value">No reviews yet! <a target="_blank" href="' + route + '#review"><span>Be the first</span></a></div>\n' +
                    '        </div>';
            }
        }

        // Mobile marker labels
        // <div class="labels">
        //     <div class="label">
        //         Trending #3 in Tokyo, <span class="gray">&nbsp;Japan</span>
        //     </div>
        //     <div class="label">
        //         Restaurant <span class="green">&nbsp;$$$</span>
        //     </div>
        // </div>

        var trending = '';

        if (isTrending) {
            trending = '<div class="labels" style="display: block!important;">\n' +
                '            <div class="label">\n' +
                trendingLocation +
                '            </div>\n' +
                '        </div>' ;
        }

        if (img.includes('assets2/image/placeholders/place.png')) {
            img = "{{asset('assets2/image/icons/placeholder.png')}}";
        }

        if (tempSrc.includes('assets2/image/placeholders/place.png')) {
            tempSrc = "{{asset('assets2/image/icons/placeholder.png')}}";
        }

        return '<div id="content" class="marker-info added">' +
            '<div class="image">' +
            '<img src="'+ img +'" temp-src="' + tempSrc + '">' +
                trending +
            '<div class="name">' +
            '<a target="_blank"  href="' + route + '">'+ name +'</a>' +
            '<span>'+ address +'</span>' +
            '</div>' +
            '</div>' + reviews +
            '<div class="add">' +
            '           <button data-date="'+ time +'" data-city="' + cityName + '" id="fly-to">' +
            'FLY TO<img src="' + "{{asset('trip_assets/images/link.svg')}}" + '">' +
            '           </button>' +
            '           <button data-name="' + name + '" data-date="' + time + '" data-city="' + cityName + '" id="prices">' +
            '               PRICES<img src="' + "{{asset('trip_assets/images/link.svg')}}" + '">' +
            '           </button>' +
            '   </div>' +
            add +
            checkins +
            '</div>';
    }

    function clearSuggestions() {
        suggestedMarkers.forEach(function (m) {
            m.remove();
        });

        suggestedMarkers = [];
    }

    function clearTopSuggestions() {
        topSuggestedMarkers.forEach(function (m) {
            m.remove();
        });

        topSuggestedMarkers = [];
    }

    function clearNearbyMarkers() {
        nearbyMarkers.forEach(function (m) {
            m.remove();
        });

        nearbyMarkers = [];
    }

    function clearTrendingMarkers() {
        trendingMarkers.forEach(function (m) {
            m.remove();
        });

        trendingMarkers = [];
        trendingCoordinates = [];
    }

    function initMap() {
        mapboxgl.accessToken = '{{config('mapbox.token')}}';
        mapboxgl.maxParallelImageRequests = 1;
        mapboxgl.setRTLTextPlugin(
            'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.3/mapbox-gl-rtl-text.js',
            null,
            true // Lazy load the plugin
        );

        var map = new mapboxgl.Map({
            container: 'maps',
            style: 'mapbox://styles/alex-sh/ckhxt4xlv15o819mljgd7uscn',
            maxBounds: mapBounds
        });

        map.dragRotate.disable();
        map.touchZoomRotate.disableRotation();

        map.on('click', function(e) {
            if (map.getZoom() < 13 || !e.originalEvent.isTrusted) {
                return;
            }

            var pixelDistance = getDistanceInOnePixel();
            var issetPlace = false;

            placesCoordinates.concat(nearbyMarkersCoordinates).concat(searchMarkersCoordinates).forEach(function(coord, i) {
                var line = turf.lineString([coord, [e.lngLat.lng, e.lngLat.lat]]);
                var length = turf.length(line, {units: 'kilometers'});

                if (length <= (pixelDistance * 50)) {
                    issetPlace = true;
                    return;
                }
            });

            if (issetPlace) {
                return;
            }
        });

        return map;
    }

    function generateTripPlacesRelations()
    {
        for (var i = 0; i < trip_places_arr.length; i++) {
            var place = trip_places_arr[i];

            var relationKey = '1_' + place.cities_id;
            if (relations[relationKey] === undefined) {
                relations[relationKey] = [];
            }
            relations[relationKey].push(place.trip_place_id.toString());

            relationKey = '2_' + place.countries_id;
            if (relations[relationKey] === undefined) {
                relations[relationKey] = [];
            }
            relations[relationKey].push(place.trip_place_id.toString());
        }
    }

    var duplicates = [];

    function addTrendingsToPlaceCoords(onlyTrending = false, forceFit = false)
    {
        isSuggestedPlaces = false;
        // savedLines = [];
        // savedLength = [];
        relations = [];
        trendingCoordinates = [];
        duplicates = [];
        showOnlyChildren = null;
        var isPlanPlaces = false;

        for (var i in trendingMarkers) {
            $(trendingMarkers[i].getElement()).find('div.cluster-count').remove();
            trendingMarkers[i].remove();
            delete trendingMarkers[i];
        }

        trip_places_arr = [...saved_trip_places_arr];

        if (trip_places_arr.length) {
            isPlanPlaces = true;
        }

        var tripPlacesMapping = [];

        saved_trip_places_arr.map(function(item) {
            tripPlacesMapping[item.id] = item;
        });

        generateTripPlacesRelations();
        for (const [key, dataFlat] of Object.entries(allTrendingBounds)) {
            if (duplicates[dataFlat.fullData.location_type + '_' + dataFlat.fullData.location.id] !== undefined
                || (dataFlat.fullData.location_type === 0 && tripPlacesMapping[dataFlat.fullData.location.id] !== undefined)
            ) {
                continue;
            }

            duplicates[dataFlat.fullData.location_type + '_' + dataFlat.fullData.location.id] = dataFlat;

            generateTrendingMarker(dataFlat.fullData);

            dataFlat.fullData.marker_type = trendingMarkerType;

            if (dataFlat.type) {
                trip_places_arr.unshift(dataFlat.fullData);
            } else {
                trip_places_arr.push(dataFlat.fullData);
            }

            if (dataFlat.fullData.parentId !== undefined) {
                var parentType = dataFlat.fullData.location_type + 1;
                var relationKey = parentType + '_' + dataFlat.fullData.parentId;
                if (relations[relationKey] === undefined) {
                    relations[relationKey] = [];
                }
                relations[relationKey].push(dataFlat.fullData.location_type + '_' + dataFlat.fullData.location.id);
            }

            if (dataFlat.fullData.parent2Id !== undefined) {
                var parentType = dataFlat.fullData.location_type + 2;
                var relationKey = parentType + '_' + dataFlat.fullData.parent2Id;
                if (relations[relationKey] === undefined) {
                    relations[relationKey] = [];
                }
                relations[relationKey].push(dataFlat.fullData.location_type + '_' + dataFlat.fullData.location.id);
            }
        }

        renderPlaceMarkers(true);

        var source = map.getSource('route0');

        if (source) {
            if (tempCoords !== null) {
                sortCoords(beforePoint, tempCoords, 0, 1, 0, 1);
            }

            if (JSON.stringify(beforeCoordinates) !== JSON.stringify(coordinates)) {
                setRouteSources();
            }
        }

        if (forceFit) {
            fitBounds(15, false, true);
        }
    }

    function onPopupOpen(e) {
        var popup = e.target;
        var element = popup.getElement();
        var height = element.offsetHeight;
        var width = element.offsetWidth;
        var anchorComponents = [];

        if (popup._pos.y < height && map.transform.height < (popup._pos.y + height)) {
            anchorComponents = ['bottom'];
        } else {
            popup.options.anchor = '';
            popup._update();

            return;
        }

        if (popup._pos.x < width / 2) {
            anchorComponents.push('left');
        } else if (popup._pos.x > map.transform.width - width / 2) {
            anchorComponents.push('right');
        }

        popup.options.anchor = anchorComponents.join('-');
        popup._update()
    }

    function placeMarkerClick(e) {
        e.stopImmediatePropagation();
        e.stopPropagation();
        var clusterId = $(this).attr('data-clusterid');
        var placeId = $(this).attr('data-place');

        if (clusterId === undefined) {
            scrollSideBar(placeId);
            if (editable && !convertedToMemory) {
                updateTrendingData(0, $(this).attr('data-placeorigid'), 1, [], true);
            } else {
                moveToMarker(placeId)
            }

            //triggerPlaceChatsWindow(place.id);
            //updateTrendingData(0, place.id);
        } else {
            map.fitBounds(clustersBounds[clusterId], {
                padding: {top: 120, bottom:50, left: 50, right: 50}
            });
        }
    }

    function placeMarkerMouseEnter(e) {
        e.stopImmediatePropagation();
        e.stopPropagation();
        var placeId = $(this).attr('data-placeorigid');
        addPopupToMap(popups[placeId]);

        var popupEl = popups[placeId].getElement();

        var locationType = $(popupEl).find('div.checkins').attr('data-locationtype');
        $(popupEl).find('div.checkins').html('<span style="width: 100px; height: 15px;" class="animation"></span>');

        if (visitsThumbsReq) {
            visitsThumbsReq.abort();
        }

        visitsThumbsReq = $.ajax({
            method: "GET",
            url: "{{url('visits/thumbs')}}/" + placeId,
            data: {'location-type': 0}
        })
            .done(function (res) {
                if (res.status !== 'success') {
                    return;
                }

                $(popupEl).find('div.checkins').replaceWith(generateVistisBlock(placeId, 0, res.data));

            }).fail(function(data) {
                if (data.status == 403) {
                    location.reload();
                }
            });
    }

    function placeMarkerMouseLeave(e) {
        var placeId = $(this).attr('data-placeorigid');
        if (!$(popups[placeId].getElement()).is(":hover")) {
            popups[placeId].remove();
        } else {
            $(popups[placeId].getElement())[0].addEventListener('mouseleave', function (e) {
                popups[placeId].remove();
            });
        }
    }

    function renderPlaceMarkers(force = false) {
        if (force) {
            //savedLines = [];
            //savedLength = [];
        }

        placesCoordinates = [];
        placesCoordinatesFlat = [];
        placesMapping = [];

        clusters = [];
        markersClusters = [];

        var existPlaces = [];
        var lng = undefined;
        var lat = undefined;
        for (var i = 0; i < trip_places_arr.length; i++) {
            var place = trip_places_arr[i];

            if (place.marker_type === trendingMarkerType) {
                var mixId = place.location_type + '_' + place.location.id;
                lng = parseFloat(place.location.lng);
                lat = parseFloat(place.location.lat);
                placesCoordinates[mixId] = [lng, lat];
                placesCoordinatesFlat.push({
                   lnglat: [lng, lat],
                   id: mixId,
                   type: place.location_type,
                   flat_id: place.location.id
                });

                place.location.lng = lng;
                place.location.lat = lat;

                placesMapping[mixId] = place;
                continue;
            }

            var coeff = 0;

            if (existPlaces[place.id]) {
                coeff = existPlaces[place.id].coeff;
            } else {
                existPlaces[place.id] = {
                    coeff: 0,
                    indexes: []
                };
            }

            existPlaces[place.id].coeff++;
            existPlaces[place.id].indexes.push(place.trip_place_id);

            var placeCoordinates = [parseFloat(place.lng) + (0.0004 * coeff), parseFloat(place.lat)];
            placesCoordinates[place.trip_place_id] = placeCoordinates;
            placesCoordinatesFlat.push({
                lnglat: placeCoordinates,
                id: place.trip_place_id,
                type: 0,
                flat_id: place.trip_place_id
            });
        }

        [clusters, markersClusters] = clusterCustomMarkers(placesCoordinates, 0);

        if (!force && markersClusters.length && Object.entries(beforeClusters).sort().toString() === Object.entries(markersClusters).sort().toString()) {
            return;
        }

        beforeClusters = markersClusters;

        bounds = new mapboxgl.LngLatBounds();
        trendingBounds = new mapboxgl.LngLatBounds();
        clustersBounds = [];
        trendingClustersBounds = [];
        renderedTrendingClusters = [];
        invisibleMarkers = [];

        locationsLevel = [];

        generateLocationsLevel(clusters, markersClusters);

        coordinates = {
            'city': [],
            'country': [],
            'world': []
        };

        renderedClusters = [];

        for (var i in markers) {
            $(markers[i].getElement()).find('div.cluster-count').remove();
            markers[i].remove();
            //markers[i].getElement().removeEventListener('click', placeMarkerClick);
        }

        for (var i in trendingMarkers) {
            $(trendingMarkers[i].getElement()).find('div.cluster-count').remove();
            trendingMarkers[i].remove();
        }

        beforeCity = null;
        beforeCountry = null;
        beforePoint = null;

        for (var i = 0; i < trip_places_arr.length; i++) {
            var place = trip_places_arr[i];

            if (place.marker_type === trendingMarkerType) {
                var placeKey = place.location_type + '_' + place.location.id;
                handleTrendingMarker(placeKey, clusters, markersClusters);

                continue;
            }

            var placeCoordinates = placesCoordinates[place.trip_place_id];
            var cluster = markersClusters[place.trip_place_id];
            var markerClass = 'place-marker marker';

            if (cluster !== undefined) {
                var clusterBounds = new mapboxgl.LngLatBounds();

                clusters[cluster].forEach(function(placeId, index) {
                    clusterBounds.extend(placesCoordinates[placeId]);
                    bounds.extend(placesCoordinates[placeId]);
                });

                clustersBounds[cluster] = clusterBounds;
                placeCoordinates = [clusterBounds.getCenter().lng, clusterBounds.getCenter().lat];

                if (renderedClusters[cluster] !== undefined) {
                    if (trendingCoordinates[renderedClusters[cluster]] !== undefined) {
                        //placeCoordinates = trendingCoordinates[renderedClusters[cluster]];
                        if (locationsLevel[renderedClusters[cluster]]) {
                            var clusterMarkerLngLat = trendingMarkers[renderedClusters[cluster]].getLngLat();
                        } else if (placesMapping[renderedClusters[cluster]].parentId !== undefined && locationsLevel[placesMapping[renderedClusters[cluster]].parentId]) {
                            var clusterMarkerLngLat = trendingMarkers[placesMapping[renderedClusters[cluster]].parentId].getLngLat();
                        } else if (placesMapping[renderedClusters[cluster]].parent2Id !== undefined && locationsLevel[placesMapping[renderedClusters[cluster]].parent2Id]) {
                            var clusterMarkerLngLat = trendingMarkers[placesMapping[renderedClusters[cluster]].parent2Id].getLngLat();
                        }

                        if (clusterMarkerLngLat !== undefined) {
                            placeCoordinates = [clusterMarkerLngLat.lng, clusterMarkerLngLat.lat];
                        }
                    }

                    if (place.status) {
                        if (beforePoint) {
                            sortCoords(beforePoint, placeCoordinates, beforeCity, place.cities_id, beforeCountry, place.countries_id);
                        }

                        beforeCity = place.cities_id;
                        beforeCountry = place.countries_id;
                        beforePoint = placeCoordinates;
                    }
                    continue;
                }

                if (locationsLevel[place.trip_place_id]) {
                    renderedClusters[cluster] = place.trip_place_id;
                }

                markerClass = 'cluster-marker marker';
            } else {
                bounds.extend(placeCoordinates);
            }

            if (locationsLevel[place.trip_place_id]) {
                var marker = markers[place.trip_place_id];

                if (!marker) {
                    var el = $(document.createElement('div'));
                    el[0].addEventListener('click', placeMarkerClick);
                    $(el).addClass(markerClass);

                    var style = 'background: url(\' ' + place.thumb + '\');' +
                        'background-size: cover;' +
                        'background-repeat: no-repeat;' +
                        'background-position: center center;' +
                        'width: 41px;' +
                        'height: 41px;' +
                        'border-radius: 50px;' +
                        'border: 2px solid #4080ff !important;' +
                        'box-shadow: inset 0 0 0 2px #fff;' +
                        'cursor: pointer;' +
                        'z-index: 1;';

                    $(el).attr('style', style);
                    $(el).attr('data-place', place.trip_place_id);
                    $(el).attr('data-placeorigid', place.id);
                } else {
                    var el = $(marker.getElement())[0];
                }

                if (activeMarker == place.trip_place_id) {
                    $(el).addClass('active');
                }

                if (cluster !== undefined) {
                    var count = 0;

                    clusters[cluster].forEach(function (i, index) {
                        if (getLocationType(place.trip_place_id) === getLocationType(i)) {
                            count++;
                        }
                    });

                    if (count > 1) {
                        $(el).append(generateCountEl(count));
                    }
                    $(el).attr('data-clusterid', cluster);
                } else {
                    $(el).removeAttr('data-clusterid');
                }

                if (marker === undefined) {
                    marker = new mapboxgl.Marker($(el)[0]);
                    markers[place.trip_place_id] = marker;
                }

                marker.setLngLat(placeCoordinates);

                if (map.getBounds().contains(marker.getLngLat())) {
                    marker.addTo(map);
                } else {
                    invisibleMarkers.push({
                        lnglat: marker.getLngLat(),
                        type: 1,
                        data: marker
                    });
                }

                marker.getElement().removeEventListener('touchend', placeMarkerMouseEnter);
                marker.getElement().removeEventListener('mouseenter', placeMarkerMouseEnter);
                marker.getElement().removeEventListener('mouseleave', placeMarkerMouseLeave);
                marker.getElement().removeEventListener('touchstart', placeMarkerClick);

                if (cluster === undefined) {
                    place.reviews_avg = place.reviews_avg ? Number(place.reviews_avg).toFixed(1) : 0;

                    if (!popups[place.id]) {
                        var contentString = generatePopup(
                            place.id,
                            //place.image,
                            '',
                            place.name,
                            place.transsingle.address,
                            place.reviews_avg,
                            place.reviews.length,
                            place.time,
                            place.cities_id,
                            place.city_name,
                            place.visits,
                            false,
                            false,
                            null,
                            '',
                            place.image
                        );

                        popups[place.id] = new mapboxgl.Popup()
                            .setHTML(contentString)
                            .setLngLat(placeCoordinates);

                        popups[place.id].on('open', onPopupOpen);
                    }

                    marker.getElement().addEventListener('touchend', placeMarkerMouseEnter);
                    marker.getElement().addEventListener('mouseenter', placeMarkerMouseEnter);
                    marker.getElement().addEventListener('mouseleave', placeMarkerMouseLeave);
                    marker.getElement().addEventListener('touchstart', placeMarkerClick);

                }

                if (place.status) {
                    if (beforePoint) {
                        sortCoords(beforePoint, placeCoordinates, beforeCity, place.cities_id, beforeCountry, place.countries_id);
                    }

                    beforeCity = place.cities_id;
                    beforeCountry = place.countries_id;
                    beforePoint = placeCoordinates;
                }
            } else {
                if (locationsLevel['2_' + place.countries_id]) {
                    placeCoordinates = getLocationCurrentCoords('2_' + place.countries_id);
                } else if (locationsLevel['1_' + place.cities_id]) {
                    placeCoordinates = getLocationCurrentCoords('1_' + place.cities_id);
                }

                if (placeCoordinates === undefined) {
                    continue;
                }

                if (place.status) {
                    if (beforePoint) {
                        sortCoords(beforePoint, placeCoordinates, beforeCity, place.cities_id, beforeCountry, place.countries_id);
                    }

                    beforeCity = place.cities_id;
                    beforeCountry = place.countries_id;
                    beforePoint = placeCoordinates;
                }
            }
        }

        if (existPlaces) {
            for (var i in existPlaces) {
                var ep = existPlaces[i];

                if (ep.coeff > 1) {
                    ep.indexes.forEach(function (mi, epi) {
                        var labelEl = $(document.createElement('div'));
                        $(labelEl).addClass('cluster-count-same');
                        $(labelEl).text('#' + (epi + 1));
                        $('div.marker.place-marker[data-place=' + mi + ']').append(labelEl);
                    });
                }
            }
        }

        if ($('div.place-marker.marker').length) {
            $('div.place-marker.marker').each(function() {
                var attr = $(this).attr('data-placeid');
                if (attr && trendingMarkers['0_' + attr]) {
                    trendingMarkers['0_' + attr].remove();
                }
            });
        }
    }

    function getLocationCurrentCoords(locationId)
    {
        if (markersClusters[locationId] !== undefined) {
            var cluster = markersClusters[locationId];
            if (trendingCoordinates[renderedClusters[cluster]] !== undefined) {
                return trendingCoordinates[renderedClusters[cluster]];
            }

        }

        return trendingCoordinates[locationId];
    }

    function moveToMarker(placeId) {
        var el = $('div.trip-place-wrap[data-placeid="' + placeId + '"]');

        if ($(el).find('.card-inner.disapproved').length) {
            return;
        }

        if ($(el).find('.card.pending').length) {
            return;
        }

        $('div.marker').removeClass('active');
        $('div.trending').removeClass('active');
        activeMarker = null;

        var center = placesCoordinates[placeId];

        $('div.marker[data-place=' + placeId + ']').addClass('active');
        activeMarker = placeId;

        map.flyTo({
            center: center,
            essential: true,
            zoom: 16.5,
            speed: 3
        });

         moveSlider($(el).attr('data-date'), $(el).attr('data-time'));
    }

    function getDistanceInOnePixel() {
        var lat = map.getCenter().lat;
        var zoom = map.getZoom();

        return 40075.016686 * Math.abs(Math.cos(lat * Math.PI/180)) / Math.pow(2, zoom + 9);
    }

    var zoomCounter = 0;
    var beforeZoomLevel = 0;


    map.on('zoom', function (e) {
        var currentZoom = map.getZoom();

        var mod = 50;

        if (currentZoom > 7) {
            mod = 10;
        }

        if (zoomCounter % mod === 0) {
            //addZoomIconsToLabels();
            if (e.originalEvent && currentZoom < cityZoomLevel && !useCountryTempZoom) {
                if (allTrendingBounds.length !== saveAllTrendingBounds.length) {
                    allTrendingBounds = [...saveAllTrendingBounds];
                    tempTrendingBounds = [];
                    addTrendingsToPlaceCoords();
                }
            }

            renderPlaceMarkers();
        }

        var source = map.getSource('route0');

        if (source) {
            if (tempCoords !== null) {
                sortCoords(beforePoint, tempCoords, 0, 1, 0, 1);
            }

            if (JSON.stringify(beforeCoordinates) !== JSON.stringify(coordinates)) {
                setRouteSources(false);
            }
        }


        if (e.originalEvent && beforeZoomLevel > map.getZoom()) {
            useCountryTempZoom = false;
            useCityTempZoom = false;
            useWorldTempZoom = false;
        }
        beforeZoomLevel = map.getZoom();

        zoomCounter++;
    });

    map.on('zoomend', function (e) {
        addZoomIconsToLabels();
        if (e.originalEvent && map.getZoom() < cityZoomLevel && !useCountryTempZoom) {
            if (allTrendingBounds.length !== saveAllTrendingBounds.length) {
                allTrendingBounds = [...saveAllTrendingBounds];
                tempTrendingBounds = [];
                addTrendingsToPlaceCoords();
                isSuggestedPlaces = false;
            }
        }

        renderPlaceMarkers(false);

        if (beforeZoomLevel > map.getZoom()) {
            useCountryTempZoom = false;
            useCityTempZoom = false;
            useWorldTempZoom = false;
            showOnlyChildren = null;
        }

        beforeZoomLevel = map.getZoom();

        var source = map.getSource('route0');

        if (source) {
            if (tempCoords !== null) {
                sortCoords(beforePoint, tempCoords, 0, 1, 0, 1);
            }

            if (JSON.stringify(beforeCoordinates) !== JSON.stringify(coordinates)) {
                setRouteSources();
            }
        }

        showInvisibleMarker();
    });


    function scrollSideBar(id, duration = 400) {
        $('div.trip').animate({
            scrollTop: $('div.trip-place-wrap[data-placeid=' + id + ']').offset().top - $('div.trip-place-wrap:first').offset().top,
            scrollLeft: $('div.trip-place-wrap[data-placeid=' + id + ']').offset().left - $('div.trip-place-wrap:first').offset().left
        }, duration);
    }

    function showInvisibleMarker()
    {
        for (var i in invisibleMarkers) {
            var invisibleMarker = invisibleMarkers[i];
            var e = invisibleMarker.data.getElement();
            var place = placesMapping[$(e).attr('data-type') + '_' + $(e).attr('data-id')];

            var parentId = null;

            if (place && place.parentId) {
                parentId = (place.location_type + 1) + '_' + place.parentId;
            }

            var showable = true;

            if (parentId && showOnlyChildren) {
                showable = parentId === showOnlyChildren;
            }

            if (!map.getBounds().contains(invisibleMarker.lnglat) || !showable) {
                continue;
            }

            addMarkerToMap(invisibleMarker.data);
        }
    }

    var currentMapPlace = null;

    var trendingData = [];
    var saveAllTrendingBounds = [];
    var tempTrendingBounds = [];

    var trendingRequest = null;

    function updateTrendingData(type, parentId, nearby = 0, latlng = [], forceFit = false, with_not_trendings = 0, show_only_children = false) {
        if (!editable || convertedToMemory) {
            return true;
        }

        if (Object.keys(preloadTrends).length && usePreload) {
            usePreload = false;
            trendingData = preloadTrends;
            allTrendingBounds = [];
            generateTrendingBounds(trendingData);
            allTrendingBounds.sort((a, b) => a.type < b.type ? -1 : (a.type > b.type ? 1 : 0));

            var onlyTrending = true;

            if (type === undefined && parentId === undefined) {
                saveAllTrendingBounds = [...allTrendingBounds];
                onlyTrending = false;
            }

            addTrendingsToPlaceCoords(onlyTrending);
            return;
        }

        var except = $('.trip-place-wrap').map(function(){return $(this).attr('data-origplaceid');}).get();

        if (!except) {
            except = [];
        }

        // if (tempTrendingBounds.length) {
        //     except = except.concat(tempTrendingBounds.map(function(e) {if (e.parent2Id !== undefined) {return e.id}}).filter(function(e) {if (e !== undefined) {return e;}}));
        // }

        if (trendingRequest) {
            trendingRequest.abort();
        }

        nearbyBounds = new mapboxgl.LngLatBounds();

        trendingRequest = $.ajax({
            method: "POST",
            url: "{{route('trip.trending_locations')}}",
            //url: "/trending_prod_response.json",
            data: {location: type, parent_id: parentId, except: except, nearby: nearby, latlng: latlng, distance: map.getContainer().clientWidth * getDistanceInOnePixel()/2, with_not_trendings: with_not_trendings}
        })
            .done(function (res) {
                if (!Object.keys(res.data).length) {
                    return;
                }

                trendingData = res.data;
                allTrendingBounds = [];
                generateTrendingBounds(trendingData);
                allTrendingBounds.sort((a, b) => a.type < b.type ? -1 : (a.type > b.type ? 1 : 0));

                if (tempTrendingBounds.length) {
                    allTrendingBounds = [...allTrendingBounds, ...tempTrendingBounds];
                } else {
                    allTrendingBounds = [...allTrendingBounds, ...saveAllTrendingBounds];
                }

                tempTrendingBounds = [...allTrendingBounds];

                var onlyTrending = true;

                if (type === undefined && parentId === undefined) {
                    saveAllTrendingBounds = [...allTrendingBounds];
                    onlyTrending = false;
                } else {
                    for (const [i, e] of Object.entries(trendingData)) {
                        nearbyBounds.extend([e.location.lng, e.location.lat]);
                    }
                }

                addTrendingsToPlaceCoords(onlyTrending, forceFit);

                if (show_only_children) {
                    showOnlyChildren = (type + 1) + '_' + parentId;
                }
            });
    }

    function generateTrendingBounds(dataObj, parentId, parent2Id) {
        var dataBounds = new mapboxgl.LngLatBounds();

        for (const [key, data] of Object.entries(dataObj)) {
            var lnglat = [parseFloat(data.location.lng), parseFloat(data.location.lat)];

            if (lnglat[0] != 0 && lnglat[1] != 0) {
                dataBounds.extend(lnglat);
            }

            var locationType = data.location_type;

            if (locationType === 0) {
                parentId = data.location.cities_id;
                parent2Id = data.location.countries_id;
            }

            data.parentId = parentId;
            data.parent2Id = parent2Id;

            if (data['related_locations'] !== undefined) {
                var relatedBounds = generateTrendingBounds(data['related_locations'], data.location.id, parentId);
                if (!relatedBounds.isEmpty()) {
                    lnglat = relatedBounds.getCenter();
                    data.location.lat = lnglat.lat;
                    data.location.lng = lnglat.lng;
                }
            }

            var result = {
                type: locationType,
                id: data.location.id,
                parentId: parentId,
                parent2Id: parent2Id,
                lnglat: lnglat,
                fullData: data
            };

            allTrendingBounds.push(result);
        }

        return dataBounds;
    }

    function generateTrendingMarker(data)
    {
        if (savedTrendingMarkers[data.location_type + '_' + data.location.id] !== undefined) {
            var marker = savedTrendingMarkers[data.location_type + '_' + data.location.id];
            trendingMarkers[data.location_type + '_' + data.location.id] = marker;
            trendingCoordinates[data.location_type + '_' + data.location.id] = [marker.getLngLat().lng, marker.getLngLat().lat];
            return;
        }

        var trendingLabel = '';
        var cityId = 0;
        var cityName = 0;

        var el = $(document.createElement('div'));
        var style = 'width: 30px;' +
            'height: 30px;' +
            'border-radius: 25px;' +
            //'border: 2px solid #fff !important;' +
            'cursor: pointer;';

        var img = $(document.createElement('img'));
        $(img).attr('temp-src', data.thumb_src);
        img[0].onerror = function(){
            this.src = this.src.replace('/th180', '');
        };
        $(img).attr('style', style);
        $(img).addClass('marker-image');

        var cover = undefined;

        if (data.location_type == '{{\App\Services\Trends\TrendsService::LOCATION_TYPE_COUNTRY}}' || data.location_type == '{{\App\Services\Trends\TrendsService::LOCATION_TYPE_CITY}}') {
            style = 'border-radius: 25px;' +
                'border: 2px solid #ff9b34 !important;' +
                'cursor: pointer;';

            var iconEl = $(document.createElement('img'));
            $(iconEl).attr('src', '{{asset('assets2/image/icons/trending-icon.png')}}');
            $(iconEl).addClass('trending-marker-icon');

            if (data.type !== 2) {
                $(el).append(iconEl);
                trendingLabel = data.local ? 'Trending in ' + data.local : 'Trending worldwide';
            } else {
                trendingLabel = '';
            }
        } else if (data.location_type == '{{\App\Services\Trends\TrendsService::LOCATION_TYPE_PLACE}}'
            && (data.place_type == '{{\App\Services\Trends\TrendsService::PLACE_TYPE_TRENDING}}'
                || data.place_type == '{{\App\Services\Trends\TrendsService::PLACE_TYPE_SUGGESTED}}')) {
            style = 'border-radius: 25px;' +
                'cursor: pointer;';
            trendingLabel = data.local ? 'Trending in ' + data.local : 'Trending worldwide';

            $(iconEl).addClass('trending-marker-icon');

            $(img).attr('category', data.place_category);

            switch (data.place_category) {
                case 'R':
                    cover = $(document.createElement('img'));
                    $(cover).attr('src', '{{asset('assets2/image/icons/rests-icon.png')}}');
                    $(cover).addClass('cover');
                    break;
                case 'H':
                    cover = $(document.createElement('img'));
                    $(cover).attr('src', '{{asset('assets2/image/icons/hotels-icon.png')}}');
                    $(cover).addClass('cover');
                    break;
                default:
                    break;
            }

            if (data.place_type == '{{\App\Services\Trends\TrendsService::PLACE_TYPE_SUGGESTED}}') {
                trendingLabel = 'Suggested by Travooo';
                var iconEl = $(document.createElement('img'));
                $(iconEl).attr('src', '{{asset('assets2/image/icons/trending-place.png')}}');
                $(iconEl).addClass('trending-marker-icon');

                if (data.place_category === 'P') {
                    style += 'border: 2px solid #808080 !important;';
                }

                $(el).addClass('suggested-border');
            } else {
                var iconEl = $(document.createElement('img'));
                $(iconEl).attr('src', '{{asset('assets2/image/icons/trending-icon.png')}}');
                $(iconEl).addClass('trending-marker-icon');

                if (data.place_category === 'P') {
                    style += 'border: 2px solid #ff9b34 !important;';
                }

                $(el).addClass('trending-border');
            }

            $(el).append(iconEl);

            if (data.city) {
                cityId = data.city.id;
                cityName = data.city.trans[0].title;
            } else {
                cityId = 0;
                cityName = '';
            }
        } else if (data.location_type == '{{\App\Services\Trends\TrendsService::LOCATION_TYPE_PLACE}}'
            && (data.place_type == '{{\App\Services\Trends\TrendsService::PLACE_TYPE_FRIENDS}}'
                || data.place_type == '{{\App\Services\Trends\TrendsService::PLACE_TYPE_TRENDING_FRIENDS}}')) {
            style = 'border-radius: 25px;' +
                'border: 2px solid #32c984 !important;' +
                'cursor: pointer;';

            var iconEl = $(document.createElement('img'));
            $(iconEl).attr('src', '{{asset('assets2/image/icons/friends-trending-place.svg')}}');
            $(iconEl).addClass('trending-marker-icon');

            $(el).append(iconEl);
            trendingLabel = 'Popular in your network';
            if (data.city) {
                cityId = data.city.id;
                cityName = data.city.trans[0].title;
            } else {
                cityId = 0;
                cityName = '';
            }
        }

        $(el).addClass('trending');
        $(el).attr('style', style);
        $(el).attr('data-id', data.location.id);
        $(el).attr('data-type', data.location_type);

        if (cover !== undefined) {
            $(el).prepend(cover);
        }

        $(el).append(img);

        var lnglat = [parseFloat(data.location.lng), parseFloat(data.location.lat)];
        var marker = new mapboxgl.Marker($(el)[0])
            .setLngLat(lnglat);


        savedTrendingMarkers[data.location_type + '_' + data.location.id] = marker;
        trendingMarkers[data.location_type + '_' + data.location.id] = marker;
        trendingCoordinates[data.location_type + '_' + data.location.id] = lnglat;

        var title = data.location.trans[0] === undefined ? '' : data.location.trans[0].title;

        var contentString = generatePopup(
            data.location.id,
            //data.src,
            '',
            title,
            '',
            0,
            0,
            0,
            cityId,
            cityName,
            data.visits,
            true,
            true,
            data.location_type,
            trendingLabel,
            data.src,
            data.city_img
        );

        if (!trendingPopups[data.location_type + '_' + data.location.id]) {

            var options = {};

            if ($('.mobile-map-view-controls:visible').length) {
             //   options = {closeOnClick: false};
            }

            trendingPopups[data.location_type + '_' + data.location.id] = new mapboxgl.Popup(options)
                .setHTML(contentString)
                .setLngLat(lnglat);

            trendingPopups[data.location_type + '_' + data.location.id].on('open', onPopupOpen);
        }

        $(el)[0].addEventListener('mouseenter', trendingMouseEnter);
        $(el)[0].addEventListener('mouseleave', trendingMouseLeave);
    }

    var visitsThumbsReq = null;

    function trendingMouseEnter(e)
    {
        e.preventDefault();
        e.stopPropagation();
        var parentId = $(this).attr('data-id');
        var locationId = $(this).attr('data-type') + '_' + $(this).attr('data-id');

        if (trendingMarkersClusters[parentId] !== undefined) {
            return;
        }

        if ($(this).attr('data-type') == 0 && parentId != activeMarker) {
            $(this).addClass('active');
        }

        addPopupToMap(trendingPopups[locationId]);
        var popupEl = trendingPopups[locationId].getElement();

        var locationType = $(popupEl).find('div.checkins').attr('data-locationtype');
        $(popupEl).find('div.checkins').html('<span style="width: 100px; height: 15px;" class="animation"></span>');

        if (visitsThumbsReq) {
            visitsThumbsReq.abort();
        }

        visitsThumbsReq = $.ajax({
            method: "GET",
            url: "{{url('visits/thumbs')}}/" + parentId,
            data: {'location-type': locationType}
        })
            .done(function (res) {
                if (res.status !== 'success') {
                    return;
                }

                $(popupEl).find('div.checkins').replaceWith(generateVistisBlock(parentId, locationType, res.data));

            }).fail(function(data) {
                if (data.status == 403) {
                    location.reload();
                }
            });
    }

    function trendingMouseLeave(e)
    {
        var parentId = $(this).attr('data-id');
        var locationId = $(this).attr('data-type') + '_' + $(this).attr('data-id');

        if (activeMarker == null || activeMarker != parentId) {
            if ($(this).attr('data-type') == 0) {
                $(this).removeClass('active');
            }
        }

        if (!$(trendingPopups[locationId].getElement()).is(":hover")) {
            trendingPopups[locationId].remove();
        } else {
            $(trendingPopups[locationId].getElement())[0].addEventListener('mouseleave', function (e) {
                trendingPopups[locationId].remove();
            });
        }
    }

    var nearblyPlacesRequest = null;

    map.on('idle',function(e) {
        showInvisibleMarker();
    });


    var moveCounter = 0;

    map.on('move',function(e) {
        if (e.originalEvent) {
            showInvisibleMarker();
            if (moveCounter % 30 === 0) {
                renderPlaceMarkers(false);
            }

            moveCounter++;
        }
    });

    var beforeRadius = 0;
    var beforeGlobalRadius = 0;
    var lastGlobalCenter = null;

    map.on('moveend',function(e) {
        addZoomIconsToLabels();
        if ((e.originalEvent || forceNearby) && map.getZoom() > cityZoomLevel) {
            forceNearby = false;
            var radius = map.getContainer().clientWidth * getDistanceInOnePixel()/2;

            var center = map.getBounds().getCenter();

            var length = 99999;

            if (lastNearbyCenter !== null) {
                var line = turf.lineString([[lastNearbyCenter.lng, lastNearbyCenter.lat], [center.lng, center.lat]]);
                length = turf.length(line, {units: 'kilometers'});
            }

            if ((length > (radius/3) && !isSuggestedPlaces) || length > 15 || (radius - beforeRadius > radius/3)) {
                updateTrendingData(0, 0, 1, [center.lat, center.lng]);
                lastNearbyCenter = center;
                beforeRadius = radius;
            }
        }

        var center = map.getBounds().getCenter();
        var length = 0;
        var radius = map.getContainer().clientWidth * getDistanceInOnePixel()/2;

        if (lastGlobalCenter !== null) {
            var line = turf.lineString([[lastGlobalCenter.lng, lastGlobalCenter.lat], [center.lng, center.lat]]);
            length = turf.length(line, {units: 'kilometers'});
        } else {
            lastGlobalCenter = center;
        }

        if (e.originalEvent && (length > radius/1.5)) {
            showOnlyChildren = null;
            lastGlobalCenter = center;
            beforeGlobalRadius = radius;
        }

        renderPlaceMarkers(false);

    });

    map.on('click',function() {
        $('div.map-placeholder-block').hide();
    });

    map.on('drag',function() {
        $('div.map-placeholder-block').hide();
    });

    map.on('mousemove', function () {
        $('div.map-placeholder-block').hide();
    });

    $('div#maps').on('mouseleave', function () {
        if (tempCoords === null && $('#modal-add_place').is(':visible') && showHint) {
            $('div.map-placeholder-block').fadeIn();
        }
    });

    function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2-lat1);  // deg2rad below
        var dLon = deg2rad(lon2-lon1);
        var a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c; // Distance in km
        return d;
    }

    function deg2rad(deg) {
        return deg * (Math.PI/180)
    }

    function clusterCustomMarkers(placesCoordinates, type) {
        var clusters = [];
        var markersClusters = [];
        var pixelDistance = getDistanceInOnePixel();
        var minPixelDistance = pixelDistance * 41;

        var marker = undefined;
        var id1 = undefined;
        var id2 = undefined;
        var length = undefined;

        var mapBounds = map.getBounds();
        var currentZoom = map.getZoom();

        for (var i = 0; i < placesCoordinatesFlat.length - 1; i++) {
            id1 = placesCoordinatesFlat[i].id;
            var iCoordinates = placesCoordinatesFlat[i].lnglat;
            if (!mapBounds.contains(iCoordinates) || (!useCityTempZoom && currentZoom < cityZoomLevel && allTrendingBounds.length && placesCoordinatesFlat[i].type === 0)) {
                continue;
            }
            for (var k = i + 1; k < placesCoordinatesFlat.length; k++) {
                id2 = placesCoordinatesFlat[k].id;
                var kCoordinates = placesCoordinatesFlat[k].lnglat;

                if (!mapBounds.contains(kCoordinates) || (!useCityTempZoom && currentZoom < cityZoomLevel && allTrendingBounds.length && placesCoordinatesFlat[k].type === 0)) {
                    continue;
                }

                if (markersClusters[k] !== undefined || (markersClusters[i] !== undefined && markersClusters[i] === markersClusters[k])) {
                    continue;
                }

                length = getDistanceFromLatLonInKm(iCoordinates[1], iCoordinates[0], kCoordinates[1], kCoordinates[0]);
                // cacheKey1 = id1 + id2;
                // cacheKey2 = id2 + id1;
                //
                // if (savedLength[cacheKey1] === undefined) {
                //     length = getDistanceFromLatLonInKm(iCoordinates[1], iCoordinates[0], kCoordinates[1], kCoordinates[0]);
                //     savedLength[cacheKey1] = length;
                //     savedLength[cacheKey2] = length;
                // } else {
                //     length = savedLength[cacheKey1];
                // }

                if (length <= minPixelDistance) {
                    if (markersClusters[i] === undefined && markersClusters[k] === undefined) {
                        clusters.push([i, k]);

                        markersClusters[i] = clusters.length - 1;
                        markersClusters[k] = clusters.length - 1;
                    } else if (markersClusters[i] !== undefined) {
                        if (markersClusters[k] === undefined) {
                            clusters[markersClusters[i]].push(k);
                            markersClusters[k] = markersClusters[i];
                        } else {
                            var clusterToRemove = markersClusters[k];
                            for (var l = 0; l < clusters[clusterToRemove].length; l++) {
                                if (clusters[clusterToRemove][l] === undefined) {
                                    continue;
                                }
                                marker = clusters[clusterToRemove][l];
                                markersClusters[marker] = markersClusters[i];
                                clusters[markersClusters[i]].push(marker);
                            }

                            clusters[clusterToRemove] = undefined;
                        }
                    } else {
                        clusters[markersClusters[k]].push(i);
                        markersClusters[i] = markersClusters[k];
                    }
                }
            }
        }

        var formattedClusters = [];
        var formattedMarkersCluster = [];

        for (i = 0; i < markersClusters.length; i++) {
            var cluster = markersClusters[i];
            if (cluster === undefined) {
                continue;
            }
            var id = placesCoordinatesFlat[i].id;
            formattedMarkersCluster[id] = cluster;
            if (formattedClusters[cluster] === undefined) {
                formattedClusters[cluster] = [];
            }

            formattedClusters[cluster].push(id.toString());
        }

        return [formattedClusters, formattedMarkersCluster];
    }

    function listenClusterClick(e)
    {
        e.stopPropagation();
        var clusterId = $(this).attr('data-clusterid');
        var clusterBounds = new mapboxgl.LngLatBounds();
        var locationId = $(this).attr('data-type') + '_' + $(this).attr('data-id');

        if (clusterId && $(this).find('.cluster-count').length) {
            clusterBounds = trendingClustersBounds[clusterId];

            if (getLocationType(locationId) === '2') {
                useWorldTempZoom = true;
            } else if (getLocationType(locationId) === '1') {
                useCountryTempZoom = true;
            }
        } else {

            if (relations[locationId]) {
                relations[locationId].forEach(function(locationId, index) {
                    clusterBounds.extend(placesCoordinates[locationId]);
                });
            }

            if (getLocationType(locationId) === '2') {
                useCountryTempZoom = true;
            } else if (getLocationType(locationId) === '1') {
                useCityTempZoom = true;
            } else {
                updateTrendingData(0, $(this).attr('data-id'), 1, [], true);

                if (activeMarker !== $(this).attr('data-id')) {
                    $('div.marker').removeClass('active');
                    $('div.trending').removeClass('active');
                    activeMarker = $(this).attr('data-id');
                    $(this).addClass('active');
                }

                return;
            }
        }

        if (!clusterBounds.isEmpty()) {
            if (!$(this).find('.cluster-count').length) {
                showOnlyChildren = locationId;
            } else {
                showOnlyChildren = null;
            }

            map.fitBounds(clusterBounds, {
                padding: {top: 100, bottom: 50, left: 50, right: 50}
            });
        } else if (getLocationType(locationId) === '1') {
            map.flyTo({
                center: [placesMapping[locationId].location.lng, placesMapping[locationId].location.lat],
                essential: true,
                zoom: cityZoomLevel,
                speed: 3
            });

            updateTrendingData(0, $(this).attr('data-id'));
        }
    }

    var locationsLevel = [];

    function getLocationType(id) {
        var type = '0';

        if (id.toString().includes('_')) {
            type = id.split('_')[0];
        }

        return type;
    }

    function generateLocationsLevel(clusters, markersClusters)
    {
        var currentZoom = map.getZoom();

        var sortedLocations = [...trip_places_arr];
        sortedLocations.sort((a, b) => ((a.location_type < b.location_type) || (a.location_type === undefined)) ? -1 : (a.location_type > b.location_type ? 1 : 0));

        for (var i in sortedLocations) {
            var location = sortedLocations[i];
            var type = location.location_type;
            if (type === undefined) {
                type = 0;
                var locationId = location.trip_place_id;
                var parentId = (type + 1) + '_' + location.cities_id;
                var parent2Id = (type + 2) + '_' + location.countries_id;
            } else {
                var locationId = type + '_' + location.location.id;
                var parentId = (type + 1) + '_' + location.parentId;
                var parent2Id = (type + 2) + '_' + location.parent2Id;
            }

            var cluster = markersClusters[locationId];

            var notInCluster = cluster === undefined;
            var clusterHasOnlyParentChildren = false;
            var allChildrenInOneCluster = false;
            var allDirectChildren = [];

            if (relations[locationId] !== undefined) {
                var oneCluster = undefined;
                allChildrenInOneCluster = relations[locationId].every(function(i) {
                    var childType = getLocationType(i);

                    if (type - childType > 1) {
                        return true;
                    }

                    var childCluster = markersClusters[i];

                    if (childCluster === undefined) {
                        return false;
                    }

                    if (oneCluster === undefined) {
                        oneCluster = childCluster;
                    }

                    return childCluster === oneCluster;
                });

                relations[locationId].every(function(i) {
                    var childType = getLocationType(i);

                    if (type - childType === 1) {
                        allDirectChildren.push(i);
                    }

                    return true;
                });
            }
            //is suggested/trending place or city
            if (type !== 2) {
                if (!notInCluster) {
                    clusterHasOnlyParentChildren = clusters[cluster].every(i =>
                        i === locationId
                        || i === parentId
                        || i === parent2Id
                        || (relations[parentId] && relations[parentId].includes(i))
                        || (parent2Id !== undefined && relations[parent2Id] && relations[parent2Id].includes(i))
                        || trendingCoordinates[parentId] === undefined
                    );
                }

                if (type === 0) {
                    var isShouldShow = notInCluster || clusterHasOnlyParentChildren;

                    if (!useCityTempZoom && currentZoom < cityZoomLevel && allTrendingBounds.length) {
                        isShouldShow = false;
                    } else if (useCityTempZoom && allTrendingBounds.length) {
                        isShouldShow = true;
                    }

                } else {
                    var locationLevel = locationsLevel[locationId] === undefined || locationsLevel[locationId];

                    var isShouldShow = locationLevel && (notInCluster || clusterHasOnlyParentChildren);

                    if (!isShouldShow && allChildrenInOneCluster && allDirectChildren.length !== 1) {
                        isShouldShow = true;
                    }

                    if (useCityTempZoom || currentZoom >= cityZoomLevel || (!useCountryTempZoom && currentZoom < countryZoomLevel)) {
                        isShouldShow = false;
                    }

                    if (useCountryTempZoom && !useCityTempZoom) {
                        isShouldShow = true;
                    }

                    if (isShouldShow) {
                        for (var i in relations[locationId]) {
                            locationsLevel[relations[locationId][i]] = !isShouldShow;
                        }
                    }
                }

                locationsLevel[locationId] = isShouldShow;

                var shouldShowParent = !isShouldShow;

                if (type === 1 && relations[locationId] && shouldShowParent) {
                    shouldShowParent = !relations[locationId].every(i => locationsLevel[i] === true);
                }

                if (locationsLevel[parentId] === undefined || (!locationsLevel[parentId] && shouldShowParent)) {
                    locationsLevel[parentId] = shouldShowParent;
                }
            } else {
                var isShouldShow = locationsLevel[locationId] === undefined || locationsLevel[locationId];

                if (!isShouldShow && allChildrenInOneCluster && allDirectChildren.length !== 1) {
                    isShouldShow = true;
                }

                if (currentZoom >= countryZoomLevel) {
                    isShouldShow = false;
                }

                if (useWorldTempZoom && !useCountryTempZoom && !useCityTempZoom) {
                    isShouldShow = true;
                }

                if (isShouldShow) {
                    for (var i in relations[locationId]) {
                        locationsLevel[relations[locationId][i]] = !isShouldShow;
                    }
                }

                locationsLevel[locationId] = isShouldShow;
            }
        }
    }

    var clusterImages = [
        '{{asset('assets2/image/icons/hotel-and-place-and-restaurant.png')}}',
        '{{asset('assets2/image/icons/place-and-restaurant.png')}}',
        '{{asset('assets2/image/icons/hotel-and-place.png')}}',
        '{{asset('assets2/image/icons/hotel-and-restaurant.png')}}'
    ];

    function handleTrendingMarker(locationId, trendingClusters, trendingMarkersClusters)
    {
        var marker = trendingMarkers[locationId];

        if (trendingPopups[locationId] !== undefined) {
            if (!mobileControlsVisible) {
                trendingPopups[locationId].remove();
            }
        }

        var cluster = trendingMarkersClusters[locationId];

        var markerEl = marker.getElement();

        markerEl.removeEventListener('mouseenter', trendingMouseEnter);
        markerEl.removeEventListener('mouseleave', trendingMouseLeave);
        markerEl.removeEventListener('touchend', trendingMouseEnter);
        markerEl.removeEventListener('click', listenClusterClick);
        markerEl.removeEventListener('touchend', listenClusterClick);

        var clusterBounds = new mapboxgl.LngLatBounds();

        if (cluster !== undefined) {
            if (!locationsLevel[locationId]) {
                return;
            }

            if (renderedClusters[cluster] !== undefined) {
                bounds.extend(trendingCoordinates[locationId]);
                trendingBounds.extend(trendingCoordinates[locationId]);
                return;
            }

            var count = 0;

            var hasPlace = false;
            var hasRest = false;
            var hasHotel = false;

            trendingClusters[cluster].forEach(function(i, index) {
                var type = getLocationType(i);
                if (getLocationType(locationId) === type) {
                    count++;
                }

                var place = placesMapping[i];
                if (type === '0' && locationsLevel[i]) {

                    if (place !== undefined) {
                        switch (place.place_category) {
                            case 'P':
                                hasPlace = true;
                                break;
                            case 'H':
                                hasHotel = true;
                                break;
                            case 'R':
                                hasRest = true;
                                break;
                            default:
                                hasPlace = true;
                                break;
                        }
                    } else {
                        hasPlace = true;
                    }
                }

            });

            if (count > 1) {
                $(markerEl).append(generateCountEl(count));

                trendingClusters[cluster].forEach(function(locationId, index) {
                    clusterBounds.extend(placesCoordinates[locationId]);
                });
            } else {
                if (relations[locationId]) {
                    relations[locationId].forEach(function(locationId, index) {
                        clusterBounds.extend(placesCoordinates[locationId]);
                    });
                }

                markerEl.addEventListener('touchend', trendingMouseEnter);
                markerEl.addEventListener('mouseenter', trendingMouseEnter);
                markerEl.addEventListener('mouseleave', trendingMouseLeave);
            }

            renderedClusters[cluster] = locationId;
            trendingClustersBounds[cluster] = clusterBounds;

            $(markerEl).attr('data-clusterid', cluster);

            markerEl.addEventListener('click', listenClusterClick);
            markerEl.addEventListener('touchend', listenClusterClick);

            var locationType = getLocationType(locationId);

            if (!clusterBounds.isEmpty() && (locationType === '0' || locationType === '1')) {
                marker.setLngLat(clusterBounds.getCenter());
                if (trendingPopups[locationId]) {
                    trendingPopups[locationId].setLngLat(clusterBounds.getCenter());
                }
            }

            var coverImg = '';

            if (hasPlace && hasRest && hasHotel) {
                coverImg = clusterImages[0];
            } else if (hasPlace && hasRest) {
                coverImg = clusterImages[1];
            } else if (hasPlace && hasHotel) {
                coverImg = clusterImages[2];
            } else if (hasRest && hasHotel) {
                coverImg = clusterImages[3];
            }

            $(markerEl).find('img.cover.to-remove').remove();

            if (coverImg && coverImg !== $(markerEl).find('img.cover').attr('src')) {
                if (!$(markerEl).find('img.cover').length) {
                    $(markerEl).prepend('<img class="cover to-remove">');
                }

                var currentImg = $(markerEl).find('img.cover').attr('src');
                $(markerEl).find('img.cover').attr('src', coverImg);

                if (!clusterImages.includes(currentImg)) {
                    $(markerEl).find('img.cover').attr('before-src', currentImg);
                }

                $(markerEl).find('img.cover').addClass('mixed');
            } else if (coverImg !== $(markerEl).find('img.cover').attr('src')
                && $(markerEl).find('img.cover').attr('before-src')
                && $(markerEl).find('img.cover').attr('before-src') !== $(markerEl).find('img.cover').attr('src')
            ) {
                $(markerEl).find('img.cover').attr('src', $(markerEl).find('img.cover').attr('before-src'));
                $(markerEl).find('img.cover').removeAttr('before-src');
                $(markerEl).find('img.cover').removeClass('mixed');
            }

            var place = placesMapping[locationId];

        } else {
            $(markerEl).find('img.cover.to-remove').remove();
            if ($(markerEl).find('img.cover').attr('before-src')
                && $(markerEl).find('img.cover').attr('before-src') !== $(markerEl).find('img.cover').attr('src')
            ) {
                $(markerEl).find('img.cover').attr('src', $(markerEl).find('img.cover').attr('before-src'));
                $(markerEl).find('img.cover').removeAttr('before-src');
                $(markerEl).find('img.cover').removeClass('mixed');
            }

            $(markerEl).removeAttr('data-clusterid');

            if (!locationsLevel[locationId]) {
                return;
            }

            markerEl.addEventListener('touchend', trendingMouseEnter);
            markerEl.addEventListener('mouseenter', trendingMouseEnter);
            markerEl.addEventListener('mouseleave', trendingMouseLeave);
            markerEl.addEventListener('click', listenClusterClick);
            markerEl.addEventListener('touchend', listenClusterClick);

            var place = placesMapping[locationId];
            var lnglat = [place.location.lng, place.location.lat];
            marker.setLngLat(lnglat);
            if (trendingPopups[locationId]) {
                trendingPopups[locationId].setLngLat(lnglat);
            }
        }

        bounds.extend(trendingCoordinates[locationId]);
        trendingBounds.extend(trendingCoordinates[locationId]);

        var parentId = null;

        if (place && place.parentId) {
            parentId = (place.location_type + 1) + '_' + place.parentId;
        }

        var showable = true;

        if (parentId && showOnlyChildren) {
            showable = parentId === showOnlyChildren;
        }

        if (map.getBounds().contains(marker.getLngLat()) && showable) {
            addMarkerToMap(marker);
        } else {
            invisibleMarkers.push({
                lnglat: marker.getLngLat(),
                type: 1,
                data: marker
            });
        }
    }

    function addMarkerToMap(marker) {
        var img = $(marker.getElement()).find('img').last();
        if (!img.attr('src')) {
            img.attr('src', img.attr('temp-src'));
        }
        marker.addTo(map);
    }

    function addPopupToMap(popup) {
        popup.addTo(map);
        var img = $(popup.getElement()).find('div.image img');
        if (!img.attr('src')) {
            img[0].onerror = function(){
                this.src = this.src.replace('/th700', '');
            };
            img.attr('src', img.attr('temp-src'));
        }
    }

    function generateCountEl(count) {
        var countEl = $(document.createElement('div'));
        $(countEl).text(count);
        $(countEl).addClass('cluster-count');

        return countEl;
    }

    var linesCache = [];

    function sortCoords(point1, point2, city1, city2, country1, country2) {
        if (JSON.stringify(point1) === JSON.stringify(point2)) {
            return;
        }

        var area = 'world';

        if (city1 === city2) {
            area = 'city';
        } else if (country1 === country2) {
            area = 'country';
        }

        coordinates[area].push([point1, point2]);
    }

    var paints = {
        'world': {
            'line-color': '#4080ff',
            'line-width': 3,
            'line-dasharray': [1, 1.5]
        },
        'country': {
            'line-color': '#4080ff',
            'line-width': 3,
            'line-dasharray': [0.0001, 1.3]
        },
        'city': {
            'line-color': '#4080ff',
            'line-width': 3
        }
    };

    var beforeCoordinates = null;

    function setRouteSources(zoomEnd = true)
    {
        var layers = map.getStyle().layers;

        for (var layer in layers) {
            var layerKey = layers[layer].id;
            if (layerKey.includes('route')) {
                map.removeLayer(layerKey);
            }
        }

        if (!zoomEnd) {
            beforeCoordinates = [];
            return;
        }

        var routeId = 0;

        for (var area in coordinates) {
            for (var i in coordinates[area]) {
                var route = coordinates[area][i];
                var source = map.getSource('route' + routeId);
                var data = {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': route
                    }
                };

                if (source) {
                    source.setData(data);
                } else {
                    map.addSource('route' + routeId, {
                        'type': 'geojson',
                        'data': data
                    });
                }

                map.addLayer({
                    'id': 'route' + routeId,
                    'type': 'line',
                    'source': 'route' + routeId,
                    'layout': {
                        'line-join': 'round',
                        'line-cap': 'round'
                    },
                    'paint': paints[area]
                });

                routeId++;
            }
        }

        beforeCoordinates = coordinates;
    }

    var labelMarkers = [];

    function addZoomIconsToLabels() {
        for (var i in labelMarkers) {
            labelMarkers[i].remove();
        }

        // var useLayers = labels;
        //
        // if (map.getZoom() <= countryZoomLevel) {
        //     useLayers = ['country-label'];
        // } else {
        //     useLayers = ['settlement-minor-label', 'settlement-major-label']
        // }

        var useLayers = ['country-label'];

        var features = map.queryRenderedFeatures({
            'sourceLayer': 'place_label',
            'layers': useLayers
        });

        features.forEach(function (item) {
            var k = item.layer.layout['text-size'];

            if (item.properties.name_en) {
                var name = item.properties.name_en;
            } else {
                var name = item.properties.name
            }

            if (item.layer.layout['text-max-width'] >= name.length || !name.includes(' ')) {
                var width = (k - 3) * name.length;
            } else {
                var width = (k - 3) * item.layer.layout['text-max-width'];
            }
            width = 0;
            var height = k * item.layer.layout['text-line-height'] * name.length / item.layer.layout['text-max-width'];

            if (!name.includes(' ')) {
                height += k;
            }

            var leftOffset = 0;
            var topOffset = 0;

            if (item.layer.layout['text-offset'] && 0) {
                leftOffset = item.layer.layout['text-offset'][0] * k;
                topOffset = item.layer.layout['text-offset'][1] * k;
            }

            var center = turf.center(item).geometry.coordinates;

            var el = $(document.createElement('img'));
            var style = 'width: ' + (18) + 'px;' +
                'height: ' + (18) + 'px;' +
                'cursor: pointer;' +
                'position:absolute;' +
                'top:' + (topOffset - height / 2) + 'px;' +
                'left:' + (leftOffset + width / 2) + 'px;' +
                'z-index: 0;';

            $(el).attr('style', style);
            $(el).addClass('label-zoom-arrow');
            $(el).attr('src', '{{asset('assets2/image/icons/country-hover.svg')}}');

            if (!labelMarkers[item.id]) {
                var marker = new mapboxgl.Marker($(el)[0]);
                $(el).attr('data-id', item.id);
                el[0].addEventListener('click', labelArrowClick);
            } else {
                var marker = labelMarkers[item.id];
                el = $($(marker.getElement())[0]);
                $(el).attr('style', style);
            }

            marker.setLngLat(center)
                .addTo(map);

            labelMarkers[item.id] = marker;
        });
    }
</script>