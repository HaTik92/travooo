<?php
$comment_childs = App\Models\ActivityMedia\MediasComments::where('reply_to', '=', $comment->id)->orderby('created_at','DESC')->get();
?>
<div topSort="{{count($comment->likes) + count($comment_childs)}}" newSort="{{strtotime($comment->created_at)}}">
<div class="post-comment-row news-feed-comment commentRow{{$comment->id}}">
    <div class="post-com-avatar-wrap">
        <img src="{{check_profile_picture($comment->user->profile_picture)}}" alt="">
    </div>
    <div class="post-comment-text">
        <div class="post-com-name-layer">
            <a href="#" class="comment-name">{{$comment->user->name}}</a>
            <div class="post-com-top-action">
                <div class="dropdown report-comment-dropdown">
                    <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow rcd-content" posttype="Mediacomment">
                        @if($comment->user->id==$authUserId)
                        <a href="javascript:;" class="dropdown-item report-edit-comment"  data-id="{{$comment->id}}" data-report="{{$photo->media->id}}">
                            <span class="icon-wrap">
                               <i class="trav-pencil" aria-hidden="true"></i>
                            </span>
                            <div class="drop-txt comment-edit__drop-text">
                                <p><b>@lang('profile.edit')</b></p>
                            </div>
                        </a>
                        @endif
                        @if($comment->user->id==$authUserId)
                        <a href="javascript:;" class="dropdown-item report-comment-delete" id="{{$comment->id}}" reportid="{{$photo->media->id}}" data-del-type="1">
                            <span class="icon-wrap rep-delete-icon">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </span>
                            <div class="drop-txt comment-delete__drop-text">
                                <p><b>Delete</b></p>
                            </div>
                        </a>
                        @endif
                         <a href="#" class="dropdown-item" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$comment->id}},this)">
                            <span class="icon-wrap">
                                <i class="trav-flag-icon-o"></i>
                            </span>
                            <div class="drop-txt comment-report__drop-text">
                                <p><b>@lang('profile.report')</b></p>
                            </div>
                        </a>
                       
                    </div>
                </div>
            </div>
        </div>
        <div class="comment-txt comment-text-{{$comment->id}}">
              @php
                $showChar = 200;
                $ellipsestext = "...";
                $moretext = "see more";
                $lesstext = "see less";
   
                $content = $comment->comment;
                $convert_content = strip_tags($content);
               
                if(mb_strlen($convert_content, 'UTF-8') > $showChar) {
                  
                    $c = mb_substr($content, 0, $showChar, 'UTF-8');

                    $html = '<span class="less-content">'.$c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span> <a href="javascript:;" class="read-more-link">' . $moretext . '</a></span>  <span class="more-content" style="display:none">' . $content . ' &nbsp;&nbsp;<a href="javascript:;" class="read-less-link">' . $lesstext . '</a></span>';

                    $content = $html;
                }
            @endphp
            <p>{!!$content!!}</p>
            <form class="commentEditForm{{$comment->id}} comment-edit-form d-none" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
            {{ csrf_field() }}<input type="hidden" data-id="pair{{$comment->id}}" name="pair" value="{{uniqid()}}"/>
            <div class="post-create-block reports-comment-block post-edit-block" tabindex="0">
                <div class="post-create-input report-create-input w-525">
                    <textarea name="text" data-id="text{{$comment->id}}" class="textarea-customize report-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="@lang('comment.write_a_comment')">{!!convert_string($comment->comment, false)!!}</textarea>
                </div>
                <div class="post-create-controls d-none">
                    <div class="post-alloptions">
                        <ul class="create-link-list">
                            <li class="post-options">
                                <input type="file" name="file[]" class="commenteditfile commenteditfile{{$comment->id}}" data-id="commenteditfile{{$comment->id}}"  style="display:none" multiple>
                                <i class="fa fa-camera click-target" data-target="commenteditfile{{$comment->id}}"></i>
                            </li>
                        </ul>
                    </div>
                <div class="comment-edit-action">
                    <a href="javascript:;" class="edit-cancel-link"  data-comment_id="{{$comment->id}}">Cancel</a>
                    <a href="javascript:;" class="edit-report-comment-link" data-comment_id="{{$comment->id}}">Post</a>
                </div>
                </div>
                <div class="medias report-media">
                    @if(is_object($comment->medias))
                       @foreach($comment->medias AS $media)
                           @php
                           $file_url = $media->media->url;
                           $file_url_array = explode(".", $file_url);
                           $ext = end($file_url_array);
                           $allowed_video = array('mp4');
                           @endphp
                           <div class="img-wrap-newsfeed">
                               <div>
                                   @if(in_array($ext, $allowed_video))
                                   <video style="object-fit: cover" class="thumb" controls>
                                       <source src="{{$file_url}}" type="video/mp4">
                                   </video>
                               @else
                               <img class="thumb" src="{{$file_url}}" alt="" >
                               @endif
                               </div>
                               <span class="close remove-media-comment-file" data-media_id="{{$media->media->id}}">
                                   <span>×</span>   
                               </span></div>
                       @endforeach
                    @endif
                </div>
            </div>
            <input type="hidden" name="media_id" value="{{$photo->media->id}}"/>
            <input type="hidden" name="comment_id" value="{{$comment->id}}">
            <input type="hidden" name="comment_type" value="1">
            <button type="submit"  class="d-none"></button>
        </form>
        </div>
        <div class="post-image-container">
            @if(is_object($comment->medias))
                @php
                    $index = 0;
                    
                @endphp
                @foreach($comment->medias AS $media)
                    @php
                        $index++;
                        $file_url = $media->media->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                    @endphp
                    @if($index % 2 == 1)
                    <ul class="post-image-list" style=" display: flex;margin-bottom: 20px;">
                    @endif
                        <li style="overflow: hidden;margin:1px;">
                            @if(in_array($ext, $allowed_video))
                            <video style="object-fit: cover" width="192" height="210" controls>
                                <source src="{{$file_url}}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            @else
                            <a href="{{$file_url}}" data-lightbox="comment__media{{$comment->id}}">
                                <img src="{{$file_url}}" alt="" style="width:192px;height:210px;object-fit: cover;">
                            </a>
                            @endif
                        </li>
                    @if($index % 2 == 0)
                    </ul>
                    @endif
                @endforeach
            @endif

        </div>

        <div class="comment-bottom-info">
            <div class="comment-info-content">
                <div class="dropdown">
                     <a href="#" class="report_comment_like like-link" id="{{$comment->id}}">
                         <i class="fa fa-heart {{($comment->likes()->where('users_id', $authUserId)->first())?'fill':''}}" aria-hidden="true"></i>
                     </a>
                    <a href="#" class="comment-likes-block report-comment-likes-block" data-id="{{$comment->id}}">
                         <span class="{{$comment->id}}-comment-like-count">{{count($comment->likes)}}</span>
                     </a>
                </div>
                <a href="#" class="mediaCommentReply reply-link {{$comment->id}}-rep-reply" id="{{$comment->id}}">{!!(count($comment_childs) > 0)?'<span>'.count($comment_childs).'</span> Replies':'<span></span> Reply'!!}</a>
                <span class="com-time"><span class="comment-dot report-com-dot"> · </span>{{diffForHumans($comment->created_at)}}</span>
            </div>
        </div>
    </div>
</div>
    <div class="{{$comment->id}}-comment-reply-block comment-reply-block" style="display: none;">
    @if(count($comment_childs) > 0)
        @foreach($comment_childs as $child)
            @include('site.trip2.partials.trip_place_media_comment_reply_block')
        @endforeach
    @endif
    @if($authUserId)
    <div class="post-add-comment-block replyForm{{$comment->id}}" style="position: relative;padding-top:0px;padding-left: 59px;display:none;">
        <div class="avatar-wrap" style="padding-right:10px">
            <img src="{{check_profile_picture(\App\Models\User\User::query()->find($authUserId)->profile_picture)}}" alt="" style="width:30px !important;height:30px !important;">
        </div>
        <div class="post-add-com-inputs">
            <form class="commentReplyForm{{$comment->id}}" data-id="{{$photo->media->id}}"  method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" data-id="pair{{$comment->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                <div class="post-create-block reports-comment-block post-reply-block" tabindex="0">
                    <div class="post-create-input report-create-input w-495">
                        <textarea name="text" data-id="{{$comment->id}}" class="report-reply-text textarea-customize report-comment-emoji" oninput="comment_textarea_auto_height(this, 'reply')" style="display:inline;vertical-align: top;min-height:50px;" placeholder="@lang('comment.write_a_comment')"></textarea>
                    </div>

                    <div class="post-create-controls d-none">
                        <div class="post-alloptions">
                            <ul class="create-link-list">
                                <li class="post-options">
                                    <input type="file" name="file[]" class="report-comment-reply-media-input" data-comment-id="{{$comment->id}}" data-id="commentreplyfile{{$comment->id}}" style="display:none" multiple>
                                    <i class="fa fa-camera click-target" data-target="commentreplyfile{{$comment->id}}"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="comment-edit-action">
                            <a href="javascript:;" class="report-comment-cancel-link">Cancel</a>
                            <a href="javascript:;" class="report-comment-link report-reply-link" data-id="{{$comment->id}}">Post</a>
                        </div>
                        <button type="submit" class="btn btn-primary d-none"></button>
                    </div>
                     <div class="medias report-media"></div>

                </div>
                <input type="hidden" name="media_id" value="{{$photo->media->id}}"/>
                <input type="hidden" name="comment_id" value="{{$comment->id}}">
            </form>
        </div>
    </div>

    @endif
</div>
</div>