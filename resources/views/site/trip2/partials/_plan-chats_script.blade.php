<script>
    $(document).ready(function () {
        var authId = "{{auth()->id()}}";

        window.Echo.private('chat-channel.' + authId).listen('ChatSendPrivateMessageEvent', function (e) {
            var data = e.data;

            console.log('chat-channel. => ', 'ChatSendPrivateMessageEvent => ', e);
            

            if ($('div.place-chat.window.opened[data-chatid=' + data.chat_conversation_id + ']').length) {
                var cls = 'message';
                var src = '';

                if (data.type !== null) {
                    cls += ' system';
                    src = '';
                } else if (data.from_id == authId) {
                    cls += ' right';
                    src = '<img src="' + data.from_user_thumb + '" title="' + data.from_user_name + '">';
                } else {
                    cls += ' left';
                    src = '<img src="' + data.from_user_thumb + '" title="' + data.from_user_name + '">';
                }

                var parsed = data.message_parsed ? data.message_parsed : data.file_link;

                var unread = data.is_read ? '' : 'unread';

                var message = '<div class="message-wrapper ' + unread + '" data-id="' + data.id + '">\n' +
                    '<div class="' + cls + '">\n' +
                    src + '\n' +
                    '<span>' + parsed + '</span>\n' +
                    '</div>\n' +
                    '</div>';

                $('div.place-chats-dialog').append(message);

                // $('div.place-chats-dialog').scrollTop($('div.place-chats-dialog')[0].scrollHeight);
            } else if ($('div.plan-chats div.place-chat-img[data-id=' + data.from_id + ']').length) {
                var unreadCount = $('div.plan-chats div.place-chat-img[data-id=' + data.from_id + '] div.unread-count').text();

                if (!unreadCount) {
                    unreadCount = 0;
                }


                unreadCount++;

                $('div.plan-chats div.place-chat-img[data-id=' + data.from_id + '] div.unread-count').remove();
                $('div.plan-chats div.place-chat-img[data-id=' + data.from_id + ']').append('<div class="unread-count">' + unreadCount + '</div>');
            } else if (data.places && $('div.place-chats div.place-chat-img[data-id=' + data.places_id + ']').length) {
                var unreadCount = $('div.place-chats div.place-chat-img[data-id=' + data.places_id + '] div.unread-count').text();

                if (!unreadCount) {
                    unreadCount = 0;
                }

                unreadCount++;

                $('div.place-chats div.place-chat-img[data-id=' + data.places_id + '] div.unread-count').remove();
                $('div.place-chats div.place-chat-img[data-id=' + data.places_id + ']').append('<div class="unread-count">' + unreadCount + '</div>');
            }
        });

        window.Echo.private('chat-channel.' + authId).listen('UpdatePlanChatsEvent', function (e) {
            var data = e.data;
            console.log('chat-channel. => ', 'UpdatePlanChatsEvent => ', e);

            if (data != '{{$trip_id}}') {
                return;
            }

            updateChats();
        });

    });

    var previousWindow = null;

    $('div.place-chats-dialog').on('scroll', function() {
        $('div.place-chats-dialog').find('div.message-wrapper').each(function () {
            if ($('div.place-chats-dialog').scrollTop() + $('div.place-chats-dialog').innerHeight() + 55 >= $(this).outerHeight() + $(this)[0].offsetTop) {
                var messageId = $(this).attr('data-id');

                if ($(this).hasClass('unread')) {
                    $.ajax({
                        method: 'POST',
                        url: "{{url('place-chats/readMessage')}}",
                        data: {'message_id': messageId}
                    })
                        .done(function (res) {
                        });

                    $(this).removeClass('unread');
                }
            }
        });
    });

    $(document).on('click', 'div.plan-chats div.places.section', function() {
        $('div.window.opened').not('div.places-chats').removeClass('opened');
        $('.plan-chats .title').removeClass('active');
        $(this).find('.title').addClass('active');

        if ($('div.places-chats').hasClass('opened')) {
            $('div.places-chats').removeClass('opened');
            previousWindow = null;
            modals.pop();
        } else {
            $('div.places-chats').addClass('opened');
            previousWindow = 'div.places-chats';
            modals.push({
                type: 'chat',
                element: $('div.places-chats')
            });
            window.history.pushState('forward', null, "");
        }
    });

    $(document).on('click', 'div.plan-chats div.chats.section', function() {
        $('div.window.opened').not('div.all-plan-chats').removeClass('opened');
        $('.plan-chats .title').removeClass('active');
        $(this).find('.title').addClass('active');

        if ($('div.all-plan-chats').hasClass('opened')) {
            $('div.all-plan-chats').removeClass('opened');
            previousWindow = null;
            modals.pop();
        } else {
            $('div.all-plan-chats').addClass('opened');
            previousWindow = 'all-plan-chats';
            modals.push({
                type: 'chat',
                element: $('div.all-plan-chats')
            });
            window.history.pushState('forward', null, "");
        }
    });

    var beforePlaceChatsRequest = null;

    $(document).on('click', 'div.places-chats div.content div.place-chat', function(e) {
        $('div.place-chats div.content').html('');
        $(this).parents('div.places-chats').removeClass('opened');
        $('div.place-chats').addClass('opened');

        var placeId = $(this).data('id');
        $('div.place-chats').data('id', placeId);
        var placeImage = $(this).find('div.image img').attr('src');
        var placeTitle = $(this).find('div.info div.title').text();
        var placeDesc = $(this).find('div.info div.desc').text();

        $('div.place-chats div.header div.title').text(placeTitle);
        $('div.place-chats div.header div.desc').text(placeDesc);

        $('div.place-chats div.content').append('<div class="animation title-loader"></div>' +
            '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
            '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
            '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
            '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>');

        if (beforePlaceChatsRequest) {
            beforePlaceChatsRequest.abort();
        }

        beforePlaceChatsRequest = $.ajax({
            method: 'GET',
            url: "{{url('place-chats/getPlaceUsersToChat?place_id=')}}" + placeId + '&plan_id=' + '{{$trip_id}}'
        })
            .done(function (res) {
                var roles = ['friends', 'followers', 'available today'];
                $('div.place-chats div.content').html('');
                if (res.status && res.status === 'success') {
                    for (i in roles) {
                        var role = roles[i];

                        if (!res.data[role]) {
                            continue;
                        }

                        var tempRole = role;

                        if (tempRole == 'available today') {
                            tempRole = 'other';
                        }

                        $('div.place-chats div.content').append('<div class="group">' + tempRole + '</div>');
                        res.data[role].forEach(function (data, key) {
                            var expIcon = data.is_expert ? ' <span class="exp-icon">EXP</span>' : '';

                            $('div.place-chats div.content').append('<div data-placeid="' + placeId + '" data-id="' + data.chat_id + '" class="place-chat">\n' +
                                '                                                <div class="place-block">\n' +
                                '                                                    <div class="image">\n' +
                                '                                                        <img src="' + data.src + '">\n' +
                                '                                                    </div>\n' +
                                '                                                    <div class="info">\n' +
                                '                                                        <div class="title light">\n' + data.name + expIcon +
                                '                                                        </div>\n' +
                                '                                                        <div class="desc">Visited ' + data.date + '</div>\n' +
                                '                                                    </div>\n' +
                                '                                                </div>\n' +
                                '                                            </div>')
                        });
                    }

                    if ($('div.place-chats.window div.content').is(':empty')) {
                        $('div.place-chats.window div.content').append('<div class="group no-users">No users to chat</div>');
                    }
                }
            });
    });

    $(document).on('click', 'div.window div.header div.back', function() {
        $(this).parents('div.window').removeClass('opened');
        $(previousWindow).addClass('opened');

        if (previousWindow == 'div.place-chats') {
            previousWindow = 'div.places-chats';
        }
    });

    var beforeUserChatRequest = null;
    var page = 1;
    var emptyMessages = false;

    $(document).on('click', 'div.place-chats.window div.place-chat, div.all-plan-chats.window div.place-chat', function(e) {
        $('div.place-chat.window div.header div.desc').removeClass('plan-chat-desc-opened');
        page = 1;
        emptyMessages = false;

        $('div.place-chats.opened').removeClass('opened');
        $('div.all-plan-chats.opened').removeClass('opened');

        var addClass = 'opened';
        $('div.place-chat.window').removeClass('all-plan');

        if ($(this).parents('div.all-plan-chats').length) {
            previousWindow = 'div.all-plan-chats';
            addClass = 'opened all-plan';
        } else {
            previousWindow = 'div.place-chats';
        }

        $('div.place-chat.window').addClass(addClass);
        $('div.place-chats-dialog').html('');

        var id = $(this).data('placeid');
        var chatId = $(this).data('id');
        var userTitle = $(this).find('div.info div.title').html();
        var userDesc = $(this).find('div.info div.desc').text();
        var images = $(this).find('div.place-block div.image').html();

        $('div.place-chat.window').attr('data-placeid', id);
        $('div.place-chat.window').attr('data-chatid', chatId);

        $('div.place-chat.window div.header div.title').html(userTitle);

        if ($(this).find('div.info div.desc').hasClass('plan-chat-desc')) {
            $('div.place-chat.window div.header div.desc').text($('div.header-title.tooltip-title span.mobile-hide').text());
            $('div.place-chat.window div.header div.desc').addClass('plan-chat-desc-opened');
        } else {
            $('div.place-chat.window div.header div.desc').text(userDesc);
        }

        $('div.place-chats-dialog').append('<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
            '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
            '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
            '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
            '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
            '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
            '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>');

        $('div.chat-users-block').hide();

        if ($(this).parents('div.all-plan-chats').length) {
            $('div.place-chat div.dropdown').attr('data-id', chatId);
            $('div.place-chat div.dropdown div.plan-users-thumbs').html(images);
            $('div.place-chat div.dropdown div.plan-users-thumbs div.notification-badge').remove();
            $('div.place-chat div.dropdown').show();
        } else {
            $('div.place-chat div.dropdown').hide();
        }

        $('div.place-chats-dialog').html('');

        loadMoreMessages(chatId, id);
    });

    function loadMoreMessages(chatId, id = 0) {
        var authId = "{{auth()->id()}}";

        if (beforeUserChatRequest) {
            beforeUserChatRequest.abort();
        }

        beforeUserChatRequest = $.ajax({
            method: 'GET',
            //url: "{{url('place-chats/getPlaceChatMessages?chat_id=')}}" + chatId
            url: "{{url('chat/messages')}}/" + chatId + '?page=' + page
        })
            .done(function (res) {
                res.forEach(function(data, key) {
                    var cls = 'message';
                    var src = '';

                    if (data.type !== null) {
                        cls += ' system';
                        src = '';
                    } else if (data.from_id == authId) {
                        cls += ' right';
                        src = '<img src="' + data.from_user_thumb + '" title="' + data.from_user_name + '">';
                    } else {
                        cls += ' left';
                        src = '<img src="' + data.from_user_thumb + '" title="' + data.from_user_name + '">';
                    }

                    var parsed = data.message_parsed ? data.message_parsed : data.file_link;

                    var unread = data.is_read ? '' : 'unread';

                    var message = '<div class="message-wrapper ' + unread + '" data-id="' + data.id + '">\n' +
                        '<div class="' + cls + '">\n' +
                        src + '\n' +
                        '<span>' + parsed + '</span>\n' +
                        '</div>\n' +
                        '</div>';

                    $('div.place-chats-dialog').prepend(message);
                });

                if (page <= 1) {
                    $('div.place-chats-dialog').scrollTop($('div.place-chats-dialog')[0].scrollHeight);
                }

                $('div.place-chats-send').show();
                $('div.place-chats-send button').attr('data-id', chatId);

                if (id) {
                    $('div.place-chats-send button').attr('data-placeid', id);
                }

                if (!res.length) {
                    emptyMessages = true;
                }

                beforeUserChatRequest = null;
                page++;

                $('div.place-chats-dialog div.info-loader').remove();
            });
    }

    $(document).on('click', 'div.place-chats-send button', function() {
        disableSendButton();
        var id = $(this).attr('data-id');
        var placeId = $(this).attr('data-placeid');
        var message = $('div.place-chats-send textarea').val();

        if (!message) {
            return;
        }

        $.ajax({
            method: 'POST',
            url: "{{url('place-chats/createPlaceChatMessage')}}",
            data: {"plan_id": '{{$trip_id}}', "place_id": placeId, "chat_id": id, 'message': message}
        })
            .done(function (res) {
                var message = '<div class="message-wrapper">\n' +
                    '<div class="message right">\n' +
                    '<img src="' + res.data.src + '">\n' +
                    '<span>' + res.data.message_parsed + '</span>\n' +
                    '</div>\n' +
                    '</div>';


                //$('div.place-chats-dialog').append(message);
                $('div.place-chats-dialog').scrollTop($('div.place-chats-dialog')[0].scrollHeight);
                $('div.place-chats-send textarea').val('');
            });
    });

    function triggerPlaceChatsWindow(placeId) {
        $('div.window.opened').removeClass('opened');
        $('div.places-chats.window div.place-chat[data-id=' + placeId + ']').trigger('click');
    }

    $(document).on('keypress', 'div.place-chats-send textarea', function(e) {
         if (e.which === 13 && !e.shiftKey) {
             e.preventDefault();
             $('div.place-chats-send button').trigger('click');
             return false;
         }
    });

    $(document).on('click', 'div.place-chat div.dropdown', function() {

        if ($('div.place-chat div.dropdown.dropdown-close').length) {
            $('div.place-chat div.dropdown.dropdown-close').removeClass('dropdown-close');
            $('div.chat-users-block').hide();
            return;
        }

        $('div.chat-users-block').html('');

        var chatId = $(this).attr('data-id');

        $.ajax({
            method: 'GET',
            url: "{{url('place-chats/getPlaceChatUsers')}}",
            data: {'chat_id': chatId}
        })
            .done(function (res) {
                res.data.forEach(function(data, key) {
                    $('div.chat-users-block').append('<div class="chat-user">\n' +
                        '                                                        <div class="image">\n' +
                        '                                                            <img src="' + data.src + '">\n' +
                        '                                                        </div>\n' +
                        '                                                        <div class="info">\n' +
                        '                                                            <div class="title">\n' + data.user.name +
                        '                                                            </div>\n' +
                        '                                                            <div class="desc">\n' + data.role +
                        '                                                            </div>\n' +
                        '                                                        </div>\n' +
                        '                                                    </div>')
                });
                $('div.chat-users-block').append('');
                $('div.chat-users-block').show();
                $('div.place-chat div.dropdown').addClass('dropdown-close');
            });
    });

    function disableSendButton() {
        $('div.place-chats-send button').addClass('disabled');
        $('div.place-chats-send button').attr('disabled', 'disabled');
    }

    function releaseSendButton() {
        $('div.place-chats-send button').removeClass('disabled');
        $('div.place-chats-send button').removeAttr('disabled');
    }

    $(document).on('change keyup', 'div.place-chats-send textarea', function(e) {
        var value = $(this).val();

        if (value.length > 0 && e.which !== 13) {
            releaseSendButton();
        } else {
            disableSendButton();
        }
    });

    disableSendButton();

    $("div.place-chats-dialog").on( 'scroll', function() {
        var loadingCount = $(this).parent().find('.post-comment-top-info span.loading-count strong').html();
        var allCount = $(this).parent().find('.post-comment-top-info span.all-comments-count').html();

        if(loadingCount === allCount){
            $('.loadMore2').hide();
        }

        if(visibleY($('div.place-chats-dialog div.message-wrapper:first')[0])) {
            if (!beforeUserChatRequest && !emptyMessages) {
                $('div.place-chats-dialog').prepend('<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
                    '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
                    '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
                    '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
                    '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
                    '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>' +
                    '<div class="info-loader"><div class="animation icon-loader"></div><div class="animation name-loader"></div></div>');
                loadMoreMessages($('div.place-chat.window').attr('data-chatid'));
            }
        }
    });


    function updateChats()
    {
        var edit = 0;

        @if ($editable)
            edit = 1;
        @endif

        $.ajax({
            method: 'GET',
            url: "{{url('place-chats/ajaxGetActualChats')}}",
            data: {plan_id: '{{$trip_id}}', edit: edit}
        })
            .done(function (res) {
                var data = $('<div>' + res + '</div>');
                var classes = $('div.plan-chats').attr('class');
                $('div.plan-chats').replaceWith($(data).find('div.plan-chats'));
                $('div.plan-chats').attr('class', classes);

                classes = $('div.places-chats').attr('class');
                $('div.places-chats').replaceWith($(data).find('div.places-chats'));
                $('div.places-chats').attr('class', classes);

                classes = $('div.all-plan-chats').attr('class');
                $('div.all-plan-chats').replaceWith($(data).find('div.all-plan-chats'));
                $('div.all-plan-chats').attr('class', classes);

                if ($('div.place-chats').hasClass('opened')) {
                    var placeId = $('div.place-chats').data('id');
                    $('div.place-chat[data-id=' + placeId + ']').trigger('click');
                }

                $('div.place-chat.window div.header div.desc.plan-chat-desc-opened').text($('div.header-title.tooltip-title span.mobile-hide').text());
            });
    }

</script>