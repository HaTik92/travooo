@if(isset($trip) && is_object($trip))
<div class="modal fade modal-sm vertical-center share-popup landscape-fix" backdrop="false" id="modal-share_popup" tabindex="-1" role="dialog" aria-labelledby="Share" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Share on travooo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="comment">
                        <div class="img">
                            <img src="{{check_profile_picture(auth()->user()->profile_picture)}}" alt=""/>
                        </div>
                        <input class="comment" placeholder="Say something...">
                    </div>
                    <div class="shared-content" data-type="plan">
                        <div class="plan-description">
                            <div class="author">
                                <img src="{{check_profile_picture($trip->author->profile_picture)}}" alt=""/>By <a href="/profile/{{$trip->author->id}}">{{$trip->author->name}}</a>
                            </div>
                            <div class="seporator">•</div>
                            <div class="date">
                            </div>
                        </div>
                        <div class="plan-name">
                            <a class="shared-url" target="_blank" href="/trip/plan/{{$trip->id}}">{{$trip->title}}</a>
                        </div>
                        <div class="plan-map">
                            <img src="{{get_plan_map($trip, 613, 378)}}" alt=""/>
                        </div>
                        <div class="swiper-container">
                            <div class="plan-countries swiper-wrapper">
                            </div>
                            <div class="swiper-countries-control">
                                <div class="swiper-prev"></div>
                                <div class="swiper-next"></div>
                            </div>
                        </div>
                    </div>
                    <div class="shared-content" data-type="step">
                        <div class="plan-description">
                            <div class="author">
                                <img src="{{check_profile_picture(auth()->user()->profile_picture)}}" alt=""/>By <a href="/profile/{{auth()->user()->id}}">{{auth()->user()->name}}</a>
                            </div>
                            <div class="seporator">•</div>
                            <div class="date">
                            </div>
                        </div>
                        <div class="plan-name">
                            <a class="shared-url" target="_blank" href="/trip/plan/{{$trip->id}}">{{$trip->title}}</a>
                        </div>
                        <div class="plan-map">
                            <img src="{{get_plan_map($trip, 613, 378)}}" alt=""/>
                        </div>
                        <div class="swiper-container">
                            <div class="plan-countries swiper-wrapper">
                            </div>
                            <div class="swiper-countries-control">
                                <div class="swiper-prev"></div>
                                <div class="swiper-next"></div>
                            </div>
                        </div>
                    </div>
                    <div class="shared-content" data-type="media">
                        <div class="plan-description">
                            <div class="author">
                                <img src="{{check_profile_picture(auth()->user()->profile_picture)}}" alt=""/>By <a href="/profile/{{auth()->user()->id}}">{{auth()->user()->name}}</a>
                            </div>
                            <div class="seporator">•</div>
                            <div class="date">
                            </div>
                        </div>
                        <div class="plan-name">
                            <a target="_blank" class="shared-url" href="/trip/plan/{{$trip->id}}">{{$trip->title}}</a>
                            <div class="seporator">•</div>
                            <a href="#">Media</a>
                        </div>
                        <div class="media">
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div  class="share-footer-modal__icons">
                        <a class="ico--ws" onclick="socialShare('whatsapp')"><i class="fab fa-whatsapp"></i></a>
                        <a class="ico--face" onclick="socialShare('facebook')"><i class="fab fa-facebook-f"></i></a>
                        <a class="ico--twitter" onclick="socialShare('twitter')"><i class="fab fa-twitter"></i></a>
                        <a class="ico--pin" onclick="socialShare('pintrest')"><i class="fab fa-pinterest-p"></i></a>
                        <a class="ico--tumb" onclick="socialShare('tumblr')"><i class="fab fa-tumblr"></i></a>
                    </div>
                    <button class="share">Share</button>
                </div>
            </div>
        </div>
    </div>
@endif