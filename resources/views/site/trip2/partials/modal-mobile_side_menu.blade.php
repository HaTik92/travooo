<!-- <div class="modal mobile-side-menu right" id="modal-mobile_side_menu" backdrop="false" tabindex="-1" role="dialog" aria-labelledby="Menu" aria-hidden="true"> -->
<div class="modal mobile-side-menu" id="modal-mobile_side_menu" backdrop="false" tabindex="-1" role="dialog" aria-labelledby="Menu" aria-hidden="true">
        <div type="button" class='custom--close' data-dismiss="modal" aria-label="Close">
            <i class="fal fa-times"></i>
        </div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="cover @if ($trip->cover) with-image @endif" @if ($trip->cover) style="background-image: url('{{$trip->cover}}')" @endif>
                    <div class="author-info">
                        <a href="{{route('profile')}}"><img src="{{check_profile_picture($trip->author->profile_picture)}}" alt=""></a>
                        <div class="text">
                            <a href="{{route('profile')}}">{{$trip->author->name}}</a>
                            @if (isset($role) && $role)
                                <span class="label">{{$role}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="plan-info">
                        <div class="flags">
                            @php
                                $isos = [];
                            @endphp
                            @foreach ($trip_places_arr as $key => $trip_place_arr)
                                @php
                                    $iso = @$trip_place_arr->city->country->iso_code;

                                    if (!$iso || isset($isos[$iso])) {
                                        continue;
                                    }

                                    $isos[$iso] = 1;

                                    if (count($isos) > 8) {
                                        continue;
                                    }
                                @endphp
                                <img data-cfsrc="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($iso)}}.png" alt="" src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($iso)}}.png">
                            @endforeach

                            @if (count($isos) > 8)
                                <span class="flags-add-count">+ {{count($isos) - 8}}</span>
                            @endif
                        </div>
                        <h4>{{$trip->title}}</h4>
                        @if(isset($do) && $do=='edit')
                        <p>This draft is visible to all admins and editors</p>
                        @endif
                    </div>
                </div>
                <div class="scrollable-box">
                    <div class="modal-inner-trip_info">
                        <div class="card-trip_data duration">
                            <div class="card-img"><img src="{{asset('trip_assets/images/duration.png')}}" alt=""></div>
                            <div class="card-body">
                                <p>
                                    {{$trip->_attribute('number_of_days')}}
    Days</p>
                                <p class="light">Duration</p>
                            </div>
                        </div>
                        <?php
                            $placesIds = [];

                            foreach ($trip_places_arr as $trip_place_arr) {
                                $placesIds[] = $trip_place_arr->trip_place_id;
                            }

                        ?>

                        <div class="card-trip_data budget">
                            <div class="card-img"><img src="{{asset('trip_assets/images/budget.png')}}" alt=""></div>
                            <div class="card-body">
                                <p>$
                                    <?php
                                        echo Illuminate\Support\Facades\DB::table('trips_places')->whereIn('id', $placesIds)->sum('budget');
                                    ?>
                                </p>
                                <p class="light">Budget</p>
                            </div>
                        </div>
                        <div class="card-trip_data distance">
                            <div class="card-img"><img src="{{asset('trip_assets/images/distance.png')}}" alt=""></div>
                            <div class="card-body">
                                <p>
                                    <?php
                                        $distanceCount = 0;
                                        $beforePlace = null;

                                        foreach (\App\Models\TripPlaces\TripPlaces::query()->whereIn('id', $placesIds)->with('place')->get() as $distancePlace) {
                                            if ($beforePlace && $beforePlace->place->id !== $distancePlace->place->id) {
                                                $distanceCount += haversineGreatCircleDistance($beforePlace->place->lat, $beforePlace->place->lng, $distancePlace->place->lat, $distancePlace->place->lng);
                                            }

                                            $beforePlace = $distancePlace;
                                        }

                                        echo round($distanceCount/1000);
                                    ?>
                                    km</p>
                                <p class="light">Distance</p>
                            </div>
                        </div>
                        <div class="card-trip_data destinations">
                            <div class="card-img"><img src="{{asset('trip_assets/images/destinations.png')}}" alt=""></div>
                            <div class="card-body">
                                <p>{{count($placesIds)}}</p>
                                <p class="light">Destinations</p>
                            </div>
                        </div>
                    </div>
                    @if(isset($do) && $do=='edit')
                    <div class="side-menu-option">
                        <a href="?"><button class="text" >Go to <strong>View Mode</strong></button></a>
                    </div>
                    @elseif (($is_editor || $is_admin))
                    <div class="side-menu-option">
                        <a href="?do=edit"><button class="text" >Go to <strong>Edit Mode</strong></button></a>
                    </div>
                    @endif
                    @if(isset($do) && $do=='edit' && $planMember)
                    <div class="side-menu-option">
                        <button class="text activity-logs">Activity Log <span style="display: none;" class="counter">0</span></button>
                    </div>
                    @endif
                    <div class="side-menu-option">
                        <div class="info-title">People in this trip plan</div>
                        <div class="info-details">
                            @if ((count($plan_people) > 0 && !$is_admin) || (count($plan_people) > 1))
                                <div class="people">
                                    <ul class="trip-avatar-list">
                                        @foreach($plan_people as $key => $plan_person)
                                            @if ($key > 1)
                                                @break;
                                            @endif
                                            <li><img src="{{$plan_person['img']}}" alt="ava"></li>
                                        @endforeach
                                    </ul>
                                    <span class="text">
                                    @foreach($plan_people as $key => $plan_person)@if ($key > 1) @break; @elseif($key > 0),@endif {{explode(' ', $plan_person['name'])[0]}}@endforeach
                                    @if (count($plan_people) > 2)
                                     <strong>+{{count($plan_people) - 2}}</strong> others
                                    @endif
                                    </span>
                                </div>
                                @if ($is_admin)
                                    <button type="button" class="invite" data-dismiss="modal" data-toggle="modal" data-target="#modal-invite"><i class="trav-user-icon"></i> <strong>Invite</strong></button>
                                @endif
                            @elseif ($is_admin)
                                <div class="people">
                                    <ul class="trip-avatar-list" style="display: none;">
                                    </ul>
                                    <span class="text">You are the only member in this trip plan, if you
                                    want to add more people <strong data-dismiss="modal" data-toggle="modal" data-target="#modal-invite">Click Here</strong></span>
                                </div>
                                <button style="display: none;" type="button" class="invite" data-dismiss="modal" data-toggle="modal" data-target="#modal-invite"><i class="trav-user-icon"></i> <strong>Invite</strong></button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="side-menu-option fullwidth buttons-group">
                @if($is_invited && $trip->users_id !== auth()->id())
                    <button class="btn gray leave-plan">Leave trip</button>
                @endif
                    <button class="btn pink exit-plan">Exit</button>
                </div>
                <div class="side-menu-option fullwidth buttons-group">
                    <div class="reactions">
                        <a href="#" class="likes-count"><i class="trav-heart-fill-icon"></i> <span>0</span></a>
                        <a href="#" class="comments-count"><i class="trav-comment-icon"></i> <span>0</span></a>
                        <span data-toggle="modal" data-target="#modal-share_popup" class="plan-share-button"><a href="#"><i class="trav-share-icon"></i> {{\App\Models\TripPlans\TripsShares::query()->where('trip_id', $trip->id)->count()}} Share</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>