<script src="{{asset('trip_assets/scripts/vendor.js')}}"></script>
<script src="{{asset('trip_assets/scripts/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{asset('assets2/js/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="{{asset('trip_assets/scripts/vendor/modernizr.js')}}"></script>

<script>
    var opened = 0;

    var modals = [];
    var searchMarkers = [];

    var selected_city_name = '';
    var addedImages = [];

    var expandedPlaces = [];

    var num_location_result = 0;

    var uploader = null;
    var editUploader = null;
    var suggestUploader = null;

    var tempCoords = null;

    var add = '{{$add}}';
    var addType = '{{$add_type}}';
    var addData = [];

    var preloadContent = <?= json_encode($content);?>;

    if (add) {
        addData = JSON.parse('{!! $add_data !!}');
    }

    @if(isset($trip) && is_object($trip))
    function addImagePreview(file, container) {
        var reader = new window.FileReader();
        reader.readAsDataURL(file.getNative());
        reader.onload = function () {
            base64data = reader.result;
            base64data = base64data.substring(base64data.indexOf(",") + 1);
            var base_64_url = "data:" + file.type + ";base64," + base64data;

            var append = '<div class="temp swiper-slide"><div class="full-pb"><div class="pb"></div></div><div class="media-pb"></div><img src="'+base_64_url+'" class="img-upload"></div>';

            if (file.type === 'video/mp4') {
                append = '<div class="temp swiper-slide"><div class="full-pb"><div class="pb"></div></div><div class="media-pb"></div><video src="'+base_64_url+'" class="img-upload"></div>'
            }

            $(append).insertBefore(container.find('button.img-upload').parent());
        };
    }

    var place_id = $('#places_real_id').val();
    var trip_place_id = $('#trip_places_real_id').val();

    var events = {
        BeforeUpload: function(up, file) {
            place_id = $('#' + up.settings.container.id).closest('.modal.modal-sidebar').find('#places_real_id').val();
            trip_place_id = $('#' + up.settings.container.id).closest('.modal.modal-sidebar').find('#trip_places_real_id').val();
            up.setOption('url', "{{url('trip/plan/'.$trip->id.'/media')}}?trip_id={{$trip->id}}&place_id="+place_id+'&trip_place_id='+trip_place_id);

            $('button.doEditPlace').prop('disabled', true);
            $('button.doAddPlace').prop('disabled', true);
        },

        FilesAdded: function(up, files) {
            $('#' + up.settings.container.id).parents('.optional').removeClass('optional');

            var maxfiles = 11 - $('#' + up.settings.container.id).find('div.swiper-slide').length;

            if (files.length > maxfiles)
            {
                var start = maxfiles - 1;
                var offset = up.files.length - maxfiles;

                up.splice(start, offset);
                files.splice(start, offset);
                toastr.options = {
                    timeOut: 3000,
                    extendedTimeOut: 0,
                    positionClass: "toast-bottom-right"
                };
                toastr.error('no more than '+ maxfiles + ' file(s)');
            }

            if (files.length === maxfiles) {
                $('#' + up.settings.container.id).find('div#edit-img-upload').hide();
            }

            plupload.each(files, function(file) {
                addImagePreview(file, $('#' + up.settings.container.id));
                up.start();
            });
        },

        UploadProgress: function(up, file) {
            $('button.img-upload').attr('disabled', 'disabled');
            $('#' + up.settings.container.id).find('div.temp div.pb:first').css('width', file.percent + '%');
            $('button.doEditPlace').prop('disabled', true);
            $('button.doAddPlace').prop('disabled', true);
        },

        UploadComplete: function(up, files) {
            $('button.doEditPlace').prop('disabled', false);
            $('button.doAddPlace').prop('disabled', false);
        },

        FileUploaded: function(up, file, result) {
            $('button.img-upload').removeAttr('disabled');
            var response = JSON.parse(result.response);
            //$('#' + up.settings.container.id).find('div.temp:last').remove();

            var append = '<div data-mediaid="' +response.id + '" class="swiper-slide"><img src="'+response.url+'" class="img-upload"><div class="remove"></div></div>';

            if (file.type === 'video/mp4') {
                append = '<div data-mediaid="' +response.id + '" class="swiper-slide"><video poster="'+response.url+'" class="img-upload"></video><div class="remove"></div></div>';
            }

            //$(append).insertBefore($('#' + up.settings.container.id).find('button.img-upload').parent());

            $('#' + up.settings.container.id).find('div.temp:first').replaceWith(append);

            addedImages.push(response.id);
        },

        Error: function(up, err) {
            toastr.options = {
                timeOut: 3000,
                extendedTimeOut: 0,
                positionClass: "toast-bottom-right"
            };
            $('button.img-upload').removeAttr('disabled');
            $('button.doEditPlace').prop('disabled', false);
            $('button.doAddPlace').prop('disabled', false);

            if (err.code === plupload.FILE_SIZE_ERROR) {
                toastr.error('File is too large to upload');
            }

            if (err.code === plupload.FILE_EXTENSION_ERROR) {
                toastr.error('Wrong file extension');
            }
        }
    };

    function initUploaders() {
        if (uploader) {
            uploader.disableBrowse(true);
        }
        if (editUploader) {
            editUploader.disableBrowse(true);
        }
        if (suggestUploader) {
            suggestUploader.disableBrowse(true);
        }

        plupload.addFileFilter('maxsizefilter', function(maxSize, file, cb) {
            if ((file.size > 10000000 && file.type.includes('image')) || (file.size > 25000000 && file.type.includes('video'))) {
                this.trigger('Error', {
                    code : plupload.FILE_SIZE_ERROR,
                    message : plupload.translate('File size error.'),
                    file : file
                });
                cb(false);
            } else {
                cb(true);
            }
        });

        var uploaderConfig = {
            // General settings
            runtimes : 'html5,flash,silverlight,html4',
            url : "{{url('trip/plan/'.$trip->id.'/media')}}?trip_id={{$trip->id}}&place_id="+place_id,
            browse_button: 'img-upload',
            container: 'media-container',
            filters : {
                mime_types : [
                    { title : "Image files", extensions : "webp,jpeg,jpg,gif,png" },
                    { title : "Video files", extensions : "mp4,avi,mov" }
                ],
                maxsizefilter: true,
            },
            rename: false,
            sortable: false,
            dragdrop: true,
            views: {
                thumbs: true, // Show thumbs
                active: 'thumbs'
            },
            flash_swf_url : 'assets2/js/plupload-2.3.6/js/Moxie.swf',
            silverlight_xap_url : 'assets2/js/plupload-2.3.6/js/Moxie.xap',

            // Event customization.
            init: events
        };


        uploader = new plupload.Uploader(uploaderConfig);

        uploaderConfig.browse_button = 'edit-img-upload';
        uploaderConfig.container = 'edit-media-container';

        editUploader = new plupload.Uploader(uploaderConfig);

        uploaderConfig.browse_button = 'suggest-img-upload';
        uploaderConfig.container = 'suggest-media-container';

        suggestUploader = new plupload.Uploader(uploaderConfig);

        uploader.init();
        editUploader.init();
        suggestUploader.init();
    }
    @endif

function clearCityAndPlaceFields(parent) {
        $(parent).find('input#places_real_id').val('');
        $(parent).find('input#cities_real_id').val('');
        $(parent).find('div.input-group-img.city').attr('style', '');
        $(parent).find('div.input-group-img.city').removeClass('chosen');
        $(parent).find('input.input.places_id').val('');
        $(parent).find('input.input.places_id').val('');
        $(parent).find('.input-group-img.place').attr('style', '');
        $(parent).find('.input-group-img.place').removeClass('chosen');
        $(parent).find('#allSearchResults').hide();
        $(parent).find('input.input.places_id').removeAttr('disabled');
        $(parent).find('input.input.places_id').removeClass('disabled');
        $(parent).find('.input-group-img.place').removeClass('disabled');
        $(parent).find('.input-group-img.place').css('cursor', 'pointer');
}

function cityChosen(parent, cityId, cityTitle, lat, lng, city, withoutFly = false, fromMap = false) {
    clearCityAndPlaceFields(parent);

    if (!withoutFly) {
        map.flyTo({
            center: [parseFloat(lng), parseFloat(lat)],
            essential: true,
            speed: 1.5,
            curve: 1,
            zoom: 12
        });
    }

    window.selected_city_id = cityId;
    window.selected_city_title = cityTitle;

    selected_city_name = city;
    $('div.map-placeholder-block').hide();
    showHint = false;

    window.locationLng = lng;
    window.locationLat = lat;
}

//Select Country/City from map search
function selectMapLocation(selectedTitle, lat, lng, zoom) {
    $('.map-place-search').val(selectedTitle)

        map.flyTo({
            center: [parseFloat(lng), parseFloat(lat)],
            essential: true,
            speed: 1.5,
            curve: 1,
            zoom: zoom
        });
}


function reselectCity(element)
{
    var parent = $(element).closest('.modal.modal-sidebar');

    if ($(element).attr('data-cityid') && $(parent).find('input#cities_real_id').val() != $(element).attr('data-cityid')) {
        var cityId = $(element).attr('data-cityid');
        var cityTitle = $(element).attr('data-citytitle');
        var cityImg = $(element).attr('data-cityimg');
        var cityLat = $(element).attr('data-citylat');
        var cityLng = $(element).attr('data-citylng');

        $(parent).find('input#cities_real_id').val(cityId);
        $(parent).find('input#cities_id').val(cityTitle);
        $(parent).find('.input-group-img.city').addClass('chosen');
        $(parent).find('.input-group-img.city').css('background-image', 'url(' + cityImg + ')');
        $(parent).find('.input-group-img.city').css('background-size', 'cover');

        cityChosen(parent, cityId, cityTitle, cityLat, cityLng, cityTitle, true);
    }
}

@if (isset($goto_first_step) AND $goto_first_step)
    $(document).ready(function () {
        $('#modal-create_trip').modal({backdrop: 'static', keyboard: false});
    });
@endif

    var editable = parseInt("{{$editable}}");

    function delay(callback, ms) {
        var timer = 0;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };

    }

    window.locationLat = false;
    window.locationLng = false;

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            document.getElementById('locationMsg').innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    getLocation();

    function showPosition(position) {
        window.locationLat = position.coords.latitude;
        window.locationLng = position.coords.longitude;
        var myEle = document.getElementById("post_actual_location");
        if(myEle){
            document.getElementById('post_actual_location').value = position.coords.latitude + ',' + position.coords.longitude;
        }
    }

    function showError(error) {
        if(document.getElementById('locationMsg') == null) return;
        switch (error.code) {
            case error.PERMISSION_DENIED:
                document.getElementById('locationMsg').innerHTML = "We are using your location information to give you a better experience showing most accurate & nearby results. Please note that some interesting functionality - like showing Places of interests and updates according to their location from you - will not be working as expected until you allow Travooo to access your location. \n\
(<a href='https://www.lifewire.com/denying-access-to-your-location-4027789' target='_blank'>Know how here</a>) " + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                document.getElementById('locationMsg').style.display = 'block';
                break;
            case error.POSITION_UNAVAILABLE:
                document.getElementById('locationMsg').innerHTML = "Location information is unavailable."
                document.getElementById('locationMsg').style.display = 'block';
                break;
            case error.TIMEOUT:
                document.getElementById('locationMsg').innerHTML = "The request to get user location timed out."
                document.getElementById('locationMsg').style.display = 'block';
                break;
            case error.UNKNOWN_ERROR:
                document.getElementById('locationMsg').innerHTML = "An unknown error occurred."
                document.getElementById('locationMsg').style.display = 'block';
                break;
        }
    }

</script>


<script>
    @if(isset($trip) && is_object($trip))
        function updatePlanContents(scrollId = false, withUpdateHtml = true, withUpdateScroll = false) {
            var trip_id = {{$trip->id}};

            if (withUpdateHtml) {
                $.ajax({
                    method: "GET",
                    url: "{{url('trip/ajaxGetPlanContents')}}",
                    data: {"trip_id": trip_id @if(isset($do) && $do=="edit") , "do": "edit" @elseif(isset($do) && $do=="preview") , "do": "preview"  @endif}
                })
                    .done(function (res) {
                        afterUpdatePlanContents(res, scrollId, withUpdateHtml, withUpdateScroll);
                    });
            } else {
                afterUpdatePlanContents('', scrollId, withUpdateHtml);
            }

        }

        function afterUpdatePlanContents(res, scrollId, withUpdateHtml, withUpdateScroll = false) {
            var result = res;
            var scrollTop = $('div.trip').scrollTop();

            if (withUpdateHtml) {
                if (withUpdateScroll) {
                    $('#planContents').html(result).promise().done(function(){
                        setTimeout(function() {
                            if ($('div.trip-place-wrap').length) {
                                $('div.trip').scroll();
                            } else {
                                map.zoomTo(0);
                            }
                        }, 1000);
                    });
                } else {
                    $('#planContents').html(result);
                }
            }

            updatePositions();

            $('div.trip').scrollTop(scrollTop);
            $('b.overall-budget').text('$' + $('input#overall-budget').val());

            expandedPlaces.forEach(function(val, i) {
                if (val) {
                    $('div.trip-place[data-id=' + i + '] div.card-collapse').collapse('show');
                }
            });

            // mobile view
            if($('.sidebar').hasClass('invited') && !$('span.navbar-text.mobile-hide:visible').length) {
                $('.card-trip_info.card-collapse').collapse('hide')
            }

            if (scrollId) {
                scrollSideBar(scrollId);
            }

            if ($('div#modal-suggestions:visible').length) {
                getSuggestions();
            }

            search();
            updateChats();

            if ($('div#modal-activity_logs').is(':visible')) {
                $('div.logs_unread').remove();
            }

            if ($('div#modal-edit_place').is(':visible') && !$('.trip-place-wrap[data-placeid="' + $('div#modal-edit_place #trip_places_real_id').val() + '"] .card-inner').not('.disapproved').length) {
                $('div#modal-edit_place').modal('hide');
                $('div#modal-add_suggestion').modal('hide');
            }

            if ($('.cover-block').is(':visible') && (!$('.trip-place-wrap[data-placeid="' + $('.cover-block:visible').attr('trip-place-id') + '"] .card-inner').not('.disapproved').length || !$('div.lightbox_imageModal[data-mediaid="' + $('.cover-block:visible').attr('id') + '"]').length )) {
                $('.lg-close').trigger('click');
            }

            updateTrendingData();

            if (scrollId) {
                var placeId = $('div.trip-place-wrap[data-placeid=' + scrollId + ']').data('origplaceid');
                updateTrendingData(0, placeId, 1, [], true);
            } else if (window.plan_map.getZoom() > cityZoomLevel && !isSuggestedPlaces) {
                var center = map.getBounds().getCenter();
                updateTrendingData(0, 0, 1, [center.lat, center.lng]);
            }
        }
    @endif

    function autoSave(saved = false)
    {
        $('div.line-grey.header').remove();
        $('button#save-draft').remove();

        if (saved) {
            $('div.header-control.ml-auto').prepend('<button type="button" id="save-draft" class="btn btn-transparent">Draft Saved</button><div class="line-grey header"></div>');
            return;
        }

        $('div.header-control.ml-auto').prepend('<button type="button" id="save-draft" class="btn btn-transparent">Saving...</button><div class="line-grey header"></div>');
    }

    var suggestedResults = [];
    var placeResults = [];
    var parent_body = null;

    function renderSuggestedPlaces() {
        suggestedResults.slice(0, 10).forEach(function (element) {
            if (!element.image) {
                element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
            } else {
                element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.image;
            }

            var reviews = [];

            if (element.reviews) {
                reviews = element.reviews;
            }

            var reviews_avg = element.reviews_avg ? Number(element.reviews_avg).toFixed(1) : 0;

            if (parent_body.find('.suggestedContainer .drop-row').length < 3) {
                element.type = 'forced';
            }

            switch (element.type) {
                case parseInt('{{\App\Services\Trends\TrendsService::PLACE_TYPE_TRENDING}}'):
                case parseInt('{{\App\Services\Trends\TrendsService::PLACE_TYPE_TRENDING_FRIENDS}}'):
                case parseInt('{{\App\Services\Trends\TrendsService::PLACE_TYPE_FRIENDS}}'):
                case 'forced':
                    var type = '<img src="https://www.travooo.com/assets2/image/icons/trending-destination-icon.svg" style="\n' +
                        '    width: 12px;\n' +
                        '    margin-right: 3px;\n' +
                        '    filter: invert(64%) sepia(0%) saturate(0%) hue-rotate(155deg) brightness(96%) contrast(85%);\n' +
                        '">Trending';
                    break;
                default:
                    var type = 'Suggested';
                    break;
            }


            var title = element.title.replace(/'/g, "\\'");

            parent_body.find('.suggestedContainer').append('<div class="drop-row" onclick="$(this).closest(\'.modal.modal-sidebar\').find(\'#places_id\').val(\''+title+'\');$(this).closest(\'.modal.modal-sidebar\').find(\'#places_real_id\').val(\''+element.id+'\');" data-reviewscount="'+ reviews.length +'" data-reviewsavg="'+ reviews_avg +'" data-cityid="'+element.city_id+'" data-citytitle="'+element.city_title+'" data-id="'+element.id+'" data-address="'+element.address+'" data-lat="'+element.lat+'" data-lng="'+element.lng+'" data-title="'+title+'" style="cursor:pointer;"><div class="img-wrap rounded place-img"><img src="' + element.image + '" id="POIImage'+element.id+'" alt="logo" style="width:60px;height:60px;"></div><div class="drop-content place-content"><div class="title"><h3 class="content-ttl" style="font-size: 18px;text-align: left;">' + title + '</h3><span>' + type + '</span></div><p class="place-name" style="width:100%;text-align: left;"><span>' + element.address + '</span></p></div></div>');
        });

        suggestedResults = suggestedResults.slice(10, 100);
        $('div.map-select-place div#allSearchResults div.places-preloader').remove();
    }

    $(document).ready(function () {
        $(document).on('hide.bs.modal','#modal-edit_place', function () {
            //clearMarkers();
            $('div.map-placeholder-block').hide();
        });

        $(document).on('hide.bs.modal','#modal-add_place', function () {
            clearMarkers();
            $('div.map-placeholder-block').hide();
        });

        $(document).on('click', 'label.btn-filter', function() {
            var isActive = $(this).hasClass('active');
            $('label.btn-filter').removeClass('active');

            if (!isActive) {
                $(this).addClass('active');
            }
        });

        function clearMarkers(withZoom = true) {
            searchMarkers.forEach(function (m) {
                m.remove();
                if (m.getPopup()) {
                    m.getPopup().remove();
                }
            });

            searchMarkers = [];
            searchMarkersCoordinates = [];

            if (withZoom) {
                //window.plan_map.setZoom(2);
            }

            tempCoords = null;

            renderPlaceMarkers();
            setRouteSources();
        }

        var currentSearch = null;

        $(document).on('keyup click', 'input#places_id', function(e) {
            if (!$(this).val()) {
                getSuggesstedPlaces(selected_city_id, $(this).closest('.modal.modal-sidebar'));
            }
        });

        $('input.map-place-search').keyup(delay(function (e) {
            let value = $(e.target).val();
            var obj = $(this);
            var parent_body = $(this).closest('.map-search');
            num_location_result = 0;

            var active_key = false;

            if (value.length >= 3) {
                if ( active_key === false ) {
                    parent_body.find('.input-group').addClass('input-group-focus');

                    parent_body.find(".map-select-map-place").html('<ul class="location-tab-list"><li class="active" data-tab_item="all">All <b class="s-result-all-count">0</b></li><li class="" data-tab_item="place">Places <b class="s-result-place-count">0</b></li><li class="" data-tab_item="city">Cities <b class="s-result-cities-count">0</b></li><li class="" data-tab_item="country">Countries <b class="s-result-countries-count">0</b></li></ul>'+
                                                                    '<div style="overflow-x: hidden; max-height: 400px;" id="allSearchResults"></div>');

                    var type = '';

                    if ($('label.btn-filter.active span').data('type')) {
                        type = $('label.btn-filter.active span').data('type');
                    }

                    if (currentSearch) {
                        currentSearch.abort();
                    }

                    // start searching all POI types
                    currentSearch = $.ajax({
                        method: "GET",
                        url: "{{url('home/searchAllPOIs')}}",
                        data: {
                            q: obj.val(), lat: window.locationLat, lng: window.locationLng, type: type, without_users: 1
                        }
                    })
                        .done(function (res) {
                            res = JSON.parse(res);
                            num_location_result = num_location_result + res.length;
                            $('.s-result-all-count').html(num_location_result)
                            $('.s-result-place-count').html(res.length)

                            clearMarkers(false);

                            res.forEach(function (element) {
                                if (!element.image) {
                                    element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
                                } else {
                                    element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.image;
                                }

                                var reviews = [];

                                if (element.reviews) {
                                    reviews = element.reviews;
                                }

                                var reviews_avg = element.reviews_avg ? Number(element.reviews_avg).toFixed(1) : 0;

                                var title = element.title.replace(/'/g, "\\'");
                                parent_body.find('.map-select-map-place #allSearchResults').append('<div class="drop-row place drop-map-row" data-cityimg="' + element.city_img + '" data-reviewscount="'+ reviews.length +'" data-reviewsavg="'+ reviews_avg +'" data-cityid="'+element.city_id+'" data-citytitle="'+element.city_title+'" data-id="'+element.id+'" data-address="'+element.address+'" data-lat="'+element.lat+'" data-lng="'+element.lng+'" data-title="'+element.title+'" style="cursor:pointer;"><div class="img-wrap rounded place-img"><img src="' + element.image + '" id="POIImage'+element.id+'" alt="logo" style="width:60px;height:60px;"></div><div class="drop-content place-content"><div class="title"><h3 class="content-ttl" style="font-size: 18px;text-align: left;">' + element.title + '</h3><span>Suggested</span></div><p class="place-name" style="width:100%;text-align: left;"><span>' + element.address + '</span></p></div></div>');

                                // element.city_id = element.city_id ? element.city_id : 2031;

                                // var contentString = generatePopup(
                                //     element.id,
                                //     element.image,
                                //     title,
                                //     element.address,
                                //     reviews_avg,
                                //     reviews.length,
                                //     new Date().toISOString().split('T')[0],
                                //     element.city_id,
                                //     element.city_title,
                                //     [],
                                //     editable
                                // );
                                //
                                // var popup = new mapboxgl.Popup()
                                //     .setHTML(contentString);
                                //
                                //
                                // var el = $(document.createElement('div'));
                                // var style = 'background: url(\' ' + element.image + '\');' +
                                //     'background-size: cover;' +
                                //     'background-repeat: no-repeat;' +
                                //     'background-position: center center;' +
                                //     'width: 50px;' +
                                //     'height: 50px;' +
                                //     'border-radius: 50px;' +
                                //     'border: 2px solid #fff !important;' +
                                //     'cursor: pointer;';
                                //
                                // $(el).addClass('place-marker marker');
                                // $(el).attr('style', style);
                                //
                                // var lnglat = [parseFloat(element.lng), parseFloat(element.lat)];
                                //
                                // var marker = new mapboxgl.Marker($(el)[0])
                                //     .setLngLat(lnglat)
                                //     .setPopup(popup)
                                //     .addTo(window.plan_map);
                                //
                                // searchMarkers.push(marker);
                                // searchMarkersCoordinates.push(lnglat);
                            });
                        });

                    //Get Cities resuult
                    $.ajax({
                        method: "GET",
                        url: "{{url('home/searchCities')}}",
                        data: {
                            q: obj.val(), lat: window.locationLat, lng: window.locationLng
                        }
                    })
                        .done(function (res) {
                            res = JSON.parse(res);

                            num_location_result = num_location_result + res.length;

                            $('.s-result-all-count').html(num_location_result)
                            $('.s-result-cities-count').html(res.length)

                            clearMarkers(false);

                            res.forEach(function (element) {
                                if (typeof element.get_medias[0] === 'undefined' || typeof element.get_medias[0].url === 'undefined' || ! element.get_medias[0].url) {
                                    element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
                                } else {
                                    element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.get_medias[0].url;
                                }

                                parent_body.find('.map-select-map-place #allSearchResults').append('<div class="drop-row city" style="cursor:pointer;" onclick="selectMapLocation(\''+element.transsingle.title+'\', \''+element.lat+'\', \''+element.lng+'\', 12)"><div class="img-wrap rounded place-img"><img src="' + element.image + '" id="city'+element.id+'" alt="logo" style="width:60px;height:60px;"></div><div class="drop-content place-content"><div class="title"><h3 class="content-ttl" style="font-size: 18px;text-align: left;">' + element.transsingle.title + '</h3><span>Suggested</span></div><p class="place-name" style="width:100%;text-align: left;"><span> City in ' + element.country.transsingle.title + '</span></p></div></div>');

                            });
                        });

                    //Get Countries resuult
                    $.ajax({
                        method: "GET",
                        url: "{{url('home/searchCountries')}}",
                        data: {
                            q: obj.val(), lat: window.locationLat, lng: window.locationLng
                        }
                    })
                        .done(function (res) {
                            res = JSON.parse(res);

                            num_location_result = num_location_result + res.length;

                            $('.s-result-all-count').html(num_location_result)
                            $('.s-result-countries-count').html(res.length)

                            clearMarkers(false);

                            res.forEach(function (element) {
                                if (typeof element.get_medias[0] === 'undefined' || typeof element.get_medias[0].url === 'undefined' || ! element.get_medias[0].url) {
                                    element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
                                } else {
                                    element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.get_medias[0].url;
                                }

                                parent_body.find('.map-select-map-place #allSearchResults').append('<div class="drop-row country" onclick="selectMapLocation(\''+element.transsingle.title+'\', \''+element.lat+'\', \''+element.lng+'\', 8)" style="cursor:pointer;"><div class="img-wrap rounded place-img"><img src="' + element.image + '" id="city'+element.id+'" alt="logo" style="width:60px;height:60px;"></div><div class="drop-content place-content"><div class="title"><h3 class="content-ttl" style="font-size: 18px;text-align: left;">' + element.transsingle.title + '</h3><span>Suggested</span></div><p class="place-name" style="width:100%;text-align: left;"><span> Country in ' + element.region.transsingle.title + '</span></p></div></div>');

                            });
                        });
                }
            } else {
                parent_body.find('#allSearchResults').empty();
            }
        }, 500));

        window.plan_map.on('dragend', function () {
            $('.map-search #allSearchResults').empty();
            $('.map-search .input-group-focus').removeClass('input-group-focus');
        });

        window.plan_map.on('click', function () {
            $('.map-search #allSearchResults').empty();
            $('.map-search .input-group-focus').removeClass('input-group-focus');
        });


        $(document).on('click', '.location-tab-list li', function(){
            var type = $(this).attr('data-tab_item')

            $('.location-tab-list li').removeClass('active')
            $(this).addClass('active')

            if(type != 'all'){
                $('#allSearchResults div.drop-row').addClass('d-none')
                $('#allSearchResults div.drop-row.' + type).removeClass('d-none')
            }else{
                $('#allSearchResults div.drop-row').removeClass('d-none')
            }

        })


        var parent_body = null;

        function renderPlaceResults() {
            placeResults.slice(0, 10).forEach(function (element) {
                if (!element.image) {
                    element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
                } else {
                    element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.image;
                }

                var title = element.title.replace(/'/g, "\\'");
                var reviews = [];

                if (element.reviews) {
                    reviews = element.reviews;
                }

                var reviews_avg = element.reviews_avg ? Number(element.reviews_avg).toFixed(1) : 0;

                parent_body.find('.map-select-place #allSearchResults').append('<div class="drop-row"  data-reviewscount="'+ reviews.length +'" data-reviewsavg="'+ reviews_avg +'" data-cityid="'+element.city_id+'" data-citytitle="'+selected_city_name+'" data-id="'+element.id+'" data-address="'+element.address+'" data-lat="'+element.lat+'" data-lng="'+element.lng+'" data-title="'+element.title+'"  style="cursor:pointer;" onclick="reselectCity($(this)); $(this).closest(\'.modal.modal-sidebar\').find(\'#places_id\').val(\'' + title + '\');$(this).closest(\'.modal.modal-sidebar\').find(\'#places_real_id\').val(\''+element.id+'\');$(this).closest(\'.modal.modal-sidebar\').find(\'.input-group-img.place\').css(\'background-image\', \'url(' + element.image + ')\');$(this).closest(\'.modal.modal-sidebar\').find(\'.input-group-img.place\').css(\'background-size\', \'cover\');"><div class="img-wrap rounded place-img"><img src="' + element.image + '" id="POIImage'+element.id+'" alt="logo" style="width:60px;height:60px;"></div><div class="drop-content place-content"><div class="title"><h3 class="content-ttl" style="font-size: 18px;text-align: left;">' + element.title + '</h3><span></span></div><p class="place-name" style="width:100%;text-align: left;"><span>' + element.address + '</span></p></div></div>');
            });

            placeResults = placeResults.slice(10, 100);
            $('div.map-select-place div#allSearchResults div.places-preloader').remove();
        }

        $('div.map-select-place').on('scroll', function() {
            if($('div.map-select-place')[0].scrollTop + $('div.map-select-place')[0].clientHeight >= $('div.map-select-place div#allSearchResults').outerHeight()) {
                if (placeResults.length) {
                    appendPreloaderPlace();
                    setTimeout(renderPlaceResults, 500);
                }

                if (suggestedResults.length) {
                    appendPreloaderPlace();
                    setTimeout(renderSuggestedPlaces, 500);
                }
            }

        });

        function appendPreloaderPlace() {
            var preloader = '<div class="places-preloader">' +
                '<div class="place-icon-preloader animation"></div>' +
                '<div class="place-info-preloader">' +
                '<div class="place-name-preloader animation"></div>' +
                '<div class="place-desc-preloader animation"></div>' +
                '<div class="place-desc-preloader animation"></div>' +
                '</div>' +
                '</div>';

            $('div.map-select-place div#allSearchResults').append(preloader);
            $('div.map-select-place div#allSearchResults').append(preloader);
            $('div.map-select-place div#allSearchResults').append(preloader);
        }

        var searchPlaces = null;

        $('input#places_id').keyup(delay(function (e) {
            let value = $(e.target).val();
            var obj = $(this);
            parent_body = $(this).closest('.modal.modal-sidebar');

            var search_in = false;
            var search_id = false;
            var active_key = false;

            if (e.keyCode === 46 || e.keyCode === 8) {
                parent_body.find(".map-select-place").html('<div id="allSearchResults"></div>');
            }

            if (value.length >= 3) {
                if ( active_key === false ) {
                    placeResults = [];
                    suggestedResults = [];

                    parent_body.find('#dropCont').addClass('input-group-focus');
                    parent_body.find(".map-select-place").html('<div id="allSearchResults"></div>');

                    appendPreloaderPlace();

                    if (searchPlaces) {
                        searchPlaces.abort();
                    }

                    // start searching all POI types
                    searchPlaces =  $.ajax({
                        method: "GET",
                        url: "{{url('home/searchAllPOIs')}}",
                        data: {
                            q: obj.val(), lat: window.locationLat, lng: window.locationLng, city_id: selected_city_id, without_users: 1
                        }
                    })
                        .done(function (res) {
                            res = JSON.parse(res);
                            var num_places_result = res.length;
                            if(num_places_result==20) {
                                $('#numAllResult').text(num_places_result+'+');
                            }
                            else {
                                $('#numAllResult').text(num_places_result);
                            }

                            parent_body.find('#allSearchResults').empty();

                            if (num_places_result === 0) {
                                parent_body.find('div#allSearchResults').html('<span class="no-results">No results found.</span>');
                            }

                            placeResults = res;

                            renderPlaceResults();

                            $('div#allSearchResults').eq(1).parent().scrollTop(0);
                        });
                }
            }
        }, 500));

        var cityReq = null;

        $('input#cities_id').keyup(delay(function (e) {
            let value = $(e.target).val();
            var obj = $(this);
            var parent_body = $(this).closest('.modal.modal-sidebar');

            var search_in = false;
            var search_id = false;
            var active_key = false;

            if (e.keyCode === 46 || e.keyCode === 8) {
                parent_body.find(".map-select-city").html(`
                                        <div id="allSearchResults"></div>
                                    `);
            }

            if (value.length >= 3) {
                if ( active_key === false ) {
                    $('div.cities').addClass('input-group-focus');

                    parent_body.find(".map-select-city").html(`
                                        <div id="allSearchResults"></div>
                                    `);

                    if (cityReq) {
                        cityReq.abort();
                    }

                    cityReq = $.ajax({
                        method: "GET",
                        url: "{{url('trip/ajaxSearchCitiesForSelect')}}",
                        data: {
                            term: obj.val()
                        }
                    })
                        .done(function (res) {
                            res = JSON.parse(res)
                            res = res.results;


                            var num_places_result = res.length;
                            if(num_places_result==20) {
                                $('#numAllResult').text(num_places_result+'+');
                            }
                            else {
                                $('#numAllResult').text(num_places_result);
                            }
                            parent_body.find('#allSearchResults').empty();

                            res.forEach(function (element) {
                                if (!element.image) {
                                    element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
                                } else {
                                    element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.image;
                                }


                                var cityTitle = element.text.replace(/'/g, "\\'");
                                var title = cityTitle + ', ' + element.country;
                                parent_body.find('.map-select-city #allSearchResults').append('<div class="drop-row" style="cursor:pointer;" onclick="cityChosen($(this).closest(\'.modal.modal-sidebar\'), \''+element.id+'\', \''+title+'\', \''+element.lat+'\', \''+element.lng+'\', \'' + cityTitle + '\');$(this).closest(\'.modal.modal-sidebar\').find(\'.input-group-img.city\').addClass(\'chosen\'); $(this).closest(\'.modal.modal-sidebar\').find(\'.input-group-img.city\').css(\'background-image\', \'url(' + element.img + ')\'); $(this).closest(\'.modal.modal-sidebar\').find(\'#cities_id\').val(\'' + title + '\');$(this).closest(\'.modal.modal-sidebar\').find(\'#cities_real_id\').val(\''+element.id+'\'); getSuggesstedPlaces('+element.id+',$(this).closest(\'.modal.modal-sidebar\'));"><div class="img-wrap rounded place-img"><img src="' + element.img + '" id="POIImage'+element.id+'" alt="logo" style="width:60px;height:60px;"></div><div class="drop-content place-content"><div class="title"><h3 class="content-ttl" style="font-size: 18px;text-align: left;">' + element.text + '</h3></div><p class="place-name" style="width:100%;text-align: left;"><span>' + element.country + '</span></p></div></div>');
                            });

                            if (res.length === 0) {
                                $('div#allSearchResults').html('<span class="no-results">No results found.</span>');
                            }
                        });
                }
            } else if (value.length === 0) {
                clearCityAndPlaceFields(parent_body);
            }
        }));


        $('#cities_id').on('select2:select', function (e) {
            var data = e.params.data;
            var selected_city_id = data.id;
            let value = $(e.target).val();
            var obj = $(this);
            var parent_body = $(this).closest('.modal.modal-sidebar');


            var search_in = false;
            var search_id = false;
            var active_key = false;
                    $('#dropCont').addClass('input-group-focus');

                    //$(this).parents('.header-search-block').addClass('show');
                    //$(".location-identity").hide();
                    parent_body.find(".map-select-place").html(`
                                        <div id="allSearchResults"></div>
                                    `);

                    // start searching all POI types
                    $.ajax({
                        method: "GET",
                        url: "{{url('home/suggestTopPlaces')}}",
                        data: {
                            city_id: selected_city_id
                        }
                    })
                        .done(function (res) {
                            res = JSON.parse(res);
                            var num_places_result = res.length;
                            if(num_places_result==20) {
                                $('#numAllResult').text(num_places_result+'+');
                            }
                            else {
                                $('#numAllResult').text(num_places_result);
                            }
                            parent_body.find('#allSearchResults').empty();

                            res.forEach(function (element) {
                                if (!element.image) {
                                    element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
                                } else {
                                    element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.image;
                                }
                                parent_body.find('#allSearchResults').append('<div class="drop-row" data-reviewscount="'+ reviews.length +'" data-reviewsavg="'+ reviews_avg +'" data-cityid="'+element.city_id+'" data-citytitle="'+element.city_title+'" data-id="'+element.id+'" data-address="'+element.address+'" data-lat="'+element.lat+'" data-lng="'+element.lng+'" data-title="'+element.title+'" style="cursor:pointer;"><div class="img-wrap rounded place-img"><img src="' + element.image + '" id="POIImage'+element.id+'" alt="logo" style="width:60px;height:60px;"></div><div class="drop-content place-content"><div class="title"><h3 class="content-ttl" style="font-size: 18px;text-align: left;">' + element.title + '</h3><span>Suggested</span></div><p class="place-name" style="width:100%;text-align: left;"><span>' + element.address + '</span></p></div></div>');

                            });
                        });

        });


        if(getUrlVars()['action']){
          $('#modal-add_place').modal('show');
        }

        $(document).on('click', '.drop-row', function (e) {
            $(this).parents('div.input-group-dropdown').parent().find('.input-group-focus').removeClass('input-group-focus');

            $('div.map-placeholder-block').hide();
            showHint = false;
        });

        $(document).on('click', 'div.modal-place', function(e) {
            if (!$(e.target).parents('div.drop-row').length && !$(e.target).hasClass('places_id') && !$(e.target).parent().hasClass('location-tab-list')) {
                $(this).find('.input-group-focus').removeClass('input-group-focus');
            }
        });

        $('div#maps').on('click', '.add-map-place', function() {
            $('#maps canvas').trigger("click");

            var country = $(this).data('countrytitle');
            var placeId = $(this).data('placeid');
            var title = $(this).data('title');

            var city = $(this).data('citytitle');
            var cityId = $(this).data('cityid');
            var placeSrc = $(this).data('placesrc');

            var cityImg = $(this).data('cityimg');

            var modal = $('#modal-add_place');

            if ($('#modal-edit_place').hasClass('show')) {
                modal = $('#modal-edit_place');
            }

            modal.modal('show');
            $('div.modal-backdrop').hide();

            if (country) {
                $(modal).find('input#cities_id').val(country);
                $(modal).find('input#cities_id').trigger('keyup');
            }

            if (city && cityId) {
                $(modal).find('input#cities_id').val(city);
                $(modal).find('input#cities_real_id').val(cityId);

                if (!title || !placeId) {
                    $(modal).find('input#cities_id').trigger('keyup');
                }

                selected_city_id = cityId;

                if (cityImg) {
                    $(modal).find('div.input-group-img.city').css('background-image', 'url(' + cityImg + ')');
                    $(modal).find('div.input-group-img.city').css('background-size', 'cover');
                }
            }

            if (title && placeId) {
                $(modal).find('input#places_id').val(title);
                $(modal).find('input#places_real_id').val(placeId);
                $(modal).find('div.input-group-img.place').css('background-image', 'url(' + placeSrc + ')');
                $(modal).find('div.input-group-img.place').css('background-size', 'cover');

                $(modal).find('div.place.disabled').removeClass('disabled');
                $(modal).find('input#places_id').removeClass('disabled');
                $(modal).find('input#places_id').prop('disabled', false);

                $('div.trending[data-id=' + placeId + ']').addClass('active');
                $('div.trending[data-id=' + placeId + ']').trigger('click');
            }
        });

        $('div.map-search, div.add-place').on('click', '.drop-row', function (e) {
            clearMarkers();

            isSuggestedPlaces = false;

            var lat = $(this).data('lat');
            var lng = $(this).data('lng');
            var title = $(this).data('title');
            var address = $(this).data('address');
            var placeId = $(this).data('id');
            var route = '/place/' + placeId
            var src = $(this).find('img').attr('src');
            var cityId =  $(this).data('cityid');
            var cityTitle =  $(this).data('citytitle');
            var cityImg =  $(this).data('cityimg');

            var reviewsCount = $(this).data('reviewscount');
            var reviewsAvg = $(this).data('reviewsavg');

            var locationType = 0;

            if (typeof placeId === 'string' && placeId.includes('city_')) {
                placeId = placeId.replace('city_', '');
                cityImg = src;
                locationType = 1;
            }

            var contentString = generatePopup(
                placeId,
                src,
                title,
                address,
                reviewsAvg,
                reviewsCount,
                new Date().toISOString().split('T')[0],
                cityId,
                cityTitle,
                [],
                editable,
                false,
                null,
                '',
                '',
                cityImg,
                locationType
            );

            var popup = new mapboxgl.Popup()
                .setHTML(contentString);

            popup.on('open', onPopupOpen);

            var el = $(document.createElement('div'));
            var style = 'background: url(\' ' + src + '\');' +
                'background-size: cover;' +
                'background-repeat: no-repeat;' +
                'background-position: center center;' +
                'width: 41px;' +
                'height: 41px;' +
                'border-radius: 50px;' +
                'border: 2px solid #fff !important;' +
                'cursor: pointer;' +
                'z-index: 1;';

            $(el).addClass('place-marker marker active');
            $(el).attr('style', style);
            $(el).attr('data-placeid', placeId);

            var lnglat = [parseFloat(lng), parseFloat(lat)];

            $(el)[0].addEventListener('mouseenter', function() {
                popup.addTo(window.plan_map);
            });

            $(el)[0].addEventListener('mouseleave', function() {
                if (!$(popup.getElement()).is(":hover")) {
                    popup.remove();
                } else {
                    $(popup.getElement())[0].addEventListener('mouseleave', function (e) {
                        popup.remove();
                    });
                }
            });

            $(el)[0].addEventListener('click', function() {
                updateTrendingData(0, placeId, 1, [], true);
            });

            var marker = new mapboxgl.Marker($(el)[0])
                .setLngLat(lnglat)
                .setPopup(popup)
                .addTo(window.plan_map);

            searchMarkers.push(marker);
            searchMarkersCoordinates.push(lnglat);

            updateTrendingData(0, placeId, 1, [], true);

            map.flyTo({
                center: lnglat,
                essential: true,
                speed: 1,
                curve: 1,
                zoom: 15
            });

            var source = window.plan_map.getSource('route0');

            if (source) {
                //tempCoords = lnglat;
            }

            if ($(this).hasClass('drop-map-row')) {
                $('.drop-map-row').closest('.modal-place').find('.map-place-search').val(title);
            }

            $.ajax({
                method: "GET",
                url: "{{url('places/ajax_get_media')}}",
                data: {
                    place_id: placeId
                }
            })
                .done(function (res) {
                });

            var src = $(this).find('.img-wrap img').attr('src');

            if (src) {
                $(this).closest('.modal.modal-sidebar').find('.input-group-img.place').css('background-image', 'url(' + src + ')');
                $(this).closest('.modal.modal-sidebar').find('.input-group-img.place').css('background-size', 'cover');
            }
        });


        $(document).on('click', 'div.card-inner', function (e) {
            if ($(e.target).hasClass('testButton') || $(e.target).hasClass('editPlaceTrigger')) {
                return;
            }

            moveToMarker($(this).parents('div.trip-place-wrap').data('placeid'));
            updateTrendingData(0, $(this).parents('div.trip-place-wrap').data('origplaceid'), 1, [], true);
            //triggerPlaceChatsWindow($(this).parents('div.trip-place-wrap').data('origplaceid'));
        });

        $(document).on('click', 'button.editPlaceTrigger', function (e) {
            moveToMarker($(this).parents('div.trip-place-wrap').data('placeid'));
            updateTrendingData(0, $(this).parents('div.trip-place-wrap').data('origplaceid'), 1, [], true);
            //triggerPlaceChatsWindow($(this).parents('div.trip-place-wrap').data('origplaceid'));

            $('button.doEditPlace').removeAttr('disabled');
        });

        $(document).on('click', '#enableEditForTitle', function (e) {
            $('#review_title').attr('readonly', false);
            $("#review_title").focus();
          });
          $(document).on('click', '#enableEditForDescription', function (e) {
            $('#review_description').attr('readonly', false);
            $("#review_description").focus();
          });
        $(document).on('click', '.testButton', function (e) {
            $(this).parents('.card-toggle').find('.card-collapse').collapse('toggle');

            if ($('div.sidebar.invited').length) {
                $('.trip-day').addClass('mobile-hide');
                $('.trip-place-wrap').addClass('mobile-hide');
                $('.trip-place-wrap div.suggestion').addClass('mobile-hide');
                $('.trip-place-wrap div.activity-info').addClass('mobile-hide');

                modals.push({
                    type: 'place',
                    element: ''
                });

                window.history.pushState('forward', null, "");
            }

            $(this).parents('.trip-place-wrap').removeClass('mobile-hide');
            $(this).parents('.trip-day').removeClass('mobile-hide');

            $(this).parents('.card-toggle').find('.card-collapse').on('hidden.bs.collapse shown.bs.collapse', function (e) {
                updatePositions();
            })

            var tripPlaceId = $(this).parents('.trip-place').data('id');

            if (expandedPlaces[tripPlaceId]) {
                expandedPlaces[tripPlaceId] = 0;
            } else {
                expandedPlaces[tripPlaceId] = 1;
            }
          });

        // Mobile view
        $(document).on('click', '.mobile-collapse-toggler', function () {
            $(this).parents('.card-toggle').find('.card-collapse').collapse('toggle');
            $('.trip-day').removeClass('mobile-hide');
            $('.trip-place-wrap').removeClass('mobile-hide');
            $('.trip-place-wrap div.suggestion').removeClass('mobile-hide');
            $('.trip-place-wrap div.activity-info').removeClass('mobile-hide');
            $('div.trip').trigger('scroll');
        });

        $(document).on('click', 'button.btn-back-mobile', function() {
            if ($('div.card-trip_info.show').length) {
                var currentPlaceId = $('.trip-place-wrap:visible').data('placeid');
                $('div.card-trip_info.show button.mobile-collapse-toggler').trigger('click');
                scrollSideBar(currentPlaceId, 0);
                modals.pop();
            } else {
                $('.mobile-trip-view-mode').show();
            }
        });

        $(document).on('click', '.editPlaceTrigger', function (e) {
            uploader.unbindAll();
            editUploader.unbindAll();
            suggestUploader.unbindAll();
            uploader.destroy();
            editUploader.destroy();
            suggestUploader.destroy();

            addedImages = [];
            var self = this;
            $(this).attr('disabled', 'disabled');
            var memory = $(this).data('memory');

            $('div#modal-edit_place div#edit-media-container div.swiper-slide').remove();
            $('div#modal-edit_place div#edit-media-container').append('<div class="swiper-slide" id="edit-img-upload">\n' +
                '                                                <button type="button" class="img-upload"></button>\n' +
                '                                            </div>');

            $('div#modal-edit_place input#known_for').tagsinput('removeAll');
            $('div#modal-edit_place #edit_time').val('');

            $('div#modal-edit_place div.story').html('');

            $('div#modal-edit_place button.add-story').show();
            $('div#modal-edit_place div#modal-story').hide();
            $('div#modal-edit_place .input-group-focus').removeClass('input-group-focus');

            $('b.overall-budget').text('$' + $(this).parents('div.trip-place-wrap').data('spent'));

            var place_id = $(this).attr('data-id');
            $.ajax({
                method: "GET",
                url: "{{url('trip/ajaxGetTripPlace')}}",
                data: {"place_id": place_id}
            })
                .done(function (res) {
                    $(self).removeAttr('disabled');
                    $('div#modal-edit_place_hidden.memory').attr('id', 'modal-edit_place');
                    $('div#modal-edit_place:not(".memory")').attr('id', 'modal-edit_place_hidden');
                    $('div#modal-edit_place.memory').modal('show');
                    $('div.modal-backdrop').hide();

                    $('div.card-activity[data-tripplaceid="' + place_id + '"]').find('span.activity-name span.new').remove();
                    $('div#modal-edit_place').find('span.new').remove();

                    var pinfo = JSON.parse(res);

                    var date = new Date(pinfo.date);
                    var today = new Date();

                    if (date >= today) {
                        disableMemoryFields($("div#modal-edit_place"));
                    } else {
                        enableMemoryFields($("div#modal-edit_place"));
                    }

                    if(pinfo.duration > (60 * 24)) {
                        var days = Math.trunc(pinfo.duration / (60 * 24));
                    }
                    else {
                        var days = 0;
                    }

                    pinfo.duration = pinfo.duration - days * (60 * 24);

                    if(pinfo.duration > 60) {
                        var hours = Math.trunc(pinfo.duration / 60);
                    }
                    else {
                        var hours = 0;
                    }

                    var minutes = pinfo.duration - (hours*60);

                    if (!pinfo.without_time) {
                        $('div#modal-edit_place #edit_time').val(pinfo.timeForPicker);
                    }

                    $('div#modal-edit_place #order').val(pinfo.order);
                    $('div#modal-edit_place #edit_duration_days').val(days);
                    $('div#modal-edit_place #edit_duration_days').parent().find('span').attr('data-before', days);
                    $('div#modal-edit_place #edit_duration_hours').val(hours);
                    $('div#modal-edit_place #edit_duration_hours').parent().find('span').attr('data-before', hours);
                    $('div#modal-edit_place #edit_duration_minutes').val(minutes);
                    $('div#modal-edit_place #edit_duration_minutes').parent().find('span').attr('data-before', minutes);
                    $('div#modal-edit_place #edit_budget_custom').val(pinfo.budget);

                    if (pinfo.budget) {
                        $('div#modal-edit_place #edit_budget_custom').change();
                    }

                    if (pinfo.duration) {
                        $('div#modal-edit_place #edit_duration_hours').change();
                    }

                    $('div#modal-edit_place input#places_id').val(pinfo.places_name);
                    $('div#modal-edit_place input#places_real_id').val(pinfo.places_id);
                    $('div#modal-edit_place input#trip_places_real_id').val(pinfo.id);

                    $('div#modal-story div.story').html(pinfo.story);

                    $('div#modal-edit_place #removePlace').attr('data-id', pinfo.id);

                    $("div#modal-edit_place .datepicker_place").datepicker({
                        changeYear: true,
                        dateFormat: "d MM yy",
                        altField: "div#modal-edit_place #actual_place_date",
                        altFormat: "yy-mm-dd",
                        onSelect: function(dateText) {
                            var date = new Date(dateText);
                            var today = new Date();

                            if (date >= today) {
                                disableMemoryFields($("div#modal-edit_place"));
                            } else {
                                enableMemoryFields($("div#modal-edit_place"));
                            }
                        },
                        defaultDate: ""
                        @if(isset($trip) && $trip->memory)
                        ,maxDate: 0
                        @elseif(isset($trip) && !$trip->memory)
                        ,minDate: 0
                        @endif
                    });

                    $("div#modal-edit_place .datepicker_place").datepicker("setDate", new Date(pinfo.dateForPicker));
                    $("div#modal-edit_place .datepicker_place").datepicker('option', {altField: "div#modal-edit_place #edit_actual_place_date",altFormat: "yy-mm-dd"});

                    if(pinfo.budget==50) {
                        $('div#modal-edit_place .amountMoney1').prop('checked', true);
                    }
                    else if(pinfo.budget==100) {
                        $('div#modal-edit_place .amountMoney2').prop('checked', true);
                    }
                    else if(pinfo.budget==200) {
                        $('div#modal-edit_place .amountMoney3').prop('checked', true);
                    } else {
                        $('div#modal-edit_place .amountMoney1').prop('checked', false);
                        $('div#modal-edit_place .amountMoney2').prop('checked', false);
                        $('div#modal-edit_place .amountMoney3').prop('checked', false);
                    }

                    $('div#modal-edit_place input.input.cities_id').change();
                    $('div#modal-edit_place .input-group-img.city').addClass('chosen');
                    $('div#modal-edit_place .input-group-img.city').css('background-image', 'url(' + pinfo.cities_img + ')');
                    $('div#modal-edit_place .input-group-img.city').css('background-size', 'cover');

                    $('div#modal-edit_place .input-group-img.place').addClass('chosen');
                    $('div#modal-edit_place .input-group-img.place').css('background-image', 'url(' + pinfo.places_img + ')');
                    $('div#modal-edit_place .input-group-img.place').css('background-size', 'cover');

                    $('div#modal-edit_place input#cities_id').val(pinfo.cities_name);
                    $('div#modal-edit_place input#cities_real_id').val(pinfo.cities_id);

                    if ($('#modal-edit_place').find('div.swiper-slide').length < 10) {
                        $('#modal-edit_place').find('div#edit-img-upload').show();
                    }

                    window.selected_city_id = pinfo.cities_id;
                    window.selected_city_title = pinfo.cities_name;
                    selected_city_name = pinfo.original_cities_name;
                    window.locationLng = pinfo.cities_lng;
                    window.locationLat = pinfo.cities_lat;

                    pinfo.images.forEach(function (e) {
                        $('div#modal-edit_place div#edit-media-container').parents('.optional').removeClass('optional');
                        var append = '<div data-placeid="' + pinfo.places_id + '" data-tripid="' + pinfo.trips_id + '" data-mediaid="' + e.media.id + '" class="swiper-slide"><img src="'+e.media.url+'" class="img-upload"><div class="remove"></div></div>';
                        if (e.media.url.split('.').pop() === 'mp4') {
                            append = '<div data-placeid="' + pinfo.places_id + '" data-tripid="' + pinfo.trips_id + '" data-mediaid="' + e.media.id + '" class="swiper-slide"><video poster="'+ e.media.source_url + '" src="'+e.media.url+'" class="img-upload"></video><div class="remove"></div></div>';
                        }
                        $('div#modal-edit_place div#edit-media-container').prepend(append);
                        addedImages.push(e.media.id);
                    });

                    if (pinfo.images.length >= 10) {
                        $('div#modal-edit_place div#edit-media-container div#edit-img-upload').hide();
                    }

                    if (pinfo.comment) {
                        pinfo.comment.split(',').forEach(function (e, i) {
                            $('div#modal-edit_place input#known_for').tagsinput('add', e);
                        });
                    }

                    if (pinfo.updated_attributes.length) {
                        pinfo.updated_attributes.forEach(function (e, i) {
                            $('div#modal-edit_place').find('label.' + e).find('span.new').remove();
                            $('div#modal-edit_place').find('label.' + e).prepend('<span class="new" style="display: inline-block; border: 1px solid #ff5a79; height: 5px;width: 5px;border-radius: 5px; background: #ff5a79; margin-right: 7px; margin-bottom: 2px;"></span>');
                        });

                        var unreadCount = $('div.logs_unread').text();
                        unreadCount--;

                        if (!unreadCount) {
                            $('div.logs_unread').remove();
                        } else {
                            $('div.logs_unread').text(unreadCount);
                            $('div.logs_unread').show();
                        }
                    }

                    $('.bootstrap-tagsinput input').each(function () {
                        if ($(this).parent().find('span').text()) {
                            $(this).trigger('blur');
                        }
                    });

                    initUploaders();
                });
          });

        $(document).on('click', '.dismissPlaceTrigger', function (e) {
            autoSave();
            var placeId = $(this).attr('data-id');

            $.ajax({
                method: "POST",
                url: "{{url('trip/ajax-dismiss-place-from-trip')}}",
                data: {"place_id": placeId}
            })
                .done(function (res) {
                    if (res.status == 'success') {
                        autoSave(true);
                        updatePlanContents();
                    }

                }).fail(function(data) {
                if (data.status == 403) {
                    location.reload();
                }
            });
            e.preventDefault();
        });

        $(document).on('click', '.showPlaceTrigger', function (e) {
            $('div#modal-show_place_suggestions').modal('show');

            $('div#modal-show_place_suggestions' +
                '_place div#edit-media-container div.swiper-slide:not("#edit-img-upload")').remove();
            var place_id = $(this).attr('data-id');
            $.ajax({
                method: "GET",
                url: "{{url('trip/ajaxGetTripPlace')}}",
                data: {"place_id": place_id}
            })
                .done(function (res) {
                    $('div.card-activity[data-tripplaceid="' + place_id + '"]').find('span.activity-name span.new').remove();
                    $('div#modal-show_place_suggestions').find('span.new').remove();

                    var pinfo = JSON.parse(res);

                    if(pinfo.duration > (60 * 24)) {
                        var days = Math.trunc(pinfo.duration / (60 * 24));
                    }
                    else {
                        var days = 0;
                    }

                    pinfo.duration = pinfo.duration - days * (60 * 24);

                    if(pinfo.duration > 60) {
                        var hours = Math.trunc(pinfo.duration / 60);
                    }
                    else {
                        var hours = 0;
                    }

                    var minutes = pinfo.duration - (hours*60);

                    $('div#modal-show_place_suggestions #edit_time').val(pinfo.timeForPicker);
                    $('div#modal-show_place_suggestions #order').val(pinfo.order);
                    $('div#modal-show_place_suggestions #edit_duration_days').val(days);
                    $('div#modal-show_place_suggestions #edit_duration_hours').val(hours);
                    $('div#modal-show_place_suggestions #edit_duration_minutes').val(minutes);
                    $('div#modal-show_place_suggestions #edit_budget_custom').val(pinfo.budget);

                    if (pinfo.budget) {
                        $('div#modal-show_place_suggestions #edit_budget_custom').change();
                    }

                    if (pinfo.duration) {
                        $('div#modal-show_place_suggestions #edit_duration_hours').change();
                    }

                    $('div#modal-show_place_suggestions input#places_id').val(pinfo.places_name);
                    $('div#modal-show_place_suggestions input#places_real_id').val(pinfo.places_id);
                    $('div#modal-show_place_suggestions input#trip_places_real_id').val(pinfo.id);
                    $('div#modal-show_place_suggestions div.story').html(pinfo.story);

                    $('div#modal-show_place_suggestions #removePlace').attr('data-id', pinfo.id);

                    $("div#modal-show_place_suggestions .datepicker_place").datepicker({
                        changeYear: true,
                        dateFormat: "d MM yy",
                        altField: "div#modal-show_place_suggestions #actual_place_date",
                        altFormat: "yy-mm-dd",
                        defaultDate: ""
                        @if(isset($trip) && $trip->memory)
                        ,maxDate: 0
                        @elseif(isset($trip) && !$trip->memory)
                        ,minDate: 0
                        @endif
                    });

                    $("div#modal-show_place_suggestions .datepicker_place").datepicker("setDate", new Date(pinfo.dateForPicker));
                    $("div#modal-show_place_suggestions .datepicker_place").datepicker('option', {altField: "div#div#modal-show_place_suggestions #edit_actual_place_date",altFormat: "yy-mm-dd"});

                    if(pinfo.budget==50) {
                        $('div#modal-show_place_suggestions .amountMoney1').attr('checked', 'checked');
                    }
                    else if(pinfo.budget==100) {
                        $('div#modal-show_place_suggestions .amountMoney2').attr('checked', 'checked');
                    }
                    else if(pinfo.budget==200) {
                        $('div#modal-show_place_suggestions .amountMoney3').attr('checked', 'checked');
                    }

                    $('div#modal-show_place_suggestions input.input.cities_id').change();
                    $('div#modal-show_place_suggestions .input-group-img.city').addClass('chosen');
                    $('div#modal-show_place_suggestions .input-group-img.city').css('background-image', 'url(' + pinfo.cities_img + ')');
                    $('div#modal-show_place_suggestions .input-group-img.city').css('background-size', 'cover');

                    $('div#modal-show_place_suggestions .input-group-img.place').addClass('chosen');
                    $('div#modal-show_place_suggestions .input-group-img.place').css('background-image', 'url(' + pinfo.places_img + ')');
                    $('div#modal-show_place_suggestions .input-group-img.place').css('background-size', 'cover');

                    $('div#modal-show_place_suggestions input#cities_id').val(pinfo.cities_name);
                    $('div#modal-show_place_suggestions input#cities_real_id').val(pinfo.cities_id);

                    pinfo.images.forEach(function (e) {
                        $('div#modal-show_place_suggestions div#edit-media-container').parents('.optional').removeClass('optional');
                        var append = '<div class="swiper-slide"><img src="'+e.media.url+'" class="img-upload"></div>';
                        if (e.media.url.split('.').pop() === 'mp4') {
                            append = '<div class="swiper-slide"><video src="'+e.media.url+'" class="img-upload"></div>';
                        }
                        $('div#modal-show_place_suggestions div#edit-media-container').prepend(append);
                    });

                    if (pinfo.comment) {
                        pinfo.comment.split(',').forEach(function (e, i) {
                            $('div#modal-edit_place input#known_for').tagsinput('add', e);
                        });
                    }

                    if (pinfo.updated_attributes) {
                        pinfo.updated_attributes.forEach(function (e, i) {
                            $('div#modal-show_place_suggestions').find('label.' + e).find('span.new').remove();
                            $('div#modal-show_place_suggestions').find('label.' + e).prepend('<span class="new" style="display: inline-block; border: 1px solid #ff5a79; height: 5px;width: 5px;border-radius: 5px; background: #ff5a79; margin-right: 7px; margin-bottom: 2px;"></span>');
                        });

                        var unreadCount = $('div.logs_unread').text();
                        unreadCount--;

                        if (!unreadCount) {
                            $('div.logs_unread').remove();
                        } else {
                            $('div.logs_unread').text(unreadCount);
                            $('div.logs_unread').show();
                        }
                    }
                });
        });
        // place_selected_flag = '{{$pre_selected_place}}';
        // if(place_selected_flag){
        //    var pre_selected_place = {!! json_encode(@$selected_place) !!};
        //    var pre_selected_city =  {!! json_encode(@$selected_city) !!};
        //    if(typeof pre_selected_place.id !='undefined' && typeof pre_selected_city !='undefined'){
        //        $('#form_trip_place').find('input#cities_id').val(pre_selected_city.title);
        //        $('#form_trip_place').find('.city').css('background-image', 'url(' + pre_selected_city.url + ')');
        //        $('#form_trip_place').find('#cities_real_id').val(pre_selected_city.url);
        //        $('#form_trip_place').find('.place').css('background-image', 'url(' + pre_selected_place.url + ')');
        //        $('#form_trip_place').find('input#places_id').val(pre_selected_place.title);
        //        $('#form_trip_place').find('#places_real_id').val(pre_selected_place.id);
        //    }
        // }
    });


    /*$('#cities_id').select2({
        ajax: {
            placeholder: 'Type to search for a City...',
            url: "{{url('trip/ajaxSearchCitiesForSelect')}}",
            dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        },
        dropdownParent: $('#modal-add_place .modal-content'),
        containerCssClass: "select2-input"
    });*/


        @if (isset($trip_id) AND $trip_id>0 AND isset($do) AND $do=="add-city")
        $(document).ready(function () {
            $('#addCityPopup').modal('show');
        });
        @endif
        @if (isset($city) AND isset($do) AND $do=="add-place")
        $(document).ready(function () {
            $('#addPlacePopup').modal('show');
        });
        @endif

        $(document).ready(function () {
            $('#modal-edit_place input[name=edit_budget]').on('click', function (e) {
                $('#modal-edit_place #edit_budget_custom').val($(this).val());
                $('#modal-edit_place #edit_budget_custom').change();
                 $('#modal-edit_place .spending_amount').html('$' + $(this).val());

            });

            $('input[name=budget]').on('click', function (e) {
                $('#budget_custom').val($(this).val());
                $('#budget_custom').change();
                 $('.spending_amount').html('$' + $(this).val());

            });

            $('body').on('change keyup', '#modal-final_step #review_title', function(e) {
                var val = $(this).val();

                if (!val) {
                    $('#btnPublishTrip').addClass('disabled');
                } else {
                    $('#btnPublishTrip').removeClass('disabled');
                }
            });

            $('body').on('click', '#btnPublishTrip', function (e) {
                var trip_id = $('#trip_id').val();
                var version_id = $('#version_id').val();
                var review_title = $('#modal-final_step #review_title:visible').val();
                var review_description = $('#modal-final_step #review_description:visible').val();
                var privacy = $('input.custom-control-input[name=privacy_create_trip]:checked').val();

                if ($('#send_back').is(":checked")) {
                    var send_back = $('#send_back').val();
                }

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxActivateTrip')}}",
                    data: {"trip_id": trip_id, "version_id": version_id, "title": review_title, "description": review_description, "send_back": send_back, "privacy": privacy}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            $('#modal-final_step').modal('hide');
                            $('#modal-share').modal('show');
                        } else if (result.status == 'error_unknow') {
                            alert('{{ __("trip.unknown_error_while_deleting_trip_plan") }}');
                        } else if (result.status == 'error_not_enough_data') {
                            alert('{{ __("trip.you_need_to_add_at_least_one_place_to_publish_your_trip") }}');
                        }

                    }).fail(function(data) {
                    if (data.status == 403) {
                        location.reload();
                    }
                });
                e.preventDefault();
            });

            $('body').on('click', '.btnInviteUser', function(e){
                var obj = $(this);
                var trip_id = $('#trip_id').val();
                var users_id = obj.attr('attr-id');
                obj.attr('disabled', true);
                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxInviteFriends')}}",
                    data: {"trip_id": trip_id, "users_id": users_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            obj.html(result.message);
                        } else {
                            alert(result.message);
                        }

                    }).fail(function(data) {
                    if (data.status == 403) {
                        location.reload();
                    }
                });
            });
        });
        $(document).ready(function () {
            $('body').on('click', '.privacy-button', function (e) {
               $($(this).find("input[type=radio]")).prop("checked", true);
               $('.privacy-button').each(function(i, obj) {
                    $(obj).removeClass('btn-transp')
                    $(obj).removeClass('btn-light-primary')
                   if($($(obj).find("input[name='privacy']:checked")).val()){
                         $(obj).addClass('btn-light-primary')
                   }else{
                        $(obj).addClass('btn-transp')
                   }
                });
            });

            $('body').on('keyup', '#budget_custom, #edit_budget_custom', function (e) {
                if(!$.isNumeric($(this).val())){
                    $(this).val('')
                }else{
                   $('.spending_amount').html('$' + $(this).val());
                }
                $('input[name=budget]').prop('checked', false);
            });

             $('body').on('click', '.city-content', function (e) {
                 window.location.href="?city_id=" + $(this).data('city_id')
             });

            $('body').on('submit', '#createTrip', function (e) {
                var values = new FormData($('#createTrip')[0]);

                if (!$('#modal-create_trip input#cover').val() && values.get('plan_type') == "1") {
                    $('#modal-create_trip div.cover-upload').addClass('required');
                    return;
                }

                $('#modal-create_trip button[type=submit]').attr('disabled', 'disabled');

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxFirstStep')}}",
                    data: values,
                    processData: false,
                    contentType: false
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            var url = "{{url('trip/plan')}}" + "/" + result.trip_id + "?do=edit";
                            if (add && addType) {
                                url += '&add=' + add + '&add-type=' + addType;
                            }

                            window.location.replace(url);
                        } else {

                        }


                    });
                e.preventDefault();
            });


            $('body').on('submit', '#editTrip', function (e) {
                var values = new FormData($('#editTrip')[0]);
                var planName =  $(this).find('input[name=title]').val();

                var title = $(this).find('input[name=title]').val();
                var description = $(this).find('textarea[name=description]').val();
                var privacy = $(this).find('input[name=privacy]:checked').val();

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxEditTrip')}}",
                    data: values,
                    processData: false,
                    contentType: false
                })
                    .done(function (res) {
                        var result = JSON.parse(res);

                        if (result.status == 'success') {
                            $('span.navbar-text').text(planName);
                            $('span.tooltiptext').text(planName);

                            $('div#modal-edit_title').modal('hide');

                            $('div#modal-final_step').find('input[name=title]').val(title);
                            $('div#modal-final_step').find('textarea[name=description]').val(description);
                            $('div#modal-final_step').find('input[name=privacy_create_trip][value='+privacy+']').prop("checked",true);

                            if (result.data.cover) {
                                $('div#modal-final_step').find('div.cover').addClass('with-image');
                                $('div#modal-final_step').find('div.cover').attr('style', 'background: url("' + result.data.cover + '");');
                            }
                        }
                    }).fail(function(data) {
                        if (data.status == 403) {
                            location.reload();
                        }
                    });
                e.preventDefault();
            });

            // Delete trip city and place
            $('body').on('click', '.removeCity', function (e) {
                $('.confirm-trip-city-delete').attr('data-id', $(this).data('city_id'))
              $('#deleteTripCityModal').modal('show')
            });


          @if(isset($trip) && is_object($trip))
            $('body').on('click', '.confirm-trip-city-delete', function (e) {
                var trip_id = {{$trip->id}};
                var city_id = $(this).data('id');
                var version_id = {{$trip->versions[0]->id}};
                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxRemoveCityFromTrip')}}",
                     data: {"city_id": city_id, "trip_id": trip_id, "version_id": version_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            if(result.last_city_id != 0){
                                 window.location.replace("{{url('trip/plan')}}/{{$trip->id}}?do=edit");
                            }else{
                                window.location.replace("{{url('trip/plan')}}/{{$trip->id}}?do=edit");
                            }
                        } else {

                        }
                    });
                e.preventDefault();
            });
        @endif

            @if(isset($city) && is_object($city))
            $('body').on('submit', '#activatePlace', function (e) {
                var values = $(this).serialize();
                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxActivateTripPlace')}}",
                    data: values
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            window.location.replace("{{url('trip/plan')}}" + "/"+{{$trip_id}}+
                            "?do=edit")
                            ;
                        } else {

                        }


                    });
                e.preventDefault();
            });

            @endif


            function delay(callback, ms) {
                var timer = 0;
                return function () {
                    var context = this, args = arguments;
                    clearTimeout(timer);
                    timer = setTimeout(function () {
                        callback.apply(context, args);
                    }, ms || 0);
                };
            }

            $('body').on('keyup', '#citySearchInput', delay(function (e) {
                var value = $(this).val();
                var selected_cities = '{{json_encode($existing_city_ids)}}';
                if (value.length >= 2) {
                    $.ajax({
                        method: "POST",
                        url: "{{url('trip/ajaxSearchCities')}}",
                        data: {"query": value, selected_cities: selected_cities}
                    })
                        .done(function (res) {
                            var result = JSON.parse(res);
                            $('#sugg-inner').html(result);

                        });
                }
                e.preventDefault();
            }, 500));


            @if(isset($trip))
            $('body').on('click', '.doAddCity', function (e) {
                var city_id = $(this).attr('data-id');
                var trip_id = {{$trip->id}};
                var version_id = {{$trip->versions[0]->id}};

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxAddCityToTrip')}}",
                    data: {"city_id": city_id, "trip_id": trip_id, "version_id": version_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            window.location.replace("{{url('trip/plan')}}" + "/" + trip_id + "?do=edit");
                        }

                    });
                e.preventDefault();
            });
            @endif


            $('body').on('keyup', '#placeSearchInput', delay(function (e) {
                var value = $(this).val();
                var selected_places = '{{json_encode($existing_place_ids)}}';
                if (value.length >= 2) {
                            @if(isset($city))
                    var city_id = {{$city->id}};
                    @endif
                    $.ajax({
                        method: "POST",
                        url: "{{url('trip/ajaxSearchPlaces')}}",
                        data: {"query": value, "city_id": city_id, selected_places: selected_places}
                    })
                        .done(function (res) {
                            var result = JSON.parse(res);
                            $('#place-sugg-inner').html(result);

                        });
                    e.preventDefault();
                }
            }, 500));
            @if(isset($trip))
            $('body').on('click', '.doAddPlace', function (e) {
                $(this).attr("disabled", true);
                var place_id = $('#modal-add_place').find('#places_real_id').val();
                var trip_id = {{$trip->id}};
                var city_id = $('#modal-add_place').find('#cities_real_id').val();
                var known_for = $('#known_for').val();
                var version_id = {{$trip->versions[0]->id}};

                var budget = $("input[name=budget]").val();
                var duration_days = $("#duration_days").val();
                var duration_hours = $("#duration_hours").val();
                var duration_minutes = $("#duration_minutes").val();
                var place_date = $("#modal-add_place #actual_place_date").val();
                var place_time = $("#modal-add_place #time").val();
                var order = $("#modal-add_place #order").val();

                var date = new Date(place_date);
                var today = new Date();

                var memory = {{$trip->memory}};

                toastr.options = {
                    timeOut: 3000,
                    extendedTimeOut: 0,
                    positionClass: "toast-bottom-right"
                };

                if (date > today && memory) {
                    toastr.error('Future date is not available for memory trip plans!');
                    $(this).removeAttr("disabled");
                    return false;
                }

                if (date < today && !memory) {
                    toastr.error('Past date is not available for upcoming trip plans!');
                    $(this).removeAttr("disabled");
                    return false;
                }

                if (city_id == "") {
                    toastr.error('Please select city!');
                    $(this).removeAttr("disabled");
                    return false;
                }

                if (place_id == "" || place_id == "null") {
                    toastr.error('Please select place!');
                    $(this).removeAttr("disabled");
                    return false;
                }

                if (place_date == "") {
                    toastr.error('Please select arrival date!');
                    $(this).removeAttr("disabled");
                    return false;
                }

                var story = $('div#modal-story div.story').html();

                autoSave();

                var self = this;

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxAddPlaceToTrip')}}",
                    data: {"city_id": city_id, "trip_id": trip_id, "place_id": place_id, "version_id": version_id,
                    "budget": budget, "duration_days": duration_days, "duration_hours": duration_hours, "duration_minutes": duration_minutes,
                    "date": place_date, "time": place_time, "known_for": known_for, "story": story, "order": order, 'medias': addedImages}
                })
                    .done(function (res) {
                        if (res.error == 'Too many rejected requests') {
                            toastr.error('You exceeded limit of 3 rejected requests');
                            return;
                        }

                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            autoSave(true);
                            addMediaToPlace(result.is_new)
                            updatePlanContents(result.trip_place_id);
                            $('button.doAddPlace').removeAttr("disabled");
                            $('#modal-add_place').modal('hide');
                            addButtonsAfterEdits();
                            updateChats();
                        } else {
                            toastr.error(result.data);
                            $('button.doAddPlace').removeAttr("disabled");
                            return;
                        }

                    }).fail(function(data) {
                    if (data.status == 403) {
                        location.reload();
                    }
                });
                e.preventDefault();
            });


            $('body').on('click', '.doEditPlace', function (e) {
                toastr.options = {
                    timeOut: 3000,
                    extendedTimeOut: 0,
                    positionClass: "toast-bottom-right"
                };

                autoSave();
                $(this).attr("disabled", true);

                var trip_place_id = $('div#modal-edit_place input#trip_places_real_id').val();
                var place_id = $('div#modal-edit_place input#places_real_id').val();
                var trip_id = {{$trip->id}};
                var city_id = $('div#modal-edit_place input#cities_real_id').val();
                var version_id = {{$trip->versions[0]->id}};

                var budget = $("div#modal-edit_place input[name=edit_budget]").val();
                var duration_days = $("div#modal-edit_place #edit_duration_days").val();
                var duration_hours = $("div#modal-edit_place #edit_duration_hours").val();
                var duration_minutes = $("div#modal-edit_place #edit_duration_minutes").val();
                var place_date = $("div#modal-edit_place #edit_actual_place_date").val();
                var place_time = $("div#modal-edit_place #edit_time").val();
                var order = $("div#modal-edit_place #order").val();
                var known_for = $('div#modal-edit_place #known_for').val();
                var story = $('div#modal-edit_place div#modal-story div.story').html();

                var date = new Date(place_date);
                var today = new Date();

                var memory = {{$trip->memory}};

                if (date > today && memory) {
                    toastr.error('Future date is not available for memory trip plans!');
                    $(this).removeAttr("disabled");
                    return false;
                }

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxEditPlaceInTrip')}}",
                    data: {"trip_place_id": trip_place_id, "city_id": city_id, "trip_id": trip_id, "place_id": place_id, "version_id": version_id,
                    "budget": budget, "duration_days": duration_days, "duration_hours": duration_hours, "duration_minutes": duration_minutes,
                    "date": place_date, "time": place_time, "known_for": known_for, "story": story, "order": order, "medias": addedImages}
                })
                    .done(function (res) {
                        autoSave(true);

                        if (res.error == 'Too many rejected requests') {
                            toastr.error('You exceeded limit of 3 rejected requests');
                            return;
                        }

                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            updatePlanContents(result.trip_place_id);
                            $(this).removeAttr("disabled");
                            $('div#modal-edit_place').modal('hide');
                            addButtonsAfterEdits();

                            $('button.editPlaceTrigger[data-id=' + trip_place_id + ']').attr('data-id', result.trip_place_id);

                            if (expandedPlaces[trip_place_id]) {
                                expandedPlaces[result.trip_place_id] = 1;
                            }
                        } else {
                            toastr.error(result.data);
                            $('button.doEditPlace').removeAttr("disabled");
                            return;
                        }

                    }).fail(function(data) {
                    if (data.status == 403) {
                        location.reload();
                    }
                });
                e.preventDefault();
            });

            $('body').on('click', '#removePlace', function (e) {
                toastr.options = {
                    timeOut: 3000,
                    extendedTimeOut: 0,
                    positionClass: "toast-bottom-right"
                };
                autoSave();
                var place_id = $(this).attr('data-id');
                var trip_id = {{$trip->id}};
                var city_id = 0;
                var version_id = {{$trip->versions[0]->id}};
                var realPlaceId = $(this).parents('div.modal-footer').find('input[name=places_real_id]').val();

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxRemovePlaceFromTrip')}}",
                    data: {"place_id": place_id}
                })
                    .done(function (res) {
                        if (res.error == 'Too many rejected requests') {
                            toastr.error('You exceeded limit of 3 rejected requests');
                            return;
                        }

                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            autoSave(true);
                            updatePlanContents(false, true, true);
                            $(this).removeAttr("disabled");
                            $('div#modal-edit_place').modal('hide');
                            addButtonsAfterEdits();
                            updateChats();
                        } else {
                        }

                    }).fail(function(data) {
                    if (data.status == 403) {
                        location.reload();
                    }
                });
                e.preventDefault();
            });
            @endif

            @if(isset($trip) && isset($city) && is_object($city))
            $('body').on('click', '.activateCity', function (e) {
                var trip_city_id = $(this).attr('data-id');
                var transportation = $("input[name='transportation']:checked").val();
                var version_id = {{$trip->versions[0]->id}};

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxActivateTripCity')}}",
                    data: {"trip_city_id": trip_city_id, "transportation": transportation, "version_id": version_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            window.location.replace("{{url('trip/plan')}}" + "/"+{{$trip_id}}+
                            "?do=edit");
                        }
                    });
                e.preventDefault();
            });
            @endif

            @if(isset($trip) && is_object($trip->versions[0]) && isset($city))
            $('body').on('click', '.editPlaceIcon', function (e) {
                var trip_place_id = $(this).attr('data-id');
                var version_id = {{$trip->versions[0]->id}};

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxDeActivateTripPlace')}}",
                    data: {"trip_place_id": trip_place_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            window.location.replace("{{url('trip/plan')}}" + "/"+{{$trip_id}}+
                            "?do=edit");
                        }


                    });
                e.preventDefault();
            });
            @endif

            @if(isset($trip) && is_object($trip->versions[0]) && isset($city))

            $('body').on('click', '.editCityIcon', function (e) {
                var trip_city_id = $(this).attr('data-id');
                var version_id = {{$trip->versions[0]->id}};

                $.ajax({
                    method: "POST",
                    url: "{{url('trip/ajaxDeActivateTripCity')}}",
                    data: {"trip_city_id": trip_city_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'success') {
                            window.location.replace("{{url('trip/plan')}}" + "/"+{{$trip_id}}+
                            "?do=edit")
                            ;
                        } else {

                        }


                    });
                e.preventDefault();
            });
            @endif



            $(".datepicker").datepicker({
                changeYear: true,
                dateFormat: "d MM yy",
                altField: "#actual_trip_start_date",
                altFormat: "yy-mm-dd"
                @if(!isset($memory))
                     ,minDate: new Date()
                @endif
                @if(isset($trip) && $trip->memory)
                    ,maxDate: 0
                @elseif(isset($trip) && !$trip->memory)
                ,minDate: 0
                @endif

            });


            $("div#modal-add_place .datepicker_place").datepicker({
                changeYear: true,
                dateFormat: "d MM yy",
                altField: "#actual_place_date",
                altFormat: "yy-mm-dd",
                onSelect: function(dateText) {
                    var date = new Date(dateText);
                    var today = new Date();

                    if (date >= today) {
                        disableMemoryFields($("div#modal-add_place"));
                    } else {
                        enableMemoryFields($("div#modal-add_place"));
                    }
                },
                defaultDate: ""
                @if(isset($trip) && $trip->memory)
                    ,maxDate: 0
                @elseif(isset($trip) && !$trip->memory)
                    ,minDate: 0
                @endif
            });

            $('.timepicker').timepicker();

            $('.timepicker').on('hideTimepicker', function() {
                $('.timepicker').blur();
            });

        });

        function doPlus(fieldId) {
            $('#' + fieldId).val(parseInt($('#' + fieldId).val()) + 1);
            $('#' + fieldId).change();
        }

        function doMinus(fieldId) {
            if (parseInt($('#' + fieldId).val()) > 0) {
                $('#' + fieldId).val(parseInt($('#' + fieldId).val()) - 1);
                $('#' + fieldId).change();
            }
        }

        function getUrlVars()
            {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for(var i = 0; i < hashes.length; i++)
                {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
            }


    </script>
{{--    @include('site.trip2.partials._maps-scripts')--}}
    @include('site.trip2.partials._map-box-scripts')
    @include('site.trip2.partials._plan-chats_script')
    <script>
        // confirmation
        $('.confirm-trip-deletion').on('click', function () {
            $.confirm({
                title: '{{ __('other.are_you_sure') }}',
                content: '{{ __('trip.if_you_canceled_this_trip_you_will') }}',
                icon: 'fa fa-question-circle',
                animation: 'scale',
                closeAnimation: 'scale',
                opacity: 0.5,
                buttons: {
                    'confirm': {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function () {

                            $.ajax({
                                method: "POST",
                                url: "{{route('trip.ajax_delete_trip')}}",
                                data: {"trip_id": {{$trip_id}}}
                            })
                                .done(function (res) {
                                    var result = JSON.parse(res);
                                    if (result.status == 'success') {
                                        window.location.replace("{{url('home')}}");
                                    } else {
                                        alert('{{ __("trip.unknown_error_while_deleting_trip_plan") }}');
                                    }


                                });
                        }
                    },
                    cancel: function () {

                    },

                }
            });
        });

    </script>

    <script type="text/javascript" src="{{asset('assets2/js/plupload-2.3.6/js/plupload.full.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets2/js/plupload-2.3.6/js/jquery.ui.plupload/jquery.ui.plupload.min.js')}}"></script>
<script type="text/javascript">
    @if(isset($trip) && is_object($trip))
        initUploaders();
    @endif

</script>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '2471864446468519',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v5.0'
    });
  };
</script>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>

<script>

    
    $(document).ready(function() {

       
        var savedRange;

        $(document).on('mouseup', 'div#modal-story div.story', function (e) {
            if (window.getSelection().toString()) {
                var x = e.pageX;
                var y = e.pageY;

                saveRange();
            } else {
                savedRange = null;
            }
        });

        $(document).on('click', 'div.editor span.bold', function() {
            resetRange();
            document.execCommand('bold', false, null);
            saveRange();
        });

        $(document).on('click', 'div.editor span.italic', function() {
            resetRange();
            document.execCommand('italic', false, null);
            saveRange();
        });

        $(document).on('click', 'div.editor span.link', function() {
            resetRange();
            var url = $(this).parent().find('input.link').val();
            if (!/^https?:\/\//i.test(url)) {
                url = 'http://' + url;
            }
            document.execCommand("CreateLink", false, url);
            var selection = document.getSelection();
            selection.anchorNode.parentElement.target = '_blank';
            saveRange();
        });
        

        $(document).on('click', 'div.editor span.unlink', function() {
            resetRange();
            document.execCommand("removeFormat");
            document.execCommand("unlink");
            saveRange();
        });

        $(document).on('mousedown', 'div.editor span', function(e) {
            e.preventDefault();
        });

        $(document).on('mousedown', 'div.editor input.link', function(e) {
            e.preventDefault();
            $(this).focus();
        });

        var ce = document.querySelector('[contenteditable]');
        ce.addEventListener('paste', function (e) {
            e.preventDefault();
            var text = e.clipboardData.getData('text/plain');
            document.execCommand('insertText', false, text);
        });

        $(document).bind('dragover drop', 'div.story', function(event){
            event.preventDefault();
            return false;
        });

        function resetRange()
        {
            var selection = getSelection();
            selection.removeAllRanges();
            selection.addRange(savedRange.cloneRange());
        }

        function saveRange() {
            var selection = getSelection();

            if (selection && selection.rangeCount > 0) {
                savedRange = selection.getRangeAt(0).cloneRange();
            }
        }

        function placeEditor(x_pos, y_pos) {
            $("div.editor").css({
                top: y_pos + 'px',
                left: x_pos + 'px'
            });
            $("div.editor").show();
        }
    });

    function publishTripPlace(button)
    {
        $(button).attr('disabled', 'disabled');
        var placeId = $(button).parents('div.trip-place-wrap').attr('data-placeid');

        $.ajax({
            method: "POST",
            url: "{{route('trip.ajax_publish_trip_place')}}",
            data: {trip_id: $('#trip_id').val(), place_id: placeId}
        })
            .done(function (res) {
                updatePlanContents();
            }).fail(function(data) {
            if (data.status == 403) {
                location.reload();
            }
        });
    }

    function addButtonsAfterEdits()
    {
        addUndoButton();

        addSendSuggestionsButton();
    }

    function addUndoButton()
    {
        var is_admin = {{$is_admin ? 1 : 0}};
        var is_editor = {{$is_editor ? 1 : 0}};

        if ($('button.btn-undo').length || (!is_admin && !is_editor)) {
            return;
        }

        $('div.header-control.ml-auto').prepend('<button type="button" class="btn btn-transparent btn-undo" data-toggle="modal" data-target="#modal-cancel">Undo</button>');
    }

    function addSendSuggestionsButton()
    {
        var is_editor = {{$is_editor ? 1 : 0}};

        $('button.send-suggestions').removeAttr('disabled');

        //$('div.header-control.ml-auto').prepend('<button type="button" class="btn btn-primary send-suggestions">SEND SUGGESTIONS</button>');
    }

    function sendSuggestions()
    {
        $('button.send-suggestions').attr('disabled', 'disabled');

        $.ajax({
            method: "POST",
            url: "{{route('trip.send_suggestions')}}",
            data: {plan_id: $('#trip_id').val()}
        })
            .done(function (res) {
                updatePlanContents();
            }).fail(function(data) {
            if (data.status == 403) {
                location.reload();
            }
        });
    }

    function undoLastAction()
    {
        autoSave();
        $('button#undo').prop('disabled', true);
        $.ajax({
            method: "POST",
            url: "{{route('trip.undo')}}",
            data: {plan_id: $('#trip_id').val()}
        })
            .done(function (res) {
                autoSave(true);
                $('div#modal-cancel').modal('hide');

                if (res.data.count <= 0) {
                    $('button.btn-undo').remove();
                }

                updatePlanContents();
                $('button#undo').prop('disabled', false);
            }).fail(function(data) {
            if (data.status == 403) {
                location.reload();
            }
        });
    }

    function cancelChanges()
    {
        autoSave();

        $.ajax({
            method: "POST",
            url: "{{route('trip.cancel_changes')}}",
            data: {plan_id: $('#trip_id').val()}
        })
            .done(function (res) {
                autoSave(true);
                location.reload();
            }).fail(function(data) {
            if (data.status == 403) {
                location.reload();
            }
        });
    }

    $(document).ready(function() {
        $(document).on('click', 'button#fly-to', function() {
            var city = $(this).data('city');
            var date = $(this).data('date');

            $('#modal-skyscanner .user-list').html('                    <div data-skyscanner-widget="SearchWidget"\n' +
                '                         data-origin-geo-lookup="true"\n' +
                '                         data-currency="USD"\n' +
                '                         data-destination-phrase="\'' + city + '\'"\n' +
                '                         data-target="_blank"\n' +
                '                         data-button-colour="#db0000"\n' +
                'data-flight-outbound-date="' + date + '"' +
                '></div>');

            var s = document.createElement("script");
            s.type = "text/javascript";
            s.src = "{{ asset('assets2/js/skyloader.js')}}";

            $('#modal-skyscanner .user-list').append(s);

            setTimeout(function() {
                $('#modal-skyscanner').modal('show');
            }, 1000);

            $('div.mapboxgl-popup').remove();
        });

        $(document).on('click', 'button#prices', function() {
            var city = $(this).data('city');
            var date = $(this).data('date');
            var name = $(this).data('name');

            $('#modal-skyscanner .user-list').html('    <div data-skyscanner-widget="HotelSearchWidget"\n' +
                '        data-locale="en-GB"\n' +
                '        data-currency="USD"\n' +
                '        data-target="_blank"\n' +
                '        data-destination-phrase="\'Eiffel Tower Modern Flat Near Eiffel Tower\'"\n' +
                '        data-button-colour="#db0000"\n' +
                'data-locale="en-GB"' +
                'data-hotel-check-in-date="' + date + '"' +
                '        data-responsive="true"\n' +
                '            ></div>');

            var s = document.createElement("script");
            s.type = "text/javascript";
            s.src = "https://widgets.skyscanner.net/widget-server/js/loader.js";

            $('#modal-skyscanner .user-list').append(s);

            setTimeout(function() {
                $('#modal-skyscanner').modal('show');
            }, 1000);

            $('div.mapboxgl-popup').remove();
        });

        $(document).on('click', 'div.suggestion button.publish', function() {
            publishTripPlace(this);
        });

        $(document).on('click', 'button.send-suggestions', function() {
            sendSuggestions();
        });

        $(document).on('click', 'button#undo', function() {
            undoLastAction();
        });

        $(document).on('click', 'button#cancel-changes', function() {
            cancelChanges();
        });

        $('div.page-planner').on('click', 'div.trip-place div.approve', function() {
            var suggestionId = $(this).data('id');

            $.ajax({
                method: "POST",
                url: "{{route('trip.ajax_approve_suggestion')}}",
                data: {suggestion_id: suggestionId}
            })
                .done(function (res) {
                    updatePlanContents();
                    addButtonsAfterEdits();
                }).fail(function(data) {
                if (data.status == 403) {
                    location.reload();
                }
            });
        });

        $('div.page-planner').on('click', 'div.trip-place div.disapprove', function() {
            var suggestionId = $(this).attr('data-id');
            $('#modal-decline_suggestion').find('button.decline').attr('data-id', suggestionId);
            $('#modal-decline_suggestion').modal('show');
        });

        $('input#edit_duration_hours, input#edit_duration_minutes, input#duration_hours, input#duration_minutes, input#known_for').on('change', function () {
            $(this).parents('.optional').removeClass('optional');
        });

        $(document).on('click', 'div.counter span.plus', function (e) {
            e.stopImmediatePropagation();
            var before = $(this).attr('data-before');
            var max = $(this).parent().find('input').attr('max');
            var min = $(this).parent().find('input').attr('min');

            if (before !== undefined && before == max) {
                var prev = $(this).parents('.col-4').prev('.col-4').find('div.counter');
                if ($(prev).length) {
                    $(this).parent().find('input').val(min);
                    $(prev).find('span.plus').trigger('click');
                }
            }

            $(this).parent().find('span').attr('data-before', $(this).parent().find('input').val());
        });

        $(document).on('click', 'div.counter span.minus', function (e) {
            var before = $(this).attr('data-before');
            var max = $(this).parent().find('input').attr('max');
            var min = $(this).parent().find('input').attr('min')

            if (before !== undefined && before == min) {
                var prev = $(this).parents('.col-4').prev('.col-4').find('div.counter');
                if ($(prev).length && $(prev).find('input').val() > $(prev).find('input').attr('min')) {
                    $(this).parent().find('input').val(max);
                    $(prev).find('span.minus').trigger('click');
                }
            }

            $(this).parent().find('span').attr('data-before', $(this).parent().find('input').val());
        });

        $('input#budget_custom, input#edit_budget_custom').on('change', function () {
            $(this).parents('.optional').removeClass('optional');
        });

        $('input#budget_custom, input#edit_budget_custom, input#known_for').on('keyup', function () {
            $(this).parents('.optional').removeClass('optional');
        });

        $('div#modal-add_place, div#modal-edit_place, div#modal-add_suggestion').on('paste', 'div.bootstrap-tagsinput input', function() {
            var inpt = $(this);
            setTimeout(function() {
                $(inpt).trigger('blur');
            }, 100);
        });


        $('div#modal-add_place, div#modal-edit_place, div#modal-add_suggestion').on('keydown', 'div.bootstrap-tagsinput input', function(e) {
            if (/(android)/i.test(navigator.userAgent)) {
                e.type = 'keypress';
                $(this).trigger(e);
            }
        });


        $('div#modal-edit_place input#known_for').tagsinput({
            confirmKeys: [13, 32, 44, 190, 46],
            maxTags: 10,
            trimValue: true,
            maxChars: 15
        });

        $('div#modal-add_place input#known_for').tagsinput({
            confirmKeys: [13, 32, 44, 190, 46],
            maxTags: 10,
            trimValue: true,
            maxChars: 15
        });

        $('div#modal-add_suggestion input#suggest_known_for').tagsinput({
            confirmKeys: [13, 32, 44, 190, 46],
            maxTags: 10,
            trimValue: true,
            maxChars: 15
        });

        $('div#modal-add_place input#known_for, div#modal-edit_place input#known_for, div#modal-add_suggestion input#suggest_known_for').on('beforeItemAdd', function(event) {
            var tags = event.item.split(/[ ,.]+/);

            if (tags.length > 1) {
                tags.forEach(function (tag, i) {
                    $(event.currentTarget).tagsinput('add', tag.slice(0, 15));
                });

                event.cancel = true;
            } else if (tags.length === 1 && tags[0].length > 15) {
                $(event.currentTarget).tagsinput('add', tags[0].slice(0, 15));
                event.cancel = true;
            }

            if (/(android)/i.test(navigator.userAgent)) {
                var inpt = $(this).parent().find('div.bootstrap-tagsinput input');
                $(inpt).val('');
                $(inpt).trigger('blur');
                setTimeout(function() {
                    $(inpt).trigger('focus');
                }, 1);
            }
        });

        $(document).on('click', "div#modal-add_place span.tag", function(e) {
            var val = $(e.target).data('item');
            $('div#modal-add_place input#known_for').tagsinput('remove', val);

            $('div#modal-add_place div.bootstrap-tagsinput input').val(val);
        });

        $(document).on('click', "div#modal-edit_place span.tag", function(e) {
            var val = $(e.target).data('item');
            $('div#modal-edit_place input#known_for').tagsinput('remove', val);

            $('div#modal-edit_place div.bootstrap-tagsinput input').val(val);
        });

        $(document).on('click', "div#modal-add_suggestion span.tag", function(e) {
            var val = $(e.target).data('item');
            $('div#modal-add_suggestion input#suggest_known_for').tagsinput('remove', val);

            $('div#modal-add_suggestion div.bootstrap-tagsinput input').val(val);
        });

        $(document).on('click', 'button.activity-logs', function(e) {
            //$('div.modal-backdrop').hide();
        });

        $(document).on('click', '.suggestable', function(e) {
            $('div.modal-backdrop').hide();
        })

        $(document).on('click', '.suggestion', function(e) {
            setTimeout(function() {$('div.modal-backdrop').hide();}, 300)
        })

        $(document).on('click', 'div.add-place-map', function() {
            $('button.btn-add_place').last().trigger('click');
        });

        $(document).on('click', 'button.btn-add_place', function(e) {
            uploader.unbindAll();
            editUploader.unbindAll();
            suggestUploader.unbindAll();
            uploader.destroy();
            editUploader.destroy();
            suggestUploader.destroy();

            showHint = true;

            addedImages = [];
            e.stopImmediatePropagation();
            $('div#modal-add_place .input-group-focus').removeClass('input-group-focus');
            $('div#modal-add_place div#media-container div.swiper-slide').remove();
            $('div#modal-add_place div#media-container').append('<div class="swiper-slide" id="img-upload">\n' +
                '                                                <button type="button" class="img-upload"></button>\n' +
                '                                            </div>');

            $('div.map-placeholder-block').show();
            $('div#modal-add_place').modal('show');
            $('div.modal-backdrop').hide();
            $('div#modal-add_place input#cities_id').val('');
            $('div#modal-add_place input#places_id').val('');
            $('div#modal-add_place input#places_real_id').val('');
            $('div#modal-add_place input#cities_real_id').val('');
            $('div#modal-add_place input#time').val('');
            $('div#modal-add_place input#order').val('');
            $('div#modal-add_place input#actual_place_date').val('');
            $('div#modal-add_place input.datepicker_place').val('');
            $('div#modal-add_place div.story').html('');

            $('div#modal-add_place b.overall-budget').text('$' + $(this).data('spent'));

            $('div#modal-add_place button.add-story').show();
            $('div#modal-add_place div#modal-story').hide();

            $('div#modal-add_place input#known_for').tagsinput('removeAll');

            $('div#modal-add_place div.input-group-img.place').attr('style', '');
            $('div#modal-add_place div.input-group-img.city').attr('style', '');
            $('div#modal-add_place div.input-group-img.city').removeClass('chosen');

            $('div#modal-add_place input#duration_days').val(0);
            $('div#modal-add_place input#duration_hours').val(0);
            $('div#modal-add_place input#duration_minutes').val(0);

            $('div#modal-add_place input#budget_custom').val('');
            $('div#modal-add_place input#editAmountMoney1').prop('checked', false);
            $('div#modal-add_place input#editAmountMoney2').prop('checked', false);
            $('div#modal-add_place input#editAmountMoney3').prop('checked', false);

            $('div#modal-add_place .input-group-img').addClass('disabled');
            $('div#modal-add_place input#places_id').addClass('disabled');
            $('div#modal-add_place input#places_id').prop('disabled', true);

            $(document).off('click', 'button.img-upload');
            initUploaders();

            var order = $(this).data('order');
            var date = $(this).data('date');
            var date_format = $(this).data('dateformat');

            if (time) {
                $('div#modal-add_place input#order').val(order);
            }

            if (date) {
                $('div#modal-add_place input#actual_place_date').val(date);
            }

            if (date_format) {
                $('div#modal-add_place input.datepicker_place').val(date_format);
            }

            date = new Date(date_format);
            var today = new Date();

            if (date >= today) {
                disableMemoryFields($("div#modal-add_place"));
            } else {
                enableMemoryFields($("div#modal-add_place"));
            }

            if ($(this).attr('data-cityid')) {
                var cityId = $(this).attr('data-cityid');
                var cityTitle = $(this).attr('data-citytitle');
                var cityImg = $(this).attr('data-cityimg');
                var cityLat = $(this).attr('data-citylat');
                var cityLng = $(this).attr('data-citylng');

                $('div#modal-add_place input#cities_real_id').val(cityId);
                $('div#modal-add_place input#cities_id').val(cityTitle);
                $('div#modal-add_place .input-group-img.city').addClass('chosen');
                $('div#modal-add_place .input-group-img.city').css('background-image', 'url(' + cityImg + ')');
                $('div#modal-add_place .input-group-img.city').css('background-size', 'cover');
                cityChosen($('div#modal-add_place'), cityId, cityTitle, cityLat, cityLng, cityTitle);
            }
        });

        $('div.open-optional').on('click', function() {
            $(this).parents('div.modal-body').find('div.optional-block').toggle();
            if ($(this).find('i').hasClass('fa-caret-down')) {
                $(this).find('i').removeClass('fa-caret-down');
                $(this).find('i').addClass('fa-caret-up');
            } else if ($(this).find('i').hasClass('fa-caret-up')) {
                $(this).find('i').removeClass('fa-caret-up');
                $(this).find('i').addClass('fa-caret-down');
            }
        });
    });
    function addMediaToPlace(providerId){

        $.ajax({
            type:"GET",
            url: "{{url('places/ajax_set_poi_media')}}",
            data: {
                place_id: providerId
            }
        });

    }
    function getSuggesstedPlaces(selected_city_id,elem) {
            parent_body = elem;
            $('.suggestedClass').addClass('input-group-focus');
            parent_body.find(".map-select-place").html('<div class="suggestedContainer" id="allSearchResults"></div>');
            $.ajax({

                        method: "GET",
                        url: "{{url('home/suggestTopPlaces')}}",
                        data: {
                            city_id: selected_city_id
                        }
                    })
                        .done(function (res) {
                            isSuggestedPlaces = true;
                            suggestedResults = [];
                            placeResults = [];
                            res = JSON.parse(res);
                            var num_places_result = res.length;
                            if(num_places_result==20) {
                                $('#numAllResult').text(num_places_result+'+');
                            }
                            else {
                                $('#numAllResult').text(num_places_result);
                            }
                            parent_body.find('.suggestedContainer').empty();

                            if (num_places_result === 0) {
                                parent_body.find('.suggestedContainer').html('<span class="no-results">No results found.</span>');
                            }

                            suggestedResults = res;
                            renderSuggestedPlaces();

                            updateTrendingData(0, selected_city_id);
                        });
        }

    function leavePlan()
    {
        var tripId = $('#trip_id').val();

        $.ajax({
            method: "POST",
            url: "{{route('trip.ajax_leave_plan')}}",
            data: {trip_id: tripId}
        })
            .done(function (res) {
                window.location.reload();
            });
    }

    function enableMemoryFields(body)
    {
        if ($(body).find('div.memory-only').find('button.add-story').attr('disabled') != 'disabled') {
            return;
        }

        $(body).find('div.optional-block').hide();
        $(body).find('label.duration').text('Stayed here for');
        $(body).find('label.budget span:first').text('How much did you spend?');
        $(body).find('div.memory-only').css('opacity', '1');
        $(body).find('div.memory-only').fadeIn();
        $(body).find('div.memory-only').find('input').removeAttr('disabled');
        $(body).find('div.memory-only').find('button').removeAttr('disabled');
    }

    function disableMemoryFields(body)
    {
        if ($(body).find('div.memory-only').find('button.add-story').attr('disabled') == 'disabled') {
            return;
        }

        $(body).find('div.optional-block').hide();
        $(body).find('label.duration').text('Planning to stay');
        $(body).find('label.budget span:first').text('How much will you spend?');
        $(body).find('div.memory-only').fadeOut();
        $(body).find('div.memory-only').find('input').attr('disabled', 'disabled');
        $(body).find('div.memory-only').find('input').val('');
        $(body).find('div.memory-only').find('input').text('');
        $(body).find('div.memory-only').find('span.tag.label').remove();
        $(body).find('div.memory-only').find('button').attr('disabled', 'disabled');
        $(body).find('div#modal-story div.story').html('');
    }

    $(document).on('click', 'button.leave-plan', function () {
        $('div#modal-leave_popup').modal('show');
    });

    $(document).on('click', 'button.exit-plan', function () {
        window.location.href = '/home';
    });

    $(document).on('click', 'div#modal-leave_popup button.leave', function () {
        leavePlan();
    });

    window.addEventListener('online', function(e) {
        $('div.offline-status').css('display', 'none');

        updatePlanContents();
    }, false);

    window.addEventListener('offline', function(e) {
        $('div.offline-status').css('display', 'flex');
    }, false);

    $(document).on('hide.bs.modal','#modal-share', function () {
        var currentURL = window.location.href.split('?')[0];
        window.location.href = currentURL;
    });

    $(document).ready(function() {
        $(document).on('click', 'button.dismiss', function() {
            $(this).parents('div.trip-place-wrap').fadeOut();
        });

        $('div.modal-place').on('mouseover', '.input-group-dropdown-body.select .map-select div#allSearchResults div.place-content h3.content-ttl', function() {
            var fullWidth = $(this)[0].scrollWidth;
            var width = $(this).width();

            var indent = width - fullWidth;

            if (indent < -1) {
                indent -= 10;
            }

            $(this).animate({textIndent: indent}, indent * (-40));
        }).on('mouseout', '.input-group-dropdown-body.select .map-select div#allSearchResults div.place-content h3.content-ttl', function() {
            $(this).animate({textIndent: 0}, 2000);
        });

        $(document).on('blur', '.bootstrap-tagsinput input', function () {
            if (!$(this).text() && !$(this).parent().find('span.tag').length) {
                this.style.width = '300px';
                $(this).attr('placeholder', 'Food, Photography, History...');
            } else {
                $(this).attr('placeholder', '');
            }

            $(this).parent().append($(this));
        });

        $(document).on('change keyup keydown', '.bootstrap-tagsinput input', function (e) {
            this.style.width = $(this).val().length + 3 + "ch";
            if (!$(this).text() && !$(this).parent().find('span.tag').length) {
                this.style.width = '300px';
                //$(this).attr('placeholder', 'Food, Photography, History...');
            } else {
                $(this).attr('placeholder', '');
            }
        });

        $('.bootstrap-tagsinput input').each(function () {
            if ($(this).parent().find('span').text()) {
                $(this).trigger('blur');
            }
        });

        $('button.add-story').on('click', function () {
            if ($('.input-disable-opacity').is(':empty')) {
                $('.disabled-input-toggle').addClass('disable--input');
                $('.disable--overlay').show()
            }
            else {
                $('.disabled-input-toggle').removeClass('disable--input');
                $('.disable--overlay').hide()
            }
            var parent = $(this).parent();
            $(parent).find('div#modal-story').show();
            $(this).hide();
        });

        $('.input-disable-opacity').on('change keyup copy paste cut', function() {
            if ($(this).is(':empty')) {
                $('.disabled-input-toggle').addClass('disable--input');
                $('.disable--overlay').show()
            } else {
                $('.disabled-input-toggle').removeClass('disable--input');
                $('.disable--overlay').hide()
          }
        });

        function check_charcount(content, max, e) {
            if (e.which != 8 && $(content).text().length > max) {
                //$(content).html($(content).html().substring(0, max));
                e.preventDefault();
            }
        }

        var content = 'story';
        var max = 3000;

        $('div.'+content).keydown(function(e){ check_charcount($(this), max, e); });
        $('div.'+content).keyup(function(e){ check_charcount($(this), max, e); });

        $('div.swiper-wrapper').on('click', 'div.swiper-slide div.remove', function () {
            var tripId = $(this).parent().data('tripid');
            var mediaId = $(this).parent().data('mediaid');
            var placeId = $(this).parent().data('placeid');

            var parent = $(this).parent();

            if (!tripId) {
                tripId = null;
            }

            if (!placeId) {
                placeId = null;
            }

            $(parent).remove();
            $('div#modal-edit_place div#edit-media-container div#edit-img-upload').show();

            var index = addedImages.indexOf(mediaId);
            addedImages.splice(index, 1);
        });

        $('div.sidebar').on('click', 'div.large-images div.large-image', function() {
            var mediaId = $(this).data('mediaid');

            $('div.lightbox_imageModal[data-mediaid='+ mediaId +']').trigger('click');
        });

        $('div.sidebar').on('click', 'a.see-more', function() {
            var mediaId = $(this).closest('div.trip-photos').find('div.media-wrap').eq(3).find('div.lightbox_imageModal').data('mediaid');
            $('div.lightbox_imageModal[data-mediaid='+ mediaId +']').trigger('click');
        });

        $(document).on('keyup', '.counter input.count', function() {
            var _this = $(this);
            var min = parseInt(_this.attr('min'));
            var max = parseInt(_this.attr('max'));
            var val = parseInt(_this.val()) || (min - 1);

            if (val < min)
                _this.val(min);
            if (val > max)
                _this.val(max);
        });

        @if ($open_affiliate_hint && 0)
        if (!Cookies.get('affiliate_hint')) {
            $('div#modal-affiliate_hint').modal('show');
        }
        @endif

        $('div#modal-affiliate_hint').on('click', 'div.actions div.ok', function () {
            $('div#modal-affiliate_hint').modal('hide');
            Cookies.set("affiliate_hint", 1);
        });

        //accept invitation
        $('body').on('click', '.accept-invite', function (e) {
            var invitation_id = $(this).attr('data-id');

            $('button.deny-invite').attr('disabled', 'disabled');
            $('button.accept-invite').attr('disabled', 'disabled');

            $.ajax({
                method: "POST",
                url: "{{ route('trip.ajax_accept_invitation') }}",
                data: {invitation_id: invitation_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.status == 'success') {
                        location.reload();
                    }

                }).fail(function(data) {
                    if (data.status == 403) {
                        location.reload();
                    }
                });
            e.preventDefault()
        });

        $('body').on('click', '.deny-invite', function (e) {
            var invitation_id = $(this).attr('data-id');

            $('button.deny-invite').attr('disabled', 'disabled');
            $('button.accept-invite').attr('disabled', 'disabled');

            $.ajax({
                method: "POST",
                url: "{{ route('trip.ajax_reject_invitation') }}",
                data: {invitation_id: invitation_id}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.status == 'success') {
                        location.reload();
                    }
                }).fail(function(data) {
                if (data.status == 403) {
                    location.reload();
                }
            });
            e.preventDefault()
        });


    });

    $('.bootstrap-tagsinput').blur(function(){jQuery(this).attr('placeholder', '')});

    $('.bootstrap-tagsinput input').on('keydown', function() {
        this.style.width = ((this.value.length + 3) * 8) + 'px';
    })

    $('input#known_for, input#suggest_known_for').on('itemRemoved', function(event) {
        $(this).siblings('.bootstrap-tagsinput').find('.tag').length == 0 && $(this).siblings('.bootstrap-tagsinput').find('input').attr("placeholder", "Food, Photography, History...");
    });

    var searchResult;
    var tripId = 0;

    @if (isset($trip))
        tripId = parseInt('{{$trip->id}}');
    @endif


    search();

    $(document).on('keyup', 'div#modal-invite div.search input', function() {
        search();
    });

    $(document).on('change', 'div#modal-invite div.search input', function() {
        search();
    });

    var searchRequest = null;

    function search()
    {
        var search = $('div#modal-invite div.search input').val();

        if (searchRequest) {
            searchRequest.abort();
        }

        var withStyles = $('div#modal-invite div.search input[name=with_styles]').is(':checked') ? 1 : 0;

        searchRequest = $.ajax({
            method: "GET",
            url: "{{route('trip.ajax_people_to_invite')}}",
            data: {search: search, trip_id: tripId, with_styles: withStyles}
        })
            .done(function (res) {
                var data = res.data;

                $('div#modal-invite div.tabs div.tab.experts span').text(data.experts.length)
                $('div#modal-invite div.tabs div.tab.friends span').text(data.friends.length)
                $('div#modal-invite div.tabs div.tab.followers span').text(data.followers.length)
                $('div#modal-invite div.tabs div.tab.locals span').text(data.locals.length)

                $('div.search-results div.spinner-border').hide();

                searchResult = data;
                renderSearchResults($('div#modal-invite div.modal-content div.modal-body div.search div.tabs div.tab.active').data('type'));
            }).fail(function(data) {
                if (data.status == 403) {
                    location.reload();
                }
            });
    }

    function renderSearchResults(tab)
    {
        $('div#modal-invite div.result-rows').html('');

        if (!searchResult[tab].length) {
            var question = '';
            var advice = '';

            switch (tab) {
                // case 'experts':
                //     question = 'There were no results.';
                //     advice = 'Please add more destinations to your plan, or check back later.';
                //     break;
                // case 'locals':
                //     question = 'Are you seeking assistance?';
                //     advice = 'Please add destinations to your Trip Plan and check back to find the relevant experts.';
                //     break;
                // case 'followers':
                //     question = 'Want to stay connected with someone during the journey?';
                //     advice = 'Once you have followers, you can invite them to your Trip Plan from here.';
                //     break;
                // case 'friends':
                //     question = 'Are you traveling with family and friends?';
                //     advice = 'Your loved ones will be displayed here when you follow each other.';
                //     break;
                default:
                    question = 'There were no results.';
                    advice = 'Please add more destinations to your plan, or check back later.';
                    break;
            }

            $('div#modal-invite div.result-rows').append('<div class="modal-inner-trip_info">\n' +
                '                                    <span class="bold">\n' + question +
                '                                    </span>\n' +
                '                                    <span>\n' + advice +
                '                                    </span>\n' +
                '                                </div>')

            return;
        }

        var timers = [];

        searchResult[tab].forEach(function(currentValue, index, array) {
            var disabled = currentValue.invited ? 'disabled' : '';
            var invite = currentValue.invited ? 'Invited' : 'Invite';
            var timeout = currentValue.timeout;
            var hasTimeout = timeout > 0;

            if (hasTimeout) {
                invite = timeout > 0 ? 'wait ' + parseInt(timeout/60) + 'min' : invite;
                timeout = 'data-timeout="' + timeout + '"';
            } else {
                timeout = '';
            }

            var elem = $('<div class="result-row">\n' +
                '                                        <div class="user">\n' +
                '                                            <img src="' + currentValue.img + '">\n' +
                '                                            <div class="desc-block">\n' +
                '                                                <div class="name">' + currentValue.name + '</div>\n' +
                '                                                <div class="desc">' + currentValue.desc + '</div>\n' +
                '                                            </div>\n' +
                '                                        </div>\n' +
                '                                        <div class="actions">\n' +
                '                                            <div class="role">\n' +
                '                                                <select name="" id="">\n' +
                '                                                    <option value="admin">Admin</option>\n' +
                '                                                    <option selected value="editor">Editor</option>\n' +
                '                                                    <option value="viewer">Viewer</option>\n' +
                '                                                </select>\n' +
                '                                            </div>\n' +
                '                                            <div ' + timeout + ' class="action invite '+ disabled +'" data-id='+ currentValue.id +'>' + invite + '</div>\n' +
                '                                        </div>\n' +
                '                                    </div>');

            $('div#modal-invite div.result-rows').append(elem);

            if (hasTimeout && timers[currentValue.id] === undefined) {
                inviteTimeout($('div#modal-invite div.result-rows div.result-row').last());
                timers[currentValue.id] = 1;
            }

            $('div#modal-invite div.result-rows div.result-row select option[value='+currentValue.role+']').last().prop('selected', true);

        });
    }

    function inviteTimeout(elem)
    {
        var timeout = $(elem).find('div.action').attr('data-timeout');

        if (timeout > 0) {
            timeout--;
            setTimeout(function() {
                $(elem).find('div.action').attr('data-timeout', timeout);
                $(elem).find('div.action').text('wait ' + parseInt(timeout/60) + 'min');
                inviteTimeout(elem);
            }, 1000);

            return;
        }

        $(elem).find('div.action').removeClass('disabled');
        $(elem).find('div.action').text('Invite');
        timers[$(elem).find('div.action').attr('data-id')] = undefined;
    }

    function invite(userId, role)
    {
        $('div.role-block.pending div.result-row').remove();
        $('div.role-block.pending div.spinner-border').show();

        $.ajax({
            method: "POST",
            url: "{{route('trip.ajax_invite_friends')}}",
            data: {trip_id: tripId, users_id: userId, role: role}
        })
            .done(function (res) {
                renderInvited();
                search();
            }).fail(function(data) {
            if (data.status == 403) {
                location.reload();
            }
        });
    }

    function cancel(userId, block)
    {
        $(block).find('div.result-row').remove();
        $(block).find('div.spinner-border').show();

        $.ajax({
            method: "POST",
            url: "{{route('trip.ajax_cancel_invitation')}}",
            data: {trip_id: tripId, users_id: userId}
        })
            .done(function (res) {
                renderInvited();
                search();
            }).fail(function(data) {
            if (data.status == 403) {
                location.reload();
            }
        });
    }

    renderInvited();
    var resendTimers = [];

    function renderInvited()
    {
        var planMembersCount = 0;

        $.ajax({
            method: "GET",
            url: "{{route('trip.ajax_invited_people')}}",
            data: {trip_id: tripId}
        })
            .done(function (res) {
                var data = res.data.users;
                var invitedCount = res.data.count;

                if (invitedCount > 1) {
                    $('div#modal-mobile_side_menu div.side-menu-option div.people ul').html('');
                    $('div#modal-mobile_side_menu div.side-menu-option div.people span.text').html('');
                }

                $('div.role-block div.spinner-border').hide();
                $('div.role-block').hide();

                resendTimers = [];

                $.each(data, function(role, value) {
                    $('div.role-block.' + role + ' div.result-row').remove();
                    data[role].forEach(function(currentValue, index, array) {
                        var disabled = role === 'pending' ? 'disabled' : '';
                        var resendDisabled = currentValue.resend_disabled ? 'disabled' : '';
                        var selected = role === 'pending' ? 'Pending' : 'Editor';

                        resendTimers[currentValue.id] = currentValue.resend_timeout;

                        var actions = '';

                        if (!currentValue.is_owner) {
                            $('div.role-block.' + role).show();
                            if ("{{auth()->id()}}" != currentValue.id) {
                                actions ='<div class="actions">\n' +
                                    '                                            <div class="role">\n' +
                                    '                                                <select ' + disabled + ' name="" id="">\n' +
                                    '                                                    <option value="admin">Admin</option>\n' +
                                    '                                                    <option selected value="editor">' + selected + '</option>\n' +
                                    '                                                    <option value="viewer">Viewer</option>\n' +
                                    '                                                </select>\n' +
                                    '                                            </div>\n'
                                    + ((role === 'pending') ? '<div class="action resend ' + resendDisabled + '" data-id='+ currentValue.id +' data-role='+ currentValue.role +' >RESEND</div>\n' : '') +
                                    '                                            <div class="action delete" data-id='+ currentValue.id +'>DELETE</div>\n' +
                                    '                                        </div>\n';
                            }

                            $('div.role-block.' + role).append('<div class="result-row">\n' +
                                '                                        <div class="user">\n' +
                                '                                            <img src="' + currentValue.img + '">\n' +
                                '                                            <div class="desc-block">\n' +
                                '                                                <div class="name">' + currentValue.name + '</div>\n' +
                                '                                                <div class="desc">' + currentValue.desc + '</div>\n' +
                                '                                            </div>\n' +
                                '                                        </div>\n' + actions +
                                '                                    </div>');

                            $('div.role-block.' + role + ' select option[value='+currentValue.role+']').prop('selected', true);
                        }

                        planMembersCount++;

                        if (selected === 'Editor' && planMembersCount <= 2 && invitedCount > 1) {
                            $('div#modal-mobile_side_menu div.side-menu-option div.people ul').append('<li><img src="' + currentValue.img + '" alt="ava"></li>');

                            if (planMembersCount === 2) {
                                $('div#modal-mobile_side_menu div.side-menu-option div.people span.text').append(', ');
                            }

                            $('div#modal-mobile_side_menu div.side-menu-option div.people span.text').append(currentValue.name.split(' ')[0]);
                        }

                        if (currentValue.resend_disabled) {
                            setTimeout(function() {
                                if (resendTimers[currentValue.id] !== undefined) {
                                    $('div.action.resend[data-id=' + currentValue.id + ']').removeClass('disabled');
                                }
                            }, resendTimers[currentValue.id] * 1000);
                        }
                    });

                    if (planMembersCount > 1) {
                        $('div#modal-mobile_side_menu div.side-menu-option div.people ul').show();
                        $('div#modal-mobile_side_menu div.side-menu-option button.invite').show();
                    }

                    if (planMembersCount > 2) {
                        $('div#modal-mobile_side_menu div.side-menu-option div.people span.text').append(' <strong>+' + (planMembersCount - 2) + '</strong> others');
                    }

                    $('div.role-block.' + role + ' select option[value='+role+']').prop('selected', true);
                });

            });
    }

    $(document).on('change', 'div.role-block div.result-row select', function() {
        $(this).prop('disabled', true);
        $(this).addClass('disabled');

        var userId = $(this).parents('div.actions').find('div.action.delete').data('id');
        var role = $(this).val();

        var self = this;

        $.ajax({
            method: 'PUT',
            url: "{{route('trip.ajax_change_invitation_request_role')}}",
            data: {user_id: userId, role: role, plan_id: tripId}
        })
            .done(function (res) {
                renderInvited();
                /*$(self).prop('disabled', false);
                $(self).removeClass('disabled');*/
            }).fail(function(data) {
            if (data.status == 403) {
                location.reload();
            }
        });
    });

    $(document).on('click', 'div#modal-invite div.modal-content div.modal-body div.search div.tabs div.tab', function() {
        $('div#modal-invite div.modal-content div.modal-body div.search div.tabs div.tab').removeClass('active');
        $(this).addClass('active');

        var type = $(this).data('type');

        if (type === 'email') {
            $('div#modal-invite div.search-results').hide();
            $('div#modal-invite div.modal-content div.modal-body div.email-invite').show();
        } else {
            $('div#modal-invite div.modal-content div.modal-body div.email-invite').hide();
            $('div#modal-invite div.search-results').show();
            renderSearchResults(type);
        }
    });

    $(document).on('click', 'div#modal-invite div.result-rows div.action.invite:not(.disabled)', function() {
        var role = $(this).parents('.actions').find('div.role select').val();
        invite($(this).data('id'), role);
        $(this).addClass('disabled');
        $(this).text('Invited');
    });

    $(document).on('click', 'div#modal-invite div.role-block div.action.resend:not(.disabled)', function() {
        invite($(this).data('id'), $(this).data('role'));
        $(this).addClass('disabled');
    });

    $(document).on('click', 'div#modal-invite div.role-block div.action.delete', function() {
        cancel($(this).data('id'), $(this).parents('.role-block'));
        $('div.search-block div.actions div.action[data-id=' + $(this).data('id') + ']').removeClass('disabled').text('Invite');
    });

    var checkinsCountRequest = null;
    var checkinsRequest = [];
    var checkinsPage = [];
    var checkinsTabs = [];
    var checkinPlaceId = null;
    var checkinLocationType = 0;

    $(document).on('click', 'div.marker-info div.checkins', function() {
        var placeId = $(this).data('id');
        var locationType = $(this).data('locationtype');
        checkinsPage = [1, 1, 1];
        checkinPlaceId = placeId;
        checkinLocationType = locationType;

        if (checkinsCountRequest) {
            checkinsCountRequest.abort();
        }

        checkinsCountRequest = $.ajax({
            method: "GET",
            url: "{{url('visits/count')}}/" + placeId,
            data: {'location-type': checkinLocationType}
        })
            .done(function (res) {
                if (res.status !== 'success') {
                    return;
                }

                $('div#modal-checkins div.tab').eq(0).find('span').text(res.data.friend);
                $('div#modal-checkins div.tab').eq(1).find('span').text(res.data.follower);
                $('div#modal-checkins div.tab').eq(2).find('span').text(res.data.expert);

                renderCheckinsTab(0);
                $('div#modal-checkins div.tab:first').trigger('click');
                $('div#modal-checkins').modal('toggle');
                $('div.mapboxgl-popup').remove();
            }).fail(function(data) {
                if (data.status == 403) {
                    location.reload();
                }
            });
    });

    $(document).on('click', 'div#modal-checkins div.tab', function() {
        $('div#modal-checkins div.tab').removeClass('active');
        $(this).addClass('active');
        $('div#modal-checkins div.modal-body').html('');
        checkinsPage = [1, 1, 1];
        renderCheckinsTab($(this).index());
    });

    function renderCheckinsTab(tab) {
        appendCheckinsPreloaders();

        if (checkinsRequest[tab]) {
            checkinsRequest[tab].abort();
        }

        var tabType = $('div#modal-checkins div.tab.active').data('type');

        checkinsRequest[tab] = $.ajax({
            method: "GET",
            url: "{{url('visits/place')}}/" + checkinPlaceId + '?page=' + checkinsPage[tab],
            data: {'type': tabType, 'location-type': checkinLocationType}
        })
            .done(function (res) {
                if (res.status !== 'success') {
                    return;
                }

                $('div#modal-checkins div.modal-body div.places-preloader').remove();

                checkinsTabs[0] = res.data.friend;
                checkinsTabs[1] = res.data.follower;
                checkinsTabs[2] = res.data.expert;

                checkinsTabs[tab].forEach(function(val, i) {
                    var person = '                <div class="person">\n' +
                        '                    <div class="info">\n' +
                        '                        <div class="image">\n' +
                        '                            <img src="' + val.src + '" alt="">\n' +
                        '                        </div>\n' +
                        '                        <div class="desc">\n' +
                        '                            <div class="name">' + val.name + '</div>\n' +
                        '                            <div class="status">' + val.visit + '</div>\n' +
                        '                        </div>\n' +
                        '                    </div>\n' +
                        '                    <div class="plan">\n' +
                        '                        <a target="_blank" href="/trip/plan/' + val.plan_id + '#step-' + val.trip_place_id + '" class="plan">View Plan</a>\n' +
                        '                    </div>\n' +
                        '                </div>';

                    $('div#modal-checkins div.modal-body').append(person)
                });

                checkinsPage[tab]++;
            }).fail(function(data) {
                if (data.status == 403) {
                    location.reload();
                }
            });
    }

    $(document).ready(function() {
        $("div#modal-checkins div.modal-body").on('scroll', function() {
            var activeCheckinsTab = $('div#modal-checkins div.tab.active').index();

            if (parseInt($('div#modal-checkins div.tab.active span').text()) > $('div#modal-checkins div.modal-body div.person').length && checkinsRequest[activeCheckinsTab].status == 200) {
                if($('div#modal-checkins div.modal-body')[0].scrollTop + $('div#modal-checkins div.modal-body')[0].clientHeight >= $('div#modal-checkins div.modal-body').outerHeight()) {
                    renderCheckinsTab(activeCheckinsTab);
                }
            }
        })
    });

    function appendCheckinsPreloaders() {
        var preloader = '<div class="places-preloader">' +
            '<div class="place-icon-preloader animation"></div>' +
            '<div class="place-info-preloader">' +
            '<div class="place-name-preloader animation"></div>' +
            '<div class="place-desc-preloader animation"></div>' +
            '<div class="place-desc-preloader animation"></div>' +
            '</div>' +
            '</div>';

        $('div#modal-checkins div.modal-body').append(preloader);
        $('div#modal-checkins div.modal-body').append(preloader);
        $('div#modal-checkins div.modal-body').append(preloader);
    }

    var openedModals = [];

    $(document).ready(function() {
        $(document).on('click', '#modal-edit_title div.cover-upload, #modal-create_trip div.cover-upload', function () {
            $(this).find('input#cover').click();
        });

        $(document).on('click', '#modal-edit_title div.cover-upload input#cover, #modal-create_trip div.cover-upload input#cover', function (e) {
            e.stopPropagation();
        });

        $(document).on('change', 'input#cover', (function (e) {
            e.preventDefault();

            var self = this;
            var input = $(this)[0];

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(self).parents('div.cover-upload').attr('style', 'background: url("' + e.target.result + '")');
                };

                reader.readAsDataURL(input.files[0]);

                $(self).parents('div.cover-upload').addClass('with-image');
            }

            $('#modal-create_trip div.cover-upload').removeClass('required');
        }));

        // Mobile
        $('#modal-create_trip').on('click', '.btn-next-mobile', function (e) {
            e.preventDefault();

            var currentStep = $(this).data('step');
            var $planTitleInp = $('#modal-create_trip .plan-title').find(':required');
            var $planCoverInp = $('#modal-create_trip .plan-cover').find('[type="file"]');
            var PLAN_TITLE_ERR_MSG = $planTitleInp.attr('placeholder') || "Please enter a trip plan name";

            if(currentStep == 'plan-title' && checkPlanItemInp($planTitleInp, PLAN_TITLE_ERR_MSG)) {
                $('#modal-create_trip .plan-title').hide();
                $('#modal-create_trip .plan-description').hide();
                $('#modal-create_trip .plan-privacy').hide();
                $('#modal-create_trip .plan-type').show();
                $(this).data('step', 'plan-type');
                $('#modal-create_trip a.btn-close-mobile').replaceWith('<button class="btn-back-mobile"><i class="trav-angle-left"></i></button>');
                $('#modal-create_trip .btn-back-mobile').data('step', 'plan-type');
            };
            if(currentStep == 'plan-type') {
                $('#modal-create_trip .plan-type').hide();
                $('#modal-create_trip .plan-cover').show();
                $(this).hide();
                $('#modal-create_trip .btn-create-submit').show();
                $('#modal-create_trip a.btn-close-mobile').replaceWith('<button class="btn-back-mobile"><i class="trav-angle-left"></i></button>');
                $(this).data('step', 'plan-cover');
                $('#modal-create_trip .btn-back-mobile').data('step', 'plan-cover');
            };
        });

        function checkPlanItemInp(elem, errMsg) {
            if (elem && !elem.val().trim()) {
                errMsg && alert(errMsg);
                return false;
            }
            return true;
        }

        $('#modal-create_trip').on('click', '.btn-back-mobile', function (e) {
            e.preventDefault();

            var currentStep = $(this).data('step');

            if(currentStep == 'plan-cover') {
                $('#modal-create_trip .plan-type').show();
                $('#modal-create_trip .plan-cover').hide();
                $(this).data('step', 'plan-type');
                $('#modal-create_trip .btn-next-mobile').show();
                $('#modal-create_trip .btn-next-mobile').data('step', 'plan-type');
                $('#modal-create_trip .btn-create-submit').hide();
            };
            if(currentStep == 'plan-type') {
                $('#modal-create_trip .plan-title').show();
                $('#modal-create_trip .plan-description').show();
                $('#modal-create_trip .plan-privacy').show();
                $('#modal-create_trip .plan-type').hide();
                $(this).data('step', 'plan-title');
                $('#modal-create_trip button.btn-back-mobile').replaceWith('<a href="/home" class="btn-close-mobile" style="text-decoration: none;"><i class="trav-close-icon" dir="auto"></i></a>');
                $('#modal-create_trip .btn-next-mobile').show();
                $('#modal-create_trip .btn-next-mobile').data('step', 'plan-title');
            };
        });

        $(window).on('popstate keyup', function (e) {
            if (e.type === 'keyup' && e.which !== 27) {
                return;
            }

            if (modals.length) {
                var modal = modals[modals.length - 1];

                switch (modal.type) {
                    case 'modal':
                        $(modal.element).modal('hide');
                        break;
                    case 'chat':
                        $('.places-chats.window').removeClass('opened');
                        $('.all-plan-chats.window').removeClass('opened');
                        $('.place-chats.window').removeClass('opened');
                        $('.place-chat.window').removeClass('opened');
                    case 'place':
                        $('.btn-back-mobile').click();
                        break;
                    default:
                        break;
                }

                return false;
            }
        });

        $(document).on('hidden.bs.modal', function (e) {
            modals.pop();
        });

        $(document).on('shown.bs.modal', function (e) {
            modals.push({
                type: 'modal',
                element: e.target
            });
            window.history.pushState('forward', null, "");
        });

        $('div#modal-create_trip input[name=title]').keydown(function (e) {
            if (e.which === 13) {
                $('div#modal-create_trip textarea[name=description]').focus();
            }
        });
    });

    $(window).on('load', function() {
        @if(isset($trip) && is_object($trip))
        $('#planContents').html(preloadContent);
        renderPlaceMarkers(true);
        setRouteSources();
        fitBounds();

        if($('.sidebar').hasClass('invited') && !$('span.navbar-text.mobile-hide:visible').length) {
            $('.card-trip_info.card-collapse').collapse('hide')
        }
        updatePlanContents(false, false);
        updatePlanData();
        @endif
    });

    updateChats();
</script>
