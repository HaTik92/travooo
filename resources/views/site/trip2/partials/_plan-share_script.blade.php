<script>
    var sharedUrl = '';
    var sharedText = '';

    function updateSharePlanPopup() {
        var destinations = [];
        var countries = [];
        var planStartDate = null;


        trip_places_arr.forEach(function(place, index) {
            if (!planStartDate) {
                planStartDate = moment(place.time).format('DD MMMM yy');
            }

            var countryId = place.city.countries_id;
            var coordinates = [parseFloat(place.lng), parseFloat(place.lat)];

            if (!destinations[countryId]) {
                destinations[countryId] = [];
            }

            destinations[countryId].push({
                'coordinates': coordinates
            });

            countries[countryId] = {
                'title': place.city.country.trans[0].title,
                'iso': place.city.country.iso_code,
            }
        });


        $('div#modal-share_popup div.shared-content[data-type=plan] div.plan-description div.date').text('Went on a trip on ' + planStartDate);
        $('div#modal-share_popup div.shared-content[data-type=plan] div.plan-name').html('<a target="_blank" class="shared-url" href="/trip/plan/{{$trip->id}}">{{$trip->title}}</a>');
        $('div#modal-share_popup div.shared-content[data-type=plan] div.plan-map img').attr('src', '{{$plan_map_preview}}');

        $('div#modal-share_popup div.shared-content[data-type=plan] div.swiper-container div.swiper-wrapper').html('');

        destinations.forEach(function(countryDestinations, countryId) {
            $('div#modal-share_popup div.shared-content[data-type=plan] div.swiper-container div.swiper-wrapper').append('<div class="plan-country swiper-slide">\n' +
                '                                    <div class="flag">\n' +
                '                                        <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/' + countries[countryId].iso.toLowerCase() + '.png" alt="">\n' +
                '                                    </div>\n' +
                '                                    <div class="desc">\n' +
                '                                        <div class="title">' + countries[countryId].title + '</div>\n' +
                '                                        <div class="destinations">' + countryDestinations.length + ' Destination(s)</div>\n' +
                '                                    </div>\n' +
                '                                </div>');
        });

        var countriesSwiper = new Swiper('div#modal-share_popup div.swiper-container', {
            slidesPerView: 'auto',
            observer: true,
            observeParents: true,
            navigation: {
                nextEl: '.swiper-countries-control .swiper-next',
                prevEl: '.swiper-countries-control .swiper-prev'
            }
        });
    }

    $(document).ready(function() {
        $(document).off('click', 'img.share-published-plan, .plan-share-button');
        $(document).on('click', 'img.share-published-plan, .plan-share-button', function() {
            $('div#modal-share_popup div.shared-content').hide();
            $('div#modal-share_popup div.shared-content[data-type=plan]').show();
            sharedUrl = $('div#modal-share_popup div.shared-content[data-type=plan] a.shared-url').attr('href');
            sharedText = $('div#modal-share_popup div.shared-content[data-type=plan] div.plan-description div.date').text();
        });

        $(document).off('click', 'div.trip-place-reactions div.share');
        $(document).on('click', 'div.trip-place-reactions div.share', function() {
            $('div#modal-share_popup div.shared-content').hide();

            var placeId = $(this).parents('div.trip-place-wrap').attr('data-placeid');
            var placeStartDate = null;

            trip_places_arr.forEach(function(place, index) {
                if (place.trip_place_id != placeId) {
                    return;
                }

                placeStartDate = moment(place.time).format('DD MMMM yy');

                $('div#modal-share_popup div.shared-content[data-type=step] div.plan-description div.date').text('Went on a place on ' + placeStartDate);
                $('div#modal-share_popup div.shared-content[data-type=step] div.plan-name').html('<a target="_blank" href="/trip/plan/{{$trip->id}}">{{$trip->title}}</a><div class="seporator">•</div><a target="_blank" class="shared-url" href="/trip/plan/{{$trip->id}}#step-' + placeId + '">' + place.name + '</a>');
                $('div#modal-share_popup div.shared-content[data-type=step] div.plan-map img').attr('src', place.place_map_preview);

                $('div#modal-share_popup div.shared-content[data-type=step] div.swiper-container div.swiper-wrapper').html('');

                $('div#modal-share_popup div.shared-content[data-type=step] div.swiper-container div.swiper-wrapper').append('<div class="plan-country swiper-slide">\n' +
                    '                                    <div class="flag">\n' +
                    '                                        <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/' + place.city.country.iso_code.toLowerCase() + '.png" alt="">\n' +
                    '                                    </div>\n' +
                    '                                    <div class="desc">\n' +
                    '                                        <div class="title">' + place.city.country.trans[0].title + '</div>\n' +
                    '                                        <div class="destinations">' + place.city_name + ', ' + place.name + '</div>\n' +
                    '                                    </div>\n' +
                    '                                </div>');
            });

            $('div#modal-share_popup button.share').attr('data-id', placeId);

            $('div#modal-share_popup div.shared-content[data-type=step]').show();
            $('div#modal-share_popup').modal('show');

            sharedUrl = $('div#modal-share_popup div.shared-content[data-type=step] a.shared-url').attr('href');
            sharedText = $('div#modal-share_popup div.shared-content[data-type=step] div.plan-description div.date').text();
        });

        $(document).off('click', 'div.cover-block button.plan_share_button');
        $(document).on('click', 'div.cover-block button.plan_share_button', function() {
            $('div#modal-share_popup div.shared-content').hide();

            var type = $(this).parents('div.cover-block').attr('type');
            var url = $(this).parents('div.cover-block').attr('url');
            var mediaId = $(this).parents('div.cover-block').attr('trip-media-id');
            var media = '';

            if (type == 1) {
                media = '<img src="' + url + '" alt="" />';
            } else {
                media = '<video controls>\n' +
                    '  <source src="' + url + '" type="video/mp4">\n' +
                    '  Your browser does not support the video tag.\n' +
                    '</video>';
            }

            $('div#modal-share_popup div.shared-content[data-type=media] div.media').html(media);
            $('div#modal-share_popup div.shared-content[data-type=media]').show();

            $('div#modal-share_popup button.share').attr('data-id', mediaId);

            $('div#modal-share_popup').modal('show');

            sharedUrl = url;
            sharedText = '';
        });

        $(document).off('click', 'div#modal-share_popup button.share');
        $(document).on('click', 'div#modal-share_popup button.share', function() {
            var type = $('div#modal-share_popup div.shared-content:visible').attr('data-type');
            var url = '';
            var comment = $('div#modal-share_popup input.comment').val();
            var data = {
                'comment': comment
            };

            var id = $('div#modal-share_popup button.share').attr('data-id');

            var sharedType = '';

            switch (type) {
                case 'plan':
                    url = '{{url('/trip/share-plan')}}';
                    data.plan_id = '{{$trip->id}}';
                    sharedType = 'Trip';
                    break;
                case 'media':
                    url = '{{url('/trip/share-media')}}';
                    data.trip_media_id = id;
                    sharedType = 'Media';
                    break;
                case 'step':
                    url = '{{url('/trip/share-place')}}';
                    data.trip_place_id = id;
                    sharedType = 'Step';
                    break;
            }

            $.ajax({
                method: "POST",
                url: url,
                data: data
            })
                .done(function (res) {
                    if (res.status === 'success') {
                        $('div#modal-share_popup').modal('hide');
                        $('div#modal-share_popup input.comment').val('');

                        toastr.options = {
                            timeOut: 3000,
                            extendedTimeOut: 0,
                            positionClass: "toast-bottom-right",
                        };

                        toastr.info('You shared a ' + sharedType + ' from Your trip plan');
                    }
                });

        });
    });

    function socialShare(type)
    {
        var socialUrl = '';
        var fullSharedUrl = encodeURIComponent('https://www.travooo.com' + sharedUrl);

        switch(type){
            case 'whatsapp':
                /*  https://web.whatsapp.com/send?text=
                    https://wa.me/(phone)?text=hello
                    https://api.whatsapp.com/send/?phone&text=hello */
                socialUrl = `https://web.whatsapp.com/send?text=${sharedText} ${fullSharedUrl}`;
                break;

            case 'facebook':
                //t= // t for text
                socialUrl = `https://www.facebook.com/sharer.php?u=${fullSharedUrl}&t=${sharedText}`;
                break;

            case 'twitter':
                socialUrl = `https://twitter.com/intent/tweet?text=${sharedText} ${fullSharedUrl}`;
                break;

            case 'pintrest':
                //&media=&description=
                socialUrl = `https://pinterest.com/pin/create/link/?url=${fullSharedUrl}&description=${sharedText}`;
                break;

            case 'tumblr':
                //t= // t for text
                socialUrl = `https://www.tumblr.com/share?t=hello&v=3&u=${fullSharedUrl}&t=${sharedText}`;
                break;
        }

        if(socialUrl) {
            window.open(socialUrl, '_blank', 'location=yes,scrollbars=yes,status=yes');
        }
    }
</script>