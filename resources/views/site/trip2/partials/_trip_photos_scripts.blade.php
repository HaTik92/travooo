<script>

      $(document).ready(function(){
         LoadMore_Show.init($('div.trip-photos-holder.place-{{$tp->id}} .post-media-inner'));
    })

     //photos lightGallery
    $('div.trip-photos-holder.place-{{$tp->id}} .lightbox_imageModal').on('click', function () {
        var slideNum = $(this).attr('data-id');

        let $lg = $(this).lightGallery({
            dynamic: true,
            index: parseInt(slideNum),
            dynamicEl: [
                    @foreach($trip_place_media AS $photo)
                        @if ($photo->media->type === \App\Models\ActivityMedia\Media::TYPE_VIDEO)
                            {
                                'poster': "{{ $photo->media->source_url ? $photo->media->source_url : ''}}",
                                'subHtml': `<div trip-media-id="{{$photo->id}}" class="cover-block"></div>`,
                                'thumb': "{{ $photo->media->source_url ? $photo->media->source_url : ''}}",
                                'html': '<video class="lg-video-object lg-html5" controls preload="none"><source src="{{ get_profile_media($photo->media->url) }}" type="video/mp4">Your browser does not support HTML5 video</video>',
                            },
                        @else
                            {
                                "src": "{{ get_profile_media($photo->media->url) }}",
                                'thumb': "{{ get_profile_media($photo->media->url) }}",
                                'subHtml': `<div trip-media-id="{{$photo->id}}" class="cover-block"></div>`
                            },
                        @endif
                    @endforeach
            ],
            addClass: 'main-gallery-block main-gallery-block-trip-photos',
            pager: false,
            share: false,
            hideControlOnEnd: true,
            loop: true,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            currentPagerPosition: 'middle',
            thumbMargin: 20,
            thumbContHeight: 200,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            enableSwipe: false,
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');

        });

        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });

        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');

            if (!$(currentImg).length) {
                currentImg = $('.main-gallery-block .lg-current .lg-video');
            }

            let currentCommentIs = $(subTtl).hasClass('comment-block');

            setTimeout(function () {
                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();

                if (currentCommentIs) {
                    $(currentImgWrap).css('padding-right', commentWidth);
                    $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    $(subTtl).css('width', currentWidth);
                }
                // $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();
            }, 250);
        };

        function loadMoreCommentsOnScroll(elem) {
            var loadingCount = $(elem).parent().find('.post-comment-top-info span.loading-count strong').html();
            var allCount = $(elem).parent().find('.post-comment-top-info span.all-comments-count').html();

            if (loadingCount === allCount) {
                $(elem).find('.loadMore1').hide();
            }

            if((checkLoaderVisible($(elem))) || (loadingCount == 0 && loadingCount < allCount)) {
                loadMoreComment(1, $(elem));
            }
        }

        function checkLoaderVisible(elem) {
            var isVisible = $(elem).find('.loadMore1:visible').length;
            
            if (isVisible && (elem[0].scrollHeight - elem.scrollTop() >= (elem.outerHeight() - 1))) {
                return true;
            }

            return false;
        }

        $lg.on('onBeforeSlide.lg', function (e, pi, i) {
            // var id = $('.lg-current .cover-block').attr('trip-media-id');
            // console.log(id)
            // console.log(this)
        });

        $lg.on('onSlideItemLoad.lg', function (e, i) {
            var id = $('div.lg .lg-item').eq(i).find('.cover-block').attr('trip-media-id');
            var _self = this;

            $.ajax({
                method: "GET",
                url: "{{url('medias/load')}}" + '/' + id
            })
                .done(function (res) {
                    $('.cover-block[trip-media-id=' + id + ']').replaceWith(res);

                    CommentEmoji($('.mediaCommentForm'));

                    $('.gallery-comment-inner').on('scroll', function() {
                        loadMoreCommentsOnScroll($('.cover-block[trip-media-id=' + id + ']'))
                    });

                    loadMoreCommentsOnScroll($('.cover-block[trip-media-id=' + id + ']'));
                    loadMoreComment(1, $('.cover-block[trip-media-id=' + id + ']'), 1);
                });

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            });


        });

        $lg.on('onAfterSlide.lg', function () {
            let media_id = $('.main-gallery-block .lg-current').find('.cover-block').attr('id');
            let type = $('.main-gallery-block').find('.'+ media_id +'-media-like-icon');
            $('.'+ media_id +'-like-count b').html($('.'+ media_id +'-media-liked-count b').html())
            //Comment_Show.init($('.photo-comment-' + media_id));

            if(type.attr('data-type') == 'like'){
                type.html('<i class="trav-heart-fill-icon" style="color:#ff5a79;"></i>')
            }else{
                type.html(' <i class="trav-heart-icon"></i>')
            }
            setWidth();
        });

        $lg.on('onAfterOpen.lg', function (e) {
            $('.lg-thumb-item').each(function(i) {
                var counters = $('div.thumbnail.lightbox_imageModal').eq(i).parent().find('.counters').html();
                $(this).find('div.counters').remove();
                $(this).append('<div class="counters" data-mediaid="' + $('div.thumbnail.lightbox_imageModal').eq(i).data('mediaid') + '">' + counters + '</div>');

                if ($('div.trip-place[data-id=' + {{$tp->id}} + '] div.thumbnail.lightbox_imageModal').eq(i).data('video') == 1) {
                    $(this).find('div.thumb-play-icon').remove();
                    $(this).append('<div class="thumb-play-icon"></div>');
                }
            });

            $('div.lg-thumb-item').css('width', 'auto');

            setTimeout(function() {
                $('div.large-image video').each(function(index, el) {
                    $(this).get(0).pause();
                    var mediaId = $(this).parent('.large-image').data('mediaid');
                    $('div.cover-block#' + mediaId).parent().find('div.lg-video-cont img').trigger('click');
                    $('div.cover-block#' + mediaId).parent().find('div.lg-video-cont video').get(0).currentTime = $(this).get(0).currentTime;
                });
            }, 250);
            setTimeout(function() {
                $('.main-gallery-block .lg-thumb-outer').show();
            }, 1000);
        });
    });

    $('body').click(function(evt){
       if(evt.target.className == "lg-inner")
          $('.lg-close.lg-icon').click();
    });

    //photos load more
    var LoadMore_Show = (()=>{
    var total_rows = 0;
    var display_unit = 3;
    function init(obj){
        var target = $(obj).find('.sortBody');
        if(target.attr('report-photo-rows')) {
            return;
        } else {
           if (target.find(">div").hasClass('media-not-found')){
               return;
           } else {
                target.find(">div").hide();
                var btn = $('<a href="javascript:;"/>').addClass('load-more-link d-inline-block mt-2').text('Load more...').click(function(){
                    LoadMore_Show.inc(target)
                });

                get_totalrows(target, btn)
            }
        }
    }

    function reload(obj)
    {
        var target = $(obj).find('.sortBody');
        target.find(">div").hide();
        target.find(">div").removeClass('displayed');
        get_totalrows(target);
    }

    function get_totalrows(obj, btn)
    {
        this.total_rows = obj.find(">div").length;
        obj.attr('report-photo-rows', this.total_rows);
        if(!obj.attr('report-photo-page'))
            obj.attr('report-photo-page', 1);
        show_rows(obj, obj.attr('report-photo-page'), btn);
    }

    function show_rows(obj, page, btn)
    {
        this.total_rows = parseInt(obj.attr('report-photo-rows'));
        var showing_rows = parseInt(page) * display_unit;
       if(this.total_rows>3){
           obj.after(btn);
       }
        showing_rows = this.total_rows > showing_rows ? showing_rows : this.total_rows;
        for(var i = 0; i < showing_rows; i++)
        {
            obj.find(">div").eq(i).show().addClass("displayed");
        }
    }

    function increase_show(obj)
    {
        var current_page = parseInt(obj.attr('report-photo-page'));
        obj.attr('report-photo-page', (current_page + 1));
        get_totalrows(obj);
    }


    return  {
        init: init,
        inc: increase_show,
        reload: reload
    }
})();

</script>