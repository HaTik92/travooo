<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - Profile</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">

        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">
            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                @include('site/profile/partials/left_menu')
            </div>

            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">


                    <div class="post-block post-map-wrapper">
                        <div class="post-map-block">
                            <div class="post-map-inner">
                                <div id="myMap" style='width:595px;height:770px;'></div>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">

                    <div class="post-block post-side-profile">
                        <div class="image-wrap" style="height:190px">
                            <div class="profile-side-block">
                                <div class="avatar-layer">
                                    <div class="ava-inner">
                                        <img src="{{check_profile_picture($user->profile_picture)}}" alt=""
                                             class="avatar" style="width:80px;height:80px;">
                                        <a href="#" class="edit-ava-link">
                                            <img src="./assets2/image/profile-ava-edit-img.png" alt="">
                                        </a>
                                    </div>
                                    <div class="ava-txt">
                                        <h4 class="ava-name">{{$user->name}}</h4>
                                        <p class="sub-ttl">{{$user->nationality}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="post-image-info">
                                <div></div>
                                <div class="follow-btn-wrap" id="follow_user_button">

                                </div>
                            </div>
                        </div>
                        <div class="post-profile-info">
                            <ul class="profile-info-list">
                                <li>
                                    <p class="info-count">{{count($user->posts)}}</p>
                                    <p class="info-label">Posts</p>
                                </li>
                                <li>
                                    <p class="info-count">{{$user->followers}}</p>
                                    <p class="info-label">Followers</p>
                                </li>
                                <li>
                                    <p class="info-count">{{$user->following}}</p>
                                    <p class="info-label">Following</p>
                                </li>
                            </ul>
                        </div>
                        <div class="post-txt-side">
                            <h5 class="ttl">About</h5>
                            <p>{{$user->about}}</p>
                        </div>
                        <div class="post-profile-side-foot">
                            <div class="post-profile-foot-inner">
                                <a href="{{$user->website}}" class="profile-website-link" target="_blank">
                    <span class="round-icon">
                      <i class="trav-link"></i>
                    </span>
                                    <span>@lang('place.website')</span>
                                </a>
                                <ul class="profile-social-list">
                                    <li>
                                        <a href="{{$user->facebook}}" class="facebook" target="_blank">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{$user->twitter}}" class="twitter" target="_blank">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{$user->instagram}}" class="instagram">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="aside-footer">
                        <ul class="aside-foot-menu">
                            <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                            <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                            <li><a href="{{url('/')}}">Advertising</a></li>
                            <li><a href="{{url('/')}}">Cookies</a></li>
                            <li><a href="{{url('/')}}">More</a></li>
                        </ul>
                        <p class="copyright">Travooo © 2017</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modals -->


<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>
<script src="{{url('assets2/js/script.js')}}"></script>


<script>

    //595x360
    var map;

    function initMap() {
        var bounds = new google.maps.LatLngBounds();
        var infowindow = new google.maps.InfoWindow();

        map = new google.maps.Map(document.getElementById('myMap'), {
            zoom: 6,
            center: new google.maps.LatLng(28.36539800, -81.52126420),
            mapTypeId: 'roadmap'
        });


        function CustomMarker(latlng, map, img, place_name, args) {
            this.latlng = latlng;
            this.args = args;
            this.img = img;
            this.place_name = place_name;
            this.setMap(map);
        }

        CustomMarker.prototype = new google.maps.OverlayView();

        CustomMarker.prototype.draw = function () {

            var self = this;

            var div = this.div;

            if (!div) {

                div = this.div = document.createElement('div');

                div.className = "dest-img-block";
                div.innerHTML = "<img class='dest-img' src='" + this.img + "' alt='' style='width:30px;height:30px;'><div class='icon-wrap'><ul class='icon-list'><li><i class='trav-comment-icon'></i></li></ul></div>";


                div.style.position = 'absolute';


                if (typeof(self.args.marker_id) !== 'undefined') {
                    div.dataset.marker_id = self.args.marker_id;
                }

                google.maps.event.addDomListener(div, "click", function (event) {
                    google.maps.event.trigger(self, "click");
                });

                var panes = this.getPanes();
                panes.overlayImage.appendChild(div);
            }

            var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

            if (point) {
                div.style.left = point.x + 'px';
                div.style.top = point.y + 'px';
            }
        };

        CustomMarker.prototype.remove = function () {
            if (this.div) {
                this.div.parentNode.removeChild(this.div);
                this.div = null;
            }
        };

        CustomMarker.prototype.getPosition = function () {
            return this.latlng;
        };


        //to use it
                @foreach($happenings AS $checkin)
                @if($checkin['lat'] && $checkin['lng'])

        var overlay{{$checkin['post_id']}} = new CustomMarker(
            new google.maps.LatLng({{$checkin['lat']}},{{$checkin['lng']}}),
            map,
            "{{$checkin['profile_picture']}}",
            "{{$checkin['place_name']}}",
            {
                marker_id: '{{$checkin["post_id"]}}',
                colour: 'Red'
            }
            );

        google.maps.event.addListener(overlay{{$checkin['post_id']}}, 'click', (function (overlay{{$checkin['post_id']}}) {
            return function () {
                infowindow.setContent("{{$checkin['photo']}} {{$checkin['place_name']}} @ {{$checkin['date']}}");
                infowindow.open(map, overlay{{$checkin['post_id']}});
            }
        })(overlay{{$checkin['post_id']}}));

        bounds.extend(new google.maps.LatLng({{$checkin['lat']}},{{$checkin['lng']}}));


        @endif
        @endforeach

        map.fitBounds(bounds);
        //map.setCenter(htmlMarker.getPosition());


    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqTXNqPdQlFIV5QNvYQeDrJ5vH0y9_D-M&callback=initMap&language=en">
</script>
</body>

</html>