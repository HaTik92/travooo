<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - Profile</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">

        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">
            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li class="active">
                        <a href="{{url('profile-about')}}">
                            <i class="trav-profile-icon"></i>
                            <span>About</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('profile-posts')}}">
                            <i class="trav-home-icon"></i>
                            <span>Posts</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('profile-travel-history')}}">
                            <i class="trav-travel-history-icon"></i>
                            <span>Travel History</span>
                        </a>
                    </li>
                </ul>

                <ul class="left-outside-menu">
                    <li>
                        <a href="{{url('profile-map')}}">
                            <i class="trav-map-o"></i>
                            <span>My Map</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('profile-visited')}}">
                            <i class="trav-home-icon"></i>
                            <span>Visited</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('profile.plans')}}">
                            <i class="trav-trip-plans-icon"></i>
                            <span>Trip Plans</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('profile-favourites')}}">
                            <i class="trav-empty-star-icon"></i>
                            <span>Favourites</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('profile-photos')}}">
                            <i class="trav-photos-icon"></i>
                            <span>@lang('place.photos')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('profile-videos')}}">
                            <i class="trav-videos-icon"></i>
                            <span>@lang('place.videos')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('profile-interests')}}">
                            <i class="trav-interests-icon"></i>
                            <span>@lang('profile.interests')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('profile-reviews')}}">
                            <i class="trav-review-icon"></i>
                            <span>@lang('place.reviews')</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('profile-badges')}}">
                            <i class="trav-badges-icon"></i>
                            <span>Badges</span>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">


                    <div class="post-block post-top-bordered">
                        <div class="post-side-top top-arrow">
                            <div class="post-top-txt horizontal">
                                <h3 class="side-ttl">All favorites <span class="count">27</span></h3>
                            </div>
                            <div class="side-right-control">
                                <div class="sort-by-select">
                                    <label>Sort by</label>
                                    <div class="sort-select-wrap">
                                        <select class="form-control" id="">
                                            <option>@lang('time.date')</option>
                                            <option>Item</option>
                                            <option>Item2</option>
                                        </select>
                                        <i class="trav-caret-down"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="activity-inner">
                            <div class="activ-row">
                                <div class="activ-inside-block">
                                    <a href="#" class="close-btn">
                                        <span aria-hidden="true">&times;</span>
                                    </a>
                                    <div class="activ-img">
                                        <img src="http://placehold.it/120x120" alt="image">
                                    </div>
                                    <div class="activ-inside-txt">
                                        <div class="activ-txt-inner">
                                            <h2 class="act-ttl"><a href="#" class="link-txt">New York City</a></h2>
                                            <div class="txt-block">
                                                <p>City in <a href="#" class="link-txt">United State of America</a></p>
                                            </div>
                                            <div class="btn-block">
                                                <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('buttons.general.follow')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="activ-after-txt">
                                    <p>
                                        <img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                                        Saved from&nbsp;
                                        <a href="#" class="link-txt">John Post</a>&nbsp;
                                        2 days ago
                                    </p>
                                </div>
                            </div>
                            <div class="activ-row">
                                <div class="activ-inside-block">
                                    <div class="activ-img">
                                        <img src="http://placehold.it/120x120" alt="image">
                                    </div>
                                    <div class="activ-inside-txt">
                                        <div class="activ-txt-inner">
                                            <h2 class="act-ttl"><a href="#" class="link-txt">Diana Walker</a>
                                                &nbsp;<span>@lang('place.photo')</span></h2>
                                            <div class="txt-block">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit curabitur
                                                    enean bibendum venenatis pharetra duis et risus nec lorem...</p>
                                            </div>
                                            <div class="btn-block">
                                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                                            class="trav-view-plan-icon"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="activ-after-txt">
                                    <p>
                                        <img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                                        Saved from&nbsp;
                                        <a href="#" class="link-txt">Diana Walker Post</a>&nbsp;
                                        3 days ago
                                    </p>
                                </div>
                            </div>
                            <div class="activ-row">
                                <div class="activ-inside-block">
                                    <div class="activ-img">
                                        <img src="http://placehold.it/120x120" alt="image">
                                    </div>
                                    <div class="activ-inside-txt">
                                        <div class="activ-txt-inner">
                                            <h2 class="act-ttl"><img src="http://placehold.it/36x20" class="small-flag"
                                                                     alt="flag">&nbsp; <a href="#" class="link-txt">Unites
                                                    States of America</a></h2>
                                            <div class="txt-block">
                                                <p>@lang('region.country_in') North America</p>
                                            </div>
                                            <div class="btn-block">
                                                <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('buttons.general.follow')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="activ-row">
                                <div class="activ-inside-block">
                                    <div class="activ-img">
                                        <img src="http://placehold.it/120x120" alt="image">
                                    </div>
                                    <div class="activ-inside-txt">
                                        <div class="activ-txt-inner">
                                            <h2 class="act-ttl"><a href="#" class="link-txt">Diana Walker</a>
                                                &nbsp;<span>Video</span></h2>
                                            <div class="txt-block">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam,
                                                    quaerat? Culpa officia expedita eaque nam?</p>
                                            </div>
                                            <div class="btn-block">
                                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                                            class="trav-view-plan-icon"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="activ-after-txt">
                                    <p><img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                                        Saved from&nbsp;
                                        <a href="#" class="link-txt">Robe Walker Post</a>&nbsp;
                                        3 days ago</p>
                                </div>
                            </div>
                            <div class="activ-row">
                                <div class="activ-inside-block">
                                    <div class="activ-img">
                                        <img src="http://placehold.it/120x120" alt="image">
                                    </div>
                                    <div class="activ-inside-txt">
                                        <div class="activ-txt-inner">
                                            <h2 class="act-ttl"><a href="#" class="link-txt">Independence day</a></h2>
                                            <div class="txt-block">
                                                <p>National Holiday in <a href="#" class="link-txt">United State of
                                                        America</a></p>
                                            </div>
                                            <div class="btn-block">
                                                <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('buttons.general.follow')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="activ-row">
                                <div class="activ-inside-block">
                                    <div class="photos-gallery">
                                        <div class="img-wrap">
                                            <img src="http://placehold.it/120x59" alt="image">
                                        </div>
                                        <div class="img-wrap">
                                            <img src="http://placehold.it/59x59" alt="image">
                                        </div>
                                        <div class="img-wrap">
                                            <img src="http://placehold.it/59x59" alt="image">
                                            <a href="#" class="cover-link">+8</a>
                                        </div>
                                    </div>
                                    <div class="activ-inside-txt">
                                        <div class="activ-txt-inner">
                                            <h2 class="act-ttl"><a href="#" class="link-txt">Jane Grogan</a>
                                                &nbsp;<span>@lang('place.photos')</span></h2>
                                            <div class="txt-block">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                                    curabitur.</p>
                                            </div>
                                            <div class="btn-block">
                                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                                            class="trav-view-plan-icon"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="activ-after-txt">
                                    <p>
                                        <img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                                        Saved from&nbsp;
                                        <a href="#" class="link-txt">Jane Grogan Post</a>&nbsp;
                                        1 month ago
                                    </p>
                                </div>
                            </div>
                            <div class="activ-row">
                                <div class="activ-inside-block">
                                    <div class="activ-img">
                                        <img src="http://placehold.it/120x120" alt="image">
                                    </div>
                                    <div class="activ-inside-txt">
                                        <div class="activ-txt-inner">
                                            <h2 class="act-ttl"><a href="#" class="link-txt">4 Days in USA</a>
                                                &nbsp;<span>@lang('trip.trip_plan')</span></h2>
                                            <div class="txt-block">
                                                <p>Create by <a href="#" class="link-txt">Stephen Bugno</a></p>
                                            </div>
                                            <div class="btn-block">
                                                <button class="btn btn-light-grey btn-bordered btn-grey-txt">View Plan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="activ-row">
                                <div class="activ-inside-block">
                                    <div class="activ-img">
                                        <img src="http://placehold.it/120x120" alt="image">
                                    </div>
                                    <div class="activ-inside-txt">
                                        <div class="activ-txt-inner">
                                            <h2 class="act-ttl"><a href="#" class="link-txt">Martin Kennedy</a></h2>
                                            <div class="txt-block">
                                                <p>“Aliquam tempor, tellus ut condimentum posuere arcu velit vulputate
                                                    nec hendrerit nunc ex et elit sed maximus orci lorem iaculis...” <a
                                                            href="#" class="link-txt">More</a></p>
                                            </div>
                                            <div class="btn-block">
                                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                                            class="trav-view-plan-icon"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="activ-after-txt">
                                    <p><img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                                        Saved from&nbsp;
                                        <a href="#" class="link-txt">Diana Walker Post</a>&nbsp;
                                        3 days ago</p>
                                </div>
                            </div>
                            <div class="activ-row">
                                <div class="activ-inside-block">
                                    <div class="activ-img">
                                        <img src="http://placehold.it/120x120" alt="image">
                                    </div>
                                    <div class="activ-inside-txt">
                                        <div class="activ-txt-inner">
                                            <h2 class="act-ttl"><a href="#" class="link-txt">Lisa Martinez</a> tip about
                                                <a href="#" class="link-txt">Haneda Airport</a></h2>
                                            <div class="txt-block">
                                                <p>“Nullam accumsan, eros in consequat imperdiet, lacus mi iaculis
                                                    viverra est ante et eros fusce accumsan sed scelerisque...” <a
                                                            href="#" class="link-txt">More</a></p>
                                            </div>
                                            <div class="btn-block">
                                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                                            class="trav-view-plan-icon"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="activ-after-txt">
                                    <p>Saved from a&nbsp;
                                        <a href="#" class="link-txt">@lang('trip.trip_plan')</a>&nbsp;
                                        by&nbsp;
                                        <a href="#" class="link-txt">Suzanne</a>&nbsp;
                                        3 months ago</p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">


                    <div class="post-block post-side-profile sm-profile">
                        <div class="image-wrap">
                            <img src="http://placehold.it/385x125" alt="">
                            <div class="post-image-info">
                                <div class="avatar-layer">
                                    <div class="ava-inner">
                                        <img src="http://placehold.it/58x58" alt="" class="avatar">
                                        <a href="#" class="edit-ava-link">
                                            <img src="./assets2/image/profile-ava-edit-img.png" alt="">
                                        </a>
                                    </div>
                                    <div class="ava-txt">
                                        <h4 class="ava-name">Justin baker</h4>
                                        <p class="sub-ttl">United States</p>
                                    </div>
                                </div>
                                <div class="follow-btn-wrap">
                                    <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side">
                                        <i class="trav-user-plus-icon"></i>
                                        <span>@lang('buttons.general.follow')</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="post-profile-info">
                            <ul class="profile-info-list">
                                <li>
                                    <p class="info-count">125</p>
                                    <p class="info-label">Posts</p>
                                </li>
                                <li>
                                    <p class="info-count">58</p>
                                    <p class="info-label">Followers</p>
                                </li>
                                <li>
                                    <p class="info-count">2</p>
                                    <p class="info-label">Following</p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="post-block post-side-search">
                        <div class="search-wrapper">
                            <input class="" id="" placeholder="@lang('profile.search_your_visited_places')" type="text">
                            <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
                        </div>
                    </div>


                    <div class="post-block post-side-type">
                        <div class="type-label">Type</div>
                        <div class="type-progress-block">
                            <div class="type-line">
                                <div class="label">@choice('trip.place', 2)</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="count">17</div>
                            </div>
                            <div class="type-line">
                                <div class="label">Posts</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 30%" aria-valuenow="30"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="count">5</div>
                            </div>
                            <div class="type-line">
                                <div class="label">Events</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 18%" aria-valuenow="18"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="count">3</div>
                            </div>
                            <div class="type-line">
                                <div class="label">@lang('place.photos')</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 12%" aria-valuenow="12"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="count">2</div>
                            </div>
                            <div class="type-line">
                                <div class="label">@lang('place.videos')</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 35%" aria-valuenow="35"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="count">6</div>
                            </div>
                            <div class="type-line">
                                <div class="label">Trip Plans</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 9%" aria-valuenow="9"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="count">1</div>
                            </div>
                        </div>
                    </div>


                    <div class="aside-footer">
                        <ul class="aside-foot-menu">
                            <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                            <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                            <li><a href="{{url('/')}}">Advertising</a></li>
                            <li><a href="{{url('/')}}">Cookies</a></li>
                            <li><a href="{{url('/')}}">More</a></li>
                        </ul>
                        <p class="copyright">Travooo © 2017</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
<!-- asking popup -->
<div class="modal fade white-style" data-backdrop="false" id="askingPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-750" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-asking-block">
                <div class="post-asking-inner">
                    <div class="avatar-wrap">
                        <img class="ava" src="http://placehold.it/50x50" alt="">
                    </div>
                    <div class="ask-content">
                        <div class="ask-row-inner">
                            <div class="ask-row">
                                <div class="ask-label">Asking</div>
                                <div class="ask-txt">
                                    <div class="radio-wrapper">
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="askingRadios" id="forRecommendation"
                                                   value="forRecommendation" type="radio">
                                            <label for="forRecommendation">For recommendations</label>
                                        </div>
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="askingRadios" id="forTips"
                                                   value="forTips" type="radio">
                                            <label for="forTips">For tips</label>
                                        </div>
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="askingRadios" id="aboutTripPlan"
                                                   value="aboutTripPlan" type="radio">
                                            <label for="aboutTripPlan">About a trip plan</label>
                                        </div>
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="askingRadios" id="generalQuestion"
                                                   value="generalQuestion" type="radio">
                                            <label for="generalQuestion">A general question</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">Topic</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="A topic for your question, For example “Photography, New York”"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">Destionation</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="@lang('discussion.strings.place_city_country')"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">Tips about</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="@lang('discussion.strings.place_city_country')"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">@lang('trip.trip_plan')</div>
                                <div class="ask-txt">
                                    <div class="ask-input-btn-line">
                                        <div class="ask-input-wrap">
                                            <textarea name="" id="" cols="" rows=""
                                                      placeholder="Select one of your trip plans"></textarea>
                                        </div>
                                        <button data-toggle="modal" data-target="#tripPlanSelectionPopup" type="button"
                                                class="btn btn-light-grey btn-bordered">Select
                                        </button>
                                    </div>
                                    <div class="ask-trip-card">
                                        <div class="img-wrap">
                                            <img src="http://placehold.it/137x170" alt="">
                                        </div>
                                        <div class="card-inner">
                                            <div class="card-close">
                                                <i class="fa fa-close"></i>
                                            </div>
                                            <h4 class="card-ttl">Over the Mediterranean Sea</h4>
                                            <div class="card-info-line">
                                                18 to 21 Sep 2017&nbsp;&nbsp;·&nbsp;&nbsp;3 Days&nbsp;&nbsp;·&nbsp;&nbsp;12K
                                                km
                                            </div>
                                            <div class="dest-label">
                                                Destinations
                                                <b>13</b>
                                            </div>
                                            <div class="destinations-img-list-wrap">
                                                <ul class="destinations-img-list">
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li>
                                                        <img src="http://placehold.it/52x52" alt="">
                                                        <a href="#" class="img-link">+8</a>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">Question</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="Write your question..."></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">Description</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="Write more details about your question..."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ask-foot-btn">
                            <div class="upload-wrap">
                                <div class="upload-blank">
                                    <i class="trav-camera"></i>
                                    <span>Attach pictures (3 max)</span>
                                </div>
                                <div class="upload-image">
                                    <ul class="image-list">
                                        <li>
                                            <img src="http://placehold.it/30x30" alt="">
                                        </li>
                                        <li class="uploading">
                                            <img src="http://placehold.it/30x30" alt="">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: 75%"
                                                     aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </li>
                                    </ul>
                                    <i class="trav-camera"></i>
                                    <span>2/3</span>
                                </div>
                            </div>
                            <div class="btn-wrap">
                                <button class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                                <button class="btn btn-light-primary btn-disabled">Post Question</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- trip plan selection popup -->
<div class="modal fade modal-child white-style" data-backdrop="false" data-modal-parent="#askingPopup"
     id="tripPlanSelectionPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-740" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-trip-selection-block">
                <div class="post-top-selection">
                    <div class="label-ttl">Select one of your trip plan</div>
                    <div class="search-layer">
                        <div class="search-block">
                            <input class="" id="tripPlanSearch" placeholder="Search in your trip plans" type="text">
                            <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
                        </div>
                    </div>
                </div>
                <div class="post-trip-select-wrap mCustomScrollbar">
                    <div class="post-trip-select-inner">
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>
<script src="{{url('assets2/js/script.js')}}"></script>

</body>

</html>