<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link rel="stylesheet" href="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.css')}}">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - Profile</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">

        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">
            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                @include('site/profile/partials/left_menu')
            </div>

            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">


                    <div class="post-block post-top-bordered">
                        <div class="post-side-top top-arrow">
                            <div class="post-top-txt horizontal">
                                <h3 class="side-ttl">All trip plans <span class="count">{{count($my_plans)}}</span></h3>
                                <button type="button" onclick="location.href='{{route('trip.plan', 0)}}';"
                                        class="btn btn-light-primary btn-bordered" style="margin-left: 20px;">New Trip
                                    Plan
                                </button>

                            </div>
                            <div class="side-right-control">
                                <form action='' method='get'>
                                    <div class="sort-by-select">
                                        <label>Sort by</label>
                                        <div class="sort-select-wrap">
                                            <select class="form-control" id="sort" name='sort'
                                                    onchange='javascript:this.form.submit();'>
                                                <option @if(isset($sort) AND $sort=="Upcoming") selected @endif>
                                                    Upcoming
                                                </option>
                                                <option @if(isset($sort) AND $sort=="Creation Date") selected @endif>
                                                    Creation Date
                                                </option>
                                                <option @if(isset($sort) AND $sort=="Title") selected @endif>Title
                                                </option>
                                            </select>
                                            <i class="trav-caret-down"></i>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="trip-plan-inner">

                            @foreach($all_plans AS $inv)
                                @if($inv->plan)
                                    <div class="trip-plan-row">
                                        <div class="trip-plan-inside-block">
                                            <div class="trip-plan-map-img">
                                                <img src="{{get_plan_map($inv->plan, 140, 150)}}" alt="map-image"
                                                     style="width:140px;height:150px;">

                                                <div class="dropdown">
                                                    <button class="btn btn-light-primary btn-bordered" type="button"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <i class="trav-pencil"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                                                        <a class="dropdown-item"
                                                           href="{{route('trip.plan', $inv->plan->id)}}">
                                                            <div class="drop-txt">
                                                                <p>Edit</p>
                                                            </div>
                                                        </a>
                                                        <a class="dropdown-item delete-trip" href="#" data-toggle="modal" data-target="#deleteTripModal"
                                                           data-id="{{$inv->plan->id}}">
                                                            <div class="drop-txt">
                                                                <p>@lang('buttons.general.crud.delete')</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-plan-inside-block trip-plan-txt">
                                            <div class="trip-plan-txt-inner">
                                                <div class="trip-txt-ttl-layer">
                                                    <h2 class="trip-ttl"><a
                                                                href="{{route('trip.view', $inv->plan->id)}}">{{$inv->plan->title}}</a>
                                                    </h2>
                                                    <p class="trip-date">{{weatherDate($inv->start_date)}}
                                                        to {{weatherDate($inv->end_date)}}</p>
                                                    <p class="trip-date" style='margin:0px'>Invited
                                                        by: {{$inv->plan->author->name}}</p>
                                                </div>
                                                <div class="trip-txt-info">
                                                    <div class="trip-info">
                                                        <div class="icon-wrap">
                                                            <i class="trav-clock-icon"></i>
                                                        </div>
                                                        <div class="trip-info-inner">
                                                            <p><b>{{dateDiffDays($inv->start_date, $inv->end_date)}}</b>
                                                            </p>
                                                            <p>Duration</p>
                                                        </div>
                                                    </div>
                                                    <div class="trip-info">
                                                        <div class="icon-wrap">
                                                            <i class="trav-distance-icon"></i>
                                                        </div>
                                                        <div class="trip-info-inner">
                                                            <p><b>{{$inv->distance}} km</b></p>
                                                            <p>@lang('trip.distance')</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="trip-plan-inside-block">
                                            <div class="dest-trip-plan">
                                                <h3 class="dest-ttl">@lang('trip.destination')
                                                    <span>{{count($inv->cities)}}</span>
                                                </h3>
                                                <ul class="dest-image-list" style="width:170px;">
                                                    @foreach($inv->cities AS $city)
                                                        <li>
                                                            <img src="{{@check_city_photo($city->getMedias[0]->url)}}"
                                                                 alt="photo" style="width:52px;height:52px;">
                                                        </li>
                                                    @endforeach

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                            @endif
                        @endforeach



                        <!--<div class="bottom-load-mark">
                  @lang('other.loading_dots')
                </div>
                -->
                        </div>
                    </div>


                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">


                    <div class="post-block post-side-profile sm-profile">
                        <div class="image-wrap" style="height:125px">
                            <div class="post-image-info">
                                <div class="avatar-layer">
                                    <div class="ava-inner" style="min-width: 65px;">
                                        <img src="{{check_profile_picture($user->profile_picture)}}" alt=""
                                             class="avatar" style="width:58px;height:58px;">
                                        <a href="#" class="edit-ava-link">
                                            <img src="./assets2/image/profile-ava-edit-img.png" alt="">
                                        </a>
                                    </div>
                                    <div class="ava-txt">
                                        <h4 class="ava-name">{{$user->name}}</h4>
                                        <p class="sub-ttl">{{$user->nationality}}</p>
                                    </div>
                                </div>
                                <div class="follow-btn-wrap" id="follow_user_button">

                                </div>
                            </div>
                        </div>
                        <div class="post-profile-info">
                            <ul class="profile-info-list">
                                <li>
                                    <p class="info-count">{{count($user->posts)}}</p>
                                    <p class="info-label">Posts</p>
                                </li>
                                <li>
                                    <p class="info-count">{{$user->followers}}</p>
                                    <p class="info-label">Followers</p>
                                </li>
                                <li>
                                    <p class="info-count">{{$user->following}}</p>
                                    <p class="info-label">Following</p>
                                </li>
                            </ul>
                        </div>
                    </div>


                    <div class="aside-footer">
                        <ul class="aside-foot-menu">
                            <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                            <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                            <li><a href="{{url('/')}}">Advertising</a></li>
                            <li><a href="{{url('/')}}">Cookies</a></li>
                            <li><a href="{{url('/')}}">More</a></li>
                        </ul>
                        <p class="copyright">Travooo © 2017</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modals -->


<!-- trip plan selection popup -->
<div class="modal fade modal-child white-style" data-backdrop="false" data-modal-parent="#askingPopup"
     id="tripPlanSelectionPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-740" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-trip-selection-block">
                <div class="post-top-selection">
                    <div class="label-ttl">Select one of your trip plan</div>
                    <div class="search-layer">
                        <div class="search-block">
                            <input class="" id="tripPlanSearch" placeholder="Search in your trip plans" type="text">
                            <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
                        </div>
                    </div>
                </div>
                <div class="post-trip-select-wrap mCustomScrollbar">
                    <div class="post-trip-select-inner">
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>
<script src="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.js')}}"></script>
<script src="{{url('assets2/js/script.js')}}"></script>

<script>
    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{route('profile.check-follow', $user->id)}}",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    $('#follow_user_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side" id="button_unfollow"><i class="trav-user-plus-icon"></i><span>unfollow</span></button>');
                } else if (res.success == false) {
                    $('#follow_user_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side" id="button_follow"><i class="trav-user-plus-icon"></i><span>@lang('buttons.general.follow')</span></button>');
                }
            });

        $('body').on('click', '#button_follow', function () {
            $.ajax({
                method: "POST",
                url: "{{route('profile.follow', $user->id)}}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#follow_user_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side" id="button_unfollow"><i class="trav-user-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });

        $('body').on('click', '#button_unfollow', function () {
            $.ajax({
                method: "POST",
                url: "{{route('profile.unfolllow', $user->id)}}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#follow_user_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side" id="button_follow"><i class="trav-user-plus-icon"></i><span>@lang('buttons.general.follow')</span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });
    });
</script>

@include('site/layouts/footer')

</body>

</html>