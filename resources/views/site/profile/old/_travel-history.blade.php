<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - Profile</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">

        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">
            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                @include('site/profile/partials/left_menu')
            </div>

            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    @if(count($visited_countries))
                        <div class="post-block post-top-bordered">
                            <div class="post-side-top top-tabs">
                                <div class="post-top-txt">
                                    <h3 class="side-ttl current">Countries <span
                                                class="count">{{count($visited_countries)}}</span></h3>
                                </div>
                                <div class="side-right-control">
                                    <div class="sort-by-select">
                                        <div class="sort-select-wrap">
                                            <select class="form-control" id="">
                                                <option>Privacy</option>
                                                <option>Item</option>
                                                <option>Item2</option>
                                            </select>
                                            <i class="trav-caret-down"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-media-inner">
                                <div class="post-slide-wrap">
                                    <ul id="interestCountries" class="post-slider">
                                        @foreach($visited_countries AS $country)
                                            <li class="interest-card">
                                                <div class="img-wrap">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$country->cover->url}}"
                                                         alt="" style="width:200px;height:250px;">
                                                    <div class="flag-img">
                                                        <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($country->iso_code)}}.png"
                                                             alt="" style="width:48px;height:48px;">
                                                    </div>
                                                </div>
                                                <div class="post-slider-caption">
                                                    <p class="post-card-name">{{$country->transsingle->title}}</p>
                                                    <p class="post-card-placement">
                                                        Country in {{@$country->region->trans[0]->title}}
                                                    </p>
                                                </div>
                                            </li>
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if(count($visited_cities))
                        <div class="post-block post-top-bordered">
                            <div class="post-side-top top-tabs">
                                <div class="post-top-txt">
                                    <h3 class="side-ttl current">Cities <span
                                                class="count">{{count($visited_cities)}}</span></h3>
                                </div>
                                <div class="side-right-control">
                                    <div class="sort-by-select">
                                        <div class="sort-select-wrap">
                                            <select class="form-control" id="">
                                                <option>Privacy</option>
                                                <option>Item</option>
                                                <option>Item2</option>
                                            </select>
                                            <i class="trav-caret-down"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-media-inner">
                                <div class="post-slide-wrap">
                                    <ul id="interestCities" class="post-slider">
                                        @foreach($visited_cities AS $city)
                                            <li class="interest-card">
                                                <div class="img-wrap dropdown-wrapper">
                                                    <img src="http://placehold.it/200x250" alt=""
                                                         style="width:200px;height:250px;">
                                                    <div class="dropdown image-dropdown">
                                                        <button class="btn btn-light-primary btn-bordered" type="button"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                            <i class="trav-pencil"></i>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                                                            <a class="dropdown-item" href="#">
                                                                <div class="drop-txt">
                                                                    <p>Visited</p>
                                                                </div>
                                                            </a>
                                                            <a class="dropdown-item" href="#">
                                                                <div class="drop-txt">
                                                                    <p>Add to Trip Plan</p>
                                                                </div>
                                                            </a>
                                                            <a class="dropdown-item" href="#">
                                                                <div class="drop-txt">
                                                                    <p>Write a Review</p>
                                                                </div>
                                                            </a>
                                                            <a class="dropdown-item" href="#">
                                                                <div class="drop-txt">
                                                                    <p>Hide from Section</p>
                                                                </div>
                                                            </a>
                                                            <a class="dropdown-item" href="#">
                                                                <div class="drop-txt">
                                                                    <p>@lang('buttons.general.delete_dots')</p>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-slider-caption">
                                                    <p class="post-card-name">{{$city->checkin[0]->city->transsingle->title}}</p>
                                                    <p class="post-card-placement">
                                                        City
                                                        in {{@$city->checkin[0]->city->country->transsingle->title}}
                                                    </p>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if(count($visited_places))
                        <div class="post-block post-top-bordered">
                            <div class="post-side-top top-tabs">
                                <div class="post-top-txt">
                                    <h3 class="side-ttl current">@choice('trip.place', 2) <span
                                                class="count">{{count($visited_places)}}</span></h3>
                                </div>
                                <div class="side-right-control">
                                    <div class="sort-by-select">
                                        <div class="sort-select-wrap">
                                            <select class="form-control" id="">
                                                <option>Privacy</option>
                                                <option>Item</option>
                                                <option>Item2</option>
                                            </select>
                                            <i class="trav-caret-down"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-media-inner">
                                <div class="post-slide-wrap">

                                    <ul class="post-slider all-showed">
                                        @foreach($visited_places AS $place)
                                            <li class="interest-card">
                                                <div class="img-wrap">
                                                    <img src="http://placehold.it/200x250" alt=""
                                                         style="width:200px;height:250px;">
                                                </div>
                                                <div class="post-slider-caption">
                                                    <p class="post-card-name">{{@$place->transsingle->title}}</p>
                                                    <p class="post-card-placement">
                                                        Park in {{@$place->city->transsingle->title}}
                                                    </p>
                                                </div>
                                            </li>
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif


                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">

                    <div class="post-block post-side-profile sm-profile">
                        <div class="image-wrap" style="height:125px">
                            <div class="post-image-info">
                                <div class="avatar-layer">
                                    <div class="ava-inner" style="min-width: 65px;">
                                        <img src="{{check_profile_picture($user->profile_picture)}}" alt=""
                                             class="avatar" style="width:58px;height:58px;">
                                        <a href="#" class="edit-ava-link">
                                            <img src="./assets2/image/profile-ava-edit-img.png" alt="">
                                        </a>
                                    </div>
                                    <div class="ava-txt">
                                        <h4 class="ava-name">{{$user->name}}</h4>
                                        <p class="sub-ttl">{{$user->nationality}}</p>
                                    </div>
                                </div>
                                <div class="follow-btn-wrap" id="follow_user_button">

                                </div>
                            </div>
                        </div>
                        <div class="post-profile-info">
                            <ul class="profile-info-list">
                                <li>
                                    <p class="info-count">{{count($user->posts)}}</p>
                                    <p class="info-label">Posts</p>
                                </li>
                                <li>
                                    <p class="info-count">{{$user->followers}}</p>
                                    <p class="info-label">Followers</p>
                                </li>
                                <li>
                                    <p class="info-count">{{$user->following}}</p>
                                    <p class="info-label">Following</p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="post-block post-side-search">
                        <div class="search-wrapper">
                            <input class="" id="" placeholder="@lang('profile.search_your_visited_places')" type="text">
                            <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
                        </div>
                    </div>

                    <div class="post-block post-side-type">
                        <div class="type-label">Type</div>
                        <div class="type-progress-block">
                            <div class="type-line">
                                <div class="label">@choice('trip.place', 2)</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="75"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="count">4</div>
                            </div>
                            <div class="type-line">
                                <div class="label">Cities</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="count">2</div>
                            </div>
                            <div class="type-line">
                                <div class="label">Countries</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="count">1</div>
                            </div>
                            <div class="type-line">
                                <div class="label">Events</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="count">1</div>
                            </div>
                        </div>
                    </div>


                    <div class="aside-footer">
                        <ul class="aside-foot-menu">
                            <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                            <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                            <li><a href="{{url('/')}}">Advertising</a></li>
                            <li><a href="{{url('/')}}">Cookies</a></li>
                            <li><a href="{{url('/')}}">More</a></li>
                        </ul>
                        <p class="copyright">Travooo © 2017</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>
<script src="{{url('assets2/js/script.js')}}"></script>

<script>
    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{route('profile.check-follow', $user->id)}}",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    $('#follow_user_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side" id="button_unfollow"><i class="trav-user-plus-icon"></i><span>unfollow</span></button>');
                } else if (res.success == false) {
                    $('#follow_user_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side" id="button_follow"><i class="trav-user-plus-icon"></i><span>@lang('buttons.general.follow')</span></button>');
                }
            });

        $('body').on('click', '#button_follow', function () {
            $.ajax({
                method: "POST",
                url: "{{route('profile.follow', $user->id)}}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#follow_user_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side" id="button_unfollow"><i class="trav-user-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });

        $('body').on('click', '#button_unfollow', function () {
            $.ajax({
                method: "POST",
                url: "{{route('profile.unfolllow', $user->id)}}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#follow_user_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side" id="button_follow"><i class="trav-user-plus-icon"></i><span>@lang('buttons.general.follow')</span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });
    });
</script>

@include('site/layouts/footer')
</body>

</html>