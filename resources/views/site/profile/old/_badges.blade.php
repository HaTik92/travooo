<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - Profile</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">

        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">
            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                @include('site/profile/partials/left_menu')
            </div>

            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">


                    <div class="post-block post-badge-block">
                        <div class="post-top-image">
                            <img src="http://placehold.it/655x225" alt="image">
                            <div class="badge-label"><span>level up!</span></div>
                        </div>
                        <div class="post-badge-inner">
                            <div class="badge-layer">
                                <h3 class="ttl">Your next Badge</h3>
                                <div class="badge-block">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/66x66" alt="">
                                    </div>
                                    <div class="badge-content">
                                        <div class="badge-txt">
                                            <h5 class="badge-ttl">Badge name</h5>
                                            <div class="info-badge">
                                                <ul class="foot-avatar-list">
                                                    <li><img class="small-ava" src="http://placehold.it/20x20"
                                                             alt="ava"></li><!--
                            -->
                                                    <li><img class="small-ava" src="http://placehold.it/20x20"
                                                             alt="ava"></li><!--
                            -->
                                                    <li><img class="small-ava" src="http://placehold.it/20x20"
                                                             alt="ava"></li>
                                                </ul>
                                                <span>87 users like you have this badge</span>
                                            </div>
                                        </div>
                                        <div class="badge-count">1000 <span>/ 135</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="badge-layer">
                                <div class="badge-top-layer">
                                    <h3 class="ttl-sm">All your badges</h3>
                                    <a href="#" class="more-link"><span>more</span> <i class="fa fa-caret-down"></i></a>
                                </div>
                                <ul class="badges-list">
                                    <li>
                                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                                    </li>
                                </ul>
                                <div class="badge-progress-block">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 75%"
                                             aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <p>135 points to the next badge</p>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">


                    <div class="post-block post-side-profile sm-profile">
                        <div class="image-wrap">
                            <img src="http://placehold.it/385x125" alt="">
                            <div class="post-image-info">
                                <div class="avatar-layer">
                                    <div class="ava-inner">
                                        <img src="http://placehold.it/58x58" alt="" class="avatar">
                                        <a href="#" class="edit-ava-link">
                                            <img src="./assets2/image/profile-ava-edit-img.png" alt="">
                                        </a>
                                    </div>
                                    <div class="ava-txt">
                                        <h4 class="ava-name">Justin baker</h4>
                                        <p class="sub-ttl">United States</p>
                                    </div>
                                </div>
                                <div class="follow-btn-wrap">
                                    <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side">
                                        <i class="trav-user-plus-icon"></i>
                                        <span>@lang('buttons.general.follow')</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="post-profile-info">
                            <ul class="profile-info-list">
                                <li>
                                    <p class="info-count">125</p>
                                    <p class="info-label">Posts</p>
                                </li>
                                <li>
                                    <p class="info-count">58</p>
                                    <p class="info-label">Followers</p>
                                </li>
                                <li>
                                    <p class="info-count">2</p>
                                    <p class="info-label">Following</p>
                                </li>
                            </ul>
                        </div>
                    </div>


                    <div class="post-block post-side-filter">
                        <div class="top-filter-wrap">
                            <div class="img-wrap">
                                <img src="http://placehold.it/54x54" alt="">
                            </div>
                            <div class="top-filter">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="80"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="filter-info">
                                    <span class="count">4,947</span>
                                    <span class="left">343 left</span>
                                </div>
                            </div>
                        </div>
                        <div class="filter-content">
                            <div class="side-filter-row">
                                <div class="label">@lang('comment.comments')</div>
                                <div class="progress-filter">
                                    <div class="progress-count">
                                        <b>163</b>&nbsp;/&nbsp;<span>500</span>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 40%"
                                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-filter-row">
                                <div class="label">Likes</div>
                                <div class="progress-filter">
                                    <div class="progress-count">
                                        <b>120</b>&nbsp;/&nbsp;<span>200</span>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 80%"
                                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-filter-row">
                                <div class="label">Trips</div>
                                <div class="progress-filter">
                                    <div class="progress-count">
                                        <b>120</b>&nbsp;/&nbsp;<span>200</span>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 80%"
                                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-filter-row">
                                <div class="label">@lang('place.reviews')</div>
                                <div class="progress-filter">
                                    <div class="progress-count">
                                        <b>120</b>&nbsp;/&nbsp;<span>200</span>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 80%"
                                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-filter-row">
                                <div class="label">@lang('place.videos')</div>
                                <div class="progress-filter">
                                    <div class="progress-count">
                                        <b>120</b>&nbsp;/&nbsp;<span>200</span>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 80%"
                                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-filter-row">
                                <div class="label">@lang('place.photos')</div>
                                <div class="progress-filter">
                                    <div class="progress-count">
                                        <b>120</b>&nbsp;/&nbsp;<span>200</span>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 80%"
                                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="aside-footer">
                        <ul class="aside-foot-menu">
                            <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                            <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                            <li><a href="{{url('/')}}">Advertising</a></li>
                            <li><a href="{{url('/')}}">Cookies</a></li>
                            <li><a href="{{url('/')}}">More</a></li>
                        </ul>
                        <p class="copyright">Travooo © 2017</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modals -->
<!-- asking popup -->
<div class="modal fade white-style" data-backdrop="false" id="askingPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-750" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-asking-block">
                <div class="post-asking-inner">
                    <div class="avatar-wrap">
                        <img class="ava" src="http://placehold.it/50x50" alt="">
                    </div>
                    <div class="ask-content">
                        <div class="ask-row-inner">
                            <div class="ask-row">
                                <div class="ask-label">Asking</div>
                                <div class="ask-txt">
                                    <div class="radio-wrapper">
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="askingRadios" id="forRecommendation"
                                                   value="forRecommendation" type="radio">
                                            <label for="forRecommendation">For recommendations</label>
                                        </div>
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="askingRadios" id="forTips"
                                                   value="forTips" type="radio">
                                            <label for="forTips">For tips</label>
                                        </div>
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="askingRadios" id="aboutTripPlan"
                                                   value="aboutTripPlan" type="radio">
                                            <label for="aboutTripPlan">About a trip plan</label>
                                        </div>
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="askingRadios" id="generalQuestion"
                                                   value="generalQuestion" type="radio">
                                            <label for="generalQuestion">A general question</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">Topic</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="A topic for your question, For example “Photography, New York”"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">Destionation</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="@lang('discussion.strings.place_city_country')"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">Tips about</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="@lang('discussion.strings.place_city_country')"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">@lang('trip.trip_plan')</div>
                                <div class="ask-txt">
                                    <div class="ask-input-btn-line">
                                        <div class="ask-input-wrap">
                                            <textarea name="" id="" cols="" rows=""
                                                      placeholder="Select one of your trip plans"></textarea>
                                        </div>
                                        <button data-toggle="modal" data-target="#tripPlanSelectionPopup" type="button"
                                                class="btn btn-light-grey btn-bordered">Select
                                        </button>
                                    </div>
                                    <div class="ask-trip-card">
                                        <div class="img-wrap">
                                            <img src="http://placehold.it/137x170" alt="">
                                        </div>
                                        <div class="card-inner">
                                            <div class="card-close">
                                                <i class="fa fa-close"></i>
                                            </div>
                                            <h4 class="card-ttl">Over the Mediterranean Sea</h4>
                                            <div class="card-info-line">
                                                18 to 21 Sep 2017&nbsp;&nbsp;·&nbsp;&nbsp;3 Days&nbsp;&nbsp;·&nbsp;&nbsp;12K
                                                km
                                            </div>
                                            <div class="dest-label">
                                                Destinations
                                                <b>13</b>
                                            </div>
                                            <div class="destinations-img-list-wrap">
                                                <ul class="destinations-img-list">
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li>
                                                        <img src="http://placehold.it/52x52" alt="">
                                                        <a href="#" class="img-link">+8</a>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">Question</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="Write your question..."></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">Description</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="Write more details about your question..."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ask-foot-btn">
                            <div class="upload-wrap">
                                <div class="upload-blank">
                                    <i class="trav-camera"></i>
                                    <span>Attach pictures (3 max)</span>
                                </div>
                                <div class="upload-image">
                                    <ul class="image-list">
                                        <li>
                                            <img src="http://placehold.it/30x30" alt="">
                                        </li>
                                        <li class="uploading">
                                            <img src="http://placehold.it/30x30" alt="">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: 75%"
                                                     aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </li>
                                    </ul>
                                    <i class="trav-camera"></i>
                                    <span>2/3</span>
                                </div>
                            </div>
                            <div class="btn-wrap">
                                <button class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                                <button class="btn btn-light-primary btn-disabled">Post Question</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- trip plan selection popup -->
<div class="modal fade modal-child white-style" data-backdrop="false" data-modal-parent="#askingPopup"
     id="tripPlanSelectionPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-740" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-trip-selection-block">
                <div class="post-top-selection">
                    <div class="label-ttl">Select one of your trip plan</div>
                    <div class="search-layer">
                        <div class="search-block">
                            <input class="" id="tripPlanSearch" placeholder="Search in your trip plans" type="text">
                            <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
                        </div>
                    </div>
                </div>
                <div class="post-trip-select-wrap mCustomScrollbar">
                    <div class="post-trip-select-inner">
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>
<script src="{{url('assets2/js/script.js')}}"></script>

</body>

</html>