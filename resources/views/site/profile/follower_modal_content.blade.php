@if(count($get_followers) > 0)
    @foreach($get_followers as $follower)
    <div class="post-comment-row">
        <div class="post-com-avatar-wraps">
            <img class="follow-avatar" src="{{check_profile_picture(@$follower->follower->profile_picture)}}" alt="">
            &nbsp;&nbsp;&nbsp;<a  href="{{url('profile/'.@$follower->follower->id)}}" class="comment-name profile-follow-link">{{@$follower->follower->name}}</a>
            {!! get_exp_icon(@$follower->follower) !!}
        </div>
        <div class="profile-followes-btn-block f-{{@$follower->follower->id}}-block">
            @if(\App\Models\UsersFollowers\UsersFollowers::where('users_id',  @$follower->follower->id)->where('followers_id', Auth::guard('user')->user()->id)->count()> 0)
                <a type="button" class="btn btn-light-grey m_button_unfollow" followId="{{@$follower->follower->id}}">Unfollow</a>
            @else
                <a type="button" class="btn btn-light-primary m_button_follow" followId="{{@$follower->follower->id}}">Follow</a>
            @endif
        </div>
    </div>
    @endforeach
@endif
