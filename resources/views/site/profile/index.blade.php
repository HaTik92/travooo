@extends('site.profile.template.profile')


@section('content')
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#askingPopup">
        @lang('profile.asking_popup')
    </button>

    <button type="button" class="btn btn-primary" id="profilePhotosTrigger">
        @lang('profile.profile_photos_popup')
    </button>

    <div class="post-block post-create-block" id="createPostBlock">
        <div class="post-create-input">
            <input type="text" id="createPostTxt" placeholder="@lang('profile.write_something')">
        </div>
        <div class="post-create-controls upload-image">
            <div class="create-list-wrap">
                <ul class="image-list">
                    <li>
                        <img src="http://placehold.it/30x30" alt="">
                    </li>
                    <li class="uploading">
                        <img src="http://placehold.it/30x30" alt="">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 75%"
                                 aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </li>
                </ul>
                <ul class="create-link-list">
                    <li>
                        <i class="trav-camera"></i>
                    </li><!--
                    -->
                    <li>
                        <i class="trav-set-location-icon"></i>
                    </li><!--
                    -->
                    <li>
                        <i class="trav-talk-icon"></i>
                    </li>
                </ul>
            </div>
            <div class="btn-wrap">
                <i class="trav-earth"></i>
                <button class="btn btn-light-primary btn-disabled">@lang('buttons.general.post')</button>
            </div>
        </div>
    </div>

    <div class="post-block post-top-bordered" style="display:none">
        <div class="post-side-top top-tabs underline-style">
            <div class="post-top-txt">
                <h3 class="side-ttl current">@lang('profile.followers') <span class="count">83</span></h3>
                <h3 class="side-ttl">@lang('profile.following') <span class="count">2</span></h3>
                <h3 class="side-ttl">@lang('profile.travel_mates') <span class="count">83</span></h3>
                <h3 class="side-ttl">@lang('profile.friends') <span class="count">45</span></h3>
            </div>
        </div>
        <div class="post-people-inner">
            <div class="post-people-block-wrap mCustomScrollbar">
                <div class="people-row">
                    <div class="main-info-layer">
                        <div class="img-wrap">
                            <img class="ava" src="http://placehold.it/50x50" alt="ava">
                        </div>
                        <div class="txt-block">
                            <div class="name">Amy Green</div>
                            <div class="info-line">
                                <div class="info-part">@lang('profile.photographer')</div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrap">
                        <button class="btn btn-light-grey btn-bordered">@lang('profile.follow')</button>
                    </div>
                </div>
                <div class="people-row">
                    <div class="main-info-layer">
                        <div class="img-wrap">
                            <img class="ava" src="http://placehold.it/50x50" alt="ava">
                        </div>
                        <div class="txt-block">
                            <div class="name">Amy Green</div>
                            <div class="info-line">
                                <div class="info-part">@lang('profile.photographer')</div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrap">
                        <button class="btn btn-light-primary btn-bordered">@lang('profile.following')</button>
                    </div>
                </div>
                <div class="people-row">
                    <div class="main-info-layer">
                        <div class="img-wrap">
                            <img class="ava" src="http://placehold.it/50x50" alt="ava">
                        </div>
                        <div class="txt-block">
                            <div class="name">Mark Poe</div>
                            <div class="info-line">
                                <div class="info-part">@lang('profile.photographer')</div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrap">
                        <button class="btn btn-light-grey btn-bordered">@lang('profile.follow')</button>
                    </div>
                </div>
                <div class="people-row">
                    <div class="main-info-layer">
                        <div class="img-wrap">
                            <img class="ava" src="http://placehold.it/50x50" alt="ava">
                        </div>
                        <div class="txt-block">
                            <div class="name">Randall Burgess</div>
                            <div class="info-line">
                                <div class="info-part">@lang('profile.photographer')</div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrap">
                        <button class="btn btn-light-grey btn-bordered">@lang('profile.follow')</button>
                    </div>
                </div>
                <div class="people-row">
                    <div class="main-info-layer">
                        <div class="img-wrap">
                            <img class="ava" src="http://placehold.it/50x50" alt="ava">
                        </div>
                        <div class="txt-block">
                            <div class="name">Andria Hinkle &nbsp;<span
                                        class="name-badge">@lang('profile.friend')</span>&nbsp;&nbsp;<span
                                        class="name-badge">@lang('profile.travel_mate')</span></div>
                            <div class="info-line">
                                <div class="info-part">@lang('profile.surfer')</div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrap">
                        <button class="btn btn-light-grey btn-bordered">@lang('profile.follow')</button>
                    </div>
                </div>
                <div class="people-row">
                    <div class="main-info-layer">
                        <div class="img-wrap">
                            <img class="ava" src="http://placehold.it/50x50" alt="ava">
                        </div>
                        <div class="txt-block">
                            <div class="name">Calvin Ackerman</div>
                            <div class="info-line">
                                <div class="info-part">@lang('profile.photographer')</div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrap">
                        <button class="btn btn-light-primary btn-bordered">@lang('profile.following')</button>
                    </div>
                </div>
                <div class="people-row">
                    <div class="main-info-layer">
                        <div class="img-wrap">
                            <img class="ava" src="http://placehold.it/50x50" alt="ava">
                        </div>
                        <div class="txt-block">
                            <div class="name">Peter Hall</div>
                            <div class="info-line">
                                <div class="info-part">@lang('profile.photographer')</div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrap">
                        <button class="btn btn-light-grey btn-bordered">@lang('buttons.general.crud.delete')</button>
                    </div>
                </div>
                <div class="people-row">
                    <div class="main-info-layer">
                        <div class="img-wrap">
                            <img class="ava" src="http://placehold.it/50x50" alt="ava">
                        </div>
                        <div class="txt-block">
                            <div class="name">Billy Flores</div>
                            <div class="info-line">
                                <div class="info-part">@lang('profile.adventurer')</div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrap">
                        <button class="btn btn-light-grey btn-bordered">@lang('buttons.general.crud.delete')</button>
                    </div>
                </div>
                <div class="people-row">
                    <div class="main-info-layer">
                        <div class="img-wrap">
                            <img class="ava" src="http://placehold.it/50x50" alt="ava">
                        </div>
                        <div class="txt-block">
                            <div class="name">Serafina Ewing</div>
                            <div class="info-line">
                                <div class="info-part">@lang('profile.explorer')</div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrap">
                        <button class="btn btn-light-grey btn-bordered">@lang('buttons.general.crud.delete')</button>
                    </div>
                </div>
                <div class="people-row">
                    <div class="main-info-layer">
                        <div class="img-wrap">
                            <img class="ava" src="http://placehold.it/50x50" alt="ava">
                        </div>
                        <div class="txt-block">
                            <div class="name">David Hurst</div>
                            <div class="info-line">
                                <div class="info-part">@lang('profile.photographer')</div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrap">
                        <button class="btn btn-light-grey btn-bordered">@lang('profile.unfriend')</button>
                    </div>
                </div>
                <div class="people-row">
                    <div class="main-info-layer">
                        <div class="img-wrap">
                            <img class="ava" src="http://placehold.it/50x50" alt="ava">
                        </div>
                        <div class="txt-block">
                            <div class="name">Andria Hinkle</div>
                            <div class="info-line">
                                <div class="info-part">@lang('profile.surfer')</div>
                            </div>
                        </div>
                    </div>
                    <div class="button-wrap">
                        <button class="btn btn-light-grey btn-bordered">@lang('profile.unfriend')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<
    <div class="post-block post-top-bordered" style="display:none;">
        <div class="post-side-top top-tabs underline-style">
            <div class="post-top-txt">
                <h3 class="side-ttl current">@lang('profile.badges')</h3>
                <h3 class="side-ttl">@lang('profile.leaderboard')</h3>
                <h3 class="side-ttl">@lang('profile.points')</h3>
            </div>
        </div>
        <div class="post-badges-inner">
            <div class="badge-list badge-view">
                <div class="badge-block">
                    <div class="badge-left">
                        <div class="badge-main-img">
                            <img src="http://placehold.it/63x63" alt="" class="badge-image">
                        </div>
                        <div class="badge-txt">
                            <div class="badge-ttl">
                                <b>@lang('profile.explorer')</b>
                                <span>@lang('profile.in')</span>
                                <a href="#" class="country-link">Japan</a>
                            </div>
                            <div class="badge-sub">
                                <span>@lang('profile.unlocked') 27 Jan 2018</span>
                            </div>
                        </div>
                    </div>
                    <div class="badge-right">
                        <span class="txt">@lang('profile.count_have_this_badge', ['count' => 258])</span>
                    </div>
                </div>
                <div class="badge-block">
                    <div class="badge-left">
                        <div class="badge-main-img">
                            <img src="http://placehold.it/63x63" alt="" class="badge-image">
                        </div>
                        <div class="badge-txt">
                            <div class="badge-ttl">
                                <b>@lang('profile.reviewer')</b>
                                <span>@lang('profile.in')</span>
                                <a href="#" class="country-link">Japan</a>
                            </div>
                            <div class="badge-sub">
                                <span>@lang('profile.unlocked') 27 Jan 2018</span>
                            </div>
                        </div>
                    </div>
                    <div class="badge-right">
                        <span class="txt">@lang('profile.count_have_this_badge', ['count' => 83])</span>
                    </div>
                </div>
            </div>
            <div class="badge-list leaderboard-view">
                <div class="badge-block">
                    <div class="badge-left">
                        <div class="badge-main-img">
                            <img src="http://placehold.it/63x63" alt="" class="badge-image">
                            <div class="hash-label">#56</div>
                        </div>
                        <div class="badge-txt">
                            <div class="badge-ttl">
                                <b>Christian Patterson</b>
                            </div>
                            <div class="badge-sub">
                                <span>@lang('profile.photographer')</span>
                            </div>
                        </div>
                    </div>
                    <div class="badge-right">
                        <img src="http://placehold.it/51x51" alt="" class="right-image">
                    </div>
                </div>
                <div class="badge-block">
                    <div class="badge-left">
                        <div class="badge-main-img">
                            <img src="http://placehold.it/63x63" alt="" class="badge-image">
                            <div class="hash-label">#1</div>
                        </div>
                        <div class="badge-txt">
                            <div class="badge-ttl">
                                <b>John Anderson</b>
                            </div>
                            <div class="badge-sub">
                                <span>@lang('profile.photographer')</span>
                            </div>
                        </div>
                    </div>
                    <div class="badge-right">
                        <img src="http://placehold.it/51x51" alt="" class="right-image">
                    </div>
                </div>
            </div>
            <div class="badge-list points-view">
                <div class="point-layer">
                    <div class="point-time">
                        <span>54 min ago</span>
                    </div>
                    <div class="point-inner-block">
                        <div class="point-main">
                            <div class="point-ttl">
                                <i class="trav-camera"></i>
                                <span>@lang('profile.you_uploaded_a_photo')</span>
                            </div>
                            <div class="point-txt">
                                <span>@lang('profile.you_just_uploaded_a')</span>&nbsp;
                                <a href="#" class="photo-link">@lang('profile.photo')</a>&nbsp;
                                <span>@lang('profile.related_to')</span>&nbsp;
                                <a href="#" class="place-link">New York City</a>
                            </div>
                        </div>
                        <div class="point-info">
                            <span>+20 @lang('profile.points')</span>
                        </div>
                    </div>
                </div>
                <div class="point-layer">
                    <div class="point-time">
                        <span>1 hour ago</span>
                    </div>
                    <div class="point-inner-block">
                        <div class="point-main">
                            <div class="point-ttl">
                                <i class="trav-talk-icon"></i>
                                <span>@lang('profile.you_commented_on_a_post')</span>
                            </div>
                            <div class="point-txt">
                                <span>@lang('profile.you_just_uploaded_a')</span>&nbsp;
                                <a href="#" class="photo-link">@lang('profile.photo')</a>&nbsp;
                                <span>@lang('profile.related_to')</span>&nbsp;
                                <a href="#" class="place-link">New York City</a>
                            </div>
                        </div>
                        <div class="point-info">
                            <span>+5 @lang('profile.points')</span>
                        </div>
                    </div>
                </div>
                <div class="point-layer">
                    <div class="point-time">
                        <span>3 hours ago</span>
                    </div>
                    <div class="point-inner-block">
                        <div class="point-main">
                            <div class="point-ttl">
                                <i class="trav-camera"></i>
                                <span>@lang('profile.you_uploaded_count_photo', ['count' => 3])</span>
                            </div>
                            <div class="point-txt">
                                <span>@lang('profile.you_just_uploaded_a')</span>&nbsp;
                                <a href="#" class="photo-link">@lang('profile.photo')</a>&nbsp;
                                <span>@lang('profile.related_to')</span>&nbsp;
                                <a href="#" class="place-link">New York City</a>
                            </div>
                        </div>
                        <div class="point-info">
                            <span>@lang('profile.count_points', ['count' => '+60'])</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-load-mark">
                @lang('profile.loading')
            </div>
        </div>
    </div>

    <div class="post-block post-top-bordered" data-content="reviews" style='display:none'>
        <div class="post-side-top top-tabs arrow-style">
            <div class="post-top-txt">
                <h3 class="side-ttl current">@lang('profile.all_reviews') <span class="count">35</span></h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <label>@lang('other.sort_by')</label>
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>@lang('time.date')</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
                <div class="sort-by-select">
                    <label>@lang('profile.score')</label>
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>All</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="review-inner-layer">
            <div class="review-block">
                <div class="review-top">
                    <div class="top-main">
                        <div class="location-icon">
                            <i class="trav-set-location-icon"></i>
                        </div>
                        <div class="review-txt">
                            <h3 class="review-ttl">New York City</h3>
                            <div class="sub-txt">
                                <div class="rate-label">
                                    <b>4.1</b>
                                    <i class="trav-star-icon"></i>
                                </div>&nbsp;
                                <span>@lang('profile.from_count_reviews', ['count' => 38547])</span>
                            </div>
                        </div>
                    </div>
                    <div class="btn-wrap">
                        <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('profile.follow')</button>
                    </div>
                </div>
                <div class="review-image-wrap">
                    <ul class="review-image-list">
                        <li>
                            <img src="http://placehold.it/200x200" alt="">
                        </li>
                        <li>
                            <img src="http://placehold.it/200x200" alt="">
                        </li>
                        <li>
                            <img src="http://placehold.it/200x200" alt="">
                            <a class="cover-link" href="#">
                                <span><b>24K</b> @lang('profile.more_photos')</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="review-foot">
                    <div class="ava-wrap">
                        <img src="http://placehold.it/42x42" alt="" class="ava-image">
                    </div>
                    <div class="review-content">
                        <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer leo neque
                            pulvinar ut neque eu, laoreet efficitur tellus etiam aliquam lacinia
                            est”</p>
                        <div class="comment-bottom-info">
                            <div class="com-star-block">
                                <span class="rate-txt">4.0</span>
                                <ul class="com-star-list">
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li class="empty"><i class="trav-star-icon"></i></li>
                                </ul>
                            </div>
                            <div class="com-time">Sunday 24 Sep 2017</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="review-block">
                <div class="review-top">
                    <div class="top-main">
                        <div class="location-icon">
                            <i class="trav-set-location-icon"></i>
                        </div>
                        <div class="review-txt">
                            <h3 class="review-ttl">Paris</h3>
                            <div class="sub-txt">
                                <div class="rate-label">
                                    <b>4.8</b>
                                    <i class="trav-star-icon"></i>
                                </div>&nbsp;
                                <span>@lang('profile.from_count_reviews', ['count' => 6867])</span>
                            </div>
                        </div>
                    </div>
                    <div class="btn-wrap">
                        <button type="button" class="btn btn-light-primary btn-bordered">@lang('profile.following')
                        </button>
                    </div>
                </div>
                <div class="review-image-wrap">
                    <ul class="review-image-list">
                        <li>
                            <img src="http://placehold.it/200x200" alt="">
                        </li>
                        <li>
                            <img src="http://placehold.it/200x200" alt="">
                        </li>
                        <li>
                            <img src="http://placehold.it/200x200" alt="">
                            <a href="#" class="cover-link">
                                <span><b>54K</b> @lang('profile.more_photos')</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="review-foot">
                    <div class="ava-wrap">
                        <img src="http://placehold.it/42x42" alt="" class="ava-image">
                    </div>
                    <div class="review-content">
                        <p>“Fusce consequat, diam a pretium aliquam, odio elit faucibus massa, a egestas
                            lorem tortor ut tortor. Fusce quis odio mattis, venenatis...” <a href="#"
                                                                                             class="more-link">@lang('buttons.general.more')</a>
                        </p>
                        <div class="comment-bottom-info">
                            <div class="com-star-block">
                                <span class="rate-txt">5.0</span>
                                <ul class="com-star-list">
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                </ul>
                            </div>
                            <div class="com-time">Sunday 24 Sep 2017</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="review-block">
                <div class="review-top">
                    <div class="top-main">
                        <div class="location-icon">
                            <i class="trav-set-location-icon"></i>
                        </div>
                        <div class="review-txt">
                            <h3 class="review-ttl">Taj Mahal</h3>
                            <div class="sub-txt">
                                <div class="rate-label">
                                    <b>4.7</b>
                                    <i class="trav-star-icon"></i>
                                </div>&nbsp;
                                <span>@lang('profile.from_count_reviews', ['count' => 4723])</span>
                            </div>
                        </div>
                    </div>
                    <div class="btn-wrap">
                        <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('profile.follow')</button>
                    </div>
                </div>
                <div class="review-image-wrap">
                    <ul class="review-image-list">
                        <li>
                            <img src="http://placehold.it/200x200" alt="">
                        </li>
                        <li>
                            <img src="http://placehold.it/200x200" alt="">
                        </li>
                        <li>
                            <img src="http://placehold.it/200x200" alt="">
                            <a class="cover-link" href="#">
                                <span><b>982K</b> @lang('profile.more_photos')</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="review-foot">
                    <div class="ava-wrap">
                        <img src="http://placehold.it/42x42" alt="" class="ava-image">
                    </div>
                    <div class="review-content">
                        <p>“Dolor sit amet, consectetur adipiscing elit. Integer leo neque pulvinar ut
                            neque eu, laoreet efficitur tellus etiam aliquam lacinia est”</p>
                        <div class="comment-bottom-info">
                            <div class="com-star-block">
                                <span class="rate-txt">5.0</span>
                                <ul class="com-star-list">
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                </ul>
                            </div>
                            <div class="com-time">Sunday 24 Sep 2017</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bottom-load-mark">
                @lang('profile.loading')
            </div>

<
        </div>
    </div>

    <div class="post-block post-top-bordered" style="display:none;">
        <div class="post-side-top top-tabs">
            <div class="post-top-txt">
                <h3 class="side-ttl current">@lang('profile.countries') <span class="count">8</span></h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>@lang('profile.privacy')</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-media-inner">
            <div class="post-slide-wrap">
                <ul id="interestCountries" class="post-slider">
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Barbados</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'Carribian'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Germany</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'Europe'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Australia</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'Oceania'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">India</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'South Asia'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Scotland</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'United Kingdom'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Canada</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'North America'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Uganda</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'East Africa'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Bolivia</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'South America'])
                            </p>
                        </div>
                    </li>
                </ul>

                <ul class="post-slider all-showed">
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Barbados</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'Carribian'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Germany</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'Europe'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Australia</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'Oceania'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">India</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'South Asia'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Scotland</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'United Kingdom'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Canada</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'North America'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Uganda</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'East Africa'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="flag-img">
                                <img src="http://placehold.it/48x48/fff" alt="">
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Bolivia</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'South America'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card find-more-wrap">
                        <a href="#" class="find-more-link">
                            <i class="trav-search-icon"></i>
                            <span>@lang('profile.find_more_events')</span>
                        </a>
                    </li>
                </ul>
                <div class="profile-slider-foot">
                    <a href="#" class="show-all-link">@lang('profile.show_all')</a>
                </div>
            </div>
        </div>
    </div>

<   <div class="post-block post-top-bordered" style="display:none;">
        <div class="post-side-top top-tabs">
            <div class="post-top-txt">
                <h3 class="side-ttl current">@lang('profile.cities') <span class="count">4</span></h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>@lang('profile.privacy')</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-media-inner">
            <div class="post-slide-wrap">
                <ul id="interestCities" class="post-slider">
                    <li class="interest-card">
                        <div class="img-wrap dropdown-wrapper">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="dropdown image-dropdown">
                                <button class="btn btn-light-primary btn-bordered" type="button"
                                        data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    <i class="trav-pencil"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                                    <a class="dropdown-item" href="#">
                                        <div class="drop-txt">
                                            <p>@lang('profile.visited')</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <div class="drop-txt">
                                            <p>@lang('profile.add_to_trip_plan')</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <div class="drop-txt">
                                            <p>@lang('profile.write_a_review')</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <div class="drop-txt">
                                            <p>@lang('profile.hide_from_section')</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <div class="drop-txt">
                                            <p>@lang('profile.delete')</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">New York city</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'United States'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap dropdown-wrapper">
                            <img src="http://placehold.it/200x250" alt="">
                            <div class="dropdown image-dropdown">
                                <button class="btn btn-light-primary btn-bordered" type="button"
                                        data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    <i class="trav-pencil"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                                    <a class="dropdown-item" href="#">
                                        <div class="drop-txt">
                                            <p>@lang('profile.visited')</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <div class="drop-txt">
                                            <p>@lang('profile.add_to_trip_plan')</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <div class="drop-txt">
                                            <p>@lang('profile.write_a_review')</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <div class="drop-txt">
                                            <p>@lang('profile.hide_from_section')</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <div class="drop-txt">
                                            <p>@lang('profile.delete')</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Paris</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'France'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Marrakech</p>
                            <p class="post-card-placement">
                                @lang('profile.city_in') Morocco
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">India</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'South Asia'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Scotland</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'United Kingdom'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Canada</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'North America'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Uganda</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'East Africa'])
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Bolivia</p>
                            <p class="post-card-placement">
                                @lang('region.country_in_name', ['name' => 'South America'])
                            </p>
                        </div>
                    </li>
                </ul>

                <div class="profile-slider-foot">
                    <a href="#" class="show-all-link">@lang('profile.show_all')</a>
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-top-bordered" style="display:none;">
        <div class="post-side-top top-tabs">
            <div class="post-top-txt">
                <h3 class="side-ttl current">@lang('profile.places') <span class="count">3</span></h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>@lang('profile.privacy')</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-media-inner">
            <div class="post-slide-wrap">

                <ul class="post-slider all-showed">
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Disneyland</p>
                            <p class="post-card-placement">
                                @lang('profile.park_in') California
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Mount everest</p>
                            <p class="post-card-placement">
                                @lang('profile.mountain_in') Asia
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">@lang('profile.hotel') del Coronado</p>
                            <p class="post-card-placement">
                                @lang('profile.hotel_in') California
                            </p>
                        </div>
                    </li>
                    <li class="interest-card find-more-wrap" style="display:none;">
                        <a href="#" class="find-more-link">
                            <i class="trav-search-icon"></i>
                            <span>@lang('profile.find_more_events')</span>
                        </a>
                    </li>
                </ul>
                <div class="profile-slider-foot">
                    <a href="#" class="show-all-link">@lang('profile.show_all')</a>
                </div>
            </div>
        </div>
    </div>


    <div class="post-block post-top-bordered" data-content='events' style="display:none">
        <div class="post-side-top top-tabs">
            <div class="post-top-txt">
                <h3 class="side-ttl current">@lang('profile.events') <span class="count">2</span></h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>@lang('profile.privacy')</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-media-inner">
            <div class="post-slide-wrap">

                <ul class="post-slider all-showed">
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Super bowl</p>
                            <p class="post-card-placement">
                                @lang('profile.event_in') United States
                            </p>
                        </div>
                    </li>
                    <li class="interest-card">
                        <div class="img-wrap">
                            <img src="http://placehold.it/200x250" alt="">
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">Paris 2017 new year event</p>
                            <p class="post-card-placement">
                                @lang('profile.event_in') Paris
                            </p>
                        </div>
                    </li>
                    <li class="interest-card find-more-wrap">
                        <a href="#" class="find-more-link">
                            <i class="trav-search-icon"></i>
                            <span>@lang('profile.find_more_events')</span>
                        </a>
                    </li>
                </ul>
                <div class="profile-slider-foot">
                    <a href="#" class="show-all-link">@lang('profile.show_all')</a>
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-top-bordered" style="display:none;" data-content="videos">
        <div class="post-side-top top-tabs arrow-style">
            <div class="post-top-txt">
                <h3 class="side-ttl current">@lang('profile.all_videos') <span class="count">6</span></h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <label>@lang('other.sort_by')</label>
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>@lang('time.date')</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-media-inner">
            <div class="post-media-list">
                <div class="media-wrap">
                    <div class="media-inner">
                        <img src="http://placehold.it/302x225" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <b>How to pick the Perfect campsite</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>6</b>
                                    <span>@lang('profile.reactions')</span>
                                </div>
                                <div class="foot-layer">
                                    <b>24</b>
                                    <span>@lang('profile.comments')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media-wrap">
                    <div class="media-inner">
                        <img src="http://placehold.it/302x225" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <b>How to pick the Perfect campsite</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>6</b>
                                    <span>@lang('profile.reactions')</span>
                                </div>
                                <div class="foot-layer">
                                    <b>24</b>
                                    <span>@lang('profile.comments')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media-wrap">
                    <div class="media-inner">
                        <img src="http://placehold.it/302x225" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <b>How to pick the Perfect campsite</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>6</b>
                                    <span>@lang('profile.reactions')</span>
                                </div>
                                <div class="foot-layer">
                                    <b>24</b>
                                    <span>@lang('profile.comments')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media-wrap">
                    <div class="media-inner">
                        <img src="http://placehold.it/302x225" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <b>How to pick the Perfect campsite</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>6</b>
                                    <span>@lang('profile.reactions')</span>
                                </div>
                                <div class="foot-layer">
                                    <b>24</b>
                                    <span>@lang('profile.comments')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-top-bordered" style="display:none;">
        <div class="post-side-top top-tabs arrow-style">
            <div class="post-top-txt">
                <h3 class="side-ttl current">@lang('profile.all_photos') <span class="count">987</span></h3>
                <h3 class="side-ttl">@lang('profile.albums') <span class="count">3</span></h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <label>@lang('other.sort_by')</label>
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>@lang('profile.location')</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-media-inner">
            <div class="post-media-list">
                <div class="media-wrap">
                    <div class="media-inner">
                        <img src="http://placehold.it/198x200" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <span>@lang('chat.from') </span>
                                <b>Span trip album</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>9</b>
                                    <span>@lang('profile.reactions')</span>
                                </div>
                                <div class="foot-layer">
                                    <b>24</b>
                                    <span>@lang('profile.comments')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media-wrap">
                    <div class="media-inner">
                        <img src="http://placehold.it/198x200" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <span>@lang('chat.from') </span>
                                <b>Span trip album</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>9</b>
                                    <span>@lang('profile.reactions')</span>
                                </div>
                                <div class="foot-layer">
                                    <b>24</b>
                                    <span>@lang('profile.comments')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media-wrap">
                    <div class="media-inner">
                        <img src="http://placehold.it/198x200" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <span>@lang('chat.from') </span>
                                <b>Span trip album</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>9</b>
                                    <span>@lang('profile.reactions')</span>
                                </div>
                                <div class="foot-layer">
                                    <b>24</b>
                                    <span>@lang('profile.comments')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media-wrap">
                    <div class="media-inner">
                        <img src="http://placehold.it/198x200" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <span>@lang('chat.from') </span>
                                <b>Span trip album</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>9</b>
                                    <span>@lang('profile.reactions')</span>
                                </div>
                                <div class="foot-layer">
                                    <b>24</b>
                                    <span>@lang('profile.comments')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media-wrap">
                    <div class="media-inner">
                        <img src="http://placehold.it/198x200" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <span>@lang('chat.from') </span>
                                <b>Span trip album</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>9</b>
                                    <span>@lang('profile.reactions')</span>
                                </div>
                                <div class="foot-layer">
                                    <b>24</b>
                                    <span>@lang('profile.comments')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media-wrap">
                    <div class="media-inner">
                        <img src="http://placehold.it/198x200" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <span>@lang('chat.from') </span>
                                <b>Span trip album</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>9</b>
                                    <span>@lang('profile.reactions')</span>
                                </div>
                                <div class="foot-layer">
                                    <b>24</b>
                                    <span>@lang('profile.comments')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media-wrap album-block create-item">
                    <div class="media-inner">
                        <a href="#" class="create-item-link">
                            <i class="trav-create-album-icon"></i>
                            <span>@lang('profile.create_album')</span>
                        </a>
                    </div>
                </div>
                <div class="media-wrap album-block">
                    <div class="media-inner">
                        <img src="http://placehold.it/198x200" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <b>The amazing rome</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>28</b>
                                    <span>@lang('profile.photos')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media-wrap album-block">
                    <div class="media-inner">
                        <img src="http://placehold.it/198x200" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <b>@lang('time.count_days', ['count' => 15]) in india</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>64</b>
                                    <span>@lang('profile.photos')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media-wrap album-block">
                    <div class="media-inner">
                        <img src="http://placehold.it/198x200" alt="">
                        <div class="media-cover-txt">
                            <div class="media-info">
                                <b>Spain trip</b>
                            </div>
                            <div class="media-info media-foot">
                                <div class="foot-layer">
                                    <b>14</b>
                                    <span>@lang('profile.photos')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-load-mark">
                @lang('profile.loading')
            </div>
        </div>
    </div>

    <div class="post-block post-top-bordered" data-content="favourites" style='display:none'>
        <div class="post-side-top top-arrow">
            <div class="post-top-txt horizontal">
                <h3 class="side-ttl">@lang('profile.all_favorites') <span class="count">27</span></h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <label>@lang('other.sort_by')</label>
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>@lang('time.date')</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="activity-inner">
            <div class="activ-row">
                <div class="activ-inside-block">
                    <a href="#" class="close-btn">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">New York City</a></h2>
                            <div class="txt-block">
                                <p>@lang('profile.city_in') <a href="#" class="link-txt">United State of America</a></p>
                            </div>
                            <div class="btn-block">
                                <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('profile.follow')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activ-after-txt">
                    <p>
                        <img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                        @lang('profile.saved_from')&nbsp;
                        <a href="#" class="link-txt">John Post</a>&nbsp;
                        2 days ago
                    </p>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">Diana Walker</a>
                                &nbsp;<span>@lang('profile.photo')</span></h2>
                            <div class="txt-block">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit curabitur
                                    enean bibendum venenatis pharetra duis et risus nec lorem...</p>
                            </div>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                            class="trav-view-plan-icon"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activ-after-txt">
                    <p>
                        <img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                        @lang('profile.saved_from')&nbsp;
                        <a href="#" class="link-txt">Diana Walker Post</a>&nbsp;
                        3 days ago
                    </p>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><img src="http://placehold.it/36x20" class="small-flag"
                                                     alt="flag">&nbsp; <a href="#" class="link-txt">Unites
                                    States of America</a></h2>
                            <div class="txt-block">
                                <p>@lang('region.country_in_name', ['name' => 'North America']) </p>
                            </div>
                            <div class="btn-block">
                                <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('profile.follow')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">Diana Walker</a>
                                &nbsp;<span>@lang('profile.video')</span></h2>
                            <div class="txt-block">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam,
                                    quaerat? Culpa officia expedita eaque nam?</p>
                            </div>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                            class="trav-view-plan-icon"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activ-after-txt">
                    <p><img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                        @lang('profile.saved_from')&nbsp;
                        <a href="#" class="link-txt">Robe Walker Post</a>&nbsp;
                        3 days ago</p>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">Independence day</a></h2>
                            <div class="txt-block">
                                <p>@lang('profile.national_holiday_in') <a href="#" class="link-txt">United State of
                                        America</a></p>
                            </div>
                            <div class="btn-block">
                                <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('profile.follow')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="photos-gallery">
                        <div class="img-wrap">
                            <img src="http://placehold.it/120x59" alt="image">
                        </div>
                        <div class="img-wrap">
                            <img src="http://placehold.it/59x59" alt="image">
                        </div>
                        <div class="img-wrap">
                            <img src="http://placehold.it/59x59" alt="image">
                            <a href="#" class="cover-link">+8</a>
                        </div>
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">Jane Grogan</a>
                                &nbsp;<span>@lang('profile.photos')</span></h2>
                            <div class="txt-block">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                    curabitur.</p>
                            </div>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                            class="trav-view-plan-icon"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activ-after-txt">
                    <p>
                        <img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                        @lang('profile.saved_from')&nbsp;
                        <a href="#" class="link-txt">Jane Grogan Post</a>&nbsp;
                        1 month ago
                    </p>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">4 Days in USA</a>
                                &nbsp;<span>@lang('trip.trip_plan')</span></h2>
                            <div class="txt-block">
                                <p>@lang('profile.create_by') <a href="#" class="link-txt">Stephen Bugno</a></p>
                            </div>
                            <div class="btn-block">
                                <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('profile.view_plan')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">Martin Kennedy</a></h2>
                            <div class="txt-block">
                                <p>“Aliquam tempor, tellus ut condimentum posuere arcu velit vulputate
                                    nec hendrerit nunc ex et elit sed maximus orci lorem iaculis...” <a
                                            href="#" class="link-txt">@lang('buttons.general.more')</a></p>
                            </div>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                            class="trav-view-plan-icon"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activ-after-txt">
                    <p><img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                        @lang('profile.saved_from')&nbsp;
                        <a href="#" class="link-txt">Diana Walker Post</a>&nbsp;
                        3 days ago</p>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">Lisa
                                    Martinez</a> @lang('profile.tip_about')
                                <a href="#" class="link-txt">Haneda Airport</a></h2>
                            <div class="txt-block">
                                <p>“Nullam accumsan, eros in consequat imperdiet, lacus mi iaculis
                                    viverra est ante et eros fusce accumsan sed scelerisque...” <a
                                            href="#" class="link-txt">@lang('buttons.general.more')</a></p>
                            </div>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                            class="trav-view-plan-icon"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activ-after-txt">
                    <p>@lang('profile.saved_from_a')&nbsp;
                        <a href="#" class="link-txt">@lang('trip.trip_plan')</a>&nbsp;
                        @lang('chat.by')&nbsp;
                        <a href="#" class="link-txt">Suzanne</a>&nbsp;
                        3 months ago</p>
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-top-bordered" data-content="visited" style="display:none;">
        <div class="post-side-top top-arrow">
            <div class="post-top-txt horizontal">
                <h3 class="side-ttl">@lang('profile.all_visited') <span class="count">8</span></h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <label>@lang('other.sort_by')</label>
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>@lang('time.date')</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="trip-plan-inner">
            <div class="trip-plan-row">
                <div class="trip-plan-inside-block">
                    <div class="trip-plan-map-img">
                        <img src="http://placehold.it/123x123" alt="map-image">
                    </div>
                </div>
                <div class="trip-plan-inside-block trip-plan-txt">
                    <div class="trip-plan-txt-inner">
                        <div class="trip-txt-ttl-layer">
                            <h2 class="trip-ttl">New york city</h2>
                            <p class="trip-txt">@lang('profile.city_in') <a href="#" class="link-txt">United State of
                                    America</a></p>
                            <p class="trip-date">@lang('profile.visited') 6 days ago</p>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered">@lang('profile.follow')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inside-block">
                    <div class="dest-trip-plan sm-image">
                        <h3 class="dest-ttl"><span>@lang('profile.your_photos')</span></h3>
                        <ul class="dest-image-list">
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="trip-plan-row">
                <div class="trip-plan-inside-block">
                    <div class="trip-plan-map-img">
                        <img src="http://placehold.it/123x123" alt="map-image">
                    </div>
                </div>
                <div class="trip-plan-inside-block trip-plan-txt">
                    <div class="trip-plan-txt-inner">
                        <div class="trip-txt-ttl-layer">
                            <h2 class="trip-ttl">Central park</h2>
                            <p class="trip-txt">@lang('profile.park_in') <a href="#" class="link-txt">New York City</a>
                            </p>
                            <p class="trip-date">@lang('profile.visited') 7 days ago</p>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered">@lang('profile.follow')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inside-block">
                    <div class="dest-trip-plan sm-image">
                        <h3 class="dest-ttl"><span>@lang('profile.your_photos')</span></h3>
                        <ul class="dest-image-list">
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li class="more-photo-link">
                                <img src="http://placehold.it/44x44" alt="photo">
                                <span class="cover">+8</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="trip-plan-row">
                <div class="trip-plan-inside-block">
                    <div class="trip-plan-map-img">
                        <img src="http://placehold.it/123x123" alt="map-image">
                    </div>
                </div>
                <div class="trip-plan-inside-block trip-plan-txt">
                    <div class="trip-plan-txt-inner">
                        <div class="trip-txt-ttl-layer">
                            <h2 class="trip-ttl"><img src="http://placehold.it/28x20" class="small-flag"
                                                      alt="flag"> Morocco</h2>
                            <p class="trip-txt">@lang('profile.country_in') <a href="#" class="link-txt">North
                                    Africa</a>
                            </p>
                            <p class="trip-date">@lang('profile.visited') 2 months ago</p>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-primary btn-bordered">
                                    @lang('profile.following')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inside-block">
                    <div class="dest-trip-plan sm-image">
                        <h3 class="dest-ttl"><span>@lang('profile.your_photos')</span></h3>
                        <ul class="dest-image-list">
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="trip-plan-row">
                <div class="trip-plan-inside-block">
                    <div class="trip-plan-map-img">
                        <img src="http://placehold.it/123x123" alt="map-image">
                    </div>
                </div>
                <div class="trip-plan-inside-block trip-plan-txt">
                    <div class="trip-plan-txt-inner">
                        <div class="trip-txt-ttl-layer">
                            <h2 class="trip-ttl">Yellowstone national park</h2>
                            <p class="trip-txt">@lang('profile.park_in') <a href="#" class="link-txt">United States of
                                    America</a></p>
                            <p class="trip-date">@lang('profile.visited') 3 months ago</p>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered">@lang('profile.follow')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inside-block">
                    <div class="dest-trip-plan sm-image">
                        <h3 class="dest-ttl"><span>@lang('profile.your_photos')</span></h3>
                        <ul class="dest-image-list">
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/44x44" alt="photo">
                            </li>
                            <li class="more-photo-link">
                                <img src="http://placehold.it/44x44" alt="photo">
                                <span class="cover">+2</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="bottom-load-mark">
                @lang('profile.loading')
            </div>
        </div>
    </div>

    <div class="post-block post-map-wrapper" data-content='my_map' style='display:none'>
        <div class="post-map-block">
            <div class="post-map-inner">
                <img src="http://placehold.it/595x770" alt="map">
                <div class="dest-img-block" style="top:30px;left:100px;">
                    <img class="dest-img" src="http://placehold.it/30x30" alt="">
                    <div class="icon-wrap">
                        <ul class="icon-list">
                            <li><i class="fa fa-camera"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="dest-img-block" style="top:90px;left:100px;">
                    <img class="dest-img" src="http://placehold.it/30x30" alt="">
                    <div class="icon-wrap">
<                       <ul class="icon-list">
                            <li>
                                <img src="./assets2/image/reaction-icon-smile-only.png" alt="smile">
                            </li>
                            <li><i class="trav-comment-icon"></i></li>
                        </ul>
>                   </div>
                </div>
                <div class="full-screen-btn">
                    <i class="trav-full-fullscreen-icon"></i>
                    <span>@lang('profile.full_screen')</span>
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-profile-about-block" data-content='about'>
        <div class="post-side-top">
            <h3 class="side-ttl">Justin baker</h3>
        </div>
        <div class="post-content-inner">
            <div class="post-form-wrapper">
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.about')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod,
                                mauris sit amet mollis luctus, mauris risus cursus ex, quis fermentum
                                felis urna ut est.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.links')</label>
                    <div class="col-sm-9">
                        <div class="post-profile-foot-inner">
                            <a href="#" class="profile-website-link">
                          <span class="round-icon">
                            <i class="trav-link"></i>
                          </span>
                                <span>@lang('profile.website')</span>
                            </a>
                            <ul class="profile-social-list">
                                <li>
                                    <a href="#" class="facebook">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="twitter">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="instagram">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.from_where')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <a href="#" class="form-link">Saudi Arabia</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.age')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <p>29</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.profession')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <p>Web Developer</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.interest')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <p>Hiking, Group Travel, Swiming</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.expert_in')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <a href="#" class="form-link">France</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.member_since')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <p>2017</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.visited')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <a href="#" class="form-link">Tokyo</a>,&nbsp;
                            <a href="#" class="form-link">Nagoya</a>,&nbsp;
                            <a href="#" class="form-link">Riyadh</a>,&nbsp;
                            <a href="#" class="form-link">Albania</a>&nbsp;
                            <a href="#" class="form-link">+3 More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-top-bordered" data-content="trip_plans" style='display:none'>
        <div class="post-side-top top-arrow">
            <div class="post-top-txt horizontal">
                <h3 class="side-ttl">@lang('profile.all_trip_plans') <span class="count">5</span></h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <label>@lang('other.sort_by')</label>
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>@lang('time.date')</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="trip-plan-inner">
            <div class="trip-plan-row">
                <div class="trip-plan-inside-block">
                    <div class="trip-plan-map-img">
                        <img src="http://placehold.it/140x150" alt="map-image">

                        <div class="dropdown">
                            <button class="btn btn-light-primary btn-bordered" type="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="trav-pencil"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                                <a class="dropdown-item" href="#">
                                    <div class="drop-txt">
                                        <p>@lang('profile.edit')</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <div class="drop-txt">
                                        <p>@lang('buttons.general.crud.delete')</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inside-block trip-plan-txt">
                    <div class="trip-plan-txt-inner">
                        <div class="trip-txt-ttl-layer">
                            <h2 class="trip-ttl">New york city the right way</h2>
                            <p class="trip-date">18 to 21 Sep 2017</p>
                        </div>
                        <div class="trip-txt-info">
                            <div class="trip-info">
                                <div class="icon-wrap">
                                    <i class="trav-clock-icon"></i>
                                </div>
                                <div class="trip-info-inner">
                                    <p><b>@choice('time.count_day', 4, ['count' => 4])</b></p>
                                    <p>@lang('book.duration')</p>
                                </div>
                            </div>
                            <div class="trip-info">
                                <div class="icon-wrap">
                                    <i class="trav-distance-icon"></i>
                                </div>
                                <div class="trip-info-inner">
                                    <p><b>237 km</b></p>
                                    <p>@lang('profile.distance')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inside-block">
                    <div class="dest-trip-plan">
                        <h3 class="dest-ttl">@lang('profile.destination') <span>6</span></h3>
                        <ul class="dest-image-list">
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="trip-plan-row">
                <div class="trip-plan-inside-block">
                    <div class="trip-plan-map-img">
                        <img src="http://placehold.it/140x150" alt="map-image">

                        <div class="dropdown">
                            <button class="btn btn-light-primary btn-bordered" type="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="trav-pencil"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                                <a class="dropdown-item" href="#">
                                    <div class="drop-txt">
                                        <p>@lang('profile.edit')</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <div class="drop-txt">
                                        <p>@lang('profile.delete')</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inside-block trip-plan-txt">
                    <div class="trip-plan-txt-inner">
                        <div class="trip-txt-ttl-layer">
                            <h2 class="trip-ttl">Over the mediterranean sea</h2>
                            <p class="trip-date">18 to 21 Sep 2017</p>
                        </div>
                        <div class="trip-txt-info">
                            <div class="trip-info">
                                <div class="icon-wrap">
                                    <i class="trav-clock-icon"></i>
                                </div>
                                <div class="trip-info-inner">
                                    <p><b>@choice('time.count_day', 16, ['count' => 16])</b></p>
                                    <p>@lang('book.duration')</p>
                                </div>
                            </div>
                            <div class="trip-info">
                                <div class="icon-wrap">
                                    <i class="trav-distance-icon"></i>
                                </div>
                                <div class="trip-info-inner">
                                    <p><b>16K km</b></p>
                                    <p>@lang('profile.distance')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inside-block">
                    <div class="dest-trip-plan">
                        <h3 class="dest-ttl">@lang('profile.destination') <span>13</span></h3>
                        <ul class="dest-image-list">
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li class="more-photo-link">
                                <img src="http://placehold.it/52x52" alt="photo">
                                <span class="cover">+8</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="trip-plan-row">
                <div class="trip-plan-inside-block">
                    <div class="trip-plan-map-img">
                        <img src="http://placehold.it/140x150" alt="map-image">

                        <div class="dropdown">
                            <button class="btn btn-light-primary btn-bordered" type="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="trav-pencil"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                                <a class="dropdown-item" href="#">
                                    <div class="drop-txt">
                                        <p>@lang('profile.edit')</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <div class="drop-txt">
                                        <p>@lang('buttons.general.crud.delete')</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inside-block trip-plan-txt">
                    <div class="trip-plan-txt-inner">
                        <div class="trip-txt-ttl-layer">
                            <h2 class="trip-ttl">New york city the right way</h2>
                            <p class="trip-date">18 to 21 Sep 2017</p>
                        </div>
                        <div class="trip-txt-info">
                            <div class="trip-info">
                                <div class="icon-wrap">
                                    <i class="trav-clock-icon"></i>
                                </div>
                                <div class="trip-info-inner">
                                    <p><b>@choice('time.count_day', 4, ['count' => 4])</b></p>
                                    <p>@lang('book.duration')</p>
                                </div>
                            </div>
                            <div class="trip-info">
                                <div class="icon-wrap">
                                    <i class="trav-distance-icon"></i>
                                </div>
                                <div class="trip-info-inner">
                                    <p><b>237 km</b></p>
                                    <p>@lang('profile.distance')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inside-block">
                    <div class="dest-trip-plan">
                        <h3 class="dest-ttl">@lang('profile.destination') <span>6</span></h3>
                        <ul class="dest-image-list">
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="trip-plan-row">
                <div class="trip-plan-inside-block">
                    <div class="trip-plan-map-img">
                        <img src="http://placehold.it/140x150" alt="map-image">

                        <div class="dropdown">
                            <button class="btn btn-light-primary btn-bordered" type="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="trav-pencil"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                                <a class="dropdown-item" href="#">
                                    <div class="drop-txt">
                                        <p>@lang('profile.edit')</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <div class="drop-txt">
                                        <p>@lang('profile.delete')</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inside-block trip-plan-txt">
                    <div class="trip-plan-txt-inner">
                        <div class="trip-txt-ttl-layer">
                            <h2 class="trip-ttl">New york city the right way</h2>
                            <p class="trip-date">18 to 21 Sep 2017</p>
                        </div>
                        <div class="trip-txt-info">
                            <div class="trip-info">
                                <div class="icon-wrap">
                                    <i class="trav-clock-icon"></i>
                                </div>
                                <div class="trip-info-inner">
                                    <p><b>@choice('time.count_day', 4, ['count' => 4])</b></p>
                                    <p>@lang('book.duration')</p>
                                </div>
                            </div>
                            <div class="trip-info">
                                <div class="icon-wrap">
                                    <i class="trav-distance-icon"></i>
                                </div>
                                <div class="trip-info-inner">
                                    <p><b>237 km</b></p>
                                    <p>@lang('profile.distance')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inside-block">
                    <div class="dest-trip-plan">
                        <h3 class="dest-ttl">@lang('profile.destination') <span>6</span></h3>
                        <ul class="dest-image-list">
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li>
                                <img src="http://placehold.it/52x52" alt="photo">
                            </li>
                            <li class="more-photo-link">
                                <img src="http://placehold.it/52x52" alt="photo">
                                <span class="cover">+2</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="bottom-load-mark">
                @lang('profile.loading')
            </div>
        </div>
    </div>

    <div class="post-block" style="display:none;">
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap">
                    <img src="http://placehold.it/45x45" alt="">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link" href="#">Justin Baker</a>
                    </div>
                    <div class="post-info">
                        @lang('profile.added_a') <b>@lang('profile.photo')</b> yesterday at 10:33am
                    </div>
                </div>
            </div>
            <div class="post-top-info-action">
                <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                        <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-share-icon"></i>
                        </span>
                            <div class="drop-txt">
                                <p><b>@lang('profile.share')</b></p>
                                <p>@lang('profile.spread_the_word')</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-heart-icon"></i>
                        </span>
                            <div class="drop-txt">
                                <p><b>@lang('profile.add_to_favorites')</b></p>
                                <p>@lang('profile.save_it_for_later')</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-flag-icon-o"></i>
                        </span>
                            <div class="drop-txt">
                                <p><b>@lang('profile.report')</b></p>
                                <p>@lang('profile.help_us_understand')</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-image-container">
            <ul class="post-image-list">
                <li>
                    <img src="http://placehold.it/595x625" alt="">

                </li>
            </ul>
        </div>
        <div class="post-footer-info">
            <div class="post-foot-block post-reaction">
                <img src="./assets2/image/reaction-icon-smile-only.png" alt="smile">
                <span><b>12</b> @lang('profile.reactions')</span>
            </div>
            <div class="post-foot-block">
                <i class="trav-comment-icon"></i>
                <ul class="foot-avatar-list">
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    -->
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    -->
                    <li class="dropdown">
                      <span data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="small-ava" src="http://placehold.it/20x20" alt="ava">
                      </span>
                        <div class="dropdown-menu dropdown-info-style post-profile-block">
                            <div class="post-prof-image">
                                <img class="prof-cover"
                                     src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S"
                                     alt="photo">
                            </div>
                            <div class="post-prof-main">
                                <div class="avatar-wrap">
                                    <img src="http://placehold.it/70x70" alt="ava">
                                </div>
                                <div class="post-person-info">
                                    <div class="prof-name">Sue Perez</div>
                                    <div class="prof-location">United States of America</div>
                                </div>
                                <div class="drop-bottom-link">
                                    <a href="#" class="profile-link">@lang('profile.view_profile')</a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <span>@choice('comment.count_comment', 20, ['count' => 20])</span>
            </div>
        </div>
        <div class="post-comment-layer">
            <div class="post-comment-top-info">
                <ul class="comment-filter">
                    <li class="active">@lang('comment.top')</li>
                    <li>@lang('comment.new')</li>
                </ul>
                <div class="comm-count-info">
                    3 / 20
                </div>
            </div>
            <div class="post-comment-wrapper">
                <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                    </div>
                    <div class="post-comment-text">
<                       <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Katherin</a>
                            <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                            <div class="com-reaction">
                                <img src="./assets2/image/icon-smile.png" alt="">
                                <span>21</span>
                            </div>
                            <div class="com-time">6 hours ago</div>
                        </div>
                    </div>
                </div>
                <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                    </div>
                    <div class="post-comment-text">

                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Amine</a>
                            <a href="#" class="comment-nickname">@ak0117</a>
                        </div>
                        <div class="comment-txt">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                doloribus.</p>
                        </div>
                        <div class="comment-bottom-info">
                            <div class="com-reaction">
                                <img src="./assets2/image/icon-like.png" alt="">
                                <span>19</span>
                            </div>
                            <div class="com-time">6 hours ago</div>
                        </div>
                    </div>
                </div>
                <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                    </div>
                    <div class="post-comment-text">

                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Katherin</a>
                            <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                            <div class="com-reaction">
                                <img src="./assets2/image/icon-smile.png" alt="">
                                <span>15</span>
                            </div>
                            <div class="com-time">6 hours ago</div>
                        </div>
                    </div>
                </div>
                <a href="#" class="load-more-link">@lang('buttons.general.load_more')...</a>
            </div>
        </div>
    </div>

    <div class="post-block" style="display:none;">
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap">
                    <img src="http://placehold.it/45x45" alt="">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link" href="#">Justin Baker</a>
                    </div>
                    <div class="post-info">
                        @lang('profile.shared_a') <b>@lang('trip.trip_plan')</b> yesterday at 10:33am
                    </div>
                </div>
            </div>
            <div class="post-top-info-action">
                <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                        <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-share-icon"></i>
                        </span>
                            <div class="drop-txt">
                                <p><b>@lang('profile.share')</b></p>
                                <p>@lang('profile.spread_the_word')</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-heart-icon"></i>
                        </span>
                            <div class="drop-txt">
                                <p><b>@lang('profile.add_to_favorites')</b></p>
                                <p>@lang('profile.save_it_for_later')</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-flag-icon-o"></i>
                        </span>
                            <div class="drop-txt">
                                <p><b>@lang('profile.report')</b></p>
                                <p>@lang('profile.help_us_understand')</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-image-container post-follow-container">
            <ul class="post-image-list">
                <li>
                    <img src="http://placehold.it/595x400" alt="">
                </li>
            </ul>
            <div class="post-follow-block">
                <div class="follow-txt-wrap">
                    <div class="follow-txt">
                        <p class="follow-destination">4 Days to Dallas</p>
                        <div class="follow-tag-wrap">
                            <span class="follow-tag">@lang('profile.solo')</span>
                            <span class="follow-tag">@lang('profile.urban')</span>
                        </div>
                    </div>
                </div>
<               <div class="follow-btn-wrap">
                    <button type="button"
                            class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                        @lang('profile.view_plan')
                        <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                    </button>
>               </div>
            </div>
        </div>
        <div class="post-footer-info">
            <div class="post-foot-block post-reaction">
                <img src="./assets2/image/reaction-icon-smile-only.png" alt="smile">
                <span><b>6</b> @lang('profile.reactions')</span>
            </div>
            <div class="post-foot-block">
                <i class="trav-comment-icon"></i>
                <ul class="foot-avatar-list">
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    -->
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    -->
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                </ul>
                <span>@choice('comment.count_comment', 20, ['count' => 20])</span>
            </div>
        </div>
    </div>

    <div class="post-block" style="display:none;">
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap">
                    <img src="http://placehold.it/45x45" alt="">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link" href="#">Justin Baker</a>
                    </div>
                    <div class="post-info">
                        @lang('profile.started_following') <a href="#" class="link-place"><img
                                    src="./assets2/image/icon-small-flag.png" alt="flag"> United States
                            of America</a> today at 5:29pm
                    </div>
                </div>
            </div>
            <div class="post-top-info-action">
                <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                        <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-share-icon"></i>
                        </span>
                            <div class="drop-txt">
                                <p><b>@lang('profile.share')</b></p>
                                <p>@lang('profile.spread_the_word')</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-heart-icon"></i>
                        </span>
                            <div class="drop-txt">
                                <p><b>@lang('profile.add_to_favorites')</b></p>
                                <p>@lang('profile.save_it_for_later')</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-flag-icon-o"></i>
                        </span>
                            <div class="drop-txt">
                                <p><b>@lang('profile.report')</b></p>
                                <p>@lang('profile.help_us_understand')</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-image-container post-follow-container">
            <ul class="post-image-list">
                <li>
                    <img src="http://placehold.it/595x210" alt="">
                </li>
            </ul>
            <div class="post-follow-block">
                <div class="follow-txt-wrap">
                    <div class="follow-flag-wrap">
                        <img src="http://placehold.it/80x60?text=flag" alt="">
                    </div>
                    <div class="follow-txt">
                        <p class="follow-name">United States of America</p>
                        <div class="follow-foot-info">
                            <ul class="foot-avatar-list">
                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava">
                                </li><!--
                          -->
                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava">
                                </li><!--
                          -->
                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava">
                                </li>
                            </ul>
                            <i class="trav-talk-icon icon-grey-comment"></i>
                            <span>@lang('profile.count_talking_about_this', ['count' => '64K'])</span>
                        </div>
                    </div>
                </div>
                <div class="follow-btn-wrap">
                    <button type="button" class="btn btn-light-grey btn-bordered">
                        @lang('profile.follow')
                    </button>
<               </div>
            </div>
        </div>
        <div class="post-footer-info">
            <div class="post-foot-block post-reaction">
                <img src="./assets2/image/reaction-icon-smile-only.png" alt="smile">
                <span><b>6</b> @lang('profile.reactions')</span>
            </div>
            <div class="post-foot-block">
                <i class="trav-comment-icon"></i>
                <ul class="foot-avatar-list">
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    -->
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    -->
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                </ul>
                <span>@choice('comment.count_comment', 20, ['count' => 20])</span>
            </div>
        </div>
    </div>

    <div class="post-block post-badge-block" data-content='badges' style='display:none'>
        <div class="post-top-image">
            <img src="http://placehold.it/655x225" alt="image">
            <div class="badge-label"><span>@lang('profile.level_up')!</span></div>
        </div>
        <div class="post-badge-inner">
            <div class="badge-layer">
                <h3 class="ttl">@lang('profile.your_next_badge')</h3>
                <div class="badge-block">
                    <div class="img-wrap">
                        <img src="http://placehold.it/66x66" alt="">
                    </div>
                    <div class="badge-content">
                        <div class="badge-txt">
                            <h5 class="badge-ttl">@lang('profile.badge_name')</h5>
                            <div class="info-badge">
                                <ul class="foot-avatar-list">
                                    <li><img class="small-ava" src="http://placehold.it/20x20"
                                             alt="ava"></li><!--
                            -->
                                    <li><img class="small-ava" src="http://placehold.it/20x20"
                                             alt="ava"></li><!--
                            -->
                                    <li><img class="small-ava" src="http://placehold.it/20x20"
                                             alt="ava"></li>
                                </ul>
                                <span>@lang('profile.count_users_like_you_have_this_badge', ['count' => 87])</span>
                            </div>
                        </div>
                        <div class="badge-count">1000 <span>/ 135</span></div>
                    </div>
                </div>
            </div>
            <div class="badge-layer">
                <div class="badge-top-layer">
                    <h3 class="ttl-sm">@lang('profile.all_your_badges')</h3>
                    <a href="#" class="more-link"><span>@lang('profile.more')</span> <i
                                class="fa fa-caret-down"></i></a>
                </div>
                <ul class="badges-list">
                    <li>
                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                        <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                </ul>
                <div class="badge-progress-block">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 75%"
                             aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <p>@lang('profile.count_points_to_the_next_badge', ['count' => 385])</p>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('sidebar')
    <div class="post-block post-side-profile">
        <div class="image-wrap">
            <img src="http://placehold.it/385x290" alt="">
            <div class="profile-side-block">
                <div class="avatar-layer">
                    <div class="ava-inner">
                        <img src="http://placehold.it/80x80" alt="" class="avatar">
                        <a href="#" class="edit-ava-link">
                            <img src="./assets2/image/profile-ava-edit-img.png" alt="">
                        </a>
                    </div>
                    <div class="ava-txt">
                        <h4 class="ava-name">Justin baker</h4>
                        <p class="sub-ttl">United States</p>
                    </div>
                </div>
            </div>
            <div class="post-image-info">
                <div></div>
                <div class="follow-btn-wrap">
                    <button type="button"
                            class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                        <i class="trav-user-plus-icon"></i>
                        <span>@lang('profile.follow')</span>
                        <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                    </button>
                </div>
            </div>
        </div>
        <div class="post-profile-info">
            <ul class="profile-info-list">
                <li>
                    <p class="info-count">125</p>
                    <p class="info-label">@lang('profile.posts')</p>
                </li>
                <li>
                    <p class="info-count">58</p>
                    <p class="info-label">@lang('profile.followers')</p>
                </li>
                <li>
                    <p class="info-count">2</p>
                    <p class="info-label">@lang('profile.following')</p>
                </li>
            </ul>
        </div>
        <div class="post-txt-side">
            <h5 class="ttl">@lang('profile.about')</h5>
            <p>Phasellus sit amet mauris at massa dignissim tristique. Nulla ut lobortis massa, viverra
                feugiat leo.</p>
        </div>
        <div class="post-profile-side-foot">
            <div class="post-profile-foot-inner">
                <a href="#" class="profile-website-link">
                    <span class="round-icon">
                      <i class="trav-link"></i>
                    </span>
                    <span>@lang('profile.website')</span>
                </a>
                <ul class="profile-social-list">
                    <li>
                        <a href="#" class="facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="instagram">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="post-block post-side-profile sm-profile">
        <div class="image-wrap">
            <img src="http://placehold.it/385x125" alt="">
            <div class="post-image-info">
                <div class="ava-inner">
                    <img src="http://placehold.it/58x58" alt="" class="avatar">
                    <a href="#" class="edit-ava-link">
<                       <img src="./assets2/image/profile-ava-edit-img.png" alt="">
>                   </a>
                </div>
                <div class="follow-btn-wrap">
                    <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side">
                        <i class="trav-user-plus-icon"></i>
                        <span>@lang('profile.follow')</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="post-profile-info">
            <ul class="profile-info-list">
                <li>
                    <p class="info-count">125</p>
                    <p class="info-label">@lang('profile.posts')</p>
                </li>
                <li>
                    <p class="info-count">58</p>
                    <p class="info-label">@lang('profile.followers')</p>
                </li>
                <li>
                    <p class="info-count">2</p>
                    <p class="info-label">@lang('profile.following')</p>
                </li>
            </ul>
        </div>
    </div>

    <div class="post-block post-side-profile sm-profile">
        <div class="image-wrap">
            <img src="http://placehold.it/385x125" alt="">
            <div class="post-image-info">
                <div class="avatar-layer">
                    <div class="ava-inner">
<                       <img src="http://placehold.it/58x58" alt="" class="avatar">
                        <a href="#" class="edit-ava-link">
                            <img src="./assets2/image/profile-ava-edit-img.png" alt="">
                        </a>
>                   </div>
                    <div class="ava-txt">
                        <h4 class="ava-name">Justin baker</h4>
                        <p class="sub-ttl">United States</p>
                    </div>
                </div>
                <div class="follow-btn-wrap">
                    <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side">
                        <i class="trav-user-plus-icon"></i>
                        <span>@lang('profile.follow')</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="post-profile-info">
            <ul class="profile-info-list">
                <li>
                    <p class="info-count">125</p>
                    <p class="info-label">@lang('profile.posts')</p>
                </li>
                <li>
                    <p class="info-count">58</p>
                    <p class="info-label">@lang('profile.followers')</p>
                </li>
                <li>
                    <p class="info-count">2</p>
                    <p class="info-label">@lang('profile.following')</p>
                </li>
            </ul>
        </div>
    </div>

    <div class="post-block post-side-search">
        <div class="search-wrapper">
            <input class="" id="" placeholder="@lang('profile.search_your_visited_places')" type="text">
            <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
        </div>
    </div>

    <div class="post-block post-side-type">
        <div class="type-label">@lang('profile.type')</div>
        <div class="type-progress-block">
            <div class="type-line">
                <div class="label">@lang('profile.places')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="75"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">4</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.cities')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">2</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.countries')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">1</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.events')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">1</div>
            </div>
        </div>
    </div>

    <div class="post-block post-side-type">
        <div class="type-label">@lang('profile.type')</div>
        <div class="type-progress-block">
            <div class="type-line">
                <div class="label">@lang('profile.places')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">17</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.posts')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 30%" aria-valuenow="30"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">5</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.events')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 18%" aria-valuenow="18"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">3</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.photos')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 12%" aria-valuenow="12"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">2</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.videos')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 35%" aria-valuenow="35"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">6</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('trip.trip_plans')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 9%" aria-valuenow="9"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">1</div>
            </div>
        </div>
    </div>

    <div class="post-block post-side-block">
        <div class="post-side-top timeline-top">
            <h3 class="side-ttl">@lang('profile.timeline')</h3>
            <div class="side-right-control">
                <a href="#" class="slide-link slide-prev">
                    <i class="trav-angle-left"></i>
                </a>
                <span class="month-name">2017</span>
                <a href="#" class="slide-link slide-next">
                    <i class="trav-angle-right"></i>
                </a>
            </div>
        </div>
        <div class="post-calendar-inner mCustomScrollbar">
            <div class="post-calendar">
                <h5 class="calendar-title">September 2017</h5>
                <div class="post-calendar_wrap">
                    <ul class="cal-day-list">
                        <li class="disabled">28</li>
                        <li class="disabled">29</li>
                        <li class="disabled">30</li>
                        <li class="disabled">31</li>
                        <li>1</li>
                        <li>2</li>
                        <li>3</li>
                        <li>4</li>
                        <li>5</li>
                        <li class="selected">6 <span class="mark">2</span></li>
                        <li>7</li>
                        <li>8</li>
                        <li>9</li>
                        <li>10</li>
                        <li>11</li>
                        <li>12</li>
                        <li>13</li>
                        <li>14</li>
                        <li>15</li>
                        <li>16</li>
                        <li>17</li>
                        <li>18</li>
                        <li>19</li>
                        <li>20</li>
                        <li>21</li>
                        <li>22</li>
                        <li>23</li>
                        <li>24</li>
                        <li>25</li>
                        <li>26</li>
                        <li>27</li>
                        <li>28</li>
                        <li>29</li>
                        <li>30</li>
                        <li class="disabled">1</li>
                    </ul>
                </div>
            </div>
            <div class="post-calendar">
                <h5 class="calendar-title">October 2017</h5>
                <div class="post-calendar_wrap">
                    <ul class="cal-day-list">
                        <li class="disabled">24</li>
                        <li class="disabled">25</li>
                        <li class="disabled">26</li>
                        <li class="disabled">27</li>
                        <li class="disabled">28</li>
                        <li class="disabled">30</li>
                        <li>1</li>
                        <li class="selected">2 <span class="mark">1</span></li>
                        <li>3</li>
                        <li>4</li>
                        <li>5</li>
                        <li>6</li>
                        <li>7</li>
                        <li>8</li>
                        <li>9</li>
                        <li>10</li>
                        <li>11</li>
                        <li>12</li>
                        <li>13</li>
                        <li>14</li>
                        <li>15</li>
                        <li>16</li>
                        <li>17</li>
                        <li>18</li>
                        <li>19</li>
                        <li>20</li>
                        <li>21</li>
                        <li>22</li>
                        <li>23</li>
                        <li>24</li>
                        <li>25</li>
                        <li>26</li>
                        <li>27</li>
                        <li>28</li>
                        <li>29</li>
                        <li>30</li>
                        <li>31</li>
                        <li class="disabled">1</li>
                        <li class="disabled">2</li>
                        <li class="disabled">3</li>
                        <li class="disabled">4</li>
                        <li class="disabled">5</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-side-block">
        <div class="post-side-top timeline-top">
            <h3 class="side-ttl">@lang('profile.timeline')</h3>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <label>@lang('other.sort_by')</label>
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>Days</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-timeline-inner mCustomScrollbar">
            <div class="timeline-block">
                <div class="timeline-label">June 2017</div>
                <ul class="timeline-media-list">
                    <li><img src="http://placehold.it/71x51" alt=""></li>
                    <li><img src="http://placehold.it/71x51" alt=""></li>
                    <li><img src="http://placehold.it/71x51" alt=""></li>
                </ul>
            </div>
            <div class="timeline-block">
                <div class="timeline-label">September 2016</div>
                <ul class="timeline-media-list">
                    <li><img src="http://placehold.it/71x51" alt=""></li>
                    <li><img src="http://placehold.it/71x51" alt=""></li>
                    <li><img src="http://placehold.it/71x51" alt=""></li>
                    <li>
                        <img src="http://placehold.it/71x51" alt="">
                        <a href="#" class="cover-link">+21</a>
                    </li>
                </ul>
            </div>
            <div class="timeline-block">
                <div class="timeline-label">September 18</div>
                <ul class="timeline-media-list">
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                </ul>
            </div>
            <div class="timeline-block">
                <div class="timeline-label">September 2</div>
                <ul class="timeline-media-list">
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li>
                        <img src="http://placehold.it/41x41" alt="">
                        <a href="#" class="cover-link">+21</a>
                    </li>
                </ul>
            </div>
            <div class="timeline-block">
                <div class="timeline-label">August 5</div>
                <ul class="timeline-media-list">
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                </ul>
            </div>
            <div class="timeline-block">
                <div class="timeline-label">Jun 21</div>
                <ul class="timeline-media-list">
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                </ul>
            </div>
            <div class="timeline-block">
                <div class="timeline-label">May 6</div>
                <ul class="timeline-media-list">
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                    <li><img src="http://placehold.it/41x41" alt=""></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="post-block post-side-map">
        <div class="post-map-block">
            <div class="post-map-inner">
                <img src="http://placehold.it/355x250" alt="map">
                <div class="zoom-controls-wrap">
                    <div class="control ctrl-plus">+</div>
                    <div class="control ctrl-minus">-</div>
                </div>
            </div>
        </div>
    </div>

    <div class="post-block post-side-filter">
        <div class="top-filter-wrap">
            <div class="img-wrap">
                <img src="http://placehold.it/54x54" alt="">
            </div>
            <div class="top-filter">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="80"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="filter-info">
                    <span class="count">4,947</span>
                    <span class="left">343 left</span>
                </div>
            </div>
        </div>
        <div class="filter-content">
            <div class="side-filter-row">
                <div class="label">@lang('comment.comments')</div>
                <div class="progress-filter">
                    <div class="progress-count">
                        <b>163</b>&nbsp;/&nbsp;<span>500</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 40%"
                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="side-filter-row">
                <div class="label">@lang('profile.likes')</div>
                <div class="progress-filter">
                    <div class="progress-count">
                        <b>120</b>&nbsp;/&nbsp;<span>200</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 80%"
                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="side-filter-row">
                <div class="label">@lang('profile.trips')</div>
                <div class="progress-filter">
                    <div class="progress-count">
                        <b>120</b>&nbsp;/&nbsp;<span>200</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 80%"
                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="side-filter-row">
                <div class="label">@lang('profile.reviews')</div>
                <div class="progress-filter">
                    <div class="progress-count">
                        <b>120</b>&nbsp;/&nbsp;<span>200</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 80%"
                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="side-filter-row">
                <div class="label">@lang('profile.videos')</div>
                <div class="progress-filter">
                    <div class="progress-count">
                        <b>120</b>&nbsp;/&nbsp;<span>200</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 80%"
                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="side-filter-row">
                <div class="label">@lang('profile.photos')</div>
                <div class="progress-filter">
                    <div class="progress-count">
                        <b>120</b>&nbsp;/&nbsp;<span>200</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 80%"
                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
