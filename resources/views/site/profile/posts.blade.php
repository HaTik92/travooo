@extends('site.profile.template.profile')
@php $title = 'Travooo - Profile Posts Page'; @endphp
@section('content')
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
    <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js"></script>
    <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css" type="text/css" />
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />

    <style>
        .select2-input {
            min-height:49px !important;
            padding:8px !important;
            border: 1px solid #e6e6e6 !important;
            margin-bottom: 8px;
        }
        .select2-selection__choice {
            background: #f7f7f7 !important;
            padding: 5px 10px !important;
            position: relative !important;
            border-radius: 0 !important;
            color: #4080ff !important;
            margin-top:0px !important;
        }
        .select2-selection__arrow{
            top:11px !important;
        }
        .ui-state-highlight {
            height:25px;
            background-color:#c4d9ff;
        }
        .padding-10{
            padding: 10px 0 !important;
        }

        .note-toolbar.panel-heading {
            height: 0;
        }

        .rep-share-block .post-foot-block.ml-auto{
            display: none !important;
        }


    </style>


    <div class="container-fluid">
        @include('site.profile._profile_header')


        <div class="custom-row">
            <!-- MAIN-CONTENT -->
            <div class="main-content-layer profile-posts">
                <div class="post-block post-top-bordered" >
                    <div class="post-side-top top-tabs arrow-style">
                        <div class="post-top-txt plan-tab-title">
                            <h3 class="side-ttl  current post-tab-link" tab-type="all_posts">
                                <a href="javascript:;">
                                    All Posts
                                </a>
                            </h3>
                            <h3 class="side-ttl  post-tab-link" tab-type="trip_plans">
                                <a href="javascript:;">
                                    Trip Plans
                                </a>
                            </h3>
                            <h3 class="side-ttl post-tab-link" tab-type="travelogs">
                                <a href="javascript:;">
                                    Travelogs
                                </a>
                            </h3>
                            <h3 class="side-ttl post-tab-link" tab-type="discussions">
                                <a href="javascript:;">
                                    Discussions
                                </a>
                            </h3>
                            <h3 class="side-ttl post-tab-link" tab-type="reviews">
                                <a href="javascript:;">
                                    Reviews
                                </a>
                            </h3>
                        </div>
                    </div>
                </div>

                @if(count($posts) > 0)
                    <div id="feed">
                        @if(session()->has('alert-success'))
                            <div class="alert alert-success" id="postSavedSuccess">
                                {{ session()->get('alert-success') }}
                            </div>
                        @endif

                        @if(session()->has('alert-danger'))
                            <div class="alert alert-danger" id="postSavedDanger">
                                {{ session()->get('alert-danger') }}
                            </div>
                        @endif

                        @if(session()->has('alert-danger-server'))
                            <div class="alert alert-danger" id="postSavedDanger">
                                {{ session()->get('alert-danger-server') }}
                            </div>
                        @endif

                        @if ($errors->has('file'))
                            <div class="alert alert-danger" id="postSavedDanger">
                                {{ $errors->first('file') }}
                            </div>
                        @endif
                        @if ($errors->has('text'))
                            <div class="alert alert-danger" id="postSavedDanger">
                                {{ $errors->first('text') }}
                            </div>
                        @endif

                        @php
                            if($user){
                                $friends = $user->friends->pluck('friends_id')->toArray();
                                $followers = $user->followings->pluck('users_id')->toArray();
                                $friends_followers = array_merge($friends, $followers);
                            }else{
                                $friends_followers = [];
                            }

                        @endphp

                        @foreach($posts AS $post)
                            @if (isset($post->permission) AND $post->permission == \App\Models\Posts\Posts::POST_PERMISSION_PRIVATE AND $post->users_id != Auth::user()->id)
                                @continue;
                            @elseif (isset($post->permission) AND $post->permission == \App\Models\Posts\Posts::POST_PERMISSION_FRIENDS_ONLY AND $post->users_id != Auth::user()->id AND !in_array( $post->users_id, getUserFriendsIds())) {
                            @continue;
                            @endif

                            @if($post->type=="other" AND $post->action=="show" AND !user_access_denied($post->user_id, true))
                                @include('site.home.partials.other_post')

                            @elseif($post->type=="place" AND $post->action=="review" AND !user_access_denied($post->user_id, true))
                                @include('site.home.new.partials.place-review')

                            @elseif($post->type=="hotel" AND $post->action=="review")
                                @include('site.home.partials.hotel_review')

                            @elseif($post->type=="post" AND $post->action=="publish" AND !user_access_denied($post->user_id, true))
                                @include('site.home.new.partials.primary-text-only-post')

                            @elseif($post->type=="trip" AND ($post->action=="create" || $post->action=='plan_updated') AND !user_access_denied($post->user_id, true))
                                @include('site.home.new.partials.trip')
                            @elseif($post->type=="discussion" AND $post->action=="create" AND !user_access_denied($post->user_id, true))
                                @include('site.home.new.partials.discussion')

                            @elseif($post->type=="report" AND ($post->action=="create" || $post->action=="update") AND !user_access_denied($post->user_id, true))
                                @include('site.home.new.partials.travlog')

                            @elseif($post->type =='share' && $post->action == 'plan_media_shared')
                                @include('site.home.new.partials.tripmediashare')

                            @elseif($post->type =='share' && $post->action == 'plan_step_shared')
                                @include('site.home.new.partials.tripplace')

                            @elseif($post->type =='share' && $post->action == 'plan_shared')
                                @include('site.home.new.partials.tripshare')

                            @elseif($post->type == 'share' AND !user_access_denied($post->user_id, true))
                                @include('site.home.new.partials.shareable-post')
                            @endif

                        @endforeach


                    </div>
                    <div id='profile_post_pagination' style='text-align:center;'>
                        <input type="hidden" id="pageno" value="1">
                        <div id="profile_post_feedloader" class="post-animation-content post-block">
                            <div class="post-top-info-layer">
                                <div class="post-top-info-wrap">
                                    <div class="post-top-avatar-wrap animation post-animation-avatar"></div>
                                    <div class="post-animation-info-txt">
                                        <div class="post-top-name animation post-animation-top-name"></div>
                                        <div class="post-info animation post-animation-top-info"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-animation-conteiner">
                                <div class="post-animation-text animation pw-2"></div>
                                <div class="post-animation-text animation pw-3"></div>
                            </div>
                            <div class="post-animation-footer">
                                <div class="post-animation-text animation pw-4"></div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="post-block no-more-block {{(count($posts) > 0)?'d-none':''}}">No more posts to show.</div>

            </div>
            <!-- SIDEBAR -->
            <div class="sidebar-layer" id="sidebarLayer">
                <aside class="sidebar">
                    @include('site.profile.partials._aside_posts_block')
                    @if($user->about !='')
                        <div class="post-block post-side-block">
                            <div class="post-side-top">
                                <h3 class="h3-side-ttls">Bio</h3>
                                @if($login_user_id != '')
                                    <h3 class="side-ttl edit-about"><span class="edit">Edit</span></h3>
                                @endif
                            </div>
                            <div class="side-post-inner profile-about-inner" id="timline_list">
                                <p class="l-breack">{{$user->about}}</p>
                            </div>
                            <div class="side-post-inner profile-bio-block d-none" >
                                <form id="profile_about_form">
                                    <textarea class="flex-text edit_textarea_about" required  id="profile_about_text"  maxlength="256" oninput="textarea_auto_height(this)">{{$user->about}}</textarea>
                                </form>
                            </div>
                        </div>
                    @endif
                    @include('site.profile.partials._aside_link_block')
                    <div class="aside-footer">
                        <ul class="aside-foot-menu">
                            <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                            <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                            <li><a href="{{url('/')}}">Advertising</a></li>
                            <li><a href="{{url('/')}}">Cookies</a></li>
                            <li><a href="{{url('/')}}">More</a></li>
                        </ul>
                        <p class="copyright">Travooo &copy; 2017 - {{date('Y')}}</p>
                    </div>

                </aside>
            </div>
        </div>
    </div>
    @if(Auth::check())
        @include('site.home.new.partials._user-who-like-modal')
        @include('site.home.new.partials._sendtofriend-modal')
        @include('site.home.new.partials._delete-modal')
        @include('site.home.partials.modal_comments_like')
        @include('site.place2.partials.modal_like')
        @include('site.home.new.partials._share-modal')
    @endif

    @include('site.home.new.partials._preloader-modal')

@endsection

@section('after_scripts')
    <link href="{{ asset('assets2/js/lightbox2/src/css/lightbox.css') }}" rel="stylesheet"/>
    <script src="//unpkg.com/jscroll/dist/jquery.jscroll.min.js"></script>
    <script src="{{ asset('/assets3/js/profile.js') }}"></script>
    <script src="{{ asset('assets2/js/posts-script.js?v=0.4') }}"></script>
    <script src="{{asset('assets2/js/lightbox2/src/js/lightbox.js')}}"></script>
    @include('site.profile._scripts')
    @include('site.home._scripts')
    @include('site.home.partials._comments_scripts')
    @include('site.discussions.partials._view-scripts')
    @include('site.partials.send-post')

    <script>
        lightbox.option({
            'disableScrolling': true,
            'fitImagesInViewport': true,
        });

        var filter_type = 'all_posts';
        var  next_additional = {{json_encode(session('loaded_ids'))}};

        $(document).ready(function () {
            //Filter by type (All Posts, Trip Plans, Travelogs, Discussions, Reviews)
            $(document).on('click', '.post-tab-link', function(){
                filter_type = $(this).attr('tab-type');
                $(this).parent().find('.current').removeClass('current')
                $(this).addClass('current');

                $('#feed').html('');
                next_additional = '';
                $('#pageno').val(1);
                $("#pagination").show();
                $("#profile_post_pagination").show();
                $('.no-more-block').addClass('d-none')
            })

            // copy post link
            $(document).on( 'click', '.copy-newsfeed-link', function(){
                var copy_link = $(this).attr('data-link');
                var _temp = $("<input>");
                $("body").append(_temp);
                _temp.val(copy_link).select();
                document.execCommand("copy");
                _temp.remove()

                toastr.success('The link copied!');
            });

            $('.modal-660').jscroll();

            $('#spamReportDlgNew').on('show.bs.modal',function (event){
                id = event.relatedTarget.dataset.id;
                type = event.relatedTarget.dataset.type;
                element = event.relatedTarget.dataset.element;
                $('#delete_id').val(id);
                $('#deletePostType').val(type);
                $('#element_id').val(element);
            });

            $(document).on('click', ".post_like_button a", function (e) {
                e.preventDefault();
                $(this).closest('.post_like_button').toggleClass('liked');
            });

            $('#usersWhoLike').on('show.bs.modal',function(e){
                $('#userWhoLikeContainer').empty();
                var datas = e.relatedTarget.dataset;
                $.ajax({
                    url: "{{route('home.userlikes')}}",
                    type: "POST",
                    data:{ type: datas.type, id: datas.id},
                    success: function(resp){
                        resp =jQuery.parseJSON(resp);
                        $('#userWhoLikeContainer').empty();
                        $('#userWhoLikeContainer').html(resp.html);
                        $('#usersWhoLike').find('.total_count').html(resp.count)
                    },error: function(){

                    }
                });


            });

            // $(document).on('click', '.discussion-modal', function(){
            //     console.log('this', $(this).attr('data-dis_id'))
            //     var discussion_id = $(this).attr('data-dis_id');
            //     var username = $(this).data('uname')

            //     window.open('{{url('discussion')}}/'+ username +'/' + discussion_id, '_blank');
            // })
            $(document).on("click", ".modal-close", function () {
                window.history.pushState({}, document.title, "/profile-posts");
            })

            //permission change functionality
            $('.permission_drop_down').on('click',function(){
                type = $(this).data('type');
                post_id = $(this).data('post-id');
                $('.add_post_permission_button_'+post_id).empty();

                $.ajax({
                    type: 'POST',
                    url: '{{ url("posts/update-post-permission")}}',
                    data: {post_id:post_id,type:type},
                    success: function(data){
                        if(data == 1){
                            if(type == 0){
                                $('.add_post_permission_button_'+post_id).html('<i class="trav-globe-icon"></i> PUBLIC');
                            }else if(type==1){
                                $('.add_post_permission_button_'+post_id).html('<i class="trav-users-icon"></i> Friends Only');
                            }else if(type==2){
                                $('.add_post_permission_button_'+post_id).html('<i class="trav-lock-icon"></i> Only Me');
                            }
                            toastr.success('Permission updated successfully');
                        }
                        else if(data ==0)
                            toastr.warning('Something went wrong');
                    }
                });
            });

            $('#profile_post_feedloader').on('inview', function (event, isInView) {
                if (isInView) {
                    var nextPage = parseInt($('#pageno').val()) + 1;
                    var checkCount = true;
                    var i = 0;
                    while(checkCount) {
                        checkCount = false;
                        if(!getFeed(nextPage, filter_type)){
                            nextPage++;
                            checkCount = true;
                            i++;

                            if(i == 10){
                                $("#profile_post_pagination").hide();
                                $('.no-more-block').removeClass('d-none')
                                break;
                            }
                        }
                    }

                }
            });

            $('#deletePostNew').on('show.bs.modal',function (event){
                id = event.relatedTarget.dataset.id;
                type = event.relatedTarget.dataset.type;
                element = event.relatedTarget.dataset.element;
                $('#delete_id').val(id);
                $('#deletePostType').val(type);
                $('#element_id').val(element);
            });
            $('.del_post_btn').on('click',function (){
                $.ajax({
                    method: "POST",
                    url: "{{url("delete-post") }}",
                    data: {id:$('#delete_id').val(),type: $('#deletePostType').val()}
                }).done(function (res) {
                    if(res>0){
                        $('#'+$('#element_id').val()).remove();
                        $('#delete_id').val('');
                        $('#deletePostType').val('');
                        $('#element_id').val('');
                        $('#deletePostNew').modal('hide');
                        toastr.success('Post has been deleted');
                    }
                });
            });

            $(document).on('click','.post-modal', function (e) {
                var post_id = $(this).data('id');
                var post_type = $(this).attr('post-type')
                var post_shared = !!$(this).attr('data-shared')
                var shared_id = $(this).attr("data-shared_id")
                let videos = []
                let currentTime = 0
                let currentVideoId
                if ($(this).find("video").length && e.target instanceof HTMLVideoElement) {
                    videos = $(this).find("video")
                    currentVideoId = e.target.id.match(/[0-9]+/g)[0]
                    currentTime = videos.length == 1 ? videos[0].currentTime : 0
                    localStorage.setItem("currentVideoId", currentVideoId)
                    localStorage.setItem("currentTime", currentTime)
                }
                $('#post_view').find(".post-type-modal-inner").remove()
                $('#post_view').find(".modal-850").remove()

                $('#post_view').append(`
                                <div class="modal-dialog modal-custom-style modal-850" role="document">
                                    <div class="modal-custom-block">
                                        <div class="post-block post-happen-question">
                                            <div class="dis-loader-bar">
                                                <span>
                                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>`)

                if(isMobileDevice()){
                    var unique_url = $(this).closest('.post-block').find('.copy-newsfeed-link').attr('data-link');
                    var win = window.open(unique_url, '_blank');
                    win.focus()
                    return;
                }else{
                    $('#post_view').modal('show')
                }

                // window.history.pushState({}, document.title, "/home");
                $.ajax({
                    method: "POST",
                    url: "{{route('post.get_post_view')}}",
                    data: {post_id: post_id, post_type, post_shared, shared_id},
                    success: function (res) {
                        if (res != '') {
                            let username = $(res).find(".username").val()
                            post_type.toLowerCase() != 'event' ? window.history.pushState({html: $('#post_view').find(".post-type-modal-inner").innerHTML, isBack: true}, document.title, `/${post_type.toLowerCase()}/${username}/${post_id}`) : ''

                            $('#post_view').find(".modal-850").remove()
                            $('#post_view').append(res);
                            answerFilterOption();

                            e.preventDefault();
                            var elem = $('#post_view').find(".comment-btn")
                            var Item = elem.attr('data-tab');
                            CommentEmoji(elem.closest('.post-block').find('.postCommentForm'))

                            $('.emojionearea-button').removeClass('d-none')
                            $('.note-placeholder').addClass('custom-placeholder')
                            if($("#post_view").find("video").length) {
                                if($("#post_view").find("video").length == 1) {
                                    let player = $("#post_view").find("video")[0]
                                    player.currentTime = localStorage.getItem('currentTime') ?? 0
                                    player.muted = false
                                    player.controls = true
                                    player.play()
                                } else {
                                    let player = $("#post_view").find(`video[data-id=video_tmp${localStorage.getItem('currentVideoId')}]`)[0]
                                    player.currentTime = 0
                                    player.muted = false
                                    player.controls = true
                                    player.play()
                                }
                            }
                        }
                    }
                })
            })

            $(document).on("click", ".modal-close", function () {
                var user_id = "{{$user->id}}";
                window.history.pushState({}, document.title, "/profile-posts/" + user_id);
            })


            $('.extednd-btn').click(function(e){
                e.preventDefault();
                $(this).toggleClass('shrink');
                $(this).closest('.share-to-friend').toggleClass('relative');
            });
            $('#sendToFriendModal').on('hidden.bs.modal', function (e) {
                $('#sendToFriendModal .extednd-btn').removeClass('shrink');
                $('#sendToFriendModal .share-to-friend').removeClass('relative');
            })
            $('.friend-search-results').mCustomScrollbar();
            $('.send_message').on('click',function(){
                var message = $('#message_popup_text').val();

                message = message+'<br><br><br>' +$('#message_popup_content').html();
                var participants = $(this).data('user');
                $.ajax({
                    method: "POST",
                    url: './../../chat/send',
                    data: {
                        chat_conversation_id: 0,
                        participants:participants,
                        message:message,
                        post_type: send_post_type,
                        post_id: send_post_id,
                    },
                    success:function(e){
                        toastr.success('Your message has been sent');
                        $('#sendToFriendModal').modal('hide');
                        $('#message_popup_content').empty();
                        $('#message_popup_text').val()
                    }
                });
                $('#sendToFriendModal .extednd-btn').removeClass('shrink');
                $('#sendToFriendModal .share-to-friend').removeClass('relative');
            });
            $('#sendToFriendModal').on('shown.bs.modal', function (event) {
                $('#message_popup_content').empty();
                var html = $('#'+event.relatedTarget.dataset.id).html();
                var jHtmlObject = jQuery(html);
                var editor = jQuery("<p>").append(jHtmlObject);
                editor.find('.post-top-info-action').remove();
                editor.find('.privacy-settings').remove();
                editor.find('.post-footer-info').remove();
                editor.find('.post-comment-wrapper').remove();
                editor.find('.post-comment-layer').remove();
                var newHtml = editor.html();

                $('#message_popup_content').html('<div class="post-block ">'+newHtml+'</div>');

            });

            $('body').on('click', '.event_like_button', function (e) {
                obj = $(this);
                event_id = $(this).attr('data-id');
                $.ajax({
                    method: "POST",
                    url: "{{ route('place.eventlike') }}",
                    data: {event_id: event_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);

                        if (result.status == 'yes') {

                            obj.find('strong').html(result.count);
                            obj.find('i').toggleClass('red');
                        } else if (result.status == 'no') {

                            obj.find('strong').html(result.count);
                            obj.find('i').toggleClass('red');

                        }
                    });
                e.preventDefault();
            });

            $('body').on('click', '.event_share_button', function (e) {
                obj = $(this);
                event_id = $(this).attr('data-id');
                $.ajax({
                    method: "POST",
                    url: "{{ route('place.eventshare') }}",
                    data: {event_id: event_id}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);

                        if (result.status == 'yes') {

                            obj.find('strong').html(result.count);
                            alert(result.msg);
                        } else if (result.status == 'no') {
                            alert(result.msg);
                        }
                    });
                e.preventDefault();
            });

        });

        function getFeed(nextPage, filter_type){
            var a= true;
            $.ajax({
                type: 'POST',
                url: '{{url("profile/update_feed")}}',
                data: {pageno: nextPage, next_additional: next_additional, filter_type:filter_type},
                async: false,
                success: function (data) {
                    if(data != ''){

                        $('#feed').append(data.view);
                        next_additional = data.next_additional

                        $('#pageno').val(nextPage);
                        lazyloader();
                        $('[data-tab]').on('click', function (e) {
                            var Item = $(this).attr('data-tab');
                            $('[data-content]').css('display', 'none');
                            $('[data-content=' + Item + ']').css('display', 'block');
                            e.preventDefault();
                            Comment_Show.init($(this).closest('.post-block'));
                        });

                    } else {
                        $("#pagination").hide();
                    }
                    a = data.view;
                }
            });
            return a;
        }

        function answerFilterOption() {
            //Answers Filter type select
            $(".sort-select").each(function () {
                var classes = $(this).attr("class"),
                    id = $(this).attr("id"),
                    name = $(this).attr("name"),
                    select = $(this).attr("data-type"),
                    type = $(this).attr("data-sorted_by"),
                    disc_id = $(this).attr("data-dis_id");

                var template = '<div class="' + classes + '" data-type="' + disc_id + '">';
                template += '<span class="sort-select-trigger">' + $(this).attr("placeholder") + '</span>';
                template += '<div class="sort-options">';
                $(this).find("option").each(function () {
                    template += '<span class="sort-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
                });
                template += '</div></div>';

                $(this).wrap('<div class="sort-select-wrapper"></div>');
                $(this).hide();
                $(this).after(template);
            });

            $(".sort-option:first-of-type").hover(function () {
                $(this).parents(".sort-options").addClass("option-hover");
            }, function () {
                $(this).parents(".sort-options").removeClass("option-hover");
            });
            $(".sort-select-trigger").on("click", function () {
                $('html').one('click', function () {
                    $(".sel-select").removeClass("opened");
                });
                $(this).parents(".sort-select").toggleClass("opened");
                event.stopPropagation();
            });


            $(".sort-option").on("click", function () {
                $(this).parents(".sort-options").find(".sort-option").removeClass("selection");
                $(this).addClass("selection");
                $(this).parents(".sort-select").removeClass("opened");
                $(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

                var discussion_id = $(this).parents(".sort-select").attr('data-type');
                var type = $(this).data("value");

                getFilterAnswers(discussion_id, type)

            });

        }

    </script>


@endsection
