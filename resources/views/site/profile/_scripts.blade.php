<script>
    var review_sort = review_score = place_id = place_name = '';
    $(document).ready(function () {
        //socal media script
        if($('#website_url').val() != ''){
            $('#website_url').css('padding-left', '71px')
        }

        $(document).on('blur', 'input.social-input', function(){
            var media_value = $(this).val()
            var new_value = media_value.replace('https://', '');

            $(this).val(new_value)
        })

        $(document).on('blur', '#website_url', function(){
            var media_value = $(this).val()
            if(media_value != ''){
                var new_value = media_value.replace('https://', '');
                $(this).val(new_value)

                $('.tr-link-protocol').show()
                $(this).css('padding-left', '71px')
            }
        })

        $(document).on('keyup', '#website_url', function(){

            var input_val = $(this).val();

            if(input_val == ''){
                $('.tr-link-protocol').hide()
                $(this).css('padding-left', '14px')
            }
        })


        filterOrder('review-sort', 'Newest');
        filterOrder('review-score', 'All');
        
        $(document).on('click', '.morelink', function(){
            var moretext = "see more";
            var lesstext = "see less";
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
        
        $('body').on('click', '.post_likes_modal', function (e) {
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('post.likes4modal') }}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    
                    $('#likesModal').find('.modal-body').html(result);
                    $('#likesModal').modal('show');
                });
            e.preventDefault();
        });


        //Review place follow/unfollow button
        $(document).on('click', '.place-follow-btn', function () {
            var place_id = $(this).attr('data-id');
            var type = $(this).attr('data-type');
            var followurl = "{{  route('place.follow', ':place_id')}}";
            var unfollowurl = "{{  route('place.unfollow', ':place_id')}}";
            var places_list = $(this).closest('.review-body-block').find('>div');
        
            if (type == "follow"){
                url = followurl.replace(':place_id', place_id);
            } else if (type == "unfollow"){
                url = unfollowurl.replace(':place_id', place_id);
            }
            
            $.ajax({
                method: "POST",
                url: url,
                data: {name: type}
            })
            .done(function (res) {
                if (res.success == true) {
                   places_list.each(function(){
                       if($(this).find('.place-follow-btn').attr('data-id') == place_id){
                           if(type == 'follow'){
                                $(this).find('.place-follow-btn').html('unfollow')
                                $(this).find('.place-follow-btn').attr('data-type', 'unfollow')
                                $(this).find('.place-follow-btn').removeClass('btn-light-primary')
                                $(this).find('.place-follow-btn').addClass('btn-light-grey')
                           }else{
                               $(this).find('.place-follow-btn').html('follow')
                               $(this).find('.place-follow-btn').attr('data-type', 'follow')
                                $(this).find('.place-follow-btn').removeClass('btn-light-grey')
                                $(this).find('.place-follow-btn').addClass('btn-light-primary')
                           }
                       }
                   })
                } else if (res.success == false) {

                }
            });
        })
        
        //---START---badges page script
        $(document).on('click', '.badge-tab', function () {
            var current_tub = $(this).attr('data-tab');
            
            $('.post-badges-inner .badge-list').each(function(){
                if($(this).hasClass(current_tub)){
                    $(this).removeClass('d-none');
                }else{
                     $(this).addClass('d-none');
                }
            })
            
            $(this).parent().find('.current').removeClass('current')
            $(this).addClass('current')
        })
        
        $('#badge_point_loader').on('inview', function (event, isInView) {
        if (isInView) {
            var nextPage = parseInt($('#pagenum').val()) + 1;
            var user_id = '{{$user->id}}';
            $.ajax({
                type: 'POST',
                url: '{{url("profile/update-badge-points")}}',
                data: {pagenum: nextPage, user_id: user_id},
                async: false,  
                success: function (data) {
                    if(data != ''){							 
                          $('#badge_points_block').append(data);
                          $('#pagenum').val(nextPage);
                      } else {								 
                          $("#badge_point_loader").hide();
                      }
                    setTimeout(function () {
                         $("#badge_point_loader").hide();
                     }, 1000);
                }
            });
        }
    });
        //---END---badges page script
 
 //---START---review page script
    //Review select sorting
    $(document).on("click", "div.review-sort .sort-option", function() {
        $(this).parents(".sort-options").find(".sort-option").removeClass("selection");
        $(this).addClass("selection");
        $(this).parents(".sort-select").removeClass("opened");
        $(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());
        review_sort = $(this).data("value")

        filterReview(review_sort, review_score, place_id)
    });
    
    
    //Review select sorting
    $(document).on("click", "div.review-score .sort-option", function() {
        $(this).parents(".sort-options").find(".sort-option").removeClass("selection");
        $(this).addClass("selection");
        $(this).parents(".sort-select").removeClass("opened");
        $(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());
        review_score = $(this).data("value")

        filterReview(review_sort, review_score, place_id)
    });
})
  
 $(document).on('mouseover', '.review-block', function () {
            $(this).find('.review-action-block .dropdown').show()
        });

$(document).on('mouseout', '.review-block', function () {
    $(this).find('.review-action-block .dropdown').hide()
});  

   //delete review
    $(document).on('click', '.delete-review', function (e) {
        var _this = $(this);
        var review_id = _this.attr('reviewId');
        var type = 'review';
        var review_count = $('.review-count').html();
       
        $.confirm({
        title: 'Confirm!',
        content: '<div style="font-size:18px;">Are you sure you want to delete this review? <div class="mb-3"></div></div>',
        columnClass: 'col-md-5 col-md-offset-5',
        closeIcon: true,
        offsetTop: 0,
        offsetBottom: 500,
        scrollToPreviousElement:false,
        scrollToPreviousElementAnimate:false,
        buttons: {
            cancel: function () {},
            confirm: {
                text: 'Confirm',
                btnClass: 'btn-danger',
                keys: ['enter', 'shift'],
                action: function(){
                    $.ajax({
                            method: "POST",
                            url: "{{ route('post.delete') }}",
                            data: {post_id: review_id, post_type: type} 
                        })
                        .done(function (res) {
                            result = JSON.parse(res);
                            if(result.status == "yes"){
                                _this.closest('.review-block').fadeOut();
                                 $('.review-count').html(parseInt(review_count) - 1);
                            }
                    });
                }
            }
        }
    });
    e.preventDefault();
    });
    
    
     //Get all reviews
    $(document).on('click', '.clear-review-search', function (e) {
        review_sort = '';
        review_score = 'all';
        place_id = '';
        place_name = '';
         $('div.review-sort').remove();
         $('div.review-score').remove();
        filterOrder('review-sort', 'Newest');
        filterOrder('review-score', 'All');
        
        $('.review-search-result-block').html('')
        
        filterReview(review_sort, review_score, place_id)
    });
    
    
    //edit review
//    $(document).on('click', '.edit-review', function (e) {
//        var review_id = $(this).attr('data-id');
//        $('.reviewEditForm' + review_id).removeClass('d-none')
//        $('.review-text-' + review_id).addClass('d-none')
//        
//        review_textarea_auto_height($('.reviewEditForm' + review_id).find('.review-edit-textarea')[0]);
//    });
    
    //Cancel edit review
    $(document).on('click', '.review-edit-cancel-link', function (e) {
        var review_id = $(this).attr('dataId');
        $('.reviewEditForm' + review_id).addClass('d-none')
        $('.review-text-' + review_id).removeClass('d-none')
    });
    
    //Save edit review
    $(document).on('click', '.review-edit-post-link', function (e) {
        var review_id = $(this).attr('dataId');
        var review_text = $('.reviewEditForm' + review_id).find('.review-edit-textarea').val().trim();
        
        if(review_text == ''){
            return;
        }
        
        $.ajax({
            method: "POST",
            url: '{{route('place.ajax_edit_review')}}/' + review_id,
            data: {text: review_text}
        })
        .done(function (result) {
            if (result.success) {
                $('.reviewEditForm' + review_id).addClass('d-none')
                $('.review-text-' + review_id +' span').html(review_text)
                $('.review-text-' + review_id).removeClass('d-none')
            }
        });
        
    });
    
    
    // Review load more
    $('#review_loader').on('inview', function (event, isInView) {
        if (isInView) {
           loadMoreReview(review_sort, review_score, place_id)
        }
    });
    

     //Review sorting by type
    function filterReview(sort, score, pl_id, pl_name =''){
         var user_id = '{{$user->id}}';
          $('.review-body-block').html('');
          $('#review_pagenum').val(1);
          
          if(pl_name != ''){
              place_name = pl_name;
          }
          if(pl_id != ''){
              place_id = pl_id;
          }
            $.ajax({
                type: 'POST',
                url: '{{url("profile/get-more-review")}}',
                data: {pagenum: 1, user_id:user_id, sort: sort, score:score, place_id:place_id},
                async: false,  
                success: function (data) {
                    if(data.view != ''){							 
                        $('.review-body-block').html(data.view);
                        $('.review-count').html(data.count)
                        $("#review_loader").show();
                        if(place_id !=''){
                            getFilterResult(place_name);
                        }
                      }else{
                        $("#review_loader").hide();
                      }
                }
            });
    }
    
    function loadMoreReview(sort, score, place_id){
         var nextPage = parseInt($('#review_pagenum').val()) + 1;
         var user_id = '{{$user->id}}';
            $.ajax({
                type: 'POST',
                url: '{{url("profile/get-more-review")}}',
                data: {pagenum: nextPage, user_id:user_id, sort: sort, score:score, place_id:place_id},
                async: false,  
                success: function (data) {
                    if(data.view != ''){							 
                          $('.review-body-block').append(data.view);
                          $('#review_pagenum').val(nextPage);
                      } else {								 
                          $("#review_loader").hide();
                      }
                }
            });
    }
    
    function getFilterResult(result){
         $('.review-search-result-block').html('<div class="review-top-bordered">'+
                    'You are viewing reviews for <b dir="auto">'+ result +'</b>'+
                    '<span class="clear-search clear-review-search">'+
                        '<i class="fa fa-times" aria-hidden="true"></i>'+
                    '</span>'+
                '</div>');
    }
    
    
    function review_textarea_auto_height(element) {
        element.style.height = "50px";
        element.style.height = (element.scrollHeight) + "px";
    }
//---END--- review page script
</script>
