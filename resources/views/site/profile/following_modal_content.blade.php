@if($following_lists['all_count'] > 0)
    @foreach($following_lists['following'] as $following)
        @if(get_class($following) == 'App\Models\UsersFollowers\UsersFollowers')
        <div class="following-people">
            <div class="post-comment-row  f-just-space">
                <div class="post-com-avatar-wraps">
                    <img class="follow-avatar" src="{{check_profile_picture(@$following->user->profile_picture)}}" alt="">
                    <div class="">
                        <a href="{{url('profile/'.$following->user->id)}}" class="comment-name profile-follow-link">{{@$following->user->name}}</a>
                    {!! get_exp_icon(@$following->user) !!}
                    </div>
                </div>
                @if(Auth::guard('user')->user()->id != $following->user->id && !user_access_denied($following->user->id))
                    <div class="profile-followes-btn-block f-{{@$following->user->id}}-block">
                        @if(\App\Models\UsersFollowers\UsersFollowers::where('users_id',  @$following->user->id)->where('followers_id', Auth::guard('user')->user()->id)->count()> 0)
                            <a type="button" class="btn btn-light-grey m_button_unfollow" followId="{{@$following->user->id}}">Unfollow</a>
                        @else
                             <a type="button" class="btn btn-light-primary m_button_follow" followId="{{@$following->user->id}}">Follow</a>
                        @endif
                    </div>
                @endif
            </div>
        </div>
        @elseif(get_class($following) == 'App\Models\Country\CountriesFollowers')
            <div class="following-countries">
                <div class="post-comment-row  f-just-space">
                    <div class="post-com-avatar-wraps">
                        <img class="follow-avatar" src="{{get_country_flag($following->country)}}" alt="">
                        <div class="fol-info-block">
                    &nbsp;&nbsp;&nbsp;     <a href="{{url('country/'.@$following->country->id)}}" class="comment-name profile-follow-link">{{@$following->country->transsingle->title}}</a>
                            <span class="fol-info-subtitle">Country</span>
                        </div>
                    </div>
                    <div class="profile-followes-btn-block countris-dest-{{@$following->country->id}}-block">
                        @if(\App\Models\Country\CountriesFollowers::where('countries_id', @$following->countries_id)->where('users_id', Auth::guard('user')->user()->id)->count()> 0)
                            <a type="button" class="btn btn-light-grey dest_button_unfollow" followtype="countries" followId="{{@$following->countries_id}}">Unfollow</a>
                        @else
                            <a type="button" class="btn btn-light-primary dest_button_follow" followtype="countries" followId="{{@$following->countries_id}}">Follow</a>
                        @endif
                    </div>
                </div>
            </div>
        @elseif(get_class($following) == 'App\Models\City\CitiesFollowers')
            <div class="following-cities">
                <div class="post-comment-row  f-just-space">
                    <div class="post-com-avatar-wraps">
                        <img class="follow-avatar" src="{{@check_city_photo( $following->city->getMedias[0]->url, 700)}}" alt="">
                        <div class="fol-info-block">
                            <a href="{{url('city/'.$following->city->id)}}" class="comment-name profile-follow-link">{{@$following->city->transsingle->title}}</a>
                            <span class="fol-info-subtitle">City in {{$following->city->country->transsingle->title}}</span>
                        </div>
                    </div>
                    <div class="profile-followes-btn-block cities-dest-{{@$following->city->id}}-block">
                        @if(\App\Models\City\CitiesFollowers::where('cities_id', @$following->cities_id)->where('users_id', Auth::guard('user')->user()->id)->count()> 0)
                            <a type="button" class="btn btn-light-grey dest_button_unfollow" followtype="cities" followId="{{@$following->city->id}}">Unfollow</a>
                        @else
                            <a type="button" class="btn btn-light-primary dest_button_follow" followtype="cities" followId="{{@$following->city->id}}">Follow</a>
                        @endif
                    </div>
                </div>
            </div>
        @elseif(get_class($following) == 'App\Models\Place\PlaceFollowers')
            <div class="following-places">
                <div class="post-comment-row  f-just-space">
                    <div class="post-com-avatar-wraps">
                        <img class="follow-avatar" src="{{@check_place_photo($following->place->getMedias[0]->url)}}" alt="">
                        <div class="fol-info-block">
                        &nbsp;&nbsp;&nbsp;<a href="{{url('place/'.$following->place->id)}}" class="comment-name profile-follow-link">{{@$following->place->transsingle->title}}</a>
                            <span class="fol-info-subtitle">{{$following->place->place_type?@explode(",", $following->place->place_type)[0]:'Place'}} in {{@$following->place->city->transsingle->title}}, {{@$following->place->country->transsingle->title}}</span>
                        </div>
                    </div>
                    <div class="profile-followes-btn-block places-dest-{{@$following->place->id}}-block">
                        @if(\App\Models\Place\PlaceFollowers::where('places_id', @$following->places_id)->where('users_id', Auth::guard('user')->user()->id)->count()> 0)
                            <a type="button" class="btn btn-light-grey dest_button_unfollow" followtype="places" followId="{{@$following->place->id}}">Unfollow</a>
                        @else
                            <a type="button" class="btn btn-light-primary dest_button_follow" followtype="places" followId="{{@$following->place->id}}">Follow</a>
                        @endif
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endif
