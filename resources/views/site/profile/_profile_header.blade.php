<?php
    $checkFollow = isset($user) ? check_follow($user->id) : null; ?>
<div class="top-banner-wrap">
    @if (session()->get('flash_success'))
        <div class="alert alert-success profile-alert">
            @if(is_array(json_decode(session()->get('flash_success'), true)))
                {!! implode('', session()->get('flash_success')->all(':message<br/>')) !!}
            @else
                {!! session()->get('flash_success') !!}
            @endif
        </div>
    @endif
    <div class="top-banner-block o-visible" id="coverImage" 
         @if(isset($user) && isset($user->cover_photo))
         style="background-image:url('{{$user->cover_photo}}')"
         @else
         style="background-image:url('{{asset('assets2/image/cover-bg.png')}}'); background-size:100% 100%;"
         @endif data-original_image="{{get_profile_original_picture($user->cover_photo, $user->id, 'cover')}}">
        <span id="coverPreloader" class="cover-preloader">
            <img src='{{asset('assets2/image/preloader.gif')}}' width='50px'/>
        </span>
         @if($login_user_id != '')
         <form>
             <ul class="cover-img-action" style="display: none;">
                <li>
                    <a type="button" class="btn btn-light button-cover-add" id="button_cover">
                        <i class="fa fa-camera click-target" aria-hidden="true"></i> {{(isset($user) && isset($user->cover_photo))?'Edit cover':'Add cover'}}</a>
                </li>
                @if(isset($user) && isset($user->cover_photo))
                    <li class="flexed del-action"><span class="cover-btn-border"></span></li>
                    <li class="del-action">
                        <a type="button" class="btn btn-light button-cover-delete" id="button_cover_delete">Delete</a>
                    </li>
                @endif
            </ul>
        </form>
        
            <ul class="nav nav-tabs expert-top-filter" role="tablist">
                @if($user->expert == 1)
                    <li class="nav-item">
                      <a class="nav-link" href="{{url('dashboard')}}">Dashboard</a>
                    </li>
                @endif
                <li class="nav-item">
                <a class="nav-link" href="{{url('chat')}}">Inbox</a>
                </li>
<!--                <li class="nav-item">
                <a class="nav-link" href="{{url('notifications')}}">Notifications</a>
                </li>-->
                <li class="nav-item">
                <a class="nav-link" href="{{url('settings')}}">Settings</a>
                </li>
            </ul>
        @endif
        <div class="top-banner-text expert-variant">
            <div class="place-info-block">
                <div class="expert-top">
                    <div class="avatar-layer">
                            @if($login_user_id != '')
                            <div class="profile-ava-block">
                                <img src="{{check_profile_picture($user->profile_picture)}}" onerror="this.src='{{asset('assets2/image/placeholders/male.png')}}'" width="146px" height="146px" alt="" class="avatar" id="avatar_image" data-original_image="{{get_profile_original_picture($user->profile_picture, $user->id, 'profile')}}">
                                <a class="edit-ava-link" id="button_profilepicture">
                                        <img src="{{asset('assets2/image/ava-edit-icon.png')}}" alt="">
                                </a>
                            </div>
                            @else
                            <div class="prof-ava-block">
                                <img src="{{check_profile_picture($user->profile_picture)}}" onerror="this.src='{{asset('assets2/image/placeholders/male.png')}}'" width="146px" height="146px" alt="" class="avatar" id="">
                            </div>
                            @endif
                       
                    </div>
                  <div class="expert-info">
                      <div class="name-title profile-name-title" style="font-size:{{calculate_charackter_font_size($user->name, 'header')}}px;">
                          @if(isset($user->country_of_nationality->iso_code))<img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($user->country_of_nationality->iso_code)}}.png" alt="flag" style="width:40px;height:26px;margin-bottom: 7px;"> @endif
                          {{$user->name}} <span class="username-wrap">{{$user->username?'(@'.$user->username.')':''}}</span>
                      </div>
                       @php
                            if($user->expert != 1 ){
                                if($login_user_id != '')
                                    $check_exp = true;
                                else
                                    $check_exp = false;
                            }else{
                             $check_exp = true;
                            }
                        @endphp
                        @if($check_exp)
                            <div class="expert-title profile-exp-content {{count($expertise)>0?'':'d-none'}}">
                                    <div class="profile-expertise-content">
                                        <div class="exp-inner"><b>Expert in:</b> </div>
                                        @php $num_output = 0; @endphp
                                        <div class="profile-sup-layer profile-exp-block">
                                            @foreach($expertise as $val)
                                                @if($loop->index < 10)
                                                    <strong>{{$val['text']}}</strong>

                                                    <sup class="profile-sup-text">
                                                        {{optimize_counter((session()->has('expertise_count') && session()->get('expertise_count')[$val['text']])?session()->get('expertise_count')[$val['text']]:0)}}
                                                    </sup>
                                                    <span class="profile-sup-seprate">
                                                    @if($loop->index < 9)
                                                        @if($loop->index != $loop->count-2 && !$loop->last),@endif
                                                        @if($loop->index == $loop->count-2) and @endif
                                                    @endif
                                                     </span>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                            </div>  
                       @endif

                            <div class="profile-expertise-content profile-tstyle-content pt-1 {{count($user->travelstyles) > 0?'':'d-none'}}">
                                    <div class="">
                                        <span><b>Travel Styles:</b></span>
                                        <span class="profile-trstyle-block">
                                            @foreach($user->travelstyles AS $uts)

                                            {{$uts->travelstyle->trans[0]->title}}
                                             @if($loop->index != $loop->count-2 && !$loop->last),@endif
                                             @if($loop->index == $loop->count-2) & @endif
                                            @endforeach
                                        </span>
                                    </div>
                            </div>
                    </div>
                </div>
            </div>
             @if($login_user_id == '')
                 @php
                 $message_me = true;

                 if(Auth::check() && isset($user->privacy->message_me) && $user->privacy->message_me != 0){
                     if(!is_friend($user->id) && $user->privacy->message_me == 1){
                         $message_me = false;
                     }

                    if(!$checkFollow && $user->privacy->message_me == 2){
                         $message_me = false;
                    }
                 }

                 @endphp
             <div class="banner-comment dropdown" id="follow_user_button">
                 @if(Auth::check())
                 @if($message_me)
                 <a type="button" class="btn btn-light-primary button-send-message btn-bordered" data-dismiss="modal" data-image="{{check_profile_picture($user->profile_picture)}}" data-name="{{$user->name}}" data-id="{{$user->id}}" data-toggle="modal" data-target="#messageToUser">
                     <i class="fa fa-envelope" aria-hidden="true"></i> <span>Message</span>
                 </a>
                 @endif

                 <a type="button" class="btn {{$checkFollow?'btn-light-primary button_follow':'btn-light-grey button_unfollow'}} btn-bordered btn-icon-side f-user-link"  id="button_follow"><i class="trav-user-plus-icon"></i> <span class="f-link-title">{{$checkFollow?__('profile.follow'):__('profile.unfollow')}}</span></a>
                 <a class="icon-wrap f-list-button {{$checkFollow?'f-button-primary':'f-button-grey'}}" type="button" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false"><i class="trav-view-plan-icon"></i></a>
                
                 <div class="dropdown-menu dropdown-menu-right dropdown-arrow f-dropdown-menu" posttype="Profile" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                    <a  class="dropdown-item f-drop-link {{$checkFollow?'button_follow':'button_unfollow'}}" >
                        <span class="icon-wrap f-drop-icon-wrap">
                            @if($checkFollow)
                                <i class="trav-user-plus-icon"></i>
                            @else
                                <i class="trav-user-minus"></i>
                            @endif
                        </span>
                        <div class="drop-txt">
                            <p><b><span class="f-link-title">{{$checkFollow?__('profile.follow'):__('profile.unfollow')}}</span> {{strtok($user->name, ' ')}}</b></p>
                            <p>{{$checkFollow?'Start':'Stop'}} seeing posts from {{strtok($user->name, ' ')}}</p>
                        </div>
                    </a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#blockusermodal">
                        <span class="icon-wrap">
                            <i class="trav-user-block-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Block {{strtok($user->name, ' ')}}</b></p>
                            <p>Block {{strtok($user->name, ' ')}} from your account</p>
                        </div>
                    </a>
                    <a class="dropdown-item prof-copy-link" data-link="{{url('profile', $user->id)}}" >
                        <span class="icon-wrap">
                            <i class="trav-link"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Copy Link</b></p>
                            <p>Paste the link anywhere you want</p>
                        </div>
                    </a>
                    <span class="f-mid-line"></span>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$user->id}},this)">
                        <span class="icon-wrap">
                            <i class="trav-flag-icon-o"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Report user</b></p>
                            <p>Help us understand</p>
                        </div>
                    </a>
                </div>
                 @else
                     <a type="button" class="btn btn-light-primary open-login btn-bordered button-send-message">
                         <i class="fa fa-envelope" aria-hidden="true"></i> <span>Message</span>
                     </a>
                     <a type="button" class="btn btn-light-primary btn-bordered btn-icon-side button_follow open-login f-user-link" ><i class="trav-user-plus-icon"></i> <span class="f-link-title">@lang('profile.follow')</span></a>
                     <a class="icon-wrap open-login f-list-button f-button-primary" type="button"><i class="trav-view-plan-icon"></i></a>
                 @endif
            </div>
            @endif
            @php
            $following_lists = $user->getFollowingList('all', 0);
            @endphp
            <div class="mobile-post-profile-info">
                @if(isset($user_id))
                    <a href="{{url('profile-posts/'.$user_id)}}" class="mobile-post-profile-info-item">
                        @else
                            <a href="{{route('profile.posts')}}" class="mobile-post-profile-info-item">
                                @endif
                    <div class="mobile-post-profile-info-item-count">{{count($user->posts)}}</div>
                    <div class="mobile-post-profile-info-item-title">Posts</div>
                </a>
                <div class="mobile-post-profile-info-item" data-toggle="modal" data-target="#mefollowers">
                    <div class="mobile-post-profile-info-item-count">{{count($user->get_followers)}}</div>
                    <div class="mobile-post-profile-info-item-title">Followers</div>
                </div>
                <div class="mobile-post-profile-info-item" data-toggle="modal" data-target="#mefollowing">
                    <div class="mobile-post-profile-info-item-count">{{$following_lists['all_count']}}</div>
                    <div class="mobile-post-profile-info-item-title">Following</div>
                </div>
                @if($login_user_id == '')
                <div class="mobile-post-profile-info-item">
             <div class="banner-comment banner-comment-mobile dropdown" id="follow_user_button">
                 @if(Auth::check())
                 <a type="button" class="btn {{$checkFollow?'btn-light-primary button_follow':'btn-light-grey button_unfollow'}} btn-bordered btn-icon-side f-user-link"  id="button_follow"> <span class="f-link-title">{{$checkFollow?__('profile.follow'):__('profile.unfollow')}}</span></a>
                 <a class="icon-wrap f-list-button {{$checkFollow?'f-button-primary':'f-button-grey'}}" type="button" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false"><i class="trav-view-plan-icon"></i></a>
                
                 <div class="dropdown-menu dropdown-menu-right dropdown-arrow f-dropdown-menu" posttype="Profile" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                    <a  class="dropdown-item f-drop-link {{$checkFollow?'button_follow':'button_unfollow'}}" >
                        <span class="icon-wrap f-drop-icon-wrap">
                            @if($checkFollow)
                                <i class="trav-user-plus-icon"></i>
                            @else
                                <i class="trav-user-minus"></i>
                            @endif
                        </span>
                        <div class="drop-txt">
                            <p><b><span class="f-link-title">{{$checkFollow?__('profile.follow'):__('profile.unfollow')}}</span> {{strtok($user->name, ' ')}}</b></p>
                            <p>{{$checkFollow?'Start':'Stop'}} seeing posts from {{strtok($user->name, ' ')}}</p>
                        </div>
                    </a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#blockusermodal">
                        <span class="icon-wrap">
                            <i class="trav-user-block-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Block {{strtok($user->name, ' ')}}</b></p>
                            <p>Block {{strtok($user->name, ' ')}} from your account</p>
                        </div>
                    </a>
                    <a class="dropdown-item prof-copy-link" data-link="{{url('profile', $user->id)}}" >
                        <span class="icon-wrap">
                            <i class="trav-link"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Copy Link</b></p>
                            <p>Paste the link anywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$user->id}},this)">
                        <span class="icon-wrap">
                            <i class="trav-flag-icon-o"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Report user</b></p>
                            <p>Help us understand</p>
                        </div>
                    </a>
                </div>
                     @else
                     <a type="button" class="btn btn-light-primary button_follow btn-bordered btn-icon-side f-user-link open-login"> <span class="f-link-title">@lang('profile.follow')</span></a>
                     <a class="icon-wrap f-list-button f-button-primary open-login" type="button"><i class="trav-view-plan-icon"></i></a>
                 @endif
            </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@if(Auth::check())
<div class="modal fade profile-folloowers-modal" tabindex="-1" role="dialog" id="mefollowers" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center">Followers</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="post-comment-wrapper  profile-follow-block p-followers-block">
                <div class="profile-followers-wrap">
                    @include('site.profile.follower_modal_content', [
                        'get_followers'=>$user->get_followers()->with('follower:id,name,profile_picture')->take(10)->get()])
                </div>
                <div id="loadMoreFollowers"  class="load-more-followers-link-user {{count($user->get_followers) <10?'d-none':''}}">
                    <input type="hidden" id="followers_pagenum" value="1">
                    <div class="rep-like-users-animation-content people-row">
                        <div class="com-like-avatar-wrap" id="hide_followers_loader">
                            <div class="like-top-avatar-wrap animation"></div>
                            <div class="like-top-name animation"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

<div class="modal fade profile-folloowers-modal" tabindex="-1" role="dialog" id="mefollowing" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <ul class="nav nav-tabs bar-list following-modal-header" role="tablist" dir="auto">
                <li class="nav-item" dir="auto">
                    <a class="nav-link active" id="allFollowing" data-toggle="tab" href="" role="tab" data-type="all" dir="auto">All <span class="fol-count all-count">{{$following_lists['all_count']}}</span></a>
                </li>
                <li class="nav-item" dir="auto">
                    <a class="nav-link" id="peopleFollowing" href="" role="tab" data-type="people" dir="auto">People <span class="fol-count people-count">{{$following_lists['peoples_count']}}</span></a>
                </li>
                <li class="nav-item" dir="auto">
                    <a class="nav-link" id="countriesFollowing" href="" role="tab" data-type="countries" dir="auto">Countries <span class="fol-count countries-count">{{$following_lists['countries_count']}}</span></a>
                </li>
                <li class="nav-item" dir="auto">
                    <a class="nav-link" id="citiesFolowing" href="" role="tab" data-type="cities" dir="auto">Cities <span class="fol-count cities-count">{{$following_lists['cities_count']}}</span></a>
                </li>
                <li class="nav-item" dir="auto">
                    <a class="nav-link" id="placesFollowing" href="" role="tab" data-type="places" dir="auto">Places <span class="fol-count places-count">{{$following_lists['places_count']}}</span></a>
                </li>
            </ul>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body pr-2">
            <div class="post-comment-wrapper profile-follow-block p-following-block">
                <div class="profile-following-wrap">
                     @include('site.profile.following_modal_content', ['following_lists'=>$following_lists])
                </div>
                <div class="not-found-block {{$following_lists['all_count'] != 0?'d-none':''}}">
                    <p>Data not found...</p>
                </div>
                <div id="loadMoreFollowing" class="load-more-following-link-user {{$following_lists['all_count'] <20?'d-none':''}}">
                    <input type="hidden" id="following_pagenum" value="1">
                    <div class="rep-like-users-animation-content people-row">
                        <div class="com-like-avatar-wrap" id="hide_following_loader">
                            <div class="like-top-avatar-wrap animation"></div>
                            <div class="like-top-name animation"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

<div class="modal fade modal-profile white-style" tabindex="-1" role="dialog" id="blockusermodal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-center">
            <div class="modal-profile-header">
                <button type="button" class="modal-profile-close" data-dismiss="modal" aria-label="Close"></button>
                <h3 class="modal-profile-title">Block User</h3>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to block {{$user->name}}?
                You can unblock users at any time from your account settings.</p>
            </div>
            <div class="modal-profile-footer">
                <button type="button" class="button" data-dismiss="modal">Cancel</button>
                <button type="button" class="button button-danger-full block-user" data-id="{{$user->id}}">Block</button>
            </div>
        </div>
    </div>
</div>

<!-- Message to user popup -->
<div class="modal fade msg-to-user-popup" id="messageToUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-650" role="document" >
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                <div class="top-title-layer" >
                    <h3 class="title" >
                        Message
                        <img class="msg-to" src="" alt="">
                        <span class="txt">Profile</span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="post-create-input">
                    <textarea name="" id="user_message_text" cols="30" rows="10" placeholder="Type a message..."></textarea>
                </div>
                <div class="post-create-controls">
                    <div class="ml-auto post-buttons">
                        <input type="hidden" id="participans_id" value="">
                        <button type="button" class="btn btn-light-grey btn-bordered mr-3" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-light-primary btn-bordered send-user-message" >Send</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@include('site.profile.template._menu-mobile')

<script>
    var checkFollowingOut = true;
    var checkFollowersOut = true;
    var following_type = 'all';
    var page_numbers = {
        'all': 1,
        'people': 1,
        'countries': 1,
        'cities': 1,
        'places': 1
    }

    $(".button-send-message").on('click', function(e) {
        @if(Auth::check())
            $('#messageToUser').modal('toggle', $(this));
        @endif
    });

    $('#messageToUser').on('show.bs.modal',function (event){
        var button = $(event.relatedTarget)

        if(event.relatedTarget){
            id =  button.attr('data-id');
            name = button.attr('data-name');
            img =  button.attr('data-image');
            $('.msg-to').attr('src',img);
            $('.txt').text(name);
            $('#participans_id').val(id)
        }

    });

    $('.send-user-message').on('click',function(){
        var message = $('#user_message_text').val();

        var participants = $('#participans_id').val();
        $.ajax({
            method: "POST",
            url: './../../chat/send',
            data: {chat_conversation_id: 0,participants:participants,message:message},
            success:function(e){
                toastr.success('Your message has been sent');
                $('#messageToUser').modal('hide');
                $('#user_message_text').empty();
                $('#user_message_text').val('');
            }
        });
    });

    $(document).ready(function () {
    // copy profile link  
    $(document).on( 'click', '.prof-copy-link', function(){
        var copy_link = $(this).attr('data-link');
        var _temp = $("<input>");
        $("body").append(_temp);
        _temp.val(copy_link).select();
         document.execCommand("copy");
         _temp.remove()
       
        toastr.success('The link copied!');
    });
    
    // load more following   
    $(".p-following-block").on( 'scroll', function(){
         if(checkFollowingOut){
            if (visibleY(document.getElementById('hide_following_loader'))) {
                loadMoreFollowing();
                checkFollowingOut = false;
            } 
         }
    });
    
    // load more followers   
    $(".p-followers-block").on( 'scroll', function(){
         if(checkFollowersOut){
            if (visibleY(document.getElementById('hide_followers_loader'))) {
                loadMoreFollow();
                checkFollowersOut = false;
            } 
         }
    });

    //Following list tab script
    $(document).on('click', '.following-modal-header li a', function (e) {
        e.preventDefault();
       var id = $(this).attr('id')
        $('.following-modal-header li a').removeClass('active')
        $(this).addClass('active')

        $('#following_pagenum').val(1)

        switch (id){
           case 'allFollowing':
               $('.profile-following-wrap>div').removeClass('d-none');
               following_type = 'all';

               if( $('#allFollowing').find('.all-count').html() > 40){
                   $('#loadMoreFollowing').removeClass('d-none')
               }else{
                   $('#loadMoreFollowing').addClass('d-none')
               }

               if($('.profile-following-wrap>div').length == 0)
                   $('.not-found-block').removeClass('d-none')
               else
                   $('.not-found-block').addClass('d-none')
               break;
            case 'peopleFollowing':
                $('.profile-following-wrap>div').addClass('d-none');
                $('.profile-following-wrap div.following-people').removeClass('d-none');
                following_type = 'people';

                if( $('#peopleFollowing').find('.people-count').html() > 10){
                    $('#loadMoreFollowing').removeClass('d-none')
                    checkFollowingOut = true
                }else{
                    $('#loadMoreFollowing').addClass('d-none')
                }

                if($('.profile-following-wrap div.following-people').length == 0)
                    $('.not-found-block').removeClass('d-none')
                else
                    $('.not-found-block').addClass('d-none')
                break;
            case 'countriesFollowing':
                $('.profile-following-wrap>div').addClass('d-none');
                $('.profile-following-wrap div.following-countries').removeClass('d-none');
                following_type = 'countries';

                if( $('#countriesFollowing').find('.countries-count').html() > 10){
                    $('#loadMoreFollowing').removeClass('d-none')
                    checkFollowingOut = true
                }else{
                    $('#loadMoreFollowing').addClass('d-none')
                }

                if($('.profile-following-wrap div.following-countries').length == 0)
                    $('.not-found-block').removeClass('d-none')
                else
                    $('.not-found-block').addClass('d-none')
                break;
            case 'citiesFolowing':
                $('.profile-following-wrap>div').addClass('d-none');
                $('.profile-following-wrap div.following-cities').removeClass('d-none');
                following_type = 'cities';

                if( $('#citiesFolowing').find('.cities-count').html() > 10){
                    $('#loadMoreFollowing').removeClass('d-none')
                    checkFollowingOut = true
                }else{
                    $('#loadMoreFollowing').addClass('d-none')
                }

                if($('.profile-following-wrap div.following-cities').length == 0)
                    $('.not-found-block').removeClass('d-none')
                else
                    $('.not-found-block').addClass('d-none')
                break;
            case 'placesFollowing':
                $('.profile-following-wrap>div').addClass('d-none');
                $('.profile-following-wrap div.following-places').removeClass('d-none');
                following_type = 'places';

                if( $('#placesFollowing').find('.places-count').html() > 10){
                    checkFollowingOut = true
                    $('#loadMoreFollowing').removeClass('d-none')
                }else{
                    $('#loadMoreFollowing').addClass('d-none')
                }

                if($('.profile-following-wrap div.following-places').length == 0)
                    $('.not-found-block').removeClass('d-none')
                else
                    $('.not-found-block').addClass('d-none')
                break;
        }

    })
    
        
    //profile avatar edit icon show/hide
    $('body').on('mouseover', '.profile-ava-block', function () {
       $(this).find('.edit-ava-link').show()
    });

    $('body').on('mouseout', '.profile-ava-block', function () {
       $(this).find('.edit-ava-link').hide()
    });
        
        $('div.profile-alert').delay(3000).slideUp(300);
        
         $('body').on('click', '.show-more', function () {
             $(this).addClass('d-none');
             $(this).parent('div').find('.more-links').removeClass('d-none');
             
         })
         
         $('body').on('click', '.visit-show-more', function () {
             $(this).addClass('d-none');
             $('.visit-more-links').removeClass('d-none');
             
         })
        $.ajax({
            method: "POST",
            url: "{{ route('profile.check-follow', $user->id) }}",
            data: {name: "test"}
        })
                .done(function (res) {
                    if (res.success == true) {
                        $('#aside_follow_user_button button').addClass('button_unfollow')
                        $('#aside_follow_user_button span').html('@lang("profile.unfollow")')
                    } else if (res.success == false) {
                        $('#aside_follow_user_button button').addClass('button_follow')
                        $('#aside_follow_user_button span').html('@lang("profile.follow")')
                    }
                });
                

        $('body').on('click', '.button_follow', function () {
            $.ajax({
                method: "POST",
                url: "{{ route('profile.follow', $user->id) }}",
                data: {name: "test"}
            })
                    .done(function (res) {
                        if (res.success == true) {
                            $('#aside_follow_user_button button, .f-drop-link, .f-user-link').removeClass('button_follow').addClass('button_unfollow')
                            $('#aside_follow_user_button span').html('@lang("profile.unfollow")')
                            
                            $('.f-user-link').removeClass('btn-light-primary').addClass('btn-light-grey')
                            $('.f-list-button').removeClass('f-button-primary').addClass('f-button-grey')
                            $('.f-link-title').html('@lang("profile.unfollow")')
                            $('.f-drop-icon-wrap').html('<i class="trav-user-minus"></i>')
                            
                            check_followdata();
                            $('.followers-count').html(parseInt($('.followers-count').html()) + 1)
                        }
                    });
        });

        $('body').on('click', '.button_unfollow', function () {
        var fwhere = 'profile';
            $.ajax({
                method: "POST",
                url: "{{  route('profile.unfolllow', $user->id)}}",
                data: {name: "test", fwhere: fwhere}
            })
                    .done(function (res) {
                        if (res.success == true) {
                            $('#aside_follow_user_button button, .f-drop-link, .f-user-link').removeClass('button_unfollow').addClass('button_follow')
                            $('#aside_follow_user_button span').html('@lang("profile.follow")')
                            
                            $('.f-user-link').removeClass('btn-light-grey').addClass('btn-light-primary')
                            $('.f-list-button').removeClass('f-button-grey').addClass('f-button-primary')
                            $('.f-link-title').html('@lang("profile.follow")')
                            $('.f-drop-icon-wrap').html('<i class="trav-user-plus-icon"></i>')
                            
                            check_followdata();
                             $('.followers-count').html(parseInt($('.followers-count').html()) - 1)
                        }
                    });
        });
        
        //Blocking user
        $('body').on('click', '.block-user', function () {
        console.log('sss', $(this).attr('data-id'))
        var user_id = $(this).attr('data-id');
            $.ajax({
                method: "POST",
                url: "{{  route('setting.blocking_users')}}",
                data: {blocked_user: user_id, is_ajax:true}
            })
                    .done(function (res) {
                        if (res.success == true) {
                            window.location.href = "{{url('/')}}"
                        }
                    });
        });
        
        //Modal follow -button
        $('body').on('click', '.m_button_follow', function () {
        var user_id = $(this).attr('followId')
        var url = "{{ route('profile.follow', ':userId') }}";
         url = url.replace(':userId', user_id);
            $.ajax({
                method: "POST",
                url: url,
                data: {name: "test"}
            })
                    .done(function (res) {
                        if (res.success == true) {
                            $('.f-'+user_id+'-block').html('<a type="button" class="btn btn-light-grey m_button_unfollow" followId="'+ user_id +'">Unfollow</a>');
                            $('.following-count').html(parseInt($('.following-count').html()) + 1)
                        }
                    });
        });

        $('body').on('click', '.m_button_unfollow', function () {
            var user_id = $(this).attr('followId')
            var url = "{{ route('profile.unfolllow', ':userId') }}";
            url = url.replace(':userId', user_id);
         
            $.ajax({
                method: "POST",
                url: url,
                data: {name: "test"}
            })
                    .done(function (res) {
                        if (res.success == true) {
                            $('.f-'+user_id+'-block').html(' <a type="button" class="btn btn-light-primary m_button_follow" followId="'+ user_id +'">Follow</a>');
                            $('.following-count').html(parseInt($('.following-count').html()) - 1)
                        } 
                    });
        });

        //Modal destination follow - button
        $('body').on('click', '.dest_button_follow', function () {
            var location_id = $(this).attr('followId')
            var location_type = $(this).attr('followtype')
            if(location_type == 'countries'){
                var url = "{{ route('country.follow', ':locationId') }}";
            }else if(location_type == 'cities'){
                var url = "{{ route('city.follow', ':locationId') }}";
            }else{
                var url = "{{ route('place.follow', ':locationId') }}";
            }

            url = url.replace(':locationId', location_id);

            $.ajax({
                method: "POST",
                url: url,
                data: {name: "test"}
            })
                .done(function (res) {
                    if (res.success == true) {
                        $('.'+ location_type +'-dest-'+ location_id +'-block').html('<a type="button" class="btn btn-light-grey dest_button_unfollow" followtype="'+ location_type +'" followId="'+ location_id +'">Unfollow</a>');
                    }
                });
        });

        //Modal destination unfollow -button
        $('body').on('click', '.dest_button_unfollow', function () {
            var location_id = $(this).attr('followId')
            var location_type = $(this).attr('followtype')
            if(location_type == 'countries'){
                var url = "{{ route('country.unfollow', ':locationId') }}";
            }else if(location_type == 'cities'){
                var url = "{{ route('city.unfollow', ':locationId') }}";
            }else{
                var url = "{{ route('place.unfollow', ':locationId') }}";
            }

            url = url.replace(':locationId', location_id);

            $.ajax({
                method: "POST",
                url: url,
                data: {name: "test"}
            })
                .done(function (res) {
                    if (res.success == true) {
                        $('.'+ location_type +'-dest-'+ location_id +'-block').html('<a type="button" class="btn btn-light-primary dest_button_follow" followtype="'+ location_type +'" followId="'+ location_id +'">Follow</a>');
                    }
                });
        });
        

        //Profile photo cropping script
        $('body').on('click', '#button_profilepicture', function (e) {
            if($('#avatar_image').data('original_image') != ''){
            var image_path = $('#avatar_image').data('original_image');
                showProfileCroppieModal(image_path, '', "{{url('profile/upload_profile_image')}}", "{{asset('assets2/image/preloader.gif')}}");
            }else{
                showProfileCroppieModal('', "{{asset('assets2/image/placeholders/male.png')}}",  "{{url('profile/upload_profile_image')}}", "{{asset('assets2/image/preloader.gif')}}");
            }
            e.preventDefault();
        });
        
        $('body').on('click', '#delete_profile_photo', function () {
            var is_image = $(this).attr('data-profile_image')
            if(is_image != ''){
                $.confirm({
                    title: 'Confirm!',
                    content: '<div class="profile-confirm-text">@lang('profile.image_confirm_alert') <div class="mb-3"></div></div>',
                    columnClass: 'col-md-5 col-md-offset-5',
                    closeIcon: true,
                    offsetTop: 0,
                    offsetBottom: 500,
                    scrollToPreviousElement:false,
                    scrollToPreviousElementAnimate:false,
                    buttons: {
                        cancel: function () {},
                        confirm: {
                            text: 'Confirm',
                            btnClass: 'btn-danger',
                            keys: ['enter', 'shift'],
                            action: function(){
                                $.ajax({
                                    url: "{{url('profile/remove-profile-image')}}",
                                    type: "POST",
                                    beforeSend: function ()
                                    {
                                        $('#cropProfileImagePop').modal('hide');
                                        $("#coverPreloader").show();
                                    },
                                    success: function (data)
                                    {
                                        $("#coverPreloader").hide();

                                    },
                                    error: function (e)
                                    {
                                        $("#err").html(e).fadeIn();
                                    }
                                })
                            }
                        }
                    }
                });
            }
        });
        

        $("#input_profilepicture").on('change', (function (e) {
           var image_input_id = this.id;
            var selectedImg = $(this)[0];
            readProfileCroppingFile(image_input_id, selectedImg, "{{url('profile/upload_profile_image')}}", 'photo');
        }));



    //pofile cover cropping script
        $('body').on('mouseover', '#coverImage', function () {
            $('.cover-img-action').show();
        });

        $('body').on('mouseout', '#coverImage', function () {
            $('.cover-img-action').hide();
        });

        $('body').on('click', '#button_cover', function (e) {

            if ($('#coverImage').attr('data-original_image') != '') {
                var image_path = $('#coverImage').attr('data-original_image');

                showCoverCroppieModal(image_path, '', "{{url('profile/upload_cover')}}", "{{asset('assets2/image/preloader.gif')}}");
            } else{
                showCoverCroppieModal('', "{{asset('assets2/image/cover-bg.png')}}", "{{url('profile/upload_cover')}}", "{{asset('assets2/image/preloader.gif')}}");
            }
            e.preventDefault();
        });

        $('body').on('click', '#button_cover_delete, #delete_profile_cover', function () {
            var is_image = $(this).attr('data-cover_image');
            if(is_image != ''){
                $.confirm({
                    title: 'Confirm!',
                    content: '<div class="profile-confirm-text">Are you sure delete cover photo? <div class="mb-3"></div></div>',
                    columnClass: 'col-md-5 col-md-offset-5',
                    closeIcon: true,
                    offsetTop: 0,
                    offsetBottom: 500,
                    scrollToPreviousElement:false,
                    scrollToPreviousElementAnimate:false,
                    buttons: {
                        cancel: function () {},
                        confirm: {
                            text: 'Confirm',
                            btnClass: 'btn-danger',
                            keys: ['enter', 'shift'],
                            action: function(){
                                $.ajax({
                                    url: "{{url('profile/deletecover')}}",
                                    type: "POST",
                                    beforeSend: function ()
                                    {
                                        $("#coverPreloader").show();
                                        $('#cropCoverImagePop').modal('hide');
                                        $("#err").fadeOut();

                                    },
                                    success: function (data)
                                    {
                                        $("#coverPreloader").hide();
                                        $('#coverImage').css('background-image', 'url("{{asset('assets2/image/cover-bg.png')}}")')
                                        $('#coverImage').attr('data-original_image',  '')
                                        $('.ava-cover').attr('src',  '{{asset('assets2/image/placeholders/pattern.png')}}')

                                        $('.cover-img-action').find('li.del-action').remove()
                                        $('.cover-img-action #button_cover').html('<i class="fa fa-camera click-target" aria-hidden="true"></i> Add cover')
                                    },
                                    error: function (e)
                                    {
                                        $("#err").html(e).fadeIn();
                                    }
                                })
                            }
                        }
                    }
                });
            }
        });

        $("#input_cover").on('change', (function (e) {
            var image_input_id = this.id;
            var selectedImg = $(this)[0];
            readProfileCroppingFile(image_input_id, selectedImg, "{{url('profile/upload_cover')}}", 'cover');
        }));
    });
    
    function check_followdata()
    {
        $.ajax({
                method: "POST",
                url: "{{  route('profile.checkcontent', $user->id)}}",
                data: {name: "count"}
            })
            .done(function (res) {
                    $('.follow-block-info').html(res);
            });
            $.ajax({
                method: "POST",
                url: "{{  route('profile.checkcontent', $user->id)}}",
                data: {name: "content"}
            })
            .done(function (res) {
                    $('#mefollowers').find('.post-comment-wrapper').html(res);
            });
    }
    
    function loadMoreFollow(){

        var nextPage = parseInt($('#followers_pagenum').val())+1;
        var url = '{{route("profile.load_more_follow")}}';
        var user_id = '{{$user_id}}';
            
        $.ajax({
            type: 'POST',
            url: url,
            data: { pagenum: nextPage, user_id:user_id },
            success: function(data){
                    if(data != ''){	
                        checkFollowersOut = true;
                       $('.profile-followers-wrap').append(data);
                       $('#followers_pagenum').val(nextPage);
                    } else {								 
                        $("#loadMoreFollowers").addClass('d-none');
                    }
            }
        });
    }

    function loadMoreFollowing(){

        var nextPage = page_numbers[following_type]+1;
        var url = '{{route("profile.load_more_following")}}';
        var user_id = '{{$user_id}}';

        $.ajax({
            type: 'POST',
            url: url,
            data: { pagenum: nextPage, type:following_type, user_id:user_id },
            success: function(data){
                    if(data != ''){
                        checkFollowingOut = true
                        $('.profile-following-wrap').append(data);
                        page_numbers[following_type] = nextPage;
                    } else {
                        $("#loadMoreFollowing").addClass('d-none');
                    }
            }
        });
    }
    
</script>
