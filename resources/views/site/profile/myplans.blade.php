@extends('site.profile.template.profile')
@php $title = 'Travooo - Profile My Plans'; @endphp
@section('before_site_style')
    @parent
    <link rel="stylesheet" href="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets2/css/bootstrap-year-calendar.css')}}">
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
@endsection

@section('content')
   <div class="container-fluid">

    @include('site.profile._profile_header')


    <div class="custom-row">
        <!-- MAIN-CONTENT -->
        <div class="main-content-layer">
            <div class="post-block post-top-bordered">
        @if($login_user_id !== '')
            <div class="post-side-top top-tabs arrow-style">
                <div class="post-top-txt plan-tab-title">
                  <h3 class="side-ttl  current paln-tab-link" tab-url="{{route('profile.update.all_plan')}}">
                      <a href="javascript:;">
                        All Trip Plans
                      </a>
                      <span class="count">{{count($all_plans)}}</span>
                  </h3>
                  <h3 class="side-ttl  paln-tab-link" tab-url="{{url('profile-plans-upcoming')}}">
                      <a href="javascript:;">
                       Upcoming
                      </a>
                  </h3>
                 <h3 class="side-ttl paln-tab-link" tab-url="{{url('profile-plans-memory')}}">
                      <a href="javascript:;">
                        Memory
                      </a>
                  </h3>
                  <h3 class="side-ttl paln-tab-link" tab-url="{{url('profile-plans-invited')}}">
                      <a href="javascript:;">
                        Invitations
                      </a>
                  </h3>
                  <h3 class="side-ttl paln-tab-link" tab-url="{{url('profile-plans-mydraft')}}">
                      <a href="javascript:;">
                        My Draft
                      </a>
                  </h3>
                </div>
            </div>
       
        <div class="post-side-top t-plan-action-block">
                <div class="post-top-txt horizontal">
                    <button type="button" onclick="location.href='{{ route('trip.plan', 0) }}';"
                            class="btn btn-light-primary btn-bordered">Create Trip Plan
                    </button>
                </div>
           
            <div class="side-right-control">
                    <div class="sort-by-select">
                        <label>@lang("other.sort_by"):</label>
                        <div class="sort-select-wrap">
                            <select name="sorted_by_date" id="plan_sort" class="sort-select sources sorted-plan-date" data-type="plan_date" placeholder="Newest">
                                <option value="desc">Newest</option>
                                <option value="asc">Oldest</option>
                                <option value="creation_date">Creation Date</option>
                            </select>
                        </div>
                    </div>
            </div>
        </div>
        @else
        <div class="post-side-top top-tabs arrow-style">
                <div class="post-top-txt plan-tab-title">
                <h3 class="side-ttl  current paln-tab-link" tab-url="{{route('profile.update.all_plan')}}">
                      <a href="javascript:;">
                        All Trip Plans
                      </a>
                    <span class="count">{{count($all_plans)}}</span>
                  </h3>
                  <h3 class="side-ttl  paln-tab-link" tab-url="{{url('profile-plans-upcoming')}}">
                      <a href="javascript:;">
                       Upcoming
                      </a>
                  </h3>
                 <h3 class="side-ttl paln-tab-link" tab-url="{{url('profile-plans-memory')}}">
                      <a href="javascript:;">
                        Memory
                      </a>
                  </h3>
                </div>
            <div class="side-right-control">
                 <div class="sort-by-select">
                        <label>@lang("other.sort_by"):</label>
                        <div class="sort-select-wrap">
                            <select name="sorted_by_date" id="plan_sort" class="sort-select sources sorted-plan-date" data-type="plan_date" placeholder="Newest">
                                <option value="desc">Newest</option>
                                <option value="asc">Oldest</option>
                                <option value="creation_date">Creation Date</option>
                            </select>
                        </div>
                    </div>
            </div>
            </div>
        @endif
        <div class="trip-plan-inner">
            <div class="t-plan-block"></div>
            <div id="loadMorePlan" class="load-more-plan">
                <input type="hidden" id="plan_pagenum" value="1">
                <div id="reportplanloader" class="trip-plan-row">
                    <div class="plan-inside-animation animation ml-0"></div>
                    <div class="plan-block-text-animation">
                        <div class="plan-block-title-animation animation"></div>
                        <div class="plan-desc-animation animation"></div>
                        <div class="plan-dest-info">
                            <div class="plan-hr-animation" id="hide_plan_loader"></div>
                            <div class="plan-dest-left-animation animation"></div>
                            <div class="plan-dest-right-animation animation" ></div>
                        </div>
                    </div>
                    <div class="plan-place-info">
                        <div class="plan-place-animation animation"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        </div>
        <!-- SIDEBAR -->
        <div class="sidebar-layer" id="sidebarLayer">
            <aside class="sidebar">
                @include('site.profile.partials._aside_posts_block')
                @if(is_object($active_plan))
                <div class="post-block post-side-block mb-2">
                    <div class="post-side-top">
                        <h3 class="side-ttl">Active Trip Plan</h3>
                    </div>
                    <div class="side-active-plan-inner">
                        <div class="active-p-map-block">
                            <div id="active_plan_map" style='width:100%;height: 368px;'></div>
                            <div class="active-p-map-info-block">
                                <div class="active-p-map-date-block">
                                    <span class="active-plan-icon-wrap"><img src="{{asset("assets2/image/active-plan-icon.png")}}"></span>
                                    <span class="active-plan-start-date">Started In <span class="active-plan-date">{{$active_plan->version->start_date->format('d M Y')}}</span></span>
                                </div>
                                <div class="active-p-map-title-block">
                                    {{$active_plan->title}}
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endif
                <div class="post-block post-side-block photo-side-block">
                    <div class="post-side-top">
                        <h3 class="side-ttl">Timeline</h3>
                         <div class="side-right-control">

                        </div>
                    </div>
                    <div class="side-post-inner aside-inner-scroller prifile-trip-calendar" id="timline_list">
                        <div id="trip_calendar"></div>
                    </div>
                </div>
                <div class="aside-footer">
                    <ul class="aside-foot-menu">
                        <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                        <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                        <li><a href="{{url('/')}}">Advertising</a></li>
                        <li><a href="{{url('/')}}">Cookies</a></li>
                        <li><a href="{{url('/')}}">More</a></li>
                    </ul>
                    <p class="copyright">Travooo &copy; 2017 - {{date('Y')}}</p>
                </div>
            </aside>
        </div>
    </div>
</div>
@endsection

@section('before_site_script')
    @parent
    <script src="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.js')}}"></script>
    <script src="{{asset('assets2/js/bootstrap-year-calendar.js')}}"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
    <script src="https://npmcdn.com/@turf/turf@5.1.6/turf.min.js"></script>
      <script>
  var current_tab = "{{route('profile.update.all_plan')}}";  
  var sort_type = 'desc';
$(document).ready(function () {
    filterOrder('sorted-plan-date', 'Newest');
    $('body').on('mouseover', '.trip-plan-row', function () {
        $(this).find('.edit-paln-dropdown').show()
    });

    $('body').on('mouseout', '.trip-plan-row', function () {
        $(this).find('.edit-paln-dropdown').hide()
    });
 
   getTabContent(current_tab, sort_type)
 
$(".sort-option").on("click", function() {
  $(this).parents(".sort-options").find(".sort-option").removeClass("selection");
  $(this).addClass("selection");
  $(this).parents(".sort-select").removeClass("opened");
  $(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());
  
 $("#loadMorePlan").removeClass('d-none');
  var selected_type =  $(this).data("value");
  getTabContent(current_tab, selected_type);
});
    
    //sorted by date
    $(document).on('click', '.paln-tab-link', function(){
        current_tub = $(this).attr('tab-url');
        $(this).parent().find('.current').removeClass('current')
        $(this).addClass('current');
         $("#loadMorePlan").removeClass('d-none');
        getTabContent(current_tub, sort_type)
    })
            
    // load all plan   
    $(document).on('inview', '#hide_plan_loader', function(event, isInView) {
         if (isInView) {
             loadMorePlan(current_tab)
         }
     });
     
     //accept invitation
    $('body').on('click', '.plan-invite-accept-link', function (e) {
        var invitation_id = $(this).attr('data-id');
        var _this = $(this);
        $.ajax({
            method: "POST",
            url: "{{ route('trip.ajax_accept_invitation') }}",
            data: {invitation_id: invitation_id}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                if (result.status == 'success') {
                   _this.parent().html('<span>Accepted</span>')
                }

            });
        e.preventDefault()
    });

    $('body').on('click', '.plan-invite-deny-link', function (e) {
        var invitation_id = $(this).attr('data-id');
        var _this = $(this);
        $.ajax({
            method: "POST",
            url: "{{ route('trip.ajax_reject_invitation') }}",
            data: {invitation_id: invitation_id}
        })
            .done(function (res) {
                var result = JSON.parse(res);
                if (result.status == 'success') {
                     _this.parent().html('<span>Rejected</span>')
                }

            });
        e.preventDefault()
    });

        
            
            
    var eventData = [
            @foreach($all_plans AS $inv)
            {
                "id:": "{{$loop->index}}",
                "name": "{{$inv->title}}",
                "event_id": "{{$inv->id}}",
                "location": '<img src="{{get_plan_map($inv, 140, 150)}}" alt="map-image" class="popover-image">',
                "startDate":  new Date("{{$inv->version->start_date}}"),
                "endDate":  new Date("{{$inv->version->end_date}}")
            },
        @endforeach
         

    ];
     var currentYear = new Date().getFullYear();

    $('#trip_calendar').calendar({
        enableContextMenu: true,
        enableRangeSelection: true,
        style:'custom',
        customDataSourceRenderer: function(element) {
                $(element).css('background-color', '#4080ff');
                $(element).css('color', 'white');
                $(element).css('border-radius', '0');
                $(element).css('margin-bottom', '1px');
            },
        mouseOnDay: function(e) {
            if(e.events.length > 0) {
                var content = '';

                for(var i in e.events) {
                    content += '<div class="event-tooltip-content">'
                                    + '<div class="event-location">' + e.events[i].location + '</div>'
                                    + '<div class="event-name" style="color:' + e.events[i].color + '">' + e.events[i].name + '</div>'
                                + '</div>';
                }

                $(e.element).popover({
                    trigger: 'click',
                    container: 'body',
                    html:true,
                    content: content
                });
                $(e.element).popover('show');
            }
        },
        mouseOutDay: function(e) {
            if(e.events.length > 0) {
                $(e.element).popover('hide');
            }
        },
        clickDay: function(e) {
            if(e.events.length > 0 ){
                window.location.href ='/trip/plan/' +e.events[0].event_id;
            }
        },
        dayContextMenu: function(e) {
            $(e.element).popover('hide');
        },
        dataSource:eventData
    });
        });
        
//load more plan function
function loadMorePlan(url){
    var nextPage = parseInt($('#plan_pagenum').val()) + 1;
    var user_id = '{{$user_id}}';

    $.ajax({
        type: 'POST',
        url: url,
        data: {pagenum: nextPage, user_id:user_id, sort:sort_type},
        success: function (data) {
            if(data != ''){
                  $('.t-plan-block').append(data);
                  $('#plan_pagenum').val(nextPage);
              } else {
                  $("#loadMorePlan").addClass('d-none');
              }
        }
    });
}

//load more plan function
function getTabContent(url, sort){
     $('.t-plan-block').html('')
     var user_id = '{{$user_id}}';
      $('#plan_pagenum').val(1)
     current_tab = url;
     sort_type = sort;
    $.ajax({
        type: 'POST',
        url: url,
        data: {pagenum: 1, user_id:user_id, sort:sort},
        success: function (data) {
            if(data != ''){
                  $('.t-plan-block').html(data)
              }else{
                  $('.t-plan-block').html('<div class="py-2">No plan added...</div>') 
              }
        }
    });
}


    @if(is_object($active_plan))
    var map, center_lat, center_lng, icon, markers = [], now = '{{\Carbon\Carbon::now()->format('Y-m-d')}}';

    initMap();

    function initMap() {
         var bounds = new mapboxgl.LngLatBounds();
         var mapBounds = [
             [-180, -85],
             [180, 85]
         ];

         mapboxgl.accessToken = '{{config('mapbox.token')}}';

         map = new mapboxgl.Map({
             container: 'active_plan_map',
             style: 'mapbox://styles/mapbox/satellite-streets-v11',
             maxBounds: mapBounds,
             attributionControl: false
         });

        var waypts = [];

        @foreach(get_active_plan_map_data($active_plan) AS $a_plan)
            @if ($a_plan['lat'] && $a_plan['lng'])
                var lnglat = [{{$a_plan['lng']}}, {{$a_plan['lat']}}];

                if(now == '{{$a_plan['date']}}'){
                    icon = "{{asset('assets2/image/active_marker.png')}}";
                }else if(now > '{{$a_plan['date']}}'){
                    icon = "{{asset('assets2/image/road_marker2.png')}}";
                }else{
                    icon = "{{asset('assets2/image/passive_marker.png')}}";
                }

                var markerEl = $('<div style="width: 14px; height: 14px; overflow: hidden; position: absolute; cursor: pointer; touch-action: none;"><img alt="" src="' + icon + '" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; width: 100%; height: 100%; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>').get(0);

                var marker = new mapboxgl.Marker(markerEl)
                    .setLngLat(lnglat)
                    .addTo(map);

                markers.push(marker);

                waypts.push(lnglat);
                bounds.extend(lnglat);
            @endif
        @endforeach

        map.fitBounds(bounds, {
            padding: {top: 50, bottom:50, left: 50, right: 50},
            maxZoom: 10
        });

        map.on('load', function () {
            map.addSource('route', {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': waypts
                    }
                }
            });

            map.addLayer({
                'id': 'route',
                'type': 'line',
                'source': 'route',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#fff',
                    'line-width': 4
                }
            });
        });
     }

@endif
    </script>

@endsection
@section('after_scripts')
 @include('site.profile.template._select_scripts')
@endsection
