@extends('site.profile.template.profile')
@php $title = 'Travooo - Profile Videos'; @endphp
@section('content')
<div class="container-fluid">

    @include('site.profile._profile_header')


    <div class="custom-row">
        <!-- MAIN-CONTENT -->
        <div class="main-content-layer">
            <div class="post-block post-top-bordered">
                <div class="post-side-top top-tabs arrow-style">
                    <div class="post-top-txt">
                        <h3 class="side-ttl current">@lang('profile.all_videos') <span class="count">{{$video_count}}</span>
                        </h3>
                    </div>
                    <div class="side-right-control">
                        <div class="sort-by-select">
                            <label>@lang('other.sort_by'):</label>
                            <div class="sort-select-wrap">
                                <select name="sorted_by" id="sources" class="sel-select sources" placeholder="Select Type">
                                    <option value="date_down">Newest</option>
                                    <option value="date_up">Oldest</option>
                                    <option value="most_liked">Most Liked</option>
                                    <option value="most_commented">Most Commented</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-media-inner prifile-video-post-media-inner">
                    <div class="post-media-list">
                        @if(count($videos) > 0)
                         <div class="p-video-block">
                            @php $index_num = 0; @endphp
                             @include('site.profile.partials._videos_block')
                        </div>
                        <div id="loadMoreVideo" class="load-more-video">
                            <input type="hidden" id="video_pagenum" value="1">
                            <div class="load-animation-block">
                                <div class="load-video-animation-item animation"></div>
                                <div class="load-video-animation-item animation"></div>
                                <div class="load-video-animation-item animation" id="hide_video_loader"></div>
                            </div>
                        </div>
                        @else
                        <div class="media-wrap media-not-found">
                            <div style="color:#808080;">No Videos Yet ...</div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>

        <!-- SIDEBAR -->
        <div class="sidebar-layer" id="sidebarLayer">
            <aside class="sidebar">
                @include('site.profile.partials._aside_posts_block')
                <div class="post-block post-side-block photo-side-block">
                    <div class="post-side-top">
                        <h3 class="side-ttl">Timeline</h3>
                        <div class="side-right-control">
                            <div class="sort-by-select">
                                <label>@lang('other.sort_by'):</label>
                                <div class="sort-select-wrap">
                                    <select name="sorted_by_timeline" id="sources" class="sel-select sources sorted-timeline" data-type="video_timeline" placeholder="Select Type">
                                        <option value="day">Days</option>
                                        <option value="month">Months</option>
                                        <option value="year">Years</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="side-photo-inner aside-inner-scroller" id="video_timline_list">
                        <div class="side-media-loader">
                            <div class="side-media-loader-title animation"></div>
                            <div class="side-media-loader-block">
                                <div class="animation"></div>
                                <div class="animation"></div>
                                <div class="animation"></div>
                                <div class="animation"></div>
                                <div class="animation"></div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('site.profile.partials._aside_link_block')
                <div class="aside-footer">
                    <ul class="aside-foot-menu">
                        <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                        <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                        <li><a href="{{url('/')}}">Advertising</a></li>
                        <li><a href="{{url('/')}}">Cookies</a></li>
                        <li><a href="{{url('/')}}">More</a></li>
                    </ul>
                    <p class="copyright">Travooo &copy; 2017 - {{date('Y')}}</p>
                </div>
            </aside>
        </div>
    </div>
</div>


@endsection



@section('after_scripts')
@include('site.profile.template._select_scripts')
@include('site.profile.partials._video_modal_block')
 <script src="{{ asset('assets2/js/posts-script.js?v=0.3') }}"></script>
 <script src="{{ asset('/assets3/js/profile.js?ver='.time()) }}"></script>
<script type="text/javascript" src="{{ asset('assets2/js/slick.min.js') }}"></script>

<script>
    var sort_type = '';
    $(document).ready(function () {
        
        $("video").bind("contextmenu",function(){
        return false;
        });
        
        $(".profile-video").lazy({
            afterLoad: function (element) {
                $('.video-play-btn').removeClass('d-none')
                  initializeVideo(element.attr('id'))
            },
        });
        getVideoTimelineMedia('month');

        $(document).on('click', '.v-play-btn', function () {
            var v_id = $(this).attr('data-v_id')
            var video = document.getElementById('lg_video_' + v_id);

            togglePlay(video, 'lg_video_' + v_id);
        })

        $(document).on('click', '.video-autoplay', function () {
            togglePlay($(this)[0], $(this).attr('id'));
        })

        //videos load more
        $(document).on('inview', '#hide_video_loader', function(event, isInView) {
             if (isInView) {
                 loadMoreVideo(sort_type)
             }
         });
         
         
        $(".aside-videos").bind("contextmenu",function(){
        return false;
        });
 
    })
    
    $(document).on('click', '.v-modal-close', function () {
        $('#profile_video_modal').hide()
    })
    
    $(document).on('click', '.lightbox_videoModal', function () {
        var id = $(this).attr('data-id')
          $(".video__cards-items").not('.slick-initialized').slick({
            variableWidth: !0,
            focusOnSelect: !0,
            infinite: !1,
            slidesToShow: 6,
            prevArrow: '<button class="video-cards-control video__cards-control--prev"><svg class="icon icon--angle-left"> <use xlink:href="{{asset('assets3/img/sprite.svg#angle-left')}}"></use> </svg></button>',
            nextArrow: '<button class="video-cards-control video__cards-control--next"><svg class="icon icon--angle-right"> <use xlink:href="{{asset('assets3/img/sprite.svg#angle-right')}}"></use> </svg></button>',
          responsive: [
              {
                  breakpoint: 768,
                  settings: {
                      slidesToShow: 1,
                      centerMode: true,
                  }
              },
              {
                  breakpoint: 908,
                  settings: {
                      slidesToShow: 2
                  }
              },
              {
                  breakpoint: 1260,
                  settings: {
                      slidesToShow: 3
                  }
              },
              {
                  breakpoint: 1380,
                  settings: {
                      slidesToShow: 4
                  }
              },
              {
                  breakpoint: 1709,
                  settings: {
                      slidesToShow: 5
                  }
              }
          ]
        });
        
        $('.video-gallery-block').find('.lg-item').addClass('d-none')  
        $('.video-inner-' + id).removeClass('d-none')
        
        $('.v-card-' + id).addClass('slick-current').addClass('slick-center').click()  
   
        
        $('#profile_video_modal').show()
    })
    
   function getVideoModal(id, obj) {
          
    $('.video-gallery-block').find('.lg-item').addClass('d-none')  
    $('.video-inner-' + id).removeClass('d-none')
    
        }
        
    function itemsSort(obj, type) {
        var list, i, switching, shouldSwitch, switchcount = 0;
        switching = true;
        parent_obj = $('.post-media-list').find('.p-video-block');
        while (switching) {
            switching = false;
            list = parent_obj.find('>div');
            shouldSwitch = false;
            for (i = 0; i < list.length - 1; i++) {
                shouldSwitch = false;
                if (type == "date_up")
                {
                    if ($(list[i]).attr('datesort') > $(list[i + 1]).attr('datesort')) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (type == "date_down")
                {
                    if ($(list[i]).attr('datesort') < $(list[i + 1]).attr('datesort')) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (type == "most_liked")
                {
                    if ($(list[i]).find('.media-liked-count b').html() < $(list[i + 1]).find('.media-liked-count b').html()) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (type == "most_commented")
                {
                    if ($(list[i]).find('.media-commented-count b').html() < $(list[i + 1]).find('.media-commented-count b').html()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                $(list[i]).before(list[i + 1]);
                switching = true;
                switchcount++;
            } else {
                if (switchcount == 0 && list.length > 1 && i == 0) {
                    switching = true;
                }
            }
        }
    }
    
    
        // video like & unlike photo
    $('body').on('click', '.photo_like_button', function (e) {
        mediaId = $(this).attr('id');
        $.ajax({
            method: "POST",
            url: "{{ route('media.likeunlike') }}",
            data: {media_id: mediaId}
        })
            .done(function (res) {
                var result = JSON.parse(res);

                if (result.status == 'yes') {
                    $('.'+ mediaId +'-like-count b').html(result.count)
                    $('.'+ mediaId +'-media-liked-count b').html(result.count)
                    $('.'+ mediaId +'-media-like-icon').html('<i class="trav-heart-fill-icon" style="color:#ff5a79;"></i>')
                    $('.'+ mediaId +'-media-like-icon').attr('data-type', 'like')

                } else if (result.status == 'no') {
                    $('.'+ mediaId +'-like-count b').html(result.count)
                    $('.'+ mediaId +'-media-liked-count b').html(result.count)
                    $('.'+ mediaId +'-media-like-icon').html('<i class="trav-heart-icon"></i>')
                    $('.'+ mediaId +'-media-like-icon').attr('data-type', 'unlike')
                }
            });
        e.preventDefault();
    });
    
            // video comment ------start------

                // -----START----- Post comment
        $('body').on('mouseover', '.news-feed-comment', function () {
            $(this).find('.post-com-top-action .dropdown').show()
        });

        $('body').on('mouseout', '.news-feed-comment', function () {
            $(this).find('.post-com-top-action .dropdown').hide()
        });

        $('body').on('mouseover', '.doublecomment', function () {
            $(this).find('.post-com-top-action .dropdown').show()
        });

        $('body').on('mouseout', '.doublecomment', function () {
            $(this).find('.post-com-top-action .dropdown').hide()
        });


    $(document).on('change', '.profile-media-file-input', function (e) {
            var comment_id = $(this).attr('data-comment_id')
                commentMediaUploadFile($(this), $(".media_comment_form"), comment_id);

        })

    $(document).on('keydown', '*[data-id="mediacommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '*[data-id="mediacommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            $(this).closest('form').submit();
        }
    });

    $(document).on('submit', '.media_comment_form', function(e){
        var form = $(this);
        var text = form.find('*[data-id="mediacommenttext"]').val().trim();
        var files = form.find('.medias').find('div').length;

        if(text == "" && files == 0)
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: '',
                    type: 'orange',
                    offsetTop: 0,
                    offsetBottom: 500,
                    content: 'Please input text or select files!',
                });
                return false;
            }
        var values = $(this).serialize();

        form.trigger('reset');
        var postId = form.find('input[name="medias_id"]').val();
        var all_comment_count = $('.'+ postId +'-all-comments-count').html();
        var url = '{{ route("media.comment", ":postId") }}';
        url = url.replace(':postId', postId);

        $.ajax({
            method: "POST",
            url: url,
            data: values
        })
            .done(function (res) {
                form.closest('.gallery-comment-wrap').find('.post-comment-wrapper').prepend(res);
                var comments = form.closest('.gallery-comment-wrap').find('.post-comment-wrapper').find('.post-comment-row').length;
                form.closest('.gallery-comment-wrap').find('.comm-count-info').find('strong').html(comments);
                form.find('.medias').find('.removeFile').each(function () {
                      $(this).click();
                  });
                $('.'+ postId +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                $("#" + postId + ".comment-not-fund-info").remove();

                form.find('.medias').empty();
                form.find('textarea').val('');
                form.find('textarea').css({"height": "0", "padding-bottom": "0"});
                form.find('.post-create-controls').css('top', '15px');

                Comment_Show.reload($('.photo-comment-' + postId));
                form.trigger('reset');
            });
        e.preventDefault();
    });

    $('body').on('click', '.postmediaCommentLikes', function (e) {
            var _this = $(this);
            commentId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('media.commentlikeunlike') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'yes') {
                            _this.find('i').addClass('fill');
                            _this.parent().find('.media-comment-likes-block').prepend('<span>' + result.name + '</span>')
                        } else if (result.status == 'no') {
                            _this.find('i').removeClass('fill');
                            _this.parent().find('.media-comment-likes-block span').each(function () {
                                if ($(this).text() == result.name)
                                    $(this).remove();
                            });
                        }
                        _this.find('.comment-like-count').html(result.count);



                    });
            e.preventDefault();
        });


        $('body').on('click', '.media-comment-likes-block', function (e) {
            var _this = $(this);
            commentId = $(this).attr('data-id');
            var like_user_count = _this.parent().find('.comment-like-count').text()
            $.ajax({
                method: "POST",
                url: "{{ route('media.commentlikeusers') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        $('.post-comment-like-users').html(res)
                        $('#commentslikePopup').find('.comment-like-count').html(like_user_count)
                        $('#commentslikePopup').modal('show')
                    });
            e.preventDefault();
        });

        $('body').on('click', '.mediaCommentReply', function (e) {
            commentId = $(this).attr('id');
            $('#mediaReplyForm' + commentId).show();
            e.preventDefault();
        });


        $(document).on('change', '.media-file-input', function (e) {
            var comment_id = $(this).attr('data-comment_id')
                commentMediaUploadFile($(this), $(".mediaCommentReplyForm"+comment_id), comment_id);

        })

        $(document).on('keydown', '.media-comment-text', function (e) {
            if (e.keyCode == 13 && !e.shiftKey) {
                e.preventDefault();
            }
        })

        $(document).on('keyup', '.media-comment-text', function (e) {
            if (e.keyCode == 13 && !e.shiftKey) {
                var comment_id = $(this).attr('data-comment_id');
                replyMediaComment(comment_id)
            }
        })

    //photo comment delete
    $('body').on('click', '.postmediaCommentDelete', function (e) {

            var _this = $(this);
            var commentId = _this.attr('id');
            var postId = _this.attr('postid');
            var media_comments_count = $('.' + postId + '-all-comments-count').html();
            var reload_obj = $('.photo-comment-' + postId);
            var type = _this.data('type');
            var parent_id = _this.data('parent')

            $.confirm({
                title: 'Delete Comment!',
                content: 'Are you sure you want to delete this comment? <div class="mb-3"></div>',
                columnClass: 'col-md-5 col-md-offset-5',
                closeIcon: true,
                offsetTop: 0,
                offsetBottom: 500,
                scrollToPreviousElement:false,
                scrollToPreviousElementAnimate:false,
                buttons: {
                    cancel: function () {},
                    confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'shift'],
                        action: function () {
                            $.ajax({
                                method: "POST",
                                url: "{{ route('media.commentdelete') }}",
                                data: {comment_id: commentId}
                            })
                            .done(function (res) {

                                $('#mediaCommentRow' + commentId).fadeOut();
                                $('.' + postId + '-all-comments-count').html(parseInt(media_comments_count) - 1);

                                if (type == 1) {
                                    $('#mediaCommentRow' + commentId).parent().remove();
                                   Comment_Show.reload($('.photo-comment-' + postId));
                                } else {
                                    if (_this.closest('.post-block').find('*[data-parent_id="' + parent_id + '"]:visible').length == 1) {
                                        $('#mediaCommentRow' + parent_id).find('.postCommentDelete').removeClass('d-none')
                                        $('#mediaCommentRow' + parent_id).find('.delete-line').removeClass('d-none')
                                    }
                                }
                            });
                        }
                    }
                }
            });
        });
        
    $(document).on('click', 'i.fa-camera', function () {
        $("#createPosTxt").click();
        var target = $(this).attr('data-target');
        var target_el = $('#' + target);
        target_el.trigger('click');
    })
    
        function replyMediaComment(id) {
        var form = $('.mediaCommentReplyForm' + id);
        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        var values = form.serialize();

        $.ajax({
            method: "POST",
            url: "{{ route('media.commentreply') }}",
            data: values
        })
                .done(function (res) {
                    form.parent().parent().before(res);
                    form.find('.medias').find('.removeFile').each(function () {
                        $(this).click();
                    });
                    $('#mediaCommentRow' + id).find('.postCommentDelete').addClass('d-none');
                    $('#mediaCommentRow' + id).find('.delete-line').addClass('d-none');
                    form.find('textarea').val('');
                    form.find('textarea').css({"height": "0", "padding-bottom": "0"});
                    form.find('.post-create-controls').css('top', '15px');
                    form.find('.medias').empty();

                });
    }
    
    function comment_textarea_auto_height(element, type) {
        element.style.height = "5px";
        element.style.paddingBottom = '30px';
        if (type == 'reply') {
            jQuery(element).closest('.post-reply-block').find('.post-create-controls').css('top', (element.scrollHeight - 25) + "px")
        } else if (type == 'edit') {
            jQuery(element).closest('.post-edit-block').find('.post-create-controls').css('top', (element.scrollHeight - 15) + "px")
        } else {
            jQuery(element).closest('.post-regular-block').find('.post-create-controls').css('top', (element.scrollHeight - 25) + "px")
        }
        element.style.height = (element.scrollHeight) + "px";
    }

    function getVideoTimelineMedia(type) {
        var user_id = '{{$user->id}}';

        $.ajax({
            method: "POST",
            url: "{{ route('profile.timeline.medias') }}",
            data: {type: type, user_id: user_id, media_type: 'video'}
        })
                .done(function (res) {
                    $('#video_timline_list').html(res)
                    $(".aside-videos").lazy();
                });
    }

    function togglePlay(video, id) {
        $("video").each(function () {
            if($(this).attr('id') != id){
                $(this).get(0).pause();
            }
        });
        
        video.addEventListener('pause', (event) => {
            if(event.target.id)
            $('.' + event.target.id).show()
        });

        if (video.paused || video.ended) {
            if(video.currentTime == 5){
                 video.currentTime = 0;
            }
            video.play();
            $('.' + id).hide()
        } else {
            video.pause();
            $('.' + id).show()
        }
    }
    
function formatTime(timeInSeconds) {
  var result = new Date(timeInSeconds * 1000).toISOString().substr(11, 8);

  return result.substr(3, 2)+':'+result.substr(6, 2);
};


function initializeVideo(video_id) {
  var video = document.getElementById(video_id);
  var videoDuration = Math.round(video.duration);
  var time = formatTime(videoDuration);
  
  $('.time-' +video_id).html(time)
  
}

//load more photo function
function loadMoreVideo(sort){
    var nextPage = parseInt($('#video_pagenum').val()) + 1;
    var user_id = '{{$user_id}}';

    $.ajax({
        type: 'POST',
        url: "{{route('profile.load_more_video')}}",
        data: {pagenum: nextPage, user_id:user_id, sort:sort},
        success: function (data) {
            if(data != ''){
                  $('.p-video-block').append(data);
                  $('#video_pagenum').val(nextPage);
                    $(".profile-video").lazy({
                        afterLoad: function (element) {
                            $('.video-play-btn').removeClass('d-none')
                              initializeVideo(element.attr('id'))
                        },
                    });
              } else {
                  $("#loadMoreVideo").addClass('d-none');
              }
        }
    });
}


</script>
<script src="{{ asset('/assets3/js/profile.js?ver='.time()) }}"></script>
@endsection
