@extends('site.profile.template.profile')

@section('content')

<div class="container-fluid">
        
        <div class="top-banner-wrap">
          <div class="top-banner-block" style="background-image: url(./assets/image/disneyland-image.jpg)">
            <ul class="nav nav-tabs expert-top-filter" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#dashPage" role="tab">Page</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#dashInbox" role="tab">Inbox</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#dashNotification" role="tab">Notifications</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#dashInsight" role="tab">Insights</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#dashSetting" role="tab">Settings</a>
              </li>
            </ul>
            <div class="top-banner-text expert-variant">
              <div class="place-info-block">
                <div class="expert-top">
                  <div class="avatar-layer">
                    <div class="ava-inner">
                      <img src="http://placehold.it/146x146" alt="" class="avatar">
                      <a href="#" class="edit-ava-link">
                        <img src="./assets/image/profile-ava-edit-img.png" alt="">
                      </a>
                    </div>
                  </div>
                  <div class="expert-info">
                    <div class="name-title">Ramon Okamoto</div>
                    <div class="name-placement">United States</div>
                    <div class="follow-block-info">
                      <ul class="foot-avatar-list">
                        <li><img class="small-ava" src="http://placehold.it/29x29" alt="ava"></li><!--
                        --><li><img class="small-ava" src="http://placehold.it/29x29" alt="ava"></li><!--
                        --><li><img class="small-ava" src="http://placehold.it/29x29" alt="ava"></li>
                      </ul>
                      <span>642 Follows</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="banner-comment">
                <a class="btn btn-light-primary" href="#">Follow</a>
              </div>
            </div>
          </div>
        </div>

        
        <div class="custom-row">
          <!-- MAIN-CONTENT -->
          <div class="main-content-layer">

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#reportsPopup">
              Reports popup
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#badgesPopup">
              Badges popup
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tripPlansPopup">
              Trip plans popup
            </button>
            <button type="button" class="btn btn-primary" id="photoPopupTrigger">
              Visited & Favorites popup
            </button>

            <div class="post-block post-create-block" id="createPostBlock">
              <div class="post-create-input">
                <input id="createPostTxt" placeholder="Write something..." type="text">
              </div>
              <div class="post-create-controls">
                <ul class="create-link-list">
                  <li>
                    <i class="trav-camera"></i>
                  </li><!--
                  --><li>
                    <i class="trav-set-location-icon"></i>
                  </li>
                </ul>
                <button class="btn btn-light-primary btn-disabled">Post</button>
              </div>
            </div>

            <div class="post-block post-badge-block">
              <div class="post-top-image">
                <img src="http://placehold.it/655x225" alt="image">
                <div class="badge-label"><span>level up!</span></div>
              </div>
              <div class="post-badge-inner">
                <div class="badge-layer">
                  <h3 class="ttl">Your next Badge</h3>
                  <div class="badge-block">
                    <div class="img-wrap">
                      <img src="http://placehold.it/66x66" alt="">
                    </div>
                    <div class="badge-content">
                      <div class="badge-txt">
                        <h5 class="badge-ttl">Badge name</h5>
                        <div class="info-badge">
                          <ul class="foot-avatar-list">
                            <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                            --><li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                            --><li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                          </ul>
                          <span>87 users like you have this badge</span>
                        </div>
                      </div>
                      <div class="badge-count">1000 <span>/ 135</span></div>
                    </div>
                  </div>
                </div>
                <div class="badge-layer">
                  <div class="badge-top-layer">
                    <h3 class="ttl-sm">All your badges</h3>
                    <a href="#" class="more-link"><span>more</span> <i class="fa fa-caret-down"></i></a>
                  </div>
                  <ul class="badges-list">
                    <li>
                      <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                      <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                      <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                      <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                      <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                      <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                      <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                      <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                    <li>
                      <a href="#" class="badge-link"><img src="http://placehold.it/52x52" alt=""></a>
                    </li>
                  </ul>
                  <div class="badge-progress-block">
                    <div class="progress">
                      <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <p>135 points to the next badge</p>
                  </div>

                </div>
              </div>
            </div>

            <div class="post-block">
              <div class="post-top-info-layer">
                <div class="post-top-info-wrap">
                  <div class="post-top-avatar-wrap">
                    <img src="http://placehold.it/45x45" alt="">
                  </div>
                  <div class="post-top-info-txt">
                    <div class="post-top-name">
                      <a class="post-name-link" href="#">Ramon Okamoto</a> 
                    </div>
                    <div class="post-info">
                      Uploaded <b>2 photos</b> yesterday at 10:33pm
                      <i class="trav-set-location-icon"></i>
                    </div>
                  </div>
                </div>
                <div class="post-top-info-action">
                  <div class="dropdown">
                    <button class="btn btn-drop-round-grey" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <!-- <i class="trav-angle-down"></i> -->
                      <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                      <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-share-icon"></i>
                        </span>
                        <div class="drop-txt">
                          <p><b>Share</b></p>
                          <p>Spread the word</p>
                        </div>
                      </a>
                      <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-flag-icon-o"></i>
                        </span>
                        <div class="drop-txt">
                          <p><b>Report</b></p>
                          <p>Help us understand</p>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="post-image-container">
                <ul class="post-image-list">
                  <li>
                    <img src="http://placehold.it/295x335" alt="">
                  </li>
                  <li>
                    <img src="http://placehold.it/295x335" alt="">
                  </li>
                </ul>
              </div>
              <div class="post-footer-info">
                <div class="post-foot-block post-reaction">
                  <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                  <span><b>6</b> Reactions</span>
                </div>
                <div class="post-foot-block">
                  <i class="trav-comment-icon"></i>
                  <ul class="foot-avatar-list">
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    --><li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    --><li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                  </ul>
                  <span>20 Comments</span>
                </div>
              </div>
            </div>

            <div class="post-block">
              <div class="post-top-info-layer">
                <div class="post-top-info-wrap">
                  <div class="post-top-avatar-wrap">
                    <img src="http://placehold.it/45x45" alt="">
                  </div>
                  <div class="post-top-info-txt">
                    <div class="post-top-name">
                      <a class="post-name-link" href="#">Ramon Okamoto</a> 
                    </div>
                    <div class="post-info">
                      Uploaded a <b>photo</b> yesterday at 10:33am
                    </div>
                  </div>
                </div>
                <div class="post-top-info-action">
                  <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                      <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-share-icon"></i>
                        </span>
                        <div class="drop-txt">
                          <p><b>Share</b></p>
                          <p>Spread the word</p>
                        </div>
                      </a>
                      <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-flag-icon-o"></i>
                        </span>
                        <div class="drop-txt">
                          <p><b>Report</b></p>
                          <p>Help us understand</p>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="post-image-container">
                <ul class="post-image-list">
                  <li>
                    <img src="http://placehold.it/595x624" alt="">
                  </li>
                </ul>
              </div>
              <div class="post-footer-info">
                <div class="post-foot-block post-reaction">
                  <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                  <span><b>12</b> Reactions</span>
                </div>
                <div class="post-foot-block">
                  <i class="trav-comment-icon"></i>
                  <ul class="foot-avatar-list">
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    --><li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    --><li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                  </ul>
                  <span>189 Comments</span>
                </div>
              </div>
              <div class="post-comment-layer">
                <div class="post-comment-top-info">
                  <ul class="comment-filter">
                    <li class="active">Top</li>
                    <li>New</li>
                  </ul>
                  <div class="comm-count-info">
                    3 / 20
                  </div>
                </div>
                <div class="post-comment-wrapper">
                  <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                      <img src="http://placehold.it/45x45" alt="">
                    </div>
                    <div class="post-comment-text">
                      <div class="post-com-name-layer">
                        <a href="#" class="comment-name">Katherin</a>
                        <a href="#" class="comment-nickname">@katherin</a>
                      </div>
                      <div class="comment-txt">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                      </div>
                      <div class="comment-bottom-info">
                        <div class="com-reaction">
                          <img src="./assets/image/icon-smile.png" alt="">
                          <span>21</span>
                        </div>
                        <div class="com-time">6 hours ago</div>
                      </div>
                    </div>
                  </div>
                  <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                      <img src="http://placehold.it/45x45" alt="">
                    </div>
                    <div class="post-comment-text">
                      <div class="post-com-name-layer">
                        <a href="#" class="comment-name">Amine</a>
                        <a href="#" class="comment-nickname">@ak0117</a>
                      </div>
                      <div class="comment-txt">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                      </div>
                      <div class="comment-bottom-info">
                        <div class="com-reaction">
                          <img src="./assets/image/icon-like.png" alt="">
                          <span>19</span>
                        </div>
                        <div class="com-time">6 hours ago</div>
                      </div>
                    </div>
                  </div>
                  <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                      <img src="http://placehold.it/45x45" alt="">
                    </div>
                    <div class="post-comment-text">
                      <div class="post-com-name-layer">
                        <a href="#" class="comment-name">Katherin</a>
                        <a href="#" class="comment-nickname">@katherin</a>
                      </div>
                      <div class="comment-txt">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                      </div>
                      <div class="comment-bottom-info">
                        <div class="com-reaction">
                          <img src="./assets/image/icon-smile.png" alt="">
                          <span>15</span>
                        </div>
                        <div class="com-time">6 hours ago</div>
                      </div>
                    </div>
                  </div>
                  <a href="#" class="load-more-link">Load more...</a>
                </div>
                <div class="post-add-comment-block">
                  <div class="avatar-wrap">
                    <img src="http://placehold.it/45x45" alt="">
                  </div>
                  <div class="post-add-com-input">
                    <input type="text" placeholder="Write a comment">
                  </div>
                </div>
              </div>
            </div>

            <div class="post-block">
              <div class="post-top-info-layer">
                <div class="post-top-info-wrap">
                  <div class="post-top-avatar-wrap">
                    <img src="http://placehold.it/45x45" alt="">
                  </div>
                  <div class="post-top-info-txt">
                    <div class="post-top-name">
                      <a class="post-name-link" href="#">Ramon Okamoto</a> 
                    </div>
                    <div class="post-info">
                      Shared a 
                      <a href="#" class="link-place">Trip Plan</a> to 
                      <a href="#" class="link-place">7 Destinations</a> in
                      <a href="#" class="link-place">Tokyo</a> 
                      on 1 Sep 2017
                    </div>
                  </div>
                </div>
                <div class="post-top-info-action">
                  <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                      <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-share-icon"></i>
                        </span>
                        <div class="drop-txt">
                          <p><b>Share</b></p>
                          <p>Spread the word</p>
                        </div>
                      </a>
                      <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                          <i class="trav-flag-icon-o"></i>
                        </span>
                        <div class="drop-txt">
                          <p><b>Report</b></p>
                          <p>Help us understand</p>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="post-image-container post-follow-container">
                <div class="post-image-inner">
                  <div class="post-map-wrap">
                    <img src="http://placehold.it/595x400" alt="map">
                    <div class="post-map-info-caption map-blue">
                      <div class="map-avatar">
                        <img src="http://placehold.it/25x25" alt="ava">
                      </div>
                      <div class="map-label-txt">
                        Checking on <b>2 Sep</b> at <b>8:30 am</b> and will stay <b>30 min</b>
                      </div>
                    </div>
                  </div>
                  <div class="post-destination-block slide-dest-hide-right-margin">
                    <div class="lSSlideOuter post-dest-slider-wrap"><div class="lSSlideWrapper usingCss"><div class="post-dest-slider lightSlider lsGrab lSSlide" id="postDestSlider" style="width: 576px; height: 70px; padding-bottom: 0%;">
                      <div class="post-dest-card lslide active" style="margin-right: 8px;">
                        <div class="post-dest-card-inner">
                          <div class="dest-image">
                            <img src="http://placehold.it/68x68" alt="">
                          </div>
                          <div class="dest-info">
                            <div class="dest-name">Bulgari Tokyo</div>
                            <div class="dest-place">Restaurant</div>
                          </div>
                        </div>
                      </div>
                      <div class="post-dest-card lslide" style="margin-right: 8px;">
                        <div class="post-dest-card-inner">
                          <div class="dest-image">
                            <img src="http://placehold.it/68x68" alt="">
                          </div>
                          <div class="dest-info">
                            <div class="dest-name">Bulgari Tokyo</div>
                            <div class="dest-place">Restaurant</div>
                          </div>
                        </div>
                      </div>
                      <div class="post-dest-card lslide" style="margin-right: 8px;">
                        <div class="post-dest-card-inner">
                          <div class="dest-image">
                            <img src="http://placehold.it/68x68" alt="">
                          </div>
                          <div class="dest-info">
                            <div class="dest-name">Bulgari Tokyo</div>
                            <div class="dest-place">Restaurant</div>
                          </div>
                        </div>
                      </div>
                    </div><div class="lSAction"><a class="lSPrev"><i class="trav-angle-left"></i></a><a class="lSNext"><i class="trav-angle-right"></i></a></div></div></div>
                  </div>
                </div>
              </div>
              <div class="post-footer-info">
                <div class="post-foot-block post-reaction">
                  <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                  <span><b>6</b> Reactions</span>
                </div>
                <div class="post-foot-block">
                  <i class="trav-comment-icon"></i>
                  <ul class="foot-avatar-list">
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    --><li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                    --><li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                  </ul>
                  <span>20 Comments</span>
                </div>
              </div>
            </div>

            <div class="post-block post-profile-about-block expert-block">
              <div class="post-content-inner">
                <div class="post-form-wrapper">
                  <div class="expert-ttl-block">
                    <div class="expert-ttl">Page info</div>
                    <div class="expert-rigth-link">
                      <a href="#" class="edit-link">Edit</a>
                    </div>
                  </div>
                  <div class="divider"></div>
                  <div class="row">
                    <label class="col-sm-3 col-form-label">About</label>
                    <div class="col-sm-9">
                      <div class="form-txt">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit cras sollicitudin dapibus libero sit amet gravida vivamus luctus rhoncus velit sit amet dictum sed ultrices eros a mi convallis, eget venenatis tortor ultricies aliquam velit tortor, laoreet eu leo nec.</p>
                        <p>Tempus interdum tortor curabitur nec leo gravida, sollicitudin nisl consectetur, pretium sapien.</p>
                      </div>
                    </div>
                  </div>
                  <div class="divider"></div>
                  <div class="row">
                    <label class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm-9">
                      <div class="form-txt">
                        <a href="#" class="form-link">contact@email.com</a>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-3 col-form-label">Phone</label>
                    <div class="col-sm-9">
                      <div class="form-txt">
                        <a href="#" class="form-link phone">+(1) 123 456789</a>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-3 col-form-label">Website</label>
                    <div class="col-sm-9">
                      <div class="form-txt">
                        <a href="#" class="form-link">www.yourwebsite.com</a>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label class="col-sm-3 col-form-label">Links</label>
                    <div class="col-sm-9">
                      <div class="post-profile-foot-inner">
                        <ul class="profile-social-list">
                          <li>
                            <a href="#" class="facebook">
                              <i class="fa fa-facebook"></i>
                            </a>
                          </li>
                          <li>
                            <a href="#" class="twitter">
                              <i class="fa fa-twitter"></i>
                            </a>
                          </li>
                          <li>
                            <a href="#" class="instagram">
                              <i class="fa fa-instagram"></i>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>

          <!-- SIDEBAR -->
          <div class="sidebar-layer" id="sidebarLayer">
            <aside class="sidebar">

              <div class="post-block post-side-block">
                <div class="post-side-top">
                  <h3 class="side-ttl"><img class="expert-icon" src="./assets/image/expert-profile-icon.png" alt=""> Expert page</h3>
                </div>
              </div>

              <div class="post-block post-side-block">
                <div class="post-side-top expert-side">
                  <h3 class="side-ttl"><i class="trav-reports-icon"></i> Reports <span class="count">26</span></h3>
                  <div class="side-right-control">
                    <a href="#" class="see-more-link">See all</a>
                  </div>
                </div>
              </div>

              <div class="post-block post-side-block">
                <div class="post-side-top">
                  <h3 class="side-ttl">Badges <span class="count">13</span></h3>
                  <div class="side-right-control">
                    <a href="#" class="see-more-link">All</a>
                  </div>
                </div>
                <div class="side-badge-inner">
                  <div class="badge-block">
                    <div class="badge-icon">
                      <img src="http://placehold.it/37x37" alt="">
                    </div>
                    <div class="badge-ttl">
                      Explorer
                    </div>
                  </div>
                  <div class="badge-block">
                    <div class="badge-icon">
                      <img src="http://placehold.it/37x37" alt="">
                    </div>
                    <div class="badge-ttl">
                      Reviewer
                    </div>
                  </div>
                  <div class="badge-block">
                    <div class="badge-icon">
                      <img src="http://placehold.it/37x37" alt="">
                    </div>
                    <div class="badge-ttl">
                      Photographer
                    </div>
                  </div>
                  <div class="badge-block">
                    <div class="badge-icon">
                      <img src="http://placehold.it/37x37" alt="">
                    </div>
                    <div class="badge-ttl">
                      Aventurer
                    </div>
                  </div>
                  <div class="badge-block">
                    <div class="badge-icon">
                      <img src="http://placehold.it/37x37" alt="">
                    </div>
                    <div class="badge-ttl">
                      Explorer
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="post-block post-side-block">
                <div class="side-expert-link-inner">
                  <div class="expert-side-link">
                      <a href="#" class="profile-website-link">
                        <span class="round-icon">
                          <i class="trav-link"></i>
                        </span>
                        <span>www.hilton.com</span>
                      </a>
                    </div>
                    <div class="expert-side-link">
                      <ul class="profile-social-list">
                        <li>
                          <a href="#" class="facebook">
                            <i class="fa fa-facebook"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#" class="twitter">
                            <i class="fa fa-twitter"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#" class="instagram">
                            <i class="fa fa-instagram"></i>
                          </a>
                        </li>
                      </ul>
                    </div>
                </div>
              </div>

              <div class="share-page-block">
                <div class="share-page-inner">
                  <div class="share-txt">Share this page</div>
                  <ul class="share-list">
                    <li>
                      <a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-pinterest-p"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-code"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
              
              <div class="aside-footer">
                <ul class="aside-foot-menu">
                  <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                  <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                  <li><a href="{{url('/')}}">Advertising</a></li>
                  <li><a href="{{url('/')}}">Cookies</a></li>
                  <li><a href="{{url('/')}}">More</a></li>
                </ul>
                <p class="copyright">Travooo &copy; 2017</p>
              </div>
            </aside>
          </div>
        </div>
      </div>


    <button type="button" class="btn btn-primary" id="profilePhotosTrigger">
        @lang('profile.profile_photos_popup')
    </button>


    <div class="post-block post-profile-about-block">
        <div class="post-side-top">
            <h3 class="side-ttl">{{$user->name}}</h3>
        </div>
        <div class="post-content-inner">
            <div class="post-form-wrapper">
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.about')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <p>{{@$user->about}}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.links')</label>
                    <div class="col-sm-9">
                        <div class="post-profile-foot-inner">
                            <a href="#" class="profile-website-link">
                          <span class="round-icon">
                            <i class="trav-link"></i>
                          </span>
                                <span><a href="{{@$user->website}}" target="_blank">@lang('profile.website')</a></span>
                            </a>
                            <ul class="profile-social-list">
                                <li>
                                    <a href="{{@$user->facebook}}" class="facebook" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{@$user->twitter}}" class="twitter" target="_blank">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{@$user->instagram}}" class="instagram" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.from_where')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            {{@$user->nationality}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.age')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <p>{{@$user->age}}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.profession')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <p>{{@$user->display_name}}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.interest')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <p>{{@$user->interests}}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.expert_in')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <a href="#" class="form-link">France</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.member_since')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <p>{{@$user->created_at}}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">@lang('profile.visited')</label>
                    <div class="col-sm-9">
                        <div class="form-txt">
                            <a href="#" class="form-link">Tokyo</a>,&nbsp;
                            <a href="#" class="form-link">Nagoya</a>,&nbsp;
                            <a href="#" class="form-link">Riyadh</a>,&nbsp;
                            <a href="#" class="form-link">Albania</a>&nbsp;
                            <a href="#" class="form-link">+3 More</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection




@section('after_scripts')
    <script>
        $(document).ready(function () {
            $.ajax({
                method: "POST",
                url: "{{ route('profile.check-follow', $user->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    if (res.success == true) {
                        $('#follow_user_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow"><i class="trav-user-plus-icon"></i><span>@lang("profile.unfollow")</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        $('#follow_user_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-user-plus-icon"></i><span>@lang("profile.follow")</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });

            $('body').on('click', '#button_follow', function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('profile.follow', $user->id) }}",
                    data: {name: "test"}
                })
                    .done(function (res) {
                        console.log(res);
                        if (res.success == true) {
                            $('#follow_user_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow"><i class="trav-user-plus-icon"></i><span>@lang("profile.unfollow")</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                        } else if (res.success == false) {
                            //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                        }
                    });
            });

            $('body').on('click', '#button_unfollow', function () {
                $.ajax({
                    method: "POST",
                    url: "{{  route('profile.unfolllow', $user->id)}}",
                    data: {name: "test"}
                })
                    .done(function (res) {
                        console.log(res);
                        if (res.success == true) {
                            $('#follow_user_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-user-plus-icon"></i><span>@lang("profile.follow")</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                        } else if (res.success == false) {
                            //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                        }
                    });
            });
        });
    </script>
@endsection