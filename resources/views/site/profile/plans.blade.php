@extends('site.profile.template.profile')

@section('before_site_style')
    @parent
    <link rel="stylesheet" href="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.css')}}">
@endsection

@section('content')


<div class="container-fluid">

    @include('site.profile._profile_header')


    <div class="custom-row">
        <!-- MAIN-CONTENT -->
        <div class="main-content-layer">
            <div class="post-block post-top-bordered">
        <div class="post-side-top top-tabs underline-style">
                <div class="post-top-txt">
                  <h3 class="side-ttl @if($action=='plans') current @endif">
                      <a href="{{url('profile-plans')}}">
                        My Plans
                      </a>
                  </h3>
                  <h3 class="side-ttl @if($action=='invited') current @endif">
                      <a href="{{url('profile-plans-invited')}}">
                        Plans Invitations
                      </a>
                  </h3>
                  <h3 class="side-ttl @if($action=='memory') current @endif">
                      <a href="{{url('profile-plans-memory')}}">
                        Memory Trip Plans
                      </a>
                  </h3>
                </div>
              </div>
        <div class="post-side-top">
            <div class="post-top-txt horizontal">
                
                <button type="button" onclick="location.href='{{ route('trip.plan', 0) }}';"
                        class="btn btn-light-primary btn-bordered"
                        style="margin-left: 20px;">@lang("profile.new_trip_plan")
                </button>
                
                <button type="button" onclick="location.href='{{ url('trip/plan/0').'?memory=1' }}';"
                        class="btn btn-light-primary btn-bordered"
                        style="margin-left: 20px;">Memory
                </button>

            </div>
            <div class="side-right-control">
                <form action='' method='get'>
                    <div class="sort-by-select">
                        <label>@lang("other.sort_by")</label>
                        <div class="sort-select-wrap">
                            <select class="form-control" id="sort" name='sort'
                                    onchange='javascript:this.form.submit();'>
                                <option @if(isset($sort) AND $sort=="Upcoming") selected @endif>
                                    @lang('profile.upcoming')
                                </option>
                                <option @if(isset($sort) AND $sort=="Creation Date") selected @endif>
                                    @lang('profile.creation_date')
                                </option>
                                <option @if(isset($sort) AND $sort=="Title") selected @endif>@lang('profile.title')
                                </option>
                            </select>
                            <i class="trav-caret-down"></i>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="trip-plan-inner">

            @foreach($all_plans AS $inv)
                    <div class="trip-plan-row">
                        <div class="trip-plan-inside-block">
                            <div class="trip-plan-map-img" style="width:140px;">
                                <img src="{{get_plan_map($inv, 140, 150)}}" alt="map-image"
                                     style="width:140px;height:150px;">

                                <div class="dropdown">
                                    <button class="btn btn-light-primary btn-bordered" type="button"
                                            data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        <i class="trav-pencil"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                                        <a class="dropdown-item"
                                           href="{{ url_with_locale('trip/plan/'.$inv->id.'?do=edit') }}">
                                            <div class="drop-txt">
                                                <p>@lang('profile.edit')</p>
                                            </div>
                                        </a>
                                        <a class="dropdown-item delete-trip" href="#" data-toggle="modal" data-target="#deleteTripModal"
                                           data-id="{{$inv->id}}">
                                            <div class="drop-txt">
                                                <p>@lang('buttons.general.crud.delete')</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-plan-inside-block trip-plan-txt">
                            <div class="trip-plan-txt-inner">
                                <div class="trip-txt-ttl-layer">
                                    <h2 class="trip-ttl"><a
                                                href="{{ url_with_locale('trip/plan/'.$inv->id) }}">{{$inv->title}}</a>
                                    </h2>
                                    <p class="trip-date">{{weatherDate($inv->myversion()[0]->start_date)}}
                                        @lang('chat.to') {{weatherDate($inv->myversion()[0]->end_date)}}</p>
                                    <p class="trip-date" style='margin:0px'>@lang('profile.invited')
                                        @lang('chat.by'): {{$inv->author->name}}</p>
                                </div>
                                <div class="trip-txt-info">
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>{{dateDiffDays($inv->myversion()[0]->start_date, $inv->myversion()[0]->end_date)}}</b>
                                            </p>
                                            <p>@lang('book.duration')</p>
                                        </div>
                                    </div>
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>{{calculate_distance($inv->id)}}</b></p>
                                            <p>@lang('profile.distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-plan-inside-block">
                            <div class="dest-trip-plan">
                                <h3 class="dest-ttl">@lang('profile.destination') <span>{{count($inv->myversion()[0]->cities)}}</span>
                                </h3>
                                <ul class="dest-image-list" style="width:170px;">
                                    @foreach($inv->myversion()[0]->cities AS $city)
                                        <li>
                                            <img src="{{@check_city_photo($city->getMedias[0]->url)}}"
                                                 alt="photo" style="width:52px;height:52px;">
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>
        @endforeach



        <div class="bottom-load-mark">
                  {{$all_plans->links()}}
        </div>
                
        </div>
    </div>

        </div>

        <!-- SIDEBAR -->
        @include('site.profile._profile_sidebar')
    </div>
</div>

    
@endsection


@section('before_site_script')
    @parent
    <script src="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.js')}}"></script>
@endsection
