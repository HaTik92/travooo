<div class="sidebar-layer" id="sidebarLayer">
    <aside class="sidebar">

        <div class="post-block post-side-block">
            <div class="post-side-top expert-side">
                <h3 class="side-ttl" data-toggle="modal" data-target="#reportsPopup"><i class="trav-reports-icon"></i> Reports <span class="count">{{count($user->reports)}}</span></h3>
                <div class="side-right-control">
                    <a href="#" class="see-more-link" data-toggle="modal" data-target="#reportsPopup">See all</a>
                </div>
            </div>
        </div>

        <div class="post-block post-side-block">
            <div class="post-side-top">
                <h3 class="side-ttl" data-toggle="modal" data-target="#badgesPopup">Badges <span class="count">{{count($user->badges)}}</span></h3>
                <div class="side-right-control">
                    <a href="#" class="see-more-link" data-toggle="modal" data-target="#badgesPopup">All</a>
                </div>
            </div>
            <div class="side-badge-inner">

                @foreach($user->badges AS $badge)
                <div class="badge-block">
                    <div class="badge-icon">
                        <img src="{{url_exists(asset('assets2/image/badges/'.$badge->badge->id.'_'.$badge->level.'.png'))?asset('assets2/image/badges/'.$badge->badge->id.'_'.$badge->level.'.png'):asset('assets2/image/badges/'.$badge->badge->id.'_'.($badge->level+1).'.png')}}" width="37px" height="37px" style="border-radius: 0;">
                    </div>
                    <div class="badge-ttl">
                        {{$badge->badge->name}}
                    </div>
                </div>
                @endforeach

            </div>
        </div>

        <div class="post-block post-side-block">
            <div class="side-expert-link-inner">
                <div class="expert-side-link">
                    <a href="{{@$user->website}}" class="profile-website-link" style="display:inline" target="_blank">
                        <span class="round-icon">
                            <i class="trav-link"></i>
                        </span>
                        <span>{{@$user->website}}</span>
                    </a>
                </div>
                <div class="expert-side-link">
                    <ul class="profile-social-list">
                        <li>
                            <a href="{{@$user->facebook}}" class="icon facebook" target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{@$user->twitter}}" class="icon twitter" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{@$user->instagram}}" class="icon instagram" target="_blank">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
<!--
        <div class="share-page-block">
            <div class="share-page-inner">
                <div class="share-txt">Share this page</div>
                <ul class="share-list">
                    <li>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-code"></i></a>
                    </li>
                </ul>
            </div>
        </div>-->

        <div class="aside-footer">
            <ul class="aside-foot-menu">
                <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                <li><a href="{{url('/')}}">Advertising</a></li>
                <li><a href="{{url('/')}}">Cookies</a></li>
                <li><a href="{{url('/')}}">More</a></li>
            </ul>
            <p class="copyright">Travooo &copy; 2017</p>
        </div>
    </aside>
</div>