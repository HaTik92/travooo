@extends('site.profile.template.profile')
@php $title = 'Travooo - Profile Reviews'; @endphp
@section('content')
<link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
<div class="container-fluid">

    @include('site.profile._profile_header')

    <div class="custom-row">
        <!-- MAIN-CONTENT -->
        <div class="main-content-layer">
            <div class="post-block post-top-bordered">
        <div class="post-side-top top-tabs arrow-style">
            <div class="post-top-txt">
                <h3 class="side-ttl current">@lang('profile.all_reviews') <span
                            class="count review-count">{{$count}}</span></h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <label>@lang('other.sort_by'):</label>
                    <div class="sort-select-wrap">
                        <select class="sort-select review-sort review-select" data-sorted_by="review" placeholder="Newest">
                            <option value="date_down">Newest</option>
                            <option value="date_up">Oldest</option>
                        </select>
                    </div>
                </div>
                <div class="sort-by-select">
                    <label>@lang('profile.score'):</label>
                    <div class="sort-select-wrap score-select-wrap">
                         <select class="sort-select review-select review-score" data-sorted_by="score"  placeholder="All">
                            <option value="all"></option>
                            <option value="1.0"></option>
                            <option value="2.0"></option>
                            <option value="3.0"></option>
                            <option value="4.0"></option>
                            <option value="5.0"></option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="review-inner-layer review-inner-content">
            @if($count)
            <div class="review-search-result-block"></div>
                <div class="review-body-block">
                    @include('site.profile.partials._review_block', ['reviews'=>$reviews])
                </div>
                <div class="review-block review-loader-block" id="review_loader">
                     <input type="hidden" id="review_pagenum" value="1">
                     <div class="review-top">
                        <div class="top-main">
                           <div class="review-animation-avatar animation"></div>
                           <div class="review-txt">
                               <div class="review-animation-top-name animation"></div>
                               <div class="review-animation-top-info animation"></div>
                           </div>
                        </div>
                     <div class="btn-animation-wrap animation"></div>
                     </div>
                     <div class="review-foot">
                        <div class="review-animation-avatar animation"></div>
                        <div class="review-txt pl-3">
                            <div class="review-f-animation-top-name animation"></div>
                            <div class="review-f-animation-top-info animation"></div>
                        </div>
                     </div>
                </div>
            @else
                <div>There are no reviews...</div>
            @endif
        </div>
    </div>

        </div>

        <!-- SIDEBAR -->
         <div class="sidebar-layer" id="sidebarLayer">
            <aside class="sidebar">
                @include('site.profile.partials._aside_posts_block')
                <div class="post-block post-side-profile sm-profile">
                  <div class="post-map-block review-map-block">
                    <div class="post-map-inner">
                        <div id="reviewMap" style='width:359px;height:320px;'></div>

                    </div>
                </div>
                </div>
            </aside>
        </div>
    </div>
</div>

@endsection



@section('after_scripts')
@include('site.profile.template._select_scripts')
@include('site.profile._scripts')
@include('site.profile.partials._review_modal_block')
<script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
<script src="https://npmcdn.com/@turf/turf@5.1.6/turf.min.js"></script>
<script type="text/javascript" src="{{ asset('assets2/js/slick.min.js') }}"></script>
<script>
    
    //Review media 
    $(document).on('click', '.review-media-wrap, .review-play-btn', function(){
        var review_id = $(this).closest('ul.review-image-list').attr('reviewId')
        var media_id = $(this).attr('reviewMediaId')
        $.ajax({
            method: "POST",
            url: "{{ route('profile.get_review_media')}}",
            data: {review_id: review_id}
        })
            .done(function (res) {
                $('.review-media-modal-block').html(res.media_view)
        
               $(".review__cards-items").not('.slick-initialized').slick({
                    centerMode: !0,
                    variableWidth: !0,
                    focusOnSelect: !0,
                    infinite: !1,
                    prevArrow: '<button class="review-cards-control review__cards-control--prev"><svg class="icon icon--angle-left"> <use xlink:href="{{asset('assets3/img/sprite.svg#angle-left')}}"></use> </svg></button>',
                    nextArrow: '<button class="review-cards-control review__cards-control--next"><svg class="icon icon--angle-right"> <use xlink:href="{{asset('assets3/img/sprite.svg#angle-right')}}"></use> </svg></button>'
                });

                $('.review-gallery-block').find('.lg-item').addClass('d-none')  
                $('.review-inner-' + media_id).removeClass('d-none')

                $('.r-card-' + media_id).addClass('slick-current').addClass('slick-center').click()  

                $('#profile_review_modal').show()
            });
                
    })
    
    $(document).on('click', '.v-modal-close', function () {
        $('#profile_review_modal').hide()
    })
    
    function getReviewMediaModal(id, obj) {
          
    $('.review-gallery-block').find('.lg-item').addClass('d-none')  
    $('.review-inner-' + id).removeClass('d-none')
    
    }
    
     $(document).on('click', '.review-video-autoplay', function () {
            togglePlay($(this)[0], $(this).attr('id'));
    })
    
    
    $(document).on('click', '.r-play-btn', function () {
        var r_id = $(this).attr('data-r_id')
        var video = document.getElementById('lg_video_' + r_id);

        togglePlay(video, 'lg_video_' + r_id);
    })
    
    function togglePlay(video, id) {
        $("video").each(function () {
            if($(this).attr('id') != id){
                $(this).get(0).pause();
            }
        });
        
        video.addEventListener('pause', (event) => {
            if(event.target.id)
            $('.' + event.target.id).show()
        });

        if (video.paused || video.ended) {
            if(video.currentTime == 5){
                 video.currentTime = 0;
            }
            video.play();
            $('.' + id).hide()
        } else {
            video.pause();
            $('.' + id).show()
        }
    }
    
    //Review aside map
    var map, markers = position = [];

    initMap();

    function generateMarker(lnglat, map, img, args, count, place_id, place_name) {
        var div = document.createElement('div');
        div.className = "dest-img-block";
        div.innerHTML = "<img class='review-map-img' src='" + img + "'><div class='count-wrap'><span>"+ count +"</span></div>";
        div.style.position = 'absolute';

        if (typeof (args.marker_id) !== 'undefined') {
            div.dataset.marker_id = args.marker_id;
        }

        div.addEventListener('click',function() {
            filterReview(review_sort, review_score, place_id, place_name)
        });

        var marker = new mapboxgl.Marker(div)
            .setLngLat(lnglat)
            .addTo(map);

        return marker;
    }

    function initMap() {
        var bounds = new mapboxgl.LngLatBounds();
        var mapBounds = [
            [-170, -85],
            [170, 85]
        ];

        mapboxgl.accessToken = '{{config('mapbox.token')}}';

        map = new mapboxgl.Map({
            container: 'reviewMap',
            style: 'mapbox://styles/mapbox/satellite-streets-v11',
            maxBounds: mapBounds,
            attributionControl: false
        });

        @if(count($reviews_for_map) > 0)
            @foreach($reviews_for_map AS $review)
                @if (!$review->place)
                    @continue
                @endif
                var lnglat = [parseFloat('{{$review->place->lng}}'), parseFloat('{{$review->place->lat}}')];

                var overlay{{$review->places_id}} = generateMarker(
                    lnglat,
                    map,
                    "{{(count($review->place->getMedias)>0)?'https://s3.amazonaws.com/travooo-images2/th1100/'.$review->place->getMedias[0]->url:'http://s3.amazonaws.com/travooo-images2/placeholders/place.png'}}",
                    {
                        marker_id: '{{$review->places_id}}',
                        colour: 'Red'
                    },
                    "{{$review->total}}",
                    "{{$review->places_id}}",
                    '{{$review->place->transsingle->title}}'
                );

                markers.push(overlay{{$review->places_id}});
                bounds.extend(lnglat);
            @endforeach
        @else
            map.setZoom(1);
        @endif

        map.fitBounds(bounds, {
            padding: {top: 50, bottom:50, left: 50, right: 50},
            maxZoom: 10
        });
    }
    </script>
@endsection
