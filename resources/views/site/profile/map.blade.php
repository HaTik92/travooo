@extends('site.profile.template.profile')
@php $title = 'Travooo - Profile My Map'; @endphp
@section('content')
<link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
<style>
    .profile-map .cluster img{
        width:30px;
        height: 30px;
        border-radius: 50%;
        border: 2px solid white;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
    }

    .profile-map .cluster span {
        position: relative;
        background: #fff;
        border-radius: 50px;
        width: 15px;
        height: 15px;
        display: block;
        padding-top: 3px;
        left: 20px;
        top: -3px;
        font-size: 10px;
    }
</style>

<div class="container-fluid">

    @include('site.profile._profile_header')


    <div class="custom-row">
        <!-- MAIN-CONTENT -->
        <div class="main-content-layer">
            <div class="post-block post-map-wrapper">
                <div class="post-map-block">
                    <div class="post-map-inner">
                        <div id="myMap" class="profile-map" style='width:615px;height:770px;'></div>

                    </div>
                </div>
            </div>
        </div>

        <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
            <aside class="sidebar">
                @include('site.profile.partials._aside_posts_block')
                @if($user->about !='')
                <div class="post-block post-side-block">
                    <div class="post-side-top">
                        <h3 class="side-ttl">Bio</h3>
                        @if($login_user_id != '')
                            <h3 class="side-ttl edit-about"><span class="edit">Edit</span></h3>
                        @endif
                    </div>
                    <div class="side-post-inner profile-about-inner" id="timline_list">
                        <p class="l-breack">{{$user->about}}</p>
                    </div>
                    <div class="side-post-inner profile-bio-block d-none" >
                        <form id="profile_about_form">
                            <textarea class="flex-text edit_textarea_about" required  id="profile_about_text"  maxlength="256" oninput="textarea_auto_height(this)">{{$user->about}}</textarea>
                        </form>
                    </div>
                </div>
                @endif
                @include('site.profile.partials._aside_link_block')
                <div class="aside-footer">
                    <ul class="aside-foot-menu">
                        <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                        <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                        <li><a href="{{url('/')}}">Advertising</a></li>
                        <li><a href="{{url('/')}}">Cookies</a></li>
                        <li><a href="{{url('/')}}">More</a></li>
                    </ul>
                    <p class="copyright">Travooo &copy; 2017 - {{date('Y')}}</p>
                </div>

            </aside>
        </div>
    </div>
</div>
@include('site/trip2/partials/modal-skyscanner')
@endsection

@section('after_scripts')
<script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
<script src="https://npmcdn.com/@turf/turf@5.1.6/turf.min.js"></script>
<script>
    var map;
    initMap();

    function generateMarker(lnglat, map, img, place_name, args, type) {
        var mark_type = '';
        var marker_icon = '';
        switch(type) {
            case 'checkin':
               mark_type = 'map-checkin-wrap';
               marker_icon = "<i class='fa fa-check' aria-hidden='true'>";
              break;
            case 'place':
               mark_type = 'map-place-wrap';
               marker_icon = "<i class='fa fa-map-marker' aria-hidden='true'></i>";
              break;
            case 'followers':
               mark_type = 'map-followers-wrap';
               marker_icon = "<i class='fa fa-user-plus' aria-hidden='true'>";
              break;
        }
        var div = document.createElement('div');
        div.className = "dest-img-block";
        div.innerHTML = "<img class='dest-img' src='" + img + "' alt='' style='width:30px;height:30px;'><div class='icon-wrap "+ mark_type +"'><ul class='icon-list'><li>"+ marker_icon +"</i></li></ul></div>";
        div.style.position = 'absolute';

        if (typeof (args.marker_id) !== 'undefined') {
            div.dataset.marker_id = args.marker_id;
        }

        var marker = new mapboxgl.Marker(div)
            .setLngLat(lnglat)
            .addTo(map);

        return marker;
    }

    function initMap() {
        var bounds = new mapboxgl.LngLatBounds();
        var mapBounds = [
            [-180, -85],
            [180, 85]
        ];

        mapboxgl.accessToken = '{{config('mapbox.token')}}';

        map = new mapboxgl.Map({
            container: 'myMap',
            style: 'mapbox://styles/mapbox/satellite-streets-v11',
            maxBounds: mapBounds
        });

        //to use it
        var markers = [];
        @foreach($happenings AS $checkin)
            @if ($checkin['lat'] && $checkin['lng'])
                var lnglat = [{{$checkin['lng']}}, {{$checkin['lat']}}];

                var overlay{{$checkin['post_id']}} = generateMarker(
                    lnglat,
                    map,
                    "{{$checkin['profile_picture']}}",
                    "{{$checkin['place_name']}}",
                    {
                        marker_id: '{{$checkin["post_id"]}}',
                        colour: 'Red'
                    },
                    "{{$checkin['type']}}"
                );

                markers.push(overlay{{$checkin['post_id']}});
                var popup = new mapboxgl.Popup()
                    .setHTML(getContentString("{{$checkin['name']}}", "{{$checkin['place_name']}}", "{{$checkin['place_id']}}", "{{$checkin['photo']}}", "{{$checkin['date']}}", "{{$checkin['type']}}", "{{$checkin['city_name']}}", "{{$checkin['raw_date']}}"));

                overlay{{$checkin['post_id']}}.setPopup(popup);

                bounds.extend(lnglat);
            @endif
        @endforeach

        map.fitBounds(bounds, {
            padding: {top: 50, bottom:50, left: 50, right: 50},
            maxZoom: 10
        });
    }

    function getContentString (user_name, place_name, place_id, place_img, date, type, city_name, raw_date){
        var type_text ='';
        switch(type) {
            case 'checkin':
               type_text = 'checked in at';
              break;
            case 'place':
               type_text = 'traveled to';
              break;
            case 'followers':
               type_text = 'follow from';
              break;
        }
        
        var content = '<div class="gm-iw-block">'+
                            '<div class="gm-iw-image-wrap"><img src="'+ place_img +'"></div>'+
                            '<div class="gm-iw-content-wrap">'+
                                '<p><span class="author-name">'+ user_name +'</span> <span class="pl-type">'+ type_text +'</span></p>'+
                                '<a href="/place/'+ place_id +'" target="_blank" class="place-name">'+ place_name +'</a>'+
                                '<p class="gm-iw-time">on '+ date +'</p>'+
                            '</div>'+
                            '<div class="gm-iw-content-btns">'+
                                '<a href="javascript:;" data-date="'+ raw_date +'" data-city="' + city_name + '" class="fly-to map-iw-link mb-2"><svg class="icon icon--plane"><use xlink:href="{{asset('assets3/img/sprite.svg#plane')}}"></use></svg> <span>fly to</span></a>'+
                                '<a href="javascript:;" data-name="' + place_name + '" data-date="' + raw_date + '" data-city="' + city_name + '" class="prices map-iw-link"><svg class="icon icon--tag"><use xlink:href="{{asset('assets3/img/sprite.svg#tag')}}"></use></svg> <span>prices</span></a>'+
                            '</div>'
                        '</div>';

        return content;
    }

    $(document).on('click', 'a.fly-to', function() {
        var city = $(this).data('city');
        var date = $(this).data('date');

        $('#modal-skyscanner .user-list').html('                    <div data-skyscanner-widget="SearchWidget"\n' +
            '                         data-origin-geo-lookup="true"\n' +
            '                         data-currency="USD"\n' +
            '                         data-destination-name="\'' + city + '\'"\n' +
            '                         data-target="_blank"\n' +
            '                         data-button-colour="#db0000"\n' +
            'data-flight-outbound-date="' + date + '"' +
            '></div>');

        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "{{ asset('assets2/js/skyloader.js')}}";

        $('#modal-skyscanner .user-list').append(s);

        setTimeout(function() {
            $('#modal-skyscanner').modal('show');
        }, 1000);
    });

    $(document).on('click', 'a.prices', function() {
        var city = $(this).data('city');
        var date = $(this).data('date');
        var name = $(this).data('name');

        $('#modal-skyscanner .user-list').html('    <div data-skyscanner-widget="HotelSearchWidget"\n' +
            '        data-locale="en-GB"\n' +
            '        data-currency="USD"\n' +
            '        data-target="_blank"\n' +
            '        data-destination-name="\'' + city + '\'"\n' +
            '        data-button-colour="#db0000"\n' +
            'data-locale="en-GB"' +
            'data-hotel-check-in-date="' + date + '"' +
            '        data-responsive="true"\n' +
            '            ></div>');

        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "https://widgets.skyscanner.net/widget-server/js/loader.js";

        $('#modal-skyscanner .user-list').append(s);

        setTimeout(function() {
            $('#modal-skyscanner').modal('show');
        }, 1000);
    });

</script>
<script src="{{ asset('/assets3/js/profile.js?ver='.time()) }}"></script>
@endsection
