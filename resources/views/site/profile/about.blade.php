@extends('site.profile.template.profile')
@php $title = 'Travooo - About Page'; @endphp
@section('content')

<div class="container-fluid">
    @include('site.profile._profile_header')
    @include('site.profile.helper.edit_inputs')


    <div class="custom-row">
        <!-- MAIN-CONTENT -->
        <div class="main-content-layer" style="position: relative;">
            <form method="post" action="{{url_with_locale('settings/account')}}" autocomplete="off" id="profile-form" data-expert-type="{{$user->type == 2?'1':'0'}}">
                <div class="post-block post-profile-about-block post-form-wrapper">
                    <div class="post-side-top name-block flex-custom f-wrap">
                        <h3 class="side-ttls" style="font-size:{{calculate_charackter_font_size($user->name, 'about')}}px;">{{$user->name}} <span class="about-uername">{{$user->username?'(@'.$user->username.')':''}}</span></h3>

                        <div class="expert-rigth-link about-edit-link">
                            @if($login_user_id != '')
                            <a href="" class="edit-link">Edit</a>
                            @endif
                        </div>
                        <div class="error-block">
                            <label id="name-error" class="error" for="name" style=""></label>
                            <label id="username-error" class="error" for="username"></label>
                        </div>
                    </div>
                   
                    <div class="post-content-inner">
                        <div class="post-form-wrapper">
                            <div class="row bb-gray">
                                <label for="" class="col-sm-3 col-form-label">Bio</label>
                                <div class="col-sm-9">
                                    <div class="form-txt about-block about-text flex-custom">

                                        <p class="l-breack">@if($user->about!=''){{$user->about}}@else <span class="bio-info">No bio added...</span> @endif</p>
                                    </div>
                                </div>
                            </div>
                            @if($user->type == 2)
                            @php
                                if($user->expert != 1 ){
                                    if($login_user_id != '')
                                        $check_exp = true;
                                    else
                                        $check_exp = false;
                                }else{
                                 $check_exp = true;
                                }
                            @endphp
                                @if($check_exp)
                                    <div class="row prof-exp-content {{count($expertise) == 0?'empty-row':''}} {{count($user->travelstyles) == 0?'bb-gray':''}}">
                                        <label for="" class="col-sm-3 col-form-label">Expertise</label>
                                        <div class="col-sm-9">
                                            <div class="form-txt expertise-block" expId="{{implode('-:', $user_expertise)}}">
                                                @if(count($expertise)>0)
                                                @php $num_output = 0; @endphp
                                                @foreach($expertise as $val)
                                                @if($num_output < 3)
                                                <a href="javascript:;" class="form-link">{{$val['text']}}</a>@if(!$loop->last),&nbsp;@endif
                                                @elseif($num_output == 3)
                                                <a href="javascript:;" class="form-link show-more" style="cursor:pointer;">+{{count($expertise)-3}} More</a>
                                                <a href="javascript:;" class="form-link more-links d-none">{{$val['text']}}</a>@if(!$loop->last)<span class="more-links d-none">,&nbsp;</span>@endif
                                                @else
                                                <a href="javascript:;" class="form-link more-links d-none">{{$val['text']}}</a>@if(!$loop->last)<span class="more-links d-none">,&nbsp;</span>@endif
                                                @endif
                                                @php $num_output ++;@endphp
                                                @endforeach
                                                @else
                                                <p>n/a</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif
                            <div class="row bb-gray prof-tstyle-content {{count($user->travelstyles) == 0?'empty-row':''}}">
                                <label for="" class="col-sm-3 col-form-label">Travel styles</label>
                                <div class="col-sm-9">
                                    <div class="form-txt travelstyle-block" styleId="{{implode(',', $styles_list)}}">
                                        @if(count($user->travelstyles)>0)
                                        @php $num_output = 0; @endphp
                                            @foreach($user->travelstyles as $val)
                                            @if($num_output < 3)
                                            <a href="javascript:;" class="form-link">{{$val->travelstyle->trans[0]->title}}</a>@if($loop->index != $loop->count-2 && !$loop->last),&nbsp;@endif @if($loop->index == $loop->count-2) & @endif
                                            @elseif($num_output == 3)
                                            <a href="javascript:;" class="form-link show-more" style="cursor:pointer;">+{{count($user->travelstyles)-3}} More</a>
                                            <a href="javascript:;" class="form-link more-links d-none">{{$val->travelstyle->trans[0]->title}}</a>@if(!$loop->last)<span class="more-links d-none">,&nbsp;</span>@endif
                                            @else
                                            <a href="javascript:;" class="form-link more-links d-none">{{$val->travelstyle->trans[0]->title}}</a>@if($loop->index != $loop->count-2 && !$loop->last)<span class="more-links d-none">,&nbsp;</span>@endif @if($loop->index == $loop->count-2)<span class="more-links d-none"> & </span>@endif
                                            @endif
                                            @php $num_output ++;@endphp
                                            @endforeach
                                        @else
                                        <p>n/a</p>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            
                            <div class="row prof-email-content  {{$user->contact_email ==''?'empty-row':''}}">
                                <label for="" class="col-sm-3 col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <div class="form-txt flex-custom email-block" style="display: block;">
                                        @if($user->contact_email!='')
                                        {{$user->contact_email}}
                                        @else
                                        n/a
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row prof-phone-content {{$user->mobile ==''?'empty-row':''}}">
                                <label for="" class="col-sm-3 col-form-label">Phone</label>
                                <div class="col-sm-9">
                                    <div class="form-txt flex-custom phone-block" style="width: 100%;">
                                        @if($user->mobile!='')
                                        {{$user->mobile}}
                                        @else
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row {{$user->nationality ==''?'empty-row':''}}">
                                <label for="" class="col-sm-3 col-form-label">Nationality</label>
                                <div class="col-sm-9">
                                    <div class="form-txt flex-custom nationality-block">
                                        @if($user->nationality!='')
                                        {{@$user->country_of_nationality->transsingle->title}}
                                        @else
                                        n/a
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label for="" class="col-sm-3 col-form-label">@lang('profile.dob')</label>
                                <div class="col-sm-9">
                                    <div class="form-txt flex-custom">
                                        @if($user->birth_date)
                                        <p>{{ \Carbon\Carbon::parse($user->birth_date)->format('d M Y') }}</p>
                                        @else
                                        <p>n/a</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label for="" class="col-sm-3 col-form-label">Gender</label>
                                <div class="col-sm-9">
                                    <div class="form-txt flex-custom">

                                        <p>
                                            @if($user->gender==1)
                                            @lang('setting.male')
                                            @elseif($user->gender==2)
                                            @lang('setting.female')
                                            @else
                                            @lang('setting.unspecified')
                                            @endif
                                        </p>

                                    </div>
                                </div>
                            </div>

                     
                            <div class="row prof-msince-content  {{$user->interests !=''?'bb-gray':''}}">
                                <label for="" class="col-sm-3 col-form-label">@lang('profile.member_since')</label>
                                <div class="col-sm-9">
                                    <div class="form-txt">
                                        <p>{{@$user->created_at->format('M, Y')}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row prof-interest-content {{$user->interests ==''?'empty-row':''}}">
                                <label for="" class="col-sm-3 col-form-label">Interests</label>
                                <div class="col-sm-9">
                                     <div class="form-txt flex-custom interest-block f-wrap" uInterest="{{$user->interests}}">
                                        @if($user->interests != '')
                                        <p>{{ str_replace(',', ', ',$user->interests) }}</p>
                                        @else
                                        <p></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>




        <!-- SIDEBAR -->

        <div class="sidebar-layer" id="sidebarLayer">
            <aside class="sidebar">
              @include('site.profile.partials._aside_posts_block')
              @include('site.profile.partials._aside_link_block')
                <div class="aside-footer">
                    <ul class="aside-foot-menu">
                        <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                        <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                        <li><a href="{{url('/')}}">Advertising</a></li>
                        <li><a href="{{url('/')}}">Cookies</a></li>
                        <li><a href="{{url('/')}}">More</a></li>
                    </ul>
                    <p class="copyright">Travooo &copy; 2017 - {{date('Y')}}</p>
                </div>
            </aside>
        </div>
    </div>
</div>


@endsection

@section('after_scripts')

<script src="//unpkg.com/jscroll/dist/jquery.jscroll.min.js"></script>

<script>
/*** Handle jQuery plugin naming conflict between jQuery UI and Bootstrap ***/
jQuery.fn.bstooltip = jQuery.fn.tooltip; 
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('/assets3/js/profile.js?ver='.time()) }}"></script>

<script>
$(document).ready(function () {
    $('.modal-660').jscroll();
});
</script>

@endsection
