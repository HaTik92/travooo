<script>
var sort_type = '';
$(document).ready(function(){
   getTimelineMedia('month');
   
//photos load more
$(document).on('inview', '#hide_photo_loader', function(event, isInView) {
     if (isInView) {
         loadMorePhoto(sort_type)
     }
 });
 
})

//load more photo function
function loadMorePhoto(sort){
    var nextPage = parseInt($('#photo_pagenum').val()) + 1;
    var user_id = '{{$user_id}}';

    $.ajax({
        type: 'POST',
        url: "{{route('profile.load_more_photo')}}",
        data: {pagenum: nextPage, user_id:user_id, sort:sort},
        success: function (data) {
            if(data != ''){
                  $('.p-photo-block').append(data);
                  $('#photo_pagenum').val(nextPage);
              } else {
                  $("#loadMorePhoto").addClass('d-none');
              }
        }
    });
}

     //photos lightGallery
    $(document).on('click', '.lightbox_imageModal', function () {
        var slideNum = $(this).attr('data-id');
       
        let $lg = $(this).lightGallery({
            dynamic: true,
            index: parseInt(slideNum),
            dynamicEl: [
                    @foreach($lightbox_photos AS $photo)
                {

                    "src": "{{ get_profile_media($photo->media->url) }}",
                    'thumb': "{{ get_profile_media($photo->media->url) }}",
                    'subHtml': `@include('site.profile.partials._photo_modal_block')`},
                    @endforeach
            ],
            addClass: 'main-gallery-block',
            pager: false,
            share: false,
            hideControlOnEnd: true,
            speed: 0,
            loop: true,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
        });

        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });

        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');
            let currentCommentIs = $(subTtl).hasClass('comment-block');
            let currentImgPos = $(currentImg).position().top;

                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                if (currentCommentIs) {
                    // console.log('yes');
                    $(currentImgWrap).css('padding-right', commentWidth);
                    // $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    // $(subTtl).css('width', currentWidth);
                }
                // $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();

        }

        $lg.on('onSlideItemLoad.lg', function (e) {

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            })
        });

        $lg.on('onAfterSlide.lg', function () {
            let media_id = $('.main-gallery-block .lg-current').find('.cover-block').attr('id');
            let type = $('.main-gallery-block').find('.'+ media_id +'-media-like-icon');
            $('.'+ media_id +'-like-count b').html($('.'+ media_id +'-media-liked-count b').html())
            Comment_Show.init($('.photo-comment-' + media_id));

            if(type.attr('data-type') == 'like'){
                type.html('<i class="trav-heart-fill-icon" style="color:#ff5a79;"></i>')
            }else{
                type.html(' <i class="trav-heart-icon"></i>')
            }
            setWidth();
        });
    });
    $('body').click(function(evt){
       if(evt.target.className == "lg-inner")
          $('.lg-close.lg-icon').click();
    });

    //photos load more
    var LoadMore_Show = (()=>{
    var total_rows = 0;
    var display_unit = 6;
    function init(obj){
        var target = $(obj).find('.sortBody');
        if(target.attr('report-photo-rows'))
        {
            return;
        }
        else
        {
           if(target.find(">div").hasClass('media-not-found')){
               return;
           }else{
            target.find(">div").hide();
            var btn = $('<a href="javascript:;"/>').addClass('load-more-link d-inline-block mt-2').text('Load more...').click(function(){
                LoadMore_Show.inc(target)
            });


            get_totalrows(target, btn)

        }
        }
    }

    function reload(obj)
    {
        var target = $(obj).find('.sortBody');
        target.find(">div").hide();
        target.find(">div").removeClass('displayed');
        get_totalrows(target);
    }

    function get_totalrows(obj, btn)
    {
        this.total_rows = obj.find(">div").length;
        obj.attr('report-photo-rows', this.total_rows);
        if(!obj.attr('report-photo-page'))
            obj.attr('report-photo-page', 1);
        show_rows(obj, obj.attr('report-photo-page'), btn);
    }

    function show_rows(obj, page, btn)
    {
        this.total_rows = parseInt(obj.attr('report-photo-rows'));
        var showing_rows = parseInt(page) * display_unit;
       if(this.total_rows>6){
           obj.after(btn);
       }
        showing_rows = this.total_rows > showing_rows ? showing_rows : this.total_rows;
        for(var i = 0; i < showing_rows; i++)
        {
            obj.find(">div").eq(i).show().addClass("displayed");
        }
    }

    function increase_show(obj)
    {
        var current_page = parseInt(obj.attr('report-photo-page'));
        obj.attr('report-photo-page', (current_page + 1));
        get_totalrows(obj);
    }


    return  {
        init: init,
        inc: increase_show,
        reload: reload
    }
})();

//photos sorting by type
function itemsSort(obj, type){
        var list, i, switching, shouldSwitch, switchcount = 0;
        switching = true;
        parent_obj = $('.post-media-list').find('.p-photo-block');
       
        while (switching) {
            switching = false;
            list = parent_obj.find('>div');
            shouldSwitch = false;
            for (i = 0; i < list.length - 1; i++) {
                shouldSwitch = false;
                if(type == "date_up")
                {
                    if ($(list[i]).attr('datesort') > $(list[i + 1]).attr('datesort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "date_down")
                {
                    if ($(list[i]).attr('datesort') < $(list[i + 1]).attr('datesort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "most_liked")
                {

                    if ($(list[i]).find('.liked-count b').html() < $(list[i + 1]).find('.liked-count b').html()) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "most_commented")
                {
                    if ($(list[i]).find('.media-commented-count b').html() < $(list[i + 1]).find('.media-commented-count b').html()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                $(list[i]).before(list[i + 1]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && list.length > 1 && i == 0) {
                    switching = true;
                }
            }
        }
      
    }

    // photos like & unlike photo
    $('body').on('click', '.photo_like_button', function (e) {
        mediaId = $(this).attr('id');
        $.ajax({
            method: "POST",
            url: "{{ route('media.likeunlike') }}",
            data: {media_id: mediaId}
        })
            .done(function (res) {
                var result = JSON.parse(res);

                if (result.status == 'yes') {
                    $('.'+ mediaId +'-like-count b').html(result.count)
                    $('.'+ mediaId +'-media-liked-count b').html(result.count)
                    $('.'+ mediaId +'-media-like-icon').html('<i class="trav-heart-fill-icon" style="color:#ff5a79;"></i>')
                    $('.'+ mediaId +'-media-like-icon').attr('data-type', 'like')

                } else if (result.status == 'no') {
                    $('.'+ mediaId +'-like-count b').html(result.count)
                    $('.'+ mediaId +'-media-liked-count b').html(result.count)
                    $('.'+ mediaId +'-media-like-icon').html('<i class="trav-heart-icon"></i>')
                    $('.'+ mediaId +'-media-like-icon').attr('data-type', 'unlike')
                }
            });
        e.preventDefault();
    });

        // photo comment ------start------

                // -----START----- Post comment
        $('body').on('mouseover', '.news-feed-comment', function () {
            $(this).find('.post-com-top-action .dropdown').show()
        });

        $('body').on('mouseout', '.news-feed-comment', function () {
            $(this).find('.post-com-top-action .dropdown').hide()
        });

        $('body').on('mouseover', '.doublecomment', function () {
            $(this).find('.post-com-top-action .dropdown').show()
        });

        $('body').on('mouseout', '.doublecomment', function () {
            $(this).find('.post-com-top-action .dropdown').hide()
        });


    $(document).on('change', '.profile-media-file-input', function (e) {
            var comment_id = $(this).attr('data-comment_id')
                commentMediaUploadFile($(this), $(".media_comment_form"), comment_id);

        })

    $(document).on('keydown', '*[data-id="mediacommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            e.preventDefault();
        }
    });

    $(document).on('keyup', '*[data-id="mediacommenttext"]', function(e){
        if(e.keyCode == 13 && !e.shiftKey){
            $(this).closest('form').submit();
        }
    });

    $(document).on('submit', '.media_comment_form', function(e){
        var form = $(this);
        var text = form.find('*[data-id="mediacommenttext"]').val().trim();
        var files = form.find('.medias').find('div').length;

        if(text == "" && files == 0)
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: '',
                    type: 'orange',
                    offsetTop: 0,
                    offsetBottom: 500,
                    content: 'Please input text or select files!',
                });
                return false;
            }
        var values = $(this).serialize();

        form.trigger('reset');
        var postId = form.find('input[name="medias_id"]').val();
        var all_comment_count = $('.'+ postId +'-all-comments-count').html();
        var url = '{{ route("media.comment", ":postId") }}';
        url = url.replace(':postId', postId);

        $.ajax({
            method: "POST",
            url: url,
            data: values
        })
            .done(function (res) {
                form.closest('.gallery-comment-wrap').find('.post-comment-wrapper').prepend(res);
                var comments = form.closest('.gallery-comment-wrap').find('.post-comment-wrapper').find('.post-comment-row').length;
                form.closest('.gallery-comment-wrap').find('.comm-count-info').find('strong').html(comments);
                form.find('.medias').find('.removeFile').each(function () {
                      $(this).click();
                  });
                $('.'+ postId +'-all-comments-count').html(parseInt(all_comment_count) + 1);
                $("#" + postId + ".comment-not-fund-info").remove();

                form.find('.medias').empty();
                form.find('textarea').val('');
                form.find('textarea').css({"height": "0", "padding-bottom": "0"});
                form.find('.post-create-controls').css('top', '15px');

                Comment_Show.reload($('.photo-comment-' + postId));
                form.trigger('reset');
            });
        e.preventDefault();
    });

    $('body').on('click', '.postmediaCommentLikes', function (e) {
            var _this = $(this);
            commentId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('media.commentlikeunlike') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'yes') {
                            _this.find('i').addClass('fill');
                            _this.parent().find('.media-comment-likes-block').prepend('<span>' + result.name + '</span>')
                        } else if (result.status == 'no') {
                            _this.find('i').removeClass('fill');
                            _this.parent().find('.media-comment-likes-block span').each(function () {
                                if ($(this).text() == result.name)
                                    $(this).remove();
                            });
                        }
                        _this.find('.comment-like-count').html(result.count);



                    });
            e.preventDefault();
        });


        $('body').on('click', '.media-comment-likes-block', function (e) {
            var _this = $(this);
            commentId = $(this).attr('data-id');
            var like_user_count = _this.parent().find('.comment-like-count').text()
            $.ajax({
                method: "POST",
                url: "{{ route('media.commentlikeusers') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        $('.post-comment-like-users').html(res)
                        $('#commentslikePopup').find('.comment-like-count').html(like_user_count)
                        $('#commentslikePopup').modal('show')
                    });
            e.preventDefault();
        });

        $('body').on('click', '.mediaCommentReply', function (e) {
            commentId = $(this).attr('id');
            $('#mediaReplyForm' + commentId).show();
            e.preventDefault();
        });


        $(document).on('change', '.media-file-input', function (e) {
            var comment_id = $(this).attr('data-comment_id')
                commentMediaUploadFile($(this), $(".mediaCommentReplyForm"+comment_id), comment_id);

        })

        $(document).on('keydown', '.media-comment-text', function (e) {
            if (e.keyCode == 13 && !e.shiftKey) {
                e.preventDefault();
            }
        })

        $(document).on('keyup', '.media-comment-text', function (e) {
            if (e.keyCode == 13 && !e.shiftKey) {
                var comment_id = $(this).attr('data-comment_id');
                replyMediaComment(comment_id)
            }
        })

    //photo comment delete
    $('body').on('click', '.postmediaCommentDelete', function (e) {

            var _this = $(this);
            var commentId = _this.attr('id');
            var postId = _this.attr('postid');
            var media_comments_count = $('.' + postId + '-all-comments-count').html();
            var reload_obj = $('.photo-comment-' + postId);
            var type = _this.data('type');
            var parent_id = _this.data('parent')

            $.confirm({
                title: 'Delete Comment!',
                content: 'Are you sure you want to delete this comment? <div class="mb-3"></div>',
                columnClass: 'col-md-5 col-md-offset-5',
                closeIcon: true,
                offsetTop: 0,
                offsetBottom: 500,
                scrollToPreviousElement:false,
                scrollToPreviousElementAnimate:false,
                buttons: {
                    cancel: function () {},
                    confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'shift'],
                        action: function () {
                            $.ajax({
                                method: "POST",
                                url: "{{ route('media.commentdelete') }}",
                                data: {comment_id: commentId}
                            })
                            .done(function (res) {

                                $('#mediaCommentRow' + commentId).fadeOut();
                                $('.' + postId + '-all-comments-count').html(parseInt(media_comments_count) - 1);

                                if (type == 1) {
                                    $('#mediaCommentRow' + commentId).parent().remove();
                                   Comment_Show.reload($('.photo-comment-' + postId));
                                } else {
                                    if (_this.closest('.post-block').find('*[data-parent_id="' + parent_id + '"]:visible').length == 1) {
                                        $('#mediaCommentRow' + parent_id).find('.postCommentDelete').removeClass('d-none')
                                        $('#mediaCommentRow' + parent_id).find('.delete-line').removeClass('d-none')
                                    }
                                }
                            });
                        }
                    }
                }
            });
        });

    // Photos comments votes
    $('body').on('click', '.media-comment-vote.up', function (e) {
      var comment_id = $(this).attr('id');
      $.ajax({
          method: "POST",
          url: "{{route('media.comment_updownvote')}}",
          data: {comment_id: comment_id, vote_type:1}
      })
          .done(function (res) {
               var up_result = JSON.parse(res);
              $('.upvote-'+comment_id+' b').html(up_result.count_upvotes);
              $('.downvote-'+comment_id+' b').html(up_result.count_downvotes);
          });
      e.preventDefault()
     });

     $('body').on('click', '.media-comment-vote.down', function (e) {
      var comment_id = $(this).attr('id');
      $.ajax({
          method: "POST",
          url: "{{route('media.comment_updownvote')}}",
          data: {comment_id: comment_id, vote_type:2}
      })
          .done(function (res) {
              var down_result = JSON.parse(res);
              $('.upvote-'+comment_id+' b').html(down_result.count_upvotes);
              $('.downvote-'+comment_id+' b').html(down_result.count_downvotes);
          });
      e.preventDefault()
     });


    // photo comment soted by upvote count and date
    $('body').on('click', '.comment-filter-type', function () {
          var comment_type = $(this).data('type');
          var media_id = $(this).closest('.post-comment-layer').find('.post-comment-wrapper').attr('data-id');

        commentSort(comment_type, $(this), $('.photo-comment-' + media_id))
    })

    function comment_textarea_auto_height(element, type) {
        element.style.height = "5px";
        element.style.paddingBottom = '30px';
        if (type == 'reply') {
            jQuery(element).closest('.post-reply-block').find('.post-create-controls').css('top', (element.scrollHeight - 25) + "px")
        } else if (type == 'edit') {
            jQuery(element).closest('.post-edit-block').find('.post-create-controls').css('top', (element.scrollHeight - 15) + "px")
        } else {
            jQuery(element).closest('.post-regular-block').find('.post-create-controls').css('top', (element.scrollHeight - 25) + "px")
        }
        element.style.height = (element.scrollHeight) + "px";
    }

    $(document).on('click', 'i.fa-camera', function () {
        $("#createPosTxt").click();
        var target = $(this).attr('data-target');
        var target_el = $('#' + target);
        target_el.trigger('click');
    })

     // photo comment ------end------


    function replyMediaComment(id) {
        var form = $('.mediaCommentReplyForm' + id);
        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        var values = form.serialize();

        $.ajax({
            method: "POST",
            url: "{{ route('media.commentreply') }}",
            data: values
        })
                .done(function (res) {
                    form.parent().parent().before(res);
                    form.find('.medias').find('.removeFile').each(function () {
                        $(this).click();
                    });
                    $('#mediaCommentRow' + id).find('.postCommentDelete').addClass('d-none');
                    $('#mediaCommentRow' + id).find('.delete-line').addClass('d-none');
                    form.find('textarea').val('');
                    form.find('textarea').css({"height": "0", "padding-bottom": "0"});
                    form.find('.post-create-controls').css('top', '15px');
                    form.find('.medias').empty();

                });
    }

    function getTimelineMedia(type) {
        var user_id = '{{$user->id}}';

            $.ajax({
                method: "POST",
                url: "{{ route('profile.timeline.medias') }}",
                data: {type:type, user_id:user_id, media_type:'photo'}
            })
            .done(function (res) {
                $('#timline_list').html(res)
            });
    }


</script>
