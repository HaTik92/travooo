@extends('site.profile.template.profile')

@section('content')
    <div class="post-block post-top-bordered">
        <div class="post-side-top top-arrow">
            <div class="post-top-txt horizontal">
                <h3 class="side-ttl">@lang('profile.all_favorites') <span class="count">27</span></h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <label>@lang('other.sort_by')</label>
                    <div class="sort-select-wrap">
                        <select class="form-control" id="">
                            <option>@lang('time.date')</option>
                            <option>Item</option>
                            <option>Item2</option>
                        </select>
                        <i class="trav-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="activity-inner">
            <div class="activ-row">
                <div class="activ-inside-block">
                    <a href="#" class="close-btn">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">New York City</a></h2>
                            <div class="txt-block">
                                <p>@lang('profile.city_in') <a href="#" class="link-txt">United State of America</a></p>
                            </div>
                            <div class="btn-block">
                                <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('profile.follow')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activ-after-txt">
                    <p>
                        <img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                        @lang('profile.saved_from')&nbsp;
                        <a href="#" class="link-txt">John Post</a>&nbsp;
                        2 days ago
                    </p>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">Diana Walker</a>
                                &nbsp;<span>@lang('profile.photo')</span></h2>
                            <div class="txt-block">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit curabitur
                                    enean bibendum venenatis pharetra duis et risus nec lorem...</p>
                            </div>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                            class="trav-view-plan-icon"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activ-after-txt">
                    <p>
                        <img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                        @lang('profile.saved_from')
                        <a href="#" class="link-txt">Diana Walker Post</a>&nbsp;
                        3 days ago
                    </p>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><img src="http://placehold.it/36x20" class="small-flag"
                                                     alt="flag">&nbsp; <a href="#" class="link-txt">Unites
                                    States of America</a></h2>
                            <div class="txt-block">
                                <p>@lang('region.country_in_name', ['name' => 'North America']) </p>
                            </div>
                            <div class="btn-block">
                                <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('profile.follow')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">Diana Walker</a>
                                &nbsp;<span>@lang('profile.video')</span></h2>
                            <div class="txt-block">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam,
                                    quaerat? Culpa officia expedita eaque nam?</p>
                            </div>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                            class="trav-view-plan-icon"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activ-after-txt">
                    <p><img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                        @lang('profile.saved_from')
                        <a href="#" class="link-txt">Robe Walker Post</a>&nbsp;
                        3 days ago</p>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">Independence day</a></h2>
                            <div class="txt-block">
                                <p>@lang('profile.national_holiday_in') <a href="#" class="link-txt">United State of
                                        America</a></p>
                            </div>
                            <div class="btn-block">
                                <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('profile.follow')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="photos-gallery">
                        <div class="img-wrap">
                            <img src="http://placehold.it/120x59" alt="image">
                        </div>
                        <div class="img-wrap">
                            <img src="http://placehold.it/59x59" alt="image">
                        </div>
                        <div class="img-wrap">
                            <img src="http://placehold.it/59x59" alt="image">
                            <a href="#" class="cover-link">+8</a>
                        </div>
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">Jane Grogan</a>
                                &nbsp;<span>@lang('profile.photos')</span></h2>
                            <div class="txt-block">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                    curabitur.</p>
                            </div>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                            class="trav-view-plan-icon"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activ-after-txt">
                    <p>
                        <img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                        @lang('profile.saved_from')
                        <a href="#" class="link-txt">Jane Grogan Post</a>&nbsp;
                        1 month ago
                    </p>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">4 Days in USA</a>
                                &nbsp;<span>@lang('trip.trip_plan')</span></h2>
                            <div class="txt-block">
                                <p>@lang('profile.create_by') <a href="#" class="link-txt">Stephen Bugno</a></p>
                            </div>
                            <div class="btn-block">
                                <button class="btn btn-light-grey btn-bordered btn-grey-txt">@lang('profile.view_plan')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">Martin Kennedy</a></h2>
                            <div class="txt-block">
                                <p>“Aliquam tempor, tellus ut condimentum posuere arcu velit vulputate
                                    nec hendrerit nunc ex et elit sed maximus orci lorem iaculis...” <a
                                            href="#" class="link-txt">@lang('buttons.general.more')</a></p>
                            </div>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                            class="trav-view-plan-icon"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activ-after-txt">
                    <p><img src="http://placehold.it/37x37" alt="" class="ava">&nbsp;
                        @lang('profile.saved_from')
                        <a href="#" class="link-txt">Diana Walker Post</a>&nbsp;
                        3 days ago</p>
                </div>
            </div>
            <div class="activ-row">
                <div class="activ-inside-block">
                    <div class="activ-img">
                        <img src="http://placehold.it/120x120" alt="image">
                    </div>
                    <div class="activ-inside-txt">
                        <div class="activ-txt-inner">
                            <h2 class="act-ttl"><a href="#" class="link-txt">Lisa
                                    Martinez</a> @lang('profile.tip_about')
                                <a href="#" class="link-txt">Haneda Airport</a></h2>
                            <div class="txt-block">
                                <p>“Nullam accumsan, eros in consequat imperdiet, lacus mi iaculis
                                    viverra est ante et eros fusce accumsan sed scelerisque...” <a
                                            href="#" class="link-txt">@lang('buttons.general.more')</a></p>
                            </div>
                            <div class="btn-block">
                                <button type="button" class="btn btn-light-grey btn-bordered"><i
                                            class="trav-view-plan-icon"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activ-after-txt">
                    <p>@lang('profile.saved_from_a')&nbsp;
                        <a href="#" class="link-txt">@lang('trip.trip_plan')</a>&nbsp;
                        @lang('chat.by')
                        <a href="#" class="link-txt">Suzanne</a>&nbsp;
                        3 months ago</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('sidebar')
    <div class="post-block post-side-profile sm-profile">
        <div class="image-wrap">
            <img src="http://placehold.it/385x125" alt="">
            <div class="post-image-info">
                <div class="avatar-layer">
                    <div class="ava-inner">
                        <img src="http://placehold.it/58x58" alt="" class="avatar">
                        <a href="#" class="edit-ava-link">
                            <img src="./assets2/image/profile-ava-edit-img.png" alt="">
                        </a>
                    </div>
                    <div class="ava-txt">
                        <h4 class="ava-name">Justin baker</h4>
                        <p class="sub-ttl">United States</p>
                    </div>
                </div>
                <div class="follow-btn-wrap">
                    <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side">
                        <i class="trav-user-plus-icon"></i>
                        <span>@lang('profile.follow')</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="post-profile-info">
            <ul class="profile-info-list">
                <li>
                    <p class="info-count">125</p>
                    <p class="info-label">@lang('profile.posts')</p>
                </li>
                <li>
                    <p class="info-count">58</p>
                    <p class="info-label">@lang('profile.followers')</p>
                </li>
                <li>
                    <p class="info-count">2</p>
                    <p class="info-label">@lang('profile.following')</p>
                </li>
            </ul>
        </div>
    </div>

    <div class="post-block post-side-search">
        <div class="search-wrapper">
            <input class="" id="" placeholder="@lang('profile.search_your_visited_places')" type="text">
            <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
        </div>
    </div>

    <div class="post-block post-side-type">
        <div class="type-label">@lang('profile.type')</div>
        <div class="type-progress-block">
            <div class="type-line">
                <div class="label">@lang('profile.places')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">17</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.posts')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 30%" aria-valuenow="30"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">5</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.events')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 18%" aria-valuenow="18"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">3</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.photos')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 12%" aria-valuenow="12"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">2</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.videos')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 35%" aria-valuenow="35"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">6</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('trip.trip_plan')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 9%" aria-valuenow="9"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">1</div>
            </div>
        </div>
    </div>
@endsection
