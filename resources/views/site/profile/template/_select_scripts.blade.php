<script>
   $(".sel-select").each(function() {
  var classes = $(this).attr("class"),
      id      = $(this).attr("id"),
      name    = $(this).attr("name"),
      select  = $(this).attr("data-type"),
      type    = $(this).attr("data-sorted_by");
      
  var template =  '<div class="' + classes + '" data-type="'+ select +'">';
      template += '<span class="sel-select-trigger">' + $(this).attr("placeholder") + '</span>';
      template += '<div class="sel-options">';
      $(this).find("option").each(function() {
        var icon    = addicon($(this).attr("value"))
        template += '<span class="sel-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '  '+ icon +'</span>';
      });
  template += '</div></div>';
  
  $(this).wrap('<div class="sel-select-wrapper"></div>');
  $(this).hide();
  $(this).after(template);
});
$(".sel-option:first-of-type").hover(function() {
  $(this).parents(".sel-options").addClass("option-hover");
}, function() {
  $(this).parents(".sel-options").removeClass("option-hover");
});
$(".sel-select-trigger").on("click", function() {
  $('html').one('click',function() {
    $(".sel-select").removeClass("opened");
  });
  $(this).parents(".sel-select").toggleClass("opened");
  event.stopPropagation();
});
$(".sel-option").on("click", function() {
  $(this).parents(".sel-select-wrapper").find("select").val($(this).data("value"));
  $(this).parents(".sel-options").find(".sel-option").removeClass("selection");
  $(this).addClass("selection");
  $(this).parents(".sel-select").removeClass("opened");
  $(this).parents(".sel-select").find(".sel-select-trigger").html($(this).html());
 

  if($(this).closest(".sel-select").attr('data-type') == 'photo_timeline'){
       getTimelineMedia($(this).data("value"));
  }else if($(this).closest(".sel-select").attr('data-type') == 'video_timeline'){
       getVideoTimelineMedia($(this).data("value"));
  }else{
     itemsSort($(this).parents(".sel-select-wrapper").find("select"), $(this).data("value")) 
  }
});

function addicon(val){
        return '';
}

function scoreicon(val){
    if(val == '1.0'){
        return '<i class="trav-star-icon" dir="auto"></i>';
    }else if(val == '2.0'){
        return '<i class="trav-star-icon" dir="auto"></i>'+
               '<i class="trav-star-icon" dir="auto"></i>';
    }else if(val == '3.0'){
        return '<i class="trav-star-icon" dir="auto"></i>'+
               '<i class="trav-star-icon" dir="auto"></i>'+
               '<i class="trav-star-icon" dir="auto"></i>';
    }else if(val == '4.0'){
        return '<i class="trav-star-icon" dir="auto"></i>'+
               '<i class="trav-star-icon" dir="auto"></i>'+
               '<i class="trav-star-icon" dir="auto"></i>'+
               '<i class="trav-star-icon" dir="auto"></i>';
    }else if(val == '5.0'){
        return '<i class="trav-star-icon" dir="auto"></i>'+
               '<i class="trav-star-icon" dir="auto"></i>'+
               '<i class="trav-star-icon" dir="auto"></i>'+
               '<i class="trav-star-icon" dir="auto"></i>'+
               '<i class="trav-star-icon" dir="auto"></i>';
    }else{
        return 'All';
    }
}


function filterOrder(class_name, selected){
    $("." + class_name).each(function() {
var classes = $(this).attr("class"),
    id      = $(this).attr("id"),
    name    = $(this).attr("name"),
    select  = $(this).attr("data-type"),
    type    = $(this).attr("data-sorted_by");
    
var template =  '<div class="' + classes + '" data-type="'+ select +'">';
    template += '<span class="sort-select-trigger">' + selected + '</span>';
    template += '<div class="sort-options">';
    $(this).find("option").each(function() {
        var icon = '';
         if(type == 'score'){
                icon = scoreicon($(this).attr("value"));
            }
        template += '<span class="sort-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + ''+ icon +'</span>';
    });
template += '</div></div>';

$(this).wrap('<div class="sort-select-wrapper"></div>');
$(this).hide();
$(this).after(template);
});

$(".sort-option:first-of-type").hover(function() {
$(this).parents(".sort-options").addClass("option-hover");
}, function() {
$(this).parents(".sort-options").removeClass("option-hover");
});
$(".sort-select-trigger").on("click", function() {
$('html').one('click',function() {
    $(".sel-select").removeClass("opened");
});
$(this).parents("."+class_name+".sort-select").toggleClass("opened");
event.stopPropagation();
});
}
</script>