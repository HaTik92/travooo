<div class="profile-drawer hidden-more-sm" id="leftOutsideMenuMobile">
    <ul class="profile-drawer-list list-style">
        <li {{ (url('profile/'.$user_id) === url()->current() || route('profile.about') === url()->current() ) ? 'class=active' : null }}>
            @if(isset($user_id))
                <a href="{{url('profile/'.$user_id)}}">
                    @else
                        <a href="{{route('profile.about')}}">
                            @endif
                            <img src="{{asset('assets2/image/icons/Profile Page - side menu/about.png')}}" alt="" class="profile-drawer-icon">
                            <span>@lang('navs.frontend.about')</span>
                        </a>
        </li>


        <li {{ url('profile-posts/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
                <a href="{{url('profile-posts/'.$user_id)}}">
                    @else
                        <a href="{{route('profile.posts')}}">
                            @endif
                            <img src="{{asset('assets2/image/icons/Profile Page - side menu/feed.png')}}" alt="" class="profile-drawer-icon">
                            <span>Posts</span>
                        </a>
        </li>

        <li {{ url('profile-liked-posts/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
                <a href="{{url('profile-liked-posts/'.$user_id)}}">
                    @else
                        <a href="{{route('profile.liked.posts')}}">
                            @endif
                            <img src="{{asset('assets2/image/icons/Profile Page - side menu/favs.png')}}" class="profile-drawer-icon">
                            <span>Liked Posts</span>
                        </a>
        </li>


        <li {{ url('profile-map/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
                <a href="{{url('profile-map/'.$user_id)}}">
                    @else
                        <a href="{{route('profile.map')}}">
                            @endif
                            <img src="{{asset('assets2/image/icons/Profile Page - side menu/myMap.png')}}" alt="" class="profile-drawer-icon">
                            <span>@lang('profile.nav.my_map')</span>
                        </a>
        </li>


        <li {{ url('profile-plans/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
                <a href="{{url('profile-plans/'.$user_id)}}">
                    @else
                        <a href="{{route('profile.plans')}}">
                            @endif
                            <img src="{{asset('assets2/image/icons/Profile Page - side menu/tripPlans.png')}}" alt="" class="profile-drawer-icon">
                            <span>@lang('profile.nav.trip_plans')</span>
                        </a>
        </li>
        <li {{ url('profile-photos/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
                <a href="{{url('profile-photos/'.$user_id)}}">
                    @else
                        <a href="{{route('profile.photos')}}">
                            @endif
                            <img src="{{asset('assets2/image/icons/Profile Page - side menu/photos.png')}}" alt="" class="profile-drawer-icon">
                            <span>@lang('profile.nav.photos')</span>
                        </a>
        </li>
        <li {{ url('profile-videos/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
                <a href="{{url('profile-videos/'.$user_id)}}">
                    @else
                        <a href="{{route('profile.videos')}}">
                            @endif
                            <img src="{{asset('assets2/image/icons/Profile Page - side menu/videos.png')}}" alt="" class="profile-drawer-icon">
                            <span>@lang('profile.nav.videos')</span>
                        </a>
        </li>
        <li {{ url('profile-reviews/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
                <a href="{{url('profile-reviews/'.$user_id)}}">
                    @else
                        <a href="{{url_with_locale('profile-reviews')}}">
                            @endif
                            <img src="{{asset('assets2/image/icons/Profile Page - side menu/reviews.png')}}" alt="" class="profile-drawer-icon">
                            <span>@lang('profile.nav.reviews')</span>
                        </a>
        </li>
    </ul>
</div>

