<!-- left outside menu -->
<div class="left-outside-menu-wrap profile-left-menu-list" id="leftOutsideMenu">
    <div class="leftOutsideMenuClose"></div>
    <ul class="left-outside-menu">
        <li {{ (url('profile/'.$user_id) === url()->current() || route('profile.about') === url()->current() ) ? 'class=active' : null }}>
            @if(isset($user_id))
            <a href="{{url('profile/'.$user_id)}}">
            @else
            <a href="{{route('profile.about')}}">
            @endif
                <i class="trav-profile-icon"></i>
                <span>@lang('navs.frontend.about')</span>
            </a>
        </li>
        

        <li {{ url('profile-posts/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
            <a href="{{url('profile-posts/'.$user_id)}}">
            @else
            <a href="{{route('profile.posts')}}">
            @endif
                <i class="trav-home-icon"></i>
                <span>Posts</span>
            </a>
        </li>

        <li {{ url('profile-liked-posts/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
                <a href="{{url('profile-liked-posts/'.$user_id)}}">
                    @else
                        <a href="{{route('profile.liked.posts')}}">
                            @endif
                           <img src="{{asset('assets2/image/icons/post-like-icon.png')}}" style="width: 32px;align-self: flex-end;">
                            <span>Liked Posts</span>
                        </a>
        </li>
        
        
        <li {{ url('profile-map/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
            <a href="{{url('profile-map/'.$user_id)}}">
            @else
            <a href="{{route('profile.map')}}">
            @endif
                <i class="trav-map-o"></i>
                <span>@lang('profile.nav.my_map')</span>
            </a>
        </li>
        
            
        <li {{ url('profile-plans/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
            <a href="{{url('profile-plans/'.$user_id)}}">
            @else
            <a href="{{route('profile.plans')}}">
            @endif
                <i class="trav-trip-plans-icon"></i>
                <span>@lang('profile.nav.trip_plans')</span>
            </a>
        </li>
        <li {{ url('profile-photos/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
            <a href="{{url('profile-photos/'.$user_id)}}">
            @else
            <a href="{{route('profile.photos')}}">
            @endif
                <i class="trav-photos-icon"></i>
                <span>@lang('profile.nav.photos')</span>
            </a>
        </li>
        <li {{ url('profile-videos/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
            <a href="{{url('profile-videos/'.$user_id)}}">
            @else
            <a href="{{route('profile.videos')}}">
            @endif
                <i class="trav-videos-icon"></i>
                <span>@lang('profile.nav.videos')</span>
            </a>
        </li>
        <li {{ url('profile-reviews/'.$user_id) === url()->current() ? 'class=active' : null }}>
            @if(isset($user_id))
            <a href="{{url('profile-reviews/'.$user_id)}}">
            @else
            <a href="{{url_with_locale('profile-reviews')}}">
            @endif
                <i class="trav-review-icon"></i>
                <span>@lang('profile.nav.reviews')</span>
            </a>
        </li>
    </ul>
</div>

