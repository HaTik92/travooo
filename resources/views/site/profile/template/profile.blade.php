@extends('site.layouts.site')

@section('before_site_style')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.css">
    <link href="{{asset('/plugins/croppie/croppie.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets2/js/lightbox2/src/css/lightbox.css') }}" rel="stylesheet"/>
    <link href="{{asset('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets2/css/profile-post.css')}}" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link href="{{asset('/plugins/intl-tel-input/css/intlTelInput.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets2/js/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('/plugins/summernote-master/tam-emoji/css/emoji.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ asset('assets2/css/profile-repolishing.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/cropper/cropper.css') }}" rel="stylesheet">
@endsection

@section('after_styles')
    <link href="{{ asset('dist-parcel/assets/Profile/main.css') }}" rel="stylesheet">
@endsection

@section('content-area')
    @include('site.profile.template._menu')

    <div class="custom-row">
        <div class="main-content-layer profile-main-content-layer">
            @yield('content')
        </div>

    </div>
@endsection

@section('before_scripts')
    @if(Auth::check())
        @include('site.profile.template._modals')
        @include('site.home.partials._spam_dialog')
    @endif
@endsection

@section('before_site_script')
    <script src="{{asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/11.1.0/nouislider.min.js"></script>
{{--    <script src="{{asset('/plugins/croppie/croppie.js')}}" type="text/javascript"></script> --}}
{{--    <script src="{{asset('/plugins/croppie/croppie_custom_functions.js')}}?v=1.0.0" type="text/javascript"></script> --}}
    <script src="{{asset('/plugins/cropper/cropper.js')}}" type="text/javascript"></script>
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.3/cropper.js" type="text/javascript"></script>--}}
    <script src="{{asset('/plugins/cropper/cropper_custom_functions.js')}}?v=1.0.0" type="text/javascript"></script>
    <script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
    <script src="{{asset('assets2/js/delete-trip.js?v='.time())}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script src="{{asset('/plugins/intl-tel-input/js/intlTelInput-jquery.js')}}" type="text/javascript"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.lazy/1.7.5/plugins/jquery.lazy.av.min.js"></script>
    <script src="{{ asset('assets2/js/summernote/summernote.js') }}"></script>
    <script src="{{ asset('/plugins/summernote-master/tam-emoji/js/config.js') }}"></script>
    <script src="{{ asset('/plugins/summernote-master/tam-emoji/js/tam-emoji.js?v='.time()) }}"></script>
    <script src="{{ asset('/plugins/summernote-master/tam-emoji/js/textcomplete.js?v='.time()) }}"></script>
    <script src="{{ asset('dist-parcel/assets/Profile/main.js') }}"></script>
@endsection
