<style>
    .post-comment-row {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
        padding-bottom: 15px;
    }
    .post-com-avatar-wrap {
        padding-right: 18px;
        padding-top: 5px;
    }
    .post-comment-row .post-com-avatar-wrap img {
        border: 1px solid #ebebeb;
        width: 45px!important;
        height: 45px;
        border-radius: 50%;
        -o-object-fit: cover;
        object-fit: cover;
    }
    .post-comment-row .post-comment-text {
        -webkit-box-flex: 1;
        -ms-flex-positive: 1;
        flex-grow: 1;
    }
    .post-comment-row .post-comment-text .comment-bottom-info a.comment-name {
        font-family: circular std book;
        font-size: 16px;
        color: #4080ff;
        float: left;
    }
    .post-comment-row .post-comment-text .comment-bottom-info p {
        font-family: circularairpro light;
        font-size: 16px;
        color: #000;
        line-height: 1.5;
        margin-bottom: 10px;
        float:left;
    }
    .post-comment-row .post-comment-text .comment-bottom-info {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }
    .post-comment-row .post-comment-text .comment-bottom-info .com-time {
        font-family: circularairpro light;
        font-size: 14px;
        color: #999;
        line-height: 19px;
    }
    .comment-bottom-info ul.post-image-list {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        margin: -4px -4px 0;
    }
    .comment-bottom-info ul.post-image-list li {
        padding: 4px;
        position: relative;
        -webkit-box-flex: 1;
        -ms-flex-positive: 1;
        flex-grow: 1;
    }
    .doublecomment {
        padding-top: 0px;
        padding-left: 59px;
    }
</style>
<div class="modal fade white-style" data-backdrop="false" id="reportsPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
      <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-660" role="document">
      <div class="modal-custom-block">
        <div class="post-block post-report-block post-mobile-full">
          <div class="post-top-report">
            <div class="report-ttl">Reports <span class="count">{{count($user->reports)}}</span></div>
            <ul class="nav nav-tabs report-filter-list" role="tablist">
              <li class="nav-item" onclick="reportsSort('Most', this)">
                <a class="nav-link active" data-toggle="tab" href="#mostPopular" role="tab">Most popular</a>
              </li>
              <li class="nav-item" onclick="reportsSort('New', this)">
                <a class="nav-link" data-toggle="tab" href="#newest" role="tab">Newest</a>
              </li>
              <li class="nav-item" onclick="reportsSort('Old', this)">
                <a class="nav-link" data-toggle="tab" href="#oldest" role="tab">Oldest</a>
              </li>
            </ul>
          </div>
          <div class="post-report-inner sortBody">
              @foreach($user->reports()->take(10)->get() AS $r)
            <div class="report-row" newSort="{{strtotime($r->created_at)}}" mostSort="{{count($r->likes)}}">
              <div class="img-wrap">
                <img class="ava" src="@if(isset($r->cover[0])){{$r->cover[0]->val}}@else {{asset('assets2/image/placeholders/no-photo.png')}} @endif" alt="ava" style="width:92px;height:92px;">
              </div>
              <div class="txt-block">
                <a href="{{url('reports/'.$r->id)}}" class="report-ttl-link">{{$r->title}}</a>
                <div class="report-time">
                  {{date("d F, Y", strtotime($r->created_at))}} By
                  <a href="#" class="name-link">{{$r->author->name}}</a>
                </div>
                <ul class="foot-avatar-list">
                  
                </ul>
              </div>
            </div>
              @endforeach
            
          </div>
            <div class="post-load-txt" id="pagination">
                <input type="hidden" id="pageno" value="1">
                <img id="feedloader" src="{{asset('assets2/image/preloader.gif')}}" width="50px"> Grabbing awesomeness!
            </div>
        </div>
      </div>
    </div>
  </div>

	<!--Delete trip -->
	<div class="modal fade white-style" data-backdrop="false" id="deleteTripModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
			<i class="trav-close-icon"></i>
		</button>
		<div class="modal-dialog modal-custom-style modal-660" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">@lang("profile.are_you_sure")</h5>

				</div>
				<div class="modal-body">
					<p>@lang("profile.if_you_canceled_this_trip_you_will_lose")</p>
				</div>
				<div class="modal-footer">
					<a href="" data-dismiss="modal">Close</a>
					<button type="button" class="btn btn-danger confirm-trip-delete" data-redirect="{{ route('profile.plans') }}" data-url="{{ route('trip.ajax_delete_trip')}}" style="padding:.5rem 1rem;">Confirm</button>

				</div>
			</div>
		</div>
	</div>
  
  <!-- trip plans popup -->
	<div class="modal fade white-style" data-backdrop="false" id="tripPlansPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
			<i class="trav-close-icon"></i>
		</button>
		<div class="story-bottom-slider-wrapper">
			<div class="bottom-slider" id="storiesModeSlider">
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">Suzanne</a>
							<ul class="slider-info-list">
								<li>Visited Places <b>6 km</b></li>
								<li>Spent <b>$ 610</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">Lonnie Nelson</a>
							<ul class="slider-info-list">
								<li>Visited Places <b>6 km</b></li>
								<li>Spent <b>$ 610</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">Barbara Hafer</a>
							<ul class="slider-info-list">
								<li>Visited Places <b>6 km</b></li>
								<li>Spent <b>$ 610</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">David</a>
							<ul class="slider-info-list">
								<li>Visited Places <b>6 km</b></li>
								<li>Spent <b>$ 610</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">Suzanne</a>
							<ul class="slider-info-list">
								<li>Visited Places <b>6 km</b></li>
								<li>Spent <b>$ 610</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">Barbara Hafer</a>
							<ul class="slider-info-list">
								<li>Visited Places <b>6 km</b></li>
								<li>Spent <b>$ 610</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">David</a>
							<ul class="slider-info-list">
								<li>Visited Places <b>6 km</b></li>
								<li>Spent <b>$ 610</b></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="story-slide">
					<div class="story-slide-inner">
						<div class="img-wrap">
							<img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
						</div>
						<div class="slide-txt">
							<a href="#" class="name-link">Suzanne</a>
							<ul class="slider-info-list">
								<li>Visited Places <b>6 km</b></li>
								<li>Spent <b>$ 610</b></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-dialog modal-custom-style modal-1140 bottom-slider-height" role="document">
			<ul class="modal-outside-link-list white-bg">
				<li class="outside-link">
					<a href="#">
						<div class="round-icon">
							<i class="trav-flag-icon"></i>
						</div>
						<span>Report</span>
					</a>
				</li>
			</ul>
			<div class="modal-custom-block">
				<button class="btn btn-mobile-side comment-toggler" id="commentToggler">
          <i class="trav-comment-icon"></i>
        </button>
				<div class="post-block post-place-plan-block">

					<div class="post-image-container post-follow-container mCustomScrollbar">
						<div class="post-image-inner">
							<div class="post-map-wrap">
								<div class="post-place-top-info">
									<span>Check the full trip plan:</span>
									<a href="#" class="place-link">My First Trip to 3 Countries</a>
								</div>
								<img src="./assets2/image/trip-plan-image.jpg" alt="map">
								<div class="destination-point" style="top:80px;left: 145px;">
									<img class="map-marker" src="./assets2/image/marker.png" alt="marker">
								</div>
							</div>
							<div class="post-destination-block slide-dest-hide-right-margin">
								<div class="post-dest-slider" id="postDestSliderInner4">
									<div class="post-dest-card">
										<div class="post-dest-card-inner">
											<div class="dest-image">
												<img src="http://placehold.it/68x68" alt="">
											</div>
											<div class="dest-info">
												<div class="dest-name">Grififth</div>
												<div class="dest-count">Observatory</div>
											</div>
										</div>
									</div>
									<div class="post-dest-card">
										<div class="post-dest-card-inner">
											<div class="dest-image">
												<img src="http://placehold.it/68x68" alt="">
											</div>
											<div class="dest-info">
												<div class="dest-name">Hearst Castle</div>
											</div>
										</div>
									</div>
									<div class="post-dest-card">
										<div class="post-dest-card-inner">
											<div class="dest-image">
												<img src="http://placehold.it/68x68" alt="">
											</div>
											<div class="dest-info">
												<div class="dest-name">SeaWorld San</div>
												<div class="dest-count">Diego</div>
											</div>
										</div>
									</div>
									<div class="post-dest-card">
										<div class="post-dest-card-inner">
											<div class="dest-image">
												<img src="http://placehold.it/68x68" alt="">
											</div>
											<div class="dest-info">
												<div class="dest-name">United Arab Emirates</div>
												<div class="dest-count">2 Destinations</div>
											</div>
										</div>
									</div>
									<div class="post-dest-card">
										<div class="post-dest-card-inner">
											<div class="dest-image">
												<img src="http://placehold.it/68x68" alt="">
											</div>
											<div class="dest-info">
												<div class="dest-name">SeaWorld San</div>
												<div class="dest-count">Diego</div>
											</div>
										</div>
									</div>
									<div class="post-dest-card">
										<div class="post-dest-card-inner">
											<div class="dest-image">
												<img src="http://placehold.it/68x68" alt="">
											</div>
											<div class="dest-info">
												<div class="dest-name">United Arab Emirates</div>
												<div class="dest-count">2 Destinations</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<!-- <div class="modal-place-block">

					</div> -->
					<div class="gallery-comment-wrap" id="galleryCommentWrap">
						<div class="gallery-comment-inner mCustomScrollbar">
							<div class="gallery-comment-top">
								<div class="top-info-layer">
									<div class="top-avatar-wrap">
										<img src="http://placehold.it/50x50" alt="">
									</div>
									<div class="top-info-txt">
										<div class="preview-txt">
											<b>By</b>
											<a class="dest-name" href="#">Elijah Hughes</a>
											<p class="dest-date">30 Aug 2017 at 10:00 pm</p>
										</div>
									</div>
								</div>
								<div class="gal-com-footer-info">
									<div class="post-foot-block post-reaction">
										<img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
										<span><b>2</b> Reactions</span>
									</div>
								</div>
							</div>
							<div class="post-comment-layer">
								<div class="post-comment-top-info">
									<div class="comm-count-info">
										5 Comments
									</div>
									<div class="comm-count-info">
										3 / 20
									</div>
								</div>
								<div class="post-comment-wrapper">
									<div class="post-comment-row">
										<div class="post-com-avatar-wrap">
											<img src="http://placehold.it/45x45" alt="">
										</div>
										<div class="post-comment-text">
											<div class="post-com-name-layer">
												<a href="#" class="comment-name">Katherin</a>
												<a href="#" class="comment-nickname">@katherin</a>
											</div>
											<div class="comment-txt">
												<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
											</div>
											<div class="comment-bottom-info">
												<div class="com-reaction">
													<img src="./assets/image/icon-smile.png" alt="">
													<span>21</span>
												</div>
												<div class="com-time">6 hours ago</div>
											</div>
										</div>
									</div>
									<div class="post-comment-row">
										<div class="post-com-avatar-wrap">
											<img src="http://placehold.it/45x45" alt="">
										</div>
										<div class="post-comment-text">
											<div class="post-com-name-layer">
												<a href="#" class="comment-name">Amine</a>
												<a href="#" class="comment-nickname">@ak0117</a>
											</div>
											<div class="comment-txt">
												<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
											</div>
											<div class="comment-bottom-info">
												<div class="com-reaction">
													<img src="./assets/image/icon-like.png" alt="">
													<span>19</span>
												</div>
												<div class="com-time">6 hours ago</div>
											</div>
										</div>
									</div>
									<div class="post-comment-row">
										<div class="post-com-avatar-wrap">
											<img src="http://placehold.it/45x45" alt="">
										</div>
										<div class="post-comment-text">
											<div class="post-com-name-layer">
												<a href="#" class="comment-name">Katherin</a>
												<a href="#" class="comment-nickname">@katherin</a>
											</div>
											<div class="comment-txt">
												<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
											</div>
											<div class="comment-bottom-info">
												<div class="com-reaction">
													<img src="./assets/image/icon-smile.png" alt="">
													<span>15</span>
												</div>
												<div class="com-time">6 hours ago</div>
											</div>
										</div>
									</div>
									<a href="#" class="load-more-link">Load more...</a>
								</div>
							</div>
						</div>
						<div class="post-add-comment-block">
							<div class="avatar-wrap">
								<img src="http://placehold.it/45x45" alt="">
							</div>
							<div class="post-add-com-input">
								<input type="text" placeholder="Write a comment">

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  
  <!-- your friends popup -->
  <div class="modal fade white-style" data-backdrop="false" id="badgesPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
      <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-700" role="document">
      <div class="modal-custom-block">
        <div class="post-block post-going-block post-mobile-full">
          <div class="post-top-topic topic-tabs">
            <div class="topic-item active">Unlocked badges <span class="count">{{count($user->badges)}}</span></div>
          </div>
          <div class="post-people-block-wrap mCustomScrollbar">
              
              @foreach($user->badges AS $badge)

            <div class="people-row">
              <div class="main-info-layer">
                <div class="img-wrap">
                  <img class="badge-img" src="{{asset('assets2/image/badges/'.$badge->badge->id.'_'.$badge->level.'.png')}}" alt="ava" style='width:50px !important;height:50px !important;border-radius: 0;'>
                </div>
                <div class="txt-block">
                  <div class="name">{{$badge->badge->name}}</div>
                  <div class="info-line">
                    <div class="info-part">Level {{$badge->level}}</div>
                  </div>
                </div>
              </div>
              <div class="badge-right-wrap">
                {{count($badge->badge->users)}} have this badge
              </div>
            </div>
              @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- modals -->
<!-- asking popup -->
<div class="modal fade white-style" data-backdrop="false" id="askingPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-750" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-asking-block">
                <div class="post-asking-inner">
                    <div class="avatar-wrap">
                        <img class="ava" src="http://placehold.it/50x50" alt="">
                    </div>
                    <div class="ask-content">
                        <div class="ask-row-inner">
                            <div class="ask-row">
                                <div class="ask-label">@lang('profile.asking')</div>
                                <div class="ask-txt">
                                    <div class="radio-wrapper">
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="askingRadios" id="forRecommendation"
                                                   value="forRecommendation" type="radio">
                                            <label for="forRecommendation">@lang('profile.for_recommendations')</label>
                                        </div>
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="askingRadios" id="forTips"
                                                   value="forTips" type="radio">
                                            <label for="forTips">@lang('profile.for_tips')</label>
                                        </div>
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="askingRadios" id="aboutTripPlan"
                                                   value="aboutTripPlan" type="radio">
                                            <label for="aboutTripPlan">@lang('profile.about_a_trip_plan')</label>
                                        </div>
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="askingRadios" id="generalQuestion"
                                                   value="generalQuestion" type="radio">
                                            <label for="generalQuestion">@lang('profile.a_general_question')</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">@lang('profile.topic')</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">@lang('profile.destination')</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="@lang('profile.place_city_country')"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">@lang('profile.tips_about')</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="@lang('profile.place_city_country')"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">@lang('trip.trip_plan')</div>
                                <div class="ask-txt">
                                    <div class="ask-input-btn-line">
                                        <div class="ask-input-wrap">
                                            <textarea name="" id="" cols="" rows=""
                                                      placeholder="@lang('profile.select_one_of_your_trip_plans')"></textarea>
                                        </div>
                                        <button data-toggle="modal" data-target="#tripPlanSelectionPopup" type="button"
                                                class="btn btn-light-grey btn-bordered">@lang('profile.select')
                                        </button>
                                    </div>
                                    <div class="ask-trip-card">
                                        <div class="img-wrap">
                                            <img src="http://placehold.it/137x170" alt="">
                                        </div>
                                        <div class="card-inner">
                                            <div class="card-close">
                                                <i class="fa fa-close"></i>
                                            </div>
                                            <h4 class="card-ttl">Over the Mediterranean Sea</h4>
                                            <div class="card-info-line">
                                                18 to 21 Sep 2017&nbsp;&nbsp;·&nbsp;&nbsp;3 Days&nbsp;&nbsp;·&nbsp;&nbsp;12K
                                                km
                                            </div>
                                            <div class="dest-label">
                                                @lang('profile.destinations')
                                                <b>13</b>
                                            </div>
                                            <div class="destinations-img-list-wrap">
                                                <ul class="destinations-img-list">
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li><img src="http://placehold.it/52x52" alt=""></li>
                                                    <li>
                                                        <img src="http://placehold.it/52x52" alt="">
                                                        <a href="#" class="img-link">+8</a>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">@lang('profile.question')</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="@lang('profile.write_your_question')"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="ask-row">
                                <div class="ask-label">@lang('profile.destination')</div>
                                <div class="ask-txt">
                                    <div class="ask-input-wrap">
                                        <textarea name="" id="" cols="" rows=""
                                                  placeholder="@lang('profile.write_more_details_about_your_question')..."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ask-foot-btn">
                            <div class="upload-wrap">
                                <div class="upload-blank">
                                    <i class="trav-camera"></i>
                                    <span>@lang('profile.attach_pictures_count_max', ['count' => 3])</span>
                                </div>
                                <div class="upload-image">
                                    <ul class="image-list">
                                        <li>
                                            <img src="http://placehold.it/30x30" alt="">
                                        </li>
                                        <li class="uploading">
                                            <img src="http://placehold.it/30x30" alt="">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: 75%"
                                                     aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </li>
                                    </ul>
                                    <i class="trav-camera"></i>
                                    <span>2/3</span>
                                </div>
                            </div>
                            <div class="btn-wrap">
                                <button class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                                <button class="btn btn-light-primary btn-disabled">@lang('buttons.general.post') @lang('profile.question')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- trip plan selection popup -->
<div class="modal fade modal-child white-style" data-backdrop="false" data-modal-parent="#askingPopup"
     id="tripPlanSelectionPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-740" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-trip-selection-block">
                <div class="post-top-selection">
                    <div class="label-ttl">@lang('profile.select_one_of_your_trip_plan')</div>
                    <div class="search-layer">
                        <div class="search-block">
                            <input class="" id="tripPlanSearch" placeholder="@lang('profile.search_in_your_trip_plans')"
                                   type="text">
                            <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
                        </div>
                    </div>
                </div>
                <div class="post-trip-select-wrap mCustomScrollbar">
                    <div class="post-trip-select-inner">
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                        <div class="select-trip-plan-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/220x310" alt="">
                            </div>
                            <div class="trip-plan-txt">
                                <h4 class="trip-ttl">New York City The Right Way</h4>
                                <p class="trip-plan-info">18 to 21 Sep 2017 · 3 Days · 12K km</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Crop profile image popup -->
<div class="modal modal-profile fade white-style" data-backdrop="false" id="cropProfileImagePop" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 801px !important;">
		<button class="modal-profile-close" type="button" data-dismiss="modal" aria-label="Close"></button>
        <div class="modal-custom-block">
            <div class="post-block post-asking-block">
                <h3 class="modal-profile-title text-center">Edit Profile Photo</h3>
				<div class="profile-image-error-block"></div>
                <div class="post-asking-inner">
                        <div class="ask-content">
                            <div class="ask-row-inner profile-upload-content">
                                <div id="default_img" class="text-center"></div>
								<canvas id="upload_demo">
									Your browser does not support the HTML5 canvas element.
								</canvas>
                            </div>
                            <div class="cr-slider-wrap">
                                <div data-zoom-slider="upload_demo"></div>
                            </div>
                            <div class="ask-foot-btn profile-photo-m-footer">
								<a href="javascript:;" class="delete-profile-photo-link hidden-sm-down" id="delete_profile_photo" data-profile_image="">Delete</a>
                                <a href="javascript:;" class="button button-danger hidden-md-up" id="delete_profile_photo" data-profile_image="">Delete</a>
                                <div class="text-right">
                                    <label class="button">
                                        <input type="file" name="profilepicture" id="input_profilepicture" style="display:none;" />
                                        Change Photo
                                    </label>
                                    <button id="cropProfileImageBtn" class="button button-primary">Apply</button>
                                    <input type="hidden" id="original_file_id">
                                    <input type="hidden" id="preview_result_id">
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Crop profile cover popup -->
<div class="modal modal-profile fade white-style" data-backdrop="false" id="cropCoverImagePop" tabindex="-1" role="dialog"
	 aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" style="max-width: 801px !important;">
		<button class="modal-profile-close" type="button" data-dismiss="modal" aria-label="Close"></button>
		<div class="modal-custom-block">
			<div class="post-block post-asking-block">
				<h3 class="modal-profile-title text-center">Edit Cover</h3>
				<div class="profile-image-error-block"></div>
				<div class="post-asking-inner">
					<div class="ask-content">
						<div class="ask-row-inner profile-upload-content">
							<div id="default_cover_img" class="text-center"></div>
							<canvas id="upload_cover_demo">
								Your browser does not support the HTML5 canvas element.
							</canvas>
						</div>
                        <div class="cr-slider-wrap">
                            <div data-zoom-slider="upload_cover_demo"></div>
                        </div>
						<div class="ask-foot-btn profile-cover-m-footer">
							<a href="javascript:;" class="delete-profile-cover-link hidden-sm-down" id="delete_profile_cover" data-cover_image="">Delete</a>
							<a href="javascript:;" class="button button-danger hidden-md-up" id="delete_profile_cover" data-cover_image="">Delete</a>
							<div class="text-right">
								<label class="button">
									<input type="file"  id="input_cover" style="display:none;" />
									Change Cover
								</label>
								<button id="cropCoverImageBtn" class="button button-primary">Apply</button>
								<input type="hidden" id="original_cover_file_id">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Show profile image popup -->
<div class="modal fade white-style" data-backdrop="false" id="showProfileImagePop" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-660" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-asking-block">
                <div class="post-asking-inner">
                        <div class="ask-content">
                          <div class="ask-row-inner">
                              <div id="default_img" class="text-center"></div>
                              <div id="show_image" class="center-block" style="height:400px;padding-bottom: 51px;"></div>
                          </div>
                          <hr />
                          <div class="ask-foot-btn d-block p-0">
                              <div class="text-center">
                                  <button class="btn btn-transp btn-clear" data-dismiss="modal" aria-label="Close">@lang('buttons.general.cancel')</button>
                                  <button id="cropOriginalImageBtn" class="btn btn-light-primary pl-5 pr-5">@lang('buttons.general.save')</button>
                                  <input type="hidden" id="original_file_id">
                                  <input type="hidden" id="preview_result_id">
                              </div>
                          </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function reportsSort(type, obj)
    {
        
        var list, i, switching, shouldSwitch, switchcount = 0;
        switching = true;
        parent_obj = $(obj).parent().parent().parent().find('.sortBody');
        while (switching) {
            switching = false;
            list = parent_obj.find('>div');
            shouldSwitch = false;
            for (i = 0; i < list.length - 1; i++) {
                shouldSwitch = false;
                if(type == "Most")
                {
                    if ($(list[i]).attr('mostSort') < $(list[i + 1]).attr('mostSort')) {
                    shouldSwitch = true;
                    break;
                    }
                }
                else if(type == "New")
                {
                    if ($(list[i]).attr('newSort') < $(list[i + 1]).attr('newSort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "Old")
                {
                    if ($(list[i]).attr('newSort') > $(list[i + 1]).attr('newSort')) {
                        shouldSwitch = true;
                        break;
                    }
                }

            }
            if (shouldSwitch) {
                $(list[i]).before(list[i + 1]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && list.length > 1 && i == 0) {
                    switching = true;
                }
            }
        }
    }
    
    // for spam reporting
    function injectData(id, obj)
    {
        // var button = $(event.relatedTarget);
        var posttype = $(obj).parent().attr("posttype");
        $("#spamReportDlg").find("#dataid").val(id);
        $("#spamReportDlg").find("#posttype").val(posttype);
    }
    $(document).ready(function(){
        $('#feedloader').on('inview', function(event, isInView) {
            if (isInView) {
                var nextPage = parseInt($('#pageno').val())+1;
                $.ajax({
                    type: 'POST',
                    url: '{{route("profile.update_report", $user->id)}}',
                    data: { pageno: nextPage },
                    success: function(data){
                        if(data != ''){							 
                            $("#reportsPopup").find('.sortBody').append(data);
                            var length = $("#reportsPopup").find('.sortBody>div').length;
                            $("#reportsPopup").find('.report-ttl').find('.count').html(length);
                            $('#pageno').val(nextPage);
                        
                        } else {								 
                            $("#pagination").hide();
                        }
                    }
                });
            }
        });
        
    });

    //photo commenting end


</script>
