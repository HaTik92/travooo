<div style="display: none;">


    <input type="text" class="flex-input edit_inputs_name name-input"  name="name" placeholder="@lang('setting.name')" required value="{{$user->name}}" style="width:40%;margin-bottom: 0;" maxlength="50">
    <input type="text" class="flex-input edit_inputs_username name-input w-32" required  name="username" placeholder="username"  value="{{$user->username}}" style="margin-bottom: 0;margin-left: 12px;" maxlength="15">
    
    <div class="inputs_phone_block">
        <input type="tel" onkeyup="this.value = this.value.replace(/[^\d]/g,'')" class="flex-input edit_inputs_phone " data-utils ="{{asset('/plugins/intl-tel-input/js/utils.js')}}" id="phone_number" name="mobile"  value="{{$user->mobile}}" style="margin-bottom: 0;padding-left: 72px !important;" maxlength="50">
        <label class="phone_error profile-error-text"></label>
    </div>
    
    <select name="nationality" id="nationality" class="custom-select edit_inputs_nationality about-select" style="width: 100%">
        <option value=""></option>
        @foreach($countries AS $country)
            @if(is_object($country) AND is_object($country->transsingle))
                <option value="{{$country->id}}" @if($user->nationality==$country->id) selected @endif>{{$country->transsingle->title}}</option>
            @endif
        @endforeach
    </select>

    <div class="exp-block">
        <div>
        <input type="text" class="flex-input exp-autocomplete name-input" id="exp_autocomplete" placeholder="@lang('setting.your_expertise')" style="margin-bottom: 0;" >
           <label for="exp_autocomplete" class="error expertise-error-block" style="color:red"></label>
           <ul class="search-selected-block p-8 expertise-sel-block">
            @if(count($expertise)>0)
                @foreach($expertise as $val)
                <li>
                    <span class="search-selitem-title">{{$val['text']}}</span>  
                    <span class="close-search-item close-expertise-filter" data-expertise-id="{{$val['id']}}"> x</span>
                </li>
                @endforeach
            @endif
            </ul>
        </div>
    </div>
    
    <input type="text" class="flex-input edit_inputs_interests name-input" maxlength="15"  name="interests" placeholder="Insert your interests and press enter..."  value=""  style="margin-bottom: 0;">

    <input type="email" class="flex-input edit_contact_email name-input"  name="contact_email" placeholder="Insert your contact email"  value="{{$user->contact_email}}"  style="margin-bottom: 0;"
           rel="tooltip" data-toggle="tooltip" data-placement="top" data-animation="false" data-html="true"
           title="<p>@lang('setting.contact_email')</p>"
    >
      
<div class="trev-style-block">
    <input type="text" class="flex-input trev-style-autocomplete name-input" id="trev_style_autocomplete" placeholder="Your Travelstyle ..." style="margin-bottom: 0;" >
    <ul class="travel-styles-ui" style="display: none;"></ul> 
    <ul class="search-selected-block p-8 styles-sel-block">
        @if(count($user_travelstyles)>0)
            @foreach($user_travelstyles as $val)
            <li>
                <span class="search-selitem-title">{{$val->travelstyle->trans[0]->title}}</span>  
                <span class="close-search-item close-styles-filter" data-styles-id="{{$val->travelstyle->id}}"> x</span>
            </li>
            @endforeach
        @endif
    </ul>
</div>

    <div class="edit_inputs_block">
        <textarea type ="text" class="flex-text edit_inputs_about " name="about" id="about" rows="3" maxlength="256" rel="tooltip"
                  data-toggle="tooltip" rel="tooltip" data-placement="top" data-animation="false" data-html="true"
                  title="<p>@lang('setting.travooo_is_built_on_relationships')</p>
                                                    <p>@lang('setting.tell_them_about_the_things_you_like')</p>
                                                    <p>@lang('setting.tell_them_what_it_like_to_have_you')</p>
                                                    <p>@lang('setting.tell_them_about_you')</p>"
        >
            @if($user->about) {{ $user->about }} @endif
        </textarea>
    </div>
    <div class="edit_inputs_social">
        <div class="row">
            <label for=""
                   class="col-sm-3 col-form-label">@lang('setting.custom_url')</label>
            <div class="col-sm-9">
                <div class="flex-custom">
                    <input type="text" class="flex-input edit_inputs_website" name="website" id="website"
                           placeholder="@lang('setting.your_website_link')"
                           value="{{$user->website}}">
                </div>
                <div class="form-txt">
                    <p>@lang('setting.add_a_link_to_your_website')</p>
                </div>
            </div>
        </div>
        <div class="row">
            <label for="" class="col-sm-3 col-form-label">@lang('setting.facebook')</label>
            <div class="col-sm-9">
                <div class="flex-custom">
                    <input type="text" class="flex-input edit_inputs_facebook" name="facebook" id="facebook"
                           placeholder="@lang('setting.facebook_profile_link')"
                           value="{{$user->facebook}}">
                </div>
            </div>
        </div>
        <div class="row">
            <label for="" class="col-sm-3 col-form-label">@lang('setting.twitter')</label>
            <div class="col-sm-9">
                <div class="flex-custom">
                    <input type="text" class="flex-input edit_inputs_twitter" name="twitter" id="twitter"
                           placeholder="@lang('setting.twitter_id')"
                           value="{{$user->twitter}}">
                </div>
            </div>
        </div>
        <div class="row">
            <label for="" class="col-sm-3 col-form-label">@lang('setting.instagram')</label>
            <div class="col-sm-9">
                <div class="flex-custom">
                    <input type="text" class="flex-input edit_inputs_instagram" name="instagram" id="instagram"
                           placeholder="@lang('setting.instagram_profile_link')"
                           value="{{$user->instagram}}">
                </div>
            </div>
        </div>
        <div class="row">
            <label for="" class="col-sm-3 col-form-label">@lang('setting.medium')</label>
            <div class="col-sm-9">
                <div class="flex-custom">
                    <input type="text" class="flex-input edit_inputs_medium" name="medium" id="medium"
                           placeholder="@lang('setting.medium_id')"
                           value="{{$user->medium}}">
                </div>
            </div>
        </div>
        <div class="row">
            <label for="" class="col-sm-3 col-form-label">@lang('setting.youtube')</label>
            <div class="col-sm-9">
                <div class="flex-custom">
                    <input type="text" class="flex-input edit_inputs_youtube" name="youtube" id="youtube"
                           placeholder="@lang('setting.channel_id')"
                           value="{{$user->youtube}}">
                </div>
            </div>
        </div>
        <div class="row">
            <label for="" class="col-sm-3 col-form-label">@lang('setting.vimeo')</label>
            <div class="col-sm-9">
                <div class="flex-custom">
                    <input type="text" class="flex-input edit_inputs_vimeo" name="vimeo" id="vimeo"
                           placeholder="@lang('setting.vimeo_id')" value="{{$user->vimeo}}">
                </div>
            </div>
        </div>
        <div class="row">
            <label for=""
                   class="col-sm-3 col-form-label">@lang('setting.tripadvisor')</label>
            <div class="col-sm-9">
                <div class="flex-custom">
                    <input type="text" class="flex-input edit_inputs_tripadvisior" name="tripadvisor"
                           id="tripadvisor"
                           placeholder="@lang('setting.tripadvisor_user_name')"
                           value="{{$user->tripadvisor}}">
                </div>
                <div class="form-txt">
                    <p>@lang('setting.add_elsewhere_links_to_your_profile_so_people')</p>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" class="expertise_url" value="{{url("home/searchCountriesCities")}}">
    <input type="hidden" class="travelstyle_url" value="{{url("home/searchTravelstyles")}}">



</div>