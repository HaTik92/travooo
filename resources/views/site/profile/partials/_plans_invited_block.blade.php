<div class="trip-plan-invite-block">
    <div class="plan-invite-info">
        <div class="img-wrap">
            <img class="ava" src="{{check_profile_picture(@$trip_plan->author->profile_picture)}}" alt="avatar">
        </div>
        <a  href="{{url('profile/'.@$trip_plan->author->id)}}">{{@$trip_plan->author->name}} </a> wants you to be the {{$invite_request->role}} of his plan.
    </div>
    <div class="plan-invite-action">
        @if($invite_request->status == 0)
            <a href="javascript:;" class="plan-invite-accept-link" data-id="{{$invite_request->id}}">Accept</a>
            <a href="javascript:;" class="plan-invite-deny-link" data-id="{{$invite_request->id}}">Reject</a>
        @elseif($invite_request->status == 1)
            <span>Accepted</span>
        @elseif($invite_request->status == 2)
            <span>Rejected</span>
        @endif
    </div>
</div>
<div class="trip-plan-row border-btm">
    <div class="trip-plan-inside-block">
        <div class="trip-plan-map-img gt-map-main-block" style="width:140px;">
            <img src="{{get_plan_map($trip_plan, 140, 150)}}" alt="map-image"
                 style="max-width:140px;height:150px;">
            <span class="gt-creation-time">{{\Carbon\Carbon::parse($trip_version->created_at)->format('M d, Y')}}</span>
        </div>
    </div>
    <div class="trip-plan-inside-block trip-plan-txt">
        <div class="trip-plan-txt-inner">
            <div class="trip-txt-ttl-layer">
                <h2 class="trip-ttl col-blue">
                    <a href="{{  route('trip.plan', $trip_plan->id) }}">{{$trip_plan->title}}</a>
                </h2>
                <p class="trip-date">{{weatherDate($trip_version->start_date)}} @lang('chat.to') {{weatherDate($trip_version->end_date)}}</p>
            </div>
            <div class="trip-txt-info">
                <div class="trip-info">
                    <div class="icon-wrap">
                        <i class="trav-clock-icon"></i>
                    </div>
                    <div class="trip-info-inner">
                        <p><b>{{ dateDiffDays($trip_version->start_date, $trip_version->end_date) ? dateDiffDays($trip_version->start_date, $trip_version->end_date) : '0 min' }}</b></p>
                        <p>@lang('book.duration')</p>
                    </div>
                </div>
                <div class="trip-info">
                    <div class="icon-wrap">
                        <i class="trav-distance-icon"></i>
                    </div>
                    <div class="trip-info-inner">
                        <p><b>{{ $trip_version->distance ? $trip_version->distance : 0}} km</b></p>
                        <p>@lang('profile.distance')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="trip-plan-inside-block">
        <div class="dest-trip-plan">
            <h3 class="dest-ttl">@lang('profile.destination') <span>{{$places->count()}}</span></h3>
            <ul class="dest-image-list" style="width:170px;">
                @foreach($places->take(6) AS $place)
                    <li>
                        <img src="{{@check_place_photo($place->place)}}"
                             alt="photo" style="width:52px;height:52px;">
                        @if($places->count() > 6 && $loop->iteration === 6)
                            <div style="color: #fff;
                                 position: relative;
                                 width: 52px;
                                 height: 52px;
                                 background: #000;
                                 opacity: .6;
                                 bottom: 52px;
                                 text-align: center;
                                 padding-top: 19px;"
                            >+{{$places->count() - 6}}</div>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>