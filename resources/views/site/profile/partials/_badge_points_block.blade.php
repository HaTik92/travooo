@if(count($user_transactions) > 0)
@foreach($user_transactions as $transactions)
@php  $trans_title = $icon_class = $content='';
      $author_link = '<a href="/profile/'.$transactions->users_id.'">'. $transactions->point_user->name .'</a>';
if($transactions->action == 'checkin'){
    $trans_title=(($transactions->author_id == Auth::user()->id)?'You c':'C').'hecked in'; 
    $content=(($transactions->author_id == Auth::user()->id)?'You j':'J').'ust checked in '; $icon_class="trav-camera"; 
}
elseif($transactions->action == 'post_like'){
    $trans_title= $author_link.' liked a post';
    $content='Just liked a post'; $icon_class="fa fa-thumbs-up"; 
}
elseif($transactions->action == 'post_comment_like'){
    $trans_title= $author_link .' liked a post comment';
    $content='Just liked a post comment'; $icon_class="fa fa-thumbs-up"; 
}
elseif($transactions->action == 'post_comment'){
    $trans_title = $author_link .' commented on a post';
    $content = 'Just commented on a post'; $icon_class="trav-talk-icon"; 
}
elseif($transactions->action == 'post_comment_reply'){
    $trans_title = $author_link . ' replied comment on a post';
    $content='Just replied commented on a post '; $icon_class="trav-talk-icon";
}
elseif($transactions->action == 'discussion_reply_upvote' && is_object($transactions->discussion_reply)){
    $trans_title = $author_link .' upvoted a discussion';
    $content = 'Just upvoted a discussion <a href="">'.substr(@$transactions->discussion_reply->reply, 0, 40).'</a>'; $icon_class="fa fa-chevron-circle-up"; 
}
elseif($transactions->action == 'media_comment'){
    $trans_title = $author_link .' commented a media'; 
    $content = 'Just commented a media'; $icon_class="trav-talk-icon"; 
}
elseif($transactions->action == 'media_like'){
    $trans_title= $author_link .' liked a media'; 
    $content='Just liked a media'; $icon_class="fa fa-thumbs-up"; 
}
elseif($transactions->action == 'media_comment_like'){
    $trans_title = $author_link .' liked a media comment';
    $content = 'Just liked a media comment'; $icon_class="fa fa-thumbs-up"; 
}
elseif($transactions->action == 'review_upvote'){
    $trans_title = $author_link .' upvoted a review';
    $content = 'Just upvoted a review'; $icon_class="fa fa-chevron-circle-up"; 
}
elseif($transactions->action == 'followed_by'){
    $trans_title = $author_link.' followed You'; 
    $content= 'Just followed by <a href="">'.$transactions->follow_users->name.'</a>'; $icon_class="trav-talk-icon";
}
elseif($transactions->action == 'friends_with'){
    $trans_title = '<a href="/profile/'.$transactions->follow_users->id.'">'. $transactions->follow_users->name .'</a> friends with'; 
    $content = 'Just friends with <a href="">'.$transactions->follow_users->name.'</a>'; $icon_class="trav-talk-icon";
}
elseif($transactions->action == 'report_comment'){
    $trans_title = $author_link.' commented a report';
    $content = 'Just commented a report <a href="">'.str_limit(@$transactions->report->title, 40).'</a>'; $icon_class="trav-talk-icon";
}
elseif($transactions->action == 'report_comment_reply'){
    $trans_title = $author_link.' replied comment a report';
    $content = 'Just replied comment a report <a href="">'.str_limit(@$transactions->report->title, 40).'</a>'; $icon_class="trav-talk-icon";
}
elseif($transactions->action == 'report_like'){
    $trans_title = $author_link .' liked a report';
    $content = 'Just liked a report <a href="">'.str_limit(@$transactions->report->title, 40).'</a>'; $icon_class="fa fa-thumbs-up";
}
elseif($transactions->action == 'report_comment_like'){
    $trans_title = $author_link.' liked a report comment';
    $content = 'Just liked a report comment<a href="">'.str_limit(@$transactions->report->title, 40).'</a>'; $icon_class="fa fa-thumbs-up";
}
elseif($transactions->action == 'profile_complete'){
    $trans_title=(($transactions->users_id == Auth::user()->id)?'You c':'C').'ompleted profile';
    $content=(($transactions->users_id == Auth::user()->id)?'You j':'J').'ust completed your profile'; $icon_class="fa fa-user-circle";
}
elseif($transactions->action == 'trip_version_accepted'){
    $trans_title = $author_link .' accepted a trip version';
    $content = 'Just accepted <a href="">'.str_limit($transactions->trip->title, 40).'</a> trip version'; $icon_class="trav-trip-plans-icon";
}
elseif($transactions->action == 'trip_like'){
    $trans_title = $author_link.' liked a trip'; 
    $content = 'Just liked trip<a href="">'.str_limit(@$transactions->trip->title, 40).'</a>'; $icon_class="fa fa-thumbs-up";
}
elseif($transactions->action == 'trip_comment'){
    $trans_title = $author_link.' commented a trip';
    $content = 'Just commented a trip <a href="">'.str_limit($transactions->trip->title, 40).'</a>'; $icon_class="trav-talk-icon";
}
elseif($transactions->action == 'trip_share'){
    $trans_title = $author_link .' shared a trip';
    $content='Just shared a trip <a href="">'.str_limit($transactions->trip->title, 40).'</a>'; $icon_class="trav-share-icon";
}
@endphp
@if($trans_title && $content)
<div class="point-layer">
    <div class="point-time">
        <span>{{diffForHumans($transactions->created_at)}}</span>
    </div>
    <div class="point-inner-block">
        <div class="point-main">
            <div class="point-ttl">
                <i class="{{$icon_class}}"></i>
                <span>{!!$trans_title!!}</span>
            </div>
            <div class="point-txt">
                {!!$content!!}
            </div>
        </div>
        <div class="point-info">
            <span>+{{$transactions->points}} Points</span>
        </div>
    </div>
</div>
@endif
@endforeach
@endif
