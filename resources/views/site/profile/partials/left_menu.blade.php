<ul class="left-outside-menu">
    <li>
        <a href="{{ route('home')}}">
            <i class="trav-home-icon"></i>
            <span>@lang('navs.general.home')</span>
            <!--<span class="counter">5</span>-->
        </a>
    </li>
    <li class="active">
        <a href="{{ route('profile.about') }}">
            <i class="trav-profile-icon"></i>
            <span>@lang('navs.frontend.about')</span>
        </a>
    </li>
    <li>
        <a href="{{ route('profile.travel_history') }}">
            <i class="trav-travel-history-icon"></i>
            <span>@lang('profile.nav.travel_history')</span>
        </a>
    </li>

    <li>
        <a href="{{ route('profile.map') }}">
            <i class="trav-map-o"></i>
            <span>@lang('profile.nav.my_map')</span>
        </a>
    </li>
    <li>
        <a href="{{route('profile.plans') }}">
            <i class="trav-trip-plans-icon"></i>
            <span>@lang('trip.trip_plans')</span>
        </a>
    </li>
    <li>
        <a href="{{ route('profile.photos')}}">
            <i class="trav-photos-icon"></i>
            <span>@lang('place.photos')</span>
        </a>
    </li>
    <li>
        <a href="{{  route('profile.videos') }}">
            <i class="trav-videos-icon"></i>
            <span>@lang('place.videos')</span>
        </a>
    </li>
    <li>
        <a href="{{url_with_locale('profile-reviews')}}">
            <i class="trav-review-icon"></i>
            <span>@lang('place.reviews')</span>
        </a>
    </li>
</ul>

