<div class="trip-plan-row trip-{{$plan['id']}}">
    <div class="trip-plan-inside-block">
        <div class="trip-plan-map-img gt-map-main-block" style="width:140px;">
            <img src="{{$plan['trip_image']}}" alt="map-image"
                 style="width:140px;height:150px;">
            <span class="gt-creation-time">{{\Carbon\Carbon::parse($plan['created_at'])->format('M d, Y')}}</span>
        </div>
    </div>
    <div class="trip-plan-inside-block trip-plan-txt">
        <div class="trip-plan-txt-inner">
            <div class="trip-txt-ttl-layer">
                <h2 class="trip-ttl col-blue"><a href="{{$plan['url']}}">{{$plan['title']}}</a>
                </h2>
                <p class="trip-date">{{weatherDate($plan['start_date'])}}
                    @lang('chat.to') {{weatherDate($plan['end_date'])}}</p>

                @if($login_user_id != '')
                <div class="dropdown edit-paln-dropdown">
                    <button class="btn btn-custom-light-primary btn-bordered" type="button"
                            data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow l-16">
                        <a class="dropdown-item edit-trip-item"  href="{{$plan['url'] . '?do=edit'}}">
                            <span class="icon-wrap panel-icon-wrap">
                                <img src="{{ asset('assets2/image/icon-pencil.png') }}" style="width: 32px;">
                            </span>
                            <div class="drop-txt">
                                <p><b>Edit Trip Plan</b></p>
                            </div>
                        </a>
                        <a class="dropdown-item delete-trip" href="#" data-toggle="modal" data-target="#deleteTripModal" data-id="{{$plan['id']}}">
                            <span class="icon-wrap close-icon-wrap">
                                <i class="trav-close-icon"></i>
                            </span>
                            <div class="drop-txt">
                                <p><b>Delete</b></p>
                            </div>
                        </a>
                    </div>
                </div>
                @endif

            </div>
            <div class="trip-txt-info">
                <div class="trip-info">
                    <div class="icon-wrap">
                        <i class="trav-clock-icon"></i>
                    </div>
                    <div class="trip-info-inner">
                        <p><b>{{$plan['duration']}}</b>
                        </p>
                        <p>@lang('book.duration')</p>
                    </div>
                </div>
                <div class="trip-info">
                    <div class="icon-wrap">
                        <i class="trav-distance-icon"></i>
                    </div>
                    <div class="trip-info-inner">
                        <p><b>{{$plan['distance']}}</b></p>
                        <p>@lang('profile.distance')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="trip-plan-inside-block">
        <div class="dest-trip-plan">
            <h3 class="dest-ttl">@lang('profile.destination') <span>{{$plan['destination_count']}}</span>
            </h3>
            <ul class="dest-image-list" style="width:170px;">
                @foreach($plan['destination'] AS $place)
                <li>
                    <img src="{{$place}}"
                         alt="photo" style="width:52px;height:52px;">
                    @if($plan['destination_count'] > 6 && $loop->iteration === 6)
                    <div style="color: #fff;
                         position: relative;
                         width: 52px;
                         height: 52px;
                         background: #000;
                         opacity: .6;
                         bottom: 52px;
                         text-align: center;
                         padding-top: 19px;
                         ">+{{$plan['destination_count'] - 6}}</div>
                    @break
                    @endif
                </li>
                @endforeach

            </ul>
        </div>
    </div>
</div>
