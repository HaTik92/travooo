@if(count($media_list)>0)
@php $index_number = 0; @endphp
    @foreach($media_list as $date=>$medias)
        <div class="side-photo-info">
            <div class="side-photo-time">{{$date}}</div>
            <div class="side-photo-wrap all-media-list d-flex">
                @foreach($medias as $k=>$media)
                    @if($media_type == 'photo')
                        @if($loop->index<=4)
                         <div class="timeline-media lightbox_imageModal" data-id="{{$index_number}}">
                            <img src="{{get_profile_media($media)}}" alt="" class="" width="50" height="50">
                         </div>
                        @php $index_number++; @endphp
                        @else
                            @if($loop->index == 5)
                            <div class="photo-cover-inner lightbox_imageModal" data-id="{{$index_number}}">
                                <img src="{{get_profile_media($media)}}" alt="" class="" width="50" height="50">
                                <span><b>{{count($medias) - 5}}+</b></span>
                            </div>
                            @php $index_number = $index_number+(count($medias) - 5); @endphp
                            @endif
                        @endif
                    @else
                        @php
                             if(strpos( $media, "https://s3.amazonaws.com") !== false)
                                 $file_url = $media;
                             else
                                 $file_url = 'https://s3.amazonaws.com/travooo-images2/' . $media;
                         @endphp
                        @if($loop->index<=2)
                        <video class="aside-videos lightbox_videoModal" id="" data-id="{{$k}}" oncontextmenu="return false;"  width="70" height="50">
                             <data-src src="{{$file_url}}#t=5" type="video/mp4"></data-src>
                               Your browser does not support the video tag.
                       </video>
                        @else
                            @if($loop->index == 3)
                            <div class="photo-cover-inner video-cover">
                                <video class="aside-videos lightbox_videoModal" id="" data-id="" oncontextmenu="return false;" width="70" height="50">
                                         <data-src src="{{$file_url}}#t=5" type="video/mp4"></data-src>
                                        Your browser does not support the video tag.
                                </video>
                                <span class="lightbox_videoModal" data-id="{{$k}}"><b>{{count($medias) - 3}}+</b></span>
                            </div>
                            @endif
                        @endif
                    @endif
                @endforeach
            </div>
        </div>
    @endforeach
@endif
