<?php
$place = \App\Models\ActivityMedia\Media::find($post->variable);
$place_review = $place->reviews()->where('by_users_id', $post->users_id)->get()->first();
//dd($place_review);
?>
@if(is_object($place))
<div class="post-block">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <img src="{{check_profile_picture($post->user->profile_picture)}}" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{url('profile/'.$post->user->id)}}">{{$post->user->name}}</a>
                    {!! get_exp_icon($post->user) !!}
                </div>
                <div class="post-info">
                    @lang('home.gave_a_rating_of') {{@$place_review->score}} @lang('chat.to') <a
                            href="{{ route('place.index', @$place->id) }}"
                            class="link-place">{{@$place->transsingle->title}}</a> {{diffForHumans($post->time)}}
                </div>
            </div>
        </div>
        @if(Auth::user()->id && Auth::user()->id!==$post->user->id)
            <div class="post-top-info-action">
                <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Placereview">
                        @include('site.home.partials._info-actions', ['post'=>$place_review])
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="post-image-container post-follow-container">
        <ul class="post-image-list">
            <li>
                <img class="lazy" data-src="https://s3.amazonaws.com/travooo-images2/th700/{{@$place->getMedias[0]->url}}" alt="">
            </li>
        </ul>
        <div class="post-follow-block">
            <div class="follow-txt-wrap">
                <div class="follow-flag-wrap">
                </div>
                <div class="follow-txt">
                    <p class="follow-name">
                        <a href='{{ route('place.index', $place->id) }}'>
                        {{$place->transsingle->title}}
                        </a>
                        <div class="follow-tag-wrap">
                        <span class="follow-tag">{{do_placetype($place->place_type)}}</span>
                        </div>
                    </p>
                    <div class="follow-foot-info">

                    </div>
                </div>
            </div>
            <div class="follow-btn-wrap check-follow-place" data-id='{{$place->id}}'>

            </div>
        </div>
    </div>

</div>
@endif
