@foreach($videos AS $video)
<div class="media-wrap" datesort="{{$video->media->uploaded_at?strtotime($video->media->uploaded_at):0}}">
    <div class="media-inner video-inner">
        @php
        if(strpos( $video->media->url, "https://s3.amazonaws.com") !== false)
        $file_url = $video->media->url;
        else
        $file_url = 'https://s3.amazonaws.com/travooo-images2/' . $video->media->url;
        @endphp
        <video class="profile-video lightbox_videoModal" id="video-{{$index_num}}" data-id="{{$video->id}}" width="195" height="195"  preload="metadata">
            <data-src src="{{$file_url}}#t=5" type="video/mp4"></data-src>
            Your browser does not support the video tag.
        </video>
        <a href="javascript:;" class="video-play-btn lightbox_videoModal video-{{$index_num}} d-none" data-id="{{$video->id}}"><i class="fa fa-play" aria-hidden="true"></i></a>
        <a href="" class="image-video-{{$index_num}}"></a>
        <div class="media-cover-txt">
            <div class="media-info media-foot">
                <div class="foot-layer media-liked-count">
                    <b>{{count($video->media->likes)}}</b>
                    <span>@lang('profile.likes')</span>
                </div>
                <div class="foot-layer media-commented-count">
                    <b>{{count($video->media->comments)}}</b>
                    <span>@lang('comment.comments')</span>
                </div>
                <time class="video-duration time-video-{{$index_num}}"></time>
            </div>
        </div>
    </div>
</div>
@php $index_num++; @endphp
@endforeach