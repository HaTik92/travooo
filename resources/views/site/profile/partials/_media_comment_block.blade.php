<?php
$comment_childs = \App\Models\ActivityMedia\MediasComments::where('reply_to', '=', $comment->id)->orderby('created_at','DESC')->get();
//dd($place_review);
?>
<div topSort="{{count($comment->likes) + count($comment_childs)}}" newSort="{{strtotime($comment->created_at)}}">
<div class="post-comment-row" id="commentRow{{$comment->id}}">
    <div class="post-com-avatar-wrap">
        <img src="{{@check_profile_picture($comment->user->profile_picture)}}" alt="">
    </div>
    <div class="post-comment-text">
        <div class="comment-bottom-info">
            <a href="#" class="comment-name">{{@$comment->user->name}}</a>
            {!! get_exp_icon(@$comment->user) !!}
            <!--<a href="#" class="comment-nickname">@katherin</a>-->
        </div>
        <div class="comment-bottom-info">
            <p>{!!$comment->comment!!}</p>
        </div>
        <div class="comment-bottom-info" style="display: flow-root">
            @if(is_object($comment->medias))
                @php
                    $index = 0;

                @endphp
                @foreach($comment->medias AS $photo)
                    @php
                        $index++;
                        $file_url = $photo->media->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                    @endphp
                        <div style="overflow: hidden; display: flex; float: left; margin: 5px;">
                            @if(in_array($ext, $allowed_video))
                            <video style="object-fit: cover" width="192" height="210" controls>
                                <source src="{{$file_url}}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            @else
                            <a href="{{$file_url}}" data-lightbox="{{$file_url}}">
                                <img src="./assets2/image/posts/90_1562264444_2.png" alt="" style="width:192px;height:210px;object-fit: cover;">
                            </a>
                            @endif
                        </div>
                @endforeach


            @endif
        </div>
        <div class="comment-bottom-info">
            <div class="com-time" posttype="Mediacomment">{{diffForHumans($comment->created_at)}}
            - <span href="#" class="postCommentLikes" id="{{$comment->id}}">{{@count($comment->likes)}} @lang('home.likes')</span>
            - <a href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$comment->id}},this)"> Report</a>
            </div>
        </div>
    </div>
</div>

@if(count($comment_childs) > 0)
    @foreach($comment_childs as $child)

        <div class="post-comment-row doublecomment" id="commentRow{{$child->id}}">
            <div class="post-com-avatar-wrap">
                <img src="{{check_profile_picture($child->user->profile_picture)}}" alt="">
            </div>
            <div class="post-comment-text">
                <div class="comment-bottom-info">
                    <a href="#" class="comment-name">{{$child->user->name}}</a>
                    {!! get_exp_icon($child->user) !!}
                    <!--<a href="#" class="comment-nickname">@katherin</a>-->
                </div>
                <div class="comment-bottom-info">
                    <p>{!!$child->text!!}</p>
                </div>
                <div class="comment-bottom-info" style="display: flow-root">
                    @if(is_object($child->medias))
                        @php
                            $index = 0;

                        @endphp
                        @foreach($child->medias AS $photo)
                            @php
                                $file_url = $photo->media->url;
                                $file_url_array = explode(".", $file_url);
                                $ext = end($file_url_array);
                                $allowed_video = array('mp4');
                            @endphp
                                <div style="overflow: hidden; display: flex;float:left; margin:5px;">
                                    @if(in_array($ext, $allowed_video))
                                    <video style="object-fit: cover" width="192" height="210" controls>
                                        <source src="{{$file_url}}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                    @else
                                    <a href="{{$file_url}}" data-lightbox="{{$file_url}}">
                                        <img src="{{$file_url}}" alt="" style="width:192px;height:210px;object-fit: cover;">
                                    </a>
                                    @endif
                                </div>
                        @endforeach

                    @endif
                </div>
                <div class="comment-bottom-info">
                    <div class="com-time" posttype="Mediacomment">{{diffForHumans($child->created_at)}}
                        - <a href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$child->id}},this)"> Report</a>
                    </div>
                </div>
            </div>
        </div>

    @endforeach
@endif


</div>
