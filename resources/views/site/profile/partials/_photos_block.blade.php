@foreach($photos AS $photo)
<div class="media-wrap" datesort="{{$photo->media->uploaded_at?strtotime($photo->media->uploaded_at):0}}" style="cursor: pointer;">
    <div class="media-inner">
        <div class="thumbnail lightbox_imageModal" data-id="{{ $index_num }}" style="width:195px;height:195px;">
            <img src="{{get_profile_media($photo->media->url)}}" style="width:300px;height:250px;object-fit: cover;" onerror="this.style.display='none';this.parentElement.parentElement.parentElement.style.display='none'" />

            <div class="media-cover-txt">
                <div class="media-info media-foot">
                    <div class="foot-layer {{$photo->media->id}}-media-liked-count liked-count">
                        <b>{{count($photo->media->likes)}}</b>
                        <span>@lang('profile.likes')</span>
                    </div>
                    <div class="foot-layer media-commented-count" style="cursor: pointer">
                        <b>{{count($photo->media->comments)}}</b>
                        <span>@lang('profile.comments')</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@php $index_num++; @endphp
@endforeach