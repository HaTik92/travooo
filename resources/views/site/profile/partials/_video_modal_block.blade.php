<div class=" modal--light custom-modal prifile-video-modal" id="profile_video_modal">
    <button class="modal__close v-modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <svg class="icon icon--close"><use xlink:href="{{asset('assets3/img/sprite.svg#close')}}"></use></svg>
    </button>
    <div class="p-video-container video-gallery-block">
        @foreach($lightbox_videos as $video)
        @php
        if(strpos( $video->media->url, "https://s3.amazonaws.com") !== false)
        $file_url = $video->media->url;
        else
        $file_url = 'https://s3.amazonaws.com/travooo-images2/' . $video->media->url;
        @endphp
        <div class="lg-item lg-loaded lg-complete d-none video-inner-{{$video->id}}">
            <div class="lg-video-wrap" style="padding-right: 390px;">
                <video class="video-card__img video-autoplay" style="width:100%;height:600px;object-fit: cover" id="lg_video_{{$video->id}}"  preload="metadata">
                    <source src="{{$file_url}}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
                <a href="javascript:;" class="v-play-btn lg_video_{{$video->id}} " data-v_id="{{$video->id}}"><i class="fa fa-play" aria-hidden="true"></i></a>
            </div>
            <div class='cover-block' id='{{ $video->media->id }}'>
                <div class='cover-block-inner comment-block'>
                    <ul class='modal-outside-link-list white-bg'>
                        <li class='outside-link v-modal-close'>
                            <a href='#'>
                                <div class='round-icon'>
                                    <i class='trav-angle-left'></i>
                                </div>
                                <span>@lang("other.back")</span>
                            </a>
                        </li>
                        <li class='outside-link'>
                            <a href='#' class='photo_like_button' id='{{ $video->media->id }}'>
                                <div class='round-icon {{ $video->media->id }}-media-like-icon' data-type="{{(count($video->media->likes()->where('users_id', $authUserId)->get()))?'like':'unlike'}}">
                                    @if(count($video->media->likes()->where('users_id', $authUserId)->get()) > 0)
                                    <i class="trav-heart-fill-icon" style="color:#ff5a79;"></i>
                                    @else
                                    <i class="trav-heart-icon"></i>
                                    @endif
                                </div>
                                <span>Like</span>
                            </a>
                        </li>
                    </ul>
                    <div class='gallery-comment-wrap'>
                        <div class='gallery-comment-inner mCustomScrollbar'>

                            @if(isset($video->media->users[0]) && is_object($video->media->users[0]))
                            <div class='top-gallery-content gallery-comment-top'>
                                <div class='top-info-layer'>
                                    <div class='top-avatar-wrap'>
                                        @if(isset($video->media->users[0]) && is_object($video->media->users[0]) && $video->media->users[0]->profile_picture != '')
                                        <img src='{{check_profile_picture($video->media->users[0]->profile_picture)}}' alt='' style='width:50px;hright:50px;'>
                                        @endif
                                    </div>
                                    <div class='top-info-txt'>
                                        <div class='preview-txt'>
                                            @if(isset($video->media->users[0]) && is_object($video->media->users[0]) && $video->media->users[0]->name != '')
                                            <a class='dest-name' href='#'>{{$video->media->users[0]->name}}</a>
                                            {!! get_exp_icon($video->media->users[0]) !!}
                                            @endif
                                            @if($video->media->uploaded_at)
                                                <p class='dest-place'>@lang("place.uploaded_a") <b>@lang("place.photo")</b> <span class="date">{{diffForHumans($video->media->uploaded_at)}}</span></p>
                                            @endif
                                        </div>
                                    </div>
                                    @if(Auth::check() && Auth::user()->id && Auth::user()->id!=$video->media->users[0]->id)
                                        <div class="d-inline-block pull-right">
                                            <div class="post-top-info-action">
                                                <div class="dropdown">
                                                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                        <i class="trav-angle-down"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Mediaupload">
                                                        @include('site.home.partials._info-actions', ['post'=>$video->media])
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class='gallery-comment-txt'>
                                    @if($video->media->title)
                                    @php

                                    $dom = new \DOMDocument;
                                    @$dom->loadHTML($video->media->title);
                                    $elements = @$dom->getElementsByTagName('div');

                                    if(count($elements)> 0){
                                    $content = $elements[0]->nodeValue;
                                    }else{
                                    $content = $video->media->title;
                                    }
                                    $showChar = 100;
                                    $ellipsestext = "...";

                                    if(strlen($content) > $showChar) {

                                    $c = substr($content, 0, $showChar);
                                    $h = substr($content, $showChar, strlen($content) - $showChar);

                                    $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span>';

                                    $content = $html;
                                    }
                                    @endphp
                                    <p style="word-wrap: break-word;">{!!$content!!}</p>
                                    @else
                                    <p>No Description ...</p>
                                    @endif
                                </div>
                                <div class='gal-com-footer-info'>
                                    <div class='post-foot-block post-reaction'>
                                        <i class='trav-heart-fill-icon'></i>
                                        <span class="{{$video->media->id}}-like-count"><b>{{count($video->media->likes)}}</b></span>
                                    </div>
                                    <div class='post-foot-block'>
                                        <i class="trav-comment-icon" dir="auto"></i>
                                        <span style="color: #2b2b2b"><b class="{{$video->media->id}}-all-comments-count">{{@count($video->media->comments)}}</b>&nbsp;@lang('comment.comments')</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class='post-comment-layer photo-comment-{{$video->media->id}}'>
                                <div class="post-comment-top-info" id="{{$video->media->id}}" >
                                    <ul class="comment-filter">
                                        <li class="comment-filter-type" data-type="Top">@lang('other.top')</li>
                                        <li class="comment-filter-type active" data-type="New">@lang('other.new')</li>
                                    </ul>
                                    <div class="comm-count-info">
                                        <strong>0</strong> / <span class="{{$video->media->id}}-all-comments-count">{{count($video->media->comments)}}</span>
                                    </div>
                                </div>
                                <div class='post-comment-wrapper sortBody' data-id="{{$video->media->id}}">
                                    @if(isset($video->media->comments) && count($video->media->comments) > 0)
                                    @foreach($video->media->comments AS $comment)
                                    @include('site.home.partials.media_comment_block', ['post_object'=>$video->media])
                                    @endforeach
                                    @else
                                    <div class="post-comment-top-info comment-not-fund-info" id="{{$video->media->id}}">
                                        <ul class="comment-filter pl-4">
                                            <li>No comments yet ..</li>
                                            <li></li>
                                        </ul>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if($authUserId)
                        <form class='media_comment_form' method='post' action='#' autocomplete='off'>
                            <div class='post-add-comment-block'>
                                <div class='avatar-wrap'>
                                    <img src='{{check_profile_picture(\App\Models\User\User::query()->find($authUserId)->profile_picture)}}' style='width:45px;height:45px;'>
                                </div>
                                <div class="post-add-com-inputs">
                                    {{ csrf_field() }}
                                    <input type="hidden" data-id="pair{{$video->media->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                                    <div class="post-create-block post-comment-create-block post-regular-block video-comment-block" id="createPostBlock" tabindex="0">
                                        <div class="post-create-input">
                                            <textarea name="text" data-id="mediacommenttext" class="textarea-customize modal-texarea"
                                                      style="display:inline;vertical-align: top;min-height:50px;" oninput="comment_textarea_auto_height(this, 'regular')" placeholder="@lang('comment.write_a_comment')"></textarea>
                                            <div class="medias modal-medias"></div>
                                        </div>

                                        <div class="post-create-controls">
                                            <div class="post-alloptions">
                                                <ul class="create-link-list">
                                                    <li class="post-options">
                                                        <input type="file" name="file[]" class="profile-media-file-input" data-comment_id="{{$video->media->id}}" id="mediacommentfile{{$video->media->id}}" style="display:none" multiple>
                                                        <i class="fa fa-camera click-target" data-target="mediacommentfile{{$video->media->id}}"></i>
                                                    </li>
                                                </ul>
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-disabled d-none"></button>
                                        </div>

                                    </div>
                                    <input type="hidden" name="medias_id" value="{{$video->media->id}}"/>
                                </div>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
            </div>

        </div>
        @endforeach      

    </div>
    <div class="video__cards" style="margin-top: 15px;padding-top:10px;padding-bottom:10px;">
        <div class="video__cards-items profile-video-padding">

            @foreach($lightbox_videos as $video)

            @php
            if(strpos( $video->media->url, "https://s3.amazonaws.com") !== false)
            $file_url = $video->media->url;
            else
            $file_url = 'https://s3.amazonaws.com/travooo-images2/' . $video->media->url;
            @endphp
            <div class="video__card v-card-{{ $video->id }}" data-id="{{ $video->id }}" data-src="{{ $file_url }}" onclick="getVideoModal({{$video->id}}, this)">
                <div class="video-card">
                    <div class="video-card__img-wrap">
                        <video class="video-card__img" style="width:100%;height:160px;">
                            <source src="{{$file_url}}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        <div class="video-card__header">
                            <div class="video-card__likes"><i class="fa fa-heart fill" aria-hidden="true" dir="auto"></i> {{ @count($video->likes) }}</div>

                            <div class="video-card__comments"><svg class="icon icon--comment">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#comment')}}"></use>
                                </svg><span class="{{$video->id}}-comment-count">{{ @count(@$video->comments) }}</span></div>
                        </div>

                        <div class="gallery__icon-play"><svg class="icon icon--play-solid">
                            <use xlink:href="{{asset('assets3/img/sprite.svg#play-solid')}}"></use>
                            </svg></div>
                    </div>

                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>


