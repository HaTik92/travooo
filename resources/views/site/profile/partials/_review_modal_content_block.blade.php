<div class="p-video-container review-gallery-block">
    @foreach($medias as $media)
    @php
        $file_url = $media->url;
        $file_url_array = explode(".", $file_url);
        $ext = end($file_url_array);
        $allowed_video = array('mp4');
    @endphp
    <div class="lg-item lg-loaded lg-complete d-none review-inner-{{$media->id}}">
        <div class="lg-video-wrap">
            @if(in_array($ext, $allowed_video))
            <video class="video-card__img review-video-autoplay" style="width:100%;height:600px;object-fit: cover" id="lg_video_{{$media->id}}"  preload="metadata">
                <source src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" type="video/mp4">
                Your browser does not support the video tag.
            </video>
            <a href="javascript:;" class="r-play-btn lg_video_{{$media->id}} " data-r_id="{{$media->id}}"><i class="fa fa-play" aria-hidden="true"></i></a>
            @else
                <img src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" class="" alt="" style="width: 530px;">
            @endif
        </div>
    </div>    
   @endforeach
</div>
<div class="review__cards" style="margin-top: 15px;padding-top:10px;padding-bottom:10px;">
    <div class="review__cards-items">

        @foreach($medias as $media)
        @php
            $file_url = $media->url;
            $file_url_array = explode(".", $file_url);
            $ext = end($file_url_array);
            $allowed_video = array('mp4');
        @endphp
            <div class="review__card r-card-{{ $media->id }}" data-id="{{ $media->id }}" data-src="https://s3.amazonaws.com/travooo-images2/{{ $file_url }}" data-type="{{in_array($ext, $allowed_video)?'video':'image'}}" onclick="getReviewMediaModal({{$media->id}}, this)">
                @if(in_array($ext, $allowed_video))
                    <div class="review-card">
                        <div class="video-card__img-wrap">
                            <video class="video-card__img" style="width:100%;height:192px;" oncontextmenu="return false;">
                                <source src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            <div class="gallery__icon-play"><svg class="icon icon--play-solid">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#play-solid')}}"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                @else
                <div class="review-card">
                    <img src="https://s3.amazonaws.com/travooo-images2/{{$file_url}}" class="" alt="" style="width: 270px;height:210px;object-fit: cover;">
                </div>
                @endif
            </div>
        @endforeach
    </div>
</div>


