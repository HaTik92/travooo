<div class='cover-block' id='{{ $photo->media->id }}'>
    <div class='cover-block-inner comment-block'>
        <ul class='modal-outside-link-list white-bg' style='display: none;'>
            <li class='outside-link'>
                <a href='#'>
                    <div class='round-icon'>
                        <i class='trav-angle-left'></i>
                    </div>
                    <span>@lang("other.back")</span>
                </a>
            </li>
            <li class='outside-link'>
            </li>
        </ul>
        <div class='gallery-comment-wrap'>
            <div class='gallery-comment-inner mCustomScrollbar'>

                @if(isset($photo->media->users[0]) && is_object($photo->media->users[0]))
                <div class='top-gallery-content gallery-comment-top'>
                    <div class='top-info-layer'>
                        <div class='top-avatar-wrap'>
                            @if(isset($photo->media->users[0]) && is_object($photo->media->users[0]) && $photo->media->users[0]->profile_picture != '')
                            <img src='{{check_profile_picture($photo->media->users[0]->profile_picture)}}' alt='' style='width:50px;hright:50px;'>
                            @endif
                        </div>
                        <div class='top-info-txt'>
                            <div class='preview-txt'>
                                @if(isset($photo->media->users[0]) && is_object($photo->media->users[0]) && $photo->media->users[0]->name != '')
                                <a class='dest-name' href='#'>{{$photo->media->users[0]->name}}</a>
                                    {!! get_exp_icon($photo->media->users[0]) !!}
                                @endif
                                @if($photo->media->uploaded_at)
                                    <p class='dest-place'>@lang("place.uploaded_a") <b>@lang("place.photo")</b> <span class="date">{{diffForHumans($photo->media->uploaded_at)}}</span></p>
                                @endif
                            </div>
                        </div>
                        @if(Auth::check() && Auth::user()->id && Auth::user()->id!==$photo->media->users[0]->id)
                            <div class="d-inline-block pull-right">
                                <div class="post-top-info-action">
                                    <div class="dropdown">
                                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false">
                                            <i class="trav-angle-down"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Mediaupload">
                                            @include('site.home.partials._info-actions', ['post'=>$photo->media])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class='gallery-comment-txt'>
                        @if($photo->media->title)
                            @php

                            $dom = new \DOMDocument;
                            @$dom->loadHTML($photo->media->title);
                            $elements = @$dom->getElementsByTagName('div');

                            if(count($elements)> 0){
                                $content = $elements[0]->nodeValue;
                            }else{
                                $content = $photo->media->title;
                            }
                                $showChar = 100;
                                $ellipsestext = "...";

                                if(strlen($content) > $showChar) {

                                    $c = substr($content, 0, $showChar);
                                    $h = substr($content, $showChar, strlen($content) - $showChar);

                                    $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span>';

                                    $content = $html;
                                }
                            @endphp
                            <p style="word-wrap: break-word;">{!!$content!!}</p>
                        @else
                            <p>No Description ...</p>
                        @endif
                    </div>
                    <div class='gal-com-footer-info'>
                        <div class='post-foot-block post-reaction'>
                            <a href='#' class='{{Auth::check()?'photo_like_button':'open-login'}}' id='{{ $photo->media->id }}'>
                                <div class='round-icon {{ $photo->media->id }}-media-like-icon' data-type="{{(count($photo->media->likes()->where('users_id', $authUserId)->get()))?'like':'unlike'}}">
                                    @if(count($photo->media->likes()->where('users_id', $authUserId)->get()) > 0)
                                        <i class="trav-heart-fill-icon" style="color:#ff5a79;"></i>
                                    @else
                                        <i class="trav-heart-icon"></i>
                                    @endif
                                </div>
                            </a>
                            <span class="{{$photo->media->id}}-like-count"><b>{{count($photo->media->likes)}}</b></span>
                        </div>
                        <div class='post-foot-block'>
                            <i class="trav-comment-icon" dir="auto"></i>
                            <span style="color: #2b2b2b"><b class="{{$photo->media->id}}-all-comments-count">{{@count($photo->media->comments)}}</b>&nbsp;@lang('comment.comments')</span>
                        </div>
                    </div>
                </div>
                @endif
                <div class='post-comment-layer photo-comment-{{$photo->media->id}}'>
                    <div class="post-comment-top-info" id="{{$photo->media->id}}" >
                        <ul class="comment-filter">
                            <li class="comment-filter-type" data-type="Top">@lang('other.top')</li>
                            <li class="comment-filter-type active" data-type="New">@lang('other.new')</li>
                        </ul>
                        <div class="comm-count-info">
                            <strong>0</strong> / <span class="{{$photo->media->id}}-all-comments-count">{{count($photo->media->comments)}}</span>
                        </div>
                    </div>
                    <div class='post-comment-wrapper sortBody' data-id="{{$photo->media->id}}">
                        @if(isset($photo->media->comments) && count($photo->media->comments) > 0)
                        @foreach($photo->media->comments AS $comment)
                            @include('site.home.partials.media_comment_block', ['post_object'=>$photo->media])
                        @endforeach
                        @else
                            <div class="post-comment-top-info comment-not-fund-info" id="{{$photo->media->id}}">
                                <ul class="comment-filter pl-4">
                                    <li>No comments yet ..</li>
                                    <li></li>
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @if($authUserId)
            <form class='media_comment_form' method='post' action='#' autocomplete='off'>
                <div class='post-add-comment-block'>
                    <div class='avatar-wrap'>
                        <img src='{{check_profile_picture(\App\Models\User\User::query()->find($authUserId)->profile_picture)}}' style='width:45px;height:45px;'>
                    </div>
                        <div class="post-add-com-inputs">
                            {{ csrf_field() }}
                            <input type="hidden" data-id="pair{{$photo->media->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                            <div class="post-create-block post-comment-create-block post-regular-block photo-comment-block" id="createPostBlock" tabindex="0">
                                <div class="post-create-input">
                                    <textarea name="text" data-id="mediacommenttext" class="textarea-customize modal-texarea"
                                        style="display:inline;vertical-align: top;min-height:50px;" oninput="comment_textarea_auto_height(this, 'regular')" placeholder="@lang('comment.write_a_comment')"></textarea>
                                    <div class="medias modal-medias"></div>
                                </div>

                                <div class="post-create-controls">
                                    <div class="post-alloptions">
                                        <ul class="create-link-list">
                                            <li class="post-options">
                                                <input type="file" name="file[]" class="profile-media-file-input" data-comment_id="{{$photo->media->id}}" id="mediacommentfile{{$photo->media->id}}" style="display:none" multiple>
                                                <i class="fa fa-camera click-target" data-target="mediacommentfile{{$photo->media->id}}"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-disabled d-none"></button>
                                </div>

                            </div>
                            <input type="hidden" name="medias_id" value="{{$photo->media->id}}"/>
                    </div>
                </div>
            </form>
            @endif
        </div>
    </div>
</div>
