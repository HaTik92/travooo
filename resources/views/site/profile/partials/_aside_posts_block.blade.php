@php
    $following_lists = $user->getFollowingList('all', 0);
    $view_following = true;

     if(isset($user->privacy->see_my_following) && $user->privacy->see_my_following != 0){
         if(!is_friend($user->id) && $user->privacy->see_my_following == 1){
             $view_following = false;
         }

        if(!check_follow($user->id) && $user->privacy->see_my_following == 2){
             $view_following = false;
        }
     }
@endphp
<div class="post-block post-side-profile sm-profile">
    <div class="post-profile-info">
        <ul class="profile-info-list">
            <li>
                @if(isset($user_id))
                    <a href="{{url('profile-posts/'.$user_id)}}">
                        @else
                            <a href="{{route('profile.posts')}}">
                                @endif
                                <p class="info-count">{{(session()->has('posts_count')?session()->get('posts_count'):0)}}</p>
                                <p class="info-label">Posts</p>
                            </a>
            </li>
            <li class="followers-inner" data-toggle="modal" data-target="#mefollowers">
                <p class="info-count followers-count">{{count($user->get_followers)}}</p>
                <p class="info-label">Followers</p>
            </li>
            <li class="followers-inner" data-toggle="modal" @if($view_following) data-target="#mefollowing" @endif>
                <p class="info-count following-count">{{$following_lists['all_count']}}</p>
                <p class="info-label">Following</p>
            </li>
        </ul>
    </div>
</div>
