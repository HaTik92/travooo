@foreach($reviews AS $review)
<div class="review-block">
    <div class="review-top">
        <div class="top-main">
            <div class="location-icon review-location">
                <img src="{{$review['reviewed_object']['avatar']}}" alt="image">
            </div>
            <div class="review-txt">
                <a href="{{$review['reviewed_object']['link']}}" class="review-ttl review-place-link">{{$review['reviewed_object']['title']}}</a>
                <div class="sub-txt">
                    <div class="rate-label">
                        <b>{{$review['reviewed_object']['score']}}</b>
                        <i class="trav-star-icon"></i>
                    </div>&nbsp;
                    <span>@lang('profile.from_count_reviews', ['count' => $review['reviewed_object']['reviews_count']])</span>
                </div>
            </div>
        </div>
        <div class="btn-wrap">
            @if($review['reviewed_object']['is_followed'])
            <button class="btn btn-light-grey btn-bordered btn-grey-txt place-follow-btn"  data-id="{{$review['reviewed_object']['id']}}" data-type="unfollow">@lang('profile.unfollow')</button>
            @else
            <button class="btn btn-light-primary btn-bordered btn-grey-txt place-follow-btn"  data-id="{{$review['reviewed_object']['id']}}" data-type="follow">@lang('profile.follow')</button>
            @endif
        </div>
    </div>
    <div class="review-image-wrap">
        <ul class="review-image-list" reviewId="{{$review['id']}}">
            @if($review['media']['count'] > 0)
                @foreach($review['media']['list'] as $media)
                    @if($loop->index <2)
                        <li>
                            @if($media['type'] == 'video')
                            <div class="review-video-inner">
                                <video style="object-fit: cover; width:200px;height:200px;cursor:pointer" oncontextmenu="return false;" class="thumb review-media-wrap" reviewMediaId="{{$media->id}}">
                                    <source src="{{$media['thumbnail_url'] ?? $media['url']}}" type="video/mp4">
                                </video>
                            <a href="javascript:;" class="review-play-btn" data-id="{{$media['id']}}"  reviewMediaId="{{$media['id']}}"><i class="fa fa-play" aria-hidden="true"></i></a>
                            </div>
                            @else
                            <img src="{{$media['thumbnail_url'] ?? $media['url']}}" class="review-media-wrap" reviewMediaId="{{$media['id']}}" alt="" style="width:200px;height:200px;cursor:pointer">
                            @endif
                        </li>
                    @else
                       @if($loop->index == 2)
                       <li>
                           @if($media['type'] == 'video')
                            <div class="review-video-inner">
                                <video style="object-fit: cover; width:200px;height:200px;" oncontextmenu="return false;" class="thumb" reviewMediaId="{{$media['id']}}">
                                    <source src="{{$media['thumbnail_url'] ?? $media['url']}}" type="video/mp4">
                                </video>
                                <a href="javascript:;" class="review-play-btn" data-id="{{$media['id']}}"  reviewMediaId="{{$media['id']}}"><i class="fa fa-play" aria-hidden="true"></i></a>
                            </div>
                            @else
                                <img src="{{$media['thumbnail_url'] ?? $media['url']}}" alt="" style="width:200px;height:200px;">
                            @endif
                            @if($review['media']['count'] - 3 !=0)
                                <a class="cover-link review-media-wrap" href="javascript:;" reviewMediaId="{{$media['id']}}">
                                    <span><b>{{$review['media']['count'] - 3}}</b> @lang('profile.more_photos')</span>
                                </a>
                            @endif
                        </li>
                       @endif
                   @endif
                @endforeach
            @endif
        </ul>
    </div>
    <div class="review-foot">
        <div class="ava-wrap">
            <img src="{{$review['author']['profile_picture']}}" alt="" class="ava-image">
        </div>
        <div class="review-content profile-review-text">
            <p class="review-text-{{$review['id']}}">“<span>{{$review['text']}}</span>”</p>
            @if($login_user_id != '')
                <div class="reviewEditForm{{$review['id']}} review-edit-block d-none">
                    <div class="review-text-inner">
                        <textarea name="text" class="review-edit-textarea" maxlength="1000"  oninput="review_textarea_auto_height(this)"   placeholder="Write a review...">{{$review['text']}}</textarea>
                    </div>
                    <div class="review-edit-action-block">
                        <div class="review-edit-action-wrap">
                            <a href="javascript:;" class="review-edit-cancel-link" dataId="{{$review['id']}}">Cancel</a>
                            <a href="javascript:;"  class="review-edit-post-link" dataId="{{$review['id']}}">Post</a>
                        </div>
                    </div>
                </div>
             @endif
            <div class="comment-bottom-info">
                <div class="com-star-block">
                    <span class="rate-txt">{{$review['score']}}</span>
                    <ul class="com-star-list">
                        <li class="{{$review['score']<1?'empty':''}}"><i class="trav-star-icon"></i></li>
                        <li class="{{$review['score']<2?'empty':''}}"><i class="trav-star-icon"></i></li>
                        <li class="{{$review['score']<3?'empty':''}}"><i class="trav-star-icon"></i></li>
                        <li class="{{$review['score']<4?'empty':''}}"><i class="trav-star-icon"></i></li>
                        <li class="{{$review['score']<5?'empty':''}}"><i class="trav-star-icon"></i></li>
                    </ul>
                </div>
                <div class="com-time">{{$review['date']}}</div>
                @if($login_user_id != '')
                <div class="review-action-block">
                     <div class="dropdown ">
                        <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="trav-angle-down"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                            <a href="javascript:;" class="dropdown-item delete-review" id="" reviewId="{{$review['id']}}">
                                <span class="icon-wrap">
                                    <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;">
                                </span>
                                <div class="drop-txt comment-delete__drop-text">
                                    <p><b>Delete</b></p>
                                </div>
                            </a>
                            
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endforeach