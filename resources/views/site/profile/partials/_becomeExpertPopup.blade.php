<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<!-- Become an Expert popup -->
<div class="modal fade become-expert-popup" id="becomeExpertPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-740" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="top-layer">
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" dir="auto">
                        <i class="trav-close-icon" dir="auto"></i>
                    </button>
                    <h3>Become an Expert</h3>
                    <p>Enter more details about you, we will review your application as soon as possible.</p>
                </div>
                <form method="post" action="" id="become_expert_form">
                    <div class="hr"></div>
                    <div class="form-group description">
                        <h4>Social network links</h4>
                        <p>Add your website or social media links</p>
                        <span data-toggle="tooltip" title="It’s important to put at least one website or social media profile link where you share your current traveling activities to help us approve your Expert Account application." data-placement="bottom">Why?</span>
                    </div>
                    <div class="s-link-block">
                        <div class="form-group site default">
                            <img src="{{asset('assets2/image/sign_up/website.png')}}">
                            <span class="bm-link-protocol">https://</span>
                            <input type="text" class="form-control" id="b_website" name="website" placeholder="Put at lease one link...">
                        </div>
                        <label id="b_website-error" class="error" for="b_website"></label>
                    </div>
                    <div class="s-link-block">
                        <div class="form-group site">
                            <img src="{{asset('assets2/image/sign_up/twitter.png')}}">
                            <input type="text" class="form-control" id="b_twitter" name="twitter" placeholder="Insert your Twitter account URL here...">
                        </div>
                        <label id="b_twitter-error" class="error" for="b_twitter"></label>
                    </div>
                    <div class="error-messages"></div>
                    <div class="form-group description links">
                        Add more links:
                        <div>
                            <img data-name="tumblr" src="https://travooo.com/assets2/image/sign_up/tumblr.png">
                            <img data-name="pinterest" src="https://travooo.com/assets2/image/sign_up/pinterest.png">
                            <img data-name="instagram" src="https://travooo.com/assets2/image/sign_up/instagram.png">
                            <img data-name="facebook" src="https://travooo.com/assets2/image/sign_up/facebook.png">
                            <img data-name="youtube" src="https://travooo.com/assets2/image/sign_up/youtube.png">
                        </div>
                    </div>
                    <div class="hr"></div>
                    <div class="form-group description">
                        <h4>Areas of expertise</h4>
                        <p>Enter any countries, cities or places where you a solid experience.</p>
                    </div>
                    <div class="form-group areas">
                        <i class="trav-search-icon"></i>
                        <input type="text" class="form-control" id="expert_search" placeholder="Countries or cities...">
                    </div>
                    <div class="form-group description s-selected-title d-none">
                        <p>Selected</p>
                    </div>
                    <div class="form-group selected-areas">
                    </div>

                    <div class="form-group description s-result-title d-none">
                        <p>Search Results</p>
                    </div>
                    <div class="form-group suggested-areas">
                    </div>
                    <div class="form-group bottom">
                        <button type="button" class="btn btn-light-primary become-expert-btn">Become an Expert</button>
                    </div>
                </form>
            </div>  
        </div>
    </div>        
</div>
<!-- Become an Expert popup -->

<div class="modal fade review-expert-popup" id="underReviewPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-615" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="top-layer">
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon" dir="auto"></i>
                    </button>
                    <h3>Your Application Is Under Review</h3>
                    <p>You have already submitted your application, and it's under review. The Support Team will get back to you soon.</p>
                </div>
                <div class="under-review-dismiss-block">
                    <button type="button" class="btn btn-light-primary under-review-expert-btn" data-dismiss="modal" aria-label="Close">Dismiss</button>
                </div>
            </div>  
        </div>
    </div>        
</div>

<script>
var expertise_list = [];
var changeTimer = false;

$(document).on('blur', '#b_twitter, #b_tumblr, #b_pinterest, #b_instagram, #b_facebook, #b_youtube', function(){
    var media_value = $(this).val()
    var new_value = media_value.replace('https://', '');

    $(this).val(new_value)
})

$(document).on('blur', '#b_website', function(){
    var media_value = $(this).val()
    if(media_value != ''){
        var new_value = media_value.replace('https://', '');
        $(this).val(new_value)

        $('.bm-link-protocol').show()
        $(this).css('padding-left', '65px')
    }
})

$(document).on('keyup', '#b_website', function(){

    var input_val = $(this).val();

    if(input_val == ''){
        $('.bm-link-protocol').hide()
        $(this).css('padding-left', '13px')
    }
})

$(document).on('keyup', '#expert_search', function () {
    var expert_search = $(this).val()

    if (changeTimer !== false)
        clearTimeout(changeTimer);
    changeTimer = setTimeout(function () {
        preloadExpertise(expert_search);
        changeTimer = false;
    }, 300);

});


$(document).on('keyup', '.s-link-block .site input', function () {
  $('div#becomeExpertPopup div.error-messages').html('');
});

$(document).on("click", ".suggested-areas .suggested-areas-item", function () {
    var _this = $(this);
    var exp_id = _this.attr('data-id');
    var title = _this.find('span').text();
    var flag = _this.find('img').length > 0 ? '<img src="' + _this.find('img').attr('src') + '">' : '';

    if ($.inArray(exp_id, expertise_list) == -1) {
        expertise_list.push(exp_id);
        $('.selected-areas').append('<div class="selected-areas-item">' + flag + '<span>' + title + '</span><i class="trav-close-icon close-exp-filter" data-exp-value="' + exp_id + '"></i></div>')
        $(this).remove()
        $('.s-selected-title').removeClass('d-none');
        $('.s-selected-title').find('p').text(expertise_list.length + ' Selected');
        
        if($('.suggested-areas .suggested-areas-item').length == 0){
            $('.s-result-title').addClass('d-none')
            $('#expert_search').val('')
        }
    }
});


$(document).on('click', '.close-exp-filter', function () {
    var expert = $(this).attr('data-exp-value');
    expertise_list = $.grep(expertise_list, function (value) {
        return value != expert;
    });

    $('.s-selected-title').find('p').text(expertise_list.length + ' Selected');

    if (expertise_list.length == 0) {
        $('.s-selected-title').addClass('d-none');
    }

    $(this).closest('.selected-areas-item').remove()

});



$(document).on('click', '.become-expert-btn', function () {
    var social_links = {};
    var check_val = '';

    $('div#becomeExpertPopup div.form-group.site input').each(function (i, e) {
        var inputId = $(e).attr('name');
        var inputVal = $(e).val();

        if (inputVal != '') {
            check_val = inputVal;
            social_links[inputId] = 'https://' + inputVal;
        }

    });

    if (check_val == '') {
        $('div#becomeExpertPopup div.error-messages').html('Any one field is required.');
        return;
    };

    if (!$('#become_expert_form').validate().form()) {
        return;
    }

    $.ajax({
        type: "POST",
        url: "{{url('/profile/postBecomeExpert')}}",
        data: {social_links: social_links, expertise_list: expertise_list}
    }).done(function (result) {
        if (result.status === 'success' && result.data) {
            $('#becomeExpertPopup').modal('hide')
            $('.dropdown-item.switch').attr('data-target', '#underReviewPopup')
                                                    
            toastr.success('Your request send successfully!');
        }
    });
})


$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    showCrosses();
    $('#become_expert_form').validate();

    $('div#becomeExpertPopup').on('click', 'div.form-group.description.links img', function () {
        var name = $(this).data('name');
        var style = 'top: 9px; left: 9px;';
        style = '';

        if (name === 'youtube') {
            style = '';
        }

        $('div#becomeExpertPopup div.form-group.site:last').parent().after('<div class="s-link-block"><div class="form-group site">\n' +
                '                            <img style="' + style + '" src="/assets2/image/sign_up/' + name + '.png">\n' +
                '                            <input type="text" class="form-control" id="b_' + name + '" name="' + name + '" placeholder="Insert your ' + ucfirst(name) + ' account URL here...">\n' +
                '                        </div><label id="b_' + name + '-error" class="error" for="b_' + name + '"></label></div>');

        $(this).remove();

        if (!$('div#becomeExpertPopup div.form-group.description.links img').length) {
            $('div#becomeExpertPopup div.form-group.description.links').remove();
        }

        switch (name) {
            case 'website':
                $("#b_website").rules("add", {
                    website: true,
                })
                break;
            case 'twitter':
                $("#b_twitter").rules("add", {
                    twitter: true,
                })
                break;
            case 'tumblr':
                $("#b_tumblr").rules("add", {
                    tumblr: true,
                })
                break;
            case 'pinterest':
                $("#b_pinterest").rules("add", {
                    pinterest: true,
                })
                break;
            case 'instagram':
                $("#b_instagram").rules("add", {
                    instagram: true,
                })
                break;
            case 'facebook':
                $("#b_facebook").rules("add", {
                    facebook: true,
                })
                break;
            case 'youtube':
                $("#b_youtube").rules("add", {
                    youtube: true,
                })
                break;
        }

        showCrosses();
    });

    function showCrosses() {
        $('div#becomeExpertPopup div.form-group.site div.cross').remove();

        if ($('div#becomeExpertPopup div.form-group.site').length > 1) {
            $.each($('div#becomeExpertPopup div.form-group.site'), function() {

                if(!$(this).hasClass('default')){
                    $(this).append('<div class="cross"><i class="trav-close-icon"></i></div>');
                }
            });
        }
    }

    function ucfirst(str) {
        var firstLetter = str.slice(0, 1);
        return firstLetter.toUpperCase() + str.substring(1);
    }

    $('div#becomeExpertPopup').on('click', 'div.form-group.site div.cross', function () {
        var parent = $(this).parent('div.form-group.site').parent();

        $(parent).remove();
        returnIcon($(parent).find('input').attr('name'));

        showCrosses();
    });

    function returnIcon(name) {
        if (!$('div#becomeExpertPopup div.form-group.description.links').length) {
            $('div#becomeExpertPopup div.error-messages').before('' +
                    '<div class="form-group description links">' +
                    'Add more links:' +
                    '<div>' +
                    '</div>' +
                    '</div>')
        }

        var icon_url = '{{asset("assets2/image/sign_up/")}}/' + name + '.png';

        $('div#becomeExpertPopup div.form-group.description.links div').prepend('<img data-name="' + name + '" src="' + icon_url + '">');
    }



    $("#b_website").rules("add", {
        website: true,
    })

    $("#b_twitter").rules("add", {
        twitter: true,
    })


    $.validator.addMethod("facebook", function (value, element) {
        return this.optional(element) || /(?:https?:\/\/)?(?:www\.)?facebook\.com\/.(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-\.]*)/i.test(value);
    }, "Please enter a valid facebook URL."
            );

    $.validator.addMethod("twitter", function (value, element) {
        return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)twitter\.com\/(#!\/)?[a-z0-9_]+$/i.test(value);
    }, "Please enter a valid twitter URL."
            );

    $.validator.addMethod("instagram", function (value, element) {
        return this.optional(element) || /(https?:\/\/(?:www\.)?instagram\.com\/([^/?#&]+)).*/i.test(value);
    }, "Please enter a valid instagram URL."
            );

    $.validator.addMethod("website", function (value, element) {
        return this.optional(element) || /^(?:(?:http|https|ftp):\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
    }, "Please enter a valid URL."
            );

    $.validator.addMethod("youtube", function (value, element) {
        return this.optional(element) || /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/i.test(value);
    }, "Please enter a valid youtube URL."
            );

    $.validator.addMethod("tumblr", function (value, element) {
        return this.optional(element) || /[^"\/www\."](?<!w{3})[A-Za-z0-9]*(?=\.tumblr\.com)|(?<=\.tumblr\.com\/blog\/).*/i.test(value);
    }, "Please enter a valid tumblr URL."
            );

    $.validator.addMethod("pinterest", function (value, element) {
        return this.optional(element) || /(?:(?:http|https):\/\/)?(?:www\.)?(?:pinterest\.com|instagr\.am)\/([A-Za-z0-9-_\.]+)/i.test(value);
    }, "Please enter a valid pinterest URL."
            );

});

function preloadExpertise(search) {
    var url = "{{url('/profile/getExpertiseLocations')}}"
    $.ajax({
        type: "POST",
        url: url,
        data: {q: search}
    }).done(function (result) {
        if (result.status === 'success') {
            $('#becomeExpertPopup div.suggested-areas').html('');

            if (result.data.length > 0) {
                $('.s-result-title').removeClass('d-none')
                result.data.forEach(function (e, i) {
                    if (!expertise_list.includes(e.id)) {
                        var flag_content = e.flag != '' ? '<img src="' + e.flag + '" alt="">' : '';

                        $('#becomeExpertPopup div.suggested-areas').append('<div class="suggested-areas-item" data-id="' + e.id + '" >' + flag_content +
                                '<span>' + e.name + '</span>' +
                                '</div>');
                    }

                });
            } else {
                $('.s-result-title').addClass('d-none')
            }
        }
    });
}
</script>
