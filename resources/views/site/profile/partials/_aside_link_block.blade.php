<div class="post-block post-side-block links-show">
    <div class="post-side-top">
        <h3 class="h3-side-ttls">Links</h3>
        @if($login_user_id != '')
        <h3 class="side-ttl edit-links"><span class="edit">Edit</span></h3>
        @endif
    </div>
    <div class="side-expert-link-inner">
        <div class="expert-side-link">
            <ul class="profile-social-list">
                @if(@$user->website || @$user->facebook || @$user->twitter || @$user->instagram || @$user->youtube || @$user->pinterest || @$user->tumblr)
                @if(@$user->website)
                <li>
                    <a href="{{@$user->website}}" class="profile-website-link" style="display:inline" target="_blank">
                        <span class="round-icon">
                            <i class="trav-link"></i>
                        </span>
                        <span>Website</span>
                    </a>
                </li>
                <li>
                    <div class="seporator">|</div>
                </li>
                @endif
                @if(@$user->facebook)
                <li>
                    <a href="{{@$user->facebook}}" class="icon" target="_blank">
                        <img src="{{asset('assets2/image/sign_up/facebook.png')}}" width="25" height="25">
                    </a>
                </li>
                @endif
                @if(@$user->twitter)
                <li>
                    <a href="{{@$user->twitter}}" class="icon" target="_blank">
                        <img src="{{asset('assets2/image/sign_up/twitter.png')}}" width="25" height="25">
                    </a>
                </li>
                @endif
                @if(@$user->instagram)
                <li>
                    <a href="{{@$user->instagram}}" class="icon" target="_blank">
                        <img src="{{asset('assets2/image/sign_up/instagram.png')}}" width="25" height="25">
                    </a>
                </li>
                @endif
                @if(@$user->youtube)
                <li>
                    <a href="{{@$user->youtube}}" class="icon" target="_blank">
                        <img src="{{asset('assets2/image/sign_up/youtube.png')}}" width="25" height="25">
                    </a>
                </li>
                @endif
                @if(@$user->pinterest)
                <li>
                    <a href="{{@$user->pinterest}}" class="icon" target="_blank">
                        <img src="{{asset('assets2/image/sign_up/pinterest.png')}}" width="25" height="25">
                    </a>
                </li>
                @endif
                @if(@$user->tumblr)
                <li>
                    <a href="{{@$user->tumblr}}" class="icon" target="_blank">
                        <img src="{{asset('assets2/image/sign_up/tumblr.png')}}" width="25" height="25">
                    </a>
                </li>
                @endif
                @else
                <li>
                    <div>No links added...</div>
                </li>
                @endif
            </ul>
        </div>
    </div>
</div>


<div class="post-block post-side-block links-edit" style="display: none;">
    <div class="post-side-top">
        <h3 class="h3-side-ttls">Links</h3>
        <h3 class="side-ttl edit-links"><span class="save">Save</span></h3>
    </div>
    <div>
        <form id="social_form">
            <div class="post-side-top social-link-block">
                <h3 class="side-ttl" dir="auto">Website</h3>
                <div class="text-right position-relative">
                    <span class="tr-link-protocol">https://</span>
                    <input type="text" class="side flex-input social-input" name="website" id="website_url" placeholder="https://example.com"  dir="auto" value="{{str_replace('https://', '', $user->website)}}">
                </div>
            </div>
            <div class="post-side-top social-link-block">
                <h3 class="side-ttl" dir="auto">Facebook</h3>
                <div class="text-right">
                    <input type="text" class="side flex-input social-input" name="facebook" id="facebook_url" placeholder="https://facebook.com/username"  dir="auto" value="{{str_replace('https://', '', $user->facebook)}}">
                </div>
            </div>
            <div class="post-side-top social-link-block">
                <h3 class="side-ttl" dir="auto">Twitter</h3>
                <div class="text-right">
                    <input type="text" class="side flex-input social-input" name="twitter" id="twitter_url" placeholder="https://twitter.com/username"   dir="auto" value="{{str_replace('https://', '', $user->twitter)}}">
                </div>
            </div>
            <div class="post-side-top social-link-block">
                <h3 class="side-ttl" dir="auto">Instagram</h3>
                <div class="text-right">
                    <input type="text" class="side flex-input social-input" name="instagram" id="instagram_url" placeholder="https://instagram.com/anypage"  dir="auto" value="{{str_replace('https://', '', $user->instagram)}}">
                </div>
            </div>
            <div class="post-side-top social-link-block">
                <h3 class="side-ttl" dir="auto">Pinterest</h3>
                <div class="text-right">
                    <input type="text" class="side flex-input social-input" name="pinterest" id="pinterest_url" placeholder="https://pinterest.com/anypage"  dir="auto" value="{{str_replace('https://', '', $user->pinterest)}}">
                </div>
            </div>
            <div class="post-side-top social-link-block">
                <h3 class="side-ttl" dir="auto">Tumblr</h3>
                <div class="text-right">
                    <input type="text" class="side flex-input social-input" name="tumblr" id="tumblr_url" placeholder="https://tumblr.com/anypage"  dir="auto" value="{{str_replace('https://', '', $user->tumblr)}}">
                </div>
            </div>
            <div class="post-side-top social-link-block">
                <h3 class="side-ttl" dir="auto">Youtube</h3>
                <div class="text-right">
                    <input type="text" class="side flex-input social-input" name="youtube" id="youtube_url" placeholder="https://youtube.com/anypage"  dir="auto" value="{{str_replace('https://', '', $user->youtube)}}">
                </div>
            </div>
        </form>
    </div>
</div>
