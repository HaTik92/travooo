@extends('site.profile.template.profile')
@php $title = 'Travooo - Profile Photos'; @endphp
@section('content')

<style>
.thumbnail {
  position: relative;
  width: 200px;
  height: 200px;
  overflow: hidden;
}
.thumbnail img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: auto;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
.thumbnail img.portrait {
  width: 100%;
  height: auto;
}

.media-comment-footer a{
  text-decoration: none !important;
}

.lg-outer .lg-thumb-outer {
    width:auto !important;
}
</style>

<div class="container-fluid">

    @include('site.profile._profile_header')


    <div class="custom-row">
        <!-- MAIN-CONTENT -->
        <div class="main-content-layer">
            <div class="post-block post-top-bordered">
        <div class="post-side-top top-tabs arrow-style">
            <div class="post-top-txt">
                <h3 class="side-ttl current">@lang('profile.all_photos') <span class="count">{{count($lightbox_photos)}}</span>
                </h3>
            </div>
            <div class="side-right-control">
                <div class="sort-by-select">
                    <label>@lang('other.sort_by'):</label>
                    <div class="sort-select-wrap">
                        <select name="sorted_by" id="sources" class="sel-select sources" placeholder="Select Type">
                            <option value="date_down">Newest</option>
                            <option value="date_up">Oldest</option>
                            <option value="most_liked">Most Liked</option>
                            <option value="most_commented">Most Commented</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-media-inner">
            <div class="post-media-list">
                @if(count($photos) > 0)
                <div class="p-photo-block">
                    @php $index_num = 0; @endphp
                    @include('site.profile.partials._photos_block')
                </div>
                <div id="loadMorePhoto" class="load-more-photo">
                    <input type="hidden" id="photo_pagenum" value="1">
                    <div class="load-animation-block">
                        <div class="load-photo-animation-item animation"></div>
                        <div class="load-photo-animation-item animation"></div>
                        <div class="load-photo-animation-item animation" id="hide_photo_loader"></div>
                    </div>
                </div>
                @else
                    <div class="media-wrap media-not-found">
                        <div style="color:#808080;">No Photos Yet ...</div>
                   </div>
                @endif
            </div>

        </div>
    </div>

        </div>

        <!-- SIDEBAR -->
           <div class="sidebar-layer" id="sidebarLayer">
            <aside class="sidebar">
                @include('site.profile.partials._aside_posts_block')
                <div class="post-block post-side-block photo-side-block">
                    <div class="post-side-top">
                        <h3 class="side-ttl">Timeline</h3>
                        <div class="side-right-control">
                            <div class="sort-by-select">
                                <label>@lang('other.sort_by'):</label>
                                <div class="sort-select-wrap">
                                    <select name="sorted_by_timeline" id="sources" class="sel-select sources sorted-timeline" data-type="photo_timeline" placeholder="Select Type">
                                        <option value="day">Days</option>
                                        <option value="month">Months</option>
                                        <option value="year">Years</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="side-photo-inner aside-inner-scroller" id="timline_list">
                        <div class="side-media-loader">
                           <div class="side-media-loader-title animation"></div>
                           <div class="side-media-loader-block">
                               <div class="animation"></div>
                               <div class="animation"></div>
                               <div class="animation"></div>
                               <div class="animation"></div>
                               <div class="animation"></div>
                           </div>
                       </div>
                    </div>
                </div>
                 @include('site.profile.partials._aside_link_block')
                <div class="aside-footer">
                    <ul class="aside-foot-menu">
                        <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                        <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                        <li><a href="{{url('/')}}">Advertising</a></li>
                        <li><a href="{{url('/')}}">Cookies</a></li>
                        <li><a href="{{url('/')}}">More</a></li>
                    </ul>
                    <p class="copyright">Travooo &copy; 2017 - {{date('Y')}}</p>
                </div>
            </aside>
        </div>
    </div>
</div>



  @include('site.home.partials.modal_comments_like')
@endsection





@section('after_scripts')
 @include('site.profile.template._select_scripts')
 <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
 <script src="{{ asset('assets2/js/posts-script.js?v=0.3') }}"></script>
 <script src="{{ asset('/assets3/js/profile.js?ver='.time()) }}"></script>
 <script src="{{ asset('assets2/js/plugin/lg-thumbnail.js') }}"></script>
 @include('site.profile._photos_scripts')


@endsection
