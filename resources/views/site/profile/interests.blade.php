@extends('site.profile.template.profile')

@section('content')

@endsection

@section('sidebar')
    <div class="post-block post-side-profile sm-profile">
        <div class="image-wrap">
            <img src="http://placehold.it/385x125" alt="">
            <div class="post-image-info">
                <div class="avatar-layer">
                    <div class="ava-inner">
                        <img src="http://placehold.it/58x58" alt="" class="avatar">
                        <a href="#" class="edit-ava-link">
                            <img src="./assets2/image/profile-ava-edit-img.png" alt="">
                        </a>
                    </div>
                    <div class="ava-txt">
                        <h4 class="ava-name">Justin baker</h4>
                        <p class="sub-ttl">United States</p>
                    </div>
                </div>
                <div class="follow-btn-wrap">
                    <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side">
                        <i class="trav-user-plus-icon"></i>
                        <span>@lang('profile.follow')</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="post-profile-info">
            <ul class="profile-info-list">
                <li>
                    <p class="info-count">125</p>
                    <p class="info-label">@lang('profile.posts')</p>
                </li>
                <li>
                    <p class="info-count">58</p>
                    <p class="info-label">@lang('profile.followers')</p>
                </li>
                <li>
                    <p class="info-count">2</p>
                    <p class="info-label">@lang('profile.following')</p>
                </li>
            </ul>
        </div>
    </div>

    <div class="post-block post-side-search">
        <div class="search-wrapper">
            <input class="" id="" placeholder="@lang('profile.search_your_visited_places')" type="text">
            <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
        </div>
    </div>

    <div class="post-block post-side-type">
        <div class="type-label">@lang('profile.type')</div>
        <div class="type-progress-block">
            <div class="type-line">
                <div class="label">@lang('profile.places')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="75"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">4</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.cities')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">2</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.countries')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">1</div>
            </div>
            <div class="type-line">
                <div class="label">@lang('profile.events')</div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="count">1</div>
            </div>
        </div>
    </div>
@endsection

