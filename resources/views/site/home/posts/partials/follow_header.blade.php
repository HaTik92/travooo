@php
    switch ($type) {
        case 'post':
            $destinationDetails = getPostDestinationDetails($obj);
            break;
        case 'report':
            $destinationDetails = getReportsDestinationDetails($obj);
            break;
        case 'discussion':
            $destinationDetails = getDiscussionDestinationDetails($obj);
            break;
        case 'trip':
            $destinationDetails = getTripDestinationDetails($obj);
            break;
    }   
@endphp
@if (isset($destinationDetails) && is_array($destinationDetails))
    @php
        $destination_id = $destinationDetails['destination_id'];
        $destination_type = $destinationDetails['destination_type'];
        $destination_name= $destinationDetails['destination_name'];
    @endphp
    <div class="post-top-info-layer">
        <div class="post-info-line">
            <i class="icon-info"></i> You're seeing this because you follow 
            <a class="post-name-link" href="{{url_with_locale( $destination_type .'/'. $destination_id )}}">
                {{$destination_name}}
            </a>
        </div>
    </div>
@endif