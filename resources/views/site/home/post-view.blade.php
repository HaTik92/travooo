<!DOCTYPE html>
<html class="page" lang="en">
    <head>
        <title>{{@$place->trans[0]->title}} on Travooo</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">
        <meta name="format-detection" content="date=no">
        <meta name="format-detection" content="address=no">
        <meta name="format-detection" content="email=no">
        <meta content="notranslate" name="google">
         <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <!-- <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css"> -->
        <link rel="stylesheet" href="{{asset('assets2/css/place.css?v='.time()) }}">
        <link rel="stylesheet" href="{{asset('assets2/css/travooo.css?v='.time())}}">
        <link rel="stylesheet" href="{{asset('assets3/css/style-13102019.css?v=0.2')}}">
        <link rel="stylesheet" href="{{asset('assets3/css/datepicker-extended.css')}}">
        <!-- <link rel="stylesheet" type="text/css" href="{{asset('assets3/css/slider-pro.min.css')}}" media="screen"/> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link href="{{ asset('assets2/js/lightbox2/src/css/lightbox.css') }}" rel="stylesheet"/>
        <link href="{{asset('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>

        <link rel="stylesheet" href="{{asset('assets3/css/star.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/lightslider.min.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <style>
            .vertical-center {
                margin-top: 44px !important;
            }
            .modal__close {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                position: absolute;
                top: 20px;
                right: 20px;
                width: 26px;
                height: 26px;
                background-color: rgba(255, 255, 255, .5);
                color: rgba(0, 0, 0, .5);
                border-radius: 50%;
            }
	        .textarea-customize {
                border: 1px solid #dcdcdc;
                border-radius: 3px;
                resize: none;
            }
            .comment {
                margin-bottom: 10px;
            }
            .comment-info{
                display: flex;
                align-items: center;
            }
            .comment-info i{
                margin-right: 9px;
                color: #4080ff;
                font-size: 18px;
            }

            .carousel-cards__control--prev {
                display:flex !important;
                /*
                background: -webkit-gradient(linear,right top,left top,from(rgba(255,255,255,0)),color-stop(70%,#fff));
                background: linear-gradient(to left,rgba(255,255,255,0) 0%,#fff 70%);
                 */
                background: none !important;
            }



            .carousel-cards__item.slick-active > .carousel-cards__card {
                border-color: #4080ff;
                -webkit-box-shadow: 0 1px 6px rgba(0,0,0,.1);
                box-shadow: 0 1px 6px rgba(0,0,0,.1);
            }

            .icon--star {
                color:  #ccc;
            }

            .icon--star-active {
                color: #4080ff!important;
            }
            .user-card.post {
                position: relative;
                padding-bottom: 50px;
            }
            .user-card.post .dropdown {
                position: absolute;
                top: 8px;
                right: 12px;
            }
            .user-card.post .updownvote {
                position: absolute;
                bottom: 15px;
            }
            #lightbox {
                display: flex;
                flex-direction: column-reverse;
            }
            .post__content .event-item::after {
                position: initial;
            }
            .post__tip
            {
                padding-bottom: 15px;
                padding-top: 5px;
            }
            .post__tip-loadmore
            {
                padding: 0;
                border: 0;
                background-color: transparent;
                cursor: pointer;
            }

            .post__tip-upvotes,
            .post__tip-downvotes,
            .post__tip-upvotes:focus,
            .post__tip-downvotes:focus
            {
                outline:none;
            }

            .post__tip-downvotes {
                padding: 0;
                border: 0;
                background-color: transparent;
                cursor: pointer;
                margin-right: 24px;
            }

            .icon-vote {
                width:20px;
                height:20px;

                fill: #AAAAAA;
            }

            .icon-vote-active {
                fill: #4080ff;
            }

            .icon--angle-down-solid{

                transform: rotate(180deg);
            }



            .bs-tooltip-bottom {
                padding: .4em 0;
            }
            .tooltip .arrow {
                top: 0;
                position: absolute;
                display: block;
                width: .8em;
                height: .4em;
                opacity: 0.8;
            }
            .tooltip .tooltip-inner {
                text-align: center;
            }
            .tooltip .arrow::before {
                bottom: 0;
                border-width: 0 .4rem .4rem;
                border-top-color: #000;
                position: absolute;
                content: "";
                border-color: transparent;
                border-style: solid;
                border-bottom-color: #000;
                right: 0;
            }
            .location-identity.active {
                color: #4080ff;
            }
            .location-identity {
                outline: none;
                position: absolute;
                right: 4px;
                top: 11px;
                background: transparent;
                color: #ccc;
            }
            .fa-heart {
                color: #a3a3a3;
            }
            .fa-heart.red {
                color: red;
            }

            .travelmate-post-footer{
                border-left: 1px solid #e6e6e6;
                border-right: 1px solid #e6e6e6;
            }

            .travelmate-post-footer .sub-footer {
                padding: 15px;
                border-bottom: 1px solid #e6e6e6;
            }

            .travelmate-post-footer-info {

            }

            .travelmate-post-footer-info {
                font-size: 1rem;
                font-weight: normal;
            }

            .d-flex {
                display: flex!important;
            }

            .flex-wrap {
                flex-wrap: wrap!important;
            }

            .justify-content-between {
                -ms-flex-pack: justify!important;
                justify-content: space-between!important;
            }

            .w-50 {
                width: 50%!important;
            }
            .w-40 {
                width: 40%!important;
            }

            .mb-3 {
                margin-bottom: 1rem!important;
            }

            /* The popup bubble styling. */
            .popup-bubble {
                /* Position the bubble centred-above its parent. */
                position: absolute;
                top: 0;
                left: 0;
                transform: translate(-50%, -100%);
                /* Style the bubble. */
                background-color: white;
                padding: 5px;
                border-radius: 5px;
                font-family: sans-serif;
                overflow-y: auto;
                max-height: 60px;
                box-shadow: 0px 2px 10px 1px rgba(0,0,0,0.5);
            }
            /* The parent of the bubble. A zero-height div at the top of the tip. */
            .popup-bubble-anchor {
                /* Position the div a fixed distance above the tip. */
                position: absolute;
                width: 100%;
                bottom: /* TIP_HEIGHT= */ 8px;
                left: 0;
            }
            /* This element draws the tip. */
            .popup-bubble-anchor::after {
                content: "";
                position: absolute;
                top: 0;
                left: 0;
                /* Center the tip horizontally. */
                transform: translate(-50%, 0);
                /* The tip is a https://css-tricks.com/snippets/css/css-triangle/ */
                width: 0;
                height: 0;
                /* The tip is 8px high, and 12px wide. */
                border-left: 6px solid transparent;
                border-right: 6px solid transparent;
                border-top: /* TIP_HEIGHT= */ 8px solid white;
            }
            /* JavaScript will position this div at the bottom of the popup tip. */
            .popup-container {
                cursor: auto;
                height: 0;
                position: absolute;
                /* The max width of the info window. */
                width: 200px;
            }
             /* need assist css under task:https://trello.com/c/zIm45cSD/1513-places-notes-about-the-need-assistance-box-on-the-rhs-read-checklist */
             .contributor-title:hover {
                color:#4080ff !important;
            }
            .contributor-title{
                color:black;
            }
            .contributor-footer-img{
                width: 40px;
                margin-right: 5px;
            }
            .expert-info-img-div{
                margin-left: 50px;
                margin-top: 20px;
                margin-bottom: 10px;
            }
            .datepicker{
                z-index: 1650 !important;
            }        
            /* round checkbox for add to tripplance popover     */
            .popover{
                max-width: 300px;
                width: 100%;
                left: 50px !important;
            }
            .skyScannerHighlight{
                background-color:#c9c9c9;
                -webkit-transition: all 1s;
                -moz-transition: all 1s;
                -o-transition: all 1s;
                transition:all 1s;
            }
            .skyScannerHighlight{
                background-color:#f2eeee;
                -webkit-transition: all 1s;
                -moz-transition: all 1s;
                -o-transition: all 1s;
                transition:all 1s;
            }
            .skyScannerHighlightoff{
                background-color:#FFFFF;
                -webkit-transition: all 1s;
                -moz-transition: all 1s;
                -o-transition: all 1s;
                transition:all 1s;
            }
            .location-container{
                background: white;
                width: 100%;
                height: 45px;
                border: 1px solid #EDEDED;
                border-radius:3px;
            }
            .trip-plan-places-image
            {
                width: 45px;
                height: 45px;
            }
            .counter-btn{
                width:13px;border-radius:3px;cursor:pointer;color:#B6B6B6
            }
            .counter-input{
                border: none;
                width: 20px;
                margin: 0px;
                padding: 0px;
                padding-bottom: 0;
                padding-left: 0;
                margin-left: 10px;
            }
            .counter-div{
                background:white;height:45px; width:100%;border:1px solid #EDEDED;border-radius:3px;
            }
            .spent-input{
                height: 45px;
                border: 1px solid #EDEDED;
                border-radius: 3px;
                width: 96px;
                padding-left: 15px;
                float: left;
            }
            .ui-timepicker-wrapper{
                z-index: 3500 !important;
            }.btn-places{
                padding: 10px;
                width: 140px;
                height: 36px;
                border-radius: 3px;
                background: #F5F5F5;
                border: 1px solid #E9E9E9;
                margin-right: 12px;
                color: #818181 !important;
                font-family: inherit;
                cursor: pointer;
                text-decoration: none;
            }
            #ck-button {
                margin-left: 8px;
                background-color:#FCFCFC;
                border-radius:4px;
                border:1px solid #D0D0D0;
                overflow:auto;
                float:left;
                background-image: linear-gradient(0deg,rgba(0,0,0,.06) 0,transparent);
            }
            #ck-button:hover {
                cursor: pointer;
            }
            #ck-button label {
                float: left;
                width: 55px;
                height: 31px;
                padding-top: 10px;
                cursor: pointer;
            }
            #ck-button label span {
                text-align:center;
                padding:3px 0px;
                display:block;
            }
            #ck-button label input {
                position:absolute;
                top:-20px;
            }
            #ck-button input:checked + #ck-button {
                background-color:#3F7EFA;
                color:#fff;
            }
            .avatar{
                margin-left: -5px;
                position: relative;
                border: 1px solid #fff;
                border-radius: 50%;
                overflow: hidden;
                width: 50px;
                /* height: 50px; */
                width: 20px;
            }
            .btn-checkin-follow{
                height: 35px;
                float: right;
                margin-top: 11px;
                margin-right: 14px;
                width: 90px;
                font-family: inherit;
                margin-top: 0px;
            }.btn-unfollow{
                color: #000000;
                background: #e0e0e0;
                font-family: inherit;
            }
            .filter-separtor{
                width: 425px;
                border-bottom: 2px solid #dddd;
                display: inline-block;
                 margin-left: 7px;
                 margin-bottom: 2px;
            }
            .filter-btn{
                outline: none;
                color: #4483FF;
                height: 35px;
                width: 163px;
                font-family: inherit;
                border-radius: 5px;
                background-color: #E3ECFF;
                outline: none;
            }
        </style>
        <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> -->
        <script src="{{ asset('assets2/js/jquery-3.1.1.min.js') }}"></script>
        <script>
            var map_var = "";
            var map_func = "";
        </script>
    </head>

    <body class="page__body">

    <svg style="position: absolute; width: 0; height: 0; overflow: hidden" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="angle-circle-up-solid" viewBox="0 0 21 21">
                <path d="M10.5.5c5.523 0 10 4.476 10 9.999 0 5.523-4.477 10-10 10s-10-4.477-10-10 4.477-10 10-10zm1.325 7.937L10.539 7.06l-.04.041-.038-.04-1.287 1.377.032.034L6.1 11.758l1.286 1.378L10.5 9.841l3.112 3.295 1.287-1.378-3.105-3.288z"/>
            </symbol>
        </defs>
    </svg>
        @include('site.layouts._header')
        <div class="page-content page__content" style="width: 60%;align-self: center">
            @include('site.home.partials.post-text-popup',array('post' => $post, 'place' => $place,'checkins'=>@$checkins))
        </div>
    </div>
</div>

@include('site.place2.partials.modal_like')

<footer class="page-footer page__footer"></footer>

<script src="{{ asset('assets/js/lightslider.min.js') }}"></script>
<script src="{{ asset('assets3/js/tether.min.js') }}"></script>
<script src="{{ asset('assets2/js/popper.min.js') }}"></script>
<script src="{{ asset('assets2/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets3/js/bootstrap-datepicker-extended.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets2/js/slick.min.js') }}"></script>
<script src="{{ asset('assets3/js/main.js?0.3.2') }}"></script>
<script src="{{ asset('assets2/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/js/lightgallery-all.min.js"></script> -->
<script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
<script src="{{ asset('assets3/js/jquery.barrating.min.js') }}"></script>
<script src="{{ asset('assets3/js/star.js') }}"></script>

<script src="{{ asset('assets2/js/emoji/jquery.emojiFace.js?v=0.1') }}"></script>
<script src="{{ asset('assets2/js/skyloader.js') }}"></script>
<!-- <script src="{{ asset('assets3/js/jquery.sliderPro.min.js') }}"></script> -->
<script src="{{ asset('assets2/js/jquery-confirm/jquery-confirm.min.js') }}"></script>
<script src="{{ asset('assets2/js/lightbox2/src/js/lightbox.js') }}"></script>
<script data-cfasync="false" src="{{asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('assets2/js/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('assets2/js/posts-script.js?v=0.3') }}"></script>
@include('site.home.partials._spam_dialog')
</body>
@include('site.home.partials.modal_comments_like')
@include('site.layouts._footer-scripts')

</html>
