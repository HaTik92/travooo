@php
$title = 'Travooo - home page';
@endphp

@section('after_styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"
      rel="stylesheet"/>
<link href="{{ asset('assets2/js/lightbox2/src/css/lightbox.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets2/css/select2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets2/js/jquery.mentionsInput/jquery.mentionsInput.css?date=20/08/2019') }}" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<link href="{{ asset('assets2/js/summernote/summernote.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets2/css/sign-up.css')}}">
<link href="{{ asset('/plugins/summernote-master/tam-emoji/css/emoji.css') }}" rel="stylesheet"/>
<link rel="stylesheet" href="{{asset('assets3/css/style-13102019.css')}}">
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-j8y0ITrvFafF4EkV1mPW0BKm6dp3c+J9Fky22Man50Ofxo2wNe5pT1oZejDH9/Dt" crossorigin="anonymous">
<link rel="stylesheet" href="{{asset('assets3/css/datepicker-extended.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/lightslider.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js"></script>
<link
rel="stylesheet"
href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css"
type="text/css"
/> 
<link rel="stylesheet" href="{{ asset('/plugins/emoji/emojionearea_custom.css')}}">
<script data-cfasync="false" defer async src="{{ asset('/plugins/emoji/emojionearea.js?v='.time()) }}" type="text/javascript"></script>
<script data-cfasync="false" src="{{ asset('chat-functions.js') }}"></script>

<link href="{{ asset('/dist-parcel/discussions.css') }}" rel="stylesheet"/>
<link href="{{ asset('/dist-parcel/assets/Newsfeed/main.css') }}" rel="stylesheet"/>

<style type="text/css">
    #progress-wrp {
        border: 1px solid #0099CC;
        padding: 1px;
        position: relative;
        height: 30px;
        border-radius: 3px;
        margin: 10px;
        text-align: left;
        background: #fff;
        box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
    }

    #progress-wrp .progress-bar {
        height: 100%;
        border-radius: 3px;
        background-color: #f39ac7;
        width: 0;
        box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
    }

    #progress-wrp .status {
        top: 3px;
        left: 50%;
        position: absolute;
        display: inline-block;
        color: #000000;
    }

    #lightbox {
        display: flex;
        flex-direction: column-reverse;
    }
    .frm-url-container {
        border: #ced1d4 1px solid;
        border-radius: 4px;
        max-width: 550px;
        padding: 5px;
        position: relative;
    }

    .url-input {
        width: 100%;
        border: none;
        height: 25px;
        padding-left: 10px;
        font-family: Arial, Helvetica, sans-serif;
        padding-right: 30px;
        min-height: 50px;
    }

    .url-input:focus {
        outline: none;
    }

    .image-container {
        position: relative;
        z-index: 1;
    }

    .image-preview {
        width: 100%;
        margin-right: 100px;
    }

    .image-preview img {
        width: 100px;
        height: 100px;
    }

    #loader {
        position: absolute;
        right: 10px;
        margin-top: -12px;
        width: 16px;
        height: 24px;
        top: 50%;
        display: none;
    }

    #output {
        display: none;
    }

    .page-title {
        color: #000;
        text-decoration: none;
        font-size: 1.2em;
        line-height: 30px;
    }

    .text-data {
        margin-top: 20px;
    }

    .prev-next-navigation .prev-img {
        background: url(prev-img.png) no-repeat center center;
        display: inline-block;
        width: 16px;
        height: 16px;
        cursor: hand;
        cursor: pointer;
        border: #9a9b9a 1px solid;
        padding: 3px;
        opacity: 0.5;
    }

    .prev-next-navigation .next-img {
        background: url(next-img.png) no-repeat center center;
        display: inline-block;
        width: 16px;
        height: 16px;
        cursor: hand;
        cursor: pointer;
        border: #9a9b9a 1px solid;
        padding: 3px;
        opacity: 0.5;
    }

    a {
        color: #0254EB
    }
    a:visited {
        color: #0254EB
    }
    a.morelink {
        text-decoration:none;
        outline: none;
        font-size: 18px;
    }
    .morecontent span {
        display: none;
    }
    .comment {
        width: 400px;
        background-color: #f0f0f0;
        margin: 10px;
    }
    .thumb::before{
        content:"X";
        color: black;
        width: 1em;
        height: 1em;
        font-size: 10px;
    }
    .textarea-customize
    {
        border: 1px solid #dcdcdc;
        border-radius: 3px;
        resize: none;
    }

    .comment_showmore_btn
    {
        text-align: right;
        cursor: pointer;
        color: blue;
    }
    .spinner-border {
        display: inline-block;
        width: 2rem;
        height: 2rem;
        vertical-align: text-bottom;
        border: .25em solid currentColor;
        border-right-color: transparent;
        border-radius: 50%;
        -webkit-animation: spinner-border .75s linear infinite;
        animation: spinner-border .75s linear infinite;
    }
    @keyframes spinner-border {
        to { transform: rotate(360deg); }
    }

    .vid-quality-selector button.active {
        background-color: white;
        color: #222;
    }

    .vid-quality-selector button {
        background-color: rgba(34, 34, 34, 0.5);
        padding: 7px 10px;
        font-weight: 700;
        font-size: 11px;
        color: white;
        cursor: pointer;
    }

    .vid-quality-selector {
        position: absolute;
        top: 55%;
        right: 30px;
        z-index: 3;
        flex-direction: column;
        border-radius: 5px;
        overflow: hidden;
        transition: all 550ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
    }

    .flex {
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flex;
        display: -o-flex;
        display: flex;
    }
    .player{
        width: 595px;
        position: relative;
    }

    .note-popover {
        display: none;
    }

    .right-inner-addon {
        position: relative;

        width: 200px;
    }
    .right-inner-addon input {
        padding-right: 30px;
        width: 170px; /* 200px - 30px */
    }
    .right-inner-addon svg {
        position: absolute;
        right: -10px;
        padding: 10px 12px;
        pointer-events: none;

        top: -8px;
    }
    input.post-input {
        border: none;
        background-color: #f8f8f8;
    }
    a.post-author-name {
        color: black;
        text-decoration: none;
    }
    .post-media-options {
        right: 0;
        top: 6px;
        position: absolute;
    }
    .check-in-search {
        left: 0;
        top: 6px;
        position: absolute;
    }
    body .modal-backdrop.show {
        opacity: 0.8;
    }
    body .modal-dialog {
        width: 600px;
    }
    #add_post_permission_button {
        color: #b4b4b5;
        font-family: inherit;
    }
    body .modal-buttons {
        text-decoration: none!important;
        cursor: pointer;
        color: #b4b4b5;
        font-weight: 500;
    }
    body .modal-buttons:hover {
        color: #b4b4b5;
    }
    i.icon {
        display: inline-block;
        border-radius: 60px;
        background-color: #e6e6e6;
        padding: 0.25em 0.3em;

    }

    .footer-actions {
        color: #000;
        background-color: #fff;
    }
    .footer-actions svg * {
        fill: #000;
    }
    .footer-actions:hover {
        background-color: #e6e6e6;
    }
    .add-post__textarea {
        height: 100%;
        overflow: hidden;
    }
    button.disabled, button:disabled {
        cursor: not-allowed;
        opacity: .65;
    }
    .label-tag {
        color:red !important;
    }

    .report-span-input:not(#post-author-name) {
        padding-left: 27px!important;
    }

    .fa.fa-clock:after {
        content: "\f017";
    }


    #checkInModal .modal-dialog .modal-content, #taggingModal .modal-dialog .modal-content {
        height: 358px;
    }

    #taggingButton {
        outline: none;
        border-radius: 50%;
    }
    #modalTagging .post-create-input {
        display: flex;
    }
    #modalTagging .post-create-input .note-editor {
        width: 80%;
    }
    #taggingButton:hover, .blue-tagging-button {
        border-radius: 50%;
        border: 1px solid #c9dcff;
    }
    #taggingButton i:hover,.blue-tagging-button i {
        border-radius: 50%;
        color: #3e81ff;
    }
    #createNewPostPopup .note-editor {
        border: none;
    }
    /*#createNewPostPopup .note-toolbar.panel-heading*/
    #createNewPostPopup .note-statusbar {
        display: none;
    }
    #createNewPostPopup .note-editor.note-frame {
        /* position: relative; */
        position: initial;
    }
    #createNewPostPopup .note-toolbar {
        position: absolute;
        height: auto;
        bottom: 14px;
        right: 17px;
    }
    #createNewPostPopup .note-toolbar .note-insert {
        /* position: absolute;
        bottom: -60px;
        z-index: 4;
        right: 6px; */
        bottom: initial!important;
        margin-right: 15px;
    }
    #createNewPostPopup .note-toolbar .note-insert button div {
        display: block!important;
    }
    #createNewPostPopup .note-toolbar .note-insert button {
        background: transparent;
        border: none;
        height: 24px;
        /* width: 24px; */
        padding: 0;
        border-radius: 50%;
    }
    #createNewPostPopup .note-editable {
        width: 100%;
        height: auto;
        min-height: 160px;
        padding: 0;
        font-family: inherit;
        font-size: 22px;
        line-height: 32px;
        color: #1a1a1a;
        resize: none;
    }
    .post-create-input .img-wrap-newsfeed video::-webkit-media-controls-enclosure {
        display: none;
    }
    #checkinSearchResults {
        min-height: 300px;
    }
    .check-in-point {
        display: none;
    }

    button.create-new-post-form-trigger, button.trav-set-location-button {
        outline: none;
    }
    .btn-link, .btn-link:active, .btn-link:focus {
        background-color: transparent !important;
        border: none;
    }
    .note-editor.note-frame .custom-placeholder {
        display: block !important;
    }
    .note-toolbar.panel-heading {
        height: 0;
    }
    .taggedPlaces {
        display: inline-block;
    }
    .summernote-selected-items i {
        color: #b3b3b3;
        font-size: 16px;
    }
    .check-follow-place button {
        margin-bottom: 0 !important;
    }
    .shared-place-slider-card img {
        max-width: unset;
    }
    .country-flag {
        height: 13px !important;
        width: auto !important;
        border-radius: unset !important;
        border: none !important;
        margin-right: 5px !important;
    }
    .medias-slider{
        width:100% !important;
    }
        
    .icon-info {
        display: inline-block;
        width: 20px;
        height: 20px;
        background-color: #fff;
        border-radius: 100%;
        text-align: center;
        background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='20' height='20'%3E%3Cpath fill='%234080FF' fill-rule='evenodd' d='M10 19.999c-5.523 0-10-4.478-10-10C0 4.476 4.477-.002 10-.002S20 4.476 20 9.999c0 5.522-4.477 10-10 10zm1-15H9v1.999h2V4.999zm0 3H9v7h2v-7z'/%3E%3C/svg%3E%0A");
        background-repeat: no-repeat;
        background-size: cover;
    }
        
    .icon-trending {
        display: inline-block;
        width: 20px;
        height: 20px;
        background-color: #fff;
        border-radius: 100%;
        text-align: center;
        background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='20' height='20'%3E%3Cpath fill='%23FF8521' fill-rule='evenodd' d='M10 19.999c-5.523 0-10-4.477-10-10.001C0 4.477 4.477-.001 10-.001s10 4.478 10 9.999c0 5.524-4.477 10.001-10 10.001zm6-12v-2h-5v2h1.586L10.5 10.085 7.707 7.292l-.263.263-.225-.181-4 5.001 1.562 1.249 2.857-3.572 2.655 2.655.207-.207.207.207L14 9.413v1.586h2v-3z'/%3E%3C/svg%3E%0A");
        background-repeat: no-repeat;
        background-size: cover;
    }

    .event-item::after{
        content: none !important;
    }
</style>
@endsection

@extends('site.layouts.site')

@section('content-area')
@include('site.layouts._left-menu')

<div class="custom-row">
    <!-- MAIN-CONTENT -->
    <div class="main-content-layer">
        <div id="feed">
        <!--
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#photosPopup">
                      Launch photos popup
                    </button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#storyPopup">
                      Launch story popup
                    </button>
        -->

        @if(session()->has('alert-success'))
        <div class="alert alert-success" id="postSavedSuccess">
            {{ session()->get('alert-success') }}
        </div>
        @endif

        @if(session()->has('alert-danger'))
        <div class="alert alert-danger" id="postSavedDanger">
            {{ session()->get('alert-danger') }}
        </div>
        @endif

        @if(session()->has('alert-danger-server'))
        <div class="alert alert-danger" id="postSavedDanger">
            {{ session()->get('alert-danger-server') }}
        </div>
        @endif

        @if ($errors->has('file'))
        <div class="alert alert-danger" id="postSavedDanger">
            {{ $errors->first('file') }}
        </div>
        @endif
        @if ($errors->has('text'))
        <div class="alert alert-danger" id="postSavedDanger">
            {{ $errors->first('text') }}
        </div>
        @endif

        <div class="alert alert-danger" id="locationMsg" style="display:none;">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
        @if(Auth::check())
            <div class="post-block create-new-post-block">

                <div class="post-top-info-layer">
                    <div class="post-top-info-wrap">
                        <div class="post-top-avatar-wrap">
                            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                        </div>
                        <div class="post-top-info-txt">
                            <div class="post-top-name">
                                <a class="post-name-link" href="{{url('profile/'.Auth::user()->id)}}">{{Auth::user()->name}}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="create-new-post-form-trigger">
                    <span class="text">Write something...</span>
                    <span>
                        <i class="trav-camera"></i>
                        <i class="trav-set-location-icon location"></i>
                    </span>
                </div>

                <div class="modal white-style create-new-post-popup post--helper" data-backdrop="false" id="createNewPostPopup" tabindex="-1" role="dialog" aria-labelledby="createNewPostPopup" aria-hidden="true">
                    <!-- Mobile element -->
                    <button class="mobile-close-btn" type="button" data-dismiss="modal" aria-label="Close"><i class="fas fa-plus"></i></button>
                    <!-- Mobile element -->
                    <div class="modal-custom-style modal-615" role="document">
                        <form class="form-horizontal" id="drop_zone" method="POST" autocomplete="off" ondrop="dropHandler(event);" ondragover="dragStartHandler(event);">
                            {{ csrf_field() }}
                            <input type="hidden" id="pair" name="pair" value="<?php echo uniqid(); ?>"/>
                            <input type="hidden" name="post_actual_location" id="post_actual_location"/>
                            <input type="hidden" name="location" id="location"/>
                            <input type="hidden" name="permission" id="permission" value="0"/>
                            <input type="hidden" name="post_actual_date" id="post_actual_date"/>
                            <input type="hidden" name="checkin_date" id="checkin_date"/>
                            <div class="modal-custom-block">
                                <div class="post-block post-create-block" id="createPostBlock">
                                    <div class="post-top-info-layer">
                                        <div class="post-top-info-wrap">
                                            <div class="post-top-avatar-wrap">
                                                <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                                            </div>
                                            <div class="post-top-info-txt">
                                                <div class="post-top-name">
                                                    <a class="post-name-link" href="{{url('profile/'.Auth::user()->id)}}">{{Auth::user()->name}}</a>
                                                </div>
                                            </div>

                                            <div class="dropdown dropdown--privacy">
                                                <button id="add_post_permission_button" class="btn button--privacy dropdown-toggle" data-toggle="dropdown" data-id="{{\App\Models\Posts\Posts::POST_PERMISSION_PUBLIC}}">
                                                    <i class="fal fa-globe-asia"></i>
                                                    <span>PUBLIC</span>
                                                </button>
                                                <div class="dropdown-menu padding--helper dropdown-menu-right permissoin_show hided">
                                                    <li class="add_post_permission dropdown-item" data-id="{{\App\Models\Posts\Posts::POST_PERMISSION_PUBLIC}}">
                                                        <span class="icon-wrap">
                                                            <i class="fal fa-globe-asia"></i>
                                                        </span>
                                                        <div class="drop-txt">
                                                            <p><b>Public</b></p>
                                                            <p>Anyone can see this post</p>
                                                        </div>
                                                    </li>
                                                    <li class="add_post_permission dropdown-item" data-id="{{\App\Models\Posts\Posts::POST_PERMISSION_FRIENDS_ONLY}}">
                                                        <span class="icon-wrap">
                                                            <i class="fal fa-user-friends"></i>
                                                        </span>
                                                        <div class="drop-txt">
                                                            <p><b>Friends Only</b></p>
                                                            <p>Only you and your friends</p>
                                                        </div>
                                                    </li>
                                                    <li class="add_post_permission dropdown-item" data-id="{{\App\Models\Posts\Posts::POST_PERMISSION_PRIVATE}}">
                                                        <span class="icon-wrap">
                                                            <i class="fal fa-lock"></i>
                                                        </span>
                                                        <div class="drop-txt">
                                                            <p><b>Only Me</b></p>
                                                            <p>Only you can see this post</p>
                                                        </div>
                                                    </li>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="post-create-input">

                                        <div class="tagging-block">
                                            
                                        </div>
                                        <div class="emoji-block" style="position: relative;">
                                            <button class="add-msg__emoji" emoji-target="add_msg_text" type="button"></button>
                                        </div>
                                        <div class="note-codable">
                                            <input id="createPostTxt" rows="5" name="text" class="w-100 post-create"></input>
                                        </div>

                                        <div class="check-in-point">
                                            <i class="trav-set-location-icon"></i> <span class="locationText"></span>
                                            <i class="trav-clock-icon"></i> <span class="locationDate"></span> at <span class="locationTime"></span>
                                        </div>
                                        <div class="post-tags-list d-none" >
                                            <div>Tags:</div>
                                            <ul class="tagged-destinations-container">
                                               
                                            </ul>
                                        </div>
                                        <div class="outer__slide-wrap">
                                            <div class="gredient__wrap"></div>
                                            <div class="media-wrap__new-slider items">
    {{--                                        <div class="add-post__media"></div>--}}                                             
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-create-controls">
                                        <div class="post-alloptions">
                                            <ul class="create-link-list">
                                                <li class="post-options">
                                                    <input type="file" name="file[]" id="file" data-page="home" style="display:none" multiple="">
                                                    <i class="trav-camera click-target" data-target="file"></i>
                                                </li>
                                                <li class="post-options">
                                                    <button class="trav-set-location-button" data-dismiss="modal" data-toggle="modal" data-target="#createNewPostCheckinPopup">
                                                        <i class="trav-set-location-icon location" data-dismiss="modal" data-toggle="modal" data-target="#createNewPostCheckinPopup"></i>
                                                    </button>
                                                </li>

                                            </ul>

                                        </div>

                                        <button type="button" id="closePostBox" class="btn btn-link mr-3" aria-label="Close">Cancel</button>

                                        <div class="spinner-border" role="status" style="display:none"><span class="sr-only">Loading...</span></div>
                                        <button type="submit" class="btn btn-link primary btn-disabled" id="sendPostForm">POST </button>
                                    </div>
                                    <div id="output"></div>
                                    <div id="loader"></div>
                                    <div class="search-box tag-tooltip" style="display:none;position: absolute;top:80px;z-index: 1000;background-color: white;width: 400px;max-height: 300px;overflow: auto;-webkit-box-shadow: 0px 0px 26px 0px rgba(0,0,0,0.27);-moz-box-shadow: 0px 0px 26px 0px rgba(0,0,0,0.27);box-shadow: 0px 0px 26px 0px rgba(0,0,0,0.27);border-radius: 3px;"><div style="font-style: italic; padding: 5px 0 0 5px" class="results-for-container"><i>Results for</i>&nbsp;<b class="results-for-query">query</b></div> <ul class="nav nav-tabs search-tabs" role="tablist"> <li class="active nav-item" role="presentation"> <a class="nav-link active" href="#lc-places-999" aria-controls="lc-places-999" role="tab" data-toggle="tab">Places <span class="num-results-places"></span></a> </li> <li class="nav-item" role="presentation"> <a class="nav-link " href="#lc-people-999" aria-controls="lc-people-999" role="tab" data-toggle="tab">People <span class="num-results-people"></span></a> </li> </ul> <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane drop-wrap active" id="lc-places-999"></div>
                                            <div role="tabpanel" class="tab-pane drop-wrap" id="lc-people-999"></div>
                                        </div></div></div>

                            </div>
                        </form>
                    </div>
                </div>



                <div class="modal white-style create-new-post-checkin-popup" data-backdrop="false" id="createNewPostCheckinPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <!-- Mobile element -->
                    <button class="mobile-close-btn" type="button" data-dismiss="modal" aria-label="Close"><i class="fas fa-plus"></i></button>
                    <!-- Mobile element -->
                    <div class="modal-custom-style modal-615" role="document">
                        <div class="modal-custom-block">
                            <div class="post-block">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <button class="back" type="button">
                                            <i class="trav-angle-left"></i>
                                        </button>
                                        <h5 class="side-ttl">Check-in</h5>
                                    </div>
                                </div>
                                <div class="checkin-details-block">
                                    <i class="trav-search-icon"></i>
                                    <input type="text" id="checkInSearchField" placeholder="Check-in location..." data-selected_place="" autocomplete="off">
                                    <div class="check-date-wrap">
                                        <i class="trav-calendar-icon"></i>
                                        <input autocomplete="off" placeholder="" value="{{ now(getLocalTimeZone())->format('M d Y') }}" type="text" name="checkin_date" class="checkin_date" required="">
                                    </div>
                                    <span class="divider">|</span>
                                    <div class="check-time-wrap">
                                        <i class="trav-clock-icon"></i>
                                        {{-- todo: getLocalTimeZone() has API limits think would be better to replace with something else when we solved global timezone issue on the project --}}
                                        <input autocomplete="off" placeholder="" value="{{ roundToHalfHour(now(getLocalTimeZone())) }}" type="text" name="checkin_time" class="checkin_time ui-timepicker-input" required="">
                                    </div>
                                </div>
                                <div class="checkin-results-block" id="checkinSearchResults">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>







                <div class="modal white-style create-new-post-checkin-popup" data-backdrop="false" id="addPostTagsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <!-- Mobile element -->
                    <button class="mobile-close-btn" type="button" data-dismiss="modal" aria-label="Close"><i class="fas fa-plus"></i></button>
                    <!-- Mobile element -->
                    <div class="modal-custom-style modal-615" role="document">
                        <div class="modal-custom-block">
                            <div class="post-block">
                                <div class="post-top-info-layer">
                                    <div class="post-top-info-wrap">
                                        <button class="back" type="button">
                                            <i class="trav-angle-left"></i>
                                        </button>
                                        <h5 class="side-ttl">Tagging</h5>
                                    </div>
                                </div>
                                <div class="checkin-details-block">
                                    <i class="trav-search-icon"></i>
                                    <input type="text" placeholder="People, places, cities..." id="modalTaggingSearchInput" autocomplete="off">
                                </div>
                                <ul class="search-selected-block tagging-search-block">
{{--                                    <li>--}}
{{--                                        <img src="" alt="">--}}
{{--                                        <span class="search-selitem-title">Marrakesh</span>--}}
{{--                                        <span class="close-search-item close-search-interest-filter" data-interest-val="marrakesh">--}}
{{--                                            <i class="trav-close-icon"></i>--}}
{{--                                        </span>--}}
{{--                                    </li>--}}
                                </ul>
                                <ul class="nav tagging-results-filter" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="tags-all-results-tab" data-toggle="pill" href="#tags-all-results" role="tab" aria-controls="pills-all" aria-selected="true">All Results</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tags-people-results-tab" data-toggle="pill" href="#tags-people-results" role="tab" aria-controls="pills-people" aria-selected="false">People <i>0</i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tags-places-results-tab" data-toggle="pill" href="#tags-places-results" role="tab" aria-controls="pills-places" aria-selected="false">Places <i>0</i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tags-cities-results-tab" data-toggle="pill" href="#tags-cities-results" role="tab" aria-controls="pills-cities" aria-selected="false">Cities <i>0</i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tags-countries-results-tab" data-toggle="pill" href="#tags-countries-results" role="tab" aria-controls="pills-countries" aria-selected="false">Countries <i>0</i></a>
                                    </li>
                                </ul>
                                <div class="tab-content" style="min-height: 70px;">
                                    <div class="tab-pane show active" id="tags-all-results" role="tabpanel" aria-labelledby="tags-all-results-tab">

                                        <div class="checkin-results-block">
                                        </div>

                                    </div>
                                    <div class="tab-pane" id="tags-people-results" role="tabpanel" aria-labelledby="tags-people-results-tab">

                                        <div class="checkin-results-block">
                                        </div>

                                    </div>
                                    <div class="tab-pane" id="tags-places-results" role="tabpanel" aria-labelledby="tags-places-results-tab">

                                        <div class="checkin-results-block">
                                        </div>

                                    </div>
                                    <div class="tab-pane" id="tags-cities-results" role="tabpanel" aria-labelledby="tags-cities-results-tab">

                                        <div class="checkin-results-block">
                                        </div>

                                    </div>
                                    <div class="tab-pane" id="tags-countries-results" role="tabpanel" aria-labelledby="tags-countries-results-tab">

                                        <div class="checkin-results-block">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal white-style create-new-place-popup" data-backdrop="false" id="createNewPlace" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-custom-style modal-800" role="document">
                        <div class="modal-custom-block">
                            <div class="new-place-block">
                                <h2>Add a missing place</h2>
                                <hr>
                                <p>Please provide some details about this place bellow, This place will be publicly visible.</p>
                                <form>
                                    {{ csrf_field() }}
                                    <div class="row mb-4">
                                        <div class="form-group col-md-6">
                                            <label for="">Place Name <span>*</span></label>
                                            <input type="text" class="form-control custom-input" required id="place_name" placeholder="What this place called">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="place_category">Category <span>*</span></label>
                                            <select name="" id="place_category" required class="form-control custom-input" style="height: unset">
                                                <option value="" disabled selected>Selecet the place Category</option>
                                                <option value="1">Point of Interest</option>
                                                <option value="2">Restaurant</option>
                                                <option value="3">Hotel</option>
                                            </select>
                                            {{-- <input type="text" class="form-control custom-input" id="place_category" placeholder="Selecet the place Category"> --}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="">Adress <span>*</span></label>
                                            <input type="text" class="form-control custom-input" required id="place_address" placeholder="Enter the place address">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="">City <span>*</span></label>
                                            <input type="text" class="form-control custom-input" required id="place_city" placeholder="Where this place is located">
                                        </div>
                                    </div>
                                    
                                    <div class="map-section">
                                        <p>Location on Map <span>*</span></p>
                                        <div id="place-map-check"></div>
                                    </div>

                                    <div class="d-flex justify-content-between align-items-center mt-2">
                                        <div>We will review your submission and email you as soon as possible.</div>
                                        <div>
                                            <button type="button" id="create-submission" class="btn btn-primary text-light" style="color: #fff">SEND</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="close-icon">
                                    <img src="{{asset('assets2/image/close-icon.svg')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                

            </div>
        @endif
        <script>
            $('body').on('submit', '.form-horizontal', function(e){
                var form = $(this);
                var inputEmptyDiv =  $('#createPostBlock').find('.note-editable div');
                $.each(inputEmptyDiv, function (i, element) {
                    if ($(element).is(':empty')) {
                        $(element).remove();
                    }
                });
                $('#createPostTxt').val($('#createPostBlock').find('.note-editable').html());
                var values = $(this).serialize();
                $.ajax({
                    method: "POST",
                    url: "{{url("new-post") }}",
                    data: values
                })
                    .done(function (res) {
                        if(res == "error")
                            alert("Post can't be send.");
                        else if(res == "api-error")
                            alert("Your post is deleted because of harmful content");
                        else
                            form.closest('.post-block').after(res);
                        $('.modal').modal('hide');
                        $('.form-horizontal').find('.medias').find('.removeFile').each(function(){
                            $(this).click();
                        });
                        $('body').removeClass('postWritted');
                        $('#sendPostForm').attr("disabled", true);

                        $('[data-tab]').on('click', function (e) {
                            var Item = $(this).attr('data-tab');
                            $('[data-content]').css('display', 'none');
                            $('[data-content=' + Item + ']').css('display', 'block');
                            e.preventDefault();
                            Comment_Show.init($(this).closest('.post-block'));
                        });

                        $('#checkinSearchResults').html('');
                        $('#addPostTagsModal .tagging-search-block').html('');
                        $('#modalTaggingSearchInput').val('');
                        $('#createPostTxt').val('');
                        $('#createPostTxt').next('.note-editor').find('.note-editable').html('');
                        $('#createPostTxt').next('.note-editor').find('.note-editing-area').find(".note-placeholder").addClass('custom-placeholder');
                        $('#createPostBlock').find('.check-in-point').hide();
                        $('#createPostBlock .note-toolbar .note-insert').css('bottom', '-60px');
                        $('#createPostBlock').find('.medias.medias-slider').html('');
                        $('#checkInSearchField').find('div:not(#placeholder)').html('');
                        $('#checkInSearchField').find('#placeholder').css('display', 'block');
                        $('#checkInSearchField').val('');
                        $('#permission').val('');
                        $('#post_actual_date').val('');
                        $('#location').val('');
                        $('#checkin_date').val('');
                        $('#post_actual_location').val('');
                        $('.check-in-point').css('display','none');
                        $('.add_post_permission[data-id="0"]').click();
                        $('#pair').val('<?php echo uniqid(); ?>');

                        $('.tagged-destinations-container').empty();
                        $('.post-tags-list').addClass('d-none');

                    });
                e.preventDefault();
            });

            $('.create-new-post-popup').on('hidden.bs.modal', function () {
                $.ajax({
                    method: "DELETE",
                    url: baseUrl + "/delete-uploaded-temp-files",
                    data: {'pair': $(this).find('#pair').val()}
                }).done(function (response) {
                    if (response.success === true) {
                        $('#pair').val('<?php echo uniqid(); ?>');
                        $('.media-wrap__new-slider').html('');
                    }
                });
            });
        </script>
        <!-- <div id="progress-wrp">
            <div class="progress-bar"></div>
            <div class="status">0%</div>
        </div> -->
        @php
        if(Auth::check()){
            $friends = auth()->user()->findFriends()->pluck('id')->toArray();
            $followers = auth()->user()->followings()->pluck('users_id')->toArray();
            $friends_followers = array_merge($friends, $followers);
        }else{
            $friends_followers = [];
        }
           $rand_pos = rand(1,3);
        @endphp
        @if(count($posts) == 0)
            @if(isset($secondary_post) && $secondary_post != '')
                {!!$secondary_post!!}
            @endif
            @if(isset($event_activity) && $event_activity !='')
                @include('site.home.partials.post_event', array('post' => $event_activity, 'me' => Auth::guard('user')->user()))
             @endif
        @endif
        @foreach($posts AS $key =>$post)
        @if (isset($post->holidays_id))
            @include('site.home.new.partials.holidays')
        @endif
        @if (isset($post->permission) AND $post->permission == \App\Models\Posts\Posts::POST_PERMISSION_PRIVATE AND $post->users_id != Auth::user()->id)
            @continue;
        @elseif (isset($post->permission) AND $post->permission == \App\Models\Posts\Posts::POST_PERMISSION_FRIENDS_ONLY AND $post->users_id != Auth::user()->id AND !in_array( $post->users_id, getUserFriendsIds())) {
            @continue;
        @endif
        @if($old_content_flag && $key == 0)
                @if(isset($secondary_post) && $secondary_post != '')
                {!!$secondary_post!!}
            @endif
            @if(isset($event_activity) && $event_activity !='')
                @include('site.home.partials.post_event', array('post' => $event_activity, 'me' => Auth::guard('user')->user()))
            @endif
        @elseif($key == $rand_pos && !$old_content_flag)
            @if(isset($secondary_post) && $secondary_post != '')
                {!!$secondary_post!!}
            @endif
            @if(isset($event_activity) && $event_activity !='')
                @include('site.home.partials.post_event', array('post' => $event_activity, 'me' => Auth::guard('user')->user()))
            @endif
        @endif
        @if($post->type=="post" AND $post->action=="like")
        @include('site.home.partials.post_like')
       
            @elseif($post->type=="event" AND $post->action=="show" AND !user_access_denied($post->user_id, true))
            @include('site.home.partials.post_event')

            @elseif($post->type=="other" AND $post->action=="show" AND !user_access_denied($post->user_id, true))
            @include('site.home.partials.other_post')

            @elseif($post->type=="travelmate" AND $post->action=="join")
            @include('site.home.partials.travelmate_join')

        @elseif($post->type=="travelmate" AND $post->action=="request")
        @include('site.home.partials.travelmate_request')

        {{-- @elseif($post->type=="TripPlan" AND $post->action=="publish" AND !user_access_denied($post->user_id, true))
        @include('site.home.partials.trip_create') --}}

        @elseif($post->type=="user" AND $post->action=="follow" AND !user_access_denied($post->user_id, true))
        @include('site.home.partials.user_follow')

        @elseif($post->type=="user" AND $post->action=="unfollow")

        @elseif($post->type=="country" AND $post->action=="follow" AND !user_access_denied($post->user_id, true))
        @include('site.home.partials.country_follow')

        @elseif($post->type=="country" AND $post->action=="unfollow")

        @elseif($post->type=="city" AND $post->action=="follow" AND !user_access_denied($post->user_id, true))
        @include('site.home.partials.city_follow')

        @elseif($post->type=="city" AND $post->action=="unfollow")

        @elseif($post->type=="place" AND $post->action=="follow" AND !user_access_denied($post->user_id, true))
        @include('site.home.partials.place_follow')

        @elseif($post->type=="place" AND $post->action=="review" AND !user_access_denied($post->user_id, true))
        {{-- @include('site.home.partials.place_review') --}}
        @include('site.home.new.partials.place-review')

        @elseif($post->type=="place" AND $post->action=="unfollow")


        @elseif($post->type=="hotel" AND $post->action=="follow")
        @include('site.home.partials.hotel_follow')

        @elseif($post->type=="hotel" AND $post->action=="review")
        @include('site.home.partials.hotel_review')

        @elseif($post->type=="hotel" AND $post->action=="unfollow")

        {{-- @elseif($post->type=="Status" AND $post->action=="comment")
        @include('site.home.partials.status_comment') --}}

        @elseif($post->type=="status" AND $post->action=="publish")
        @include('site.home.partials.status_publish')

        @elseif($post->type=="status" AND $post->action=="like")
        @include('site.home.partials.status_like')

        @elseif($post->type=="status" AND $post->action=="unlike")

        @elseif($post->type=="media" AND $post->action=="upload")

        @include('site.home.new.partials.primary-media-post')
        {{-- @include('site.home.partials.media_upload') --}}

        {{-- @elseif($post->type=="media" AND $post->action=="comment")
        @include('site.home.partials.media_comment')

        @elseif(($post->type=="Medias" OR $post->type=="Media") AND $post->action=="comment")
        @include('site.home.partials.media_comment') --}}

        @elseif($post->type=="media" AND $post->action=="like")
        @include('site.home.partials.media_like')

        @elseif($post->type=="media" AND $post->action=="unlike")
        @include('site.home.partials.media_unlike')

        @elseif($post->type=="checkin" AND
        ($post->action=="do" || ($post->action=="update" && in_array($post->users_id, $friends_followers))))
        @include('site.home.partials.checkin_do')

        @elseif($post->type=="discussion" AND $post->action=="reply" AND !user_access_denied($post->user_id, true))
        @include('site.home.partials.discussion_reply')

        @elseif($post->type=="post" AND $post->action=="comment")
        @include('site.home.partials.post_comment')

        @elseif($post->type=="users" AND $post->action=="registrate")
        @elseif(strtolower($post->type)=="post" AND strtolower($post->action)=="publish" AND !user_access_denied($post->user_id, true))
        {{-- new post type / reworked --}}
                            {{-- when we create a media file and write text, only the text is posted --}}
       @include('site.home.new.partials.primary-text-only-post')
{{--       @include('site.home.partials.post_regular_old')--}}
        {{-- @include('site.home.partials.post_regular') --}}
        @elseif($post->type=="trip" AND $post->action=="invite_friends")
        @elseif($post->type=="trip" AND strtolower($post->action)=="activate")
        @include('site.home.partials.trip_activate')

        @elseif($post->type=="trip" AND $post->action=="create")
        {{-- @include('site.home.new.partials.trip') --}}
        @elseif($post->type=="trip" AND $post->action=='plan_updated')
        @include('site.home.new.partials.trip')
        @elseif($post->type=="trip" AND $post->action=="accept_invitation")
        @elseif($post->type=="trip" AND $post->action=="reject_invitation")
        @elseif($post->type=="trip" AND $post->action=="delete")
        @elseif($post->type=="post" AND $post->action=="like")
        @elseif($post->type=="post" AND $post->action=="unlike")
        @elseif($post->type=="status" AND $post->action=="comment")
        @elseif($post->type=="media" AND $post->action=="like")
        @elseif($post->type=="media" AND $post->action=="unlike")
        @elseif($post->type=="discussion" AND $post->action=="upvote")
        @elseif($post->type=="discussion" AND $post->action=="downvote")
        @elseif($post->type=="discussion" AND $post->action=="create" AND !user_access_denied($post->user_id, true))
        {{-- @include('site.home.partials.discussion_create') --}}
        @include('site.home.new.partials.discussion')

        @elseif($post->type=="report" AND ($post->action=="create" || $post->action=="update") AND !user_access_denied($post->user_id, true))
        {{-- @include('site.home.partials.report_create') --}}
        @include('site.home.new.partials.travlog')
        @elseif($post->type=="report" AND $post->action=="share")
        @include('site.home.partials.report_share')

        @elseif($post->type=="report" AND $post->action=="comment")

        @elseif($post->type =='trips_medias_shares')
            @include('site.home.new.partials.tripmediashare')
        @elseif($post->type =='trips_places_shares')
            @include('site.home.new.partials.tripplace')
        @elseif($post->type =='trip_places_comments' AND !user_access_denied($post->user_id, true))
            @include('site.home.new.partials.tripplacecomment')
        @elseif($post->type == 'share' AND !user_access_denied($post->user_id, true))
            @include('site.home.new.partials.shareable-post')
        @endif

        @endforeach
        {{--<!-- @include('site.home.partials.trending_destinations') -->
        <!-- @include('site.home.partials.upcoming_events') -->
        <!-- @include('site.home.partials.places_you_might_like') -->
        <!-- @include('site.home.partials.discover_new_people') -->
        <!-- @include('site.home.partials.upcoming_holidays') -->
        <!-- @include('site.home.partials.videos_you_might_like') -->--}}
        {{-- @include('site.home.partials.collective_followers_places')
        @include('site.home.partials.recommended_countries_cities')
        @include('site.home.partials.recommended_places') --}}
        {{-- @include('site.home.partials.trending_destinations') --}}
        {{-- @include('site.home.partials.videos_you_might_like')
        @include('site.home.partials.trending_events')
        @include('site.home.partials.people_you_might_know')
        @include('site.home.partials.discover_new_travellers')
        @include('site.home.partials.discover_new_destinations') --}}
        {{-- @include('site.home.new.partials.today-trip') --}}
        {{-- @include('site.home.new.partials.trip-start') --}}
      


        @include('site.home.partials.modal_comments_like')

        </div>
        <div id='pagination' style='text-align:center;'>
            <input type="hidden" id="pageno" value="1">
            <div id="feedloader" class="post-animation-content post-block">
                <div class="post-top-info-layer">
                    <div class="post-top-info-wrap">
                        <div class="post-top-avatar-wrap animation post-animation-avatar"></div>
                        <div class="post-animation-info-txt">
                            <div class="post-top-name animation post-animation-top-name"></div>
                            <div class="post-info animation post-animation-top-info"></div>
                        </div>
                    </div>
                </div>
                <div class="post-animation-conteiner">
                    <div class="post-animation-text animation pw-2"></div>
                    <div class="post-animation-text animation pw-3"></div>
                </div>
                <div class="post-animation-footer">
                    <div class="post-animation-text animation pw-4"></div>
                </div>
            </div>
        </div>

    </div>

    <!-- SIDEBAR -->
    <div class="sidebar-layer" id="sidebarLayer">
        <aside class="sidebar">

            <div class="post-block post-side-block">
                <div class="post-side-top">
                    <h3 class="side-ttl">@lang('home.top_places')</h3>
                    <div class="side-right-control">
                        <a href="#" class="see-more-link">@lang('buttons.general.see_more')</a>
                    </div>
                </div>
                <div class="post-side-inner place-animation-side-block" id="placeAsideAnimation">
                    <div class="side-place-block place-animation-side-block">
                        <div class="place-animation-title">
                            <div class="place-top-info-layer">
                                <div class="place-top-info-wrap">
                                    <div class="place-top-avatar-wrap animation post-animation-avatar"></div>
                                    <div class="place-animation-info-txt">
                                        <div class="place-top-name animation"></div>
                                        <div class="place-info animation"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="place-animation-content">
                            <div class="animation"></div><div class="animation"></div><div class="animation"></div><div class="animation"></div>
                        </div>
                        <div class="place-animation-footer">
                            <div class="place-animation-count animation"></div>
                        </div>
                    </div>
                    <div class="side-place-block place-animation-side-block">
                        <div class="place-animation-title">
                            <div class="place-top-info-layer">
                                <div class="place-top-info-wrap">
                                    <div class="place-top-avatar-wrap animation post-animation-avatar"></div>
                                    <div class="place-animation-info-txt">
                                        <div class="place-top-name animation"></div>
                                        <div class="place-info animation"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="place-animation-content">
                            <div class="animation"></div><div class="animation"></div><div class="animation"></div><div class="animation"></div>
                        </div>
                        <div class="place-animation-footer">
                            <div class="place-animation-count animation"></div>
                        </div>
                    </div>
                </div>
                <div class="post-side-inner d-none" id="placeAsideBlock">
                    @foreach($top_places as $tplace)
                    <div class="side-place-block">
                        <div class="side-place-top">
                            <div class="side-place-avatar-wrap">

                                <img class="lazy" data-src="https://dug67gld7k1m8.cloudfront.net/180x0/{{@$tplace->place->medias[0]->url}}"
                                     alt="" style="width:46px;height:46px;">
                            </div>
                            <div class="side-place-txt">
                                <a class="side-place-name"
                                   href="{{ route('place.index', @$tplace->place->id) }}">{{@$tplace->place->trans[0]->title}}</a>
                                <div class="side-place-description">
                                    <b>{{@do_placetype($tplace->place->place_type)}}</b> in <a
                                        href="{{ route('city.index', @$tplace->place->city->id) }}">{{@$tplace->place->city->trans[0]->title}}</a>,
                                    <a href="{{ route('country.index', @$tplace->place->country->id)}}">{{@$tplace->place->country->trans[0]->title}}</a>
                                </div>
                            </div>
                        </div>
                        <ul class="side-place-image-list">
                            @if(isset($tplace->place))
                            @foreach($tplace->place->medias AS $pm)
                            @if(!$loop->first AND $loop->iteration<6)
                            <li>
                                <img class="lazy" data-src="https://dug67gld7k1m8.cloudfront.net/180x0/{{@$pm->url}}"
                                     alt="photo" style="width:79px;height:75px;"></li>
                            @endif
                            @endforeach
                            @endif
                        </ul>
                        <div class="side-place-bottom">
                            <div class="side-follow-info">
                                <b>{{ @count($tplace->place->followers) }}</b> @lang('home.following_this_place')
                            </div>
                            @if(Auth::check())
                                <?php
                                    $placefollow = \App\Models\Place\PlaceFollowers::where('users_id', $me->id)->where('places_id', $tplace->places_id)->get();
                                ?>
                                @if(count($placefollow) > 0)
                                <button type="button" class="btn btn-light-grey btn-bordered" onclick="newsfeed_place_following('unfollow', {{$tplace->places_id}}, this)">Unfollow
                                </button>
                                @else
                                <button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following('follow', {{$tplace->places_id}}, this)">@lang('home.follow')
                                </button>
                                @endif
                            @else
                                <button type="button" class="btn btn-light-primary btn-bordered open-login">@lang('home.follow')</button>
                            @endif
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
            
            @if(Auth::check())
            <div class="post-block post-side-tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#global"
                           role="tab">@lang('home.global')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#friends" role="tab">@lang('home.friends')</a>
                    </li>
                </ul>
         
                <div class="tab-content">
                    <div class="tab-pane active" id="global" role="tabpanel">
                        <div class="side-tab-inner mCustomScrollbar" style="overflow:hidden">
                            @foreach($activity_logs AS $activity_log)

                            @if(($activity_log->type!="Discussion" && $activity_log->action!="view" && $activity_log->action!="session_start" && $activity_log->type!="Event"))

                            <div class="side-pane-row">
                                <div class="side-pane-avatar-wrap">
                                    <img class="lazy" data-src="{{check_profile_picture($activity_log->user->profile_picture)}}"
                                         alt="">
                                </div>
                                <div class="side-pane-txt">
                                    <div class="side-pane-post-ttl">
                                         <a class="in-side-link"
                                            href="{{url('profile/'.$activity_log->user->id)}}">{{$activity_log->user->name}}</a>
                                         {!! get_exp_icon($activity_log->user) !!}
                                         {!!activityAction($activity_log->type, $activity_log->action, $activity_log->variable)!!}
                                    </div>
                                    <!-- SHOULD THINK OF BETTER WAY TO DO THAT -->
                                     <div class="side-pane-date date-format">{{diffForHumans($activity_log->time)}}</div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-pane" id="friends" role="tabpanel">@lang('home.friends_tab')</div>
                </div>
            </div>
            @endif

            <!--

            <div class="post-block post-side-block">
                <div class="post-side-top">
                    <h3 class="side-ttl">@lang('home.stories')</h3>
                    <div class="side-right-control">
                        <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                        <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
                    </div>
                </div>
                <div class="post-side-inner">
                    <div class="post-slide-wrap">
                        <ul id="storySlider" class="post-slider">
                            <li>
                                <img src="assets2/image/photos/frommoroccotojapan/maxresdefault (1).jpg"
                                     alt="">
                                <div class="post-slider-caption">
                                    <a class="post-slide-name" href="#">4 Days from Morocco to Japan</a>
                                    <div class="post-slide-description">
                                        When I get the idea of going to japan I was thinking
                                    </div>
                                </div>
                            </li>
                            <li>
                                <img src="assets2/image/photos/frommoroccotojapan/maxresdefault (1).jpg"
                                     alt="">
                                <div class="post-slider-caption">
                                    <a class="post-slide-name" href="#">4 Days from Morocco to Japan</a>
                                    <div class="post-slide-description">
                                        When I get the idea of going to japan I was thinking
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            -->

            @include('site.layouts._sidebar')

        </aside>
    </div>
@if(Auth::check())
    @include('site.home.partials._spam_dialog')
    @include('site.dashboard._modals')


    <div class="modal fade" id="likesModal" tabindex="-1" role="dialog" aria-labelledby="likesModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Message to user popup -->
<div class="modal msg-to-user-popup" id="messageToUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-650" role="document" >
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                <div class="top-title-layer" >
                    <h3 class="title" >
                        Message
                        <img class="msg-to" src="" alt="">
                        <span class="txt">Profile</span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="post-create-input">
                    <textarea name="" id="" cols="30" rows="10" placeholder="Type a message..."></textarea>
                    {{-- <div class="medias medias-slider" >
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="409600"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed"><div uid="270941"><img class="thumb" src="https://s3.amazonaws.com/travooo-images2/th180/places/ChIJZUSMKDu4t4kRWDMSA8_l8-4/8964d37ff4fe547ec178a908c65204f8ec8086b7.jpg"></div><span class="close removeFile"><span>×</span></span></div>
                        <div class="img-wrap-newsfeed loading">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div> --}}
                </div>
                <div class="post-create-controls">
                    {{-- <div class="emoji-block">
                        <button class="add-msg__emoji" emoji-target="add_msg_text" type="button">🙂</button>
                    </div> --}}
                    {{-- <div class="post-alloptions" >
                        <ul class="create-link-list" >
                            <li class="post-options" >
                                <input type="file" name="file[]" id="file" style="display:none" multiple="" >
                                <i class="trav-camera click-target" data-target="file" ></i>
                            </li>
                        </ul>
                    </div> --}}
                    <div class="ml-auto post-buttons">
                        <button type="button" class="btn btn-light-grey btn-bordered mr-3" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-light-primary btn-bordered" >Send</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@include('site.home.new.partials._user-who-like-modal')
@include('site.home.new.partials._sendtofriend-modal')
@include('site.home.new.partials._delete-modal')
@include('site.home.new.partials._share-modal')
@include('site.home.new.partials._preloader-modal')
@endif
@endsection

@section('before_site_script')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script defer src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<script src="{{ asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js') }}"></script>
<script src="{{asset('assets2/js/lightbox2/src/js/lightbox.js')}}"></script>
<script src="https://underscorejs.org/underscore-min.js"></script>
<script src="{{ asset('assets2/js/jquery.mentionsInput/jquery.mentionsInput.js?date=31/08/2019') }}"></script>
<script src="https://podio.github.io/jquery-mentions-input/lib/jquery.elastic.js"></script>
<script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
<script src="{{ asset('assets2/js/posts-script.js?v=0.3') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="{{ asset('assets2/js/summernote/summernote.js') }}"></script>
<script src="{{ asset('assets2/js/emoji/jquery.emojiFace.js?v=0.1') }}"></script>
<script src="{{ asset('/plugins/summernote-master/tam-emoji/js/config.js') }}"></script>
<script src="{{ asset('/plugins/summernote-master/tam-emoji/js/tam-emoji.js?v='.time()) }}"></script>
<script src="{{ asset('/plugins/summernote-master/tam-emoji/js/textcomplete.js?v='.time()) }}"></script>
<script src="{{ asset('assets3/js/bootstrap-datepicker-extended.js') }}"></script>
<script src="{{asset('assets2/js/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="{{ asset('assets/js/lightslider.min.js') }}"></script>
<script src="{{ asset('assets3/js/tether.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />
<script src="{{ asset('assets2/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>


<script>
    lightbox.option({
        'disableScrolling': true,
        'fitImagesInViewport': true,
    });
</script>
@include('site.home.walkthrough._steps-modal')
@include('site.home._scripts')
@include('site.home.partials._comments_scripts')
@include('site.home.new.partials._create-options-modal')
@include('site.discussions.partials._view-scripts')
@include('site.partials.send-post')

<script>
    var postsAjax = 0;

$("body").delegate( ".post-txt-wrap.post-modal a", "click", function(e) {
  e.stopPropagation()
});

$(function () {
    var hash = window.location.hash;
    if (hash != "") {
        if(hash === '#create-post'){
            setTimeout(function(){  $('.create-new-post-form-trigger').click()}, 100);
            window.history.pushState({}, document.title, "/home");
        }
    }

    $(document).on("keyup", '.note-editable',  function(e) {
        if ($(this).closest('#createNewPostPopup').length == 0)
            $('#sendPostForm').removeAttr("disabled");
    });


    $(document).on('click','.vid-quality-selector button',function(e){
        var type = $(this).attr('data-index');
        $('.vid-quality-selector button').removeClass('active');
        $(this).addClass('active');
        if(type == 0){// 720p

        }else if(type == 1){// 360p

        }else{// 240p

        }
    });

    $(".datepicker").datepicker({
        dateFormat: "d MM yy",
        altField: "#post_actual_date",
        altFormat: "yy-mm-dd",
        maxDate: 0,

    });

    $('body').on('click', '.dpSwitch', function (e) {
        $(".datepicker").datepicker("show");
    });
});
        setTimeout(function () {
            travSlider = $("#newTravelerDiscover").lightSlider({
                autoWidth: true,
                pager: false,
                controls: false,
                slideMargin: 9,
                loop: true,
                onSliderLoad: function (el) {
                    $('#loadAnimation').addClass('d-none')
                    $('#newTravelerDiscover').removeClass('d-none')
                        travSlider.refresh();
                    $(el).height(240);
                }, });

            destSlider = $("#discoverNewDestination").lightSlider({
                autoWidth: true,
                pager: false,
                controls: false,
                slideMargin: 20,
                loop: true,
                responsive: [{
                    breakpoint:767,
                    settings: {
                        slideMargin: 9,
                    },
                }],
                onSliderLoad: function (el) {
                    $('#destAnimation').addClass('d-none')
                    $('#discoverNewDestination').removeClass('d-none')
                        destSlider.refresh();
                    $(el).height(300);
                }, });
        }, 2500);


        setTimeout(function () {
            $('#placeAsideAnimation').remove()
            $('#placeAsideBlock').removeClass('d-none')
        }, 3500);


// for spam reporting
function injectData(id, obj)
{
    // var button = $(event.relatedTarget);
    var posttype = $(obj).parent().attr("posttype");
    $("#spamReportDlg").find("#dataid").val(id);
    $("#spamReportDlg").find("#posttype").val(posttype);
}

// for trips liking
function tripslike(type, id, obj)
{
    $.ajax({
        url: "{{route('trip.likeunlike')}}",
        type: "POST",
        data: { type: type, id: id},
        dataType: "json",
        success: function(data, status){
            $(obj).find('b').html(data.count);
        },
        error: function(){}
    });
}

// for trip commenting
function tripcomment_delete(id, obj, e)
{
    // commentId = $(obj).attr('id');
    if(!confirm("Are you sure delete this comment?"))
        return;
    $.ajax({
        method: "POST",
        url: "{{ route('trip.commentdelete') }}",
        data: {comment_id: id}
    })
        .done(function (res) {
            $('#tripcommentRow'+id).fadeOut();

        });

        e.preventDefault();
}

// for checkin commenting checkincomment_delete
function checkincomment_delete(id, obj, e)
{
    // commentId = $(obj).attr('id');
    //alert(commentId);
    $.ajax({
        method: "POST",
        url: "{{ route('post.checkincommentdelete') }}",
        data: {comment_id: id}
    })
        .done(function (res) {
            console.log('done');
            $('#checkincommentRow'+id).fadeOut();

        });

        e.preventDefault();
}

//dragable images post
const slider = document.querySelector('.items');
let isDown = false;
let startX;
let scrollLeft;

slider.addEventListener('mousedown', (e) => {
  isDown = true;
  slider.classList.add('active');
  startX = e.pageX - slider.offsetLeft;
  scrollLeft = slider.scrollLeft;
});
slider.addEventListener('mouseleave', () => {
  isDown = false;
  slider.classList.remove('active');
});
slider.addEventListener('mouseup', () => {
  isDown = false;
  slider.classList.remove('active');
});
slider.addEventListener('mousemove', (e) => {
  if(!isDown) return;
  e.preventDefault();
  const x = e.pageX - slider.offsetLeft;
  const walk = (x - startX) * 3; //scroll-fast
  slider.scrollLeft = scrollLeft - walk;
  console.log(walk);
});

//for checkin liking
function checkinlike(id, obj)
{
    $.ajax({
        url: "{{route('post.checkinlikeunlike')}}",
        type: "POST",
        data: { id: id},
        dataType: "json",
        success: function(data, status){
            $(obj).find('b').html(data.count);
        },
        error: function(){}
    });
}

//for following of new traveller
function newsfeed_traveller_following(type, id, obj)
{
    var followurl = "{{  route('profile.follow', ':user_id')}}";
    var unfollowurl = "{{  route('profile.unfolllow', ':user_id')}}";
    if(type == "follow")
    {
        url = followurl.replace(':user_id', id);
    }
    else if(type == "unfollow")
    {
        url = unfollowurl.replace(':user_id', id);
    }
    $.ajax({
        method: "POST",
        url: url,
        data: {name: "test"}
    })
    .done(function (res) {
        if (res.success == true) {
            if(type == "follow")
            {
                $(obj).replaceWith('<button type="button" class="btn btn-light-grey btn-bordered" onclick="newsfeed_traveller_following(\'unfollow\','+ id +', this)">Unfollow</button>');
            }
            else if(type == "unfollow")
            {
                $(obj).replaceWith('<button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_traveller_following(\'follow\','+ id +', this)">@lang('home.follow')</button>');
            }
        } else if (res.success == false) {

        }
    });
}

//holiday show popover

$(".post-holiday-popover").click(function(){
    let holId = $(this).data("show_holiday")
    if ($(`.holiday_popover_${holId}`).hasClass("show")) {
        $(`.holiday_popover_${holId}`).removeClass("show")
        $(`.holiday_popover_${holId}`).addClass("d-none")
        console.log("show")
    }
    else {
        $(`.holiday_popover_${holId}`).addClass("show")
        $(`.holiday_popover_${holId}`).removeClass("d-none")
        console.log("hide")
    }
})

//for following of place
function newsfeed_place_following(type, id, obj)
{
    var followurl = "{{  route('place.follow', ':user_id')}}";
    var unfollowurl = "{{  route('place.unfollow', ':user_id')}}";
    if(type == "follow")
    {
        url = followurl.replace(':user_id', id);
    }
    else if(type == "unfollow")
    {
        url = unfollowurl.replace(':user_id', id);
    }
    $.ajax({
        method: "POST",
        url: url,
        data: {name: "test"}
    })
    .done(function (res) {
        if (res.success == true) {
            $(obj).parent().find("b").html(res.count);
            if(type == "follow")
            {
                $(obj).replaceWith('<button type="button" class="btn btn-light-grey btn-bordered" onclick="newsfeed_place_following(\'unfollow\','+ id +', this)">Unfollow</button>');
            }
            else if(type == "unfollow")
            {
                $(obj).replaceWith('<button type="button" class="btn btn-light-primary btn-bordered" onclick="newsfeed_place_following(\'follow\','+ id +', this)">@lang('home.follow')</button>');
            }
        } else if (res.success == false) {

        }
    });
}


//for following of post holiday city/country
function newsfeed_city_country_following(type, holidayType, id, obj)
{
    let url_id
    var followurl = "{{ route('city.follow', ':user_id')}}";
    var unfollowurl = "{{ route('city.unfollow', ':user_id')}}";
    if(type == "follow")
    {
        url_id = followurl.replace(':user_id', id);
        url = url_id.replace('city', holidayType);
    }
    else if(type == "unfollow")
    {
        url_id = unfollowurl.replace(':user_id', id);
        url = url_id.replace('city', holidayType);
    }
    $.ajax({
        method: "POST",
        url: url,
        data: {name: "test"}
    })
    .done(function (res) {
        if (res.success == true) {
            // $(obj).parent().find("b").html(res.count);
            $(`.foll_${id}`).html(res.count);
            if(type == "follow")
            {
                $(`.follow_${id}`).replaceWith(`<button type="button" class="btn btn-light-grey btn-bordered follow_${id}" onclick="newsfeed_city_country_following('unfollow', '${holidayType}', ${id}, this)">Unfollow</button>`);
            }
            else if(type == "unfollow")
            {
                $(`.follow_${id}`).replaceWith(`<button type="button" class="btn btn-light-primary btn-bordered follow_${id}" onclick="newsfeed_city_country_following('follow', '${holidayType}', ${id}, this)">@lang('home.follow')</button>`);
            }
        } else if (res.success == false) {

        }
    });
}

// function getSecondaryPost(){
//     nextPage = parseInt($('#pageno').val());

//     var postsInterval_1 = setInterval(function () {
//         if (!postsAjax) {
//             postsAjax = 1;
//             $.ajax({
//                 type: 'POST',
//                 url: '{{url("home/update_feed")}}',
//                 data: { is_secondary: 1,pageno: nextPage, key: 'a' },
//                 success: function(data){
//                     if(data != ''){
//                         $('#feed').append(data);

//                         lazyloader();
//                         $('[data-tab]').on('click', function (e) {
//                             var Item = $(this).attr('data-tab');
//                             $('[data-content]').css('display', 'none');
//                             $('[data-content=' + Item + ']').css('display', 'block');
//                             e.preventDefault();
//                             Comment_Show.init($(this).closest('.post-block'));
//                         });
//                         $("#pagination").fadeOut().delay(800).fadeIn();
//                     } else {
//                         $("#pagination").hide();
//                     }
//                 }
//             }).always(function () {
//                 postsAjax = 0;
//                 clearInterval(postsInterval_1)
//             });
//         }
//     }, 500);
// }

//function for random secondary post 
function getSecondarySuggestion(){
    nextPage = parseInt($('#pageno').val());

    var postsInterval_2 = setInterval(function () {
        if (!postsAjax) {
            postsAjax = 1;
            $.ajax({
                type: 'POST',
                url: '{{url("home/update_feed")}}',
                data: { is_suggestion: 1,pageno: nextPage, key: 'b' },
                success: function(data){
                    if(data != ''){
                        $('#feed').append(data);

                        lazyloader();
                        $('[data-tab]').on('click', function (e) {
                            var Item = $(this).attr('data-tab');
                            $('[data-content]').css('display', 'none');
                            $('[data-content=' + Item + ']').css('display', 'block');
                            e.preventDefault();
                            Comment_Show.init($(this).closest('.post-block'));
                        });

                        $("#pagination").fadeOut().delay(800).fadeIn();
                    } else {
                        $("#pagination").hide();
                    }
                }
            }).always(function () {
                postsAjax = 0;
                clearInterval(postsInterval_2)
            });
        }
    }, 500);
}
</script>

<script type="text/javascript">
window.addEventListener('load', function () {
  // Check for new tours and activities
//   $.ajax({
//         type: 'GET',
//         url: '{{ url("updateToursCities")}}',
//         data: {},
//         success: function(data){
//             console.log('Tours Cities');
//         }
//     });

//     $.ajax({
//         type: 'GET',
//         url: '{{ url("updateToursCountries")}}',
//         data: {},
//         success: function(data){
//             console.log('Tours Countries');
//         }
//     });

    // $.ajax({
    //     type: 'GET',
    //     url: '{{ url("updateActivitiesCities")}}',
    //     data: {},
    //     success: function(data){
    //         console.log('Activities Cities');
    //     }
    // });

//     $.ajax({
//         type: 'GET',
//         url: '{{ url("updateActivitiesCountries")}}',
//         data: {},
//         success: function(data){
//             console.log('Activities Countries');
//         }
//     });


});
var scroll_flag_enable = false;
var gb_integerPos =1;
var gb_previous_Scroll = 0;
var gb_scroll_pointer_primary = 1;
var gb_scroll_pointer_secondary = 1;
$(window).on('load', function(){
    scroll_flag_enable = true;
});

// setInterval(function(){
//     getSecondaryPost();
// }, 20000);


var gb_additional_distance = 0;
    $(document).scroll(function() {
        current_scrolling = $(document).scrollTop();
        var distanceFromBottom = Math.floor($(document).height() - $(document).scrollTop() - $(window).height());
        if(parseInt($('#pageno').val()) >3){
            gb_additional_distance =3000;
        }
        if(scroll_flag_enable && current_scrolling>0 &&  distanceFromBottom<(5000+gb_additional_distance)){
            scroll_flag_enable =false;
            var nextPage = parseInt($('#pageno').val())+1;
            $('#pageno').val(nextPage);

            var postsInterval_3 = setInterval(function () {
                if (!postsAjax) {
                    postsAjax = 1;
                    $.ajax({
                        type: 'POST',
                        url: '{{url("home/update_feed")}}',
                        data: { pageno: nextPage, key: 'c' },
                        // async: true,
                        success: function(data){
                            if(data != ''){
                                $('#feed').append(data);

                                lazyloader();
                                $('[data-tab]').on('click', function (e) {
                                    var Item = $(this).attr('data-tab');
                                    $('[data-content]').css('display', 'none');
                                    $('[data-content=' + Item + ']').css('display', 'block');
                                    e.preventDefault();
                                    Comment_Show.init($(this).closest('.post-block'));

                                });
                                getSecondarySuggestion();
                                scroll_flag_enable =true;
                                $("#pagination").fadeOut().delay(800).fadeIn();
                            } else {
                                $("#pagination").hide();
                            }
                        }
                    }).always(function () {
                        postsAjax = 0;
                        clearInterval(postsInterval_3)
                    });
                }
            }, 500);
        }
    });

$(document).ready(function() {
    //scroll based content loading
   

    // copy post link  
    $(document).on( 'click', '.copy-newsfeed-link', function(){
        var copy_link = $(this).attr('data-link');
        var _temp = $("<input>");
        $("body").append(_temp);
        _temp.val(copy_link).select();
         document.execCommand("copy");
         _temp.remove()

        toastr.success('The link copied!');
    });
    //followplace 

    $('body').find('.follow_place').on('click',function (e){
        var id = $(this).attr("place_id");
        var type =  $(this).attr("type");
        var followurl = "{{  route('place.follow', ':place_id')}}";
        var unfollowurl = "{{  route('place.unfollow', ':user_id')}}";
        if(type == "follow")
        {
            url = followurl.replace(':user_id', id);
        }
        else if(type == "unfollow")
        {
            url = unfollowurl.replace(':user_id', id);
        }
        $.ajax({
            method: "POST",
            url: url,
            data: {name: "test"}
        })
        .done(function (res) {
            console.log(res);
        });
    });
    
    //permission change functionality
    $('.permission_drop_down').on('click',function(){
        type = $(this).data('type');
        post_id = $(this).data('post-id');
        $('.add_post_permission_button_'+post_id).empty();

         $.ajax({
            type: 'POST',
            url: '{{ url("posts/update-post-permission")}}',
            data: {post_id:post_id,type:type},
            success: function(data){
               if(data == 1){
                    if(type == 0){
                        $('.add_post_permission_button_'+post_id).html('<i class="trav-globe-icon"></i> PUBLIC');
                    }else if(type==1){
                        $('.add_post_permission_button_'+post_id).html('<i class="trav-users-icon"></i> Friends Only');
                    }else if(type==2){
                        $('.add_post_permission_button_'+post_id).html('<i class="trav-lock-icon"></i> Only Me');
                    }
                    toastr.success('Permission updated successfully');
               }
                else if(data ==0)
                    toastr.warning('Something went wrong');
            }
        });
    });
    //ended

    // Trip plan home slider
    // $('body').find('.trip-plan-home-slider').each(function (index) {
    //     $(this).lightSlider({
    //         autoWidth: true,
    //         slideMargin: 10,
    //         pager: false,
    //         controls: false,
    //         onBeforeSlide: function (el) {
    //             console.log(el.getCurrentSlideCount());
    //         }
    //     });
    // });
    // var homeTripSlider = $('.trip-plan-home-slider').lightSlider({
    //                 autoWidth: true,
    //                 slideMargin: 10,
    //                 pager: false,
    //                 controls: false
    //             });
    //             $('.trip-plan-home-slider-prev').click(function(e){
    //                 e.preventDefault();
    //                 homeTripSlider.goToPrevSlide();
    //             });
    //             $('.trip-plan-home-slider-next').click(function(e){
    //                 e.preventDefault();
    //                 homeTripSlider.goToNextSlide();
    //             });

    $('.back').click(function () {
        $(this).closest('.modal').modal('hide');
        $('#createNewPostPopup').modal('show');
    })
    $('.create-new-post-form-trigger').click(function () {
        $('#sendPostForm').attr('disabled', 'disabled');
        $('#createNewPostPopup').modal('show');
    })
    $(document).on('click', '#closePostBox', function () {
        $('#checkinSearchResults').html('');
        $('#addPostTagsModal .tagging-search-block').html('');
        $('#modalTaggingSearchInput').val('');
        $('#createPostTxt').val('');
        $('#location').val('');
        $('#permission').val('');
        $('#checkin_date').val('');
        $('#modalTaggingSearchInput').val('');
        $('#createPostTxt').next('.note-editor').find('.note-editable').html('');
        $('#createPostTxt').next('.note-editor').find('.note-editing-area').find(".note-placeholder").addClass('custom-placeholder');
        $('#createPostBlock').find('.check-in-point').hide();
        $('#createPostBlock .note-toolbar .note-insert').css('bottom', '-60px');
        $('#createPostBlock').find('.medias.medias-slider').html('');
        $('#checkInSearchField').find('div:not(#placeholder)').html('');
        $('#checkInSearchField').find('#placeholder').css('display', 'block');
        $('#checkInSearchField').val('');
        $('#permission').val('');
        $('#post_actual_date').val('');
        $('#location').val('');
        $('#checkin_date').val('');
        $('#post_actual_location').val('');
        $('.check-in-point').css('display','none');
        $('.add_post_permission[data-id="0"]').click();
        $('.tagged-destinations-container').empty();
        $('.post-tags-list').addClass('d-none');

        $('#createNewPostPopup').modal('hide');
    });
    $(document).on('shown.bs.modal', '#createNewPostCheckinPopup', function () {
        var data = {
            q: '',
            lat: window.locationLat,
            lng: window.locationLng
        }
        $.ajax({
            type: 'POST',
            url: '{{url("home/locationSearchResult")}}',
            data: data,
            success: function(data){
                var content = '';
                $.each(JSON.parse(data), function (index, value) {
                    var location = value.id + "," + value.title + "," + value.lat + "," + value.lng + "," + value.type;
                    content += '<div class="checkin-results-item" data-location="' + location + '">';
                    content += '<img class="user-img" src="' + value.img + '" alt="" dir="auto">';
                    content += '<div class="text" dir="auto">';
                    content += '<div class="location-title" dir="auto">' + value.title + '</div>';
                    content += '<div class="location-description" dir="auto">' + value.description ? value.description : "" + '</div>';
                    content += '</div>';
                    content += '</div>';
                });
                $('#checkinSearchResults').html(content);
            }
        });
    });
    var currentRequest = null;
    var taggingCurrentRequest = null;
    $(document).on('keyup', '#checkInSearchField', delay(function (e) {
        var data = {
            q: $(this).val(),
            lat: window.locationLat,
            lng: window.locationLng
        }
        currentRequest = $.ajax({
            type: 'POST',
            url: '{{url("home/locationSearchResult")}}',
            data: data,
            beforeSend : function()    {
                if(currentRequest != null) {
                    currentRequest.abort();
                }
            },
            success: function(data){
                var content = '';
                $.each(JSON.parse(data), function (index, value) {
                    var location = value.id + "," + value.title + "," + value.lat + "," + value.lng + "," + value.type;
                    content += '<div class="checkin-results-item" data-location="' + location + '">';
                    content += '<img class="user-img" src="' + value.img + '" alt="" dir="auto">';
                    content += '<div class="text">';
                    content += '<div class="location-title">' + value.title + '</div>';
                    content += '<div class="location-description">' + value.description + '</div>';
                    content += '</div>';
                    content += '</div>';
                });
                $('#checkinSearchResults').html(content);
            }
        });
    },500));
    $(document).on('keyup', '#modalTaggingSearchInput', delay(function (e) {
        taggingCurrentRequest = $.ajax({
            type: 'POST',
            url: '{{url("home/taggingSearchResult")}}',
            data: {q: $(this).val()},
            beforeSend : function()    {
                if(taggingCurrentRequest != null) {
                    taggingCurrentRequest.abort();
                }
            },
            success: function(data){
                var content = '';
                var contentPeople = '';
                var contentPlaces = '';
                var contentCities = '';
                var contentCountries = '';
                var data = JSON.parse(data);
                if (data.counts.people >= 20) {
                    $('#tags-people-results-tab i').html('20+');
                } else {
                    $('#tags-people-results-tab i').html(data.counts.people);
                }
                if (data.counts.places >= 20) {
                    $('#tags-places-results-tab i').html('20+');
                } else {
                    $('#tags-places-results-tab i').html(data.counts.places);
                }
                if (data.counts.cities >= 20) {
                    $('#tags-cities-results-tab i').html('20+');
                } else {
                    $('#tags-cities-results-tab i').html(data.counts.cities);
                }
                if (data.counts.countries >= 20) {
                    $('#tags-countries-results-tab i').html('20+');
                } else {
                    $('#tags-countries-results-tab i').html(data.counts.countries);
                }

                $.each(data.results, function (index, value) {
                    var location = value.id + "," + value.title + "," + value.type;
                    content += '<div class="checkin-results-item" data-title="' + value.title + '" data-id="' + value.id + '" data-type="' + value.type.toLowerCase() + '">';
                    content += '<img class="user-img" src="' + value.img + '" alt="">';
                    content += '<div class="text">';
                    content += '<div class="location-title">' + value.title + '<span>' + value.type + '</span></div>';
                    content += '<div class="location-description">' + value.description + '</div>';
                    content += '</div>';
                    content += '</div>';

                    var separateContent = '<div class="checkin-results-item" data-title="' + value.title + '" data-location="' + location + '" data-id="' + value.id + '" data-type="' + value.type.toLowerCase() + '">';
                    separateContent += '<img class="user-img" src="' + value.img + '" alt="">';
                    separateContent += '<div class="text">';
                    separateContent += '<div class="location-title">' + value.title + '</div>';
                    separateContent += '<div class="location-description">' + value.description + '</div>';
                    separateContent += '</div>';
                    separateContent += '</div>';

                    if (value.type == 'People') {
                        contentPeople += separateContent;
                    } else if (value.type == 'Place') {
                        contentPlaces += separateContent;
                    } else if (value.type == 'Country') {
                        contentCountries += separateContent;
                    } else if (value.type == 'City') {
                        contentCities += separateContent;
                    }
                });

                $('#tags-all-results .checkin-results-block').html(content);
                $('#tags-people-results .checkin-results-block').html(contentPeople);
                $('#tags-places-results .checkin-results-block').html(contentPlaces);
                $('#tags-cities-results .checkin-results-block').html(contentCities);
                $('#tags-countries-results .checkin-results-block').html(contentCountries);

            }
        });
    },500));
    $(document).on('DOMSubtreeModified', '.add-post__media', function (e) {
        $('#createPostBlock .note-editable').trigger('DOMSubtreeModified');
    });

    $(document).on('DOMSubtreeModified', '#createPostBlock .note-editable', function (e) {
        if ($(this).html() != '' && $(this).html() != '<br dir="auto">' && $(this).html() != '<br>') {
            if (($(this).find('div').length > 0 && $(this).find('div').html() != '' && $(this).find('div').html() != '<br dir="auto">') || ($(this).find('div').length == 0 && $(this).html() != '')) {
                $('#sendPostForm').attr('disabled', false);
            } else {
                if ($('#location').val() == '' && $('#createNewPostPopup').find('.img-wrap-newsfeed').length == 0)
                    $('#sendPostForm').attr('disabled', true);
            }
        } else {
            if ($('#location').val() == '' && $('#createNewPostPopup').find('.img-wrap-newsfeed').length == 0)
                $('#sendPostForm').attr('disabled', true);
        }
    });

    $(document).on('change', '#location', function () {
        if ($(this).val()) {
            $('#sendPostForm').attr('disabled', false);
        } else {
            $('#createPostBlock .note-editable').trigger('DOMSubtreeModified');
        }
    });

    $(document).on('hidden.bs.modal', '#createNewPostCheckinPopup', function () {
        var location = $(this).find('#checkInSearchField').val();
        var emojyPosition = parseInt($('#createNewPostPopup .note-toolbar .note-insert').css('bottom'), 10);
        if (location !== '') {
            $('#createPostBlock').find('.check-in-point').show();
            if (emojyPosition == -60)
                $('#createNewPostPopup .note-toolbar .note-insert').css('bottom', (emojyPosition-36)+'px');
            $('input[name="location"]').val($('#checkInSearchField').data('selected_place')).trigger('change');
        } else {
            $('#createPostBlock').find('.check-in-point').hide();
            if (emojyPosition != -60)
                $('#createNewPostPopup .note-toolbar .note-insert').css('bottom', '-60px');
            $('input[name="location"]').val('').trigger('change');
        }
        $('#createPostBlock').find('.check-in-point').find('.locationText').html(location);
        $('#createPostBlock').find('.check-in-point').find('.locationDate').html($(this).find('input[name="checkin_date"]').val());
        $('#createPostBlock').find('.check-in-point').find('.locationTime').html($(this).find('input[name="checkin_time"]').val());
        $('#checkin_date').val($('.checkin_date').val() + ' ' + $('.checkin_time').val());
        // console.log($('.checkin_date').val() + $('.checkin_time').val(), 44455);
    });

    $(document).on('click', '#checkinSearchResults .checkin-results-item', function () {
       $('#checkInSearchField').val($(this).find('.location-title').html());
       $('#checkInSearchField').data('selected_place', $(this).data("location"));
    });

    $(document).on('keyup', '#createPostBlock .note-editable', function () {
        checkTagExist();
    });

    $(document).on('shown.bs.modal', '#addPostTagsModal', function () {
        var taggedElements = $(this).find('.tagging-search-block').find('li');
        if (taggedElements.length > 0) {
            $.each(taggedElements, function (i, element) {
                if ($('.note-editing-area').find("a[href='/" + $(this).closest('li').data("type") + "/" + $(this).closest('li').data("id") + "']").length === 0) {
                    $(this).remove();
                }
            });
        }
    });

    $(document).on('hidden.bs.modal', '#addPostTagsModal', function () {
        checkTagExist();
    });

    $(document).on('click', '#addPostTagsModal .close-search-interest-filter[data-interest-val="marrakesh"]', function () {
        $(this).parent('li').remove();
        $('.note-editing-area').find("a[href='/" + $(this).closest('li').data("type") + "/" + $(this).closest('li').data("id") + "']").remove();

        checkTagExist();
    });

    function checkTagExist() {
        if ($('.note-editing-area').find('.summernote-selected-items').length) {
            if (!$('.add-post__tags').hasClass('add-post-tags-active')) {
                $('.add-post__tags').addClass('add-post-tags-active')
            }
        } else {
            $('.add-post__tags').removeClass('add-post-tags-active');
            $('#createPostBlock .note-editable').find('.tagging-search-block').html('');
        }
    }

    $(document).on('click', '#addPostTagsModal .checkin-results-item', function () {
        var link = '';
        var type = $(this).data('type');
        if (type == 'people') {
            type = 'profile';
        }
        var selectedPlaceContent = '<li class="tagged-item"  data-title="' + $(this).data("title") + '" data-type="' + type + '" data-id="' + $(this).data("id") + '"  data-link="/' + type + "/" + $(this).data("id") + '">';
        selectedPlaceContent += '<img src="' + $(this).find("img").attr("src") + '" alt="">';
        selectedPlaceContent += '<span class="search-selitem-title">' + $(this).data("title") + '</span>';
        selectedPlaceContent += '<span class="close-search-item close-search-interest-filter" data-interest-val="marrakesh"><i class="trav-close-icon"></i></span>';
        selectedPlaceContent += '</li>';

        if ($('.search-selected-block').find("li[data-link='/" + type + "/" + $(this).data("id") + "']").length === 0) {
            $('.tagging-search-block').append(selectedPlaceContent);
            var element = $('.search-selected-block').find("li[data-link='/" + type + "/" + $(this).data("id") + "']")

            link += '<a href="' + element.data('link') + '" class="summernote-selected-items post-text-tag-' + element.data('id') + '" data-toggle="popover" data-html="true" data-original-title="" title="">';
            if (element.data('type') !== 'profile')
                link += '<i data-id="' + element.data('id') + '"  class="trav-set-location-icon"></i> ';
            link += element.data("title") + '</a> ';

            if ($('#createPostTxt div:not(.taggedPlaces)').length > 0) {
                $('#createPostTxt div:not(.taggedPlaces)').append(link);
            } else {
                $('#createPostTxt').append(link);
            }
            if ($('#createPostTxt').next('.note-editor').find('.note-editable').find('div:not(.taggedPlaces)').length > 0) {
                $('#createPostTxt').next('.note-editor').find('.note-editable').find('div:not(.taggedPlaces)').append(link);
            } else {
                $('#createPostTxt').next('.note-editor').find('.note-editable').find('br').remove();
                $('#createPostTxt').next('.note-editor').find('.note-editable').append(link);
            }
            $('#createPostTxt').next('.note-editor').find('.note-editing-area').find(".note-placeholder").removeClass('custom-placeholder');

            if (!$('.add-post__tags').hasClass('add-post-tags-active')) {
                $('.add-post__tags').addClass('add-post-tags-active')
            }
        }
    });

    $(document).on('hidden.bs.modal', '#createNewPostPopup', function () {
        $('.add-post__tags').removeClass('add-post-tags-active')
    });

    //Create new post medias slider

    $('.medias.medias-slider').lightSlider({
        // width: 100%,
        // slideMargin: 10,
        // pager: false,
        controls: false,
        addClass: 'new-post-medias', 
    });

    $('.checkin_date').datepicker({
        format: 'M dd yyyy',
    });
    $('.checkin_time').timepicker({
        timeFormat: 'h:i A'
    });

    document.textcompleteUrl = "{{ url('reports/get_search_info') }}";
    // Relative path to emojis
    var taggingButton = `<button class="add-post__tags" data-toggle="modal" data-target="#addPostTagsModal" type="button"><i class="trav-tag-icon" data-dismiss="modal" data-toggle="modal" data-target="#addPostTagsModal"></i></button>`;
    document.emojiSource = "{{ asset('plugins/summernote-master/tam-emoji/img') }}";
    $("#createPostTxt").summernote({
        airMode: false,
        focus: false,
        disableDragAndDrop: true,
        placeholder: 'Write something...',
        toolbar: [
            ['insert', ['emoji']],
            ['custom', ['textComplate']],
        ],
        callbacks: {
            onFocus: function(e) {
                $(e.target).parent('.note-editing-area').find(".note-placeholder").removeClass('custom-placeholder');
                $(e.target).closest('#createPostTxt').find('.post-create-controls').removeClass('d-none');
            },
            onInit: function(e) {
                e.editingArea.find(".note-placeholder").addClass('custom-placeholder');
                $('.note-toolbar .note-custom').html(`<button class="add-post__tags" data-toggle="modal" data-target="#addPostTagsModal" type="button"><i class="trav-tag-icon" data-dismiss="modal" data-toggle="modal" data-target="#addPostTagsModal"></i></button>`)
            }
        }
    });

$(document).on("click" ,".modal-close", function(event) {
    let videos = $("#post_view").find("video[class='lslide active']")
    if ($("#post_view").find("video").length) {
        videos[0].pause() 
        if($("#post_view").find("video").length == 1) {
            localStorage.setItem("currentTime", videos[0].currentTime)
            document.getElementById(`video_tmp${localStorage.getItem("currentVideoId")}`).currentTime = localStorage.getItem("currentTime")
            document.getElementById(`video_tmp${localStorage.getItem("currentVideoId")}`).play()
        }
    }
})

$(document).on('click','.post-modal', function (e) {
            var post_id = $(this).data('id');
            var post_type = $(this).attr('post-type')
            var post_shared = !!$(this).attr('data-shared')
            var shared_id = $(this).attr("data-shared_id")
            let videos = []
            let currentTime = 0
            let currentVideoId
            if ($(this).find("video").length && e.target instanceof HTMLVideoElement) {
                videos = $(this).find("video")
                currentVideoId = e.target.id.match(/[0-9]+/g)[0]
                currentTime = videos.length == 1 ? videos[0].currentTime : 0
                localStorage.setItem("currentVideoId", currentVideoId)
                localStorage.setItem("currentTime", currentTime)
            }
            $('#post_view').find(".post-type-modal-inner").remove()
            $('#post_view').find(".modal-850").remove()
            
            $('#post_view').append(`
                                <div class="modal-dialog modal-custom-style modal-850" role="document">
                                    <div class="modal-custom-block">
                                        <div class="post-block post-happen-question">
                                            <div class="dis-loader-bar">
                                                <span>
                                                    <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>`)
            $('#post_view').modal('show')
            // window.history.pushState({}, document.title, "/home");
            $.ajax({
                method: "POST",
                url: "{{route('post.get_post_view')}}",
                data: {post_id: post_id, post_type, post_shared, shared_id},
                success: function (res) {
                    if (res != '') {
                        let username = $(res).find(".username").val()
                        post_type.toLowerCase() != 'event' ? window.history.pushState({html: $('#post_view').find(".post-type-modal-inner").innerHTML, isBack: true}, document.title, `${post_type.toLowerCase()}/${username}/${post_id}`) : ''

                        $('#post_view').find(".modal-850").remove()
                        $('#post_view').append(res);
                        answerFilterOption();

                        e.preventDefault();
                        var elem = $('#post_view').find(".comment-btn")
                        var Item = elem.attr('data-tab');
                        CommentEmoji(elem.closest('.post-block').find('.postCommentForm'))
                        
                        $('.emojionearea-button').removeClass('d-none')
                        $('.note-placeholder').addClass('custom-placeholder')
                        if($("#post_view").find("video").length) {
                            if($("#post_view").find("video").length == 1) {
                                let player = $("#post_view").find("video")[0]
                                player.currentTime = localStorage.getItem('currentTime') ?? 0
                                player.muted = false
                                player.controls = true
                                player.play()
                            } else {
                                let player = $("#post_view").find(`video[data-id=video_tmp${localStorage.getItem('currentVideoId')}]`)[0]
                                player.currentTime = 0
                                player.muted = false
                                player.controls = true
                                player.play()
                            }
                        }
                    }
                }
            })
        })

        $(document).on("click", ".modal-close", function () {
            window.history.pushState({}, document.title, "/home");
        }) 

        window.addEventListener("hashchange", function(e) {
        console.log("back", e)
        })
        

        function answerFilterOption() {
            //Answers Filter type select
            $(".sort-select").each(function () {
                var classes = $(this).attr("class"),
                        id = $(this).attr("id"),
                        name = $(this).attr("name"),
                        select = $(this).attr("data-type"),
                        type = $(this).attr("data-sorted_by"),
                        disc_id = $(this).attr("data-dis_id");

                var template = '<div class="' + classes + '" data-type="' + disc_id + '">';
                template += '<span class="sort-select-trigger">' + $(this).attr("placeholder") + '</span>';
                template += '<div class="sort-options">';
                $(this).find("option").each(function () {
                    template += '<span class="sort-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
                });
                template += '</div></div>';

                $(this).wrap('<div class="sort-select-wrapper"></div>');
                $(this).hide();
                $(this).after(template);
            });

            $(".sort-option:first-of-type").hover(function () {
                $(this).parents(".sort-options").addClass("option-hover");
            }, function () {
                $(this).parents(".sort-options").removeClass("option-hover");
            });
            $(".sort-select-trigger").on("click", function () {
                $('html').one('click', function () {
                    $(".sel-select").removeClass("opened");
                });
                $(this).parents(".sort-select").toggleClass("opened");
                event.stopPropagation();
            });


            $(".sort-option").on("click", function () {
                $(this).parents(".sort-options").find(".sort-option").removeClass("selection");
                $(this).addClass("selection");
                $(this).parents(".sort-select").removeClass("opened");
                $(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

                var discussion_id = $(this).parents(".sort-select").attr('data-type');
                var type = $(this).data("value");

                getFilterAnswers(discussion_id, type)

            });

        }


    // $('.select2ClassLocation').select2({
    //     width: null,
    //     placeholder: placeholder + " Check-in location...",
    //     containerCssClass: "select2-input",
    //     dropdownParent: $('#checkInModal'),
    //     escapeMarkup: function(m) {
    //         return m;
    //     },
    //
    //     ajax: {
    //         url: './home/select2locations',
    //         dataType: 'json',
    //         delay: 500,
    //         data: function (params) {
    //             var query = {
    //                 q: params.term,
    //                 lat: window.locationLat,
    //                 lng: window.locationLng
    //             }
    //
    //             // Query parameters will be ?search=[term]&type=public
    //             return query;
    //         },
    //         // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    //     }
    // });

    function formatTime(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = (hours < 10 ? '0'+hours : hours) + ':' + minutes;
        return strTime;
    }
    var currentdate = new Date();
    var datetime = currentdate.getFullYear() + "-"
        + (currentdate.getMonth()+1)  + "-"
        +  currentdate.getDate() + "T"
        + formatTime(currentdate);

    $('#post_date').attr('value', datetime);
    $('.select2ClassLocation').on('select2:select', function (e) {
        $('#location').val($(this).val()).trigger('change');
    });
    $('#post_actual_date').val($('#post_date').val());
    $('#post_date').on('change', function (e) {
        $('#post_actual_date').val($(this).val());
    });

    var image_src;

	$("body").on("click","#prev-img", function(e){
		if(current_image_position>0)
		{
			current_image_position--;
			$("#image-preview").html('<img src="'+image_src[current_image_position]+'">');
		}
	});

	$("body").on("click","#next-img", function(e){
		if(current_image_position<total_images)
		{
			current_image_position++;
			$("#image-preview").html('<img src="'+image_src[current_image_position]+'">');
		}
	});


});


// $(document).ready(function(){
//
//     var taggingApp = new TAGGING_APP(
//         '999',
//         '.post-create-input',
//         '.note-editable',
//         '#createPostTxt',
//         {
//             tooltipContainerEl: $('#createPostBlock').get(0),
//             tooltipTop: 80
//         });
//
// });


    $(document).ready(function(){
        @if(Auth::check())
            window.Echo.private('notifications-channel.192').listen('ExpertStatusUpdateEvent', function (e) {
                var isApproved = parseInt(e.data.status);

                console.log('notifications-channel.192 => ', 'ExpertStatusUpdateEvent => ', e);

                if (isApproved) {
                    alert('approved ' + e.data.points);
                } else {
                    alert('disapproved');
                }
            });
        @endif
        $('#feedloader').on('inview', function(event, isInView) {
            if (isInView) {
                var nextPage = parseInt($('#pageno').val())+1;
                var postsInterval_4 = setInterval(function () {
                    if (!postsAjax) {
                        postsAjax = 1;
                        $.ajax({
                            type: 'POST',
                            url: '{{url("home/update_feed")}}',
                            data: { pageno: nextPage, key: 'd' },
                            success: function(data){
                                if(data != ''){
                                    $('#feed').append(data);
                                    $('#pageno').val(nextPage);
                                    lazyloader();
                                    $('[data-tab]').on('click', function (e) {
                                        var Item = $(this).attr('data-tab');
                                        $('[data-content]').css('display', 'none');
                                        $('[data-content=' + Item + ']').css('display', 'block');
                                        e.preventDefault();
                                        Comment_Show.init($(this).closest('.post-block'));
                                    });
                                    $("#pagination").fadeOut().delay(800).fadeIn();
                                } else {
                                    $("#pagination").hide();
                                }
                            }
                        }).always(function () {
                            postsAjax = 0;
                            clearInterval(postsInterval_4)
                        });
                    }
                }, 500);
            }
        });
    });


$(document).ready(function() {
    $('.add-post__emoji').emojiInit({
        fontSize:20,
        success : function(data){

        },
        error : function(data,msg){
        }
    });
});

$(document).on('click', '.add-post__post-type', function(){
    var data_type = $(this).attr('data-type');
    if( data_type == "image" )
    {
        if($(this).attr('data-triggered') == 'post')
            $("#_add_post_imagefile").click();
        else
            $("#_add_review_imagefile").click();
    }
    else if( data_type == "video" )
    {
        $("#_add_post_videofile").click();
    }
});

$(document).on('click','.add-post__post-type i.fa-camera',function () {
    var target_el = $('#_add_post_imagefile');
    target_el.trigger('click');
});

$("#_add_post_imagefile , #_add_review_imagefile, #file").change(function(selector){
    var triggered = selector.target.id;
    var fileArray = this.files;
    var mediaBlock = document.getElementsByClassName('img-wrap-newsfeed');
    var limitFileUpload = 0;
    var allowedItems = 10;
    var imgMaxSize = 10000000;
    var videoMaxSize = 250000000;
    if (mediaBlock.length + fileArray.length > 10) {
        allowedItems -= mediaBlock.length;
    }
    $.each(fileArray,function(i,v){
        var file  = v;
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if ((file.size > imgMaxSize && ((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) || (file.size > videoMaxSize && (/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type)))) {
            alert("The file can't be uploaded.");
        } else {
            if (limitFileUpload < Math.abs(allowedItems)) {
                if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])) && !/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type)) {
                    $('#cover').css('background-image', 'url(noimage.png)');
                    $("#message").html("<p style='color:red'>Please Select A valid Image File</p>");
                    $("#_add_post_imagefile").val("");
                    $("#_add_review_imagefile").val("");
                    return false;
                } else {
                    var fRead = new FileReader();
                    fRead.onload = (function (file) {
                        return function (e) {
                            var count = $('.img-wrap-newsfeed').children().length;
                            var fd = new FormData();
                            fd.append('mediafiles', e.target.result);
                            // console.log(fd);
                            $.ajax({
                                method: "POST",
                                url: "/place/upload-media-content",
                                data: fd,
                                contentType: false,
                                processData: false,
                                async: true,
                                xhr: function () {
                                    var xhr = $.ajaxSettings.xhr();
                                    xhr.upload.addEventListener("progress", function (evt) {

                                        if (evt.lengthComputable) {
                                            var percentComplete = ((evt.loaded / evt.total) * 100);
                                            if (triggered == '_add_review_imagefile')
                                                $('#' + count + '_media_content_review').width(percentComplete / 2.5 + 'px');
                                            else
                                                $('#' + count + '_media_content').width(percentComplete / 2.5 + 'px');
                                        }
                                    }, false);
                                    return xhr;
                                },
                                beforeSend: function () {
                                    var media = '';
                                    if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                                        media = $('<video style="object-fit:cover;border-radius:5px" width="40" height="40" controls>' +
                                            '<source src=' + e.target.result + ' type="video/mp4">' +
                                            '<source src="movie.ogg" type="video/ogg">' +
                                            '</video>');
                                        // media = $('<img/>').addClass('thumb').attr('src', 'https://cdn3.iconfinder.com/data/icons/google-material-design-icons/48/ic_play_arrow_48px-512.png');

                                    } else {
                                        media = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element
                                    }
                                    var button = $('<span/>').addClass('close-image').addClass('removeFile').attr('data-name', file.name).click(function () {
                                        var thisButton = $(this);
                                        $.ajax({
                                            method: "DELETE",
                                            url: baseUrl + "/delete-uploaded-temp-files" ,
                                            data: {
                                                'pair': $('#pair').val(),
                                                'name': $(this).attr('data-name')
                                            }
                                        }).done(function (response) {
                                            if (response.success === true) {
                                                //create close button element
                                                if (triggered == '_add_review_imagefile')
                                                    $('#' + count + '_media_attached_review').remove();
                                                else
                                                    $('#' + count + '_media_attached').remove();
                                                thisButton.parent().remove();
                                            }
                                        });
                                    }).append('<span class="close removeFile">&times;</span>');


                                    var imgitem = $('<div>').append(media); //create Image Wrapper element
                                    var progressdivcontainer = '';
                                    if (triggered == '_add_review_imagefile') {
                                        progressdivcontainer = $('<div  id="' + count + '_media_content_review" style="border-radius: 5px;position: absolute;opacity: 1;height: 10px;background: green;width: 39px;margin-top: 14px;" />').addClass('media-progress-bar');
                                        imgitem = $('<div/>').addClass('img-wrap-newsfeed lslide').append(progressdivcontainer).append(imgitem).append(button);
                                        $(".add-post__media").append(imgitem);
                                    } else {
                                        progressdivcontainer = $('<div  id="' + count + '_media_content" style="border-radius: 5px;position: absolute;opacity: 1;height: 10px;background: green;width: 39px;margin-top: 14px;" />').addClass('media-progress-bar');
                                        imgitem = $('<div/>').addClass('img-wrap-newsfeed lslide').append(progressdivcontainer).append(imgitem).append(button);
                                        $(".media-wrap__new-slider").append(imgitem);
                                    }

                                },
                            }).done(function (data) {
                                var inputitem = '';
                                if (triggered == '_add_review_imagefile') {
                                    inputitem = $('<input type="hidden" id="' + count + '_media_attached_review" class="attachmentElm" name="mediafiles[]" />').val(data);
                                    $('#_place_add_review_form').append(inputitem);
                                    $('.add-post__footer-right').show();
                                    $('#' + count + '_media_content_review').width('0px');
                                } else {
                                    inputitem = $('<input type="hidden" id="' + count + '_media_attached" class="attachmentElm" name="mediafiles[]" />').val(data);
                                    $('#_place_add_post_form').append(inputitem);
                                    $('.add-post__footer-right').show();
                                    $('#' + count + '_media_content').width('0px');
                                }

                            });

                        };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.

                }
            }
            limitFileUpload++;
        }
    });
    $("#_add_post_imagefile").val("");
    $("#_add_review_imagefile").val("");
});

$("#_add_post_videofile").change(function(){

    var file = this.files[0];

    if ( !/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type) )
    {
        $('#cover').css('background-image', 'url(noimage.png)');
        $("#message").html("<p style='color:red'>Please Select A valid Image File</p>");
        $("#_add_post_videofile").val("");
        return false;
    }
    else
    {
        var fRead = new FileReader();
        fRead.onload = (function(file){
            return function(e) {

                var video = $('<video controls>'+
                    '<source src='+e.target.result+' type="video/mp4">'+
                    '<source src="movie.ogg" type="video/ogg">'+
                    '</video>');

                var button = $('<span/>').addClass('close-image').addClass('removeFile').click(function(){     //create close button element
                    $(this).parent().remove();
                }).append('<span>&times;</span>');

                var inputitem = $('<input type="hidden" name="mediafiles[]" />').val(e.target.result);

                var videoitem = $('<div/>').append(video);
                videoitem = $('<div/>').addClass('img-wrap-newsfeed lslide').append(videoitem).append(button).append(inputitem);

                $(".medias.medias-slider").append(videoitem);
            };
        })(file);
        fRead.readAsDataURL(file); //URL representing the file's data.
        $("#_add_post_videofile").val("");
    }
});




$(document).on('click', '.add_post_permission', function(e){
    var iconClass = $(this).find('.icon-wrap i').attr('class');
    $('#add_post_permission_button i').removeAttr('class').addClass(iconClass);
    $('#add_post_permission_button span').html($(this).find('.drop-txt p b').html());
    $('#permission').val($(this).data('id'));
});
$(document).ready(function(){
    $(document).on('click', ".post_like_button a", function (e) {
        e.preventDefault();
        $(`a[data-name=${$(this).data('name')}]`).closest('.post_like_button').toggleClass('liked');
    });
    $('#usersWhoLike').on('show.bs.modal',function(e){
        $('#userWhoLikeContainer').empty();
        var datas = e.relatedTarget.dataset;
        $.ajax({
            url: "{{route('home.userlikes')}}",
            type: "POST",
            data:{ type: datas.type, id: datas.id},
            success: function(resp){
                resp =jQuery.parseJSON(resp);
                $('#userWhoLikeContainer').empty();
                $('#userWhoLikeContainer').html(resp.html);
                $('#usersWhoLike').find('.total_count').html(resp.count)
            },error: function(){

            }
        });
       

    });
    $('.extednd-btn').click(function(e){
        e.preventDefault();
        $(this).toggleClass('shrink');
        $(this).closest('.share-to-friend').toggleClass('relative');
    });
    $('#sendToFriendModal').on('hidden.bs.modal', function (e) {
        $('#sendToFriendModal .extednd-btn').removeClass('shrink');
        $('#sendToFriendModal .share-to-friend').removeClass('relative');
    })
    $('.friend-search-results').mCustomScrollbar();
    $('.send_message').on('click',function(){
        var message = $('#message_popup_text').val();

        message = message+'<br><br><br>' +$('#message_popup_content').html();
        var participants = $(this).data('user');
        $.ajax({
            method: "POST",
            url: './../../chat/send',
            data: {
                chat_conversation_id: 0,
                participants: participants,
                message: message,
                post_type: send_post_type,
                post_id: send_post_id,
            },
            success:function(e){
                toastr.success('Your message has been sent');
                $('#sendToFriendModal').modal('hide');
                $('#message_popup_content').empty();
                $('#message_popup_text').val()
            }
        });
        $('#sendToFriendModal .extednd-btn').removeClass('shrink');
        $('#sendToFriendModal .share-to-friend').removeClass('relative');
    });
    $('#sendToFriendModal').on('shown.bs.modal', function (event) {
            $('#message_popup_content').empty();
            var html = $('#'+event.relatedTarget.dataset.id).html();
            var jHtmlObject = jQuery(html);
            var editor = jQuery("<p>").append(jHtmlObject);
            editor.find('.post-top-info-action').remove();
            editor.find('.privacy-settings').remove();
            editor.find('.post-footer-info').remove();
            editor.find('.post-comment-wrapper').remove();
            editor.find('.post-comment-layer').remove();
            var newHtml = editor.html();
            
            $('#message_popup_content').html('<div class="post-block ">'+newHtml+'</div>');

    });
    $('#deletePostNew').on('show.bs.modal',function (event){
        id = event.relatedTarget.dataset.id;
        type = event.relatedTarget.dataset.type;
        element = event.relatedTarget.dataset.element;
        $('#delete_id').val(id); 
        $('#deletePostType').val(type); 
        $('#element_id').val(element); 
    });
    $('.del_post_btn').on('click',function (){
        $.ajax({
            method: "POST",
            url: "{{url("delete-post") }}",
            data: {id:$('#delete_id').val(),type: $('#deletePostType').val()}
        }).done(function (res) {
            if(res>0){
                $('#'+$('#element_id').val()).remove();
                $('#delete_id').val(''); 
                $('#deletePostType').val(''); 
                $('#element_id').val(''); 
                $('#deletePostNew').modal('hide');
                $('#post_view').data("bs.modal")?._isShown ? $('#post_view').modal('hide') : '';
                toastr.success('Post has been deleted');
            }
        });
    });
    $('.follow_unfollow_post_users').on('click',function(){
        id =$(this).data('user_id');
        type = $(this).data('type');
        var followurl = "{{  route('profile.follow', ':user_id')}}";
        var unfollowurl = "{{  route('profile.unfolllow', ':user_id')}}";
        if(type == "follow")
        {
            url = followurl.replace(':user_id', id);
        }
        else if(type == "unfollow")
        {
            url = unfollowurl.replace(':user_id', id);
        }
        $.ajax({
            method: "POST",
            url: url,
            data: {name: "test"}
        })
        .done(function (res) {
            if (res.success == true) {
                toastr.success('User removed from your followings');
                if(type == "follow"){
                    $('#follow_unfollow_post_users').data('type', 'unfollow');
                }else{
                    $('#follow_unfollow_post_users').data('type', 'follow');
                }

            }
        });
    });


    
    $('#messageToUser').on('show.bs.modal',function (event){
        id = event.relatedTarget.dataset.id;
        name = event.relatedTarget.dataset.name;
        img = event.relatedTarget.dataset.image;
        $('.msg-to').attr('src',img);
        $('.txt').text(name);
    });

    $(document).on('click', '.create-new-post-popup .post-tags-list .delete', function () {
        $(this).parent('.tag').remove();
        if($.trim($('.create-new-post-popup .post-tags-list ul').html()) == '') {
            $('.create-new-post-popup .post-tags-list').addClass('d-none');
        }
    });

    // Mobile features
    var isTouchDevice = "ontouchstart" in window || navigator.msMaxTouchPoints > 0;
    if(isTouchDevice) {
        $('.create-new-post-form-trigger').click(function () {
            $('.mobile-user-actions-popup').addClass('show');
            $('.mobile-user-actions-popup').addClass('bottom');
            $('.modal-backdrop').remove();
        })
        $('.create-new-post-popup .mobile-close-btn, .create-new-post-checkin-popup .mobile-close-btn, #closePostBox, #sendPostForm').click(function () {
            $('.mobile-user-actions-popup.slide-up').removeClass('slide-up');
            $('.mobile-user-actions-popup.bottom').removeClass('bottom');
            $('.mobile-user-actions-popup').removeClass('show');
            $('body').css('overflow', 'auto');
        });
    };


});
</script>
@endsection


@section('after_scripts')
<!-- @include('site.layouts._footer-scripts') -->
@endsection
