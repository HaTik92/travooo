<?php
    $post_type = $post['type'] ;
    $variable = $post['variable'] ;
    $report = \App\Models\Reports\Reports::find($post['variable']);
    $str = '';
    
        if(isset($report->infos)){
            foreach($report->infos AS $ri){
                if($ri->var=='text'){
                    $str .= $ri->val; 
                }
            }
        }
        $word = str_word_count(strip_tags($str));
        $m = floor($word / 200);
        $s = floor($word % 200 / (200 / 60));
        if($m>0)
            $est = $m . ' minute' . ($m == 1 ? '' : 's') ;
        else
            $est =  $s . ' second' . ($s == 1 ? '' : 's');
    $following = \App\Models\User\User::find(Auth::user()->id);
    $followers = [];
    foreach($following->get_followers as $f):
        $followers[] = $f->followers_id;
    endforeach;
    $post = $report;
?>
@if(isset( $report ))
    @php
        $uniqueurl = url('reports', $report->id);
    @endphp
    <div class="post-block  post-block-notification  travlog" id="_report_{{$report->id}}">
        <div class="post-top-info-layer">
            @if(!isset($all))
            <div class="post-info-line">
                <i class="icon-infos"></i>  Trending Travelog on  <a class="post-name-link">Travooo</a>
            </div>
            @else
            <div class="post-info-line">
                <i class="icon-infos"></i>  Suggested <a class="post-name-link"> Travelog</a>
            </div>
            @endif
        </div>
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap ava-50">
                    <img src="{{check_profile_picture($report->author->profile_picture)}}" alt="">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link"
                            href="{{url('profile/'.$report->author->id)}}">{{$report->author->name}}</a>
                    </div>
                    <div class="post-info">
					    <a href="{{url('reports', $report->id)}}" target="_blank" class="post-title-link">
                        wrote a <strong>travelog</strong>
                        {{-- about
                        @if(isset($report->cities_id) && $report->cities_id !='')
                        <a href="/city/{{$report->cities_id}}" target="_blank"
                            class="link-place">{{$report->city->transsingle->title}}</a>
                        @elseif(isset($report->countries_id) && $report->countries_id !='')
                        <a href="/country/{{$report->countries_id}}" target="_blank"
                            class="link-place">{{$report->country->transsingle->title}}</a>
                        @endif --}}
                        on {{diffForHumans_2($report->created_at)}}
						</a>
                    </div>
                </div>
            </div>
            <div class="post-top-info-action" dir="auto" id="report_{{$report->id}}" >
                <div class="dropdown" dir="auto">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"      aria-expanded="false" dir="auto">
                        <i class="trav-angle-down" dir="auto"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Report" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                        @if(Auth::user()->id != $report->author->id)
                            @if(in_array($report->author->id,$followers))
                                <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $report->author->id}}" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                        <i class="trav-user-plus-icon" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                        <p dir="auto"><b dir="auto">Unfollow {{ $report->author->name ?? 'User' }}</b></p>
                                        <p dir="auto">Stop seeing posts from {{ $report->author->name ?? 'User' }}</p>
                                    </div>
                                </a>
                            @endif
                        @endif
                        <a class="dropdown-item" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$report->id}}" data-toggle="modal" data-id="{{$report->id}}" data-type="report">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-share-icon" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Share</b></p>
                                <p dir="auto">Spread the word</p>
                            </div>
                        </a>
                        <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-link" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Copy Link</b></p>
                                <p dir="auto">Paste the link anywhere you want</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#sendToFriendModal" data-id="_report_{{$report->id}}" data-toggle="modal" data-target="" dir="auto">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-share-on-travo-icon" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                                <p dir="auto">Share with your friends</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$report->id}},this)" dir="auto">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-flag-icon-o" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Report</b></p>
                                <p dir="auto">Help us understand</p>
                            </div>
                        </a>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="topic-inside-panel-wrap"  onclick=" window.open('/reports/{{$report->id}}'); " style="cursor: pointer;">
            <div class="topic-inside-panel">
                <div class="panel-txt">
                    <h3 class="panel-ttl"><a style="text-decoration: underline;    color: black;" target="_blank" href="/reports/{{$report->id}}">{{$report->title}}</a></h3>
                    <div class="post-txt-wrap post-modal" data-shared="{{isset($sharing_flag)}}" post-type="{{$post_type}}" data-id="{{@$shared_variable ?? @$report->id}}" data-shared_id="{{@$shared_variable ? $report->id : '' }}" style="cursor:pointer">
                        <div>

                            <span class="less-content disc-ml-content">{!! substr($report->description,0, 200) !!}<span class="moreellipses">...</span>
                                @if(strlen($report->description)>200)
                                <a href="javascript:;" class="read-more-link">More</a>@endif
                            </span>
                            <span class="more-content disc-ml-content" style="display: none;">{!! $report->description !!}
                                <a href="javascript:;" class="read-less-link"> Less</a>
                            </span>
                        </div>
                        @if($est != '0 seconds')
                        <div class="read-time-badge">{{$est}} read</div>
                        @endif
                    </div>
                </div>
                <div class="panel-img disc-panel-img">
                    <img src="@if(isset($report->cover[0])){{replace_s3_path_with_cloudfront($report->cover[0]->val,'200x0')}}@else {{asset('assets2/image/placeholders/no-photo.png')}} @endif"
                        alt="{{$report->title}}" style="width: 170px; height: 120px;">
                </div>
            </div>
        </div>
        @php
        // dd($report);
            $flag_liked  =false;
            if(count($report->likes)>0){
                foreach($report->likes as $like){
                    if($like->users_id == Auth::user()->id)
                            $flag_liked = true;
                }
            }
        @endphp
        <div class="post-footer-info"> 
            @include('site.home.new.partials._comments-posts', ['post'=>$report, 'post_type'=>'report'])
        </div>
        <div class="post-comment-wrapper" id="following{{$report->id}}">
            @if(count($report->comments)>0)
                @foreach($report->comments AS $comment)
                    @php
                    if(!in_array($report->users_id, $followers))
                        continue;
                    @endphp
{{--                    @include('site.home.new.partials.post_comment_block')--}}
                @endforeach
            @endif
        </div>
        <div class="post-comment-layer" data-content="comments{{$report->id}}" style='display:none;'>

        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
            </ul>
            <div class="comm-count-info">
                <strong class="{{$report->id}}-opened-comments-count">{{count($report->comments) > 3? 3 : count($report->comments)}}</strong> / <span class="{{$report->id}}-comments-count">{{count($report->comments)}}</span>
            </div>
        </div>
        <div class="ss post-comment-wrapper sortBody" id="comments{{$report->id}}">
            @if(count($report->comments)>0)
                @foreach($report->comments()->take(3)->get() AS $comment)
                    @if(!user_access_denied($comment->author->id))
                        @include('site.home.partials.new-post_comment_block', ['post_id'=>$report->id, 'type'=>'report', 'comment'=>$comment])
                    @endif
                @endforeach
            @endif
        </div>
        @if(count($report->comments)>3)
            <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3"  data-type="report" data-id="{{$report->id}}">Load more...</a>
        @endif

        @include('site.home.partials.new-comment_form_block', ['post_type'=>'report', 'post_id'=>$report->id])
        </div>
    </div>

@endif
