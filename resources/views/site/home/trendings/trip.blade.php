
<style>
    .mapboxgl-ctrl mapboxgl-ctrl-attrib mapboxgl-compact {
        display: none;
    }
    .mapboxgl-ctrl-bottom-left,.mapboxgl-ctrl-top-right{
        display: none;
    }
    .mapboxgl-ctrl-bottom-right{
        display: nsone;
    }
</style>
<?php
    $mapindex                   = $post['type'] ."_create_".$post['variable']."_".strtotime(date('Y-m-d H:m:s')).rand(1, 5000);
    $variable                   = $post['variable'];
    $post_type                  = $post['type'];
    $post_action                = 'create';    
    $trip_plan                  = \App\Models\TripPlans\TripPlans::find($post['variable']);
    $post                       = $trip_plan;
    $contributer                = [];
    $places                     = [];
    $countries                  = [];
    $cities                     = [];
    $total_destination          = 0;
    $trip_data                  = [];
    // $intial_latlng              = [];
    $is_valid_trip              = true;
    $locationData               = [];
    $followers                  = [];
    $trip_card_type             = 'single-city';
    $_totalDestination          = ['city_wise' => [], 'country_wise' => []];

    if (!($trip_plan && $trip_plan->published_places && count($trip_plan->published_places))) {
        $is_valid_trip = false;
    }else{
        if(!in_array(@$trip_plan->id, get_triplist4me())){
            $is_valid_trip = false;
        }
    }

    if($is_valid_trip){
        $author         = Auth::user();
        $contributer    = $trip_plan->contribution_requests->where('status',1)->pluck('users_id')->toArray();
        $followers      = \App\Models\UsersFollowers\UsersFollowers::where('followers_id', Auth::user()->id)->where('follow_type', 1)->pluck('users_id')->toArray();
        $contributer[]  = @$trip_plan->users_id;

        if(isset($trip_plan->author->id)){
            if($author->id != $trip_plan->author->id ){
                if(in_array ($author->id ,$contributer)){
                    $author = $trip_plan->author;
                }else {
                    $following = \App\Models\UsersFollowers\UsersFollowers::where('followers_id',$author->id)->pluck('users_id')->toArray();
                    $ans=  array_values(array_intersect($following,$contributer));
                    if(count($ans)>0){
                        $user_find =\App\Models\User\User::find($ans[0]);
                        if(isset($user_find)){
                            $author = $user_find;
                        }
                    }else{
                        $author = @$trip_plan->author;
                    }
                }
            }
        }else{
            $author = @$author;
        }

        foreach ($trip_plan->published_places as $tripPlace) {
            if (!$tripPlace->place or !$tripPlace->country or !$tripPlace->city) {
                continue;
            }
            if (!isset($places[$tripPlace->places_id])) {
                $total_destination++;
                $_totalDestination['city_wise'][$tripPlace->cities_id][$tripPlace->places_id] = $tripPlace->place;
                $_totalDestination['country_wise'][$tripPlace->countries_id][$tripPlace->places_id] = $tripPlace->place;
                $places[$tripPlace->places_id] = [
                    'type' => 'place',
                    'id' => $tripPlace->places_id,
                    'country_id' => $tripPlace->countries_id,
                    'city_id' => $tripPlace->cities_id,
                    'title' => isset($tripPlace->place->transsingle->title) ? str_replace("'", '',$tripPlace->place->transsingle->title)  : null,
                    'lat' => $tripPlace->place->lat,
                    'lng' => $tripPlace->place->lng,
                    'image' => check_place_photo($tripPlace->place),
                ];
            }
            if (!isset($countries[$tripPlace->countries_id])) {
                $countries[$tripPlace->countries_id] = [
                    'type' => 'country',
                    'id' => $tripPlace->countries_id,
                    'title' => isset($tripPlace->country->trans[0]->title) ? str_replace("'", '',$tripPlace->country->trans[0]->title) : null,
                    'lat' => $tripPlace->country->lat,
                    'lng' => $tripPlace->country->lng,
                    'image' => get_country_flag($tripPlace->country),
                    'total_destination' => 0,
                ];
            }
            if (!isset($cities[$tripPlace->cities_id])) {
                $cities[$tripPlace->cities_id] = [
                    'type' => 'city',
                    'id' => $tripPlace->cities_id,
                    'country_id' => $tripPlace->countries_id,
                    'title' => isset($tripPlace->city->trans[0]->title) ? str_replace("'", '',$tripPlace->city->trans[0]->title) : null,
                    'lat' => $tripPlace->city->lat,
                    'lng' => $tripPlace->city->lng,
                    // 'image' => isset($tripPlace->city->medias[0]->media->path) ?
                    //     check_city_photo($tripPlace->city->medias[0]->media->path) :
                    //     url('assets2/image/placeholders/pattern.png'),
                    'image' => check_place_photo($tripPlace->place),
                    'total_destination' => 0,
                ];
            }
        }

        // $intial_latlng  =  [collect($places)->first()->lng, collect($places)->first()->lat];

        foreach ($countries as $country_id => &$country) {
            $country['total_destination'] = isset($_totalDestination['country_wise'][$country_id]) ? count($_totalDestination['country_wise'][$country_id]) : 0;
        }
        foreach ($cities as $city_id =>  &$city) {
            $city['total_destination'] = isset($_totalDestination['city_wise'][$city_id]) ? count($_totalDestination['city_wise'][$city_id]) : 0;
        }

        $country_title =  isset(array_values($countries)[0]['title']) ? array_values($countries)[0]['title'] : null;
        if (count($countries) > 1) {
            $trip_card_type = "multiple-country";
            $locationData   = array_values($countries);
            $country_title  = null;
        } else {
            if (count($cities) > 1) {
                $trip_card_type = "multiple-city";
                $locationData   = array_values($cities);
            } else {
                $trip_card_type = "single-city";
                $locationData   = array_values($places);
            }
        }

        $tripPlanDisplayTime = $trip_plan->created_at;
        if($post_action == "plan_updated"){
            $tripPlanDisplayTime = $post->time;
        }
    }

?>
@if($is_valid_trip && count($locationData)>0)
    @php
        $uniqueurl = url('trip-plan/'. $author->username, $post->id);
    @endphp

    <div class="post-block post-block-notification" id="trip_{{$trip_plan->id}}">
        <div class="post-top-info-layer">
            <div class="post-info-line">
                @if(@$all)
                    <i class="icon-infos"></i>   Suggested <a class="post-name-link">Plan</a>
                @else
                    <i class="icon-infos"></i>   Trending  <a class="post-name-link">Plan</a>
                @endif
                
            </div>
        </div>
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap">
                    <a class="post-name-link" href="{{url('profile/'.$author->id)}}">
                        <img src="{{check_profile_picture($author->profile_picture)}}" alt="">
                    </a>
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link" href="{{url('profile/'.$author->id)}}">{{$author->name}}</a>{!! get_exp_icon($author) !!}
                    </div>
                    <div class="post-info">
                        {{-- log : {{$post->id}} --}}
                        {{-- 22222 || id : {{$trip_plan->id}} --}}
                        @if($trip_plan->memory !=0)
                            @if($post_action == "plan_updated") Edited @else Posted a @endif <strong>Memory Trip</strong>   
                        @else 
                            @if($post_action == "plan_updated") Edited @else Planning a @endif <strong>Upcoming Trip</strong>
                        @endif
                        {{ count($places) }} Destinations @if($country_title) in <strong>{{$country_title}} </strong> @else in <strong>{{ count($countries) }} countries</strong> @endif <a href="{{$uniqueurl}}" target="_blank" class="post-title-link"> on {{diffForHumans($tripPlanDisplayTime)}} </a>
                    </div>
                </div>
            </div>
            <div class="post-top-info-action" dir="auto">
                <div class="dropdown" dir="auto">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"      aria-expanded="false" dir="auto">
                        <i class="trav-angle-down" dir="auto"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Trip" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                        @if(Auth::check() && Auth::user()->id != $author->id)
                            @if(in_array($author->id,$followers))
                                <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $author->id}}" dir="auto">
                                    <span class="icon-wrap" dir="auto">
                                        <i class="trav-user-plus-icon" dir="auto"></i>
                                    </span>
                                    <div class="drop-txt" dir="auto">
                                        <p dir="auto"><b dir="auto">Unfollow {{ $author->name ?? 'User' }}</b></p>
                                        <p dir="auto">Stop seeing posts from {{ $author->name ?? 'User' }}</p>
                                    </div>
                                </a>
                            @endif
                        @endif
                        <a class="dropdown-item" dir="auto" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$variable}}" data-toggle="modal" data-id="{{$variable}}" data-type="trip">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-share-icon" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Share</b></p>
                                <p dir="auto">Spread the word</p>
                            </div>
                        </a>
                        <a class="dropdown-item copy-newsfeed-link" data-text="" data-link="{{$uniqueurl}}">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-link" dir="auto"></i>
                            </span>
                            <div class="drop-txt">
                                <p dir="auto"><b dir="auto">Copy Link</b></p>
                                <p dir="auto">Paste the link anywhere you want</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#sendToFriendModal" data-id="trip_{{$trip_plan->id}}" data-toggle="modal" data-target="" dir="auto">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-share-on-travo-icon" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                                <p dir="auto">Share with your friends</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$trip_plan->id}},this)" dir="auto">
                            <span class="icon-wrap" dir="auto">
                                <i class="trav-flag-icon-o" dir="auto"></i>
                            </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Report</b></p>
                                <p dir="auto">Help us understand</p>
                            </div>
                        </a>

                    </div>
                </div>
            </div>
        </div>
        <div class="post-image-container">
            <!-- New element -->
            <div class="trip-plan-container">
                <div class="trip-plan-map post-modal" data-shared="{{isset($sharing_flag)}}" post-type="trip" data-id="{{@$trip_plan->id}}" data-shared_id="" style="cursor:pointer">
                    <img style='width:100%;height:400px;' src="{{tripPlanThumb($trip_plan, 615, 400)}}">
                    <a href="/trip/plan/{{$trip_plan->id}}"  target="_blank" class="btn btn-light-primary trip-plan-btn" style="color:white">View Plan</a>
                    {{-- <div class="forecast-map-marker map-popover" type="button" data-placement="top" data-toggle="popover"
                        data-html="true" data-original-title="" title="">
                        <i class="fas fa-map-marker-alt"></i>
                        <span class="count-label">3</span>
                    </div>
                    <div class="forecast-map-marker transfer" style="top: 20%; left: 80%;">
                        <i class="fas fa-plane"></i>
                    </div>
                    <div class="forecast-map-marker transfer" style="top: 40%; left: 60%;">
                        <i class="fas fa-bus"></i>
                    </div>
                    <div class="map-marker-img multiple" style="top: 20%; left: 30%;">
                        <span class="count-label">3</span>
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1597941078_image.png" alt="">
                    </div>
                    <div class="map-marker-img" style="top: 60%; left: 20%;">
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1597941078_image.png" alt="">
                    </div> --}}
                </div>
                <div class="trip-plan-slider-container">
                    <a  class="trip-plan-home-slider{{$mapindex}}prev trip-plan-home-slider-prev"></a>
                    <a  class="trip-plan-home-slider{{$mapindex}}next trip-plan-home-slider-next"></a>
                    <div class="trip-plan-home-slider{{$mapindex}}" style="margin-left: 5px;">
                        @foreach ($locationData as $key=>$item)
                            @if($key==0)
                                <div class="trip-plan-home-slider-item active">
                                    <img src="{{ $item['image'] }}" alt="">
                                    <div class="title"> 
                                        {{$item['title']}} 
                                        @if ($item['type'] != 'place')
                                           <br> {{ $item['total_destination'] }}  Destination
                                        @endif
                                    </div>
                                </div>
                            @elseif($key == array_key_last($places))
                                <div class="trip-plan-home-slider-item ss" style="margin-right: 0px !important">
                                    <img src="{{ $item['image'] }}" alt="">
                                    <div class="title"> 
                                        {{$item['title']}} 
                                        @if ($item['type'] != 'place')
                                           <br> {{ $item['total_destination'] }}  Destination
                                        @endif
                                    </div>
                                </div>
                            @else 
                                <div class="trip-plan-home-slider-item">
                                    <img src="{{ $item['image'] }}" alt="">
                                    <div class="title"> 
                                        {{$item['title']}} 
                                        @if ($item['type'] != 'place')
                                           <br> {{ $item['total_destination'] }}  Destination
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- New element END -->
        </div>
        @php
            $flag_liked  =false;
           if(count($trip_plan->likes->where('places_id',null))>0){
               foreach($trip_plan->likes as $like){
                   if(Auth::check() && $like->users_id == Auth::user()->id)
                        $flag_liked = true;
               }
           }
        @endphp
        <div class="post-footer-info">
            @include('site.home.new.partials._comments-posts', ['post'=>$trip_plan, 'post_type'=>'trip', 'check' => true])
        </div>
        <div class="post-comment-wrapper" id="following{{$trip_plan->id}}">
            @if(count($trip_plan->comments)>0)
                @foreach($trip_plan->comments AS $comment)
                    @php
                    if(!in_array($comment->users_id, $followers))
                        continue;
                    @endphp
                     @include('site.home.partials.new-post_comment_block', ['post_id'=>$trip_plan->id, 'type'=>'trip', 'comment'=>$comment])
                @endforeach
            @endif
        </div>
        <div class="post-comment-layer" data-content="comments{{$trip_plan->id}}" style='display:none;'>

            <div class="post-comment-top-info">
                <ul class="comment-filter">
                    <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                    <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
                </ul>
                <div class="comm-count-info">
                   <strong class="{{$trip_plan->id}}-opened-comments-count">{{count($trip_plan->comments) > 3? 3 : count($trip_plan->comments)}}</strong> / <span class="{{$trip_plan->id}}-comments-count">{{count($trip_plan->comments)}}</span>
                </div>
            </div>
            <div class="ss post-comment-wrapper sortBody comments{{$trip_plan->id}}">
                @if(count($trip_plan->comments)>0)
                    @foreach($trip_plan->comments()->take(3)->get() AS $comment)
                        @if(!user_access_denied($comment->author->id))
                            @include('site.home.partials.new-post_comment_block', ['post_id'=>$trip_plan->id, 'type'=>'trip', 'comment'=>$comment])
                        @endif
                    @endforeach
                @endif
            </div>
        @if(count($trip_plan->comments)>3)
            <a href="javascript:;" class="load-more-comment-link" data-type="trip" pagenum="0" comskip="3" data-id="{{$variable}}">Load more...</a>
        @endif

        @include('site.home.partials.new-comment_form_block', ['post_type'=>'trip', 'post_id'=>$variable, 'post' => $trip_plan])
        </div>
        <!-- Removed comments block -->
    </div>

    @include('site.home.common_trip_script')
@endif
