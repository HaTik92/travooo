<div class="w-through-left-outside-menu">
    <ul>
        <li class="w-through-steps-wrap l-menu-step-2  active t-30">
            <i class="trav-home-icon" dir="auto"></i> Home
        </li>
        <li class="w-through-steps-wrap l-menu-step-3  t-100">
            <i class="trav-trip-plans-p-icon" dir="auto"></i> Trip Plans
        </li>
        <li class="w-through-steps-wrap l-menu-step-4 t-180">
            <i class="trav-ask-icon" dir="auto"></i> Ask
        </li>
        <li class="w-through-steps-wrap l-menu-step-5 t-300">
            <i class="fa-2x fa fa-newspaper-o" aria-hidden="true" ></i> Travelogs
        </li>
    </ul>
</div>

<!--walkthrough Welcome modal-->
<div class="modal fade white-style w-through-modal right" id="walkthrough_step_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog w-through-style w-780" role="document">
        <div class="modal-content w-through-m-content">
            <div class="modal-body">
                <div class="w-through-m-header">
                    <img class="w-through-welcome-img" src="{{asset('assets2/image/w-through/w-through-welcome.png')}}">
                </div>
                <div class="w-through-description">
                    <span class="waktrough-skip"  data-dismiss="modal" aria-label="Close">
                        {{ __('Skip')}} <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    </span>
                    <h1 class="desc-title">Welcome to Travooo</h1>
                    <div class="content">You are a Travooo user now, welcome aboard! Take a look at all the cool features we have to offer by clicking on the button below.</div>
                    <div class="w-through-next">Get Started</div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--walkthrough Newsfeed modal-->
<div class="modal fade white-style w-through-modal right" id="walkthrough_step_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog w-through-style w-780" role="document">
        <div class="modal-content w-through-m-content">
            <div class="modal-body">
                <div class="w-through-m-header">
                    <img class="w-through-welcome-img" src="{{asset('assets2/image/w-through/w-through-welcome.png')}}">
                </div>
                <div class="w-through-description pb-16">
                     <span class="waktrough-skip"  data-dismiss="modal" aria-label="Close">
                        {{ __('Skip')}} <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    </span>
                    <h1 class="desc-title">Home</h1>
                    <div class="content"> This is your very own social feed which reflects on your preferences and interests to bring you all the content that matters to you, all the while tangling you with what other people in your circle are up to.
                        <a href="javascript:;" class="w-more-link">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="w-through-next">Next</div>
                    <ul class="w-through-dots-list">
                        <li class="active"></li><li></li><li></li><li></li><li></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--walkthrough Trip Plan modal-->
<div class="modal fade white-style w-through-modal right" id="walkthrough_step_3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog w-through-style w-780" role="document">
        <div class="modal-content w-through-m-content">
            <div class="modal-body">
                <div class="w-through-m-header">
                    <img class="w-through-welcome-img" src="{{asset('assets2/image/w-through/w-through-welcome.png')}}">
                </div>
                <div class="w-through-description pb-16">
                     <span class="waktrough-skip"  data-dismiss="modal" aria-label="Close">
                        {{ __('Skip')}} <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    </span>
                    <h1 class="desc-title">Trip Plans</h1>
                    <div class="content"> On Travooo, you can create, join, and collaborate on your upcoming or past trips. You can also see where your friends are planning to go.
                        <a href="javascript:;" class="w-more-link">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="w-through-next">Next</div>
                    <ul class="w-through-dots-list">
                        <li></li><li class="active"></li><li></li><li></li><li></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--walkthrough Ask modal-->
<div class="modal fade white-style w-through-modal right" id="walkthrough_step_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog w-through-style w-780" role="document">
        <div class="modal-content w-through-m-content">
            <div class="modal-body">
                <div class="w-through-m-header">
                    <img class="w-through-welcome-img" src="{{asset('assets2/image/w-through/w-through-welcome.png')}}">
                </div>
                <div class="w-through-description pb-16">
                     <span class="waktrough-skip"  data-dismiss="modal" aria-label="Close">
                        {{ __('Skip')}} <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    </span>
                    <h1 class="desc-title">Ask</h1>
                    <div class="content">Participate in topics of discussion associated with the destinations that you love. Get recognition when you answer other people’s questions. 
                        <a href="javascript:;" class="w-more-link">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="w-through-next">Next</div>
                    <ul class="w-through-dots-list">
                        <li></li><li></li><li class="active"></li><li></li><li></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<!--walkthrough Travelogs modal-->
<div class="modal fade white-style w-through-modal right" id="walkthrough_step_5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog w-through-style w-780" role="document">
        <div class="modal-content w-through-m-content">
            <div class="modal-body">
                <div class="w-through-m-header">
                    <img class="w-through-welcome-img" src="{{asset('assets2/image/w-through/w-through-welcome.png')}}">
                </div>
                <div class="w-through-description pb-16">
                     <span class="waktrough-skip"  data-dismiss="modal" aria-label="Close">
                        {{ __('Skip')}} <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    </span>
                    <h1 class="desc-title">Travelogs</h1>
                    <div class="content">Express your ideas and experiences in a heartful way with Travelogs, a blend of words and visuality.
                        <a href="{{url('/dashboard#reports')}}" class="w-more-link">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="w-through-next">Next</div>
                    <ul class="w-through-dots-list">
                        <li></li><li></li><li></li><li class="active"></li><li></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--walkthrough Rating modal-->
<div class="modal fade white-style w-through-modal right" id="walkthrough_step_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog w-through-style w-780" role="document">
        <div class="modal-content w-through-m-content">
            <div class="modal-body">
                <div class="w-through-m-header">
                    <div class="w-through-ratig-wrap">
                        <img src="{{asset('assets2/image/w-through/silver.png')}}">
                        <img src="{{asset('assets2/image/w-through/gold.png')}}">
                    </div>
                </div>
                <div class="w-through-description pb-16">
                    <h1 class="desc-title">Ranking</h1>
                    <div class="content">Contribute and become as the leader of your favorite travel destinations. Unlock special privileges, and get access to our affiliation program! 
                        <a href="javascript:;" class="w-more-link">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="w-through-next">Finish</div>
                    <ul class="w-through-dots-list">
                        <li></li><li></li><li></li><li></li><li class="active"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--walkthrough expert modal-->
<div class="modal fade white-style w-through-modal right" id="walkthrough_step_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog w-through-style w-780" role="document">
        <div class="modal-content w-through-m-content">
            <div class="modal-body">
                <div class="w-through-verified-header">
                    <img class="walkthrough-step-7-img" src="{{asset('assets2/image/w-through/gold.png')}}">
                </div>
                <div class="w-through-description pb-16">
                    <h1 class="desc-title">You truly made our day <img src="{{asset('assets2/image/reaction-icon-smile-only.png')}}" width="30"></h1>
                    <div class="content">
                        We are very excited to see you on Travooo! Thank you for joining us. As a token of appreciation, we are rewarding you the <span class="wt-badge-name"></span> badge.
                        This will help you stand out amongst other members on Travooo. To know more about our Ranking System, click the button below.
                    </div>
                    <a href="{{url('/dashboard#ranking')}}" class="w-through-next">Learn More</a>
                    <ul class="w-through-dots-list">
                        <li></li><li></li><li></li><li></li><li class="active"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<!--walkthrough Exp verified modal-->
<div class="modal fade white-style w-through-modal" id="walkthrough_verified_screen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog w-through-style w-780" role="document">
        <div class="modal-content w-through-m-content">
            <div class="modal-body">
                <div class="w-through-verified-header">
                    <img src="{{asset('assets2/image/w-through/gold.png')}}">
                </div>
                <div class="w-through-description">
                    <h1 class="desc-title">You truly made our day <img src="{{asset('assets2/image/reaction-icon-smile-only.png')}}" width="30"></h1>
                    <div class="content">
                        We are so happy to see you on Travooo. Thank you so much for joining us. as our way to say thank you (again) we are rewarding
                        you the Bronze badge..... if you want to know more about our ranking system click on the button bellow.
                    </div>
                    <div class="w-through-next">Learn More</div>
                </div>
            </div>
        </div>
    </div>
</div>
