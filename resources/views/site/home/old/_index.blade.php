<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-j8y0ITrvFafF4EkV1mPW0BKm6dp3c+J9Fky22Man50Ofxo2wNe5pT1oZejDH9/Dt" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="./assets2/css/style.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"
          rel="stylesheet"/>
    <link href="{{asset('assets2/js/lightbox2/src/css/lightbox.css')}}" rel="stylesheet"/>
    <link href="./assets2/css/select2.min.css" rel="stylesheet"/>
    <title>Travooo - home page</title>
</head>
<body>

<div class="main-wrapper newsfeed-page">
    @include('site/layouts/header')

    <div class="content-wrap">

        <button class="btn btn-mobile-side sidebar-toggler" id="sidebarToggler">
            <i class="trav-cog"></i>
        </button>
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid mobile-fullwidth">
            @include('site/layouts/left_menu')
            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">
                    <!--
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#photosPopup">
                                  Launch photos popup
                                </button>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#storyPopup">
                                  Launch story popup
                                </button>
                    -->

                    @if(session()->has('alert-success'))
                        <div class="alert alert-success" id="postSavedSuccess">
                            {{ session()->get('alert-success') }}
                        </div>
                    @endif

                    @if(session()->has('alert-danger'))
                        <div class="alert alert-danger" id="postSavedDanger">
                            {{ session()->get('alert-danger') }}
                        </div>
                    @endif

                    @if(session()->has('alert-danger-server'))
                        <div class="alert alert-danger" id="postSavedDanger">
                            {{ session()->get('alert-danger-server') }}
                        </div>
                    @endif

                    @if ($errors->has('file'))
                        <div class="alert alert-danger" id="postSavedDanger">
                            {{ $errors->first('file') }}
                        </div>
                    @endif
                    @if ($errors->has('text'))
                        <div class="alert alert-danger" id="postSavedDanger">
                            {{ $errors->first('text') }}
                        </div>
                    @endif

                    <div class="alert alert-danger" id="locationMsg" style="display:none;">

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                    </div>

                    <form class="form-horizontal" method="POST" action="{{url("new-post")}}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="post-block post-create-block" id="createPostBlock">
                            <div class="post-create-input">
                                <input type="text" name="text" id="createPostTxt" placeholder="Write something...">
                                <div class="videos"></div>
                            </div>

                            <div class="post-create-controls">
                                <div class="post-alloptions">
                                    <ul class="create-link-list">
                                        <li class="post-options">
                                            <input type="file" name="file[]" id="file" style="display:none" multiple>
                                            <i class="trav-camera click-target" data-target="file"></i>
                                        </li>
                                        <li class="post-options">
                                            <i class="trav-set-location-icon location"></i>
                                        </li>
                                        <li class="post-options">
                                            <i class="fas fa-calendar-alt dpSwitch"
                                               style='font-size:20px;color:#ccc;cursor:pointer;'></i>
                                            <input type="text" class="datepicker" name="post_date" id="post_date"
                                                   readonly style="color:#ccc;">
                                            <input type="hidden" name="post_actual_date" id="post_actual_date">
                                        </li>
                                    </ul>
                                    <div class="locationLoad">@lang('other.loading_dots')</div>
                                    <div class="locationSelect">
                                        <select class="select2Class select-location" data-live-search="true"
                                                name="location">
                                            <optgroup label="places" class="place-option"></optgroup>
                                            <optgroup label="cities" class="cities-option"></optgroup>
                                            <optgroup label="countries" class="countries-option"></optgroup>
                                        </select>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary btn-disabled" id="sendPostForm">Post
                                </button>
                            </div>
                        </div>
                    </form>

                    @foreach($posts AS $post)

                        @if($post->type=="Post" AND $post->action=="like")
                            @include('site.home.partials.post_like')

                        @elseif($post->type=="Travelmate" AND $post->action=="join")
                            @include('site.home.partials.travelmate_join')

                        @elseif($post->type=="Travelmate" AND $post->action=="request")
                            @include('site.home.partials.travelmate_request')

                        @elseif($post->type=="TripPlan" AND $post->action=="publish")
                            @include('site.home.partials.trip_create')

                        @elseif($post->type=="User" AND $post->action=="follow")
                            @include('site.home.partials.user_follow')

                        @elseif($post->type=="User" AND $post->action=="unfollow")

                        @elseif($post->type=="Country" AND $post->action=="follow")
                            @include('site.home.partials.country_follow')

                        @elseif($post->type=="Country" AND $post->action=="unfollow")

                        @elseif($post->type=="City" AND $post->action=="follow")
                            @include('site.home.partials.city_follow')

                        @elseif($post->type=="City" AND $post->action=="unfollow")

                        @elseif($post->type=="Place" AND $post->action=="follow")
                            @include('site.home.partials.place_follow')

                        @elseif($post->type=="Place" AND $post->action=="review")
                            @include('site.home.partials.place_review')

                        @elseif($post->type=="Place" AND $post->action=="unfollow")


                        @elseif($post->type=="Hotel" AND $post->action=="follow")
                            @include('site.home.partials.hotel_follow')

                        @elseif($post->type=="Hotel" AND $post->action=="review")
                            @include('site.home.partials.hotel_review')

                        @elseif($post->type=="Hotel" AND $post->action=="unfollow")

                        @elseif($post->type=="status" AND $post->action=="comment")
                            @include('site.home.partials.status_comment')

                        @elseif($post->type=="status" AND $post->action=="publish")
                            @include('site.home.partials.status_publish')

                        @elseif($post->type=="status" AND $post->action=="like")
                            @include('site.home.partials.status_like')

                        @elseif($post->type=="status" AND $post->action=="unlike")

                        @elseif(strtolower($post->type)=="media" AND $post->action=="upload")
                            @include('site.home.partials.media_upload')

                        @elseif($post->type=="media" AND $post->action=="comment")
                            @include('site.home.partials.media_comment')

                        @elseif(($post->type=="Medias" OR $post->type=="Media") AND $post->action=="comment")
                            @include('site.home.partials.media_comment')

                        @elseif($post->type=="media" AND $post->action=="like")
                            @include('site.home.partials.media_like')

                        @elseif($post->type=="media" AND $post->action=="unlike")
                            @include('site.home.partials.media_unlike')

                        @elseif(strtolower($post->type)=="checkin" AND $post->action=="do")
                            @include('site.home.partials.checkin_do')



                        @elseif($post->type=="Discussion" AND $post->action=="reply")
                            @include('site.home.partials.discussion_reply')

                        @elseif($post->type=="Post" AND $post->action=="comment")
                            @include('site.home.partials.post_comment')

                        @elseif($post->type=="Users" AND $post->action=="registrate")
                        @elseif($post->type=="Post" AND $post->action=="publish")
                            @include('site.home.partials.post_regular')
                        @elseif($post->type=="Trip" AND $post->action=="invite_friends")
                        @elseif($post->type=="Trip" AND $post->action=="activate")
                        @elseif($post->type=="Trip" AND $post->action=="create")
                        @elseif($post->type=="Trip" AND $post->action=="accept_invitation")
                        @elseif($post->type=="Trip" AND $post->action=="reject_invitation")
                        @elseif($post->type=="Trip" AND $post->action=="delete")
                        @elseif($post->type=="Post" AND $post->action=="like")
                        @elseif($post->type=="Post" AND $post->action=="unlike")
                        @elseif($post->type=="Status" AND $post->action=="comment")
                        @elseif($post->type=="Media" AND $post->action=="like")
                        @elseif($post->type=="Media" AND $post->action=="unlike")
                        @elseif($post->type=="Discussion" AND $post->action=="upvote")
                        @elseif($post->type=="Discussion" AND $post->action=="downvote")
                        @elseif($post->type=="Discussion" AND $post->action=="create")

                        @else
                            <pre>
                            {{var_dump($post)}}
                            </pre>
                            <br/>
                            <br/>
                        @endif

                    @endforeach






                    @include('site.home.partials.trending_destinations')
                <!-- @include('site.home.partials.upcoming_events') -->
                <!-- @include('site.home.partials.places_you_might_like') -->
                <!-- @include('site.home.partials.discover_new_people') -->
                <!-- @include('site.home.partials.upcoming_holidays') -->
                <!-- @include('site.home.partials.videos_you_might_like') -->
                    @include('site.home.partials.discover_new_travellers')
                    @include('site.home.partials.discover_new_destinations')


                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">

                        <div class="post-block post-side-tabs">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#global" role="tab">Global</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#friends"
                                       role="tab">@lang('profile.friends')</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="global" role="tabpanel">
                                    <div class="side-tab-inner mCustomScrollbar">
                                        @foreach($activity_logs AS $activity_log)
                                            @if(($activity_log->type!="Trip" && $activity_log->action!="accept_invitation") && ($activity_log->type!="Trip" && $activity_log->action!="invite_friends")
                                            && ($activity_log->type!="Trip" && $activity_log->action!="activate") && $activity_log->type!="Event")
                                                <div class="side-pane-row">
                                                    <div class="side-pane-avatar-wrap">
                                                        <img src="{{check_profile_picture($activity_log->user->profile_picture)}}"
                                                             alt="">
                                                    </div>
                                                    <div class="side-pane-txt">
                                                        <div class="side-pane-post-ttl">
                                                            <a class="in-side-link"
                                                               href="{{url('profile/'.$activity_log->user->id)}}">{{$activity_log->user->name}}</a>
                                                            {!! get_exp_icon($activity_log->user) !!}
                                                            {!!activityAction($activity_log->type, $activity_log->action, $activity_log->variable)!!}
                                                        </div>
                                                        <!-- SHOULD THINK OF BETTER WAY TO DO THAT -->
                                                        <div class="side-pane-date date-format">{{diffForHumans($activity_log->time)}}</div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                                <div class="tab-pane" id="friends" role="tabpanel">friends tab</div>
                            </div>
                        </div>

                        <div class="post-block post-side-block">
                            <div class="post-side-top">
                                <h3 class="side-ttl">Top places</h3>
                                <div class="side-right-control">
                                    <a href="#" class="see-more-link">See more</a>
                                </div>
                            </div>
                            <div class="post-side-inner">
                                @foreach($top_places as $tplace)
                                    <div class="side-place-block">
                                        <div class="side-place-top">
                                            <div class="side-place-avatar-wrap">
                                                <img src="https://s3.amazonaws.com/travooo-images2/{{@$tplace->place->medias[0]->url}}"
                                                     alt="" style="width:46px;height:46px;">
                                            </div>
                                            <div class="side-place-txt">
                                                <a class="side-place-name"
                                                   href="{{url('place/'.$tplace->place->id)}}">{{@$tplace->place->trans[0]->title}}</a>
                                                <div class="side-place-description">
                                                    <b>{{do_placetype($tplace->place->place_type)}}</b> in <a
                                                            href="{{url('city').'/'.$tplace->place->city->id}}">{{@$tplace->place->city->trans[0]->title}}</a>,
                                                    <a href="{{url('country').'/'.$tplace->place->country->id}}">{{@$tplace->place->country->trans[0]->title}}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="side-place-image-list">
                                            @if(isset($tplace->place))
                                                @foreach($tplace->place->medias AS $pm)
                                                    @if(!$loop->first AND $loop->iteration<6)
                                                        <li>
                                                            <img src="https://s3.amazonaws.com/travooo-images2/{{@$pm->url}}"
                                                                 alt="photo" style="width:79px;height:75px;"></li>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </ul>
                                        <div class="side-place-bottom">
                                            <div class="side-follow-info">
                                                <b>0</b> Following this place
                                            </div>
                                            <button type="button"
                                                    class="btn btn-light-grey btn-bordered">@lang('buttons.general.follow')
                                            </button>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>

                        <div class="post-block post-side-block">
                            <div class="post-side-top">
                                <h3 class="side-ttl">Stories</h3>
                                <div class="side-right-control">
                                    <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                                    <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
                                </div>
                            </div>
                            <div class="post-side-inner">
                                <div class="post-slide-wrap">
                                    <ul id="storySlider" class="post-slider">
                                        <li>
                                            <img src="assets2/image/photos/frommoroccotojapan/maxresdefault (1).jpg"
                                                 alt="">
                                            <div class="post-slider-caption">
                                                <a class="post-slide-name" href="#">4 Days from Morocco to Japan</a>
                                                <div class="post-slide-description">
                                                    When I get the idea of going to japan I was thinking
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="assets2/image/photos/frommoroccotojapan/maxresdefault (1).jpg"
                                                 alt="">
                                            <div class="post-slider-caption">
                                                <a class="post-slide-name" href="#">4 Days from Morocco to Japan</a>
                                                <div class="post-slide-description">
                                                    When I get the idea of going to japan I was thinking
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                                <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                                <li><a href="{{url('/')}}">Advertising</a></li>
                                <li><a href="{{url('/')}}">Cookies</a></li>
                                <li><a href="{{url('/')}}">More</a></li>
                            </ul>
                            <p class="copyright">Travooo &copy; 2017</p>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="photosPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-custom-header">
        <div class="modal-header_avatar-info">
            <div class="head-ava">
                <img src="http://placehold.it/50x50" alt="Header avatar">
            </div>
            <div class="head-txt">
                <div class="head-name">Richard hylton</div>
                <div class="head-sub-txt">@lang('place.photos') <span>1</span></div>
            </div>
        </div>
        <div class="modal-header_close">
            <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                <i class="trav-close-icon"></i>
            </button>
        </div>
    </div>
    <div class="modal-dialog photo-popup-style" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="m-photo-wrapper">
                    <div class="m-photo-block">
                        <ul class="modal-outside-link-list">
                            <li class="outside-link">
                                <a href="#">
                                    <div class="round-icon">
                                        <i class="trav-comment-icon"></i>
                                        <span class="count">189</span>
                                    </div>
                                    <span>@lang('comment.comments')</span>
                                </a>
                            </li>
                            <li class="outside-link">
                                <a href="#">
                                    <div class="round-icon">
                                        <i class="trav-flag-icon"></i>
                                    </div>
                                    <span>@lang('other.report')</span>
                                </a>
                            </li>
                        </ul>
                        <div class="photo-top">
                            <div class="photo-info">
                                Added on
                                <span>March 30, 2017</span>
                            </div>
                            <div class="photo-reaction">
                                <img src="./assets2/image/reaction-icon-smile-only-alter.png" alt="smile">
                                <span>12</span>
                            </div>
                        </div>
                        <div class="photo-wrap">
                            <img src="http://placehold.it/750x510" alt="photo">
                        </div>
                        <div class="photo-bottom">
                            <div class="photo-caption">
                                <div class="avatar-wrap">
                                    <img src="http://placehold.it/25x25" alt="ava">
                                </div>
                                <div class="photo-label-txt">Where did you take this picture?</div>
                            </div>
                            <div class="photo-caption">
                                <div class="avatar-wrap">
                                    <img src="http://placehold.it/25x25" alt="ava">
                                </div>
                                <div class="photo-label-txt">I really love to walk in this beautiful street</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- story popup -->
<div class="modal fade white-style" data-backdrop="false" id="storyPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-750" role="document">
        <div class="modal-custom-block">
            <div class="story-date-layer">
                <div class="s-date-line">
                    <div class="s-date-badge">@lang('time.day_value', ['value' => '1'])</div>
                </div>
                <div class="s-date-line">
                    <div class="s-date-badge">Sun, 18 Jun 2017</div>
                </div>
                <div class="s-date-line">
                    <i class="trav-full-clock-icon"></i>
                    <div class="s-date-badge">@3:00pm</div>
                </div>
            </div>

            <div class="post-block">
                <div class="post-top-info-layer">
                    <div class="post-top-info-wrap">
                        <div class="post-top-avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-top-info-txt">
                            <div class="post-top-name">
                                        <span class="dropdown">
                                            <a class="post-name-link" data-toggle="dropdown" aria-haspopup="true"
                                               aria-expanded="false">Rabat Sale</a>
                                            <ul class="dropdown-menu dropdown-menu-left dropdown-arrow dropdown-place-info">
                                                <li class="dropdown-item">
                                                    <span class="icon-wrap">
                                                        <i class="trav-clock-icon"></i>
                                                    </span>
                                                    <div class="place-info-label">@lang('place.website'):</div>
                                                    <a href="www.onda.ma" class="web-site">www.onda.ma</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <span class="icon-wrap">
                                                        <i class="trav-earth"></i>
                                                    </span>
                                                    <div class="place-info-label">Phone number:</div>
                                                    <a href="callto:+212537808090"
                                                       class="smpl-style">+212 5378-08090</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <span class="icon-wrap">
                                                        <i class="trav-phone"></i>
                                                    </span>
                                                    <div class="place-info-label">Business hours:</div>
                                                    <span class="business-hour">Open 24 hours</span>
                                                </li>
                                                <li class="dropdown-item">
                                                    <span class="icon-wrap">
                                                        <i class="trav-set-location-icon"></i>
                                                    </span>
                                                    <div class="place-info-label">Address:</div>
                                                    <span class="smpl-style">Sale, Morocco</span>
                                                </li>
                                            </ul>
                                        </span>
                                <span class="event-tag">Airport</span>
                                <i class="trav-set-location-icon"></i>
                                Rabat, Morocco
                                <i class="trav-get-direction-icon"></i>
                                <a href="#" class="post-name-link">Get directions</a>
                            </div>
                            <div class="post-info">
                                @lang('trip.spent') <b>$240</b>, Stayed <b>@lang('time.count_min', ['count' => 40])</b>
                            </div>
                        </div>
                    </div>
                    <div class="post-top-info-action">
                        <div class="post-action-icon">
                            <i class="trav-bookmark-icon"></i>
                        </div>
                    </div>
                </div>
                <div class="post-content-inner">
                    <div class="post-main-content">
                        <img src="http://placehold.it/680x390" alt="">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam doloribus maxime soluta,
                            deserunt ipsam quia dicta, ex dolor rem quibusdam laborum, quidem eos dolorem eum dolore
                            saepe facere aperiam iure.</p>
                        <h3>Heading here</h3>
                        <ul class="modal-image-list">
                            <li><img src="http://placehold.it/360x270" alt=""></li>
                            <li><img src="http://placehold.it/315x270" alt=""></li>
                        </ul>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi aliquid rem dolore
                            explicabo exercitationem. Porro recusandae reiciendis labore aliquam laborum?</p>
                    </div>
                    <div class="post-tips">
                        <div class="post-tips-top">
                            <h4 class="post-tips_ttl">Tips about Rabat Sale Airport:</h4>
                        </div>
                        <div class="post-tips_block-wrap">
                            <div class="post-tips_block">
                                <div class="tips-content">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                        architecto vel magnam accusamus nemo odio unde autem, provident eos itaque quas
                                        at, vitae ducimus. Earum magnam quod a tempore.</p>
                                </div>
                                <div class="tips-footer">
                                    <div class="tip-by-info">
                                        <div class="tip-by-img">
                                            <img src="http://placehold.it/25x25" alt="">
                                        </div>
                                        <div class="tip-by-txt">
                                            @lang('other.by') <span class="tag blue">Suzanne</span> <span
                                                    class="date-info">5 days ago</span>
                                        </div>
                                    </div>
                                    <ul class="tip-right-info-list">
                                        <li class="round">
                                            <i class="trav-bookmark-icon"></i>
                                        </li>
                                        <li class="round">
                                            <i class="trav-flag-icon"></i>
                                        </li>
                                        <li class="round blue">
                                            <i class="trav-chevron-up"></i>
                                        </li>
                                        <li class="count">
                                            <span>6</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="post-tips_block">
                                <div class="tips-content">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                        architecto vel magnam accusamus nemo odio unde autem, provident eos itaque quas
                                        at, vitae ducimus. Earum magnam quod a tempore.</p>
                                </div>
                                <div class="tips-footer">
                                    <div class="tip-by-info">
                                        <div class="tip-by-img">
                                            <img src="http://placehold.it/25x25" alt="">
                                        </div>
                                        <div class="tip-by-txt">
                                            @lang('other.by') <span class="link">Diana</span> <span
                                                    class="date-info">1 month ago</span>
                                        </div>
                                    </div>
                                    <ul class="tip-right-info-list">
                                        <li class="round">
                                            <i class="trav-bookmark-icon"></i>
                                        </li>
                                        <li class="round">
                                            <i class="trav-flag-icon"></i>
                                        </li>
                                        <li class="round">
                                            <i class="trav-chevron-up"></i>
                                        </li>
                                        <li class="count">
                                            <span>4</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="more-tips-link">More tips...</a>
                    </div>
                </div>
                <div class="modal-map">
                    <img class="map" src="http://placehold.it/750x300" alt="map">
                    <div class="modal-map_bottom-info">
                        <div class="map-point-ttl">
                            Rabat sale airport
                        </div>
                        <ul class="post-round-info-list">
                            <li>
                                <div class="map-reaction">
                                            <span class="dropdown">
                                                <img src="./assets2/image/reaction-icon-smile-only.png" alt="smile"
                                                     data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <div class="dropdown-menu dropdown-menu-middle-right dropdown-arrow dropdown-emoji-wrap">
                                                    <ul class="dropdown-emoji-list">
                                                        <li>
                                                            <a href="#" class="emoji-link">
                                                                <div class="emoji-wrap like-icon"></div>
                                                                <span class="emoji-name">@lang('chat.emoticons.like')</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="emoji-link">
                                                                <div class="emoji-wrap haha-icon"></div>
                                                                <span class="emoji-name">@lang('chat.emoticons.haha')</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="emoji-link">
                                                                <div class="emoji-wrap wow-icon"></div>
                                                                <span class="emoji-name">@lang('chat.emoticons.wow')</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="emoji-link">
                                                                <div class="emoji-wrap sad-icon"></div>
                                                                <span class="emoji-name">@lang('chat.emoticons.sad')</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="emoji-link">
                                                                <div class="emoji-wrap angry-icon"></div>
                                                                <span class="emoji-name">@lang('chat.emoticons.angry')</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </span>
                                    <span class="count">6</span>
                                </div>
                            </li>
                            <li>
                                <div class="round-icon">
                                    <i class="trav-comment-icon"></i>
                                </div>
                                <span class="count">3</span>
                            </li>
                            <li>
                                <div class="round-icon">
                                    <i class="trav-light-icon"></i>
                                </div>
                                <span class="count">37</span>
                            </li>
                            <li>
                                <a href="#" class="info-link">
                                    <div class="round-icon">
                                        <i class="trav-share-fill-icon"></i>
                                    </div>
                                    <span>@lang('profile.share')</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="post-block">
                <div class="post-top-info-layer">
                    <div class="post-top-info-wrap">
                        <div class="post-top-avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-top-info-txt">
                            <div class="post-top-name">
                                        <span class="dropdown">
                                            <a class="post-name-link" data-toggle="dropdown" aria-haspopup="true"
                                               aria-expanded="false">Haneda</a>
                                            <ul class="dropdown-menu dropdown-menu-left dropdown-arrow dropdown-place-info">
                                                <li class="dropdown-item">
                                                    <span class="icon-wrap">
                                                        <i class="trav-clock-icon"></i>
                                                    </span>
                                                    <div class="place-info-label">@lang('place.website'):</div>
                                                    <a href="www.onda.ma" class="web-site">www.onda.ma</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <span class="icon-wrap">
                                                        <i class="trav-earth"></i>
                                                    </span>
                                                    <div class="place-info-label">Phone number:</div>
                                                    <a href="callto:+212537808090"
                                                       class="smpl-style">+212 5378-08090</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <span class="icon-wrap">
                                                        <i class="trav-phone"></i>
                                                    </span>
                                                    <div class="place-info-label">Business hours:</div>
                                                    <span class="business-hour">Open 24 hours</span>
                                                </li>
                                                <li class="dropdown-item">
                                                    <span class="icon-wrap">
                                                        <i class="trav-set-location-icon"></i>
                                                    </span>
                                                    <div class="place-info-label">Address:</div>
                                                    <span class="smpl-style">Sale, Morocco</span>
                                                </li>
                                            </ul>
                                        </span>
                                <span class="event-tag">Airport</span>
                                <i class="trav-set-location-icon"></i>
                                Tokyo, Japan
                                <i class="trav-get-direction-icon"></i>
                                <a href="#" class="post-name-link">Get directions</a>
                            </div>
                            <div class="post-info">
                                @lang('trip.spent') <b>$240</b>, Stayed <b>@lang('time.count_min', ['count' => 40])</b>
                            </div>
                        </div>
                    </div>
                    <div class="post-top-info-action">
                        <div class="post-action-icon">
                            <i class="trav-bookmark-icon"></i>
                        </div>
                    </div>
                </div>
                <div class="post-content-inner">
                    <div class="post-main-content">
                        <h3>Aenean consequat lobortis</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam doloribus maxime soluta,
                            deserunt ipsam quia dicta, ex dolor rem quibusdam laborum, quidem eos dolorem eum dolore
                            saepe facere aperiam iure.</p>
                        <h3>Heading here</h3>
                        <img src="http://placehold.it/680x270" alt="">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi aliquid rem dolore
                            explicabo exercitationem. Porro recusandae reiciendis labore aliquam laborum?</p>
                    </div>
                    <div class="post-tips">
                        <div class="post-tips-top">
                            <h4 class="post-tips_ttl">Tips about Rabat Sale Airport:</h4>
                        </div>
                        <div class="post-tips_block-wrap">
                            <div class="post-tips_block">
                                <div class="tips-content">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                        architecto vel magnam accusamus nemo odio unde autem, provident eos itaque quas
                                        at, vitae ducimus. Earum magnam quod a tempore.</p>
                                </div>
                                <div class="tips-footer">
                                    <div class="tip-by-info">
                                        <div class="tip-by-img">
                                            <img src="http://placehold.it/25x25" alt="">
                                        </div>
                                        <div class="tip-by-txt">
                                            @lang('other.by') <span class="tag blue">Suzanne</span> <span
                                                    class="date-info">5 days ago</span>
                                        </div>
                                    </div>
                                    <ul class="tip-right-info-list">
                                        <li class="round">
                                            <i class="trav-bookmark-icon"></i>
                                        </li>
                                        <li class="round">
                                            <i class="trav-flag-icon"></i>
                                        </li>
                                        <li class="round blue">
                                            <i class="trav-chevron-up"></i>
                                        </li>
                                        <li class="count">
                                            <span>6</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="post-tips_block">
                                <div class="tips-content">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                        architecto vel magnam accusamus nemo odio unde autem, provident eos itaque quas
                                        at, vitae ducimus. Earum magnam quod a tempore.</p>
                                </div>
                                <div class="tips-footer">
                                    <div class="tip-by-info">
                                        <div class="tip-by-img">
                                            <img src="http://placehold.it/25x25" alt="">
                                        </div>
                                        <div class="tip-by-txt">
                                            @lang('other.by') <span class="link">Diana</span> <span
                                                    class="date-info">1 month ago</span>
                                        </div>
                                    </div>
                                    <ul class="tip-right-info-list">
                                        <li class="round">
                                            <i class="trav-bookmark-icon"></i>
                                        </li>
                                        <li class="round">
                                            <i class="trav-flag-icon"></i>
                                        </li>
                                        <li class="round">
                                            <i class="trav-chevron-up"></i>
                                        </li>
                                        <li class="count">
                                            <span>4</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="more-tips-link">More tips...</a>
                    </div>
                </div>
                <div class="modal-map">
                    <img class="map" src="http://placehold.it/750x330" alt="map">
                    <div class="modal-map_bottom-info">
                        <div class="map-point-ttl">
                            Haneda airport
                        </div>
                        <ul class="post-round-info-list">
                            <li>
                                <div class="map-reaction">
                                            <span class="dropdown">
                                                <img src="./assets2/image/reaction-icon-smile-only.png" alt="smile"
                                                     data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <div class="dropdown-menu dropdown-menu-middle-right dropdown-arrow dropdown-emoji-wrap">
                                                    <ul class="dropdown-emoji-list">
                                                        <li>
                                                            <a href="#" class="emoji-link">
                                                                <div class="emoji-wrap like-icon"></div>
                                                                <span class="emoji-name">@lang('chat.emoticons.like')</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="emoji-link">
                                                                <div class="emoji-wrap haha-icon"></div>
                                                                <span class="emoji-name">@lang('chat.emoticons.haha')</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="emoji-link">
                                                                <div class="emoji-wrap wow-icon"></div>
                                                                <span class="emoji-name">@lang('chat.emoticons.wow')</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="emoji-link">
                                                                <div class="emoji-wrap sad-icon"></div>
                                                                <span class="emoji-name">@lang('chat.emoticons.sad')</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="emoji-link">
                                                                <div class="emoji-wrap angry-icon"></div>
                                                                <span class="emoji-name">@lang('chat.emoticons.angry')</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </span>
                                    <span class="count">6</span>
                                </div>
                            </li>
                            <li>
                                <div class="round-icon">
                                    <i class="trav-comment-icon"></i>
                                </div>
                                <span class="count">3</span>
                            </li>
                            <li>
                                <div class="round-icon">
                                    <i class="trav-light-icon"></i>
                                </div>
                                <span class="count">37</span>
                            </li>
                            <li>
                                <a href="#" class="info-link">
                                    <div class="round-icon">
                                        <i class="trav-share-fill-icon"></i>
                                    </div>
                                    <span>@lang('profile.share')</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<script src="./assets2/js/select2.min.js"></script>
<script src="{{asset('assets2/js/lightbox2/src/js/lightbox.js')}}"></script>

<script src="./assets2/js/posts-script.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".datepicker").datepicker({
            dateFormat: "d MM yy",
            altField: "#post_actual_date",
            altFormat: "yy-mm-dd",
            maxDate: 0,

        });
        $('body').on('click', '.dpSwitch', function (e) {
            $(".datepicker").datepicker("show");
        });
    });
    $(document).ready(function () {
        $('body').on('click', '.post_like_button', function (e) {
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{url('posts/likeunlike')}}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (res.status == 'yes') {


                    } else if (res == 'no') {

                    }
                    console.log('#post_like_count_' + postId);
                    $('#post_like_count_' + postId).html('<a href="#" data-tab="comments' + postId + '"><b>' + result.count + '</b> Likes</a>');
                });
            e.preventDefault();
        });


        $('body').on('submit', '.commentForm', function (e) {
            var values = $(this).serialize();
            $.ajax({
                method: "POST",
                url: "{{url('posts/comment')}}",
                data: values
            })
                .done(function (res) {

                });
            e.preventDefault();
        });

    });
    $('[data-tab]').on('click', function (e) {
        var Item = $(this).attr('data-tab');
        $('[data-content]').css('display', 'none');
        $('[data-content=' + Item + ']').css('display', 'block');
        e.preventDefault()
    });


    $(".check-follow-city").each(function () {
        var city_id = $(this).attr('data-id');
        var item = $(this);
        $.ajax({
            method: "POST",
            url: "{{url('city/')}}/" + city_id + "/check-follow",
            data: {name: "test"}
        })
            .done(function (res) {
                console.log(res);
                if (res.success == true) {
                    item.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_unfollow_city" data-id="' + item.attr('data-id') + '">@lang('buttons.general.unfollow')<</button>');
                } else if (res.success == false) {
                    console.log($(this).attr('data-id'));
                    item.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_follow_city" data-id="' + item.attr('data-id') + '">@lang('buttons.general.follow')</button>');
                }
            });
    });


    $(".check-follow-country").each(function () {
        var country_id = $(this).attr('data-id');
        var item = $(this);
        $.ajax({
            method: "POST",
            url: "{{url('country/')}}/" + country_id + "/check-follow",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    item.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_unfollow_country" data-id="' + item.attr('data-id') + '">@lang('buttons.general.unfollow')<</button>');
                } else if (res.success == false) {
                    //console.log($(this).attr('data-id'));
                    item.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_follow_country" data-id="' + item.attr('data-id') + '">@lang('buttons.general.follow')</button>');
                }
            });
    });

    $(".check-follow-place").each(function () {
        var place_id = $(this).attr('data-id');
        var item = $(this);
        $.ajax({
            method: "POST",
            url: "{{url('place/')}}/" + place_id + "/check-follow",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    item.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_unfollow_place" data-id="' + item.attr('data-id') + '">@lang('buttons.general.unfollow')<</button>');
                } else if (res.success == false) {
                    //console.log($(this).attr('data-id'));
                    item.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_follow_place" data-id="' + item.attr('data-id') + '">@lang('buttons.general.follow')</button>');
                }
            });
    });


    $('body').on('click', '#button_follow_city', function () {
        var city_id = $(this).attr('data-id');
        $.ajax({
            method: "POST",
            url: "{{url('city')}}/" + city_id + "/follow",
            data: {name: "test"}
        })
            .done(function (res) {
                console.log(res);
                if (res.success == true) {
                    $(this).parent.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_unfollow_city" data-id="' + city_id + '">@lang('buttons.general.unfollow')<</button>');
                } else if (res.success == false) {
                    //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                }
            });
    });

    $('body').on('click', '#button_unfollow_city', function () {
        var city_id = $(this).attr('data-id');
        $.ajax({
            method: "POST",
            url: "{{url('city')}}/" + city_id + "/unfollow",
            data: {name: "test"}
        })
            .done(function (res) {
                console.log(res);
                if (res.success == true) {
                    $(this).parent.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_follow_city" data-id="' + city_id + '">@lang('buttons.general.follow')</button>');
                } else if (res.success == false) {
                    //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                }
            });
    });


</script>
<script>
    function timeSince(date) {

        var seconds = Math.floor((new Date() - date) / 1000);

        var interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return interval + " years";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes";
        }
        return Math.floor(seconds) + " seconds";
    }
</script>

<style>

    .thumb {
        margin: 10px 5px 0 0;
        width: 150px;
    }

    video::-webkit-media-controls-fullscreen-button {
        display: block;
    }

    video::-webkit-media-controls-mute-button {
        display: block;
    }

    video::-webkit-media-controls-current-time-display {
        font-size: 11px
    }

    video::-webkit-media-controls-time-remaining-display {
        font-size: 11px
    }

    .locationSelect {
        display: none;
        width: 50%
    }

    .locationLoad {
        display: none;
    }

    .post-alloptions {
        flex: 1;
        display: flex;
        align-items: center;
    }

    .select2-dropdown {
        z-index: 10000;
    }

</style>
@include('site/layouts/footer')
</body>
</html>
