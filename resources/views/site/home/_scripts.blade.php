<script type="text/javascript">
   
    var url = new URL(window.location.href);
    var register_type = 1;
    var search_params = new URLSearchParams(url.search);
    register_type = search_params.get('type')
    var verified_screen = search_params.get('exp-verified-screen')
    var w_through_step = 1;

    
    if (search_params.get('sign-up') === '1') {
        window.history.pushState({}, document.title, "/home");
        $('#walkthrough_step_1').modal('toggle');
        $('#walkthrough_step_1').removeClass('right').addClass('left')
        var badge_name = search_params.get('badge')
        w_through_step++;
    }

    
     if(verified_screen == 1){
        window.history.pushState({}, document.title, "/home");
        $('#walkthrough_verified_screen').modal('toggle');
    }
    
    $(document).ready(function () {
        $(document).on('click', '.w-through-next', function(){
            $('.w-through-left-outside-menu').show();
            if(w_through_step > 2){

                if(badge_name != null && w_through_step == 6){
                    w_through_step = w_through_step + 1;
                    var badge_url_name = badge_name.toLowerCase();
                    var badge_img_url = badge_url_name + '.png';

                    $('#walkthrough_step_7 .walkthrough-step-7-img').attr('src', '{{asset('assets2/image/w-through/')}}/' + badge_img_url)
                    $('#walkthrough_step_7 .wt-badge-name').html(badge_name)
                }

                $('.w-through-left-outside-menu ul li').removeClass('active')
                $('.w-through-left-outside-menu ul li.l-menu-step-'+w_through_step).addClass('active')
            }
            
            $('body').removeAttr('style')
            $('div.modal').modal('hide');

            if(badge_name == null && w_through_step == 7){
                return;
            }

            $('#walkthrough_step_' + w_through_step).modal('toggle');
            $('#walkthrough_step_' + w_through_step).removeClass('right').addClass('left')
            $('body.modal-open').css('overflow', 'hidden')

             w_through_step++;
        })


        $(document).on('click', '.waktrough-skip', function(){
            $('.w-through-left-outside-menu').hide()
        })
        
        $('body').on('click', '.post_likes_modal', function (e) {
            postId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('post.likes4modal') }}",
                data: {post_id: postId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    
                    $('#likesModal').find('.modal-body').html(result);
                    $('#likesModal').modal('show');
                });
            e.preventDefault();
        });
        $('body').on('click', '.post_like_button a', function (e) {
            postId = $(this).attr('id');
            type = $(this).data('type');
            if(type == 'trip'){
                postId = $(this).attr('id');
                $.ajax({
                    url: "{{route('trip.likeunlike')}}",
                    type: "POST",
                    data: { type: type, id: postId},
                    dataType: "json",
                    success: function(resp, status){
                        if(resp.status ='yes'){
                            $('.trip_like_count_' + postId).html('<a href="#usersWhoLike" data-toggle="modal" class="post_like_button" id="' + postId + '" data-id="'+postId+'" data-type="trip"><b>' + resp.count + '</b> Likes</a>');
                        }
                    },
                    error: function(){}
                });
                
            }else if(type == 'report'){
               
                $.ajax({
                    method: "POST",
                    url: "{{ route('report.like') }}",
                    data: {post_id: postId}
                })
                .done(function (res) {
                    var resp = JSON.parse(res);
                    $('.report_like_count_' + postId).html('<a href="#usersWhoLike" data-toggle="modal" class="post_like_button" id="' + postId + '" data-id="'+postId+'" data-type="report"><b>' + resp.count + '</b> Likes</a>');
                });
            }else if(type == 'review'){
                value_vote = $(this).data('value');
                var _this = $(this);
                var vote_type = (value_vote == 1)?0:1;

                $.ajax({
                    method: "POST",
                    url: "{{route('place.revirews_updownvote')}}",
                    data: {review_id: postId, vote_type:value_vote}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if(result.status ='yes')
                            _this.data('value', vote_type)

                            $('#review_like_count_' + postId).html('<a  data-toggle="modal" class="trip_like_button" id="' + postId + '" data-id="'+postId+'" data-value="" data-type="review"><b>' + result.count_upvotes + '</b> Was this helpful?</a>');
                    });
                e.preventDefault();
            }else if(type == 'event'){
                postId = $(this).attr('id');
                $.ajax({
                    method: "POST",
                    url: "{{route('place.eventlike')}}",
                    data: {event_id: postId}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if(result.status ='yes')
                            $('.event_like_count_' + postId).html('<a  data-toggle="modal" class="post_like_button" id="' + postId + '" data-id="'+postId+'" data-value="" data-type="event"><b>' + result.count + '</b> Likes</a>');
                    });
                e.preventDefault();
            }else if(type == 'trip-media'){
                postId = $(this).attr('id');
                $.ajax({
                    method: "POST",
                    url: "{{ route('media.trip-place.likeunlike') }}",
                    data: {media_id: postId}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if(result.status ='yes')
                            $('.trip_place_like_count_' + postId).html('<a  data-toggle="modal" class="post_like_button" id="' + postId + '" data-id="'+postId+'" data-value="" data-type="trip-media"><b>' + result.count + '</b> Likes</a>');
                    });
                e.preventDefault();
            }else{
                $.ajax({
                    method: "POST",
                    url: "{{ route('post.likeunlike') }}",
                    data: {post_id: postId}
                })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (res.status == 'yes') {


                        } else if (res == 'no') {

                        }
                        $('.Post_like_count_' + postId).html('<a href="#usersWhoLike" data-toggle="modal" class="trip_like_button" id="' + postId + '" data-id="'+postId+'" data-type="post"><b>' + result.count + '</b> Likes</a>');
                    });
                e.preventDefault();
            }
        });
        // $('body').on('click', '.post_share_button', function (e) {
        //     postId = $(this).attr('id');
        //     $.ajax({
        //         method: "POST",
        //         url: "{{ route('post.shareunshare') }}",
        //         data: {post_id: postId}
        //     })
        //     .done(function (res) {
        //             var result = JSON.parse(res);
        //             if (result.status == 'yes') {
        //                 $('#shares' + result.rowId).append(result.row);
        //             } else if (result.status == 'no') {
        //                 $('#shareRow' + result.rowId).remove();
        //             }
        //             $('#post_share_count_' + postId).find('strong').html(result.count);
        //         });
        //     e.preventDefault();
        // });

        //Discussion upvote
        // $(document).on('click', '.discussion-vote-link.up', function (e) {
        //     console.log("one")
        //     var _this = $(this);
        //     var discussion_id = $(this).attr('id');
        //     var parent_content = _this.closest('#updown_' + discussion_id)
        //     if(_this.hasClass('disabled')){
        //         is_add = false;
        //     }else{
        //         is_add = true;
        //     }

        //     $.ajax({
        //         method: "POST",
        //         url: "{{route('discussion.updownvote')}}",
        //         data: {discussion_id: discussion_id, type:1, is_add:is_add}
        //     })
        //         .done(function (res) {
        //             $('.upvote-count_' + discussion_id).html(res.count_upvotes)
        //             $('.downvote-count_' + discussion_id).html(res.count_downvotes)

        //             if(_this.hasClass('disabled')){
        //                 _this.removeClass('disabled')
        //             }else{
        //                 _this.addClass('disabled')
        //             }

        //             if(!parent_content.find('.discussion-vote-link.down').hasClass('disabled')){
        //                 parent_content.find('.discussion-vote-link.down').addClass('disabled')
        //             }
        //         });

        //     e.preventDefault()
        // });

        //Discussion downvote
        // $(document).on('click', '.discussion-vote-link.down', function (e) {
        //     var discussion_id = $(this).attr('id');
        //     var parent_content = $('#updown_' + discussion_id)
        //     var _this = parent_content.find('.discussion-vote-link.down');
        //     if(_this.hasClass('disabled')){
        //         is_add = false;
        //     }else{
        //         is_add = true;
        //     }

        //     $.ajax({
        //         method: "POST",
        //         url: "{{route('discussion.updownvote')}}",
        //         data: {discussion_id: discussion_id, type:0, is_add:is_add}
        //     })
        //         .done(function (res) {
        //             $('.upvote-count_' + discussion_id).html(res.count_upvotes)
        //             $('.downvote-count_' + discussion_id).html(res.count_downvotes)

        //             if(_this.hasClass('disabled')){
        //                 _this.removeClass('disabled')
        //             }else{
        //                 _this.addClass('disabled')
        //             }

        //             if(!parent_content.find('.discussion-vote-link.up').hasClass('disabled')){
        //                 parent_content.find('.discussion-vote-link.up').addClass('disabled')
        //             }

        //         });
        //     e.preventDefault()
        // });

        $('body').on('click', '.media_like_button', function (e) {
            mediaId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('media.likeunlike') }}",
                data: {media_id: mediaId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    console.log(result);
                    if (result.status == 'yes') {


                    } else if (result.status == 'no') {

                    }
                    console.log('<a href="#" data-tab="comments' + mediaId + '"><b>' + result.count + '</b> Likes</a>');
                    $('#media_like_count_' + mediaId).html('<a href="#" class="media_like_button" id="' + mediaId + '"><b>' + result.count + '</b> Likes</a>');
                });
            e.preventDefault();
        });


        // todo: this is a temporary solution, in further we need to do this via search
        $('body').on('keyup', '.friend-search input', function (e) {
            var filter = $(this).val();
            $('.friend-search-results-item').each(function() {
                if ($(this).find('.location-title').text().search(new RegExp(filter, "i")) < 0) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        });
    });
    
    //Travel mates join
    $(document).on('click', '.m-join-btn', function (e) {
        var btn_wrap = $(this).parent();
        var id = $(this).attr('request_id');
        $.ajax({
            method: "POST",
            url: "{{ route('travelmate.ajaxjoin') }}",
            data: {request_id: id}
        })
            .done(function (res) {
                if(res == "error")
                {
                    alert("Can't send request.");
                }
                else
                {
                    btn_wrap.html(res);
                }
            });
        e.preventDefault();
    });


    $(".check-follow-city").each(function () {
        var city_id = $(this).attr('data-id');
        var item = $(this);
        $.ajax({
            method: "POST",
            url: "{{url_with_locale('city/')}}/" + city_id + "/check-follow",
            data: {name: "test"}
        })
            .done(function (res) {
                console.log(res);
                if (res.success == true) {
                    if (item.find('button').hasClass('btn-light-primary')) {
                        item.html('<button type="button" class="btn btn-light-primary" id="button_unfollow_city" data-id="' + item.attr('data-id') + '">@lang('buttons.general.unfollow')</button>');
                    } else {
                        item.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_unfollow_city" data-id="' + item.attr('data-id') + '">@lang('buttons.general.unfollow')</button>');
                    }
                } else if (res.success == false) {
                    console.log($(this).attr('data-id'));
                    if (item.find('button').hasClass('btn-light-primary')) {
                        item.html('<button type="button" class="btn btn-light-primary" id="button_follow_city" data-id="' + item.attr('data-id') + '">@lang('buttons.general.follow')</button>');
                    } else {
                        item.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_follow_city" data-id="' + item.attr('data-id') + '">@lang('buttons.general.follow')</button>');
                    }
                }
            });
    });


    $(".check-follow-country").each(function () {
        var country_id = $(this).attr('data-id');
        var item = $(this);
        $.ajax({
            method: "POST",
            url: "{{url_with_locale('country/')}}/" + country_id + "/check-follow",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    if (item.find('button').hasClass('btn-light-primary')) {
                        item.html('<button type="button" class="btn btn-light-primary" id="button_unfollow_country" data-id="' + item.attr('data-id') + '">@lang('buttons.general.unfollow')</button>');
                    } else {
                        item.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_unfollow_country" data-id="' + item.attr('data-id') + '">@lang('buttons.general.unfollow')</button>');
                    }
                } else if (res.success == false) {
                    //console.log($(this).attr('data-id'));
                    if (item.find('button').hasClass('btn-light-primary')) {
                        item.html('<button type="button" class="btn btn-light-primary" id="button_follow_country" data-id="' + item.attr('data-id') + '">@lang('buttons.general.follow')</button>');
                    } else {
                        item.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_follow_country" data-id="' + item.attr('data-id') + '">@lang('buttons.general.follow')</button>');
                    }
                }
            });
    });

    $(".check-follow-place").each(function () {
        console.log(764738947)
        var place_id = $(this).attr('data-id');
        var item = $(this);
        $.ajax({
            method: "POST",
            url: "{{url_with_locale('place/')}}/" + place_id + "/check-follow",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    if (item.find('button').hasClass('btn-light-primary')) {
                        item.html('<button type="button" class="btn btn-light-primary" id="button_unfollow_place" data-id="' + item.attr('data-id') + '">@lang('buttons.general.unfollow')</button>');
                    } else {
                        item.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_unfollow_place" data-id="' + item.attr('data-id') + '">@lang('buttons.general.unfollow')</button>');
                    }
                } else if (res.success == false) {
                    //console.log($(this).attr('data-id'));
                    if (item.find('button').hasClass('btn-light-primary')) {
                        item.html('<button type="button" class="btn btn-light-primary" id="button_follow_place" data-id="' + item.attr('data-id') + '">@lang('buttons.general.follow')</button>');
                    } else {
                        item.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_follow_place" data-id="' + item.attr('data-id') + '">@lang('buttons.general.follow')</button>');
                    }
                }
            });
    });
    
    
    
    $('body').on('click', '#button_follow_place', function () {
        var place_id = $(this).attr('data-id');
        $.ajax({
            method: "POST",
            url: "{{url_with_locale('place')}}/" + place_id + "/follow",
            data: {name: "test"}
        })
            .done(function (res) {
                console.log(res);
                if (res.success == true) {
                    $('#check_follow_place'+place_id).html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_unfollow_place" data-id="' + place_id + '">@lang('buttons.general.unfollow')</button>');
                } else if (res.success == false) {
                    //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                }
            });
    });


    $('body').on('click', '#button_follow_city', function () {
        var city_id = $(this).attr('data-id');
        $.ajax({
            method: "POST",
            url: "{{url_with_locale('city')}}/" + city_id + "/follow",
            data: {name: "test"}
        })
            .done(function (res) {
                console.log(res);
                if (res.success == true) {
                    $(this).parent.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_unfollow_city" data-id="' + city_id + '">@lang('buttons.general.unfollow')</button>');
                } else if (res.success == false) {
                    //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                }
            });
    });

    $('body').on('click', '#button_unfollow_city', function () {
        var city_id = $(this).attr('data-id');
        $.ajax({
            method: "POST",
            url: "{{url_with_locale('city')}}/" + city_id + "/unfollow",
            data: {name: "test"}
        })
            .done(function (res) {
                console.log(res);
                if (res.success == true) {
                    $(this).parent.html('<button type="button" class="btn btn-light-grey btn-bordered" id="button_follow_city" data-id="' + city_id + '">@lang('buttons.general.follow')</button>');
                } else if (res.success == false) {
                    //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                }
            });
    });

    function post_delete(param, type, obj, e) {
        $.confirm({
         title: 'Confirm!',
         content: 'Are you sure you want to delete this post? <div class="mb-3"></div>',
         columnClass: 'col-md-5 col-md-offset-5',
         closeIcon: true,
         offsetTop: 0,
         offsetBottom: 500,
         buttons: {
             cancel: function () {},
             confirm: {
                 text: 'Confirm',
                 btnClass: 'btn-danger',
                 keys: ['enter', 'shift'],
                 action: function(){
                     $.ajax({
                            method: "POST",
                            url: "{{ route('post.delete') }}",
                            data: {post_id: param, post_type: type} 
                        })
                        .done(function(res) {
                            result = JSON.parse(res);
                            if(result.status == "yes")
                            {
                                $(obj).parent().parent().parent().parent().parent().remove();
                            }else{
                                //
                                alert(result.status);
                            }
                        });
                 }
             }
         }
     });

        e.preventDefault();
    }
    
    
</script>
<script>
    function timeSince(date) {

        var seconds = Math.floor((new Date() - date) / 1000);

        var interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return interval + " years";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes";
        }
        return Math.floor(seconds) + " seconds";
    }
</script>


<style>

    .thumb {
        /*margin: 10px 5px 0 0;*/
        /* width: 150px; */
        height: 130px;
    }

    video::-webkit-media-controls-fullscreen-button {
        display: block;
    }

    video::-webkit-media-controls-mute-button {
        display: block;
    }

    video::-webkit-media-controls-current-time-display {
        font-size: 11px
    }

    video::-webkit-media-controls-time-remaining-display {
        font-size: 11px
    }

    .locationSelect {
        display: none;
        width: 50%
    }

    .locationLoad {
        display: none;
    }

    .post-alloptions {
        flex: 1;
        display: flex;
        align-items: center;
    }

    .select2-dropdown {
        z-index: 10000;
    }

    .img-wrap-newsfeed, .plus-icon
    {
        width: 151px;
        height: 130px;
        float: left;
        margin: 10px;
    }

    .img-wrap-newsfeed>div,
    .plus-icon>div
    {

        overflow: hidden;
        justify-content: center;
        display: flex;
    }

    .img-wrap-newsfeed .close
    {
        margin-top: -133px;
        margin-left: 135px;
        position:absolute;
        margin-right: 5px;
        opacity: 1;
    }

    .post-create-input .medias
    {
        margin-top: 2px;
        display: inline-block;
    }

</style>
