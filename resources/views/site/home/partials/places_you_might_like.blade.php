<div class="post-block">
    <div class="post-side-top">
        <h3 class="side-ttl">@lang('home.places_you_might_like') <span class="count">20</span></h3>
        <div class="side-right-control">
            <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
            <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
        </div>
    </div>
    <div class="post-side-inner">
        <div class="post-slide-wrap">
            <ul id="placeYouMightLike" class="post-slider post-slider-active">
                @foreach($top_places as $tplace)
                <li>
                    <div class="image-wrap">
                        <img src="./assets2/image/photos/statueofliberty/statueliberty_julienneschaer-098_ade18569-a8db-2480-d62e197cc6f43b4b.jpg" alt="" style="width:222px;height:151px;">
                    </div>
                    <div class="post-slider-caption transparent-caption">
                        <p class="post-slide-name" href="#">Statue of Liberty</p>
                        <div class="post-slide-description">
                            United States of America
                        </div>
                    </div>
                </li>
                @endforeach
                <li>
                    <div class="image-wrap">
                        <img src="./assets2/image/photos/reichstag/reichstag-buliding-berlin.jpg" alt="" style="width:222px;height:151px;">
                    </div>
                    <div class="post-slider-caption transparent-caption">
                        <p class="post-slide-name" href="#">Reichstag</p>
                        <div class="post-slide-description">
                            Germany
                        </div>
                    </div>
                </li>
                <li>
                    <div class="image-wrap">
                        <img src="./assets2/image/photos/krugernationalpark/Animals_Will_Fight.jpeg" alt="" style="width:222px;height:151px;">
                    </div>
                    <div class="post-slider-caption transparent-caption">
                        <p class="post-slide-name" href="#">Kruger National Park</p>
                        <div class="post-slide-description">
                            South Africa
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="post-map-wrap">
            <img src="./assets2/image/photos/krugernationalpark/kruger.jpg" alt="map">

            <iframe width="600" height="400" src="https://www.mapsdirections.info/en/custom-google-maps/map.php?width=600&height=400&hl=ru&q=New%20York%20City+(Your%20Business%20Name)&ie=UTF8&t=p&z=4&iwloc=B&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.mapsdirections.info/en/custom-google-maps/">Embed Google Map</a> by <a href="https://www.mapsdirections.info/en/">Measure area on map</a></iframe>

            <div class="post-map-info-caption map-black">
                <div class="map-avatar">
                    <img src="http://placehold.it/25x25" alt="ava">
                </div>
                <div class="map-label-txt">@lang('home.when_is_the_best_time_to_visit')</div>
            </div>
        </div>
    </div>
</div>