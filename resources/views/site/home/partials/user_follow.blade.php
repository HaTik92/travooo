<?php
$following = \App\Models\User\User::find($post->variable);
?>
@if(is_object($following) && is_object($post))
    <div class="post-block post-top-bordered">
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap">
                    <img src="{{check_profile_picture($post->user->profile_picture)}}" alt="">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name profile-name">
                        <a class="post-name-link" href="{{url('profile/'.$post->user->id)}}">{{$post->user->name}}</a>
                        {!! get_exp_icon($post->user) !!}
                        @lang('home.started_following')
                        <a href="{{url('profile/'.$following->id)}}" class="post-name-link">{{$following->name}}</a>
                    </div>
                    <div class="post-info-date">
                        4 Hours ago
                    </div>
                </div>
            </div>
            @if(Auth::user()->id && Auth::user()->id!==$post->user->id)
                <div class="post-top-info-action">
                    <div class="dropdown">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="trav-angle-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Userfollow">
                            @include('site.home.partials._info-actions', ['post'=>$post])
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="post-content-inner">
            <div class="post-profile-wrap">
                <div class="post-profile-block">
                    <div class="post-prof-image">
                        <img class="prof-cover"
                             src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S"
                             alt="photo">
                        <a href="#" class="btn btn-light-primary btn-follow">
                            <i class="trav-user-plus-icon"></i>
                        </a>
                    </div>
                    <div class="post-prof-main">
                        <div class="avatar-wrap">
                            <a href="{{url('profile/'.$following->id)}}">
                                <img src="{{check_profile_picture($following->profile_picture)}}" alt="ava">
                            </a>
                        </div>
                        <div class="post-person-info">
                            <div class="prof-name">{{$following->name}}</div>
                            <div class="prof-location">{{$following->display_name}}</div>
                        </div>

                    </div>
                    <div class="drop-bottom-link">
                        <a href="{{url('profile/'.$following->id)}}" class="profile-link">@lang('home.view_profile')</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endif
