<div class="post-block">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-round-icon-wrap">
                <i class="trav-event-icon"></i>
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="#">@lang('home.independence_day')</a>
                </div>
                <div class="post-event-info">
                    <span class="event-tag">@lang('home.national_Holiday')</span>
                    @lang('home.in')
                    <span class="dropdown">
                        <a class="event-place" href="#" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">Unites States of America</a>
                        <div class="dropdown-menu dropdown-info-style post-profile-block location-block">
                            <div class="post-prof-image">
                                <img class="prof-cover"
                                     src="http://www.midtownhotelnyc.com/resourcefiles/homeimages/hilton-garden-inn-new-york-manhattan-midtown-east-home1-top.jpg"
                                     alt="photo">
                            </div>
                            <div class="post-prof-main">
                                <div class="flag-wrap">
                                    <img src="http://placehold.it/153x53?text=flag" alt="ava">
                                </div>
                                <div class="post-person-info">
                                    <div class="prof-name">United States of America</div>
                                    <div class="prof-location">@lang('region.country_in_name', ['name' => 'North America'])</div>
                                    <div class="prof-button-wrap">
                                        <button type="button"
                                                class="btn btn-light-grey btn-bordered">@lang('home.follow')</button>
                                    </div>
                                    <div class="prof-follow-count">@lang('profile.count_followers', ['count' => '28K'])</div>
                                </div>
                                <div class="drop-bottom-link">
                                    <a href="#" class="profile-link">@lang('home.view_profile')</a>
                                </div>
                            </div>
                        </div>
                    </span>
                </div>
            </div>
        </div>
        <div class="post-top-info-action">
            <div class="dropdown">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <i class="trav-angle-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Upcomingholiday">
                    @include('site.home.partials._info-actions')
                </div>
            </div>
        </div>
    </div>
    <div class="post-image-container post-follow-container">
        <ul class="post-image-list">
            <li>
                <img src="./assets2/image/photos/users/Chicago_tour_from_toronto1.jpg" alt="">
            </li>
        </ul>
        <div class="post-follow-block">
            <div class="follow-txt-wrap">
                <div class="date-block">
                    <span class="month blue">jul</span>
                    <span class="count-day">15</span>
                </div>
                <div class="follow-txt">
                    <p class="follow-posted">@lang('home.posted_by') <a href="#">Donec Ultrices Nunc</a></p>
                    <p class="follow-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium,
                        veritatis.</p>
                </div>
            </div>
            <div class="follow-btn-wrap">
                <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right">
                    @lang('home.follow')
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                </button>
            </div>
        </div>
    </div>
    <div class="post-footer-info">
        <div class="post-foot-block post-reaction">
            <img src="./assets2/image/reaction-icon-smile-only.png" alt="smile">
            <span><b>12</b> @lang('other.reactions')</span>
        </div>
        <div class="post-foot-block">
            <i class="trav-comment-icon"></i>
            <ul class="foot-avatar-list">
                <li><img class="small-ava" src="./assets2/image/photos/profiles/78_refashionista_sheri_pavlovic.jpg"
                         alt="ava"></li>
                <li><img class="small-ava" src="./assets2/image/photos/profiles/Randy.jpg" alt="ava"></li>
                <li><img class="small-ava"
                         src="./assets2/image/photos/profiles/Stacy-Headshot-April-2014-closeup-360x360.jpg" alt="ava">
                </li>
            </ul>
            <span>@lang('comment.count_comment', ['count' => 23])</span>
        </div>
    </div>
</div>