<div class="post-block">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-round-icon-wrap">
                <i class="trav-event-icon"></i>
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="#">Quick Chek New Jersey Festival of Ballooning</a>
                </div>
                <div class="post-event-info">
                    <span class="event-tag">@lang('home.event')</span>
                    @lang('home.in') <a class="event-place" href="#">New York City</a>
                </div>
            </div>
        </div>
        <div class="post-top-info-action">
            <div class="dropdown">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="trav-angle-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Upcomingevents">
                    @include('site.home.partials._info-actions')
                </div>
            </div>
        </div>
    </div>
    <div class="post-image-container post-follow-container">
        <ul class="post-image-list">
            <li>
                <img src="./assets2/image/photos/users/ballon.jpg" alt="" style="width:595px;height:350px;">
            </li>
        </ul>
        <div class="post-follow-block">
            <div class="follow-txt-wrap">
                <div class="date-block">
                    <span class="month">jul</span>
                    <span class="count-day">15</span>
                </div>
                <div class="follow-txt">
                    <p class="follow-posted">@lang('home.posted_by') <a href="#">Donec Ultrices Nunc</a></p>
                    <p class="follow-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium, veritatis.</p>
                </div>
            </div>
            <div class="follow-btn-wrap">
                <button type="button" class="btn btn-light-grey btn-bordered">@lang('home.follow')</button>
            </div>
        </div>
    </div>
    <div class="post-footer-info">
        <div class="post-foot-block post-reaction">
            <img src="./assets2/image/reaction-icon-smile-only.png" alt="smile">
            <span><b>6</b> @lang('other.reactions')</span>
        </div>
        <div class="post-foot-block">
            <i class="trav-comment-icon"></i>
            <ul class="foot-avatar-list">
                <li><img class="small-ava" src="./assets2/image/photos/profiles/78_refashionista_sheri_pavlovic.jpg" alt="ava"></li>
                <li><img class="small-ava" src="./assets2/image/photos/profiles/Randy.jpg" alt="ava"></li>
                <li><img class="small-ava" src="./assets2/image/photos/profiles/Stacy-Headshot-April-2014-closeup-360x360.jpg" alt="ava"></li>
            </ul>
            <span>@choice('comment.count_comment', 20, ['count' => 20])</span>
        </div>
    </div>
</div>