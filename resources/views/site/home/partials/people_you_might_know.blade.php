<?php

use App\Models\User\UsersFriends;
use App\Models\UsersFollowers\UsersFollowers;
use Carbon\Carbon;

$topUser = null;
$userType = null;
$usersList = collect([]);

if(Auth::check()){
    
$user = auth()->user();
$friends_ids = $user->user_friends->pluck('friends_id')->toArray();
$followings_ids = $user->following_user->pluck('users_id')->toArray();

$followings = UsersFollowers::whereIn('followers_id', $followings_ids)
    ->whereDate('created_at', '>', Carbon::now()->subDays(3))
    ->with('follower.following_user')
    ->orderByDesc('created_at')
    ->get();

$friends = UsersFriends::whereIn('users_id', $friends_ids)
    ->whereDate('created_at', '>', Carbon::now()->subDays(3))
    ->with('user.following_user')
    ->orderByDesc('created_at')
    ->get();

$lastFollower = $followings->first();
$lastFriend = $friends->first();



if($lastFollower && $lastFriend) {
    if($lastFollower->created_at->gt($followings->last()->created_at)) {
        $topUser = $lastFollower->follower;
        $usersList = $topUser->following_user->map(function($item) { return $item->user; });
        $userType = 'follower';
    } else {
        $topUser = $lastFriend->user;
        $usersList = $topUser->user_friends->map(function($item) { return $item->friend; });
        $userType = 'friend';
    }
} elseif($lastFollower && !$lastFriend) {
    $topUser = $lastFollower->follower;
    $usersList = $topUser->following_user->map(function($item) { return $item->user; });
    $userType = 'follower';
} elseif(!$lastFollower && $lastFriend) {
    $topUser = $lastFriend->user;
    $usersList = $topUser->user_friends->map(function($item) { return $item->friend; });
    $userType = 'friend';
} else {
    $usersList = collect([]);
}

$user = auth()->user();
$myMedias = $user->my_medias;
$myPhotos = $user->my_medias->filter(function($myMedia) {
    return substr(@$myMedia->media->url, -4) != '.mp4';
});

}

?>
@if($topUser)
<div class="post-block post-top-bordered">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <img src="{{check_profile_picture($topUser->profile_picture)}}" alt="{{ $topUser->name }}">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name profile-name">
                    <a class="post-name-link" href="{{url('profile/'.$topUser->id)}}">{{ $topUser->name }}</a>
                    {{ $userType == 'follower' ? 'Started following' : 'Is friend with ' }}
                     <a href="{{url('profile/'.$usersList->first()->id)}}" class="post-name-link">{{ $usersList->first()->name }}</a>

                    @if($usersList->count() > 1)
                    and <a class="post-name-link post-more-people" type="button" data-placement="bottom" data-toggle="popover" data-html="true" data-original-title="" title="">{{ $usersList->count() - 1 }} more people</a>
                    @endif
                    <div id="popover-content-people" style="display: none;">
                        <div class="popover-people-list">
                            <div class="popover-people-list-item">
                                <img src="https://s3.amazonaws.com/travooo-images2/users/profile/219/1594031404_image.png" alt="">
                                <div class="txt">
                                    <a href="">Gerald Stuber</a>
                                    <span>United States</span>
                                </div>
                                <a class="btn btn-light-grey btn-bordered">Follow</a>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="post-info-date">
                    {{ diffForHumans($lastFollower->created_at) }}
                </div>
            </div>
        </div>
    </div>
    <div class="post-content-inner">
        <div class="post-profile-wrap">
            <div class="post-profile-block">
                @php
                    $main_user = $topUser;
                @endphp
                <div class="post-prof-image">
                    <img class="prof-cover" src="{{ check_cover_photo($main_user->cover_photo) }}" alt="photo">
                    <a class="btn btn-light-primary btn-follow">
                        <i class="fas fa-plus-square"></i>
                    </a>
                </div>
                <div class="post-prof-main">
                    <div class="avatar-wrap">
                        <a href="{{url('profile/'.$main_user->id)}}">
                            <img src="{{check_profile_picture($main_user->profile_picture)}}" alt="{{ $main_user->name }}">
                        </a>
                    </div>
                    <div class="post-person-info">
                        <div class="prof-name">{{ $main_user->name }}</div>
                        <div class="prof-location">{{$main_user->display_name}}</div>
                        @php
                            $medias = $main_user->my_medias->filter(function($myMedia) {
                                if(is_object($myMedia->media)) {
                                    return substr($myMedia->media->url, -4) != '.mp4';
                                }
                                return false;
                            })->take(-3)
                        @endphp
                        <ul class="user-photos">
                            @foreach($medias as $media)
                            <li>
                                <img src="{{ check_post_photo($media->media->url) ?: asset('assets2/image/placeholders/no-photo.png') }}" alt="{{ $media->media->author_name }}">
                            </li>
                            @endforeach
                            @for ($i = 0; $i < 3 - $medias->count(); $i++)
                                <li>
                                    <img src="{{asset('assets2/image/placeholders/no-photo.png')}}" alt="image">
                                </li>
                            @endfor
                        </ul>
                    </div>
                </div>
                <div class="drop-bottom-link">
                    <a href="{{url('profile/'.$main_user->id)}}" class="profile-link">View profile</a>
                </div>
            </div>
            @if($usersList->count())
            <div class="post-profile-block-slider-wrapper">
                <div class="lSSlideOuter ">
                    <div class="lSSlideWrapper usingCss" style="transition-duration: 400ms; transition-timing-function: ease;">
                        <div class="post-profile-blocks-slider lightSlider lsGrab lSSlide" style="{height: 374px;}">
                            @foreach($usersList->take(10) as $user_i)
                            <div class="post-profile-block lslide active" style="width: 289px; margin-right: 10px;">
                                <div class="post-prof-image">
                                    <img class="prof-cover" src="{{ check_cover_photo($user_i->cover_photo) }}" alt="photo">
                                    <a class="btn btn-light-primary btn-follow">
                                        <i class="trav-user-plus-icon"></i>
                                    </a>
                                </div>
                                <div class="post-prof-main">
                                    <div class="avatar-wrap">
                                        <a href="{{url('profile/'.$user_i->id)}}">
                                            <img src="{{check_profile_picture($user_i->profile_picture)}}" alt="ava">
                                        </a>
                                    </div>
                                    <div class="post-person-info">
                                        <div class="prof-name">{{$user_i->name}}</div>
                                        <div class="prof-location">{{$user_i->display_name}}</div>
                                        @php
                                            $medias = $user_i->my_medias->filter(function($myMedia) {
                                                if(is_object($myMedia->media)) {
                                                    return substr($myMedia->media->url, -4) != '.mp4';
                                                }
                                                return false;
                                            })->take(-3);
                                        @endphp
                                        <ul class="user-photos">
                                        @foreach($medias as $media)
                                            <li>
                                                <img src="{{ ($media->media->url) ?'https://s3.amazonaws.com/travooo-images2/'.$media->media->url: asset('assets2/image/placeholders/no-photo.png') }}" alt="{{ $media->media->author_name }}">
                                            </li>
                                        @endforeach
                                        @for ($i = 0; $i < 3 - $medias->count(); $i++)
                                            <li>
                                                <img src="{{asset('assets2/image/placeholders/no-photo.png')}}" alt="image">
                                            </li>
                                        @endfor
                                        </ul>
                                    </div>
                                </div>
                                <div class="drop-bottom-link">
                                    <a href="{{url('profile/'.$user_i->id)}}" class="profile-link">View profile</a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endif
<script>
    $(document).ready(function(){
        $('.post-profile-blocks-slider').lightSlider({
            item: 1,
            loop: true,
            slideMargin: 10,
            pager: false,
            controls: true
        });
    })
</script>