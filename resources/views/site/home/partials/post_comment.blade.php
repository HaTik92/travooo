<div class="post-block post-top-bordered">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap post-marked-avatar">
                <img src="http://placehold.it/20x20" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name profile-name post-ellipsis">
                    <a class="post-name-link" href="#">Tom</a>
                    @lang('home.added_a_comment_about_a')
                    <a href="#" class="post-name-link">@lang('profile.photo')</a>
                    @lang('home.in')
                    <a href="#" class="post-name-link"><img src="./assets/image/icon-small-flag.png" alt="flag"> United
                        States of America</a>
                </div>
            </div>
        </div>
        <div class="post-top-info-action">
            <div class="dropdown">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <i class="trav-angle-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment">
                    @include('site.home.partials._info-actions')
                </div>
            </div>
        </div>
    </div>
    <div class="post-content-inner">
        <div class="post-image-container post-follow-container">
            <ul class="post-image-list">
                <li>
                    <img src="http://placehold.it/595x360" alt="">
                </li>
            </ul>
            <div class="post-footer-info post-footer-padded">
                <div class="post-foot-block post-reaction">
                    <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                    <span><b>6</b> @lang('other.reactions')</span>
                </div>
                <div class="post-foot-block">
                    <i class="trav-comment-icon"></i>
                    <ul class="foot-avatar-list">
                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                        -->
                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                        -->
                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                    </ul>
                    <span>@choice('comment.count_comment', 20, ['count' => 20])</span>
                </div>
            </div>
            <div class="follow-comment-wrap comment-on-photo">
                <div class="post-comment-wrapper">
                    <div class="post-comment-row">
                        <div class="post-com-avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-comment-text">
                            <div class="post-com-name-layer">
                                <a href="#" class="comment-name">Tom</a>
                                <a href="#" class="comment-nickname">@tom2011</a>
                            </div>
                            <div class="comment-txt">
                                <p>Class aptent taciti sociosqu ad litora torquent per conubia.</p>
                            </div>
                            <div class="comment-bottom-info">
                                <div class="com-reaction">
                                    <img src="./assets/image/icon-smile.png" alt="">
                                    <span>21</span>
                                </div>
                                <div class="com-time">6 hours ago</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="post-follow-block">
            </div> -->
        </div>
    </div>
</div>