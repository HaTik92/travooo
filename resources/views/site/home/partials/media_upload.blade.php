<?php
use App\Models\Posts\PostsViews;
use Illuminate\Support\Facades\Auth;
$post_object = App\Models\ActivityMedia\Media::find($post->variable);
?>
@if(is_object($post_object))
    <?php
    $pv = new PostsViews;
    $pv->posts_id = $post_object->id;
    $pv->users_id = Auth::guard('user')->user()->id;
    $pv->gender = Auth::guard('user')->user()->gender;
    $pv->nationality = Auth::guard('user')->user()->nationality;
    $pv->save();

    ?>
    <?php
                $file_url = $post_object->url;
                $file_url_array = explode(".", $file_url);
                $ext = end($file_url_array);
                $allowed_video = array('mp4');
                ?>
    <div class="post-block">

        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap">
                    <img src="{{check_profile_picture($post->user->profile_picture)}}" alt="">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link" href="{{url('profile/'.$post->user->id)}}">{{$post->user->name}}</a>
                        {!! get_exp_icon($post->user) !!}
                    </div>
                    <div class="post-info">
                        @lang('home.added_a')
                        @if(in_array($ext, $allowed_video))
                        <b>video</b>
                        @else
                        <b>@lang('profile.photo')</b>
                        @endif
                        {{diffForHumans($post->time)}}
                    </div>
                </div>
            </div>
            <div class="post-top-info-action">
                <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Mediaupload">
                        @if(Auth::user()->id)
                            @if(Auth::user()->id==$post->users_id)
                                <a class="dropdown-item" href="#" onclick="post_delete('{{$post_object->id}}','{{$post->type}}', this, event)">
                                    <span class="icon-wrap">
                                        <img src="{{asset('assets2/image/delete.svg')}}" style="width:20px;">
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>@lang('buttons.general.crud.delete')</b></p>
                                        <p style="color:red">@lang('home.remove_this_post')</p>
                                    </div>
                                </a>
                            @else
                                @include('site.home.partials._info-actions', ['post'=>$post_object])
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="post-image-container">
            @if(isset($post->text) && $post->text!='')
            <div class="post-txt-wrap">
                <p class="post-txt-lg media_more_{{$post->id}}">

                        <?php
                            $showChar = 200;
                            $ellipsestext = "...";
                            $moretext = "see more";
                            $lesstext = "see less";

                            $content = $post->text;

                            if(strlen($content) > $showChar) {

                                $c = substr($content, 0, $showChar);
                                $h = substr($content, $showChar, strlen($content) - $showChar);

                                $html = $c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span>' . $h . '</span>&nbsp;&nbsp;<a href="#" class="morelink">' . $moretext . '</a></span>';

                                $content = $html;
                            }
                        ?>
                        {!!$content!!}

                </p>
            </div>
            @endif
            <ul class="post-image-list">

                @if(in_array($ext, $allowed_video))
{{--                    <div id="player" class="player">--}}

                        <video width="595" controls>
                            <source src="{{$post_object->url}}" type="video/mp4">

                            Your browser does not support the video tag.
                        </video>

{{--                        <div class="vid-quality-selector flex"><button data-index="0" class="active">720p</button><button data-index="1">360p</button><button data-index="2" class="">240p</button></div>--}}
{{--                    </div>--}}

                @else
                <li style="position: relative;width: 595px;min-height: 375px;max-height: 595px;overflow: hidden;">
                    <a href="{{$post_object->url}}" data-lightbox="{{$post_object->url}}">
                        <img src="{{$post_object->url}}" alt=""  style="position: absolute;left: 50%;top: 50%;height: 100%;width: auto;-webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                    </a>
                </li>
                @endif
            </ul>
        </div>

        <div class="post-footer-info pt-2">
            <div class="post-foot-block post-reaction">
            <span class="media_like_button" id="{{$post_object->id}}">
                <a href="#">
                    <img src="{{asset('assets2/image/like.svg')}}" alt="Like this post" style="width:16px;height:16px;margin:5px;vertical-align:baseline;">
                </a>
            </span>
                <span id="media_like_count_{{$post_object->id}}">
                    <a href='{{ route('media.showlikes', $post_object->id) }}' class="media_like_button" id='{{$post_object->id}}' rel="modal:open">
                        <b>{{count($post_object->likes)}}</b> @lang('home.likes')
                    </a>
                </span>
            </div>
            <div class="post-foot-block">
                <a href="#" data-tab="comments{{$post_object->id}}">
                    <i class="trav-comment-icon"></i>
                </a>

                <ul class="foot-avatar-list"></ul>
                <span>
                    <a href='#' data-tab='comments{{$post_object->id}}'><span class="{{$post_object->id}}-media-comments-count">{{count($post_object->comments)}}</span> @lang('comment.comments')</a>
                </span>
            </div>
        </div>



        <div class="post-comment-layer" data-content="comments{{$post_object->id}}" style="display:none;">
                <div class="post-comment-top-info {{$post_object->id}}-media-comment-info">
                    <ul class="comment-filter">
                        <li onclick="commentSort('Top', this)">@lang('comment.top')</li>
                        <li class="active" onclick="commentSort('New', this)">@lang('comment.new')</li>
                    </ul>
                    <div class="comm-count-info">
                        <strong>0</strong> / <span class="{{$post_object->id}}-media-comments-count">{{count($post_object->comments)}}</span>
                    </div>
                </div>
            <div class="post-comment-wrapper sortBody" id="commentsContainer{{$post_object->id}}">
                @if(count($post_object->comments)>0)
                    @foreach($post_object->comments AS $comment)
                        @include('site.home.partials.media_comment_block')
                    @endforeach
                @endif
            </div>

            @if(Auth::user())
                <div class="post-add-comment-block">
                    <div class="avatar-wrap">
                        <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                             style="width:45px;height:45px;">
                    </div>
                    <div class="post-add-com-inputs">
                        <form class="mediacommentForm{{$post_object->id}}" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" id="pair{{$post_object->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                            <div class="post-create-block post-comment-create-block post-regular-block pb-2" id="createPostBlock" tabindex="0">
                                <div class="post-create-input p-create-input b-whitesmoke ">
                                    <textarea name="text" id="text{{$post_object->id}}" class="textarea-customize post-comment-emoji"
                                        style="display:inline;vertical-align: top;min-height:50px;"  placeholder="@lang('comment.write_a_comment')"></textarea>
                                </div>

                                <div class="post-create-controls b-whitesmoke d-none">
                                    <div class="post-alloptions">
                                        <ul class="create-link-list">
                                            <li class="post-options">
                                                <input type="file" name="file[]" id="mediacommentfile{{$post_object->id}}" style="display:none" multiple>
                                                <i class="fa fa-camera click-target" data-target="mediacommentfile{{$post_object->id}}"></i>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="comment-edit-action">
                                        <a href="javascript:;" class="p-comment-cancel-link media-cancel-link">Cancel</a>
                                        <a href="javascript:;" class="p-comment-link media-comment-link-{{$post_object->id}}">Post</a>
                                    </div>

                                </div>
                                 <div class="medias p-media b-whitesmoke"></div>
                            </div>
                            <input type="hidden" name="post_id" value="{{$post_object->id}}"/>
                            <button type="submit" class="btn btn-primary d-none"></button>
                        </form>

                    </div>
                </div>
            @endif
        </div>

    </div>
    <script>
    $(document).ready(function(){
        document.emojiSource = "{{ asset('plugins/summernote-master/tam-emoji/img') }}";
        document.textcompleteUrl = "{{ url('reports/get_search_info') }}";
        CommentEmoji($('.mediacommentForm{{$post_object->id}}'));
        
        $(document).on('click', '.media-cancel-link', function(){
            var form = $(this).closest('form');
            form.find('.post-create-controls').addClass('d-none')
            form.find('.emojionearea-button').addClass('d-none');
            form.find('textarea').val('');
            form.find(".note-editable").html('');
            form.find(".note-placeholder").addClass('custom-placeholder')
            form.find('.medias').find('.removeFile').each(function(){
                          $(this).click();
                      });
            form.find('.medias').empty();

        })
         
        $('#mediacommentfile{{$post_object->id}}').on('change', function(){

            var commentform = $(".mediacommentForm{{$post_object->id}}");
            if((commentform.find('.medias').children().length + $(this)[0].files.length) > 10)
            {
                alert("The maximum number of files to upload is 10.");
                $(this).val("");
                return;
            }
            if (window.File && window.FileReader && window.FileList && window.Blob){ //check File API supported browser
                commentform.find('textarea').html('');

                var data = $(this)[0].files;
                $.each(data, function(index, file){ //loop though each file

                    var upload = new Upload(file);
                    var uid = Math.floor(Math.random()*1000000);
                    if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){

                        var fRead = new FileReader();
                        fRead.onload = (function(file){
                            return function(e) {

                                var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element

                                var button = $('<span/>').addClass('close').addClass('removeFile').click(function(){     //create close button element
                                    var filename = upload.getName();
                                    commentFileDelete(filename, this, $('#pair{{$post_object->id}}').val());
                                }).append('<span>&times;</span>');

                                var imgitem = $('<div>').attr('uid', uid).append(img); //create Image Wrapper element
                                imgitem = $('<div/>').addClass('img-wrap-newsfeed').append(imgitem).append(button);
                                commentform.find('.medias').append(imgitem);
                                upload.doUpload(uid, commentform.find('#pair{{$post_object->id}}').val());
                            };
                        })(file);
                        fRead.readAsDataURL(file); //URL representing the file's data.

                    } else if(/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type)){

                        var fRead = new FileReader();
                        fRead.onload = (function(file){
                            return function(e) {

                                var video = $('<video style="object-fit:cover;" width="151" height="130" controls>'+'<source src='+e.target.result+' type="video/mp4">'+ '<source src="movie.ogg" type="video/ogg">'+ '</video>');

                                var button = $('<span/>').addClass('close').addClass('removeFile').click(function(){     //create close button element
                                    var filename = upload.getName();
                                    commentFileDelete(filename, this, $('#pair{{$post_object->id}}').val());
                                }).append('<span>&times;</span>');

                                var videoitem = $('<div/>').attr('attr', uid).append(video);
                                videoitem = $('<div/>').addClass('img-wrap-newsfeed').append(videoitem).append(button);
                                commentform.find('.medias').append(videoitem);
                                upload.doUpload(uid, commentform.find('#pair{{$post_object->id}}').val());
                            };
                        })(file);
                        fRead.readAsDataURL(file); //URL representing the file's data.

                    }
                });
                 commentform.find('textarea').focus();
                $(this).val("");

            }else{
                alert("Your browser doesn't support File API!"); //if File API is absent
            }

        });

        $('body').on('click', '.media-comment-link-{{$post_object->id}}', function (e) {
            $('.mediacommentForm{{$post_object->id}}').find('button[type=submit]').click();
        });
        

        $('body').on('submit', '.mediacommentForm{{$post_object->id}}', function (e) {
            var form = $(this);
            var text = form.find('textarea').val().trim();
            var files = form.find('.medias').find('div').length;

            var postId = form.find('input[name="post_id"]').val();
            var url = '{{ route("media.comment", ":postId") }}';
            url = url.replace(':postId', postId);

            var media_comments_count = $('.'+ postId +'-media-comments-count').html();

            if(text == "" && files == 0)
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: '',
                    type: 'orange',
                    offsetTop: 0,
                    offsetBottom: 500,
                    content: '<span style="font-size:17px;">Please input text or select files!</span>',
                });
                return false;
            }
         
            var values = $(this).serialize();
            $.ajax({
                method: "POST",
                url: url,
                data: values
            })
                .done(function (res) {
                    $('#commentsContainer{{$post_object->id}}').prepend(res);
                    if(media_comments_count == 0){
                        $('.'+ postId +'-media-comment-info').removeAttr('style')
                    }
                   form.find('.medias').find('.removeFile').each(function(){
                            $(this).click();
                        });

                    form.find('.medias').empty();
                    form.find('textarea').val('');
                    form.find(".note-editable").html('');
                    form.find(".note-placeholder").addClass('custom-placeholder')
                    form.find('.post-create-controls').addClass('d-none');
                    form.find('.emojionearea-button').addClass('d-none');
                     
                    $('.'+ postId +'-media-comments-count').html(parseInt(media_comments_count) + 1)

                    Comment_Show.reload(form.closest('.post-block'));
                });
            e.preventDefault();
        });

        $(".media_more_{{$post->id}}").find(".morelink").click(function(){
            var moretext = "see more";
            var lesstext = "see less";
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    });
    </script>

    <style>
    .textarea-customize
    {
        border: 1px solid #dcdcdc;
        border-radius: 3px;
        resize: none;
    }
    .input_customize
    {
        border: 1px solid #dcdcdc;
        background-color: white!important;
    }
</style>
@endif
