<?php
$city = \App\Models\City\Cities::find($post->variable);
?>
@if(is_object($city))
    <div class="post-block">
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap">
                    <img src="{{check_profile_picture($post->user->profile_picture)}}" alt="">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link" href="{{url('profile/'.$post->user->id)}}">{{$post->user->name}}</a>
                        {!! get_exp_icon($post->user) !!}
                    </div>
                    <div class="post-info">
                        @lang('home.started_following') <a href="{{ route('city.index', $city->id) }}"
                                                           class="link-place">{{$city->transsingle->title}}</a> {{diffForHumans($post->time)}}
                    </div>
                </div>
            </div>
            @if ($post->user->id !== Auth::user()->id)
                <div class="post-top-info-action">
                    <div class="dropdown">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="trav-angle-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Cityfollow">
                            @include('site.home.partials._info-actions', ['post'=>$post])
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="post-image-container post-follow-container">
            <ul class="post-image-list">
                <li>
                    <img src="https://s3.amazonaws.com/travooo-images2/th700/{{@$city->getMedias[0]->url}}" alt="">
                </li>
            </ul>
            <div class="post-follow-block">
                <div class="follow-txt-wrap">
                    <div class="follow-flag-wrap">
                    </div>
                    <div class="follow-txt">
                        <p class="follow-name">{{$city->transsingle->title}}</p>
                        <div class="follow-foot-info">
                            <i class="trav-talk-icon icon-grey-comment"></i>
                            <span @if(count($city->followers)>0) style="cursor: pointer" data-toggle="modal" data-target="#cityfollowers_{{$city->id}}" @endif>@lang('home.count_following_this', ['count' => count($city->followers)])</span>
                        </div>
                    </div>
                </div>
                <div class="follow-btn-wrap check-follow-city" data-id='{{$city->id}}'>

                </div>
            </div>
        </div>
    </div>

    @if(count($city->followers)>0)
    <div class="modal fade" tabindex="-1" role="dialog" id="cityfollowers_{{$city->id}}" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">@lang('home.count_following_this', ['count' => count($city->followers)])</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="post-comment-wrapper">
                    @foreach($city->followers as $follower)
                    <div class="post-comment-row">
                        <div class="post-com-avatar-wrap">
                            <img src="{{check_profile_picture($follower->user->profile_picture)}}" alt="">
                            <a class="comment-name">&nbsp;&nbsp;&nbsp;{{$follower->user->name}}</a>
                            {!! get_exp_icon($follower->user) !!}
                        </div>
                        <div class="post-comment-text">
                            <div class="post-com-name-layer">
                                <!-- <a href="#" class="comment-name"></a> -->
                                <!--<a href="#" class="comment-nickname">@katherin</a>-->
                            </div>
                            <div class="comment-bottom-info">
                                <!-- <div class="com-time">3 minutes ago</div> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            </div>
        </div>
    </div>
    @endif


@endif
