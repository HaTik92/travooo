<?php
    $post_type = $post->type;
    $discussion = \App\Models\Discussion\Discussion::find($post->variable);
?>

@if(isset($discussion) AND is_object($discussion))
<div class="post-block post-top-bordered">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap ava-50">
                <img src="{{check_profile_picture($discussion->author->profile_picture)}}"
                                            alt="" style="width:50px;height:50px;">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link"
                        href="{{ url_with_locale('profile/'.$discussion->author->id)}}">{{$discussion->author->name}}</a>
                    {!! get_exp_icon($discussion->author) !!}
                </div>
                <div class="post-info">
                    posted a discussion about
                    <a
                            href="{{url_with_locale(strtolower($discussion->destination_type)."/".$discussion->destination_id)}}"
                            class="link-place">{{getDiscussionDestination($discussion->destination_type, $discussion->destination_id)}}</a>
                    on {{diffForHumans($discussion->created_at)}}
                </div>
            </div>
        </div>
        @if($discussion->author->id !== Auth::user()->id)
            <div class="post-top-info-action">
                <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion">
                        @include('site.home.partials._info-actions', ['post'=>$discussion])
                    </div>
                </div>
            </div>
        @endif

    </div>
    <div class="topic-inside-panel-wrap">
        <div class="topic-inside-panel">
            <div class="panel-txt {{count(@$discussion->media)>0?'w-75':'w-100'}}">
                <h3 class="panel-ttl">
                    <a href="{{route('discussion.index').'#d-'.$discussion->id}}" dddddd="">{!! str_limit($discussion->question, 25, ' ...') !!}</a>
                </h3>
                <p style="overflow-wrap: break-word;">
                    {!! str_limit($discussion->description, 100, ' ...') !!}</p>
            </div>
            @if(count(@$discussion->media)>0)
                <div class="panel-img disc-panel-img">
                    <img src="{{@@$discussion->media[0]->media_url}}"
                         alt="" style="width:120px;height:120px;">
                </div>
            @endif
        </div>
    </div>

    <div class="post-tips-top-layer post-topic-wrapper">
        @if(count($discussion->replies) > 0)
            <div class="post-tips-top">
                @if($discussion->type==1)
                    <h4 class="post-tips-ttl">@lang('discussion.strings.top_recommendations_by_users')</h4>
                @elseif($discussion->type==2)
                    <h4 class="post-tips-ttl">@lang('discussion.strings.top_tips_by_users')</h4>
                @elseif($discussion->type==3)
                    <h4 class="post-tips-ttl">@lang('discussion.strings.top_planning_tips_by_users')</h4>
                @elseif($discussion->type==4)
                    <h4 class="post-tips-ttl">@lang('discussion.strings.top_answers_by_users')</h4>
                @endif
            </div>
            <div class="post-tips-main-block">
                @foreach($discussion->replies()->orderBy('num_upvotes', 'desc')->get() AS $reply)
                    @if ($loop->first)
                        <div class="post-tips-row">
                            <div class="tips-top">
                                <div class="tip-avatar">
                                    <img src="{{check_profile_picture($reply->author->profile_picture)}}"
                                            alt="" style="width:25px;height:25px;">
                                </div>
                                <div class="tip-content ov-wrap">
                                    <div class="top-content-top">
                                        <a href="{{url_with_locale('profile/'.$reply->author->id)}}"
                                            class="name-link">{{$reply->author->name}}</a>
                                        {!! get_exp_icon($reply->author) !!}
                                        <span>@lang('discussion.strings.said')</span><span
                                                class="dot"> · </span>
                                        <span>{{weatherTime($reply->created_at)}}</span>
                                        @if ($reply->author->id !== Auth::user()->id)
                                            <div class="d-inline-block pull-right">
                                                <div class="post-top-info-action">
                                                    <div class="dropdown">
                                                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                                                                aria-haspopup="true" aria-expanded="false">
                                                            <i class="trav-angle-down"></i>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="DiscussionReplies">
                                                            @include('site.home.partials._info-actions', ['post'=>$reply])
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="tip-txt">
                                        <p>{!!$reply->reply!!}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="tips-footer updownvote" id="{{$reply->id}}">


                            </div>
                        </div>
                    @endif

                @endforeach
            </div>
        @endif
    </div>

</div>
@endif
