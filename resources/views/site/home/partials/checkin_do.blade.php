<?php
use Illuminate\Support\Facades\Hash;$post_type = $post->type;
$post_action = $post->action;
if($post->action == 'update') {
    $post = $post->activityLog;
}

$post_object = $post->post;

$post_followers = null;

$media_array = [];
if(isset($post_object->medias)){
    foreach($post_object->medias as $media){
        $media_array[] =$media->medias_id;
    }
    $media_array = App\Models\ActivityMedia\Media::whereIn('id',$media_array)->get();
}

if(isset($post_object->checkin[0]->place) && is_object($post_object->checkin[0]->place)) {
    $place = $post_object->checkin[0]->place;
    $place_route = route('place.index', $place->id);
} else if(isset($post_object->checkin[0]->city) && is_object($post_object->checkin[0]->city)) {
    $place = $post_object->checkin[0]->city;
    $place_route = route('city.index', $place->id);
} elseif(isset($post_object->checkin[0]->country) && is_object($post_object->checkin[0]->country)) {
    $place = $post_object->checkin[0]->country;
    $place_route = route('country.index', $place->id);
}

$checkin_time = strtotime($post_object->checkin[0]->checkin_time);
$checkin_created = strtotime($post_object->checkin[0]->created_at);

if($checkin_time == $checkin_created){
    $time_checking = date('H:i', $checkin_created);
    $today_flag = true;
    $info_title = ' Is checking-in <a href="'.$place_route.'"
                           class="link-place">'.$place->transsingle->title.'</a>
'.' Today at '.$time_checking;
}else if($checkin_time > $checkin_created){
    $info_title = ' Added a future check-in <a href="'.$place_route.'"
                           class="link-place">'.$place->transsingle->title.'</a>
'.diffForHumans($post_object->checkin[0]->created_at);
}else{
    $info_title = ' Checked-in to <a href="'.$place_route.'"
                           class="link-place">'.$place->transsingle->title.'</a>
'.diffForHumans($post_object->checkin[0]->created_at);
}

if(strtotime(date('Y-m-d',$checkin_time)) == strtotime(date('Y-m-d'))){
    $top_time_checking = date('H:i', $checkin_time);
    $today_flag = true;
    $top_title = ' is checking-in <a href="'.$place_route.'"
                           class="link-place">'.$place->transsingle->title.'</a>
'.' Today at '.$top_time_checking;
}else{
    $top_title = ' checked-in to <a href="'.$place_route.'"
                           class="link-place">'.$place->transsingle->title.'</a>
'.diffForHumans($post_object->checkin[0]->checkin_time);
}

?>
@if(is_object($post_object))
<div class="post-block">
    @if($post_action == 'update')
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-info-txt">
                    <div class="post-info">
                        <a class="post-name-link link-place" href="{{($post->user->id == Auth::user()->id)?url('profile/'):url('profile/'.$post->user->id)}}">{{$post->user->name}}</a>
                        {!! get_exp_icon($post->user) !!}{!! $top_title !!}
                    </div>
                </div>
            </div>
        </div>

        <div href="#" style="border-radius: 10px; border: 1px solid #ebebeb; padding: 12px">
    @endif

    @include('site.home.partials.checkin_do_content', ['post' => $post, 'post_object' => $post_object])

    @if($post_action == 'update')
        </div>
    @endif
    <div class="post-footer-info">
        <div class="post-foot-block post-reaction" onclick="checkinlike({{ $post_object->checkin[0]->id }}, this)">
            <span>
                <a href="#">
                    <i class="trav-heart-fill-icon"></i>
                </a>
            </span>
            <span id="checkin_like_count_{{$post_object->checkin[0]->id}}"><a href="#"><b>{{count($post_object->checkin[0]->likes)}}</b> Likes</a></span>
        </div>
        <div class="post-foot-block">
            <a href="#" data-tab="checkincomments{{$post_object->checkin[0]->id}}">
                <i class="trav-comment-icon"></i>
            </a>

            <ul class="foot-avatar-list"></ul>
            <span><a href="#" data-tab="checkincomments{{$post_object->checkin[0]->id}}">{{count($post_object->checkin[0]->comments)}} Comments</a></span>
        </div>
    </div>
    <div class="post-comment-layer" data-content="checkincomments{{$post_object->checkin[0]->id}}" style='display:none;'>

        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <li onclick="commentSort('Top', this)">@lang('comment.top')</li>
                <li class="active" onclick="commentSort('New', this)">@lang('home.new')</li>
            </ul>
            <div class="comm-count-info">
            </div>
        </div>
        <div class="post-comment-wrapper sortBody" id="checkincomments{{$post_object->checkin[0]->id}}">
            @if(count($post_object->checkin[0]->comments)>0)
                @foreach($post_object->checkin[0]->comments AS $comment)


                    @include('site.home.partials.checkin_do_comment_block')
                @endforeach
            @endif
        </div>
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-input">
                <form class="checkincomments{{$post_object->checkin[0]->id}}" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" id="pair{{$post->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                    <div class="post-block post-create-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" id="checkincommentstext{{$post_object->checkin[0]->id}}" class="textarea-customize"
                                style="display:inline;vertical-align: top;height:40px;" placeholder="@lang('comment.write_a_comment')"></textarea>
                            <div class="medias">
                                <!-- <div class="plus-icon" style="display: none;">
                                    <div>
                                        <span class="close" onclick="javascript:$('#commentfile').click();"><span style="font-size:110px;">+</span></span>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <!-- <li class="post-options">
                                        <input type="file" name="file[]" id="commentfile{{$post->id}}" style="display:none" multiple>
                                        <i class="trav-camera click-target" data-target="commentfile{{$post->id}}"></i>
                                    </li> -->
                                </ul>
                            </div>

                            <button type="submit" class="btn btn-primary">@lang('buttons.general.send')</button>

                        </div>

                    </div>
                    <input type="hidden" name="post_id" value="{{$post_object->checkin[0]->id}}"/>
                </form>

                </div>
            </div>
            <script>

                $('#checkincommentstext{{$post_object->checkin[0]->id}}').keydown(function(e){
                    if(e.keyCode == 13 && !e.shiftKey){
                        e.preventDefault();
                    }
                });
                $('#checkincommentstext{{$post_object->checkin[0]->id}}').keyup(function(e){
                    if(e.keyCode == 13 && !e.shiftKey){
                        $('.checkincomments{{$post_object->checkin[0]->id}}').find('button[type=submit]').click();
                        // $('.checkincomments{{$post_object->checkin[0]->id}}').submit();
                    }
                });
                $('body').on('submit', '.checkincomments{{$post_object->checkin[0]->id}}', function (e) {
                    var form = $(this);
                    var text = form.find('textarea').val().trim();
                    // var files = $('.tripscomment{{$post->id}}').find('.medias').find('div').length;

                    if(text == "")
                    {
                        // alert('Please input text or select files.');
                        return false;
                    }
                    var values = form.serialize();
                    form.find('button[type=submit]').attr('disabled', true);
                    $.ajax({
                        method: "POST",
                        url: "{{ route('post.checkincomment') }}",
                        data: values
                    })
                        .done(function (res) {
                            $('#checkincomments{{$post_object->checkin[0]->id}}').append(res);
                            // $('.tripscomment{{$post->id}}').find('.medias').empty();
                            form.find('textarea').val('');
                            form.find('button[type=submit]').removeAttr('disabled');
                        });
                    e.preventDefault();
                });
            </script>
        @endif
    </div>
</div>

@if(is_object($post_followers) && count($post_followers)>0)
    <div class="modal fade" tabindex="-1" role="dialog" id="checkinfollowers_{{$post_object->id}}" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">{{(is_object($post_followers)) ? count($post_followers) : ''}} @lang('home.following_this')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="post-comment-wrapper">
                    @foreach($post_followers as $follower)
                    <div class="post-comment-row">
                        <div class="post-com-avatar-wrap">
                            <img src="{{check_profile_picture($follower->user->profile_picture)}}" alt="">
                            <a class="comment-name">&nbsp;&nbsp;&nbsp;{{$follower->user->name}}</a>
                            {!! get_exp_icon($follower->user) !!}
                        </div>
                        <div class="post-comment-text">
                            <div class="post-com-name-layer">
                                <!-- <a href="#" class="comment-name"></a> -->
                                <!--<a href="#" class="comment-nickname">@katherin</a>-->
                            </div>
                            <div class="comment-bottom-info">
                                <!-- <div class="com-time">3 minutes ago</div> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            </div>
        </div>
    </div>
    @endif
@endif

<script type="module">

    // import {getLinkPreview} from 'link-preview-js';
    // console.log(getLinkPreview);

    $(document).ready(function(){
        $(document).on('click', ".read-more-link", function () {
            $(this).closest('.post-txt-wrap').find('.less-content').hide()
            $(this).closest('.post-txt-wrap').find('.more-content').show()
            $(this).hide()
        });
        $(document).on('click', ".read-less-link", function () {
            $(this).closest('.more-content').hide()
            $(this).closest('.post-txt-wrap').find('.less-content').show()
            $(this).closest('.post-txt-wrap').find('.read-more-link').show()
        });
    })
</script>
