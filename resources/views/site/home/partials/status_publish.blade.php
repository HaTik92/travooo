<?php
use Illuminate\Support\Facades\Auth;
use App\Models\Posts\PostsViews;
$post_object = App\Models\Posts\Posts::find($post->variable);
?>
@if(is_object($post_object))
    <?php
    $pv = new PostsViews;
    $pv->posts_id = $post_object->id;
    $pv->users_id = Auth::guard('user')->user()->id;
    $pv->gender = Auth::guard('user')->user()->gender;
    $pv->nationality = Auth::guard('user')->user()->nationality;
    $pv->save();

    ?>
    <div class="post-block">
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap">
                    <img src="{{check_profile_picture($post->user->profile_picture)}}" alt="">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link" href="{{url('profile/'.$post->user->id)}}">{{$post->user->name}}</a>
                        {!! get_exp_icon($post->user) !!}
                    </div>
                    <div class="post-info">
                        @lang('home.updated_his') <b>@lang('home.status')</b> {{diffForHumans($post->time)}}
                    </div>
                </div>
            </div>
            <div class="post-top-info-action">
                <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Statuspublish">
                        @if(Auth::user()->id)
                            @if(Auth::user()->id && Auth::user()->id==$post->users_id)
                            <a class="dropdown-item" href="#">
                                <span class="icon-wrap">
                                    <!--<i class="trav-flag-icon-o"></i>-->
                                </span>
                                <div class="drop-txt">
                                    <p><b>@lang('buttons.general.crud.delete')</b></p>
                                    <p style="color:red">@lang('home.remove_this_post')</p>
                                </div>
                            </a>
                            @else
                                @include('site.home.partials._info-actions', ['post'=>$post_object])
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="post-image-container">
            {!!$post_object->text!!}
        </div>
        <div class="post-footer-info">
            <div class="post-foot-block post-reaction">
                <img src="./assets2/image/reaction-icon-smile-only.png" alt="smile">
                <span><b>6</b> @lang('other.reactions')</span>
            </div>
            <div class="post-foot-block">
                <i class="trav-comment-icon"></i>
                <ul class="foot-avatar-list">
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                    <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                    <li class="dropdown">
                    <span data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="small-ava" src="http://placehold.it/20x20" alt="ava">
                    </span>
                        <div class="dropdown-menu dropdown-info-style post-profile-block">
                            <div class="post-prof-image">
                                <img class="prof-cover"
                                     src="http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1450820945/Godafoss-Iceland-waterfall-winter-SPIRIT1215.jpg?itok=hvUl-l-S"
                                     alt="photo">
                            </div>
                            <div class="post-prof-main">
                                <div class="avatar-wrap">
                                    <img src="./assets2/image/photos/profiles/sue-perez.jpg" alt="ava">
                                </div>
                                <div class="post-person-info">
                                    <div class="prof-name">Sue Perez</div>
                                    <div class="prof-location">United States of America</div>
                                </div>
                                <div class="drop-bottom-link">
                                    <a href="#" class="profile-link">@lang('home.view_profile')</a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <span>@choice('comment.count_comment', 20, ['count' => 20])</span>
            </div>
        </div>
        <div class="post-comment-layer">
            <div class="post-comment-top-info">
                <ul class="comment-filter">
                    <li class="active">@lang('comment.top')</li>
                    <li>@lang('comment.new')</li>
                </ul>
                <div class="comm-count-info">
                    3 / 20
                </div>
            </div>
            <div class="post-comment-wrapper">
                <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                        <img src="./assets2/image/photos/profiles/katherine.jpg" alt="">
                    </div>
                    <div class="post-comment-text">
                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Katherin</a>
                            <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel.
                                Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                            <div class="com-reaction">
                                <img src="./assets2/image/icon-smile.png" alt="">
                                <span>21</span>
                            </div>
                            <div class="com-time">6 hours ago</div>
                        </div>
                    </div>
                </div>
                <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                        <img src="./assets2/image/photos/profiles/amine.jpg" alt="">
                    </div>
                    <div class="post-comment-text">
                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Amine</a>
                            <a href="#" class="comment-nickname">@ak0117</a>
                        </div>
                        <div class="comment-txt">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                        </div>
                        <div class="comment-bottom-info">
                            <div class="com-reaction">
                                <img src="./assets2/image/icon-like.png" alt="">
                                <span>19</span>
                            </div>
                            <div class="com-time">6 hours ago</div>
                        </div>
                    </div>
                </div>
                <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                        <img src="./assets2/image/photos/profiles/katherine.jpg" alt="">
                    </div>
                    <div class="post-comment-text">
                        <div class="post-com-name-layer">
                            <a href="#" class="comment-name">Katherin</a>
                            <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel.
                                Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                            <div class="com-reaction">
                                <img src="./assets2/image/icon-smile.png" alt="">
                                <span>15</span>
                            </div>
                            <div class="com-time">6 hours ago</div>
                        </div>
                    </div>
                </div>
                <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
            </div>
        </div>
    </div>
@endif
