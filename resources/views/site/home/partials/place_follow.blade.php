<?php
$place = \App\Models\Place\Place::find($post->variable);
?>
@if(is_object($place))
    <div class="post-block">
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap">
                    <img src="{{check_profile_picture($post->user->profile_picture)}}" alt="">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link" href="{{url('profile/'.$post->user->id)}}">{{$post->user->name}}</a>
                        {!! get_exp_icon($post->user) !!}
                    </div>
                    <div class="post-info">
                        @lang('home.started_following') <a href="{{ route('place.index', $place->id) }}"
                                                           class="link-place">{{$place->transsingle->title}}</a> {{diffForHumans($post->time)}}
                    </div>
                </div>
            </div>
            @if(Auth::user()->id && Auth::user()->id!==$post->user->id)
                <div class="post-top-info-action">
                    <div class="dropdown">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="trav-angle-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Placefollow">
                            @include('site.home.partials._info-actions', ['post'=>$post])
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="post-image-container post-follow-container">
            <ul class="post-image-list">
                <li>
                    <img class="lazy" data-src="https://s3.amazonaws.com/travooo-images2/th700/{{@$place->getMedias[0]->url}}" alt="">
                </li>
            </ul>
            <div class="post-follow-block">
                <div class="follow-txt-wrap">
                    <div class="follow-flag-wrap"></div>
                    <div class="follow-txt">
                        <p class="follow-name">
                            <a href='{{route('place.index', $place->id) }}'>
                                {{$place->transsingle->title}}
                            </a>
                        <div class="follow-tag-wrap">
                            <span class="follow-tag">{{do_placetype($place->place_type)}}</span>
                        </div>
                        </p>
                        <div class="follow-foot-info" style="padding-top:8px">
                            <span @if(count($place->followers)>0) style="cursor: pointer" data-toggle="modal" data-target="#followersmodal_{{$place->id}}" @endif>{{count($place->followers)}} @lang('home.following_this_place')</span>
                        </div>
                    </div>
                </div>
                <div class="follow-btn-wrap check-follow-place" id="check_follow_place{{$place->id}}" data-id='{{$place->id}}'>

                </div>
            </div>
        </div>

    </div>

    @if(count($place->followers)>0)
    <div class="modal fade" tabindex="-1" role="dialog" id="followersmodal_{{$place->id}}" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">{{count($place->followers)}} @lang('home.following_this_place')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="post-comment-wrapper">
                    @foreach($place->followers as $follower)
                    <div class="post-comment-row">
                        <div class="post-com-avatar-wrap">
                            <img src="{{check_profile_picture($follower->user->profile_picture)}}" alt="">
                            <a class="comment-name">&nbsp;&nbsp;&nbsp;{{$follower->user->name}}</a>
                            {!! get_exp_icon($follower->user) !!}
                        </div>
                        <div class="post-comment-text">
                            <div class="post-com-name-layer">
                                <!-- <a href="#" class="comment-name"></a> -->
                                <!--<a href="#" class="comment-nickname">@katherin</a>-->
                            </div>
                            <div class="comment-bottom-info">
                                <!-- <div class="com-time">3 minutes ago</div> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            </div>
        </div>
    </div>
    @endif

@endif
