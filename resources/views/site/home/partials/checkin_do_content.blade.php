<div class="post-top-info-layer">
    <div class="post-top-info-wrap">
        <div class="post-top-avatar-wrap">
            <img src="{{check_profile_picture($post->user->profile_picture)}}" alt="">
        </div>
        <div class="post-top-info-txt">
            <div class="post-top-name">
                <a class="post-name-link" href="{{($post->user->id == Auth::user()->id)?url('profile/'):url('profile/'.$post->user->id)}}">{{$post->user->name}}</a>
                {!! get_exp_icon($post->user) !!}
            </div>
            <div class="post-info">
                {!! $info_title !!}
            </div>
        </div>
    </div>
    <div class="post-top-info-action">
        <div class="dropdown">
            <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="trav-angle-down"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Checkin">
                @if(Auth::user()->id)
                    @if(Auth::user()->id==$post_object->users_id)
                        <a class="dropdown-item" href="#" onclick="post_delete('{{$post_object->id}}','{{$post_type}}', this, event)">
                            <span class="icon-wrap">
                                <img src="{{asset('assets2/image/delete.svg')}}" style="width:20px;">
                            </span>
                            <div class="drop-txt">
                                <p><b>@lang('buttons.general.crud.delete')</b></p>
                                <p style="color:red">@lang('home.remove_this_post')</p>
                            </div>
                        </a>
                    @else
                        @include('site.home.partials._info-actions', ['post'=>$post_object])
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>
<div class="post-txt-wrap">
    <div>
            <span class="less-content disc-ml-content">{!! substr(strip_tags($post_object->text),0, 123) !!}
                @if(strlen($post_object->text)>123)<span
                        class="moreellipses">...</span><a href="javascript:;"
                                                          class="read-more-link">More</a>@endif</span>
        <span class="more-content disc-ml-content" style="display: none;">{!! $post_object->text !!}<a href="javascript:;"
                                                                                                       class="read-less-link">Less</a></span>
    </div>
</div>
<div class="check-in-point d-block">
    <i class="trav-set-location-icon"></i> <strong>{{@$post_object->checkin[0]->location}}</strong>
    <i class="trav-clock-icon"></i>{{diffForHumans($post_object->checkin[0]->checkin_time)}}
</div>
<div class="post-image-container">
@php
    $post_counter= 0;
@endphp
@if(count($media_array) == 1)
    @php
        $post_media = $media_array[0];
        $file_url = $post_media->url;
        $file_url_array = explode(".", $file_url);
        $ext = end($file_url_array);
        $allowed_video = array('mp4');
    @endphp
    <!-- New element -->
        <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;">
            @if(in_array($ext, $allowed_video))
                <li style="padding: 0;position: relative;margin:0 0 0 0;overflow: hidden;">
                    <video width="100%" height="auto" controls="">
                        <source src="{{$post_media->url}}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                    <a href="javascript:;" class="v-play-btn play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                </li>
            @else
                <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;">
                    <a href="https://s3.amazonaws.com/travooo-images2/{{$post_media->url}}" data-lightbox="media__post210172">
                        <img src="https://s3.amazonaws.com/travooo-images2/{{$post_media->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                    </a>
                </li>
            @endif
        </ul>
    @elseif(count($media_array) == 2)
        <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;">
            @foreach ($media_array as $post_media)
                @php
                    $file_url = $post_media->url;
                    $file_url_array = explode(".", $file_url);
                    $ext = end($file_url_array);
                    $allowed_video = array('mp4');
                @endphp
                @if(in_array($ext, $allowed_video))
                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                        <video width="100%"  >
                            <source src="{{$post_media->url}}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        <a href="javascript:;" class="v-play-btn "><i class="fa fa-play" aria-hidden="true"></i></a>
                    </li>
                @else
                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                        <a href="{{$post_media->url}}" data-lightbox="media__post199366">
                            <img src="{{$post_media->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                        </a>
                    </li>
                @endif
            @endforeach


        </ul>
    @elseif(count($media_array)  >2)
        <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;">
        @foreach ($media_array as $post_media)
            @php
                $file_url = $post_media->url;
                $file_url_array = explode(".", $file_url);
                $ext = end($file_url_array);
                $allowed_video = array('mp4');
                $post_counter++;
            @endphp
            <!-- New element -->
                @if(in_array($ext, $allowed_video))
                    @if( $post_counter  <=3)
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                            <video width="100%"  >
                                <source src="{{$post_media->url}}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </li>
                    @endif
                @else
                    @if( $post_counter  <=3)
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                            @if(count($media_array)>3 &&  $post_counter ==3)
                                <a class="more-photos" href="" data-lightbox="media__post199366">{{count($media_array) -3 . ' More Photos'}}</a>
                            @endif
                            <a  href="{{$post_media->url}}" data-lightbox="media__post199366">
                                <img src="{{$post_media->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                            </a>
                        </li>
                    @endif
                @endif
            <!-- New element END -->
            @endforeach
        </ul>
    @endif
</div>