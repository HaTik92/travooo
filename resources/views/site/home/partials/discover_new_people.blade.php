<div class="post-block">
    <div class="post-side-top">
        <h3 class="side-ttl">@lang('home.discover_new_people')</h3>
        <div class="side-right-control">
            <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
            <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
        </div>
    </div>
    <div class="post-side-inner">
        <div class="post-slide-wrap slide-hide-right-margin">
            <ul id="newPeopleDiscover" class="post-slider">
                @foreach($new_people AS $new_user)
                <li class="post-follow-card">
                    <div class="follow-card-inner">
                        <div class="image-wrap">
                            <img class="lazy" data-src="{{check_profile_picture($new_user->profile_picture)}}" alt="">
                        </div>
                        <div class="post-slider-caption">
                            <p class="post-card-name">{{$new_user->name}}</p>
                            {!! get_exp_icon($new_user) !!}
                            <p class="post-card-spec">{{$new_user->display_name}}</p>
                            <button type="button" class="btn btn-light-grey btn-bordered">@lang('home.follow')</button>
                            <p class="post-card-follow-count">@lang('profile.count_followers', ['count' => '12K'])</p>
                        </div>
                    </div>
                </li>
                @endforeach

            </ul>
        </div>
    </div>
</div>
