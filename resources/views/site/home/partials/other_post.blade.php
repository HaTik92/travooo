<?php

$following = \App\Models\User\User::find($me->id);

$followers = [];
foreach($following->get_followers as $f):
    $followers[] = $f->followers_id;
endforeach;
$internal = false;

$act_log = $post;
$post_type = $post->type;
$post = \App\Models\Posts\Posts::find($act_log->variable);

$url = strip_tags($post->text);

$given_domain = parse_url($url)['host'];
$internal_domain = request()->getHttpHost();

if($given_domain == $internal_domain) {
    $internal = true;
}

$ch = curl_init();

curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

$data = curl_exec($ch);
curl_close($ch);

// Load HTML to DOM Object
$dom = new DOMDocument();

@$dom->loadHTML(mb_convert_encoding($data, 'HTML-ENTITIES', 'UTF-8'));

// Parse DOM to get Title
$nodes = $dom->getElementsByTagName('title');
if(count($nodes)) {
    $title = $nodes->item(0)->nodeValue;
} else {
    $title = '';
}

// Parse DOM to get Meta Description
$metas = $dom->getElementsByTagName('meta');
$body = "";
for ($i = 0; $i < $metas->length; $i ++) {
    $meta = $metas->item($i);
    if ($meta->getAttribute('name') == 'description') {
        $body = $meta->getAttribute('content');
    }
}

// Parse DOM to get Images
$image_urls = array();
$images = $dom->getElementsByTagName('img');

for ($i = 0; $i < $images->length; $i ++) {
    $image = $images->item($i);
    $src = $image->getAttribute('src');

    if(filter_var($src, FILTER_VALIDATE_URL)) {
        $image_urls[] = $src;
    }
}

$output = [
    'title' => $title,
    'image_src' => count($image_urls) ? $image_urls[0] : 'https://travooo.com/assets2/image/placeholders/photo.png',
    'body' => $body
];

?>
@if(isset($post))
<div class="post-block" id="link_{{$post->id}}">
   
    @php
        $uniqueurl = url('external/' . $post->author->username, $post->id);
    @endphp
    <a href="{{$uniqueurl}}" class="unique--link"></a>
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap ava-50">
                <img src="{{check_profile_picture(@$post->author->profile_picture)}}" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{url('profile/'.@$post->author->id)}}">{{@$post->author->name}}</a>
                    {!! get_exp_icon($post->author) !!}
                </div>
                <div class="post-info">
                    <a target="_blank" class="post-title-link">
                        Posted a <strong>link</strong>
                        on {{diffForHumans($post->created_at)}} <i class="trav-globe-icon"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="post-top-info-action" dir="auto" >
            <div class="dropdown" dir="auto">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"      aria-expanded="false" dir="auto">
                    <i class="trav-angle-down" dir="auto"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                    @if(Auth::user()->id != $post->author->id)
                        @if(in_array($post->author->id,$followers))
                            <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $post->author->id}}" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-user-plus-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Unfollow {{ $post->author->name ?? 'User' }}</b></p>
                                    <p dir="auto">Stop seeing posts from {{ $post->author->name ?? 'User' }}</p>
                                </div>
                            </a>
                        @endif
                    @endif
                    <a class="dropdown-item {{!Auth::check()? 'open-login':''}}" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$post->id}}" data-toggle="modal" data-id="{{$post->id}}" data-type="text">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Share</b></p>
                            <p dir="auto">Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item copy-newsfeed-link" data-text="" data-link="{{$uniqueurl}}">
                        <span class="icon-wrap">
                            <i class="trav-link" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Copy Link</b></p>
                            <p dir="auto">Paste the link anywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{!Auth::check()? 'open-login':''}}" href="#sendToFriendModal" data-id="link_{{$post->id}}" data-toggle="modal" data-target="" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-on-travo-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                            <p dir="auto">Share with your friends</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{!Auth::check()? 'open-login':''}}" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$post->id}},this)" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-flag-icon-o" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Report</b></p>
                            <p dir="auto">Help us understand</p>
                        </div>
                    </a>
                    @if(Auth::user()->id == $post->author->id)
                <a class="dropdown-item" data-toggle="modal" data-type="{{$post_type}}" data-element = "link_{{$post->id}}" href="#deletePostNew" data-id="{{$post->id}}">
                            <span class="icon-wrap">
                            <i class="far fa-trash-alt"></i>
                            </span>
                            <div class="drop-txt">
                                <p><b style="color: red;">Delete</b></p>
                                <p>Delete this post forever</p>
                            </div>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="topic-inside-panel-wrap p-0">
        <div class="topic-inside-panel d-inline-flex w-100" style="word-break: break-word;">
            @if(!$internal)
                <div class="panel-txt">
                    <h3 class="panel-ttl">
                        @if($output['title'])
                            <a target="_blank"  href="{{ $url }}">{!! str_limit($output['title'], 50, ' ...') !!}<i class="trav-link-out-icon"></i></a>
                        @endif
                    </h3>
                    <div class="post-txt-wrap">
                        <div>
                            <span class="less-content disc-ml-content">{{substr($output['body'],0, 100) }}
                                @if(strlen($output['body'])>100)<span
                                        class="moreellipses">...</span><a href="javascript:;"
                                                                          class="read-more-link">More</a>@endif</span>
                            <span class="more-content disc-ml-content" style="display: none;">{!! $output['body'] !!}<a href="javascript:;" class="read-less-link">Less</a></span>
                        </div>
                    </div>
                </div>
                <div class="panel-img disc-panel-img">
                    <img src="{{ $output['image_src'] }}" alt="" style="width: 130px;
                    height: 140px;">
                </div>
            @else
                <a href="{{ strip_tags($post->text) }}">{{ strip_tags($post->text) }}<i class="trav-link-out-icon" style="color: #999999; font-size: 14px; margin-left: 10px;"></i></a>
            @endif
        </div>
    </div>
    @php
    $variable = $post->id;
    $flag_liked  =false;
        if(count($post->likes)>0){
            foreach($post->likes as $like){
                if($like->users_id == Auth::user()->id)
                    $flag_liked = true;
            }
        }
    @endphp
<div class="post-footer-info">
   @include('site.home.new.partials._comments-posts', ['post'=>$post, 'post_type'=>'text'])
</div>
<div class="post-comment-wrapper" id="following{{$post->id}}">
    @if(count($post->comments)>0)
        @foreach($post->comments AS $comment)
            @if(!user_access_denied($comment->author->id))
                @php
                if(!in_array($comment->users_id, $followers))
                    continue;
                @endphp
                 @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
            @endif
        @endforeach
    @endif
</div>
<div class="post-comment-layer" data-content="comments{{$post->id}}" style='display:none;'>

    <div class="post-comment-top-info">
        <ul class="comment-filter">
            <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
            <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
        </ul>
        <div class="comm-count-info">
            <strong class="{{$post->id}}-opened-comments-count">{{count($post->comments) > 3? 3 : count($post->comments)}}</strong> / <span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span>
        </div>
    </div>
    <div class="ss post-comment-wrapper sortBody" id="comments{{$post->id}}">
        @if(count($post->comments)>0)
            @foreach($post->comments()->take(3)->get() AS $comment)
                @if(!user_access_denied($comment->author->id))
                    @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                @endif
            @endforeach
        @endif
    </div>
     @if(count($post->comments)>3)
        <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="post" data-id="{{$variable}}">Load more...</a>
    @endif

        @include('site.home.partials.new-comment_form_block', ['post_type'=>'post', 'post_id'=>$post->id])
</div>
</div>

<script type="module">

    // import {getLinkPreview} from 'link-preview-js';
    // console.log(getLinkPreview);

    $(document).ready(function(){
        $(document).on('click', ".read-more-link", function () {
            $(this).closest('.post-txt-wrap').find('.less-content').hide()
            $(this).closest('.post-txt-wrap').find('.more-content').show()
            $(this).hide()
        });
        $(document).on('click', ".read-less-link", function () {
            $(this).closest('.more-content').hide()
            $(this).closest('.post-txt-wrap').find('.less-content').show()
            $(this).closest('.post-txt-wrap').find('.read-more-link').show()
        });
    })
</script>
@endif
