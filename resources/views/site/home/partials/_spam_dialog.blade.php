<!-- Spam Report Box Strat -->
<div class="modal fade spam-report-modal" tabindex="-1" role="dialog" id="spamReportDlg" aria-hidden="true" style="z-index:1051">
    <div class="modal-dialog  modal-custom-style" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="side-ttl">Report Post</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-target="#spamReportDlg">
                    <i class="trav-close-icon"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-check">
                        <i class="fa fa-exclamation-circle report-exclamation" aria-hidden="true"></i>
                        <h5 class="d-inline-block">What is the problem with this post?</h5>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container">
                                    <div class="radio-title-group">
                                        <div class="input-container">
                                            <input id="spam" class="radio-button" type="radio" name="spam_post_type" value="0" checked="checked"/>
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    {{ trans('labels.backend.spam_reports.spam') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="fake-news" class="radio-button" type="radio" name="spam_post_type" value="2"/>
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    {{ trans('labels.backend.spam_reports.fake_news') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="harassment" class="radio-button" type="radio" name="spam_post_type" value="3"/>
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    {{ trans('labels.backend.spam_reports.harassment') }}                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="hate-speech" class="radio-button" type="radio" name="spam_post_type" value="4"/>
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    {{ trans('labels.backend.spam_reports.hate_speech') }}                                               </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="nudity" class="radio-button" type="radio" name="spam_post_type" value="5"/>
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    {{ trans('labels.backend.spam_reports.nudity') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="terrorism" class="radio-button" type="radio" name="spam_post_type" value="6"/>
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    {{ trans('labels.backend.spam_reports.terrorism') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="violence" class="radio-button" type="radio" name="spam_post_type" value="7"/>
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    {{ trans('labels.backend.spam_reports.violence') }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="other" class="radio-button" type="radio" name="spam_post_type" value="1"/>
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    {{ trans('labels.backend.spam_reports.other') }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control report-span-input" id="spamText"  placeholder="Something Else" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="send-report-btn" id="spamSend">@lang('buttons.general.send')</button>
            </div>
            <input type="hidden" name="dataid" id="dataid">
            <input type="hidden" name="posttype" id="posttype">
        </div>
    </div>

</div>
<!-- Spam Report Box End -->

<script>
    // report spam......
    
    $("#spamReportDlg").on('hidden.bs.modal', function(event){
        var modal = $(this);
        modal.find("#dataid").val('');
        modal.find("#posttype").val('');
        modal.find("input[name=spam_post_type]")[0].click();
        modal.find("#spamText").val("");
    });
    $("#spamReportDlg").find("input[type=radio]").click(function(){
            $("#spamReportDlg").find("#spamText").css("display", "block") ;
    });
    $("#spamSend").click(function(){
        var modal = $(this).closest("#spamReportDlg");
        if(modal.find("input[name=spam_post_type]:checked").val() == 1 && modal.find("#spamText").val() == "")
        {
            alert("Please input some text.");
            return;
        }
        var data = {
            type: modal.find("input[name=spam_post_type]:checked").val(),
            text: modal.find("#spamText").val(),
            data_id: modal.find("#dataid").val(),
            post_type: modal.find("#posttype").val()
        };
        $.ajax({
            url: "{{route('post.spam')}}",
            type: "POST",
            data: data,
            dataType: "json",
            success: function(data, status){

                if(data == 'Thanks for your report.'){
                    var icon = 'fa fa fa-check';
                    var alert_type = 'green';
                }else{
                    var icon = 'fa fa-warning';
                    var alert_type = 'orange';
                }

                modal.find(".close").click();
                $.alert({
                    icon: icon,
                    title: '',
                    type: alert_type,
                    offsetTop: 0,
                    offsetBottom: 500,
                    scrollToPreviousElement:false,
                    content: data,
                });
            },
            error: function(){}
        });
    });
</script>
