<?php
use App\Models\Posts\PostsViews;
$post_type = $post->type;
$post = \App\Models\Posts\Posts::find($post->variable);

$following = \App\Models\User\User::find($me->id);
$followers = [];
foreach($following->get_followers as $f):
    $followers[] = $f->followers_id;
endforeach;
?>
@if(is_object($post))
    <?php
    $pv = new PostsViews;
    $pv->posts_id = $post->id;
    $pv->users_id = Auth::guard('user')->user()->id;
    $pv->gender = Auth::guard('user')->user()->gender;
    $pv->nationality = Auth::guard('user')->user()->nationality;
    $pv->save();

    ?>
    <div class="post-block">
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap">
                    <a class="post-name-link" href="{{url('profile/'.$post->author->id)}}">
                        <img src="{{check_profile_picture($post->author->profile_picture)}}" alt="">
                    </a>
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link"
                           href="{{url('profile/'.$post->author->id)}}">{{$post->author->name}}</a>
                        {!! get_exp_icon($post->author) !!}
                    </div>
                    <div class="post-info">
                        {{diffForHumans($post->created_at)}}
                    </div>
                </div>
            </div>
            <div class="post-top-info-action">
                <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post">
                        @if(Auth::user()->id )
                            @if(Auth::user()->id && Auth::user()->id==$post->users_id)
                                <a class="dropdown-item" href="#" onclick="post_delete('{{$post->id}}','Post', this, event)">
                                    <span class="icon-wrap">
                                        <!--<i class="trav-flag-icon-o"></i>-->
                                    </span>
                                    <div class="drop-txt">
                                        <p><b>@lang('buttons.general.crud.delete')</b></p>
                                        <p style="color:red">@lang('home.remove_this_post')</p>
                                    </div>
                                </a>
                            @else
                                @include('site.home.partials._info-actions', ['post'=>$post])
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="post-image-container">
            @if(isset($post->text) && $post->text!='')
            <div class="post-txt-wrap">
                    <?php

                        $dom = new \DOMDocument;
                        @$dom->loadHTML($post->text);
                        $elements = @$dom->getElementsByTagName('div');
                        $elem_content = '';
                        $text = $post->text;

                        if(count($elements)> 0){
                            $elem_content = @$elements[0]->nodeValue;
                        }

                        $showChar = 200;
                        $ellipsestext = "...";
                        $moretext = "see more";
                        $lesstext = "see less";

                        if(strlen($elem_content) > $showChar) {
                            $c = substr($elem_content, 0, $showChar);

                            $html = '<div class="post-moreless-block"><div class="post-less-block">'.$c . '<span class="moreellipses"> ' . $ellipsestext . '&nbsp;</span><a href="javascript:;" class="post-more-link">' . $moretext . '</a></div>'
                                    . '<div class="post-more-block d-none">'. $text. '<a href="javascript:;" class="post-less-link">' . $lesstext . '</a></div></div>';

                        }else{
                            $html = $text;
                        }

                         $content = $html;
                    ?>
                    {!!$content!!}
            </div>
            @endif

            @if(is_object($post->medias) && count($post->medias) >= 4)

                @foreach($post->medias AS $photo)
                    @php
                        $file_url = $photo->media->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                        if($loop->index > 3) {
                            echo "<a href='".$file_url."' data-lightbox='media__post".$post->id."' style='display:none'></a>";
                            continue;
                        }
                    @endphp
                    @if($loop->index % 3 == 1 || $loop->index == 0)
                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                    @endif
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden; @if($loop->index == 0) height:395px;@else height:200px; @endif padding:0;">
                            @if(in_array($ext, $allowed_video))
                            <video width="595" controls>
                                <source src="{{$file_url}}" type="video/mp4">

                                Your browser does not support the video tag.
                            </video>
                            @else
                            <a href="{{$file_url}}" data-lightbox="media__post{{$post->id}}">
                                <img src="{{$file_url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                            </a>
                            @endif

                            @if($loop->index == 3 && count($post->medias) > 4)
                                <div href="{{$file_url}}" data-lightbox="media__post{{$post->id}}" style="position: absolute;top: 0px;bottom: 0px;left: 0px;right: 0px;background-color: rgba(0,0,0,0.5);color: white;font-weight: bold;display: flex;align-items: center;justify-content: center;">+{{count($post->medias) - 4}} Photos</div>
                            @endif
                        </li>
                    @if($loop->index % 3 == 0)
                    </ul>
                    @endif
                @endforeach



            @elseif(is_object($post->medias) && count($post->medias) == 3)
                @foreach($post->medias AS $photo)
                    @php
                        $file_url = $photo->media->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                    @endphp
                    @if($loop->index % 2 == 1 || $loop->index == 0)
                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                    @endif
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden; @if($loop->index == 0) height:395px;@else height:200px; @endif ">
                            @if(in_array($ext, $allowed_video))
                            <video width="595" controls>
                                <source src="{{$file_url}}" type="video/mp4">

                                Your browser does not support the video tag.
                            </video>
                            @else
                            <a href="{{$file_url}}" data-lightbox="media__post{{$post->id}}">
                                <img src="{{$file_url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                            </a>
                            @endif

                        </li>
                    @if($loop->index % 2 == 0)
                    </ul>
                    @endif
                @endforeach

            @elseif(is_object($post->medias) && count($post->medias) == 2)
                @foreach($post->medias AS $photo)
                    @php
                        $file_url = $photo->media->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                    @endphp

                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;">
                            @if(in_array($ext, $allowed_video))
                            <video width="595" controls>
                                <source src="{{$file_url}}" type="video/mp4">

                                Your browser does not support the video tag.
                            </video>
                            @else
                            <a href="{{$file_url}}" data-lightbox="media__post{{$post->id}}">
                                <img src="{{$file_url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                            </a>
                            @endif

                        </li>
                    </ul>
                @endforeach

            @elseif(is_object($post->medias) && count($post->medias) == 1)
                @foreach($post->medias AS $photo)
                    @php
                        $file_url = $photo->media->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                    @endphp

                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;min-height: 375px;max-height:595px;">
                            @if(in_array($ext, $allowed_video))
                            <video width="595" controls>
                                <source src="{{$file_url}}" type="video/mp4">

                                Your browser does not support the video tag.
                            </video>
                            @else
                            <a href="{{$file_url}}" data-lightbox="media__post{{$post->id}}">
                                <img src="{{$file_url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                            </a>
                            @endif

                        </li>
                    </ul>
                @endforeach


            @endif
        </div>
        @include('site.home.partials._post_footer')

        <div class="post-comment-wrapper" id="following{{$post->id}}">
            @if(count($post->comments)>0)
                @foreach($post->comments AS $comment)
                    @php
                    if(!in_array($comment->users_id, $followers))
                        continue;
                    @endphp
                    @include('site.home.partials.post_comment_block')
                @endforeach
            @endif
        </div>


        <div class="post-comment-layer" data-content="comments{{$post->id}}" style='display:none;'>

                <div class="post-comment-top-info">
                    <ul class="comment-filter">
                        <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                        <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
                    </ul>
                    <div class="comm-count-info">
                        <strong>0</strong> / <span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span>
                    </div>
                </div>
                <div class="post-comment-wrapper sortBody" id="comments{{$post->id}}">
                    @if(count($post->comments)>0)
                        @foreach($post->comments AS $comment)
                            @php
                            if(in_array($comment->users_id, $followers))
                                continue;
                            @endphp
                            @include('site.home.partials.post_comment_block')
                        @endforeach
                    @endif
                </div>
            @if(Auth::user())
                <div class="post-add-comment-block">
                    <div class="avatar-wrap">
                        <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                             style="width:45px;height:45px;">
                    </div>
                    <div class="post-add-com-inputs">
                    <form class="commentForm{{$post->id}}" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" id="pair{{$post->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                        <div class="post-create-block post-comment-create-block post-regular-block pb-2" tabindex="0">
                            <div class="post-create-input p-create-input b-whitesmoke">
                                <textarea name="text" id="text{{$post->id}}" class="textarea-customize post-comment-emoji"
                                    style="display:inline;vertical-align: top;min-height:50px;" placeholder="@lang('comment.write_a_comment')"></textarea>
                            </div>
                            <div class="post-create-controls  b-whitesmoke d-none">
                                <div class="post-alloptions">
                                    <ul class="create-link-list">
                                        <li class="post-options">
                                            <input type="file" name="file[]" id="commentfile{{$post->id}}" style="display:none" multiple>
                                            <i class="fa fa-camera click-target" aria-hidden="true" data-target="commentfile{{$post->id}}"></i>
                                        </li>
                                    </ul>
                                </div>
                                
                                <div class="comment-edit-action">
                                        <a href="javascript:;" class="p-comment-cancel-link post-r-cancel-link">Cancel</a>
                                        <a href="javascript:;" class="p-comment-link post-r-comment-link-{{$post->id}}" data-p_id="{{$post->id}}">Post</a>
                                </div>
                            </div>
                             <div class="medias p-media b-whitesmoke"></div>
                        </div>
                        <input type="hidden" name="post_id" value="{{$post->id}}"/>
                        <button type="submit" class="btn btn-primary d-none"></button>
                    </form>

                    </div>
                </div>
            @endif
        </div>
        <div class="post-comment-layer" data-content="shares{{$post->id}}" style='display:none;'>
            <div class="post-comment-wrapper" id="shares{{$post->id}}">
                @if(count($post->shares)>0)
                    @foreach($post->shares AS $share)
                        <div class="post-comment-row" id="shareRow{{$share->id}}">
                            <div class="post-com-avatar-wrap">
                                <img src="{{check_profile_picture($share->author->profile_picture)}}" alt="">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">{{$share->author->name}}</a>
                                    {!! get_exp_icon($share->author) !!}
                                    <!--<a href="#" class="comment-nickname">@katherin</a>-->
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-time">{{diffForHumans($share->created_at)}}</div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                @endif
            </div>
        </div>
    </div>


<script>
    $(document).ready(function(){
        document.emojiSource = "{{ asset('plugins/summernote-master/tam-emoji/img') }}";
        document.textcompleteUrl = "{{ url('reports/get_search_info') }}";
        CommentEmoji($('.commentForm{{$post->id}}'));
        
        $(document).on('click', '.post-r-cancel-link', function(){
            var form = $(this).closest('form');
            form.find('.post-create-controls').addClass('d-none')
            form.find('.emojionearea-button').addClass('d-none');
            form.find('textarea').val('');
            form.find(".note-editable").html('');
            form.find(".note-placeholder").addClass('custom-placeholder')
            form.find('.medias').find('.removeFile').each(function(){
                          $(this).click();
                      });
            form.find('.medias').empty();

        })
        
        
        $('#commentfile{{$post->id}}').on('change', function(){

            var commentform = $(".commentForm{{$post->id}}");
            if((commentform.find('.medias').children().length + $(this)[0].files.length) > 10)
            {
                alert("The maximum number of files to upload is 10.");
                $(this).val("");
                return;
            }
            if (window.File && window.FileReader && window.FileList && window.Blob){ //check File API supported browser
                commentform.find('textarea').html('');

                var data = $(this)[0].files;
                $.each(data, function(index, file){ //loop though each file

                    var upload = new Upload(file);
                    var uid = Math.floor(Math.random()*1000000);
                    if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){

                        var fRead = new FileReader();
                        fRead.onload = (function(file){
                            return function(e) {

                                var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element

                                var button = $('<span/>').addClass('close').addClass('removeFile').click(function(){     //create close button element
                                    var filename = upload.getName();
                                    commentFileDelete(filename, this, $('#pair{{$post->id}}').val());
                                }).append('<span>&times;</span>');

                                var imgitem = $('<div>').attr('uid', uid).append(img); //create Image Wrapper element
                                imgitem = $('<div/>').addClass('img-wrap-newsfeed').append(imgitem).append(button);
                                commentform.find('.medias').append(imgitem);
                                upload.doUpload(uid, $('#pair{{$post->id}}').val());
                            };
                        })(file);
                        fRead.readAsDataURL(file); //URL representing the file's data.

                    } else if(/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type)){

                        var fRead = new FileReader();
                        fRead.onload = (function(file){
                            return function(e) {

                                var video = $('<video style="object-fit:cover;" width="151" height="130" controls>'+
                                '<source src='+e.target.result+' type="video/mp4">'+
                                '<source src="movie.ogg" type="video/ogg">'+
                                '</video>');

                                var button = $('<span/>').addClass('close').addClass('removeFile').click(function(){     //create close button element
                                    var filename = upload.getName();
                                    commentFileDelete(filename, this, $('#pair{{$post->id}}').val());
                                }).append('<span>&times;</span>');

                                var videoitem = $('<div/>').attr('attr', uid).append(video);
                                videoitem = $('<div/>').addClass('img-wrap-newsfeed').append(videoitem).append(button);
                                commentform.find('.medias').append(videoitem);
                                upload.doUpload(uid, $('#pair{{$post->id}}').val());
                            };
                        })(file);
                        fRead.readAsDataURL(file); //URL representing the file's data.

                    }
                });
                commentform.find('textarea').focus();
                $(this).val("");

            }else{
                alert("Your browser doesn't support File API!"); //if File API is absent
            }

        });

        
        $('body').on('click', '.post-r-comment-link-{{$post->id}}', function (e) {
            var post_id = $(this).attr('data-p_id')
             $('.commentForm'+post_id).find('button[type=submit]').click();
        });


        $('body').on('submit', '.commentForm{{$post->id}}', function (e) {
            var form = $(this);
            var post_id = '{{$post->id}}';
            var comments_count = $('.'+ post_id +'-comments-count').html();
            var text = form.find('textarea').val().trim();
            var files = form.find('.medias').find('div').length;

            if(text == "" && files == 0)
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: '',
                    type: 'orange',
                    offsetTop: 0,
                    offsetBottom: 500,
                    content: '<span style="font-size:17px;">Please input text or select files!</span>',
                });
                return false;
            }
            var values = $(this).serialize();
            
            $.ajax({
                method: "POST",
                url: "{{ route('post.comment') }}",
                data: values
            })
                .done(function (res) {
                    $('#comments{{$post->id}}').prepend(res);
                    form.find('.medias').find('.removeFile').each(function(){
                            $(this).click();
                        });
                    form.find('.medias').empty();
                    form.find('textarea').val('');
                    form.find(".note-editable").html('');
                    form.find(".note-placeholder").addClass('custom-placeholder')
                    form.find('.post-create-controls').addClass('d-none');
                    form.find('.emojionearea-button').addClass('d-none');
          
                    $('.'+ post_id +'-comments-count').html(parseInt(comments_count) + 1)

                    Comment_Show.reload(form.closest('.post-block'));
                });
            e.preventDefault();
        });

    });
</script>

@endif
