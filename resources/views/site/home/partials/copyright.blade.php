<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="/frontend_assets/css/style.css">
    <title>Travooo - forget pass</title>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
    <script src="/frontend_assets/js/script.js"></script>
</head>

<body>
<header class="main-header">
    <nav class="navbar navbar-toggleable-md navbar-light bg-faded px-4">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
            <i class="trav-bars"></i>
        </button>
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{url('assets2/image/main-circle-logo.png')}}" alt="">
        </a>

        <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarSupportedContent">
            <ul class="nav navbar-nav ">
                <li class="nav-item">What is travooo?</li>
                <li class="nav-item"><b>Experts</b></li>
                <li class="nav-item">Help</li>
            </ul>
            <ul class="navbar-custom-menu navbar-nav pull-right">
                <li class="nav-item dropdown ">
                    @if(Auth::user())
                        <a class="profile-link" href="#" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}"
                                 alt="">
                            <span>{{Auth::user()->name}}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-use-menu">
                            <a class="dropdown-item" href="{{route('profile')}}">
                                <div class="drop-txt">
                                    <p>@lang('profile.profile')</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="{{route('dashboard')}}">
                                <div class="drop-txt">
                                    <p>@lang('dashboard.my_dashboard')</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="{{route('setting')}}">
                                <div class="drop-txt">
                                    <p>@lang('setting.settings')</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="{{url_with_locale('activitylog')}}">
                                <div class="drop-txt">
                                    <p>@lang('activity_log')</p>
                                </div>
                            </a>
                            <a class="dropdown-item logout-link" href="{{url('user/logout')}}">
                                <div class="drop-txt">
                                    <p>@lang('dashboard.log_out')</p>
                                </div>
                            </a>
                        </div>
                    @else
                        <div>
                            <a href="{{url('/login')}}" class="btn btn-xs btn-outline-primary text-primary">Log in</a>
                            <a href="{{url('/login')}}" class="btn btn-xs btn-primary bg-primary mx-1 text-white">Sign
                                up</a>
                        </div>
                    @endif
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="blue_background">
    <div class="container">
        <div class="col-sm-12 py-5">
            <a href="" class="text-white"><i class="fa fa-long-arrow-left"></i>&nbsp Help Center</a>
        </div>
        <div class="col-sm-6">
            <h1><b>Report copyright infringement</b></h1>
        </div>
        <div class="col-sm-5 offset-sm-5 pb-4">
            <input id="search" type="text" style="border-bottom: 2px solid black" placeholder="Search"
                   aria-label="Search">
        </div>
    </div>
</div>
<div class="content_section " style="background-color: white">
    <div class="col-sm-12 py-4">
        <div class="container">
            <!-- Footer -->
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group">
                <div class="row">
                        <span class="col-sm-12 text-left ">
                            Please fill out the form below to report content that you belive violates or infringes your copyright. Be aware that filing a DMCA
                            complaint initiates a statutorily-defined legal process and we will share your full complaint, including your contact information, with the alleged violator. For more information see our
                            <a href="">Copyright abd DMCA policy</a>. To report violations of the <a href="">Terms of Service</a> please see
                            <a href="">How to report violations</a>.
                        </span>
                </div>
            </div>
            <form method="POST" id="copyrightForm" class="demo-form" action="{{url('copyright-infringement')}}">
                {{ csrf_field() }}
                <section id="sender-data">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="sender_type"><b>Tell us about yourself</b></label>
                            </div>
                            <div class="col-sm-8">
                                <div class="custom-radio">
                                    <input class="radio active_checkbox" checked type="radio" name="sender_type"
                                           value="copyright_owner" id="copyright-owner" autocomplete="off">
                                    <label for="copyright-owner">I am the copyright owner.</label>
                                </div>
                                <div class="custom-radio">
                                    <input class="radio active_checkbox" type="radio" name="sender_type"
                                           value="authorized_by_copyright_owner" id="authorized-by-owner"
                                           autocomplete="off">
                                    <label for="authorized-by-owner">I am an authorized representative of the copyright
                                        owner</label><br>
                                </div>
                                <div class="custom-radio">
                                    <input class="radio active_checkbox" type="radio" name="sender_type"
                                           value="non_above" id="non-above" autocomplete="off">
                                    <label for="non-above">Non of the above</label><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="non-of-above-message" class="form-control p-3">
                        Travooo only handles requests that are submitted  by copyright owner or an authorized representative
                        of the copyright owner. Please see our <a class="text-decoration-none" href="/copyright-policy">Copyright Policy</a> for more information
                    </div>
                    <div id="authorized-owner">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="ownerName"><b>Copyright owner's full name</b></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="owner_name" class="form-control" id="ownerName"
                                           autocomplete="off"  required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="senderName"><b>Your full name</b></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="sender_name" class="form-control" id="senderName"
                                           autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="company"><b>Company</b> (optional)</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="company" class="form-control" id="company" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="job"><b>Job title</b></label>
                                </div>
                                <div class="col-sm-8 text-left text-left">
                                    <input type="text" name="job_title" class="form-control" id="job" autocomplete="off" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="senderEmail"><b>Your email</b></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="sender_email" class="form-control" id="senderEmail"
                                           placeholder="This is the email we'll use to contact you." autocomplete="off" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="street"><b>Street address</b></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="street" class="form-control" id="street"
                                           placeholder="Enter your current address" autocomplete="off" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="city"><b>City</b></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="city" class="form-control" id="city" autocomplete="off" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="state"><b>State/Province</b></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="state" class="form-control" id="state" autocomplete="off" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="postalCode"><b>Postal code</b></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="postal_code" class="form-control" id="postalCode"
                                           autocomplete="off" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="country"><b>Country</b></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="country" class="form-control" id="country" autocomplete="off" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="phone"><b>Phone number </b>(optional)</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="tel" pattern="[+]{1}[0-9]{11,14}" data-utils ="{{asset('/plugins/intl-tel-input/js/utils.js')}}" placeholder="Phone Number" name="phone" class="form-control" id="phone" value="+" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="fax"><b>Fax number</b>(optional)</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="tel" name="fax" data-utils ="{{asset('/plugins/intl-tel-input/js/utils.js')}}" class="form-control" id="fax" value="+" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="about-work">
                    <div id="original-links-section">
                        <div id="original-links-section-form">
                            <h4 class="py-3"><b>About your copyrighted work</b></h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4 text-left text-left">
                                        <label for="description"><b>Description of the original work</b></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <textarea type="text" name="description[]" rows="3" class="form-control" id="description"
                                                  placeholder='Please provide a detailed description of the original work.(e.g.,"photograph of a dog in a bathtub I&#8217ve taken.")'
                                                  autocomplete="off" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="original-link"><b>Link(s) to the original work</b>(optional)</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <textarea type="text" name="original_link[]" class="form-control"
                                                  id="original-link"
                                                  placeholder="Please provide direct link(s) to the original work if available. Enter one link per line."
                                                  autocomplete="off" datasrc="url"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="original-more"><b>Adding more links?</b></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="checkbox" autocomplete="off" onclick="moreLinks()" id="original-more" required>
                                <label for="original-more"></label><br>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="about-infringing">
                    <div id="about-infringing-section">
                        <div id="about-infringing-section-form" class="report-section-form" data-num="">
                            <h4 class="py-3"><b>About the infringing material</b></h4>
                            <div class="form-group infringement-location">
                                <div class="row ">
                                    <div class="col-sm-4">
                                        <label for=""><b>Where is the infringement happening on Travooo?</b></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="custom-radio">
                                            <input class="radio active_checkbox" checked type="radio"
                                                   name="infringement_location[]" value="post" id="loc-post" autocomplete="off">
                                            <label for="loc-post">In a Post</label><br>
                                        </div>
                                        <div class="custom-radio">
                                            <input class="radio active_checkbox" type="radio" name="infringement_location[]"
                                                   value="post_discussion" id="post-discussion" autocomplete="off">
                                            <label for="post-discussion">In a Post Discussion</label>
                                        </div>

                                        <div class="custom-radio">
                                            <input class="radio active_checkbox" type="radio" name="infringement_location[]"
                                                   value="travelog" id="travelog" autocomplete="off">
                                            <label for="travelog">In a Travelog</label>
                                        </div>

                                        <div class="custom-radio">
                                            <input class="radio active_checkbox" type="radio" name="infringement_location[]"
                                                   value="profile_image" id="profile-image" autocomplete="off">
                                            <label for="profile-image">Profile images (user's header, profile image)</label>
                                        </div>
                                        <div class="custom-radio">
                                            <input class="radio active_checkbox" type="radio" name="infringement_location[]"
                                                   value="trip_plan" id="trip-plan" autocomplete="off">
                                            <label for="trip-plan">In a Trip Plan</label>
                                        </div>
                                        <div class="custom-radio">
                                            <input class="radio active_checkbox" type="radio" name="infringement_location[]"
                                                   value="event" id="event" autocomplete="off">
                                            <label for="event">In an Event</label>
                                        </div>

                                        <div class="custom-radio">
                                            <input class="radio active_checkbox" type="radio" name="infringement_location[]"
                                                   value="other" id="other" autocomplete="off">
                                            <label for="other">Other</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="infringement-report-source" class="infringement-report-source">
                                <div class="report-section">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="reported-url"><b>Reported URL</b></label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="text" name="reported_url[]" class="form-control" oninput="getUrlLocation(event)"
                                                       id="reported-url" placeholder="https://travooo.com/..." autocomplete="off" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="inf-desc"><b>Describe Infringement</b></label>
                                            </div>
                                            <div class="col-sm-8">
                                            <textarea name="infringement_description[]" class="form-control"
                                                      id="inf-desc" autocomplete="off" required></textarea>
                                                <span>Please provide a detailed description of the infringement. Please note that we are unable to process a DMCA takedown for Tweets that do not contain links to allegedly infringing materials unless the content of the Tweet itself is copyrighted.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="report-more"><b>Adding more to report?</b></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="checkbox" autocomplete="off" onclick="moreToReport()" id="report-more" required>
                                <label for="report-more">Yes</label><br>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="required-statements">
                    <h3>Required statements</h3>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-sm-4 text-left">
                                <label for=""><b>512(f) Acknowledgment</b></label>
                            </div>
                            <div class="col-sm-8 text-left">

                                <div class="custom-radio">
                                    <input class="radio active_checkbox" type="radio" checked value="authorized"
                                           id="authorized" autocomplete="off">
                                    <label for="authorized">I understand that under 17 U.S.C. § 512(f), I may be liable
                                        for any damages, including costs and attorneys'fees, if I knowingly materially
                                        misrepresent that reported material activity is infringing.</label>
                                </div>

                                <div class="custom-radio">
                                    <input class="radio active_checkbox"  type="radio"
                                           value="not_authorized" id="not_authorized" autocomplete="off">
                                    <label for="not_authorized" id="disclaimer_label">I have good faith belief that use
                                        of the material in the manner complained of is not authorized by the copyright
                                        owner, its agent, or the law</label><br>
                                    <div class="col-sm-12 p-2 my-2" id="disclaimer"
                                         style="background-color: #fff5f6;display: none">
                                        <b>You must acknowledge the above disclaimer.</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 p-0">
                            <label for="notify-info"><b>Please type this exact statement into the field
                                    below:</b></label>
                        </div>
                        <div class="col-sm-12 p-3 m-0" id="notify-info">
                            The information in this notification is accurate, and I state under penalty of perjury that
                            I am authorized to act on behalf of the copyright owner.
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="authority-act"><b>Authority to Act</b></label>
                            </div>
                            <div class="col-sm-8">
                                <textarea name="authority_act" class="form-control" id="authority-act" placeholder=""
                                          autocomplete="off" required></textarea>
                            </div>
                            <span class="col-sm-12 p-3">
                                    If you are unsure whether the material you are reporting is in fact infriging (and, for example, not protected by fair use),you may wish to contact an attorney before filing a notification with us.
                                </span>
                            <div class="col-sm-12 p-3">
                                <b>Please electronically sign this notice by typing your full name here:</b>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="signature"><b>Signature</b></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" name="signature" class="form-control" id="signature"
                                       placeholder="Your full name" autocomplete="off" required>


                            </div>
                            <span class="col-sm-8 offset-sm-4 py-4">
                                     I undrestand that order to process this notice, Twitter will provide the affected user with <b>a full copy of this complaint,</b> including my full name, email,street address, and any other information included in my report, and may provide a version to third parties, such as <a
                                        href="">Lumen</a>, with the contact information removed. For more information see our <a
                                        href="">Copyright Policy</a>
                                </span>
                        </div>
                    </div>
                </section>
                <div class="row">
                    <div class="col-sm-7 offset-sm-4">
                        <button type="button" id="nextBtn" class="next btn my-4 px-4"
                                onclick="next(1)">@lang('buttons.general.next')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="foot-bottom blue_background">
    <div class="container">
        <footer class="page-footer font-small indigo">
            <!-- Footer Links -->
            <div class="container text-center text-md-left">
                <!-- Grid row -->
                <div class="row">
                    <div class="col-md-4 text-center mx-auto text-left">
                        <!-- Links -->
                        <h6 class="font-weight-bold mt-3 mb-4 py-2 text-left">Help Center</h6>
                        <ul class="list-unstyled text-left">
                            <li class="py-1">
                                <a href="#!">Using Twitter</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Managing your account</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Safety and security</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Rules and policies</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Contact us</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Grid column -->
                    <hr class="clearfix w-100 d-md-none">
                    <!-- Grid column -->
                    <div class="col-md-4 text-center mx-auto text-left">
                        <!-- Links -->
                        <h6 class="font-weight-bold mt-3 mb-4 py-2 text-left">Footer Menu</h6>
                        <ul class="list-unstyled text-left">
                            <li class="py-1">
                                <a href="#!">Insights</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Success Stories</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Solutions</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Collections</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Advertising blog</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Flight School</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Grid column -->
                    <hr class="clearfix w-100 d-md-none">
                    <!-- Grid column -->
                    <div class="col-md-4 text-center mx-auto text-left">
                        <!-- Links -->
                        <h6 class="font-weight-bold mt-3 mb-4 py-2 text-left">Footer Menu</h6>
                        <ul class="list-unstyled text-left text-white">
                            <li class="py-1">
                                <a href="#!">Using Twitter</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Managing your account</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Safety and security</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Rules and policies</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Contact us</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Grid column -->
                </div>
                <!-- Grid row -->
            </div>
            <!-- Footer Links -->
        </footer>

    </div>
</div>
<footer class="footer text-white">
    <div class="foot-bottom">
        <div class="foot-top">
            <ul class="aside-foot-menu text-black">
                <li><a href="{{url('/')}}">About</a></li>
                <li><a href="{{url('/')}}">Careers</a></li>
                <li><a href="{{url('/')}}">Sitemap</a></li>
                <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                <li><a href="{{url('/')}}">Contact</a></li>
                <li><a href="{{route('help.index')}}">Help Center</a></li>
            </ul>
        </div>
        <p class="copyright">Travooo © 2020</p>
    </div>
</footer>
<style>
    .blue_background {
        background-color: rgb(64, 128, 255);
        z-index: 109;
        color: white;
        background-image: url({{url("assets/image/copyright_logo.png")}});
        background-position: right bottom;
        background-size: cover;
    }

    .blue_background a, .blue_background li {
        color: white;
    }

    .active-cyan-2 input.form-control[type=text]:focus:not([readonly]) {
        border-bottom: 1px solid #4dd0e1;
        box-shadow: 0 1px 0 0 #4dd0e1;
    }

    .active-cyan-2 input[type=text]:focus:not([readonly]) {
        border-bottom: 1px solid #4dd0e1;
        box-shadow: 0 1px 0 0 #4dd0e1;
    }

    section a {
        color: #4080ff !important;
    }

    button, .btn-primary {
        background: #4080ff;
        background-color: #4080ff;
        font-family: inherit;
        color: white !important;
    }

    .aside-foot-menu li a {
        color: black !important;
    }

    .list-unstyled li a {
        color: white !important;
    }

    .custom-radio label {
        position: relative;
        padding-left: 36px;
        font-size: 16px;
        font-weight: 200;
        line-height: 20px;
        color: #000000;
        font-family: "Circular Std - Book";
    }

    .custom-radio label:after {
        content: "";
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        left: 0;
        width: 16px;
        height: 16px;
        border-radius: 4px;
        border: 2px solid #4080ff;
    }

    .custom-radio input:checked + label {
        font-weight: 400;
    }

    .custom-radio input:checked + label:before {
        content: "";
        position: absolute;
        top: 50%;
        left: 5px;
        transform: translateY(-50%);
        width: 6px;
        height: 6px;
        border-radius: 2px;
        background: #4080ff;

    }

    .custom-radio input {
        display: none;
    }

    .form-control {
        border-radius: 5px;
        background-color: rgb(247, 247, 247);
        border: none;
    }

    #notify-info {
        background-color: rgb(247, 247, 247);
        border: 1px solid #e6e6e6;
        border-radius: 3px;
    }

    .form-group {
        padding: 9px 0;
    }

    textarea {
        resize: none
    }

    #search::placeholder {
        font-family: inherit;
        color: #94b8ff
    }

    #search {
        background-color: transparent;
        border-bottom: 1px solid #94b8ff !important;
        color: inherit;
        width: 100%;
    }

    span {
        line-height: 1.6;
    }

    input.invalid, textarea.invalid {
        background-color: #ffdddd;
    }

    section {
        display: none;
    }

    #nextBtn {
        font-family: inherit;
        border-radius: 4px;
        padding: 10px 20px;
    }

    #original-link {
        max-lines: 10;
        resize: vertical;
        max-height: 10em;
        overflow: hidden;
        height: unset;
    }

    .profile {
        border-bottom: 1px solid #b3bfc8;
        border-radius: 0;
    }

    #select2-reported-url-container {
        border-bottom: 2px solid #b3bfc8;
    }

    .select2-selection__arrow{
        display: none;
    }

    .select2-selection--single {
        border: 0!important;
    }

</style>
<script>
    var currentSection = 0; // Current Section is set to be the first Section (0)
    //if sender click in radio button
    var clickOnLocationButton = true;

    showSection(currentSection); // Display the current Section
    function showSection(n) {
        $( "input, textarea" ).on("change paste keyup", function() {
            $(this).removeClass( "invalid" );
        });
        // This function will display the specified tab of the form...
        var section = document.getElementsByTagName("section");
        $("#non-of-above-message").hide();
        $(".profile-section").hide();

        $("#non-above").click(function () {
            $("#non-of-above-message").show();
            $("#authorized-owner").hide();
            $("#nextBtn").hide();
        });

        $("#authorized-by-owner, #copyright-owner").click(function () {
            $("#non-of-above-message").hide();
            $("#authorized-owner").show();
            $("#nextBtn").show();
        });

        // document.getElementById("original-link").style.height = "auto!important";
        $("#original-link").css('height', 'auto!important');
        section[n].style.display = "block";
        if (section[n].id === 'about-infringing') {
            // document.getElementById("nextBtn").disabled = true;
        }

        //... and fix the Previous/Next buttons:
        if (n == (section.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Send";
        } else {
            document.getElementById("nextBtn").innerHTML = "Next";
        }
    }

    $(document).on('change', `#about-infringing-section .custom-radio`, (e)=>{
        let length = $("#about-infringing-section").children().length;
        let num =  length > 1 ? length - 1 : '';
        num = $(e.target).closest(`.report-section-form`).attr('data-num');
        // $(e.target).closest('.report-section-form').find('div[class=infringement-report-source]').attr('checked', true);//.trigger('click')
        if (!['cover_image','profile_picture'].includes(e.target.value) && clickOnLocationButton) {
            if(e.target.value === 'profile_image') {
                $(e.target).closest(`.report-section-form`).find(`.infringement-report-source`).html(`<div class="profile-section">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="reported-url${num}"><b>Account using your image</b></label>
                            </div>
                            <div class="col-sm-8">
                                <select class="form-control profile" name="reported_url[${num}]"
                                        id="reported-url${num}" placeholder="@" autocomplete="off" required>
                                    <option value="">
                                        </option>
                                </select>
                                <span ><small class="text-secondary">Please provide the @username of the account using your work.</small></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="inf-desc"><b>Where is in the content?</b></label>
                            </div>

                            <div class="col-sm-8">
                                <div class="custom-radio">
                                    <input class="radio active_checkbox" type="radio" name="infringement_description[${num}]"
                                           value="profile_picture" id="profile-picture${num}" autocomplete="off" checked>
                                    <label for="profile-picture${num}">It's this user's avatar (profile picture)</label>
                                </div>
                                <div class="custom-radio">
                                    <input class="radio active_checkbox" type="radio" name="infringement_description[${num}]"
                                       value="cover_image" id="header-image${num}" autocomplete="off">
                                    <label for="header-image${num}">It's this user's header image</label>
                                </div>
                            </div>
                        </div>
                    </div>
                <div>`)
            } else {
                $(e.target).closest(`.report-section-form`).find(`div[class=infringement-report-source]`).html(`<div class="report-section">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="reported-url${num}"><b>Reported URL</b></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" name="reported_url[]" class="form-control" oninput="getUrlLocation(event)"
                                       id="reported-url${num}" placeholder="https://travooo.com/..." autocomplete="off" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="inf-desc${num}"><b>Describe Infringement</b></label>
                            </div>
                            <div class="col-sm-8">
                                <textarea name="infringement_description[${num}]" class="form-control"
                                          id="inf-desc${num}" autocomplete="off" required></textarea>
                                <span>Please provide a detailed description of the infringement. Please note that we are unable to process a DMCA takedown for Tweets that do not contain links to allegedly infringing materials unless the content of the Tweet itself is copyrighted.</span>
                            </div>
                        </div>
                    </div>
                </div>`);
            }
        }

        $(`#infringement-report-source${num} .profile`).select2({
            width:'100%',
            method: "GET",
            placeholder: 'Search User',
            ajax: {
                url: "{{url('home/searchUsers')}}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                    };
                },
                processResults: function (data) {
                    var users = [];
                    data.forEach(function (item, key) {
                        if (item) {
                            users.push({'id' : item.id, 'text' : item.name})
                        }
                    });
                    return {
                        results: users
                    };
                },
                cache: true
            }
        });
    });

    function next(n) {
        // This function will figure out which section to display
        var section = document.getElementsByTagName("section");
        // Exit the function if any field in the current section is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current section:
        section[currentSection].style.display = "none";
        // Increase or decrease the current section by 1:
        currentSection = currentSection + n;
        // if you have reached the end of the form...
        if (currentSection >= section.length) {
            // ... the form gets submitted:
            document.getElementById("copyrightForm").submit();
            return false;
        }
        // Otherwise, display the correct section:
        showSection(currentSection);
    }

    function validateForm() {
        $("input, textarea" ).removeClass( "invalid" );
        // This function deals with validation of the form fields
        var section, elems, i, valid = true;
        section = document.getElementsByTagName("section");
        elems = section[currentSection].querySelectorAll('input,textarea');
        // A loop that checks every input field in the current section:
        for (i = 0; i < elems.length; i++) {
            if (elems[i].type === "checkbox") {
                continue;
            }
            // If a field is optional...
            if (elems[i].id === 'fax' || elems[i].id === 'phone' || elems[i].id === 'company') {
                if ((elems[i].id === 'fax' || elems[i].id === 'phone') && elems[i].value.length > 1) {
                    let reg = /(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)/;
                    var res = elems[i].value.match(reg);
                    if(res == null) {
                        elems[i].className += " invalid";
                        valid = false;
                    }
                }
                continue;
            } else if (elems[i].id.includes('original-link') && elems[i].value) {
                let urls = elems[i].value.split(' ');
                let counter = 0;
                urls.forEach(function(data) {
                    counter++;
                    if (!this.isValidUrl(data.trim()) || counter >= 10) {
                        elems[i].className += " invalid";
                        valid = false;
                    }
                });
            } else {
                if (elems[i].value && elems[i].value.trim().length < 3){
                    elems[i].className += " invalid";
                    valid = false;
                }
            }

            if (elems[i].id.includes('reported-url')) {
                let urls = elems[i].value.split(' ');
                let counter = 0;
                urls.forEach(function(data) {
                    counter++;
                    if (!this.isValidUrl(data) || !data.includes(window.location.hostname)) {
                        elems[i].className += " invalid";
                        valid = false;
                    }
                });
            }

            if (elems[i].id === 'senderEmail') {
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                if (reg.test(elems[i].value) == false) {
                    elems[i].className += " invalid";
                    valid = false;
                }
            }

            if (elems[i].name === 'sender_type') {
                if (elems[i].value === 'non_above' && elems[i].checked === true) {
                    elems[i].parentElement.style.backgroundColor = '#ffdddd';
                    valid = false;
                }
            }

            if (elems[i].value == "") {
                // add an "invalid" class to the field:
                elems[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        return valid; // return the valid status
    }

    function moreToReport() {
        var num = $("#about-infringing-section").children().length;
        if (num > 9) {
            alert('You can add no more than 10 entries');
        } else {
            var div = $("#about-infringing-section-form:first");
            let clone = div.clone();
            // Clone it and assign the new ID (i.e: from num 4 to ID "klon4")
            clone.prop('id', 'about-infringing-section-form' + num);
            clone.attr('data-num', num).find("h4").each(function () {
                $(this).html("<b>About the infringing material in Link #" + num + "</b>");
            });
            clone.find("label").each(function () {
                let forAttr = $(this).attr('for');
                $(this).attr("for", forAttr + num);
            });
            clone.find('input').each(function () {
                $(this).attr('id', $(this).attr('id') + num);

                if (['cover_image','profile_picture'].includes($(this).attr('value'))) {
                    $(this).prop('name', 'infringement_description[' + num + ']');
                } else if ( $(this).attr('type') == 'radio' ) {
                    $(this).prop('name', 'infringement_location[' + num + ']');
                }
            });
            clone.css("border-top", "1px solid #e6e6e6");
            clone.find('profile-section').attr('class', 'profile-section' + num);
            clone.find('#infringement-report-source').attr('id', 'infringement-report-source' + num);
            clone.find('report-section').attr('class', 'report-section' + num);
            clone.appendTo("#about-infringing-section");
            var checkBoxes = $("#report-more");
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        }
    }

    function moreLinks() {
        // let el = document.getElementById("nextBtn");
        var num = $("#original-links-section").children().length;
        if (num > 9) {
            alert('You can add no more than 10 entries');
        } else {
            var div = $("#original-links-section-form:first");
            $(div).find("input, select").each(function () {
                let id = $(this).attr('id');
                $(this).attr("id", id + num);
            });
            $(div).find("label").each(function () {
                let forAttr = $(this).attr('for');
                $(this).attr("for", forAttr + num);
            });
            $(div).find("h4").each(function () {
                $(this).html("<b>About your copyrighted work#" + num + "</b>");
            });

            div.clone().prop('id', 'original-links-section-form' + num).appendTo("#original-links-section");
            $('#original-links-section-form').find('input').each(function () {
                $(this).attr('id', $(this).attr('id')).next('label').attr('for', $(this).attr('id'))
            });
            var checkBoxes = $("#original-more");
            $("#original-links-section").not(":first-child").css("border-top", "1px solid #e6e6e6");
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
            // num = count + 1;
        }
    }

    function isValidUrl(userInput) {
        let reg = /http(s)?:\/\/.?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&=])*/;
        var res = userInput.match(reg);
        if(res == null)
            return false;
        else
            return true;
    }
    function getUrlLocation(e) {
        let value = e.target.value;
        clickOnLocationButton = false;
        if(value.includes("/reports/")) {
            $(e.target).closest('.report-section-form').find('input[value=travelog]').attr('checked', true).trigger('click')
        } else if(value.includes("/trip/plan/")) {
            $(e.target).closest('.report-section-form').find('input[value=trip_plan]').attr('checked', true).trigger('click')
        }
        clickOnLocationButton = true;
    }

</script>

</body>

</html>

