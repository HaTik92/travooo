<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="/frontend_assets/css/style.css">
    <title>Travooo - forget pass</title>
</head>

<body>
<header class="main-header">
    <nav class="navbar navbar-toggleable-md navbar-light bg-faded px-4">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
            <i class="trav-bars"></i>
        </button>
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{url('assets2/image/main-circle-logo.png')}}" alt="">
        </a>

        <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarSupportedContent">
            <ul class="nav navbar-nav ">
                <li class="nav-item">What is travooo?</li>
                <li class="nav-item"><b>Experts</b></li>
                <li class="nav-item">Help</li>
                <li class="nav-item">Appeal</li>
            </ul>
            <ul class="navbar-custom-menu navbar-nav pull-right">
                <li class="nav-item dropdown ">
                    @if(Auth::user())
                        <a class="profile-link" href="#" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}"
                                 alt="">
                            <span>{{Auth::user()->name}}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-use-menu">
                            <a class="dropdown-item" href="{{route('profile')}}">
                                <div class="drop-txt">
                                    <p>@lang('profile.profile')</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="{{route('dashboard')}}">
                                <div class="drop-txt">
                                    <p>@lang('dashboard.my_dashboard')</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="{{route('setting')}}">
                                <div class="drop-txt">
                                    <p>@lang('setting.settings')</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="{{url_with_locale('activitylog')}}">
                                <div class="drop-txt">
                                    <p>@lang('activity_log')</p>
                                </div>
                            </a>
                            <a class="dropdown-item logout-link" href="{{url('user/logout')}}">
                                <div class="drop-txt">
                                    <p>@lang('dashboard.log_out')</p>
                                </div>
                            </a>
                        </div>
                    @else
                        <div>
                            <a href="{{url('/login')}}" class="btn btn-xs btn-outline-primary text-primary">Log in</a>
                            <a href="{{url('/login')}}" class="btn btn-xs btn-primary bg-primary mx-1 text-white">Sign up</a>
                        </div>
                    @endif
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="blue_background">
    <div class="container">
        <div class="row ">

        <div class="col-sm-12 py-5">
            <a href="" class="text-white"><i class="fa fa-long-arrow-left"></i>&nbsp Help Center</a>
        </div>
        <div class="col-sm-5">
            <h1><b>Hi @ {{ Auth::user()->name }} Copyright Policy</b></h1>
        </div>
        <div class="col-sm-5 offset-sm-5 pb-4">
            <input  id="search" type="text" style="border-bottom: 2px solid black" placeholder="Search" aria-label="Search">
        </div>
        </div>
    </div>
</div>
<div class="p-4 text-center post-section">
    <a href="{{ $link }}">Your post</a> was removed due to a copyright claim
    @if($sender_name)
        by <b>{{ $sender_name }}</b>
    @endif
</div>
<div class="content_section " style="background-color: white">
    <div class="container">
        <div class="row ">
    <div class="col-sm-4 text-center p-3">
        <!-- Links -->
        <ul class="text-left">
            <li class="py-3 active">
                <a href="#users-posts">Users Posts</a>
            </li>
            <li class="py-3">
                <a href="#trip-plans">Trip Plans</a>
            </li>
            <li class="py-3">
                <a href="#travelogs">Travelogs</a>
            </li>
            <li class="py-3">
                <a href="#photos-videos">Photos/Videos</a>
            </li>
            <li class="py-3">
                <a href="#events">Events</a>
            </li>
        </ul>
    </div>
            <div class="col-sm-8 text-centertext-left p-3">
                <!-- Links -->
                <div class="col-sm-12 text-centertext-left">
                    <h2 class="py-3" id="users-posts"><b>Users Posts</b></h2>
                    <div class="content pb-4">Please fill out the form below to report content that you believe violates or
                        infringes your copyright. Be aware that filing a DMCA complaint initiates a statutorily-defined
                        legal process and we will share your full complaint, including your contact information, with
                        the alleged violator. For more information see our <a href="">Copyright and DMCA policy</a>. To report
                        violations of the <a href="">Terms of Service</a> please see <a href="">How to report violations</a>.
                        <br><br>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque laoreet ornare ligula, at
                        egestas purus feugiat ac. Morbi molestie finibus nulla id porta. Etiam ut orci sed tellus
                        pellentesque luctus. Fusce tellus augue, vestibulum ac cursus sit amet, convallis eu sem.
                        Curabitur viverra lacus ac sem placerat efficitur. Curabitur pulvinar vitae mi in rhoncus. Donec
                        sollicitudin auctor ex ut pretium. Praesent vel faucibus tortor, id ultrices est. Fusce laoreet
                        accumsan orci, at tincidunt sem pretium vel. Fusce convallis, metus vel fringilla ultricies,
                        tortor magna ornare lorem, tincidunt porttitor mi lectus sit amet lorem. Vivamus gravida
                        lobortis neque sit amet porttitor. Suspendisse ut augue sem.
                        <br><br>
                        Quisque mattis congue mi, id bibendum lectus semper at. Donec suscipit sem non eros malesuada,
                        finibus cursus elit venenatis. Sed viverra condimentum sagittis. Cras euismod, est quis
                        consequat venenatis, velit risus tincidunt felis, pellentesque porttitor justo velit nec enim.
                        Praesent at metus in ipsum tincidunt ultricies. Mauris quis nisl at dui feugiat euismod.
                        Maecenas eu felis lorem. Nulla sed mattis urna, et commodo libero. Praesent aliquet enim eu
                        tincidunt posuere.
                        <br><br>
                        Please fill out the form below to report content that you believe violates or
                        infringes your copyright. Be aware that filing a DMCA complaint initiates a statutorily-defined
                        legal process and we will share your full complaint, including your contact information, with
                        the alleged violator. For more information see our <a href="">Copyright and DMCA policy</a>. To report
                        violations of the <a href="">Terms of Service</a> please see <a href="">How to report violations</a>.

                    </div>
                </div>
                <div class="col-sm-12 text-centertext-left">
                    <h2 id="trip-plans"  class="py-3"><b>Trip Plans</b></h2>
                    <div class="content pb-4">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque laoreet ornare ligula, at
                        egestas purus feugiat ac. Morbi molestie finibus nulla id porta. Etiam ut orci sed tellus
                        pellentesque luctus. Fusce tellus augue, vestibulum ac cursus sit amet, convallis eu sem.
                        Curabitur viverra lacus ac sem placerat efficitur. Curabitur pulvinar vitae mi in rhoncus. Donec
                        sollicitudin auctor ex ut pretium. Praesent vel faucibus tortor, id ultrices est. Fusce laoreet
                        accumsan orci, at tincidunt sem pretium vel. Fusce convallis, metus vel fringilla ultricies,
                        tortor magna ornare lorem, tincidunt porttitor mi lectus sit amet lorem. Vivamus gravida
                        lobortis neque sit amet porttitor. Suspendisse ut augue sem.
                        <br><br>
                        Quisque mattis congue mi, id bibendum lectus semper at. Donec suscipit sem non eros malesuada,
                        finibus cursus elit venenatis. Sed viverra condimentum sagittis. Cras euismod, est quis
                        consequat venenatis, velit risus tincidunt felis, pellentesque porttitor justo velit nec enim.
                        Praesent at metus in ipsum tincidunt ultricies. Mauris quis nisl at dui feugiat euismod.
                        Maecenas eu felis lorem. Nulla sed mattis urna, et commodo libero. Praesent aliquet enim eu
                        tincidunt posuere.
                    </div>
                </div>
                <div class="col-sm-12 text-centertext-left">
                    <h2 id="travelogs"  class="py-3"><b>Travelogs</b></h2>
                    <div class="content pb-4">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque laoreet ornare ligula, at
                        egestas purus feugiat ac. Morbi molestie finibus nulla id porta. Etiam ut orci sed tellus
                        pellentesque luctus. Fusce tellus augue, vestibulum ac cursus sit amet, convallis eu sem.
                        Curabitur viverra lacus ac sem placerat efficitur. Curabitur pulvinar vitae mi in rhoncus. Donec
                        sollicitudin auctor ex ut pretium. Praesent vel faucibus tortor, id ultrices est. Fusce laoreet
                        accumsan orci, at tincidunt sem pretium vel. Fusce convallis, metus vel fringilla ultricies,
                        tortor magna ornare lorem, tincidunt porttitor mi lectus sit amet lorem. Vivamus gravida
                        lobortis neque sit amet porttitor. Suspendisse ut augue sem.
                        <br><br>
                        Quisque mattis congue mi, id bibendum lectus semper at. Donec suscipit sem non eros malesuada,
                        finibus cursus elit venenatis. Sed viverra condimentum sagittis. Cras euismod, est quis
                        consequat venenatis, velit risus tincidunt felis, pellentesque porttitor justo velit nec enim.
                        Praesent at metus in ipsum tincidunt ultricies. Mauris quis nisl at dui feugiat euismod.
                        Maecenas eu felis lorem. Nulla sed mattis urna, et commodo libero. Praesent aliquet enim eu
                        tincidunt posuere.
                    </div>
                </div>
                <div class="col-sm-12 text-centertext-left">
                    <h2 id="photos-videos"  class="py-3"><b>Photos & Videos</b></h2>
                    <div class="content pb-4">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque laoreet ornare ligula, at
                        egestas purus feugiat ac. Morbi molestie finibus nulla id porta. Etiam ut orci sed tellus
                        pellentesque luctus. Fusce tellus augue, vestibulum ac cursus sit amet, convallis eu sem.
                        Curabitur viverra lacus ac sem placerat efficitur. Curabitur pulvinar vitae mi in rhoncus. Donec
                        sollicitudin auctor ex ut pretium. Praesent vel faucibus tortor, id ultrices est. Fusce laoreet
                        accumsan orci, at tincidunt sem pretium vel. Fusce convallis, metus vel fringilla ultricies,
                        tortor magna ornare lorem, tincidunt porttitor mi lectus sit amet lorem. Vivamus gravida
                        lobortis neque sit amet porttitor. Suspendisse ut augue sem.
                        <br><br>
                        Quisque mattis congue mi, id bibendum lectus semper at. Donec suscipit sem non eros malesuada,
                        finibus cursus elit venenatis. Sed viverra condimentum sagittis. Cras euismod, est quis
                        consequat venenatis, velit risus tincidunt felis, pellentesque porttitor justo velit nec enim.
                        Praesent at metus in ipsum tincidunt ultricies. Mauris quis nisl at dui feugiat euismod.
                        Maecenas eu felis lorem. Nulla sed mattis urna, et commodo libero. Praesent aliquet enim eu
                        tincidunt posuere.

                    </div>
                </div>
                <div class="col-sm-12 text-centertext-left"content >
                    <h2 id="events"  class="py-3"><b>Events</b></h2>
                    <div class="content pb-4">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque laoreet ornare ligula, at
                        egestas purus feugiat ac. Morbi molestie finibus nulla id porta. Etiam ut orci sed tellus
                        pellentesque luctus. Fusce tellus augue, vestibulum ac cursus sit amet, convallis eu sem.
                        Curabitur viverra lacus ac sem placerat efficitur. Curabitur pulvinar vitae mi in rhoncus. Donec
                        sollicitudin auctor ex ut pretium. Praesent vel faucibus tortor, id ultrices est. Fusce laoreet
                        accumsan orci, at tincidunt sem pretium vel. Fusce convallis, metus vel fringilla ultricies,
                        tortor magna ornare lorem, tincidunt porttitor mi lectus sit amet lorem. Vivamus gravida
                        lobortis neque sit amet porttitor. Suspendisse ut augue sem.
                        <br><br>
                        Quisque mattis congue mi, id bibendum lectus semper at. Donec suscipit sem non eros malesuada,
                        finibus cursus elit venenatis. Sed viverra condimentum sagittis. Cras euismod, est quis
                        consequat venenatis, velit risus tincidunt felis, pellentesque porttitor justo velit nec enim.
                        Praesent at metus in ipsum tincidunt ultricies. Mauris quis nisl at dui feugiat euismod.
                        Maecenas eu felis lorem. Nulla sed mattis urna, et commodo libero. Praesent aliquet enim eu
                        tincidunt posuere.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="foot-bottom blue_background">
    <footer class="page-footer font-small indigo">
            <!-- Footer Links -->
            <div class="container text-center text-md-left">
                <!-- Grid row -->
                <div class="row">
                    <div class="col-md-4 text-center mx-auto text-left">
                        <!-- Links -->
                        <h6 class="font-weight-bold mt-3 mb-4 py-2 text-left">Help Center</h6>
                        <ul class="list-unstyled text-left">
                            <li class="py-1">
                                <a href="#!">Using Twitter</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Managing your account</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Safety and security</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Rules and policies</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Contact us</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Grid column -->
                    <hr class="clearfix w-100 d-md-none">
                    <!-- Grid column -->
                    <div class="col-md-4 text-center mx-auto text-left">
                        <!-- Links -->
                        <h6 class="font-weight-bold mt-3 mb-4 py-2 text-left">Footer Menu</h6>
                        <ul class="list-unstyled text-left">
                            <li class="py-1">
                                <a href="#!">Insights</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Success Stories</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Solutions</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Collections</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Advertising blog</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Flight School</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Grid column -->
                    <hr class="clearfix w-100 d-md-none">
                    <!-- Grid column -->
                    <div class="col-md-4 text-center mx-auto text-left">
                        <!-- Links -->
                        <h6 class="font-weight-bold mt-3 mb-4 py-2 text-left">Footer Menu</h6>
                        <ul class="list-unstyled text-left text-white">
                            <li class="py-1">
                                <a href="#!">Using Twitter</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Managing your account</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Safety and security</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Rules and policies</a>
                            </li>
                            <li class="py-1">
                                <a href="#!">Contact us</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Grid column -->
                </div>
                <!-- Grid row -->
            </div>
            <!-- Footer Links -->
        </footer>
</div>
<footer class="footer text-white">
    <div class="foot-bottom">
        <div class="foot-top">
            <ul class="aside-foot-menu text-black">
                <li><a href="{{url('/')}}">About</a></li>
                <li><a href="{{url('/')}}">Careers</a></li>
                <li><a href="{{url('/')}}">Sitemap</a></li>
                <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                <li><a href="{{url('/')}}">Contact</a></li>
                <li><a href="{{route('help.index')}}">Help Center</a></li>
            </ul>
        </div>
        <p class="copyright">Travooo © 2020</p>
    </div>
</footer>
<style >
    .blue_background {
        background-color: rgb(64, 128, 255);
        z-index: 109;
        color: white;
        background-image: url({{url("assets/image/copyright_logo.png")}});
        background-position: right bottom;
        background-size: cover;
    }
    .blue_background a,.blue_background li{
        color: white;
    }
    .active-cyan-2 input.form-control[type=text]:focus:not([readonly]) {
        border-bottom: 1px solid #4dd0e1;
        box-shadow: 0 1px 0 0 #4dd0e1;
    }
    .active-cyan-2 input[type=text]:focus:not([readonly]) {
        border-bottom: 1px solid #4dd0e1;
        box-shadow: 0 1px 0 0 #4dd0e1;
    }

    .content a {
        color: #4080ff!important;
    }
    /* Style for "Users Post" */
    .content {
        font-size: 18px;
        font-weight: 300;
        line-height: 1.6;
    }
    button, .btn-primary {
        background: #4080ff;
        background-color: #4080ff;
        font-family : inherit;
        color: white!important;
    }
    .aside-foot-menu li a {
        color:black!important;
    }

    .content_section ul li a {
        font-size: 22px;
        font-weight: 700;
        text-decoration: none;
        line-height: 28px;
    }

    .content_section ul li a:active {
        font-weight: bold;
    }

    .content_section ul li a:hover {
        cursor: pointer;
    }

    .post-section {
        background-color: #fff9e5;
    }




    #search::placeholder{
        font-family : inherit;
        color:#94b8ff
    }
    #search {
        background-color: transparent;
        border-bottom: 1px solid #94b8ff!important;
        color: inherit;
        width: 100%;
    }






</style>
<script>

</script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="/frontend_assets/js/script.js"></script>

</body>

</html>

