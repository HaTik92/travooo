<?php
 $followers = [];
    if(Auth::check()){
        $following = \App\Models\User\User::find(Auth::user()->id);
        foreach($following->get_followers as $f){
            $followers[] = $f->followers_id;
        }
    }
    $id = rand(1, 10000);
?>
<div class="post-block discover-travellers-block">
    <div class="post-side-top">
        <h3 class="side-ttl m-0">@lang('home.discover_new_travelers')</h3>
        <div class="side-right-control">
            <a href="#" class="slide-link slide-prev" id="newTravelerDiscover-prev-{{ $id }}"><i class="trav-angle-left"></i></a>
            <a href="#" class="slide-link slide-next" id="videosYouMightLike-next-{{ $id }}"><i class="trav-angle-right"></i></a>
        </div>
    </div>
    <div class="post-side-inner">
        <div class="post-slide-wrap">
            
            <ul id="newTravelerDiscover-{{$id}}" class="post-slider slide-same-height">
                @foreach($new_travellers AS $traveller)
                    <li class="post-follow-card post-travel-card">
                        <div class="follow-card-inner">
                            <div class="image-wrap">
                                <img class="lazy" src="{{check_profile_picture($traveller->profile_picture)}}" alt=""
                                     style='width:62px;height:62px;'>
                            </div>
                            <div class="post-slider-caption">
                                <p class="post-card-name" title="{{$traveller->name}}">{{$traveller->name}}</p>
                                {!! get_exp_icon($traveller) !!}
                                <p class="post-card-spec">{{$traveller->display_name}}</p>
                                @if(in_array($traveller->id, $followers))
                                <button type="button"
                                        class="btn btn-light-grey btn-bordered" onclick="newsfeed_traveller_following('unfollow',{{$traveller->id}}, this)">Unfollow</button>
                                @elseif($traveller->id == Auth::user()->id)
                                <button type="button"
                                        class="btn btn-light-grey btn-bordered">@lang('home.follow')</button>
                                @else
                                <button type="button"
                                        class="btn btn-light-primary btn-bordered" onclick="newsfeed_traveller_following('follow',{{$traveller->id}}, this)">@lang('home.follow')</button>
                                @endif
                                <p class="post-card-follow-count">
                                    @php($countFollowers = @$traveller->followings->count())
                                    @if($countFollowers)
                                        Followed by <a>{{ $traveller->followings->first()->user->name }}</a> {{ $countFollowers > 1 ? 'and ' . ($countFollowers - 1) . ' more' : '' }}
                                    @endif
                                </p>
                            </div>
                        </div>
                    </li>
                @endforeach

            </ul>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        // Post type - shared place, slider
        var videosYouMightLike = $('#newTravelerDiscover-{{$id}}').lightSlider({
            autoWidth: true,
            slideMargin: 20,
            pager: false,
            controls: false
        });

        $('#newTravelerDiscover-prev-{{ $id }}').click(function(e){
            e.preventDefault();
            videosYouMightLike.goToPrevSlide();
        });
        $('#videosYouMightLike-next-{{ $id }}').click(function(e){
            e.preventDefault();
            videosYouMightLike.goToNextSlide();
        });
    })
</script>