
<script type="text/javascript">

    $(document).ready(function () {
        document.emojiSource = "{{ asset('plugins/summernote-master/tam-emoji/img') }}";
        document.textcompleteUrl = "{{ url('reports/get_search_info') }}";
        
        $('body').on('keypress','.comment-reply-txt', function(event){
           
           var keycode = (event.keyCode ? event.keyCode : event.which);
           if(keycode == '13'){
              _id = $(this).data('data_id');
              type  = $(this).data('type');
              _this = $(this);
              var reload_obj = _this.closest('.post-block');
               $.ajax({
                   headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   },
                   method: "POST",
                   url: "{{ route('post.new-comment') }}",
                   data: {post_id: $(this).data('id'),'post_type':$(this).data('type'),'text': $('#post-comment-'+$(this).data('data_id')).val(),'file':$('post-image-'+$(this).data('data_id')).val(),child:$('#'+$(this).data('parent_id')+'-parent-comment').val()}
               }).done(function (res) {
                   if(res !=0){
                       if(type=='discussion'){
                        $('body').find('#discussioReply'+_id).append(res);
                       } else if(type== 'post' ||type == 'trip' || type == 'report' || type == 'trip-place' || type == 'trip-media' || type == 'event'){
                            $('body').find('#comments'+_id).prepend(res);
                            $('body').find('#post-comment-'+_id).val('');
                            $('body').find('post-image-'+_id).val('');
                            Comment_Show.reload(reload_obj);
                       }else{
                            $(res).insertAfter('#commentRow'+_id);
                            $('body').find('#post-comment-'+_id).val('');
                            $('body').find('post-image-'+_id).val('');
                       }
                   }
               });

           }
       });

        $('body').on('click', '.cancel-comment-btn', function (e) {
            var form = $(this).closest('form');
            form.find(".note-placeholder").removeClass('d-none');
            $(this).closest('.mobile--padding').hide()
            form.find('.medias').find('.removeFile').each(function(){
                $(this).click();
            });
            form.find('.medias').empty();
            form.find('textarea').val('');
            form.find(".note-editable").html('');
            form.find(".note-placeholder").show('');

            if ($(this).hasClass('comment-replay')) {
                $('#replyForm' + form.data('id')).hide()
            }
        });
       
       
        $('body').on('keypress','.note-editable', function(event){
           $(this).closest('form').find(".note-placeholder").removeClass('custom-placeholder')
           var keycode = (event.keyCode ? event.keyCode : event.which);
           if(keycode == '13'){
               $(this).closest('form').find(".note-placeholder").addClass('custom-placeholder')
               $(this).closest('form').find('button[type=submit]').click()
               event.preventDefault();
           }
       });
       
       
        $('body').on('submit', '.postCommentForm', function (e) {
            var form = $(this);
            var post_id = $(this).attr('data-id');
            var comment_id = $(this).attr('data-comment-id');
            var post_type = $(this).attr('data-type');
            var comments_count = $('.'+ post_id +'-comments-count').html();
            var opened_count = $('.'+ post_id +'-opened-comments-count').html();
            var text = form.find('textarea').val().trim();
            var files = form.find('.medias').find('div').length;
            form.find(".note-editable").html('');
            if(text == "" && files == 0) {
                $.alert({
                    icon: 'fa fa-warning',
                    title: '',
                    type: 'orange',
                    offsetTop: 0,
                    offsetBottom: 500,
                    content: '<span style="font-size:17px;">Please input text!</span>',
                });
                return false;
            }
            var values = $(this).serialize();
            $.ajax({
                method: "POST",
                url: "{{ route('post.new-comment') }}",
                data: values
            }).done(function (res) {
                if(res != 0) {
                    if (values.indexOf("edit_comment=1") >= 0) {
                        $('.comment_Row' + comment_id).each(function () {
                            $(this).closest('.comment-parent-div').html(res);
                        })
                    } else {
                        if(post_type == 'discussion'){
                            var root = form.parents('.post-add-comment-block');
                            var isSub = root.hasClass("sub-comment-block");
                            var _block = null;
                            if(isSub){
                                _block = form.parents('.post-tips-row').find(".sub-replies");
                            }
                            if(isSub){
                                _block.prepend(res).fadeIn('slow');
                            }else{
                                $('body').find('#discussioReply'+post_id).prepend(res);
                            }
                        } else if(post_type == 'post' ||post_type == 'trip' || post_type == 'report' || post_type == 'trip-place' || post_type == 'trip-media' || post_type == 'event') {
                            if ($('body').find('.comments'+post_id).length != 0) {
                                $('body').find('.comments'+post_id).prepend(res);
                            } else {
                                $('body').find('#comments'+post_id).prepend(res);
                            }
                            $('.'+ post_id +'-comments-count').html(parseInt(comments_count) + 1);
                            $('.'+ post_id +'-opened-comments-count').html(parseInt(opened_count) + 1);
                            form.closest('.post-comment-layer').find('.load-more-comment-link').attr('comskip', parseInt(opened_count) + 1)
                        } else {
                            $(res).insertAfter('#commentRow'+post_id);
                        }
                    }

                    form.find('.medias').find('.removeFile').each(function(){
                            $(this).trigger('click');
                            $(this)[0].click();
                    });
                    form.find('.medias').empty();
                    form.find('textarea').val('');
                    form.find(".note-editable").html('');
                }
            });
            e.preventDefault();
        });
        
        $(document).on('inview', '#loadMoreDescReplies', function (event, isInView) {
            if (isInView) {
                var nextPage = parseInt($('#reply_pagenum').val()) + 1;
                var discussion_id = $(this).attr('discId')

                $.ajax({
                    type: 'POST',
                    url: "{{route('discussion.get_more_replies')}}",
                    data: {pagenum: nextPage, discussion_id: discussion_id},
                    success: function (data) {
                        if (data != '') {
                            $('.disc-reply-content').append(data)
                            $('#reply_pagenum').val(nextPage);
                        } else {
                            $('#moreReplies').hide();
                        }
                    }
                });
            }
        });
        
        $("body").on("inview", "#loadMore", function (event, isInView) {
            if(isInView) {
                var _this = $(this);
                var post_id = _this.attr('data-id');
                var post_type = _this.attr('data-type');
                var skip = _this.attr('comskip');
                var pagenum = parseInt(_this.attr('pagenum')) + 1
                var loaded_comment = $('.'+ post_id +'-opened-comments-count').html()
                $.ajax({
                    type: "POST",
                    url: "{{ route('post.load-more-comment') }}",
                    data: {'post_id':post_id, 'post_type': post_type, 'pagenum': pagenum, 'skip': skip},
                    success: function (res) {
                        console.log(res)
                        if(res.count > 0){
                            if(post_type !='discussion'){
                                $('body').find('#comments'+post_id).append(res.view);
                                $('.'+ post_id +'-opened-comments-count').html(parseInt(loaded_comment) + res.count)
                            }else{
                                $('body').find('#discussioReply'+post_id).append(res.view);
                            }
                        }   
                        if(res.count < 10){
                            _this.hide()
                        }
                        
                        _this.attr('pagenum', pagenum)
                    }
                });
            }
        })

        //load more comment
        $('body').on('click', '.load-more-comment-link', function (e) {
            var _this = $(this);
            var post_id = _this.attr('data-id');
            var post_type = _this.attr('data-type');
            var skip = _this.attr('comskip');
            var pagenum = parseInt(_this.attr('pagenum')) + 1;
            var loaded_comment = $('.'+ post_id +'-opened-comments-count').html();

            $.ajax({
                method: "POST",
                url: "{{ route('post.load-more-comment') }}",
                data: {'post_id':post_id, 'post_type': post_type, 'pagenum': pagenum, 'skip': skip}
            }).done(function (res) {
                if(res.count > 0){
                    if(post_type !='discussion'){
                        $('body').find('.comments' + post_id).append(res.view);
                        $('body').find('#comments' + post_id).append(res.view);
                        $('.'+ post_id +'-opened-comments-count').html(parseInt(loaded_comment) + res.count)
                    }else{
                         $('body').find('#discussioReply'+post_id).append(res.view);
                    }
                }
                if(res.count < 10){
                    _this.hide()
                }
                _this.attr('pagenum', pagenum)
            });
            e.preventDefault();
        });
        
        
        
         $(document).on('change', '.commentfile', function (e) {
            var post_id = $(this).attr('data-id')
            $(this).closest('form').find('.medias').find('.removeFile').each(function(){
                        $(this).click();
                    });
       
                mediaUploadFile($(this), $(this).closest('form'), post_id);

        })

        // More/less content
       $(document).on('click', ".read-more-link", function(){
         $(this).closest('.comment-txt').find('.less-content').hide()
         $(this).closest('.comment-txt').find('.more-content').show()
         $(this).hide()
       });

       $(document).on('click', ".read-less-link", function(){
         $(this).closest('.more-content').hide()
         $(this).closest('.comment-txt').find('.less-content').show()
         $(this).closest('.comment-txt').find('.read-more-link').show()
       });

        // -----START----- Post comment
        $('body').on('mouseover', '.news-feed-comment', function () {
            $(this).find('.post-com-top-action .dropdown').show()
        });

        $('body').on('mouseout', '.news-feed-comment', function () {
            $(this).find('.post-com-top-action .dropdown').hide()
        });

        $('body').on('mouseover', '.doublecomment', function () {
            $(this).find('.post-com-top-action .dropdown').show()
        });

        $('body').on('mouseout', '.doublecomment', function () {
            $(this).find('.post-com-top-action .dropdown').hide()
        });



        $('body').on('click', '.postCommentDelete', function (e) {
            e.preventDefault();
            var _this = $(this);
            var commentId = _this.attr('id');
            var post_id = _this.data('post');
            var comments_count = $('.' + post_id + '-comments-count').html()
            var reload_obj = _this.closest('.post-block');
            var type = _this.data('type');
            var parent_id = _this.data('parent')
            $.confirm({
                title: 'Delete Comment!',
                content: 'Are you sure you want to delete this comment? <div class="mb-3"></div>',
                columnClass: 'col-md-5 col-md-offset-5',
                closeIcon: true,
                offsetTop: 0,
                offsetBottom: 500,
                scrollToPreviousElement:false,
                scrollToPreviousElementAnimate:false,
                buttons: {
                    cancel: function () {},
                    confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'shift'],
                        action: function () {
                            $.ajax({
                                method: "POST",
                                url: "{{ route('post.commentdelete') }}",
                                data: {comment_id: commentId}
                            })
                                    .done(function (res) {
                                        $('#commentRow' + commentId).fadeOut();
                                        $('.' + post_id + '-comments-count').html(parseInt(comments_count) - 1);
                                        if (type == 1) {
                                            $('#commentRow' + commentId).parent().remove();
                                            Comment_Show.reload(reload_obj);
                                        } else {
                                            if (_this.closest('.post-block').find('*[data-parent_id="' + parent_id + '"]:visible').length == 1) {
                                                $('#commentRow' + parent_id).find('.postCommentDelete').removeClass('d-none')
                                                $('#commentRow' + parent_id).find('.delete-line').removeClass('d-none')
                                            }
                                        }
                                    });
                        }
                    }
                }
            });
        });


        $('body').on('click', '.postCommentLikes', function (e) {
            var _this = $(this);
            commentId = $(this).attr('id');
            dataType = $(this).attr('data-type');

            $.ajax({
                method: "POST",
                url: "{{ route('post.commentlikeunlike') }}",
                data: {
                    comment_id: commentId,
                    type: dataType
                }
            }).done(function (res) {
                var result = JSON.parse(res);
                if (result.status == 'yes') {
                    _this.find('i').addClass('fill');
                    _this.parent().find('.comment-likes-block').prepend('<span>' + result.name + '</span>')
                } else if (result.status == 'no') {
                    _this.find('i').removeClass('fill');
                    _this.parent().find('.comment-likes-block span').each(function () {
                        if ($(this).text() == result.name)
                            $(this).remove();
                    });
                }
                _this.find('.comment-like-count').html(result.count);
            });
            e.preventDefault();
        });

        $('body').on('click', '.comment-likes-block', function (e) {
            var _this = $(this);
            commentId = $(this).attr('data-id');

            $.ajax({
                method: "POST",
                url: "{{ route('post.commentlikeusers') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        $('.post-comment-like-users').html(result.text)
                        $('#commentslikePopup').find('.comment-like-count').html(result.count)
                        $('#commentslikePopup').modal('show')
                    });
            e.preventDefault();
        });


        $('body').on('click', '.checkinCommentLikes', function (e) {
            obj = $(this);
            commentId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('post.checkincommentlikeunlike') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        console.log(result);
                        if (res.status == 'yes') {

                        } else if (res == 'no') {

                        }
                        //console.log('#post_like_count_' + postId);
                        obj.html(result.count + " @lang('home.likes')");
                    });
            e.preventDefault();
        });

        $('body').on('click', '.post-edit-comment', function (e) {
            var _this = $(this);
            var commentId = _this.attr('data-id');

            if (_this.closest('.comment-parent-div').find('.comment-popup' + commentId).length > 0) {
                var comment_popup = $('.comment-popup' + commentId);
                var parentBlock = comment_popup.find('.commentEditForm' + commentId);
                comment_popup.find('.comment-text-' + commentId + ' p').addClass('d-none');
                comment_popup.find(".post-image-container").addClass('d-none');
                comment_popup.find(".note-placeholder").addClass('d-none');
            } else {
                parentBlock = $('.commentEditForm' + commentId);
                parentBlock.find(".note-placeholder").addClass('d-none');
                $('.comment-text-' + commentId + ' p').addClass('d-none');
                $('.comment_Row' + commentId).find(".post-image-container").addClass('d-none');
            }

            setTimeout(function () {
                parentBlock.find('.note-editable').html($('textarea[data-id="text' + commentId + '"]').html());
            }, 150);

            editPostCommentMedia(commentId);
            CommentEmoji(parentBlock);

            parentBlock.removeClass('d-none');
            parentBlock.find(".note-placeholder").removeClass('custom-placeholder');
            parentBlock.find(".emojionearea-button").removeClass('d-none');
            e.preventDefault();
        });

        $('body').on('click', '.edit-cancel-link', function (e) {
            var _this = $(this);

            var commentId = _this.attr('data-comment_id');
            $('.commentEditForm' + commentId).addClass('d-none')
            $('.comment-text-' + commentId + ' p').removeClass('d-none')

            e.preventDefault();
        });
        
        
        $('body').on('click', '.post-r-reply-comment-link', function (e) {
            var comment_id = $(this).attr('data-comment_id')
            replyPostComment(comment_id);
        });


        $('body').on('click', '.edit-post-link', function (e) {
            var _this = $(this);
            var commentId = _this.attr('data-comment_id');
            var form = _this.closest('.commentEditForm' + commentId);
            var text = form.find('textarea').val().trim();
            var files = form.find('.medias').find('div').length;
            var comment_type = form.find("input[name='comment_type']").val();

            if (text == "" && files == 0)
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: '',
                    type: 'orange',
                    offsetTop: 0,
                    offsetBottom: 500,
                    content: 'Please input text or select files!',
                });
                return false;
            }
            var values = form.serialize();
            $.ajax({
                method: "POST",
                url: "{{ route('post.commentedit') }}",
                data: values
            })
                    .done(function (res) {
                        form.find('.medias').find('.removeFile').each(function () {
                            $(this).click();
                        });
                        if (comment_type == 1) {
                            $('#commentRow' + commentId).parent().html(res);
                        } else {
                            $('#commentRow' + commentId).replaceWith(res);
                        }
                    });
            e.preventDefault();


            e.preventDefault();
        });

        $('body').on('click', '.remove-media-file', function (e) {
            var media_id = $(this).attr('data-media_id');
            $(this).closest('.medias').append('<input type="hidden" name="existing_media[]" value="' + media_id + '">')
            $(this).parent().remove();
        });


        $('body').on('click', '.postCommentReply', function (e) {
            var commentId = $(this).attr('id');
            CommentEmoji( $(this).parents('.post-comment-wrapper').find('.reply_Form' + commentId).find('.postCommentForm'));
        
            $('.emojionearea-button').removeClass('d-none');
            $('.note-placeholder').addClass('custom-placeholder');
            $(this).closest('.post-comment-wrapper').find('.reply_Form' + commentId).toggle();
            e.preventDefault();
        });




        // follow post comment likes user
        $('body').on('click', '.pcl-follow', function () {
            var _this = $(this);
            var user_id = $(this).attr('data-id');
            var follow_url = "{{  route('profile.follow', ':user_id')}}";
            var url = follow_url.replace(':user_id', user_id);

            $.ajax({
                method: "POST",
                url: url,
                data: {type: "follow"}
            })
                    .done(function (res) {
                        if (res.success == true) {
                            _this.html('unfollow');
                            _this.removeClass('pcl-follow');
                            _this.removeClass('btn-light-primary');
                            _this.addClass('pcl-unfollow');
                            _this.addClass('btn-light-grey');
                        }
                    });
        });

        // unfollow post comment likes user
        $('body').on('click', '.pcl-unfollow', function () {
            var _this = $(this);
            var user_id = $(this).attr('data-id');
            var unfollow_url = "{{  route('profile.unfolllow', ':user_id')}}";
            var url = unfollow_url.replace(':user_id', user_id);

            $.ajax({
                method: "POST",
                url: url,
                data: {type: "unfollow"}
            })
                    .done(function (res) {
                        _this.html('follow');
                        _this.removeClass('pcl-unfollow');
                        _this.removeClass('btn-light-grey');
                        _this.addClass('pcl-follow');
                        _this.addClass('btn-light-primary');
                    });
        });

        // -----END----- Post comment

        // -----START----- Media comment
       
        
        $('body').on('click', '.media-edit-comment', function (e) {
            var _this = $(this);

            var commentId = _this.attr('data-id');
            editMediaComment(commentId)
            var mediaId = _this.attr('data-media');
            $('.mediaCommentEditForm' + commentId).removeClass('d-none')
            $('.media-comment-text-' + commentId + '>div').addClass('d-none')
            $('.media-comment-text-' + commentId + '>p').addClass('d-none')
            
            CommentEmoji($('.mediaCommentEditForm' + commentId));
            $('.mediaCommentEditForm' + commentId).find(".note-placeholder").removeClass('custom-placeholder')

            e.preventDefault();
        });
        
        $('body').on('click', '.edit-media-comment-link', function (e) {
            var _this = $(this);
            var commentId = _this.attr('data-comment_id');
            var form = _this.closest('.mediaCommentEditForm' + commentId);
            var text = form.find('textarea').val().trim();
            var files = form.find('.medias').find('div').length;
            var comment_type = form.find("input[name='comment_type']").val();

            if (text == "" && files == 0)
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: '',
                    type: 'orange',
                    offsetTop: 0,
                    offsetBottom: 500,
                    content: 'Please input text or select files!',
                });
                return false;
            }
            var values = form.serialize();
            $.ajax({
                method: "POST",
                url: "{{ route('media.commentedit') }}",
                data: values
            })
                    .done(function (res) {
                        form.find('.medias').find('.removeFile').each(function () {
                            $(this).click();
                        });
                        if (comment_type == 1) {
                            $('#mediaCommentRow' + commentId).parent().html(res);
                        } else {
                            $('#mediaCommentRow' + commentId).replaceWith(res);
                        }
                    });
            e.preventDefault();
        });
        
        $('body').on('click', '.postmediaCommentDelete', function (e) {
            
            var _this = $(this);
            var commentId = _this.attr('id');
            var postId = _this.attr('postid');
            var media_comments_count = $('.' + postId + '-media-comments-count').html();
            var reload_obj = _this.closest('.post-block');
            var type = _this.data('type');
            var parent_id = _this.data('parent')

            $.confirm({
                title: 'Delete Comment!',
                content: 'Are you sure you want to delete this comment? <div class="mb-3"></div>',
                columnClass: 'col-md-5 col-md-offset-5',
                closeIcon: true,
                offsetTop: 0,
                offsetBottom: 500,
                scrollToPreviousElement:false,
                scrollToPreviousElementAnimate:false,
                buttons: {
                    cancel: function () {},
                    confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'shift'],
                        action: function () {
                            $.ajax({
                                method: "POST",
                                url: "{{ route('media.commentdelete') }}",
                                data: {comment_id: commentId}
                            })
                            .done(function (res) {
                              
                                $('#mediaCommentRow' + commentId).fadeOut();
                                $('.' + postId + '-media-comments-count').html(parseInt(media_comments_count) - 1);

                                if (type == 1) {
                                    $('#mediaCommentRow' + commentId).parent().remove();
                                    Comment_Show.reload(reload_obj);
                                } else {
                                    if (_this.closest('.post-block').find('*[data-parent_id="' + parent_id + '"]:visible').length == 1) {
                                        $('#mediaCommentRow' + parent_id).find('.postCommentDelete').removeClass('d-none')
                                        $('#mediaCommentRow' + parent_id).find('.delete-line').removeClass('d-none')
                                    }
                                }
                            });
                        }
                    }
                }
            });
        });
        
        $('body').on('click', '.remove-media-comment-file', function (e) {
            var media_id = $(this).attr('data-media_id');
            $(this).closest('.medias').append('<input type="hidden" name="existing_medias[]" value="' + media_id + '">')
            $(this).parent().remove();
        });
        
        $('body').on('click', '.edit-media-cancel-link', function (e) {
            var _this = $(this);

            var commentId = _this.attr('data-comment_id');
            $('.mediaCommentEditForm' + commentId).addClass('d-none')
            $('.media-comment-text-' + commentId + '>p').removeClass('d-none')
            $('.media-comment-text-' + commentId + '>div').removeClass('d-none')

            e.preventDefault();
        });

        $(document).on('change', '.media-file-input', function (e) {
            var comment_id = $(this).attr('data-comment_id')
                mediaUploadFile($(this), $(this).closest('form'), comment_id);

        })

        $(document).on('click', '.media-reply-comment-link', function (e) {
                var comment_id = $(this).attr('data-comment_id');
                replyMediaComment(comment_id)
        })

        $('body').on('click', '.mediaCommentReply', function (e) {
            commentId = $(this).attr('id');
            $('#mediaReplyForm' + commentId).show();
            CommentEmoji($('.mediaCommentReplyForm' + commentId));
            e.preventDefault();
        });


        $('body').on('click', '.postmediaCommentLikes', function (e) {
            var _this = $(this);
            commentId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('media.commentlikeunlike') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        var result = JSON.parse(res);
                        if (result.status == 'yes') {
                            _this.find('i').addClass('fill');
                            _this.parent().find('.media-comment-likes-block').prepend('<span>' + result.name + '</span>')
                        } else if (result.status == 'no') {
                            _this.find('i').removeClass('fill');
                            _this.parent().find('.media-comment-likes-block span').each(function () {
                                if ($(this).text() == result.name)
                                    $(this).remove();
                            });
                        }
                        _this.find('.comment-like-count').html(result.count);



                    });
            e.preventDefault();
        });

        //Discussion reply upvote
        $(document).on('click', '.upvote-link.up', function (e) {
            var reply_id = $(this).attr('id');
            var parent_content = $('.updown_' + reply_id)
            var _this = parent_content.find('.upvote-link.up');
            $.ajax({
                method: "POST",
                url: "{{route('discussion.upvote')}}",
                data: {replies_id: reply_id}
            })
            .done(function (res) {
                parent_content.find('.upvote-count').html(res.count_upvotes)
                parent_content.find('.downvote-count').html(res.count_downvotes)
                
                if(_this.hasClass('disabled')){
                    _this.removeClass('disabled')
                }else{
                    _this.addClass('disabled')
                }
                
                if(!parent_content.find('.upvote-link.down').hasClass('disabled')){
                    parent_content.find('.upvote-link.down').addClass('disabled')
                }
            });
                    
            e.preventDefault()
        });

        // //Discussion reply downvote
        $(document).on('click', '.upvote-link.down', function (e) {
            var reply_id = $(this).attr('id');
            var parent_content = $('.updown_' + reply_id)
            var _this = parent_content.find('.upvote-link.down');
            $.ajax({
                method: "POST",
                url: "{{route('discussion.downvote')}}",
                data: {replies_id: reply_id}
            })
            .done(function (res) {
                parent_content.find('.upvote-count').html(res.count_upvotes)
                parent_content.find('.downvote-count').html(res.count_downvotes)
                
                if(_this.hasClass('disabled')){
                    _this.removeClass('disabled')
                }else{
                    _this.addClass('disabled')
                }
                
                if(!parent_content.find('.upvote-link.up').hasClass('disabled')){
                    parent_content.find('.upvote-link.up').addClass('disabled')
                }

            });
            e.preventDefault()
        });

        $('body').on('click', '.media-comment-likes-block', function (e) {
            var _this = $(this);
            commentId = $(this).attr('data-id');
            var like_user_count = _this.parent().find('.comment-like-count').text()
            $.ajax({
                method: "POST",
                url: "{{ route('media.commentlikeusers') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        $('.post-comment-like-users').html(res)
                        $('#commentslikePopup').find('.comment-like-count').html(like_user_count)
                        $('#commentslikePopup').modal('show')
                    });
            e.preventDefault();
        });

        // -----END----- Media comment
        $('.comment-btn').on('click',function(){
            var id = $(this).data('id');
            $('#following'+id).empty();
            
        });

    });

    function comment_textarea_auto_height(element, type) {
        element.style.height = "5px";
        element.style.paddingBottom = '30px';
        if (type == 'reply') {
            jQuery(element).closest('.post-reply-block').find('.post-create-controls').css('top', (element.scrollHeight - 25) + "px")
        } else if (type == 'edit') {
            jQuery(element).closest('.post-edit-block').find('.post-create-controls').css('top', (element.scrollHeight - 15) + "px")
        } else {
            jQuery(element).closest('.post-regular-block').find('.post-create-controls').css('top', (element.scrollHeight - 25) + "px")
        }
        element.style.height = (element.scrollHeight) + "px";
    }

    function editPostCommentMedia(id) {
        $('#commenteditfile' + id).on('change', function () {
            mediaUploadFile($(this), $(".commentEditForm" + id), id);
        });
    }

    function editMediaComment(id) {
        $('#mediacommenteditfile' + id).on('change', function () {
            mediaUploadFile($(this), $(this).closest('form'), id);
//            mediaUploadFile($(this), $(".mediaCommentEditForm" + id), id);
        });
    }


    function replyPostComment(id) {
        var form = $('.commentReplyForm' + id);
        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: '<span style="font-size:17px;">Please input text or select files!</span>',
            });
            return false;
        }
        var values = form.serialize();

        $.ajax({
            method: "POST",
            url: "{{ route('post.commentreply') }}",
            data: values
        })
                .done(function (res) {
                    form.parent().parent().before(res);
                    form.find('.medias').find('.removeFile').each(function () {
                        $(this).click();
                    });
                    $('#commentRow' + id).find('.postCommentDelete').addClass('d-none');
                    $('#commentRow' + id).find('.delete-line').addClass('d-none');
                    form.find('textarea').val('');
                    form.find(".note-editable").html('');
                    form.find(".note-placeholder").addClass('custom-placeholder')
                    form.find('.post-create-controls').addClass('d-none');
                    form.find('.emojionearea-button').addClass('d-none');
                    form.find('.medias').empty();

                });
    }

    function replyMediaComment(id) {
        var form = $('.mediaCommentReplyForm' + id);
        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div').length;

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        var values = form.serialize();

        $.ajax({
            method: "POST",
            url: "{{ route('media.commentreply') }}",
            data: values
        })
                .done(function (res) {
                    form.parent().parent().before(res);
                    form.find('.medias').find('.removeFile').each(function () {
                        $(this).click();
                    });
                    $('#mediaCommentRow' + id).find('.postCommentDelete').addClass('d-none');
                    $('#mediaCommentRow' + id).find('.delete-line').addClass('d-none');
                    
                    form.find('textarea').val('');
                    form.find(".note-editable").html('');
                    form.find(".note-placeholder").addClass('custom-placeholder')
                    form.find('.post-create-controls').addClass('d-none');
                    form.find('.emojionearea-button').addClass('d-none');
                    
                    form.find('.medias').empty();

                });
    }

    $(document).on('click', '[data-tab]', function (e) {
         e.preventDefault();
      
        var Item = $(this).attr('data-tab');
        $('[data-content]').css('display', 'none');
        $('[data-content=' + Item + ']').css('display', 'block');
        CommentEmoji($(this).closest('.post-block').find('.postCommentForm'))
        
        $('.emojionearea-button').removeClass('d-none')
        $('.note-placeholder').addClass('custom-placeholder')
      
    });

    $(document).on('click', 'i.fa-camera', function () {
        var target = $(this).attr('data-target');
        var target_el = $('#' + target);
        target_el.trigger('click');
    })
    
    
    //Comment summernote emoji
    function CommentEmoji(element){
        var placeholder = 'Write a comment';
        
        if((element.find(".post-comment-emoji").attr('placeholder'))){
            placeholder = element.find(".post-comment-emoji").attr('placeholder')
        }
        
         element.find(".post-comment-emoji").summernote({
            airMode: false,
            focus: false,
            disableDragAndDrop: true,
            placeholder: placeholder,
            toolbar: [
                ['insert', ['emoji']],
                ['custom', ['textComplate']],
            ],
            callbacks: {
                onFocus: function(e) {
                   $(e.target).closest('form').find(".note-placeholder").removeClass('custom-placeholder')
                },
                 onInit: function(e) {
                   e.editingArea.find(".note-placeholder").addClass('custom-placeholder')
                   e.editingArea.find('.note-editable').html('')
                },
                onChange: function(contents, $editable) {
                    if(contents !='<br>' && contents !=''){
                        $editable.closest('form').find(".note-placeholder").addClass('d-none')
                    }else{
                        $editable.closest('form').find(".note-placeholder").removeClass('d-none')
                    }
                   
                   
                  }
              }
            });
    }
    
    
    function mediaUploadFile(_this, form_obj, id) {

        if ((form_obj.find('.medias').children().length + _this[0].files.length) > 10)
        {
            alert("The maximum number of files to upload is 10.");
            _this.val("");
            return;
        }
        if (window.File && window.FileReader && window.FileList && window.Blob) { //check File API supported browser

            var data = _this[0].files;
            $.each(data, function (index, file) { //loop though each file

                var upload = new Upload(file);
                var uid = Math.floor(Math.random() * 1000000);
                if (/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)) {

                    var fRead = new FileReader();
                    fRead.onload = (function (file) {
                        return function (e) {

                            var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element

                            var button = $('<span/>').addClass('close').addClass('removeFile').click(function () {     //create close button element
                                var filename = upload.getName();
                                commentFileDelete(filename, this, form_obj.find('*[data-id="pair'+id+'"]').val());
                            }).append('<span>&times;</span>');

                            var imgitem = $('<div>').attr('uid', uid).append(img); //create Image Wrapper element
                            imgitem = $('<div/>').addClass('img-wrap-newsfeed').append(imgitem).append(button);
                            form_obj.find('.medias').append(imgitem);
                            upload.doUpload(uid, form_obj.find('*[data-id="pair'+id+'"]').val());

                        };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.

                } else if (/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type)) {

                    var fRead = new FileReader();
                    fRead.onload = (function (file) {
                        return function (e) {

                            var video = $('<video style="object-fit:cover;" width="151" height="130" controls>' +
                                    '<source src=' + e.target.result + ' type="video/mp4">' +
                                    '<source src="movie.ogg" type="video/ogg">' +
                                    '</video>');

                            var button = $('<span/>').addClass('close').addClass('removeFile').click(function () {     //create close button element
                                var filename = upload.getName();
                                commentFileDelete(filename, this, form_obj.find('*[data-id="pair'+id+'"]').val());
                            }).append('<span>&times;</span>');

                            var videoitem = $('<div/>').attr('attr', uid).append(video);
                            videoitem = $('<div/>').addClass('img-wrap-newsfeed').append(videoitem).append(button);
                            form_obj.find('.medias').append(videoitem);
                            upload.doUpload(uid, form_obj.find('*[data-id="pair'+id+'"]').val());
                        };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.

                }
            });
            form_obj.find('.note-editable').focus();
            _this.val("");

        } else {
            alert("Your browser doesn't support File API!"); //if File API is absent
        }
    }

</script>

<style>

    .thumb {
        width: 45px;
        height: 45px;
        object-fit: cover;
    }
    
    .b-gray{
        background: #f5f5f5;
    }

    video::-webkit-media-controls-fullscreen-button {
        display: block;
    }

    video::-webkit-media-controls-mute-button {
        display: block;
    }

    video::-webkit-media-controls-current-time-display {
        font-size: 11px
    }

    video::-webkit-media-controls-time-remaining-display {
        font-size: 11px
    }

    .locationSelect {
        display: none;
        width: 50%
    }

    .locationLoad {
        display: none;
    }

    .post-alloptions {
        flex: 1;
        display: flex;
        align-items: center;
    }

    .select2-dropdown {
        z-index: 10000;
    }

    .img-wrap-newsfeed, .plus-icon
    {
        width: 151px;
        height: 130px;
        float: left;
        margin: 10px;
    }

    .img-wrap-newsfeed>div,
    .plus-icon>div
    {

        overflow: hidden;
        justify-content: center;
        display: flex;
    }

    .img-wrap-newsfeed .close
    {
        margin-top: -133px;
        margin-left: 135px;
        position:absolute;
        margin-right: 5px;
        opacity: 1;
    }

    .post-create-input .medias
    {
        margin-top: 2px;
        display: inline-block;
    }

</style>
