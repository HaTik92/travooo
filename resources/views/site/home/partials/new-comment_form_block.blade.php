@if(Auth::user())
<div class="post-add-comment-block new-coment-wrap">
    <div class="avatar-wrap">
        <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
    </div>

    <div class="add-comment-input-group n-post-comment-block">
        <form class="postCommentForm " method="POST" data-id="{{  isset($post_report_comment) ? $post->variable : $post->id }}" data-type="{{$post_type}}" autocomplete="off" enctype="multipart/form-data">
            {{ csrf_field() }}<input type="hidden" data-id="pair{{$post->id}}" name="pair" value="{{uniqid()}}"/>
            <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0">
                <div class="post-create-input n-post-create-input b-whitesmoke">
                    <textarea name="text" class="comment-reply-txt post-comment-emoji" placeholder="{{isset($placeholder)?$placeholder:''}}"></textarea>
                    <div class="n-post-img-wrap">
                        <div class='post-buttons-comment'>
                            <button class="cancel-comment-btn cancel-post__btn" type="button">Cancel</button>
                            <button class="post__btn">Post</button>
                        </div>
                        <input type="file" name="file" class="commentfile" id="{{$post_type}}commentfile{{$post->id}}" data-id="{{$post->id}}"  style="display:none">
                        <i class="fa fa-camera click-target" data-target="{{$post_type}}commentfile{{$post->id}}"></i>
                    </div>
                </div>
                <div class="medias b-whitesmoke">
                </div>
            </div>
            <input type="hidden" name="post_id" value="{{  isset($post_report_comment) ? $post->variable : $post->id }}">
            <input type="hidden" name="post_type" value="{{$post_type}}">
            <button type="submit"  class="d-none"></button>
        </form>
        <div class="comment-rseply-media"></div>
    </div>
</div>
@endif