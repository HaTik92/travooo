@php

use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Posts\Posts;


$locations = collect([]);

if(Auth::check()){

$user = auth()->user();

if($user->travelstyles && @$user->travelstyles->count()) {

$followingCountries = @$user->followingCountries;
$followingCities = @$user->followingCities;

$placesCount = @$followingCountries->count() + @$followingCities->count();

if($placesCount < 20) {
    $tsCountries = collect([]);
    $tsCities = collect([]);

    $user->travelstyles->each(function($ts) use(&$tsCountries, &$tsCities) {
        $tsCountries = $tsCountries->merge($ts->countriesLifestyles->pluck('countries_id'));
        $tsCities = $tsCities->merge($ts->citiesLifestyles->pluck('cities_id'));
    });

    $post_data = [
        'countries' => $tsCountries->unique(),
        'cities' => $tsCities->unique()
    ];

    $post_data = json_encode($post_data);
}

$locations = json_decode($post_data);
}
if(@$locations){
$countries = Countries::whereIn('id', @$locations->countries)->get();
$cities = Cities::whereIn('id', @$locations->cities)->get();

$countries = @$countries->map(function($country) {
    $followersCount = count(@$country->followers);

    return collect([
        'id' => $country->id,
        'title' => @$country->transsingle->title,
        'description' => !empty(@$country->transsingle->address) ? @$country->transsingle->address : '',
        'img' => check_country_photo(@$country->getMedias[0]->url, 180),
        'followers' => $followersCount > 1000 ? floor($followersCount/1000) . 'K' : $followersCount,
        'type' => 'country',
        'url' => url('country/'.$country->id)
    ]);
})->toArray();

$cities = @$cities->map(function($city) {
    $followersCount = count(@$city->followers);

    return collect([
        'id' => $city->id,
        'title' => @$city->transsingle->title,
        'description' => !empty(@$city->transsingle->address) ? @$city->transsingle->address : '',
        'img' => check_city_photo(@$city->getMedias[0]->url, 180),
        'followers' => $followersCount > 1000 ? floor($followersCount/1000) . 'K' : $followersCount,
        'type' => 'city',
        'url' => url('city/'.$city->id)
    ]);
})->toArray();

$locations = collect(array_merge(@$cities, @$countries));

}
$id = str_random(5);
}
@endphp
@if($locations)
<div class="post-block">
    <div class="post-side-top">
        <h3 class="side-ttl"><i class="fas fa-thumbs-up"></i> Recommended Places to Follow <span class="count">{{ $locations->count() }}</span></h3>
        <div class="side-right-control">
            <a href="#" id="recommended-places-slider-prev-{{ $id }}" class="slide-link recommended-places-slider-prev"><i class="trav-angle-left"></i></a>
            <a href="#" id="recommended-places-slider-next-{{ $id }}" class="slide-link recommended-places-slider-next"><i class="trav-angle-right"></i></a>
        </div>
    </div>
    <div class="post-side-inner">
        <div class="post-slide-wrap slide-hide-right-margin">
            <div class="lSSlideOuter ">
                <div class="lSSlideWrapper usingCss" style="transition-duration: 400ms; transition-timing-function: ease;">
                    <ul id="recommended-places-slider-{{$id}}" class="post-slider recommended-places-slider lightSlider lsGrab lSSlide" style="width: 1000px; height: 300px; padding-bottom: 0%; transform: translate3d(0px, 0px, 0px);">
                        @foreach($locations as $location)
                        <li class="lslide active" style="margin-right: 20px;">
                            <a href="{{ $location['url'] }}" target="_blank">
                                <img class="lazy" alt="{{ $location['title'] }}" style="width: 230px; height: 300px;" src="{{ $location['img'] }}">
                                <div class="post-slider-caption transparent-caption">
                                    <p class="post-slide-name">{{ $location['title'] }}</p>
                                    <div class="post-slide-description">
                                        <i class="trav-set-location-icon"></i> {{ $location['title'] }}
                                    </div>
                                </div>
                                <div class="place-badge">{{ ucfirst($location['type']) }}</div>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        // Post type - shared place, slider
        var homeRecommendedePlacesSlider = $('#recommended-places-slider-{{$id}}').lightSlider({
            autoWidth: true,
            slideMargin: 20,
            pager: false,
            controls: false
        });

        $('#recommended-places-slider-prev-{{ $id }}').click(function(e){
            e.preventDefault();
            homeRecommendedePlacesSlider.goToPrevSlide();
        });
        $('#recommended-places-slider-next-{{ $id }}').click(function(e){
            e.preventDefault();
            homeRecommendedePlacesSlider.goToNextSlide();
        });
    })
</script>

@endif