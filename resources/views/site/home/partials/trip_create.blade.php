<?php
$trip = \App\Models\TripPlans\TripPlans::find($post->variable);
// dd($trip);
?>
@if(!is_object($trip))
<div class="post-block">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <img src="{{check_profile_picture($trip->author->profile_picture)}}" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{url('profile/'.$trip->author->id)}}">{{$trip->author->name}}</a>
                    {!! get_exp_icon($trip->author) !!}
                </div>
                <div class="post-info">
                    @lang('home.created_a') <b>@lang('trip.trip_plan')</b> yesterday at 10:33am
                </div>
            </div>
        </div>
        <div class="post-top-info-action">
            <div class="dropdown">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <i class="trav-angle-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow">
                    <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                            <i class="trav-share-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>@lang('home.share')</b></p>
                            <p>@lang('home.spread_the_word')</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                            <i class="trav-heart-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>@lang('home.add_to_favorites')</b></p>
                            <p>@lang('home.save_it_for_later')</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#">
                        <span class="icon-wrap">
                            <i class="trav-flag-icon-o"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>@lang('home.report')</b></p>
                            <p>@lang('home.help_us_understand')</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="post-image-container post-follow-container">
        <ul class="post-image-list">
            <li>
                <img src="{{get_plan_map($trip, 595, 335)}}" alt="" style="width:595px;height:335px;">
            </li>
        </ul>
        <div class="post-follow-block">
            <div class="follow-txt-wrap">
                <div class="follow-txt">
                    <p class="follow-destination">{{$trip->title}}</p>
                    <div class="follow-tag-wrap">
                        <!--<span class="follow-tag">solo</span>
                            <span class="follow-tag">urban</span>-->
                    </div>
                </div>
            </div>
            <div class="follow-btn-wrap">
                <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right"
                    onclick="location.href='{{ route('trip.view', $trip->id) }}';">
                    @lang('home.view_plan')
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                </button>
            </div>
        </div>
    </div>
    <div class="post-footer-info">
        <div class="post-foot-block post-reaction">
            <img src="./assets2/image/reaction-icon-smile-only.png" alt="smile">
            <span><b>6</b> @lang('other.reactions')</span>
        </div>
        <div class="post-foot-block">
            <i class="trav-comment-icon"></i>
            <ul class="foot-avatar-list">
                <li><img class="small-ava" src="./assets2/image/photos/profiles/78_refashionista_sheri_pavlovic.jpg"
                        alt="ava"></li>
                <li><img class="small-ava" src="./assets2/image/photos/profiles/Randy.jpg" alt="ava"></li>
                <li><img class="small-ava"
                        src="./assets2/image/photos/profiles/Stacy-Headshot-April-2014-closeup-360x360.jpg" alt="ava">
                </li>
            </ul>
            <span>@choice('comment.count_comment', 20, ['count' => 20])</span>
        </div>
    </div>
</div>
@endif



<script>
    var trip_id  ='{{$post->variable}}';
    var homeTripSlider=[];
    console.log('next-btn_'+trip_id);
    $(document).ready(function(){
        var temp = $('.slider-div_'+trip_id).lightSlider({
            autoWidth: true,
            slideMargin: 10,
            pager: false,
            controls: false
        });
        homeTripSlider['{{$post->variable}}'] =temp;
        $('.trip-plan-home-slider-prev prev-btn_'+trip_id).click(function(e){
            e.preventDefault();
            homeTripSlider['{{$post->variable}}'].goToPrevSlide(); 
        });
        $('.trip-plan-home-slider-next next-btn_'+trip_id).click(function(e){
            e.preventDefault();
            homeTripSlider['{{$post->variable}}'].goToNextSlide(); 
        });
    });
</script>