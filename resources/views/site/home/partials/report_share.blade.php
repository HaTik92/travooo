<?php
    $post_type = $post->type;
    $report = \App\Models\Reports\Reports::find($post->variable);
?>
@if(isset($report->shares) && count($report->shares)>0)
@foreach($report->shares as $share)
<div class="post-block">
        <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <img src="{{check_profile_picture($share->author->profile_picture)}}" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{url('profile/'.$share->author->id)}}">{{$share->author->name}}</a>
                    {!! get_exp_icon($share->author) !!}
                </div>
                <div class="post-info">
                  Shared Report {{diffForHumans($post->time)}}
                </div>
            </div>
        </div>
            @if($share->author->id !== Auth::user()->id)
                <div class="post-top-info-action">
                    <div class="dropdown">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="trav-angle-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="ReportShare">
                        @include('site.home.partials._info-actions', ['post'=>$report])

                    </div>
                </div>
            @endif
        </div>
    </div>
    @if($share->text)
    <div class="post-txt-wrap" dir="auto">
        <p class="post-txt-lg" dir="auto">{{$share->text}}</p>
    </div>
    @endif
    <div class="travlog-info-block">
        <div class="travlog-info-inner">

            <div class="img-wrap">
                <a href="{{url('reports/'.$report->id)}}">
                    <img src="@if(isset($report->cover[0])){{$report->cover[0]->val}}@else {{asset('assets2/image/placeholders/no-photo.png')}} @endif" alt="{{$report->title}}">
                </a>
            </div>
            <div class="info-wrapper">
                <div class="country-name">
                    <a href="{{url('country/'.$report->countryOrCity->id)}}">
                        {{$report->countryOrCity->transsingle->title}}
                    </a>
                </div>
                <div class="info-title">
                    <a href="{{url('reports/'.$report->id)}}">
                        {{$report->title}}
                    </a>
                </div>
                <div class="foot-block">
                    <div class="author-side">
                        <div class="img-wrap">
                            <img class="ava" src="{{check_profile_picture($report->author->profile_picture)}}" alt="avatar" style="width:45px;height:45px;">
                        </div>
                        <div class="author-info">
                            <div class="author-name">By
                                <a href="{{url('profile/'.$report->author->id)}}"
                                    class="author-link">{{$report->author->name}}</a>
                                {!! get_exp_icon($report->author) !!}
                            </div>
                            <div class="time">
                                {{diffForHumans_2($report->created_at)}}
                            </div>
                        </div>
                    </div>
                    <div class="like-side">
                        <div class="post-foot-block">
                            <i class="trav-heart-fill-icon"></i>&nbsp;
                            <span>{{count($report->likes)}}</span>
                        </div>
                        <div class="post-foot-block">
                            <i class="trav-comment-icon"></i>&nbsp;
                            <span>{{count($report->comments)}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif
