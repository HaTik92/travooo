@foreach($likes as $like)
<div class="people-row">
    <div class="main-info-layer">
        <div class="img-wrap">
            <img class="ava" src="{{check_profile_picture($like->author->profile_picture) }}" alt="ava" style="width:50px;height:50px;">
        </div>
        <div class="txt-block">
            <div class="name">{{$like->author->name}}</div>
            {!! get_exp_icon($like->author) !!}
        </div>
    </div>
    <div class="button-wrap">
        @if(count($like->author->get_followers()->where('users_id', $like->author->id)->where('followers_id', Auth::user()->id)->get())>0)
        <button class="btn btn-light-grey btn-bordered pcl-unfollow" data-id="{{$like->author->id}}">Unfollow</button>
        @else
            <button class="btn btn-light-primary btn-bordered pcl-follow" data-id="{{$like->author->id}}" {{$like->author->id == Auth::user()->id?'disabled':''}}>Follow</button>
        @endif
    </div>
</div>
@endforeach
