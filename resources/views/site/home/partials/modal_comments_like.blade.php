<!-- comments like users popup -->
<div class="modal fade white-style"  id="commentslikePopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-660" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-modal-comment travlog-comment">
                <div class="post-comment-main">
                    <div class="comment-like-top-info">
                        <i class="fa fa-heart fill" aria-hidden="true"></i> 
                        <span class="comment-like-count">0</span>
                    </div>
                    <div class="comment-like-people-block">
                        <div class="post-comment-like-users">
                        </div>
                        <div id="loadMoreUsers" commentId="" class="load-more-comment-link-user d-none">
                            <input type="hidden" id="like_pagenum" value="1">
                            <div id="commentlikeloader" class="like-users-animation-content people-row">
                                <div class="com-like-avatar-wrap">
                                    <div class="like-top-avatar-wrap animation"></div>
                                    <div class="like-top-name animation"></div>
                                </div>

                                <div class="comment-like-follow">
                                    <div id="hide_comlike_loader" class="com-like-follow-block animation"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>