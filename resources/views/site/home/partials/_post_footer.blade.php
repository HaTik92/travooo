<div class="post-footer-info">
    <div class="post-foot-block post-reaction">
        <span class="post_like_button" id="{{$post->id}}">
            <a href='#'>
                <img src="{{asset('assets2/image/like.svg')}}" alt="Like this post"
                     style="width:16px;height:16px;margin:5px;vertical-align:baseline;">
            </a>
        </span>
        <span id="post_like_count_{{$post->id}}">
            @if(count($post->likes))<a href='#' class="post_likes_modal" id='{{$post->id}}'>@endif
                <b>{{count($post->likes)}}</b> Likes
            @if(count($post->likes))</a>@endif
        </span>
    </div>
    <div class="post-foot-block">
        <a href='#' data-tab='comments{{$post->id}}'>
            <i class="trav-comment-icon"></i>
        </a>

        <ul class="foot-avatar-list"></ul>
        <span><a href='#'
                 data-tab='comments{{$post->id}}'><span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span> @lang('comment.comments')</a></span>
    </div>
    <div class="post-foot-block">
        <span class="post_share_button" id="{{$post->id}}">
            <a href='#'>
                <i class="trav-share-icon"></i>
            </a>
        </span>
        <span id="post_share_count_{{$post->id}}"><a href='#'
                                                     data-tab='shares{{$post->id}}'><strong>{{count($post->shares)}}</strong> Shares</a></span>
    </div>
</div>