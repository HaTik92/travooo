<div topSort="{{count($comment->likes)}}" newSort="{{strtotime($comment->created_at)}}">
<div class="post-comment-row" id="checkincommentRow{{$comment->id}}">
    <div class="post-com-avatar-wrap">
        <img src="{{check_profile_picture($comment->author->profile_picture)}}" alt="">
    </div>
    <div class="post-comment-text">
        <div class="post-com-name-layer">
            <a href="#" class="comment-name">{{$comment->author->name}}</a>
            {!! get_exp_icon($comment->author) !!}
            <!--<a href="#" class="comment-nickname">@katherin</a>-->
        </div>
        <div class="comment-txt">
            <p>{!!$comment->text!!}</p>
        </div>

        <div class="comment-bottom-info">
            <!--
            <div class="com-reaction">
                <img src="./assets2/image/icon-smile.png" alt="">
                <span>21</span>
            </div>
            -->
            <div class="com-time" posttype="Checkincomment">{{diffForHumans($comment->created_at)}}
            - <a href="#" class="checkinCommentLikes" id="{{$comment->id}}">{{count($comment->likes)}} @lang('home.likes')</a>
            <!-- - <a href="#" class="postCommentReply" id="{{$comment->id}}">Reply</a> -->
            @if($comment->author->id==Auth::user()->id)
            - <a href="#" onclick="checkincomment_delete({{ $comment->id }}, this, event)" id="{{$comment->id}}" style="color:red">Delete</a>
            @endif
            - <a href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$comment->id}},this)"> Report</a>
            </div>
        </div>
    </div>
</div>

</div>
