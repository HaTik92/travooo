<?php
    $post_type = $post->action;
    $post = \App\Models\TripPlans\TripPlans::find($post->variable);
    $plan_comments = $post->comments;
?>

@if(is_object($post))
<div class="post-block">
    <div class="post-top-info-layer">
    <div class="post-top-info-wrap">
        <div class="post-top-avatar-wrap">
        <img src="{{check_profile_picture($post->author->profile_picture)}}" alt="">
        <?php //exit("here"); ?>
        </div>
        <div class="post-top-info-txt">
        <div class="post-top-name">
            <a class="post-name-link"
            href="{{url('profile/'.$post->author->id)}}">{{$post->author->name}}</a>
            {!! get_exp_icon($post->author) !!}
        </div>
        <div class="post-info">
            Actived a <b>Plan Trip</b> {{diffForHumans_2($post->created_at)}}
        </div>
        </div>
    </div>
    <div class="post-top-info-action">
        <div class="dropdown">
        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="trav-angle-down"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Trip">
            @if($post->author->id !== auth()->id())
                @include('site.home.partials._info-actions', ['post'=>$post])
            @endif

        </div>
        </div>
    </div>
    </div>
    <div class="post-image-container post-follow-container">
    <ul class="post-image-list">
    @if(is_object($post->meidas))
        @foreach($post->medias as $photo)
        <li>
            <img src="{{$photo->media->url}}" alt="">
        </li>
        @endforeach
    @endif
    </ul>
    <div class="post-follow-block">
        <div class="follow-txt-wrap">
        <div class="follow-txt">
            <p class="follow-destination">{{ $post->title }}</p>
            <div class="follow-tag-wrap">
            <span class="follow-tag">solo</span>
            <span class="follow-tag">urban</span>
            </div>
        </div>
        </div>
        <div class="follow-btn-wrap">
        <button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" onclick="javascript:window.location.replace('{{url('trip/view')}}' + '/' + {{$post->id}});">
            View plan
            <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
        </button>
        </div>
    </div>
    </div>
    <div class="post-footer-info">
    <div class="post-foot-block post-reaction" style="cursor: pointer" onclick="tripslike('{{ $post_type }}', {{ $post->id }}, this)">
        <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
        <span><b>{{count($post->likes()->whereNull('places_id')->get())}}</b> Reactions</span>
    </div>
    <div class="post-foot-block" data-tab="tripscomment{{ $post->id}}">
        <i class="trav-comment-icon"></i>
        <ul class="foot-avatar-list">
        @if(is_object($post->users))
            @foreach($post->users as $user)
                <!-- <li><img class="small-ava" width="20" height="20" src="{{check_profile_picture($user->user->profile_picture)}}" alt="ava"></li> -->
            @endforeach
        @endif
        </ul>
        <span>{{ count($plan_comments) }} Comments</span>
    </div>
    </div>

    <div class="post-comment-layer" data-content="tripscomment{{$post->id}}" style='display:none;'>
        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <!-- <li onclick="commentSort('Top', this)">@lang('comment.top')</li>
                <li class="active" onclick="commentSort('New', this)">@lang('home.new')</li> -->
            </ul>
            <div class="comm-count-info">
            </div>
        </div>
        <div class="post-comment-wrapper sortBody" id="tripscomment{{$post->id}}">
            @if(count($plan_comments)>0)
                @foreach($plan_comments AS $comment)
                    @if(!user_access_denied($comment->author->id))
                         @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'trip', 'comment'=>$comment])
                @endif
                @endforeach
            @endif
        </div>
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-input">
                <form class="tripscomment{{$post->id}}" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" id="pair{{$post->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                    <div class="post-block post-create-block" id="createPostBlock" tabindex="0">
                        <div class="post-create-input">
                            <textarea name="text" id="tripscommenttext{{$post->id}}" class="textarea-customize"
                                style="display:inline;vertical-align: top;height:40px;" placeholder="@lang('comment.write_a_comment')"></textarea>
                            <div class="medias">
                                <!-- <div class="plus-icon" style="display: none;">
                                    <div>
                                        <span class="close" onclick="javascript:$('#commentfile').click();"><span style="font-size:110px;">+</span></span>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div class="post-create-controls">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <!-- <li class="post-options">
                                        <input type="file" name="file[]" id="commentfile{{$post->id}}" style="display:none" multiple>
                                        <i class="trav-camera click-target" data-target="commentfile{{$post->id}}"></i>
                                    </li> -->
                                </ul>
                            </div>

                            <button type="submit" class="btn btn-primary">@lang('buttons.general.send')</button>

                        </div>

                    </div>
                    <input type="hidden" name="post_id" value="{{$post->id}}"/>
                </form>

                </div>
            </div>
            <script>

            $('body').on('click', ".morelink", function(){
                var moretext = "see more";
                var lesstext = "see less";
                if($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });

                $('#tripscommenttext{{$post->id}}').keydown(function(e){
                    if(e.keyCode == 13 && !e.shiftKey){
                        e.preventDefault();
                    }
                });
                $('#tripscommenttext{{$post->id}}').keyup(function(e){
                    if(e.keyCode == 13 && !e.shiftKey){
                        $('.tripscomment{{$post->id}}').find('button[type=submit]').click();
                    }
                });
                $('body').on('submit', '.tripscomment{{$post->id}}', function (e) {
                    var form = $(this);
                    var text = form.find('textarea').val().trim();
                    // var files = $('.tripscomment{{$post->id}}').find('.medias').find('div').length;

                    if(text == "")
                    {
                        // alert('Please input text or select files.');
                        return false;
                    }
                    var values = form.serialize();
                    form.find('button[type=submit]').attr('disabled', true);
                    $.ajax({
                        method: "POST",
                        url: "{{ route('trip.comment') }}",
                        data: values
                    })
                        .done(function (res) {
                            $('#tripscomment{{$post->id}}').prepend(res);
                            // $('.tripscomment{{$post->id}}').find('.medias').empty();
                            $('.tripscomment{{$post->id}}').find('textarea').val('');
                            form.find('button[type=submit]').removeAttr('disabled');
                        });
                    e.preventDefault();
                });
            </script>
        @endif
    </div>
</div>

@endif
