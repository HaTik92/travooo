<?php
$hotel = \App\Models\Hotels\Hotels::find($post->variable);
?>
@if(is_object($hotel))
<div class="post-block">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <img src="{{check_profile_picture($post->user->profile_picture)}}" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{url('profile/'.$post->user->id)}}">{{$post->user->name}}</a>
                    {!! get_exp_icon($post->user) !!}
                </div>
                <div class="post-info">
                    @lang('home.started_following') <a href="{{ route('hotel.index', $hotel->id) }}"
                                                       class="link-place">{{$hotel->transsingle->title}}</a> {{diffForHumans($post->time)}}
                </div>
            </div>
        </div>
        @if ($post->user->id !== Auth::user()->id)
            <div class="post-top-info-action">
                <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Hotelfollow">
                        @include('site.home.partials._info-actions', ['post'=>$post])
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="post-image-container post-follow-container">
        <ul class="post-image-list">
            <li>
                <img src="https://s3.amazonaws.com/travooo-images2/th700/{{@$hotel->getMedias[0]->url}}" alt="">
            </li>
        </ul>
        <div class="post-follow-block">
            <div class="follow-txt-wrap">
                <div class="follow-flag-wrap">
                </div>
                <div class="follow-txt">
                    <p class="follow-name">
                        <a href='{{ route('hotel.index', $hotel->id) }}'>
                        {{$hotel->transsingle->title}}
                        </a>
                        <div class="follow-tag-wrap">
                        <span class="follow-tag">{{do_placetype($hotel->place_type)}}</span>
                        </div>
                    </p>
                    <div class="follow-foot-info">
                        <span>@lang('home.count_talking_about_this', ['count' => count($hotel->followers)])</span>
                    </div>
                </div>
            </div>
            <div class="follow-btn-wrap check-follow-place" data-id='{{$hotel->id}}'>

            </div>
        </div>
    </div>
    <div class="post-footer-info">
        <div class="post-foot-block post-reaction">
            <img src="./assets2/image/reaction-icon-smile-only.png" alt="smile">
            <span><b>6</b> @lang('other.reactions')</span>
        </div>
        <div class="post-foot-block">
            <i class="trav-comment-icon"></i>
            <ul class="foot-avatar-list">
                <li><img class="small-ava" src="./assets2/image/photos/profiles/78_refashionista_sheri_pavlovic.jpg" alt="ava"></li>
                <li><img class="small-ava" src="./assets2/image/photos/profiles/Randy.jpg" alt="ava"></li>
                <li><img class="small-ava" src="./assets2/image/photos/profiles/Stacy-Headshot-April-2014-closeup-360x360.jpg" alt="ava"></li>
            </ul>
            <span>@choice('comment.count_comment', 20 , ['count' => 20])</span>
        </div>
    </div>
</div>
@endif
