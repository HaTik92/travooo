@php

use App\Models\Place\Place;
use App\Models\PlacesTop\PlacesTop;

$user = auth()->user();
$topPlaces = PlacesTop::all()->pluck('places_id')->toArray();

$followedPlaces = $user->following_places->pluck('places_id')->toArray();

$diff = array_diff($topPlaces, $followedPlaces);
$locations = collect([]);

if(count($diff) && is_array($diff)) {
    $post_data = json_encode($diff);
    $places_id = json_decode($post_data);

    $places = collect([]);

    foreach (array_chunk($diff, 10) as $dif) {
        $places = $places->merge(Place::whereIn('id', $dif)->get());
    }

    $locations = $places->map(function($place) {
        return [
            'id' => $place->id,
            'title' => @$place->transsingle->title,
            'description' => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
            'img' => check_place_photo($place, 180),
            'type' => 'place',
            'url' => url('place/'.$place->id),
            'city' => @$place->city->transsingle->title,
            'placeType' => @$place->place_type
        ];
    });
}

$id = str_random(5);

@endphp

@if($locations->count())
<div class="post-block discover-destinations-block" dir="auto">
    <div class="post-side-top" dir="auto">
        <h3 class="side-ttl">
            <i class="trav-trending-destination-icon" dir="auto"></i> Trending destinations <span class="count">{{ $locations->count() }}</span>
        </h3>
        <div class="side-right-control" dir="auto">
            <a href="#" id="discoverNewDestination-prev-{{ $id }}" class="slide-link discoverNewDestination-prev" dir="auto"><i class="trav-angle-left" dir="auto"></i></a>
            <a href="#" id="discoverNewDestination-next-{{ $id }}" class="slide-link discoverNewDestination-next" dir="auto"><i class="trav-angle-right" dir="auto"></i></a>
        </div>
    </div>
    <div class="post-side-inner" dir="auto">
        <div class="post-slide-wrap slide-hide-right-margin" dir="auto">

            <div class="lSSlideOuter ">
                <div class="lSSlideWrapper usingCss" style="transition-duration: 400ms; transition-timing-function: ease;">
                    <ul id="discoverNewDestination-{{ $id }}" class="trending-destinations-slider post-slider lightSlider lsGrab lSSlide" dir="auto" style="width: 1250px; transform: translate3d(0px, 0px, 0px); height: 300px; padding-bottom: 0%;">
                        @foreach($locations as $location)
                        <li dir="auto" class="clone left lslide active" style="margin-right: 20px;">
                            <img class="lazy" alt="{{ $location['title'] }}" style="width: 230px; height: 300px;" dir="auto" src="{{ $location['img'] }}">
                            <div class="post-slider-caption transparent-caption" dir="auto">
                                <p class="post-slide-name" dir="auto">
                                    <a href="{{ $location['url'] }}" style="color:white;" dir="auto">
                                        {{ $location['title'] }}
                                    </a>
                                </p>
                                <div class="post-slide-description" dir="auto">
                                    <span class="tag" dir="auto">{{do_placetype($location['placeType'])}}</span>
                                     {{ $location['city'] ? "in " . $location['city'] : "" }}
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        // Post type - shared place, slider
        var trendingDestinationsSlider = $('#discoverNewDestination-{{ $id }}').lightSlider({
            autoWidth: true,
            slideMargin: 20,
            pager: false,
            controls: false
        });

        $('#discoverNewDestination-prev-{{ $id }}').click(function(e){
            e.preventDefault();
            trendingDestinationsSlider.goToPrevSlide();
        });
        $('#discoverNewDestination-next-{{ $id }}').click(function(e){
            e.preventDefault();
            trendingDestinationsSlider.goToNextSlide();
        });
    })
</script>

@endif