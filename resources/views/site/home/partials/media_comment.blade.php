<?php
$post_object = App\Models\ActivityMedia\Media::find($post->variable);
?>
@if(is_object($post_object) && is_object($post_object->mediaUser))
    <pre>
</pre>
    <div class="post-block post-top-bordered">
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap post-marked-avatar">
                    <img src="{{check_profile_picture($post->user->profile_picture)}}" alt=""
                         style="width:20px;height:20px;">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name profile-name post-ellipsis">
                        <a class="post-name-link" href="{{url('profile/'.$post->user->id)}}">{{$post->user->name}}</a>
                        {!! get_exp_icon($post->name) !!}
                        @lang('home.added_a_comment_about_a')
                        <a href="#" class="post-name-link">@lang('profile.photo')</a>

                    </div>
                </div>
            </div>
            <div class="post-top-info-action">
                @if (Auth::user()->id !== $post->user->id)
                    <div class="dropdown">
                        <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="trav-angle-down"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Mediacomment">
                            @include('site.home.partials._info-actions', ['post'=>$post_object])
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="post-content-inner">
            <div class="post-image-container post-follow-container">
                <ul class="post-image-list">
                    <li style="position: relative;width: 595px;height: 595px;overflow: hidden;">
                        <img src="{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;height: 100%;width: auto;-webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                    </li>
                </ul>
                <div class="post-footer-info post-footer-padded">
                <span class="media_like_button" id="{{$post_object->id}}">
                    <a href='#'>
                        <img src="{{asset('assets2/image/like.svg')}}" alt="Like this post"
                             style="width:16px;height:16px;margin:5px;vertical-align:baseline;">
                    </a>
                </span>
                    <span id="media_like_count_{{$post_object->id}}"><a href='#'
                                                                        data-tab='comments{{$post_object->id}}'><b>{{count($post_object->likes)}}</b> Likes</a></span>
                    <div class="post-foot-block">
                        <a href='#' data-tab='comments{{$post_object->id}}'>
                            <i class="trav-comment-icon"></i>
                        </a>

                        <ul class="foot-avatar-list"></ul>
                        <span><a href='#' comment_count='{{count($post_object->comments)}}' class='comment_count{{$post_object->id}}' data-tab='comments{{$post_object->id}}'>{{count($post_object->comments)}}
                                @lang('home.comments')</a></span>
                    </div>
                </div>

                <div class="follow-comment-wrap comment-on-photo" style="background-color:white!important">
                    <div class="post-comment-top-info">
                        <ul class="comment-filter">
                            <li class="active" onclick="commentSort('Top', this)">@lang('comment.top')</li>
                            <li onclick="commentSort('New', this)">@lang('home.new')</li>
                        </ul>
                        <div class="comm-count-info">
                        </div>
                    </div>
                    <div class="post-comment-wrapper sortBody">
                        @if(count($post_object->comments)>0)
                            @foreach($post_object->comments AS $comment)
                                @include('site.home.partials.media_comment_block')
                            @endforeach
                        @endif

                    </div>
                </div>

                <div class="post-follow-block" style="display:block!important">
                    @if(Auth::user())
                        <div class="post-add-comment-block">
                            <div class="avatar-wrap">
                                <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                                     style="width:45px;height:45px;">
                            </div>
                            <div class="post-add-com-input">
                                <form method="post" class="mediacommentForm">
                                    <input type="text" name="text" id="text{{$post_object->id}}"
                                           placeholder="@lang('comment.write_a_comment')" class="input_customize">
                                    <input type="hidden" name="post_id" value="{{$post_object->id}}"/>
                                </form>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endif

<style>
    .input_customize
    {
        border: 1px solid #dcdcdc;
        background-color: white!important;
    }
</style>
