<?php
$comment_childs = App\Models\ActivityMedia\MediasComments::where('reply_to', '=', $comment->id)->orderby('created_at','DESC')->get();

?>
<div topSort="{{count($comment->likes)}}" newSort="{{strtotime($comment->created_at)}}">
    <div class="post-comment-row news-feed-comment" id="mediaCommentRow{{@$comment->id}}">
        <div class="post-com-avatar-wrap">
            <img src="{{@check_profile_picture($comment->user->profile_picture)}}" alt="">
        </div>
        <div class="post-comment-text" style="width: 82%">
            <div class="post-com-name-layer">
                <a href="{{url('profile/'.@$comment->user->id)}}"
                    class="comment-name">{{@$comment->user->name}}</a>
                    {!! get_exp_icon($comment->name) !!}
                    <div class="post-com-top-action">
                        <div class="dropdown">
                            <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="trav-angle-down"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Mediacomment">
                                @if(Auth::check() && @$comment->user->id==Auth::user()->id)
                                <a href="javascript:;" class="dropdown-item media-edit-comment"  data-id="{{@$comment->id}}" data-media="{{@$post_object->id}}">
                                    <span class="icon-wrap">
                                        <i class="trav-pencil" aria-hidden="true"></i>
                                    </span>
                                    <div class="drop-txt comment-edit__drop-text">
                                        <p><b>@lang('profile.edit')</b></p>
                                    </div>
                                </a>
                                @else
                                    <a href="javascript:;" class="dropdown-item" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$comment->id}},this)">
                                    <span class="icon-wrap">
                                        <i class="trav-flag-icon-o"></i>
                                    </span>
                                        <div class="drop-txt comment-report__drop-text">
                                            <p><b>@lang('profile.report')</b></p>
                                        </div>
                                    </a>
                                @endif
                                @if(Auth::check() && @$comment->user->id==Auth::user()->id && count($comment_childs) == 0)
                                <button class="dropdown-item postmediaCommentDelete pmd-link" id="{{@$comment->id}}" postid="{{$post_object->id}}" data-type="1">
                                    <span class="icon-wrap">
                                        <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;">
                                    </span>
                                    <div class="drop-txt comment-delete__drop-text">
                                        <p><b>Delete</b></p>
                                    </div>
                                </button>
                                @endif
                            </div>
                        </div>
                    </div>
            </div>
            <div class="comment-txt p-comment-text media-comment-text-{{$comment->id}}">
                @php
                    $showChar = 100;
                    $ellipsestext = "...";
                    $moretext = "more";
                    $lesstext = "less";

                    $comment_content = $comment->comment;
                    $convert_content = strip_tags($comment_content);

                    if(mb_strlen($convert_content, 'UTF-8') > $showChar) {

                        $c = mb_substr($comment_content, 0, $showChar, 'UTF-8');

                        $html = '<span class="less-content">'.$c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span> <a href="javascript:;" class="read-more-link">' . $moretext . '</a></span>  <span class="more-content" style="display:none">' . $comment_content . ' &nbsp;&nbsp;<a href="javascript:;" class="read-less-link">' . $lesstext . '</a></span>';

                        $comment_content = $html;
                    }
                    
                @endphp
                <p>{!!$comment_content!!}</p>
                <form class="mediaCommentEditForm{{$comment->id}} comment-edit-form d-none" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
                    {{ csrf_field() }}<input type="hidden" data-id="pair{{$comment->id}}" name="pair" value="{{uniqid()}}"/>
                    <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0">
                        <div class="post-create-input p-create-input b-whitesmoke">
                            <textarea name="text" id="text{{$comment->id}}" class="textarea-customize post-comment-emoji" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="@lang('comment.write_a_comment')">{!!$comment->comment!!}</textarea>
                       </div>
                        <div class="post-create-controls b-whitesmoke d-none">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <li class="post-options">
                                        <input type="file" name="file[]" class="commentmediaeditfile" id="mediacommenteditfile{{$comment->id}}"  style="display:none" multiple>
                                        <i class="fa fa-camera click-target" data-target="mediacommenteditfile{{$comment->id}}"></i>
                                    </li>
                                </ul>
                            </div>
                            <div class="comment-edit-action">
                                <a href="javascript:;" class="edit-media-cancel-link"  data-comment_id="{{$comment->id}}">Cancel</a>
                                <a href="javascript:;" class="edit-media-comment-link" data-comment_id="{{$comment->id}}">Post</a>
                            </div>
                        </div>
                        <div class="medias p-media b-whitesmoke">
                           @if(is_object($comment->medias))
                           @foreach($comment->medias AS $photo)
                           @if($photo->media)
                                @php
                                    $file_url = $photo->media->url;
                                    $file_url_array = explode(".", $file_url);
                                    $ext = end($file_url_array);
                                    $allowed_video = array('mp4');
                                @endphp
                                <div class="img-wrap-newsfeed">
                                    <div>
                                        @if(in_array($ext, $allowed_video))
                                        <video style="object-fit: cover" class="thumb" controls>
                                            <source src="{{$file_url}}" type="video/mp4">
                                        </video>
                                        @else
                                        <img class="thumb" src="{{$file_url}}" alt="" >
                                        @endif
                                    </div>
                                    <span class="close remove-media-comment-file" data-media_id="{{$photo->media->id}}">
                                        <span>×</span>
                                    </span></div>
                           @endif
                           @endforeach
                           @endif
                       </div>
                    </div>
                    <input type="hidden" name="media_id" value="{{$post_object->id}}"/>
                    <input type="hidden" name="comment_id" value="{{$comment->id}}">
                    <input type="hidden" name="comment_type" value="1">
                    <button type="submit"  class="d-none"></button>
                </form>
            </div>
            <div class="post-image-container">
                @if(is_object($comment->medias))
                    @php
                        $index = 0;

                    @endphp
                    @foreach($comment->medias AS $photo)
                        @if($photo->media)
                            @php
                                $index++;
                                $file_url = $photo->media->url;
                                $file_url_array = explode(".", $file_url);
                                $ext = end($file_url_array);
                                $allowed_video = array('mp4');
                            @endphp
                            @if($index % 2 == 1)
                            <ul class="post-image-list" style="display: flex;margin-bottom: 20px;">
                            @endif
                                <li style="overflow: hidden; margin:1px">
                                    @if(in_array($ext, $allowed_video))
                                    <video style="object-fit: cover" width="192" height="210" controls>
                                        <source src="{{$file_url}}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                    @else
                                    <a href="{{$file_url}}" data-lightbox="mediacoment__media{{$comment->id}}">
                                        <img src="{{$file_url}}" alt="" style="width:192px;height:210px;object-fit: cover;">
                                    </a>
                                    @endif
                                </li>
                            @if($index % 2 == 0)
                            </ul>
                            @endif
                        @endif
                    @endforeach
                @endif

            </div>
            <div class="comment-bottom-info">
                <div class="comment-info-content">
                    <div class="dropdown">
                    @if(Auth::check())
                        <a href="javascript:;" class="postmediaCommentLikes like-link dropbtn" id="{{$comment->id}}"><i class="fa fa-heart {{($comment->likes()->where('users_id', Auth::user()->id)->first())?'fill':''}}" aria-hidden="true"></i> <span class="comment-like-count">{{count($comment->likes)}}</span></a>
                    @else
                            <a href="javascript:;" class="postmediaCommentLikes like-link dropbtn open-login"><i class="fa fa-heart" aria-hidden="true"></i> <span class="comment-like-count">{{count($comment->likes)}}</span></a>
                    @endif
                    @if(count($comment->likes))
                        <div class="dropdown-content media-comment-likes-block" data-id="{{$comment->id}}">
                            @foreach($comment->likes()->orderBy('created_at', 'DESC')->take(7)->get() as $like)
                                <span>{{$like->author->name}}</span>
                            @endforeach
                             @if(count($comment->likes)>7)
                            <span>...</span>
                            @endif
                        </div>
                    @endif
                </div>
                      <a href="javascript:;" class="mediaCommentReply reply-link" id="{{$comment->id}}">Reply</a>
                      <span class="com-time"><span class="comment-dot"> · </span>{{diffForHumans($comment->created_at)}}</span>
                </div>
            </div>
        </div>
    </div>
@if(count($comment_childs) > 0)
    @foreach($comment_childs as $child)
        @include('site.home.partials.media_comment_reply_block')
    @endforeach
@endif
@if(Auth::user())
<div class="post-add-comment-block media-reply" id="mediaReplyForm{{$comment->id}}" style="padding-top:0px;padding-left: 59px;display:none;">
    <div class="avatar-wrap" style="padding-right:10px">
        <img src="{{check_profile_picture(@$comment->user->profile_picture)}}" alt="" style="width:30px !important;height:30px !important;">
    </div>
    <div class="post-add-com-inputs">
        <form class="mediaCommentReplyForm{{@$comment->id}}" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" data-id="pair{{$comment->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
            <div class="post-create-block post-comment-create-block post-reply-block" tabindex="0">
                <div class="post-create-input p-create-input b-whitesmoke">
                    <textarea name="text" data-id="text{{$comment->id}}" class="textarea-customize media-comment-text post-comment-emoji"  style="display:inline;vertical-align: top;min-height:50px;" placeholder="@lang('comment.write_a_comment')"></textarea>
                </div>

                <div class="post-create-controls b-whitesmoke d-none">
                    <div class="post-alloptions">
                        <ul class="create-link-list">
                            <li class="post-options">
                                <input type="file" class="media-file-input" data-comment_id="{{$comment->id}}" name="file[]" id="mediacommentreplyfile{{$comment->id}}" style="display:none" multiple>
                                <i class="fa fa-camera click-target" data-target="mediacommentreplyfile{{$comment->id}}"></i>
                            </li>
                        </ul>
                    </div>

                    <div class="comment-edit-action">
                        <a href="javascript:;" class="p-comment-cancel-link media-cancel-link">Cancel</a>
                        <a href="javascript:;" class="p-comment-link media-reply-comment-link" data-comment_id="{{$comment->id}}">Post</a>
                    </div>
                    
                    <button type="submit" class="btn btn-primary d-none"></button>

                </div>
                <div class="medias p-media b-whitesmoke"></div>
            </div>
            <input type="hidden" name="media_id" value="{{$post_object->id}}"/>
            <input type="hidden" name="comment_id" value="{{$comment->id}}">
        </form>
    </div>
</div>
@endif
</div>
