@php($id = rand(1, 1000))
@if($top_places->count())
<div class="post-block discover-destinations-block">
    <div class="post-side-top">
        <h3 class="side-ttl">@lang('home.discover_new_destinations')</h3>
        <div class="side-right-control">
            <a href="#" class="slide-link slide-prev" id="destinations-slider-prev-{{$id}}"><i class="trav-angle-left"></i></a>
            <a href="#" class="slide-link slide-next" id="destinations-slider-next-{{$id}}"><i class="trav-angle-right"></i></a>
        </div>
    </div>
    <div class="post-side-inner">
        <div class="post-slide-wrap slide-hide-right-margin">
            <ul id="destAnimation" class="discover-dest-animation-content">
                    <li>
                        <div class="dest-animation-card-inner">
                            <div class="dest-animation-wrap">
                                 <p class="animation dest-animation-title"></p>
                                 <p class="animation dest-animation-text"></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dest-animation-card-inner">
                            <div class="dest-animation-wrap">
                                 <p class="animation dest-animation-title"></p>
                                 <p class="animation dest-animation-text"></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="dest-animation-card-inner">
                            <div class="dest-animation-wrap">
                                 <p class="animation dest-animation-title"></p>
                                 <p class="animation dest-animation-text"></p>
                            </div>
                        </div>
                    </li>
            </ul>
            <ul id="discoverNewDestination" class="post-slider d-none">
                @foreach($top_places as $tplace)
                    <li>
                        <img class="lazy" data-src="{{check_place_photo(@$tplace->place->medias[0]->url, 180)}}"
                             alt="{{@$tplace->place->transsingle->title}}" style='width:230px;height:300px;'>
                        <div class="post-slider-caption transparent-caption">
                            <p class="post-slide-name">
                                <a href="{{($tplace->place)?url_with_locale('place/'.$tplace->place->id):''}}" style="color:white;">
                                {{@$tplace->place->transsingle->title}}
                                </a>
                            </p>
                            <div class="post-slide-description">
                                <span class="tag">{{@do_placetype($tplace->place->place_type)}}</span>
                                @lang('other.in') {{@$tplace->place->city->transsingle->title}}
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        // Post type - shared place, slider
        var homeRecommendedePlacesSlider = $('#discoverNewDestination').lightSlider({
            autoWidth: true,
            slideMargin: 20,
            pager: false,
            controls: false
        });

        $('#destinations-slider-prev-{{$id}}').click(function(e){
            e.preventDefault();
            homeRecommendedePlacesSlider.goToPrevSlide();
        });
        $('#destinations-slider-next-{{$id}}').click(function(e){
            e.preventDefault();
            homeRecommendedePlacesSlider.goToNextSlide();
        });
    })
</script>
@endif