<div class="post-block">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <img src="{{check_profile_picture($post->author->profile_picture)}}"
                     alt="{{$post->author->name}}'s profile picture">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{url('profile/'.$post->author->id)}}">{{$post->author->name}}</a>
                    {!! get_exp_icon($post->author) !!}
                </div>
                <div class="post-info">
                    @if($post->checkin[0]->place_id)
                        Checked in at <i class="trav-set-location-icon"></i> <a class="link-place"
                                                                                href="{{ route('place.index', $post->checkin[0]->place_id) }}">{{$post->checkin[0]->location}}</a>
                    @elseif($post->checkin[0]->city_id)
                        Checked in at <i class="trav-set-location-icon"></i> <a class="link-place"
                                                                                href="{{ route('city.index', $post->checkin[0]->city_id) }}">{{$post->checkin[0]->location}}</a>
                    @elseif($post->checkin[0]->country_id)
                        Checked in at <i class="trav-set-location-icon"></i> <a class="link-place"
                                                                                href="{{ route('country.index', $post->checkin[0]->country_id) }}">{{$post->checkin[0]->location}}</a>
                    @endif
                    {{$post->created_at}}
                </div>
            </div>
        </div>
        <div class="post-top-info-action">
            <div class="dropdown">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <i class="trav-angle-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcheckin">
                    @if(Auth::user()->id)
                        @if(Auth::user()->id==$post->users_id)
                            <a class="dropdown-item" href="#">
                                <span class="icon-wrap">
                                    <i class="trav-flag-icon-o"></i>
                                </span>
                                <div class="drop-txt">
                                    <p><b>@lang('buttons.general.crud.delete')</b></p>
                                    <p style="color:red">@lang('home.remove_this_post')</p>
                                </div>
                            </a>
                        @else
                            @include('site.home.partials._info-actions', ['post'=>$post])
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="post-image-container">
        <div class="post-txt-wrap">
            <p class="post-txt">{{$post->text}}</p>
        </div>
        @if(is_object($post->medias))
            <ul class="post-image-list">
                @foreach($post->medias AS $photo)
                    <li>
                        <img src="{{$photo->media->url}}" alt="" style="width:192px;height:210px">
                    </li>
            @endforeach

            <!--<li class="more-photos-wrap">
                <img src="https://s3.amazonaws.com/travooo-images2/th230/" alt="" style="width:192px;height:210px">
                <a href="#" class="more-photos-link">
                    <span> More Photos</span>
                </a>
            </li>-->

            </ul>

        @endif
    </div>
    <div class="post-footer-info">
        <div class="post-foot-block post-reaction">
            <span class="post_like_button" id="{{$post->id}}">
                <a href='#'>
                    <img src="{{asset('assets2/image/like.svg')}}" alt="Like this post"
                         style="width:16px;height:16px;margin:5px;vertical-align:baseline;">
                </a>
            </span>
            <span id="post_like_count_{{$post->id}}"><a href='#'
                                                        data-tab='comments{{$post->id}}'><b>{{count($post->likes)}}</b> @lang('home.likes')</a></span>
        </div>
        <div class="post-foot-block">
            <a href='#' data-tab='comments{{$post->id}}'>
                <i class="trav-comment-icon"></i>
            </a>
            <ul class="foot-avatar-list">
                <li><img class="small-ava" src="./assets2/image/photos/profiles/78_refashionista_sheri_pavlovic.jpg"
                         alt="ava"></li>
            </ul>
            <span><a href='#' data-tab="comments{{$post->id}}">{{count($post->comments)}} @lang('comment.comments')</a></span>
        </div>
    </div>
    <div class="post-comment-layer" data-content="comments{{$post->id}}" style='display:none;'>
        @if(count($post->comments)>0)
            <div class="post-comment-top-info">
                <ul class="comment-filter">
                    <li class="active">@lang('comment.top')</li>
                    <li>@lang('comment.new')</li>
                </ul>
                <div class="comm-count-info">
                    3 / 20
                </div>
            </div>
            <div class="post-comment-wrapper">
                @foreach($post->comments AS $comment)
                    <div class="post-comment-row">
                        <div class="post-com-avatar-wrap">
                            <img src="{{check_profile_picture($comment->author->profile_picture)}}" alt="">
                        </div>
                        <div class="post-comment-text">
                            <div class="post-com-name-layer">
                                <a href="#" class="comment-name">{{$comment->author->name}}</a>
                                {!! get_exp_icon($comment->author) !!}
                                <a href="#" class="comment-nickname">@katherin</a>
                            </div>
                            <div class="comment-txt">
                                <p>{{$comment->text}}</p>
                            </div>
                            <div class="comment-bottom-info">
                                <!--
                                <div class="com-reaction">
                                    <img src="./assets2/image/icon-smile.png" alt="">
                                    <span>21</span>
                                </div>
                                -->
                                <div class="com-time">{{diffForHumans($comment->created_at)}}</div>
                            </div>
                        </div>
                    </div>
            @endforeach
            <!--<div class="post-comment-row">
                <div class="post-com-avatar-wrap">
                    <img src="./assets2/image/photos/profiles/amine.jpg" alt="">
                </div>
                <div class="post-comment-text">
                    <div class="post-com-name-layer">
                        <a href="#" class="comment-name">Amine</a>
                        <a href="#" class="comment-nickname">@ak0117</a>
                    </div>
                    <div class="comment-txt">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                    </div>
                    <div class="comment-bottom-info">
                        <div class="com-reaction">
                            <img src="./assets2/image/icon-like.png" alt="">
                            <span>19</span>
                        </div>
                        <div class="com-time">6 hours ago</div>
                    </div>
                </div>
            </div>
            -->

                <a href="#" class="load-more-link">@lang('buttons.general.load_more')...</a>
            </div>
        @endif
        @if(Auth::user())
            <div class="post-add-comment-block">
                <div class="avatar-wrap">
                    <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                         style="width:45px;height:45px;">
                </div>
                <div class="post-add-com-input">
                    <form>
                        <input type="text" placeholder="@lang('comment.write_a_comment')">
                        <input type="hidden" name="post_id" value="{{$post->id}}"/>
                    </form>
                </div>
            </div>
        @endif
    </div>
</div>
