<?php

use App\Models\User\UsersMedias;
use App\Models\ActivityMedia\Media;
use Illuminate\Support\Facades\DB;

$places = '';
$cities = '';
$countries = '';

if(Auth::check() && !isset($suggestion)){
    $user = auth()->user();
    $places = $user->followedPlaces->pluck('places_id')->implode(', ');
    $cities = $user->followedCountries->pluck('countries_id')->implode(', ');
    $countries = $user->followedCities->pluck('cities_id')->implode(', ');
}else if(Auth::check() && isset($suggestion)){
    if(isset($suggestion['places'])){
        $places = $suggestion['places'];
    }else if(isset($suggestion['city'])){
        $cities = $suggestion['city'];
    }else if(isset($suggestion['contry'])){
        $countries =$suggestion['contry'];
    }
}

$places_query = '' ;
$cities_query = '' ;
$countries_query = '' ;

    if($places !='' )
        $places_query = 'SELECT medias.id as id FROM `medias` INNER JOIN `places_medias` ON `medias`.`id` = `places_medias`.`medias_id` WHERE `places_medias`.`places_id`  IN ('.$places.') AND RIGHT(url, 4) = ".mp4" AND `medias`.`deleted_at` IS NULL ';
    if($cities !='' )
        $cities_query = 'SELECT medias.id as id FROM `medias` INNER JOIN `cities_medias` ON `medias`.`id` = `cities_medias`.`medias_id` WHERE `cities_medias`.`cities_id`  IN ('.$cities.') AND RIGHT(url, 4) = ".mp4" AND `medias`.`deleted_at` IS NULL ';
    if($countries !='' )
        $countries_query = 'SELECT medias.id as id FROM `medias` INNER JOIN `countries_medias` ON `medias`.`id` = `countries_medias`.`medias_id` WHERE `countries_medias`.`countries_id`  IN ('.$countries.') AND RIGHT(url, 4) = ".mp4" AND `medias`.`deleted_at` IS NULL';
   $results = [] ;
    if(!($places_query =='' && $cities_query  == '' && $countries_query == '')) {

        if($places_query !='' && ($cities_query !='' || $countries_query!=''))
            $places_query =$places_query .' UNION ALL';
        if($cities_query !='' &&  $countries_query!='')
            $cities_query = $cities_query.' UNION ALL';
        $results = DB::select('
            SELECT * FROM (
                '.$places_query.' '.$cities_query.' '.$countries_query. '
            ) AS results
        ');
    }
    $medias =[]; 
    foreach($results as $result){
        if(isset($result->id))
        $medias [] =$result->id;
    }

    $medias = Media::whereIn('id',$medias)->get();

    
    // $following_locations['places_ids'] = $following_locations['places_ids']->toArray();
    // $following_locations['countries_ids'] = $following_locations['countries_ids']->toArray();
    // $following_locations['cities_ids'] = $following_locations['cities_ids']->toArray();
    // $medias = $medias->filter(function($media) use($following_locations) {
    //     $checkins = collect([]);

    //     if(@$media->postMedia->checkin) {
    //         $checkins = @$media->postMedia->checkin->filter(function($checkin) use($following_locations) {
    //             return in_array($checkin->place_id,  $following_locations['places_ids'])
    //                 || in_array($checkin->country_id,  $following_locations['countries_ids'])
    //                 || in_array($checkin->city_id,  $following_locations['cities_ids']);
    //         });
    //     }
    //     return $checkins ? $checkins->count() : false;
    // });

    // $medias = $medias->sortBy(function ($media) {
    //     $post = $media->postMedia->post;
    //     return $post->likes->count() + $post->shares->count() + $post->comments->count();
    // });
    $id = rand(1, 10000);

?>

@if($medias->count() && $medias->count()>3)
<div class="post-block post-block-trending-videos">
    <div class="post-side-top">
        <h3 class="side-ttl"> @lang('home.videos_you_might_like') <span class="count">{{ $medias->count() }}</span></h3>
        <div class="side-right-control">
            <a href="#" class="slide-link video-slide-prev" id="videosYouMightLike-prev-{{ $id }}"><i class="trav-angle-left"></i></a>
            <a href="#" class="slide-link video-slide-next" id="videosYouMightLike-next-{{ $id }}"><i class="trav-angle-right"></i></a>
        </div>
    </div>
    <div class="post-side-inner">
        <ul id="loadReportDestAnimation" class="discover-dest-animation-content d-none">
            <li>
                <div class="dest-animation-card-inner">
                    <div class="dest-animation-wrap">
                        <p class="animation dest-animation-title"></p>
                        <p class="animation dest-animation-text"></p>
                    </div>
                </div>
            </li>
            <li>
                <div class="dest-animation-card-inner">
                    <div class="dest-animation-wrap">
                        <p class="animation dest-animation-title"></p>
                        <p class="animation dest-animation-text"></p>
                    </div>
                </div>
            </li>
            <li>
                <div class="dest-animation-card-inner">
                    <div class="dest-animation-wrap">
                        <p class="animation dest-animation-title"></p>
                        <p class="animation dest-animation-text"></p>
                    </div>
                </div>
            </li>
        </ul>
        <div class="post-slide-wrap">
            <div class="lSSlideOuter ">
                <div class="lSSlideWrapper usingCss" style="transition-duration: 400ms; transition-timing-function: ease;">
                    <ul id="videosYouMightLike-{{ $id }}" class="post-slider trending-videos-slider lightSlider lsGrab lSSlide" style="width: 1488px; height: 268px; padding-bottom: 0%; transform: translate3d(0px, 0px, 0px);">
                        @foreach($medias as $media)
                        <li class="trending-videos-slider-item lslide active" style="margin-right: 20px;">
                            <div class="video-box">
                                <video width="100%" height="auto">
                                    <source src="https://s3.amazonaws.com/travooo-images2/{{ $media->url }}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                                <a href="javascript:;" class="video-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                            </div>
                            <div class="text">
                                <div class="ttl">{!! substr(strip_tags($media->title),0, 50) !!}{{ strip_tags($media->title) > 50 ? '...' : '' }}</div>
                                <div class="author">Posted by <a href="{{ url_with_locale('profile/'.@$media->users[0]->id)}}">{{ @$media->users[0]->name }}</a></div>
                                <div class="post-footer-info">
                                    <div class="post-foot-block post-reaction">
                                        <span class="post_like_button" id="601451">
                                        <a href="#">
                                        <i class="trav-heart-fill-icon"></i>
                                        </a>
                                        </span>
                                            <span id="post_like_count_601451" data-toggle="modal" data-target="#usersWhoLike">
                                        <b>{{ $media->likes->count() }}</b> Likes
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        // Post type - shared place, slider
        var videosYouMightLike = $('#videosYouMightLike-{{ $id }}').lightSlider({
            autoWidth: true,
            slideMargin: 20,
            pager: false,
            controls: false
        });

        $('#videosYouMightLike-prev-{{ $id }}').click(function(e){
            e.preventDefault();
            videosYouMightLike.goToPrevSlide();
        });
        $('#videosYouMightLike-next-{{ $id }}').click(function(e){
            e.preventDefault();
            videosYouMightLike.goToNextSlide();
        });
    })
</script>
@endif