<?php
$post_type = $post->type;
$post_object = \App\Models\TravelMates\TravelMatesRequests::find($post->variable);
$trip_plan_first_country = @$post_object->plan_country->country;
$trip_plan_last_country = '';

if(is_object(@$post_object->plan) && $post_object->plans_id){
    if(count(@$post_object->plan->places) > 0){
        if(@$post_object->plan->places->last()->country->id != @$trip_plan_first_country->id){
            $trip_plan_last_country = @$post_object->plan->places->last()->country;
        }
    }
}
?>
@if(is_object($post_object) && $post_object->plans_id && $post_object->users_id !== auth()->id())
<div class="post-block">
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <img src="{{check_profile_picture($post_object->author->profile_picture)}}" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{url('profile/'.$post_object->author->id)}}">{{$post_object->author->name}}</a>
                    {!! get_exp_icon($post_object->author) !!}
                </div>
                <div class="post-info">
                    is requesting someone to join him on his <a href="{{route('trip.view', $post_object->plan->id)}}" class="link-place">{{ $post_object->plan->title }}</a>
                    {{diffForHumans_2($post_object->created_at)}}
                </div>
            </div>
            <div class="m-join-block follow-btn-wrap">
                <!-- <button type="button" class="btn btn-light-grey btn-bordered">@lang('travelmate.ask_to_join')</button> -->
                @if(count($post_object->going()->where('users_id', Illuminate\Support\Facades\Auth::guard('user')->user()->id)->get()) <= 0)
                    <button type="button" name="submit" class="btn btn-light-primary btn-bordered m-join-btn" request_id="{{$post_object->id}}">@lang('travelmate.ask_to_join')</button>
                @elseif($post_object->going()->where('users_id', Illuminate\Support\Facades\Auth::guard('user')->user()->id)->get()->first()->status==0)
                    <button type="button"
                            class="btn btn-light-bg-grey btn-bordered text-uppercase">Pending ...</button>
                @elseif($post_object->going()->where('users_id', Illuminate\Support\Facades\Auth::guard('user')->user()->id)->get()->first()->status==1)
                    <button type="button"
                            class="btn btn-success btn-bordered">@lang('travelmate.joined')</button>
                @elseif($post_object->going()->where('users_id', Illuminate\Support\Facades\Auth::guard('user')->user()->id)->get()->first()->status==2)
                    <button type="button"
                            class="btn btn-light-red-bordered btn-bordered">declined</button>
                @endif
            </div>
        </div>
        @if(Auth::user()->id!==$post_object->author->id)
            <div class="post-top-info-action">
                <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="TravelMateRequest">
                        @include('site.home.partials._info-actions', ['post'=>$post_object])
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="post-image-container post-follow-container post-mates-conteiner">
        <div class="m-main-block">
            <div class="m-map-wrap">
                <img src="{{getStaticGmapURLForPlan($post_object->plan, '615x320', 'satellite')}}" alt="map">
            </div>
            <div class="m-info-block">
                <div class="m-join-info-block d-flex justify-content-between">
                    <div class="m-country-block">
                        @if($trip_plan_first_country)
                        <div class="m-country-wrap">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/small/{{strtolower(@$trip_plan_first_country->iso_code)}}.png" class="flag" alt="flag"><span>{{@$trip_plan_first_country->transsingle->title}}</span>
                        </div>
                        @endif
                        
                        @if($trip_plan_last_country !='' && $trip_plan_first_country)
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        @endif
                        @if($trip_plan_last_country !='')
                        <div class="m-country-wrap">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/small/{{strtolower(@$trip_plan_last_country->iso_code)}}.png" class="flag" alt="flag"><span>{{@$trip_plan_last_country->transsingle->title}}</span>
                        </div>
                        @endif

                    </div>
                    <div class="d-flex">
                        <div class="m-participants-imgs">
                            @foreach($post_object->plan->join_requests->going()->orderBy('id', 'DESC')->limit(3)->get() as $request)
                            <img style="background-image: url({{asset('assets2/image/placeholders/male.png')}})" src="{{check_profile_picture($request->author->profile_picture)}}" alt="">
                            @endforeach
                        </div>
                        <div class="m-participants-count">
                            <span class="count">+{{$post_object->plan->join_requests->going()->count()}}</span>@lang('travelmate.more_going')
                        </div>
                    </div>
                </div>
                <div class="m-info-footer-block">
                    <div class="row mb-2">
                        <div class="col-sm-6 pr-4">
                            <div class="d-flex justify-content-between">
                                <div class="m-left-info">
                                    <div class="icon-wrap">
                                        <i class="trav-map-marker-icon"></i>
                                    </div>
                                    <span>Destinations:</span>
                                </div>
                                <div class="m-right-info">{{$post_object->plan->trips_places()->count()}}</div>
                            </div>
                        </div>
                        <div class="col-sm-6 pl-4">
                            <div class="d-flex justify-content-between">
                                <div class="m-left-info">
                                    <div class="icon-wrap">
                                        <i class="trav-budget-icon"></i>
                                    </div>
                                    <span class="m-info-title">@lang('trip.budget_dd')</span>
                                </div>
                                <div class="m-right-info">${{$post_object->plan->budget}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 pr-4">
                            <div class="d-flex justify-content-between">
                                <div class="m-left-info">
                                    <div class="icon-wrap">
                                        <i class="trav-point-flag-icon f-20"></i>
                                    </div>
                                     <span class="m-info-title">@lang('trip.starting_dd')</span>
                                </div>
                                <div class="m-right-info text-uppercase">{{plan_starting_date($post_object->plan)}}</div>
                            </div>
                        </div>
                        <div class="col-sm-6 pl-4">
                            <div class="d-flex justify-content-between">
                                <div class="m-left-info">
                                    <div class="icon-wrap">
                                        <i class="trav-clock-icon"></i>
                                    </div>
                                     <span class="m-info-title">@lang('trip.duration_dd')</span>
                                </div>
                                <div class="m-right-info">{{calculate_duration($post_object->plan->id, 'all')}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
    </div>

</div>
@endif
