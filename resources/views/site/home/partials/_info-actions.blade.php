{{-- todo: needs styles fixes --}}

@if($post && is_object($post))
    @if(isset($page) && $page == 'discussion_page')
        @if (Auth::id() !== $post->users_id || !Auth::check())
            @if(Auth::check())
                <a style=" padding: 5px 25px" class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData('{{$post->id}}',this)">
            @else
                <a style=" padding: 5px 25px" class="dropdown-item open-login" href="#">
            @endif
                    <div class="drop-txt">
                        <p><b>@lang('home.report')</b></p>
                    </div>
                </a>
        @endif
        <a style="margin-left: 0 !important; cursor: pointer; padding: 5px 25px" class="dropdown-item upvote-link {{ $post->upvotes && in_array(Auth::id(), $post->upvotes->pluck('users_id')->toArray()) ? 'down' : 'up' }}" id="{{ $post->id }}">
            <div class="drop-txt">
                @if ($post->upvotes && in_array(Auth::id(), $post->upvotes->pluck('users_id')->toArray()))
                    <p><b class="vote-text">@lang('home.downvote')</b></p>
                @else
                    <p><b  class="vote-text">@lang('home.upvote')</b></p>
                @endif
            </div>
        </a>
        @if($post->users_id == Auth::id())
            <a style=" padding: 5px 25px" href="javascript:;" class="dropdown-item edit-reply"  data-id="{{$reply->id}}">
                <div class="drop-txt">
                    <p><b>@lang('profile.edit')</b></p>
                </div>
            </a>
        @endif
        @if($post->users_id == Auth::id() || $post->discussion->users_id == Auth::id())
            <a style="padding: 5px 25px" class="dropdown-item delete-reply" href="#" reportid="{{ $post->id }}" id="{{ $post->id }}">
                <div class="drop-txt">
                    <p><b style="color:red">@lang('home.delete')</b></p>
                </div>
            </a>
        @endif
    @else
        @if(Auth::check())
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData('{{$post->id}}', this)">
        @else
            <a class="dropdown-item open-login" href="#">
        @endif
                <span class="icon-wrap">
                    <i class="trav-flag-icon-o"></i>
                </span>
                <div class="drop-txt">
                    <p><b>@lang('home.report')</b></p>
                    <p>@lang('home.help_us_understand')</p>
                </div>
            </a>
    @endif
@endif
