<?php

use App\Models\Place\Place;
use App\Models\Country\Countries;
use App\Models\City\Cities;
use App\Models\Posts\Posts;
use App\Models\User\User;
use Carbon\Carbon;

$places = [];
$countries = [];
$cities = [];
$followers = [];

if(Auth::check()){
    
$user = auth()->user();

$user->friends->each(function($friend) use (&$places, &$countries, &$cities) {
    $places = array_merge($places,
        $friend->friend->followedPlaces
            ->filter(function($place) {
                return $place->created_at ? $place->created_at->greaterThan(Carbon::now()->subDays(3)) : false;
            })
            ->pluck('places_id')->toArray());

    $countries = array_merge($countries,
        $friend->friend->followedCountries
            ->filter(function($country) {
                return $country->created_at ? $country->created_at->greaterThan(Carbon::now()->subDays(3)) : false;
            })
            ->pluck('countries_id')->toArray());

    $cities = array_merge($cities,
        $friend->friend->followedCities
            ->filter(function($city) {
                return $city->created_at ? $city->created_at->greaterThan(Carbon::now()->subDays(3)) : false;
            })
            ->pluck('cities_id')->toArray());
});

$user->followings->each(function($friend) use (&$places, &$countries, &$cities) {
    $places = array_merge($places,
        $friend->following->followedPlaces
            ->filter(function($place) {
                return $place->created_at ? $place->created_at->greaterThan(Carbon::now()->subDays(3)) : false;
            })
            ->pluck('places_id')->toArray());

    $countries = array_merge($countries,
        $friend->following->followedCountries
            ->filter(function($country) {
                return $country->created_at ? $country->created_at->greaterThan(Carbon::now()->subDays(3)) : false;
            })
            ->pluck('countries_id')->toArray());

    $cities = array_merge($cities,
        $friend->following->followedCities
            ->filter(function($city) {
                return $city->created_at ? $city->created_at->greaterThan(Carbon::now()->subDays(3)) : false;
            })
            ->pluck('cities_id')->toArray());
});

$friends = $user->friends->pluck('friends_id')->toArray();
$followings = $user->followings->pluck('users_id')->toArray();

$followers = array_merge($friends, $followings);

}

$post_data = json_encode([
    'places' => $places,
    'countries' => $countries,
    'cities' => $cities,
    'followers' => $followers
]);

$locations = json_decode($post_data);

$followers = User::whereIn('id', $locations->followers)->get();

$places = Place::whereIn('id', $locations->places)->get();
$countries = Countries::whereIn('id', $locations->countries)->get();
$cities = Cities::whereIn('id', $locations->cities)->get();

$places = $places->map(function($place) {
    $followersCount = count($place->followers);

    return [
        'id' => $place->id,
        'title' => @$place->transsingle->title,
        'description' => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
        'img' => check_place_photo(@$place, 180),
        'followers' => $followersCount > 1000 ? floor($followersCount/1000) . 'K' : $followersCount,
        'type' => 'place'
    ];
})->toArray();

$countries = $countries->map(function($country) {
    $followersCount = count($country->followers);

    return [
        'id' => $country->id,
        'title' => @$country->transsingle->title,
        'description' => !empty(@$country->transsingle->address) ? @$country->transsingle->address : '',
        'img' => check_country_photo(@$country, 180),
        'followers' => $followersCount > 1000 ? floor($followersCount/1000) . 'K' : $followersCount,
        'type' => 'country'
    ];
})->toArray();

$cities = $cities->map(function($city) {
    $followersCount = count($city->followers);

    return [
        'id' => $city->id,
        'title' => @$city->transsingle->title,
        'description' => !empty(@$city->transsingle->address) ? @$city->transsingle->address : '',
        'img' => check_city_photo(@$city, 180),
        'followers' => $followersCount > 1000 ? floor($followersCount/1000) . 'K' : $followersCount,
        'type' => 'city'
    ];
})->toArray();

$taggidPlacesData = collect(array_merge($places, $countries,$cities));

$id = str_random(5);
?>
@if($taggidPlacesData->count())
    <div class="post-block post-block-notification" id="post_{{$id}}">
        <div class="post-top-info-layer">
            <div class="post-info-line">
                <ul class="avatar-list">
                    @php
                    $countImages = $followers->take(3);
                    $countNames = 0;
                    @endphp
                    @foreach($countImages as $countImage)
                        <li><img src="{{check_profile_picture($countImage->profile_picture)}}" class="small-ava"></li>
                    @endforeach
                </ul>
                @foreach($followers as $follower)
                        @php
                            $Fname  = explode(' ',@$follower->name);
                         @endphp
                    @php($countNames++)
                    
                    <a class="post-name-link" href="{{url('profile/'.$follower->id)}}">{{@$Fname[0]}}</a>
                    @if($countNames === 1 && $followers->count() > 1)
                        ,
                    @endif

                    @if($countNames > 2)
                        @break
                    @endif
                @endforeach
                @if($followers->count() > 2)
                    <a class="post-name-link post-more-people" type="button" data-placement="bottom" data-toggle="popover" data-html="true" data-original-title="" title="">+{{ $followers->count() - 2 }} others</a>
                @endif
                followed these places
            </div>
        </div>
        @if (!empty($taggidPlacesData))
            <div class="post-txt-wrap">
                <div class="post-side-top">
                    <h3 class="side-ttl"> Popular in your network </h3>
                    <div class="side-right-control">
                        <a href="#" id="places-you-might-like-slider-prev-{{$id}}" class="slide-link places-you-might-like-slider-prev"><i class="trav-angle-left"></i></a>
                        <a href="#" id="places-you-might-like-slider-next-{{$id}}" class="slide-link places-you-might-like-slider-next"><i class="trav-angle-right"></i></a>
                    </div>
                </div>
                <div class="post-side-inner">
                    <div class="post-slide-wrap slide-hide-right-margin">
                        <div class="lSSlideOuter ">
                            <div class="lSSlideWrapper usingCss" style="transition-duration: 400ms; transition-timing-function: ease;">
                                <ul id="places-you-might-like-slider-{{$id}}" class="post-slider places-you-might-like-slider lightSlider lSSlide lsGrab" style="width: 1480px; height: 337px; padding-bottom: 0%; transform: translate3d(0px, 0px, 0px);">
                                    @foreach($taggidPlacesData as $place)
                                        <li class="lslide active" style="margin-right: 20px;">
                                            <div class="post-popular-inner">
                                                <div class="img-wrap">
                                                    <a style="text-decoration: none;" href="{{ route('place.index', @$place['id']) }}">
                                                        <img style="background-image: url({{$place['img']}})" src="{{$place['img']}}" alt="{{$place['title']}}">
                                                    </a>
                                                </div>
                                                <div class="pop-txt">
                                                <span class="location-badge" style="background: #4080ff;">
                                                    <i class="far fa-plus-square follow_place" style="cursor: pointer" data-place_id="{{ @$place['id']}}" data-type="follow"></i>
                                                    </span>
                                                    <h5><a style="text-decoration: none;" href="{{ route('place.index', @$place['id']) }}">{{$place['title']}}</a></h5>
                                                    <div class="reactions">
                                                        <button class="post__comment-btn">
                                                            <span><strong>{{$place['followers']}}</strong> Talking about this</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </div>
@endif
<script>
    $(document).ready(function(){
        var homeRecommendedePlacesSlider = $('#places-you-might-like-slider-{{$id}}').lightSlider({
            autoWidth: true,
            slideMargin: 20,
            pager: false,
            controls: false
        });

        $('#places-you-might-like-slider-prev-{{$id}}').click(function(e){
            e.preventDefault();
            homeRecommendedePlacesSlider.goToPrevSlide();
        });
        $('#places-you-might-like-slider-next-{{$id}}').click(function(e){
            e.preventDefault();
            homeRecommendedePlacesSlider.goToNextSlide();
        });
        // Post text tag popover
        var tagProfileIDs = '{{isset($tagProfileIDs) ? $tagProfileIDs : ""}}';
        var profileids = tagProfileIDs.split(',');
        if (profileids.length > 0) {
            $.each(profileids, function (i, v) {
                $(`.post-text-tag-${v}`).popover({
                    html: true,
                    content: $(`#popover-content-${v}`).html(),
                    template: '<div class="popover bottom tagging-popover" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
            })
        }
        $(document).on('click', ".read-more-link", function () {
            $(this).closest('.post-txt-wrap').find('.less-content').hide()
            $(this).closest('.post-txt-wrap').find('.more-content').show()
            $(this).hide()
        });
        $(document).on('click', ".read-less-link", function () {
            $(this).closest('.more-content').hide()
            $(this).closest('.post-txt-wrap').find('.less-content').show()
            $(this).closest('.post-txt-wrap').find('.read-more-link').show()
        });

        // Post type - shared place, slider
        $('#shared-place-slider-{{ $id }}').lightSlider({
            autoWidth: true,
            slideMargin: 22,
            pager: false,
            controls: false,
        });
        // Video
        $('.v-play-btn, .video-play-btn').click(function(){
            $(this).siblings('video').attr('controls', true);
            $(this).siblings('video').get(0).play();
            $(this).toggleClass('hide');
        });
        $('.post-image-container video, .post-block-trending-videos video').on('playing', function () {
            $(this).siblings('.v-play-btn, .video-play-btn').addClass('hide')
        });
        $('.post-image-container video, .post-block-trending-videos video').on('pause', function () {
            $(this).siblings('.v-play-btn, .video-play-btn').removeClass('hide')
        });
        // Trim more text
        if($('.less-content .trim')) {
            let content = $('.less-content .trim');
            let trim = function(content) {
                content.html(content.text().trim())
            }
            trim(content);
        }
    })
</script>
