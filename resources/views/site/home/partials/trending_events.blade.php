@php

use App\Models\Events\Events;

$user = auth()->user();

$events = Events::whereIn('places_id', $following_locations['places_ids'])
    ->orWhereIn('countries_id', $following_locations['countries_ids'])
    ->orWhereIn('cities_id', $following_locations['cities_ids'])
    ->get();

$events = $events->sortByDesc(function($event) {
    return $event->shares->count();
})->take(8);

$id = str_random(5);

$events = $events->map(function($event) {

    if($event->places_id) {
        $type = 'place';
        $loc = $event->place;
    } elseif($event->countries_id) {
        $type = 'country';
        $loc = $event->country;
    } else {
        $type = 'city';
        $loc = $event->city;
    }
    return [
        'id' => $loc->id,
        'title' => @$event->title,
        'description' => !empty(@$event->address) ? @$event->address : '',
        'img' => asset('assets2/image/events/'.$event->category.'/'.rand(1,10).'.jpg'),
        'type' => $type,
        'url' => url('place/'.$loc->id),
        'placeType' => @$loc->place_type ?: 'Event'
    ];
});

@endphp

@if($events->count())
<div class="post-block discover-destinations-block" dir="auto">
    <div class="post-side-top" dir="auto">
        <h3 class="side-ttl">
            <i class="trav-trending-destination-icon" dir="auto"></i> Trending events <span class="count">{{ $events->count() }}</span>
        </h3>
        <div class="side-right-control" dir="auto">
            <a href="#" id="discoverNewEvents-prev-{{ $id }}" class="slide-link discoverNewDestination-prev" dir="auto"><i class="trav-angle-left" dir="auto"></i></a>
            <a href="#" id="discoverNewEvents-next-{{ $id }}" class="slide-link discoverNewDestination-next" dir="auto"><i class="trav-angle-right" dir="auto"></i></a>
        </div>
    </div>
    <div class="post-side-inner" dir="auto">
        <div class="post-slide-wrap slide-hide-right-margin" dir="auto">

            <div class="lSSlideOuter ">
                <div class="lSSlideWrapper usingCss" style="transition-duration: 400ms; transition-timing-function: ease;">
                    <ul id="discoverNewEvents-{{ $id }}" class="trending-destinations-slider post-slider lightSlider lsGrab lSSlide" dir="auto" style="width: 1250px; transform: translate3d(0px, 0px, 0px); height: 300px; padding-bottom: 0%;">
                        @foreach($events as $event)
                        <li dir="auto" class="clone left lslide active" style="margin-right: 20px;">
                            <img class="lazy" alt="{{ $event['title'] }}" style="width: 230px; height: 300px;" dir="auto" src="{{ $event['img'] }}">
                            <div class="post-slider-caption transparent-caption" dir="auto">
                                <p class="post-slide-name" dir="auto">
                                    <a href="{{ $event['url'] }}" style="color:white;" dir="auto">
                                        {{ $event['title'] }}
                                    </a>
                                </p>
                                <div class="post-slide-description" dir="auto">
                                    @if(do_placetype($event['placeType']))
                                    <span class="tag" dir="auto">{{do_placetype($event['placeType'])}}</span>
                                    @endif
                                    in {{ $event['title'] }}
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<script>
    $(document).ready(function(){
        // Post type - shared place, slider
        var trendingDestinationsSlider = $('#discoverNewEvents-{{ $id }}').lightSlider({
            autoWidth: true,
            slideMargin: 20,
            pager: false,
            controls: false
        });

        $('#discoverNewEvents-prev-{{ $id }}').click(function(e){
            e.preventDefault();
            trendingDestinationsSlider.goToPrevSlide();
        });
        $('#discoverNewEvents-next-{{ $id }}').click(function(e){
            e.preventDefault();
            trendingDestinationsSlider.goToNextSlide();
        });
    })
</script>