
        <div class="post-comment-row doublecomment post-comment-reply commentRow{{$child->id}}" id="commentRow{{$child->id}}" data-parent_id="{{$child->parents_id}}" >
            <div class="post-com-avatar-wrap">
                <img src="{{check_profile_picture($child->author->profile_picture)}}" alt="">
            </div>
            <div class="post-comment-text">
                <div class="post-com-name-layer">
                    <a href="#" class="comment-name">{{$child->author->name}}</a>
                    {!! get_exp_icon($child->author) !!}
                    <div class="post-com-top-action">
                        <div class="dropdown">
                            <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="trav-angle-down"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Tripcomment">
                                @if(Auth::check() && $child->author->id==Auth::user()->id)
                                <a href="#" class="dropdown-item trip-edit-comment" data-id="{{$child->id}}" data-post="{{$plan->id}}">
                                    <span class="icon-wrap">
                                        <i class="trav-pencil" aria-hidden="true"></i>
                                    </span>
                                    <div class="drop-txt comment-edit__drop-text">
                                        <p><b>@lang('profile.edit')</b></p>
                                    </div>
                                </a>

                                <a href="javascript:;" class="dropdown-item plan-comment-delete" id="{{$child->id}}" data-parent="{{$child->parents_id}}" data-del-type="2">
                                    <span class="icon-wrap">
                                        <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;">
                                    </span>
                                    <div class="drop-txt comment-delete__drop-text">
                                        <p><b>Delete</b></p>
                                    </div>
                                </a>
                                @endif
                                <a href="#" class="dropdown-item" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$child->id}},this)">
                                    <span class="icon-wrap">
                                        <i class="trav-flag-icon-o"></i>
                                    </span>
                                    <div class="drop-txt comment-report__drop-text">
                                        <p><b>@lang('profile.report')</b></p>
                                    </div>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="comment-txt comment-text-{{$child->id}}">
                    @php
                        $showCommentChar = 100;
                        $ellipsestext = "...";
                        $commentmoretext = "more";
                        $commentlesstext = "less";

                        $comment_content = $child->text;

                        if(strlen($comment_content) > $showCommentChar) {

                            $l = substr($comment_content, 0, $showCommentChar);
                            $m = substr($comment_content, $showCommentChar, strlen($comment_content) - $showCommentChar);

                            $html = $l . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span><span class="morecontent"><span style="display:none">' . $m . '</span>&nbsp;&nbsp;<a href="#" class="morecommentlink">' . $commentmoretext . '</a></span>';

                            $comment_content = $html;
                        }
                    @endphp
                    <p>{!!$comment_content!!}</p>
                    <form class="tripCommentEditForm{{$child->id}} comment-edit-form d-none" method="POST" action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
                        {{ csrf_field() }}<input type="hidden" data-id="pair{{$child->id}}" name="pair" value="{{uniqid()}}"/>
                        <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0">
                            <div class="post-create-input">
                                <textarea name="text" data-id="text{{$child->id}}" class="textarea-bg-white textarea-customize" oninput="comment_textarea_auto_height(this, 'edit')" style="display:inline;vertical-align: top;min-height:50px; height:auto;" placeholder="@lang('comment.write_a_comment')">{!!$child->text!!}</textarea>
                                <div class="medias event-media">
                                    @if(is_object($child->medias))

                                        @foreach($child->medias AS $photo)
                                            @php
                                            $file_url = $photo->media->url;
                                            $file_url_array = explode(".", $file_url);
                                            $ext = end($file_url_array);
                                            $allowed_video = array('mp4');
                                            @endphp
                                            <div class="img-wrap-newsfeed">
                                                <div>
                                                    @if(in_array($ext, $allowed_video))
                                                    <video style="object-fit: cover" class="thumb" controls>
                                                        <source src="{{$file_url}}" type="video/mp4">
                                                    </video>
                                                @else
                                                <img class="thumb" src="{{$file_url}}" alt="" >
                                                @endif
                                                </div>
                                                <span class="close remove-media-file" data-media_id="{{$photo->media->id}}">
                                                    <span>×</span>
                                                </span></div>
                                        @endforeach
                                    @endif
                                </div></div>
                            <div class="post-create-controls">
                                <div class="post-alloptions">
                                    <ul class="create-link-list">
                                        <li class="post-options">
                                            <input type="file" name="file[]" class="trip-comment-edit-media-input" data-id="commenteditfile{{$child->id}}"  style="display:none" multiple>
                                            <i class="fa fa-camera click-target" data-target="commenteditfile{{$child->id}}"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="comment-edit-action">
                                    <a href="javascript:;" class="edit-cancel-link"  data-comment_id="{{$child->id}}">Cancel</a>
                                    <a href="javascript:;" class="edit-trip-comment-link" data-comment_id="{{$child->id}}">Post</a>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="post_id" value="{{$plan->id}}"/>
                        <input type="hidden" name="comment_id" value="{{$child->id}}">
                         <input type="hidden" name="comment_type" value="2">
                        <button type="submit"  class="d-none"></button>
                    </form>
                </div>
                <div class="post-image-container">
                    @if(is_object($child->medias))
                        @php
                            $index = 0;

                        @endphp
                        @foreach($child->medias AS $photo)
                            @php
                                $index++;
                                $file_url = $photo->media->url;
                                $file_url_array = explode(".", $file_url);
                                $ext = end($file_url_array);
                                $allowed_video = array('mp4');
                            @endphp
                            @if($index % 2 == 1)
                            <ul class="post-image-list" style=" display: flex;margin-bottom: 20px;">
                            @endif
                                <li style="overflow: hidden;margin:1px;">
                                    @if(in_array($ext, $allowed_video))
                                    <video style="object-fit: cover" width="192" height="210" controls>
                                        <source src="{{$file_url}}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                    @else
                                    <a href="{{$file_url}}" data-lightbox="comment__replymedia{{$child->id}}">
                                        <img src="{{$file_url}}" alt="" style="width:192px;height:210px;object-fit: cover;">
                                    </a>
                                    @endif
                                </li>
                            @if($index % 2 == 0)
                            </ul>
                            @endif
                        @endforeach
                    @endif

                </div>
                <div class="comment-bottom-info">
                    <div class="comment-info-content">
                        <div class="dropdown">
                            <a href="#" class="plan_comment_like like-link" id="{{$child->id}}"><i class="fa fa-heart {{(Auth::check() && $child->likes()->where('users_id', Auth::user()->id)->first())?'fill':''}}" aria-hidden="true"></i> <span class="comment-like-count">{{count($child->likes)}}</span></a>
                            @if(count($child->likes))
                            <div class="dropdown-content trip-comment-likes-block" data-id="{{$child->id}}">
                                @foreach($child->likes()->orderBy('created_at', 'DESC')->take(7)->get() as $like)
                                <span>{{$like->author->name}}</span>
                                @endforeach
                                @if(count($child->likes)>7)
                                <span>...</span>
                                @endif
                            </div>
                            @endif
                        </div>
                        <span class="com-time"><span class="comment-dot"> · </span>{{diffForHumans($child->created_at)}}</span>
                    </div>
                </div>
            </div>
        </div>
