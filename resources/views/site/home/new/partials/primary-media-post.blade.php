<?php
use App\Models\Posts\PostsViews;
use Illuminate\Support\Facades\Auth;
$post_object = App\Models\ActivityMedia\Media::find($post->variable);
?>
@if(is_object($post_object))
    <?php
    if(Auth::check()){
    $pv = new PostsViews;
    $pv->posts_id = $post_object->id;
    $pv->users_id = Auth::guard('user')->user()->id;
    $pv->gender = Auth::guard('user')->user()->gender;
    $pv->nationality = Auth::guard('user')->user()->nationality;
    $pv->save();
    }

?>
<?php
                $file_url = $post_object->url;
                $file_url_array = explode(".", $file_url);
                $ext = end($file_url_array);
                $allowed_video = array('mp4');
?>
@php
$today_flag = false;
$subtitle_checkin = '';
$info_title = '';
if(isset($post->checkin[0])){
    if(strtotime($post->checkin[0]->checkin_time) ==  strtotime(date('Y-m-d'))){
       $time_checking = date('H:i', strtotime($post->checkin[0]->checkin_time));
       $today_flag = true;
       $info_title = ' is checking-in '.@$post->checkin[0]->place->transsingle->title. ' at  '.diffForHumans($post->checkin[0]->checkin_time);
       $subtitle_checkin = '<a class="post-name-link" href="'.url('profile/'.$post->author->id).'">'.@$post->author->name. '</a> is checking-in '.@$post->checkin[0]->place->transsingle->title. ' Today at '.$time_checking  ;
   }else if(strtotime($post->checkin[0]->checkin_time) >  strtotime(date('Y-m-d'))){
       $subtitle_checkin = ' will check-in to '.@$post->checkin[0]->place->transsingle->title. ' '.diffForHumans($post->checkin[0]->checkin_time)  ;
       $info_title = ' will check-in to '.@$post->checkin[0]->place->transsingle->title. ' '.diffForHumans($post->checkin[0]->checkin_time);

   }else{
       $info_title = ' checked-in to '.@$post->checkin[0]->place->transsingle->title. '  '.diffForHumans($post->checkin[0]->checkin_time);
       $subtitle_checkin = ' checked-in to '.@$post->checkin[0]->place->transsingle->title. '  '.diffForHumans($post->checkin[0]->checkin_time)  ;
   }
    //dd($data_diff); 
    //dateDiffernceInDays($post->checkin[0]);   
}   
@endphp
<div class="post-block {{($today_flag == true)?"post-block-notification":"" }}">
    @if(isset($post->checkin[0]) && $today_flag)
        <div class="post-top-info-layer">
            <div class="post-info-line">
                {!! $subtitle_checkin !!}
            </div>
        </div>
    @endif
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <a class="post-name-link" href="{{url('profile/'.$post->user->id)}}">
                    <img src="{{check_profile_picture($post->user->profile_picture)}}" alt="">
                </a>
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{url('profile/'.$post->user->id)}}">{{$post->user->name}}</a>{!! get_exp_icon($post->user) !!}
                    {{-- <span class="exp-icon">
                        <i class="fas fa-city"></i>
                    </span> --}}
                </div>
                <div class="post-info">
                    @if(isset($post->checkin[0]))
                       
                        {!! $info_title !!}
                    @else
                        @if(in_array($ext, $allowed_video))
                            Uploaded a <strong>video</strong> {{diffForHumans($post_object->created_at)}}
                        @else 
                            Uploaded a <strong>photo</strong> {{diffForHumans($post_object->created_at)}}
                        @endif
                    @endif
                    <!-- New element -->
                    <!-- privacy settings -->
                    <div class="dropdown privacy-settings" style="position: relative">
                        <button class="btn btn--sm btn--outline dropdown-toggle" data-toggle="dropdown">
                            <i class="trav-globe-icon"></i>
                            PUBLIC
                        </button>
                        <div class="dropdown-menu dropdown-menu-left permissoin_show hided">
                            <a class="dropdown-item" href="#">
                                <span class="icon-wrap">
                                    <i class="trav-globe-icon"></i>
                                </span>
                                <div class="drop-txt">
                                    <p><b>Public</b></p>
                                    <p>Anyone can see this post</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="#">
                                <span class="icon-wrap">
                                    <i class="trav-users-icon"></i>
                                </span>
                                <div class="drop-txt">
                                    <p><b>Friends Only</b></p>
                                    <p>Only you and your friends can see this post</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="#">
                                <span class="icon-wrap">
                                    <i class="trav-lock-icon"></i>
                                </span>
                                <div class="drop-txt">
                                    <p><b>Only Me</b></p>
                                    <p>Only you can see this post</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- privacy settings -->
                    <!-- New element END -->
                </div>
            </div>
        </div>
        <div class="post-top-info-action">
            <div class="dropdown">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <i class="trav-angle-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Post">
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#deletePostNew">
                        <span class="icon-wrap">

                        </span>
                        <div class="drop-txt">
                            <p><b>Delete</b></p>
                            <p style="color:red">Remove this post</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @if(isset($post->text) && $post->text!='')
        <div class="post-txt-wrap">
            <div>
                <span class="less-content disc-ml-content">{!! $post->text !!}</span>
            </div>
        </div>
    @endif
    <div class="post-image-container">
        <!-- New element -->
        <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;">
            @if(in_array($ext, $allowed_video))
            <li style="padding: 0;position: relative;margin:0 0 0 0;overflow: hidden;">
                <video width="100%" height="auto">
                    <source src="{{$post_object->url}}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
                <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
            </li>

            @else
            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;">
                <a href="{{$post_object->url}}"
                    data-lightbox="media__post210172">
                    <img src="{{$post_object->url}}"
                        alt=""
                        style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                </a>
            </li>
            @endif

        </ul>
        <!-- New element END -->
    </div>
        @php
            $flag_liked  = false;
            $post = $post_object->post()->first();
            $post_type = 'Post';
            if($post->likes){
                foreach($post->likes as $like){
                    if($like->users_id == Auth::user()->id) {
                        $flag_liked = true;
                    }
                }
            }
        @endphp
        <div class="post-footer-info">
            @include('site.home.new.partials._comments-posts', ['post'=>$post, 'post_type'=>$post_type, 'check' => true, 'shareable_flag' => @$shareable_flag])
        </div>
        <div class="post-comment-wrapper" id="following{{$post->id}}">
            @if(count($post->comments)>0)
                @foreach($post->comments AS $comment)
                    @if(!user_access_denied($comment->author->id))
                        @php
                            if(!in_array($comment->users_id, $followers))
                                continue;
                        @endphp
                        @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>$post_type, 'comment'=>$comment])
                    @endif
                @endforeach
            @endif
        </div>
        <div class="post-comment-layer" data-content="comments{{$post->id}}" style='display:none;'>

            <div class="post-comment-top-info">
                <ul class="comment-filter">
                    <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                    <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
                </ul>
                <div class="comm-count-info">
                    <strong class="{{$post->id}}-opened-comments-count">{{count($post->comments) > 3? 3 : count($post->comments)}}</strong> / <span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span>
                </div>
            </div>
            <div class="post-comment-wrapper comments{{$post->id}}">
                @if(count($post->comments)>0)
                    @foreach($post->comments()->take(3)->get() AS $comment)
                        @if(!user_access_denied($comment->author->id))
                            @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment])
                        @endif
                    @endforeach
                @endif
            </div>
            @if(count($post->comments)>3)
                <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="post" data-id="{{$variable}}">Load more...</a>
            @endif

            @include('site.home.partials.new-comment_form_block', ['post_type'=>'post', 'post_id'=>$post->id])
        </div>
@endif
<script>
    $(document).ready(function(){
        var tagProfileIDs = '{{isset($tagProfileIDs) ? $tagProfileIDs : ""}}';
        var profileids = tagProfileIDs.split(',');
        if (profileids.length > 0) {
            $.each(profileids, function (i, v) {
                $(`.post-text-tag-${v}`).popover({
                    html: true,
                    content: $(`#popover-content-${v}`).html(),
                    template: '<div class="popover bottom tagging-popover" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
            })
        }
        $(document).on('click', ".read-more-link", function () {
            $(this).closest('.post-txt-wrap').find('.less-content').hide()
            $(this).closest('.post-txt-wrap').find('.more-content').show()
            $(this).hide()
        });
        $(document).on('click', ".read-less-link", function () {
            $(this).closest('.more-content').hide()
            $(this).closest('.post-txt-wrap').find('.less-content').show()
            $(this).closest('.post-txt-wrap').find('.read-more-link').show()
        });

        // Post type - shared place, slider
        $('.shared-place-slider').lightSlider({
            autoWidth: true,
            slideMargin: 22,
            pager: false,
            controls: false,
        });
        // Video
        $('.v-play-btn, .video-play-btn').click(function(){
            $(this).siblings('video').attr('controls', true);
            $(this).siblings('video').get(0).play();
            $(this).toggleClass('hide');
        });
        $('.post-image-container video, .post-block-trending-videos video').on('playing', function () {
            $(this).siblings('.v-play-btn, .video-play-btn').addClass('hide')
        });
        $('.post-image-container video, .post-block-trending-videos video').on('pause', function () {
            $(this).siblings('.v-play-btn, .video-play-btn').removeClass('hide')
        });
        // Trim more text
        if($('.less-content .trim')) {
            let content = $('.less-content .trim');
            let trim = function(content) {
                content.html(content.text().trim())
            };
            trim(content);
        }
    });
</script>