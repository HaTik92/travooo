<style>
    .mapboxgl-ctrl mapboxgl-ctrl-attrib mapboxgl-compact {
        display: none;
    }
    .mapboxgl-ctrl-bottom-left,.mapboxgl-ctrl-top-right{
        display: none;
    }
    .mapboxgl-ctrl-bottom-right{
        display: none;
    }
</style>


@php
    $mapindex = rand(1, 5000);


    $intial_latlng=  [];
    $places = [];
    $places = [];
    if(isset($weather_for_trip['current_trip'])){
        $OldDate = strtotime($weather_for_trip['current_trip']->date);
        $NewDate = date('M j, Y', $OldDate);
        $diff = date_diff(date_create($NewDate),date_create(date("M j, Y")));
        $intial_latlng =  [$weather_for_trip['trip']->places[0]->lng, $weather_for_trip['trip']->places[0]->lat];

        foreach($weather_for_trip['trip']->places as $place){
            if($place->cities_id == $weather_for_trip['city']->id){
                $places[] = [
                    'lat'=>$place->lat,
                    'lng'=>$place->lng,
                    'media'=>@$place->medias[0]->url
                ];
            }
        }
    }
    
@endphp

@if(isset($weather_for_trip))
    <div class="post-block post-block-notification">
        <div class="post-top-info-layer">
            <div class="post-info-line">
                <i class="fas fa-cloud-sun" rel="tooltip" data-toggle="tooltip" data-animation="false" title=""
                    data-original-title="The total number of views your posts have garnered"></i>
                Weather forcast about your upcoming trip <a href="/trip/plan/{{@$weather_for_trip['trip']->id}}?do=edit" class="link-place primary"><i class="trav-link-out-icon"></i>{{@$weather_for_trip['trip']->title}} </a>
            </div>
        </div>
        <div class="post-weather-inner">
            <div class="forecast-box">
                <div class="forecast-map">
                    <div id="weather_plan_map{{$mapindex}}" style='width:100%;height:400px;'></div>
                    <div class="map-days-list">
                        <span class="map-days-list-item">Day @if(@$diff->days == 0) 1 @else {{@$diff->days}} @endif</span>

                    </div>
                    <div class="map-day-forecast">
                        <strong>{{date('d F')}}</strong> @if(@$weather_for_trip['weather'][0]->WeatherText == 'stormy')<i
                            class="fas fa-cloud-showers-heavy"></i> @else<i class="fas fa-sun"></i>@endif
                        <strong>{{@$weather_for_trip['weather'][0]->Temperature->Metric->Value}}° C</strong>
                        {{-- <span>&nbsp;&middot;&nbsp;</span> Min 
                            <strong>2° C</strong> --}}
                    </div>
                </div>
                <div class="forecast-place">
                    <img src="https://s3.amazonaws.com/travooo-images2/th1100/{{@$weather_for_trip['city']->getMedias[0]->url}}"
                        alt="" class="forecast-place-img">
                    <div class="forecast-place-text">
                        <h3 class="ttl">{{@$weather_for_trip['city']->transsingle->title}}</h3>
                        <div class="weather">
                            <strong>{{date('d F')}}</strong>
                            <i class="fas fa-thermometer-half"></i> Max
                            <strong>{{@$weather_for_trip['weather'][0]->Temperature->Metric->Value}}° C</strong>

                        </div>
                        <div class="note">
                            @if(@$weather_for_trip['weather'][0]->WeatherText == 'stormy')
                            <i class="fas fa-exclamation-circle"></i>
                            Stormy and cold weather, Not suitable for the trip.
                            @else
                            {{@$weather_for_trip['weather'][0]->WeatherText}}
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var mapBounds = [
                [-180, -85],
                [180, 85]
            ];
            intial_latlng = jQuery.parseJSON('{!! json_encode( $intial_latlng) !!}');
        
            mapboxgl.accessToken = 'pk.eyJ1IjoiZHh0cmlhbiIsImEiOiJja2Zta3hoN24wNmN4MnpwbHd0aHRvOWt3In0.XuP_qBknLJLeMMSjsL2Wsg';
            var map = new mapboxgl.Map({
                container: 'weather_plan_map{{$mapindex}}', 
                style: 'mapbox://styles/mapbox/satellite-streets-v11',
                center:intial_latlng,
                maxZoom: 13,
                interactive: false
            }).on('load', function (e) {
        
                places = jQuery.parseJSON('{!! json_encode( $places,JSON_HEX_APOS) !!}');
                lat_lng= [];
                $.each(places,function(index,place){
                    var el = $(document.createElement('div'));
                        var style = 'background: url(\' https://s3.amazonaws.com/travooo-images2/'+place.media+' \');' +
                            'background-size: cover;' +
                            'background-repeat: no-repeat;' +
                            'background-position: center center;' +
                            'width: 36px;' +
                            'height: 36px;' +
                            'border-radius: 50px;' +
                            'border: 2px solid #4080ff !important;' +
                            'box-shadow: inset 0 0 0 2px #fff;' +
                            'cursor: pointer;';
                    $(el).attr('style', style);
                    $(el).addClass('marker');
                    lat_lng.push([place['lng'], place['lat']]);
                    marker = new mapboxgl.Marker($(el)[0]).setLngLat([place['lng'], place['lat']]).addTo(e.target);
                });
                    var geojson = {
                        'type': 'FeatureCollection',
                        'features': [
                            {
                                'type': 'Feature',
                                'geometry': {
                                    'type': 'LineString',
                                    'properties': {},
                                    'coordinates':lat_lng
                                }
                            }
                        ]
                    };
        
                this.addSource('LineString{{$mapindex}}', {
                    'type': 'geojson',
                    'data': geojson
                    });
                this.addLayer({
                    'id': 'LineString{{$mapindex}}',
                    'type': 'line',
                    'source': 'LineString{{$mapindex}}',
                    'layout': {
                        'line-join': 'round',
                        'line-cap': 'round'
                    },
                    'paint': {
                        'line-color': '#5487E9',
                        'line-width': 5,
                    }
                });
                var coordinates = geojson.features[0].geometry.coordinates;
        
                // Pass the first coordinates in the LineString to `lngLatBounds` &
                // wrap each coordinate pair in `extend` to include them in the bounds
                // result. A variation of this technique could be applied to zooming
                // to the bounds of multiple Points or Polygon geomteries - it just
                // requires wrapping all the coordinates with the extend method.
                var bounds = coordinates.reduce(function (bounds, coord) {
                return bounds.extend(coord);
                }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));
                
                this.fitBounds(bounds, {
                padding: 20
                });
            });;
            
            
        
    </script>
@endif