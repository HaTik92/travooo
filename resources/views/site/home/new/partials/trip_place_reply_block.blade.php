
        <div class="post-comment-row doublecomment post-comment-reply" id="commentRow{{$child->id}}" data-parent_id="{{$child->parents_id}}" >
            <div class="post-com-avatar-wrap">
                <img src="{{check_profile_picture($child->user->profile_picture)}}" alt="">
            </div>
            <div class="post-comment-text">
                <div class="post-com-name-layer">
                    <a href="#" class="comment-name">{{$child->user->name}}</a>
                    {!! get_exp_icon($child->user) !!}
                    <div class="post-com-top-action">
                        <div class="dropdown">
                            <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="trav-angle-down"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment">
                                @if($child->user->id==Auth::user()->id)
                                <a href="#" class="dropdown-item post-edit-comment" data-id="{{$child->id}}" data-post="{{$child->id}}">
                                    <span class="icon-wrap">
                                        <i class="trav-pencil" aria-hidden="true"></i>
                                    </span>
                                    <div class="drop-txt comment-edit__drop-text">
                                        <p><b>@lang('profile.edit')</b></p>
                                    </div>
                                </a>

                                <a href="javascript:;" class="dropdown-item postCommentDelete" id="{{$child->id}}" data-parent="{{$child->parents_id}}" data-type="2">
                                    <span class="icon-wrap">
                                        <img src="http://travooo.loc/assets2/image/delete.svg" style="width:20px;">
                                    </span>
                                    <div class="drop-txt comment-delete__drop-text">
                                        <p><b>Delete</b></p>
                                    </div>
                                </a>
                                @endif
                                <a href="#" class="dropdown-item" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$child->id}},this)">
                                    <span class="icon-wrap">
                                        <i class="trav-flag-icon-o"></i>
                                    </span>
                                    <div class="drop-txt comment-report__drop-text">
                                        <p><b>@lang('profile.report')</b></p>
                                    </div>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="comment-txt comment-text-{{$child->id}}">
                    @php
                       $showCommentChar = 100;
                        $ellipsestext = "...";
                        $commentmoretext = "more";
                        $commentlesstext = "less";

                        $comment_content = $child->comment;
                        $convert_content = strip_tags($comment_content);

                        if(mb_strlen($convert_content, 'UTF-8') > $showCommentChar) {

                            $l = mb_substr($comment_content, 0, $showCommentChar, 'UTF-8');

                            $html = '<span class="less-content">'.$l . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span> <a href="javascript:;" class="read-more-link">' . $commentmoretext . '</a></span>  <span class="more-content" style="display:none">' . $comment_content . ' &nbsp;&nbsp;<a href="javascript:;" class="read-less-link">' . $commentlesstext . '</a></span>';

                            $comment_content = $html;
                        }
                    @endphp
                    <p>{!!$comment_content!!}</p>
                    
                </div>
                <div class="post-image-container">
                    @if(is_object($child->medias))
                        @php
                            $index = 0;

                        @endphp
                        @foreach($child->medias AS $photo)
                            @php
                                $index++;
                                $file_url = $photo->media->url;
                                $file_url_array = explode(".", $file_url);
                                $ext = end($file_url_array);
                                $allowed_video = array('mp4');
                            @endphp
                            @if($index % 2 == 1)
                            <ul class="post-image-list" style=" display: flex;margin-bottom: 20px;">
                            @endif
                                <li style="overflow: hidden;margin:1px;">
                                    @if(in_array($ext, $allowed_video))
                                    <video style="object-fit: cover" width="192" height="210" controls>
                                        <source src="{{$file_url}}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                    @else
                                    <a href="{{$file_url}}" data-lightbox="comment__replymedia{{$child->id}}">
                                        <img src="{{$file_url}}" alt="" style="width:192px;height:210px;object-fit: cover;">
                                    </a>
                                    @endif
                                </li>
                            @if($index % 2 == 0)
                            </ul>
                            @endif
                        @endforeach
                    @endif

                </div>
                <div class="comment-bottom-info">
                    <div class="comment-info-content" posttype="Postcomment">
                        <div class="dropdown">
                            <a href="#" class="postCommentLikes like-link" id="{{$child->id}}"><i class="trav-heart-fill-icon {{($child->likes()->where('user_id', Auth::user()->id)->first())?'fill':''}}" aria-hidden="true"></i> <span class="comment-like-count">{{count($child->likes)}}</span></a>
                            @if(count($child->likes))
                            <div class="dropdown-content comment-likes-block" data-id="{{$child->id}}">
                                @foreach($child->likes()->orderBy('created_at', 'DESC')->take(7)->get() as $like)
                                <span>{{$like->user->name}}</span>
                                @endforeach
                                @if(count($child->likes)>7)
                                <span>...</span>
                                @endif
                            </div>
                            @endif
                        </div>
                        <span class="com-time"><span class="comment-dot"> · </span>{{diffForHumans($child->created_at)}}</span>
                    </div>
                </div>
            </div>
        </div>
