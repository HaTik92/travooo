<?php
$evt = $post;
if(isset($evt)){
    $post_type = 'Event';
    $rand = rand(1, 10);
    $loc = $evt->place ? $evt->place:
        ($evt->city ? $evt->city:
            ($evt->country ? $evt->country : []));
    $post = $evt; 
    $variable = $post->id;
    $json= json_decode($post->variable);
    $followers = [];
    if(Auth::check()){
        $following = \App\Models\User\User::find(Auth::user()->id);
        foreach($following->get_followers as $f){
            $followers[] = $f->followers_id;
        }
        $username = auth()->user()->username;
    }else{
        $username = 'event';
    }

    $intial_latlng = [$evt->lng, $evt->lat];
}
?>
<div class="post-type-modal-inner text-post">
    {{-- <input type="hidden" class="username" value="{{$post->author->username}}"> --}}
    <!-- Primary post block - text -->
    <div class="post-block event-item">
        <!-- Event item, copied from place module -->

        <div class="event-item__header" dir="auto">
            <div class="event-item__header-main" dir="auto">
                <div class="event-item__date" dir="auto">
                    <div class="month">{{date('M', strtotime($evt->start))}}</div>
                    <div class="day">{{date('j', strtotime($evt->start))}}</div>
                </div>
                <div class="event-item__title-wrap" dir="auto">
                    <h2 class="event-item__title" dir="auto"><a href="#" dir="auto" tabindex="0">Event in {{ @$loc->trans[0]->title }}</a></h2>
                    <div class="event-item__subtitle" dir="auto"><i class="trav-set-location-icon"></i>{{ @$loc->trans[0]->title }}</div>
                </div>
            </div>
            <div class="event-item__header-right" dir="auto">
                <div class="event-item__side-buttons" posttype="Event" dir="auto">
                    <button class="event-item__side-btn event_share_button" type="button" tabindex="0" data-toggle="modal" data-target="#shareablePost" data-type="event" data-id="{{$evt->id}}">
                        <div class="event-item__side-btn-icon"><svg class="icon icon--share">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#share')}}"></use>
                            </svg></div>
                        <div class="event-item__side-btn-text ">Share</div>
                    </button>

                    <button class="event-item__side-btn" type="button" tabindex="0" data-toggle="modal" data-id="{{@$evt->id}}" data-type="{{$post_type}}"
                        data-target="#spamReportDlg" onclick="">
                        <div class="event-item__side-btn-icon"><svg class="icon icon--flag">
                                <use xlink:href="{{asset('assets3/img/sprite.svg#flag')}}"></use>
                            </svg></div>
                        <div class="event-item__side-btn-text">Report</div>
                    </button>
                </div>
            </div>

        </div>
        {{-- <div class="event-item__map" dir="auto"> --}}
        <div class="event-plan-container" style="width: 100%;">
            <div class="event-plan-map" style="width: 100%;"">
                <div id="event_map{{$evt->id}}" style='width:100%;'></div>
            </div>
        </div>
        {{-- </div> --}}
        @php
        $is_event = true;
        $flag_liked  =false;
            if(count($post->likes)>0){
                foreach($post->likes as $like){
                    if(Auth::check() && $like->users_id == Auth::user()->id)
                        $flag_liked = true;
                }
            }
        @endphp
        <!-- Copied from post block -->
        <div class="post-footer-info">
            @include('site.home.new.partials._comments-posts', ['post'=>$post, 'post_type'=>'event'])
         </div>
        <!-- Copied from post block END -->
        <div class="event-item__meta" dir="auto">
            <div class="event-item__meta-row" dir="auto">
                <div class="event-item__meta-item" dir="auto">
                    <div class="event-item__meta-label" dir="auto">Short description</div>
                    <div class="event-item__meta-value" dir="auto">
                        {{$evt->title}}
                    </div>
                </div>
                <div class="event-item__meta-item" dir="auto">
                    <div class="event-item__meta-label" dir="auto">Address</div>
                    <div class="event-item__meta-value" dir="auto">{{@$evt->address}}</div>
                </div>
                <div class="event-item__meta-item" dir="auto">
                    <div class="event-item__meta-label" dir="auto">Duration</div>
                    <div class="event-item__meta-value" dir="auto"><strong dir="auto">{{date('j M', strtotime($evt->start))}} - {{date('j M', strtotime($evt->end))}}</strong></div>
                </div>
                <div class="event-item__meta-item" dir="auto">
                    <div class="event-item__meta-label" dir="auto">Website</div>
                    <div class="event-item__meta-value" dir="auto"><a href="#" class="web">{{@$evt->website ?? ''}}</a>
                    </div>
                </div>

                {{-- <div class="event-item__meta-item eventcomment-block comment-block-112249 event-modal-comment-content"
                    style="flex-basis: 100%;" dir="auto">
                    <div class="event-item__meta-label" dir="auto">
                        Reviews
                        <button class="reviews-toggle-btn">
                            <i class="trav-angle-down"></i>
                        </button>
                    </div>
                    <div class="reviews-wrapper">
                        <!-- Copied from profile reviews -->
                        <div class="review-inner-layer review-inner-content" dir="auto">
                            <div class="review-body-block" dir="auto">
                                <div class="review-block" dir="auto">
                                    <div class="review-foot" dir="auto">
                                        <div class="ava-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png"
                                                alt="" class="ava-image" dir="auto">
                                        </div>
                                        <div class="review-content profile-review-text" dir="auto">
                                            <!-- New element -->
                                            <a href="#" class="review-author-name">John</a>
                                            <!-- New element -->
                                            <p class="review-text-266840" dir="auto">Dolor sit amet consectetur
                                                adipiscing elit</p>
                                            <div class="comment-bottom-info" dir="auto">
                                                <div class="com-star-block" dir="auto">
                                                    <ul class="com-star-list" dir="auto">
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                    </ul>
                                                    <span class="rate-txt" dir="auto"><b>5</b> / 5</span>
                                                </div>
                                                <div class="com-time" dir="auto"><span class="comment-dot" dir="auto"> ·
                                                    </span>Sep 16, 2020</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="review-block" dir="auto">
                                    <div class="review-foot" dir="auto">
                                        <div class="ava-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png"
                                                alt="" class="ava-image" dir="auto">
                                        </div>
                                        <div class="review-content profile-review-text" dir="auto">
                                            <!-- New element -->
                                            <a href="#" class="review-author-name">Amine</a>
                                            <!-- New element -->
                                            <p class="review-text-266840" dir="auto">Dolor sit amet consectetur
                                                adipiscing elit sit amet consectetur adipiscing elit sit amet
                                                consectetur adipiscing elit sit amet consectetur adipiscing elit</p>
                                            <div class="comment-bottom-info" dir="auto">
                                                <div class="com-star-block" dir="auto">
                                                    <ul class="com-star-list" dir="auto">
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                    </ul>
                                                    <span class="rate-txt" dir="auto"><b>5</b> / 5</span>
                                                </div>
                                                <div class="com-time" dir="auto"><span class="comment-dot" dir="auto"> ·
                                                    </span>Sep 16, 2020</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="review-block" dir="auto">
                                    <div class="review-foot" dir="auto">
                                        <div class="ava-wrap" dir="auto">
                                            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1603387925_image.png"
                                                alt="" class="ava-image" dir="auto">
                                        </div>
                                        <div class="review-content profile-review-text" dir="auto">
                                            <!-- New element -->
                                            <a href="#" class="review-author-name">Katherin</a>
                                            <!-- New element -->
                                            <p class="review-text-266840" dir="auto">Dolor sit amet consectetur
                                                adipiscing elit sit amet consectetur adipiscing elit sit amet
                                                consectetur adipiscing elit sit amet consectetur adipiscing elit sit
                                                amet consectetur adipiscing elit sit amet consectetur adipiscing elit
                                            </p>
                                            <div class="comment-bottom-info" dir="auto">
                                                <div class="com-star-block" dir="auto">
                                                    <ul class="com-star-list" dir="auto">
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                        <li class="" dir="auto"><i class="trav-star-icon"
                                                                dir="auto"></i></li>
                                                    </ul>
                                                    <span class="rate-txt" dir="auto"><b>5</b> / 5</span>
                                                </div>
                                                <div class="com-time" dir="auto"><span class="comment-dot" dir="auto"> ·
                                                    </span>Sep 16, 2020</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <a href="#" class="more-reviews">More...</a>
                            </div>
                        </div>
                        <!-- Copied from profile reviews END -->
                    </div>
                </div> --}}
                <div class="event-item__meta-item eventcomment-block comment-block-112249 event-modal-comment-content"
                    style="flex-basis: 100%;" dir="auto">
                    <div class="event-item__meta-label" dir="auto">Comments</div>
                    <!-- Copied from home page -->
                    <div class="post-comment-layer" data-content="comments{{$post->id}}" style="display: block;" dir="auto">
                        <div class="post-comment-top-info">
                            <ul class="comment-filter">
                                <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                                <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
                            </ul>
                            <div class="comm-count-info">
                                <strong class="{{$post->id}}-opened-comments-count">{{count($post->comments) > 10? 10 : count($post->comments)}}</strong> / <span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span>
                            </div>
                        </div>
                        <div class="post-comment-wrapper comments{{$post->id}}" id="comments{{$post->id}}">
                            @if(count($post->comments)>0)
                                @foreach($post->comments()->take(10)->get() AS $comment)
                                    @if(!user_access_denied($comment->author->id))
                                        @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'event', 'comment'=>$comment])
                                    @endif
                                @endforeach
                            @endif
                        </div>
                            @if(count($post->comments)>10)
                                <div id="loadMore" class="loadMore" style="display: block;" dir="auto" data-type="{{$post_type}}" pagenum="0" comskip="10" data-id="{{$post->id}}">
                                    <input type="hidden" id="reply_pagenum" value="1" dir="auto">
                                    <div id="discReplyLoader" discid="2" class="disc-reply-animation" dir="auto">
                                        <div class="disc-reply-avatar-wrap" dir="auto">
                                            <div class="animation disc-reply-animation-avatar" dir="auto"></div>
                                            <div class="disc-reply-top-name animation" dir="auto"></div>
                                        </div>
                                        <div class="post-comment-text" dir="auto">
                                            <div class="disc-reply-an-info animation" dir="auto"></div>
                                            <div class="disc-reply-an-sec-info animation" dir="auto"></div>
                                        </div>
                                    </div>
                                </div>
                                {{-- <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="post" data-id="{{$post->id}}">Load more...</a> --}}
                            @endif
                            
                            @include('site.home.partials.new-comment_form_block', ['post_type'=>'event', 'post_id'=>$post->id])
                        </div>
                    </div>
                    <!-- Copied from home page END -->
                </div>
            </div>
        </div>

        <!-- Event item, copied from place module END -->
    </div>
    <!-- Primary post block - text END -->
</div>
<script>
    $(`#event_map{{$evt->id}}`).css('height', 300);
    $(".event-plan-map").css('max-height', 300)
    $(document).on('click', '.morelink', function () {
        var moretext = "see more";
        var lesstext = "see less";
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
    $(document).on('click', '.reviews-toggle-btn', function () {
        $(this).toggleClass('active');
        $(this).parent().siblings('.reviews-wrapper').toggleClass('show')
    });

    intial_latlng = jQuery.parseJSON('{!! json_encode( $intial_latlng) !!}');
    mapboxgl.accessToken = 'pk.eyJ1IjoiZHh0cmlhbiIsImEiOiJja2Zta3hoN24wNmN4MnpwbHd0aHRvOWt3In0.XuP_qBknLJLeMMSjsL2Wsg';
    var _locationData  = JSON.parse('[{"type":"place","id":22,"country_id":104,"city_id":180,"title":"Banca Popolare di Spoleto","lat":"{{$evt->lat}}","lng":"{{$evt->lng}}","image":"{{asset('assets2/image/events/'.$evt->category.'/'.$rand.'.jpg')}}"}]');
    var marker ='';

    var map = new mapboxgl.Map({
            container: 'event_map{{$evt->id}}', 
            style: 'mapbox://styles/mapbox/streets-v11',
            center:intial_latlng,
            // zoom: 15,
            interactive: false
        }).on('load', function (e) {
            lat_lng= [];
            $.each(_locationData,function(index,place){
                var el = $(document.createElement('div'));
                    var style = 'background: url(\''+place['image']+'\');' +
                        'background-size: cover;' +
                        'background-repeat: no-repeat;' +
                        'background-position: center center;' +
                        'width: 36px;' +
                        'height: 36px;' +
                        'border-radius: 50px;' +
                        'border: 2px solid #4080ff !important;' +
                        'box-shadow: inset 0 0 0 2px #fff;' +
                        'cursor: pointer;';
                $(el).attr('style', style);
                $(el).addClass('marker');
                lat_lng.push([place['lng'], place['lat']]);
                marker = new mapboxgl.Marker($(el)[0]).setLngLat([place['lng'], place['lat']]).addTo(e.target);
            });
                var geojson = {
                    'type': 'FeatureCollection',
                    'features': [
                        {
                            'type': 'Feature',
                            'geometry': {
                                'type': 'LineString',
                                'properties': {},
                                'coordinates':lat_lng
                            }
                        }
                    ]
                };

            this.addSource('LineString{{$evt->id}}', {
                'type': 'geojson',
                'data': geojson
                });
            this.addLayer({
                'id': 'LineString{{$evt->id}}',
                'type': 'line',
                'source': 'LineString{{$evt->id}}',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#5487E9',
                    'line-width': 5,
                }
            });
            var coordinates = geojson.features[0].geometry.coordinates;

            // Pass the first coordinates in the LineString to `lngLatBounds` &
            // wrap each coordinate pair in `extend` to include them in the bounds
            // result. A variation of this technique could be applied to zooming
            // to the bounds of multiple Points or Polygon geomteries - it just
            // requires wrapping all the coordinates with the extend method.
            var bounds = coordinates.reduce(function (bounds, coord) {
                return bounds.extend(coord);
            }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));
            
            this.fitBounds(bounds, {
                padding: 20,
                zoom: 16
            });
        });;

</script>
<!-- Event text popup END -->