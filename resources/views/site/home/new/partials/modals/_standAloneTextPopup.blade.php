<!-- Standalone text popup -->
<?php
$me = auth()->user();
$variable = $post->variable;
$post_type = "Post";
$media_array = [];
if(isset($post->medias)){
    foreach($post->medias as $media){
        $media_array[] =$media->medias_id;
    }
    $media_array = App\Models\ActivityMedia\Media::whereIn('id',$media_array)->get();
}
$followers = [];
if(Auth::check()){
$following = \App\Models\User\User::find($me->id);
foreach($following->get_followers as $f):
    $followers[] = $f->followers_id;
endforeach;
}
$today_flag = false;
$post_counter= 0;
$info_title = '';
if(isset($post->checkin[0])){
if(strtotime($post->checkin[0]->checkin_time) == strtotime(date('Y-m-d'))){
$time_checking = date('H:i', strtotime($post->checkin[0]->checkin_time));
$today_flag = true;
$info_title = ' is checking-in '.@$post->checkin[0]->place->transsingle->title. '
'.diffForHumans($post->checkin[0]->checkin_time);
$subtitle_checkin = '<a class="post-name-link" href="'.url('profile/'.$post->author->id).'">'.@$post->author->name.
    '</a> is checking-in '.@$post->checkin[0]->place->transsingle->title. ' Today at '.$time_checking ;
}else if(strtotime($post->checkin[0]->checkin_time) > strtotime(date('Y-m-d'))){
$subtitle_checkin = ' will check-in to '.@$post->checkin[0]->place->transsingle->title. '
'.diffForHumans($post->checkin[0]->checkin_time) ;
$info_title = ' will check-in to '.@$post->checkin[0]->place->transsingle->title. '
'.diffForHumans($post->checkin[0]->checkin_time);

}else{
$info_title = ' Checked-in to '.@$post->checkin[0]->place->transsingle->title. '
'.diffForHumans($post->checkin[0]->checkin_time);
$subtitle_checkin = ' Checked-in to '.@$post->checkin[0]->place->transsingle->title. '
'.diffForHumans($post->checkin[0]->checkin_time) ;
}
//dateDiffernceInDays($post->checkin[0]);
}
?>
<!-- Media text popup -->
{{-- <div class="modal white-style media-text-modal" data-backdrop="false" id="mediaTextPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> --}}
{{-- <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
    <i class="trav-close-icon"></i>
</button> --}}
<div class="post-type-modal-inner {{count($media_array) > 0 && $post_type == "Post" ? '' : 'text-post'}}">
    <!-- Media block -->
    <input type="hidden" class="username" value="{{$post->author->username}}">
    @if (count($media_array) > 0 && $post_type == "Post")
    <div class="media-block">
        @if (count($media_array) > 0)
        @if(isset($post->checkin[0]))
        <div class="check-in-info d-block">
            <i class="trav-set-location-icon"></i> <strong>{{@$post->checkin[0]->place->transsingle->title}}</strong>
            @if(isset($post->checkin[0]->city->transsingle->title))
            , {{@$post->checkin[0]->city->transsingle->title}}, {{@$post->checkin[0]->country->transsingle->title}}
            @endif
            <i class="trav-clock-icon"></i>{{diffForHumans(@$post->checkin[0]->checkin_time)}}
        </div>
        @endif
        <div class="mediaTextPopupSlider slider">
            @foreach ($media_array as $post_object)
            @php
            $file_url = $post_object->url;
            $file_url_array = explode(".", $file_url);
            $ext = end($file_url_array);
            $allowed_video = array('mp4');
            // $post_counter++;
            @endphp
            <!-- New element -->
            @if(in_array($ext, $allowed_video))
            {{-- @if( $post_counter <=3) --}}
            <video width="100%" height="auto" data-id="{{'video_tmp'.$post_object->id}}" controls>
                <source src="{{$post_object->url}}" type="video/mp4">
                Your browser does not support the video tag.
            </video>
            {{-- <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a> --}}
            {{-- @endif --}}
            @else
            {{-- @if( $post_counter <=3) --}}
            <div><img src="{{$post_object->url}}" alt=""></div>
            {{-- @endif --}}
            @endif
            <!-- New element END -->
            @endforeach
            {{-- </div> --}}
        </div>
        @endif
    </div>
    @endif
    <!-- Media block END -->
    <!-- Primary post block - text -->
    <div class="post-block scrollable @if(isset($shareable_flag)) post-block-notification @else {{($today_flag == true)?"post-block-notification":""}} @endif" id="post_{{$post->id}}"">
    @if($shareable_flag)
    <div class=" post-top-info-layer">
        <div class="post-info-line">
            @php
            $sharing_users_list =\App\Models\User\User::whereIn('id',$sharing_users_list)->get();
            @endphp
            @if($sharing_users_list->count()>1)
            <a class="post-name-link" target="blank"
                href="{{url('profile/'.@$sharing_users_list[0]->id)}}">{{@$sharing_users_list[0]->name}}</a> , <a
                class="post-name-link" target="blank"
                href="{{url('profile/'.@$sharing_users_list[1]->id)}}">{{@$sharing_users_list[1]->name}}</a> <a
                class="post-name-link">+{{$sharing_users_list->count()-2}}</a> shared this post
            @else
            <a class="post-name-link" target="blank"
                href="{{url('profile/'.@$sharing_users_list[0]->id)}}">{{@$sharing_users_list[0]->name}}</a> shared this
            post
            @endif
        </div>
    </div>
    @elseif(isset($post->checkin[0]) && $today_flag)
    <div class="post-top-info-layer">
        <div class="post-info-line">
            {!! $subtitle_checkin !!}
        </div>
    </div>
    @endif
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <a class="post-name-link" href="{{url('profile/'.$post->author->id)}}">
                    <img src="{{check_profile_picture($post->author->profile_picture)}}" alt="">
                </a>
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{url('profile/'.$post->author->id)}}">{{$post->author->name}}</a>
                    <span class="exp-icon">EXP</span>
                </div>
                <div class="post-info" dir="auto">
                    @php
                    $uniqueurl = url('post/'. $post->author->username, $post->id);
                    @endphp
                    <a href="{{$uniqueurl}}" target="_blank" class="post-title-link">
                        @if(isset($post->checkin[0]))
                        {!! $info_title !!}
                        @else
                        @if(count($media_array) == 1)
                        @php
                        $post_object = $media_array[0];
                        $file_url = $post_object->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                        @endphp
                        @if(in_array($ext, $allowed_video))
                        Uploaded a <strong>video</strong> {{diffForHumans($post->created_at)}}
                        @else
                        Uploaded a <strong>photo</strong> {{diffForHumans($post->created_at)}}
                        @endif
                        @elseif(count($media_array) >1)
                        Uploaded <strong> media </strong> {{diffForHumans($post->created_at)}}
                        @else
                        {{diffForHumans($post->created_at)}}
                        @endif
                        @endif
                        @php
                        $admin_post_flag = false;
                        if(Auth::check() && $post->author->id == Auth::user()->id)
                        $admin_post_flag=true;
                        @endphp
                    </a>
                    @if($post->permission == 1)
                    <i class="trav-users-icon"></i>
                    @elseif($post->permission == 2)
                    <i class="trav-lock-icon"></i>
                    @else
                    <i class="trav-globe-icon"></i>
                    @endif
                </div>
            </div>
        </div>
        <!-- New elements -->
        <div class="post-top-info-action">
            <div class="dropdown">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <i class="trav-angle-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" dir="auto"
                    x-placement="bottom-end"
                    style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                    @if(Auth::check() && Auth::user()->id != $post->author->id)
                    @if(in_array($post->author->id,$followers))
                    <a class="dropdown-item follow_unfollow_post_users" data-type="unfollow"
                        data-user_id="{{ $post->author->id}}" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-user-plus-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Unfollow {{ $post->author->name ?? 'User' }}</b></p>
                            <p dir="auto">Stop seeing posts from {{ $post->author->name ?? 'User' }}</p>
                        </div>
                    </a>
                    @endif
                    @endif
                    <a class="dropdown-item" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}"
                        data-tab="shares{{$variable}}" data-toggle="modal" data-id="{{$variable}}" data-type="text">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Share</b></p>
                            <p dir="auto">Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-link" dir="auto"></i>
                        </span>
                        <div class="drop-txt">
                            <p dir="auto"><b dir="auto">Copy Link</b></p>
                            <p dir="auto">Paste the link anywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#sendToFriendModal" data-id="post_{{$post->id}}" data-toggle="modal"
                        data-target="" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-on-travo-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                            <p dir="auto">Share with your friends</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#spamReportDlgNew" data-toggle="modal" data-target="#" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-flag-icon-o" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Report</b></p>
                            <p dir="auto">Help us understand</p>
                        </div>
                    </a>
                    @if(Auth::check() && Auth::user()->id == $post->author->id)
                    <a class="dropdown-item" data-toggle="modal" data-type="{{$post_type}}"
                        data-element="post_{{$post->id}}" href="#deletePostNew" data-id="{{$post->id}}">
                        <span class="icon-wrap">
                            <i class="far fa-trash-alt"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b style="color: red;">Delete</b></p>
                            <p>Delete this post forever</p>
                        </div>
                    </a>
                    @endif
                </div>
            </div>
        </div>
        <!-- New elements END -->
    </div>
    <!-- New element -->
    <div class="post-txt-wrap">
        <div>
            <span class="less-content disc-ml-content">{!! substr($post->text,0, 500) !!}
                @if(strlen($post->text)>500)<span class="moreellipses">...</span><a href="javascript:;"
                    class="read-more-link">&nbsp;More</a>@endif
                <span>
                    <span class="more-content disc-ml-content" style="display: none;">{!! $post->text !!} <a
                            href="javascript:;" class="read-less-link">&nbsp;Less</a></span>
        </div>
    </div>
    <!-- Post text tag tooltip content -->
    <div id="popover-content" style="display: none;">
        <div class="user-hero">
            <img src="https://s3.amazonaws.com/travooo-images2/post-comment-photo/831_1601563217_images.jpg" alt="">
            <div class="user-hero-text">
                <div class="name">Sue Perez</div>
                <div class="location">
                    <i class="trav-set-location-icon"></i>
                    United States
                </div>
            </div>
            <button type="button" class="btn btn-light-primary btn-bordered">Follow </button>
        </div>
        <div class="user-badges">
            <div class="badges-ttl">Badges</div>
            <div class="badges-list">
                <img src="https://travooo.com/assets2/image/badges/1_1.png" alt="Socializer">
                <img src="https://travooo.com/assets2/image/badges/5_1.png" alt="Personality">
                <img src="https://travooo.com/assets2/image/badges/1_1.png" alt="Socializer">
                <img class="fade" src="https://travooo.com/assets2/image/badges/5_1.png" alt="Personality">
                <img class="fade" src="https://travooo.com/assets2/image/badges/1_1.png" alt="Socializer">
            </div>
            <button type="button" class="btn btn-light-primary btn-bordered">Follow </button>
        </div>
        <div class="actions">
            <a class="btn btn-light-grey">Follow</a>
            <a class="btn btn-light-grey popover-msg-btn">Message</a>
        </div>
    </div>
    <!-- Post text tag tooltip content END -->
    <!-- New element END -->
    <div class="post-image-container">
    </div>
    @php
    $flag_liked =false;
    if(count($post->likes)>0){
    foreach($post->likes as $like){
    if(Auth::check() && $like->users_id == Auth::user()->id)
    $flag_liked = true;
    }
    }
    $post = @$shared_post ?? $post;
    @endphp
    <div class="post-footer-info">
        @include('site.home.new.partials._comments-posts', ['post'=>$post, 'check' => false])
    </div>
    <div class="post-comment-wrapper" id="following{{$post->id}}">
        @if(count($post->comments)>0)
        @foreach($post->comments AS $comment)
        @if(!user_access_denied($comment->author->id))
        @php
        if(!in_array($comment->users_id, $followers))
        continue;
        @endphp
        @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment, 'comment_popup' => 1])
        @endif
        @endforeach
        @endif
    </div>
    <!-- Copied from home page -->
    <div class="post-comment-layer" data-content="comments{{$post->id}}" style="display: block;" dir="auto">
        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')
                </li>
            </ul>
            <div class="comm-count-info">
                <strong
                    class="{{$post->id}}-opened-comments-count">{{count($post->comments) > 10? 10 : count($post->comments)}}</strong>
                / <span class="{{$post->id}}-comments-count">{{count($post->comments)}}</span>
            </div>
        </div>
        <div class="post-comment-wrapper comments{{$post->id}}" id="comments{{$post->id}}">
            @if(count($post->comments)>0)
            @foreach($post->comments()->take(10)->get() AS $comment)
            @if(!user_access_denied($comment->author->id))
            @include('site.home.partials.new-post_comment_block', ['post_id'=>$post->id, 'type'=>'post', 'comment'=>$comment, 'comment_popup' => 1])
            @endif
            @endforeach
            @endif
        </div>
        @if(count($post->comments)>10)
        <div id="loadMore" class="loadMore" style="display: block;" dir="auto" data-type="{{$post_type}}" pagenum="0"
            comskip="10" data-id="{{$post->id}}">
            <input type="hidden" id="reply_pagenum" value="1" dir="auto">
            <div id="discReplyLoader" discid="2" class="disc-reply-animation" dir="auto">
                <div class="disc-reply-avatar-wrap" dir="auto">
                    <div class="animation disc-reply-animation-avatar" dir="auto"></div>
                    <div class="disc-reply-top-name animation" dir="auto"></div>
                </div>
                <div class="post-comment-text" dir="auto">
                    <div class="disc-reply-an-info animation" dir="auto"></div>
                    <div class="disc-reply-an-sec-info animation" dir="auto"></div>
                </div>
            </div>
        </div>
        {{-- <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="post" data-id="{{$post->id}}">Load
        more...</a> --}}
        @endif

        @include('site.home.partials.new-comment_form_block', ['post_type'=>'post', 'post_id'=>$post->id])
    </div>
</div>
{{-- </div>
<!-- Primary post block - text END -->
</div> --}}
{{-- @if (count($media_array) > 0)
</div>
@endif --}}
<script>
    slider = $('.mediaTextPopupSlider').lightSlider({
        item: 1,
        loop: false,
        slideMargin: 0,
        pager: false,
        enableDrag: true
    });
    if ($(".mediaTextPopupSlider").find("video[data-id]").length > 1) {
        let videoIndex = $(".lslide").map((index, el) => $(el).data("id") ==
            `video_tmp${localStorage.getItem("currentVideoId")}` ? index : null)[0]
        slider.goToSlide(videoIndex)
    }
    $('.post-type-modal-inner .media-block .v-play-btn').click(function () {
        $(this).siblings('video').get(0).play();
        $(this).toggleClass('hide');
    });
    $('.post-type-modal-inner .media-block video').on('playing', function () {
        $(this).siblings('.v-play-btn, .video-play-btn').addClass('hide')
    });
    $('.post-type-modal-inner .media-block video').on('pause', function () {
        $(this).siblings('.v-play-btn, .video-play-btn').removeClass('hide')
    });
    // });
</script>
<!-- Media text popup END -->
