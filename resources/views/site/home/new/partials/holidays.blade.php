@php
    ($post->type == "city")?$trans = $post->city_trans:$trans = $post->country_trans;
    ($post->cities_id)?$holId = $post->cities_id:$holId = $post->countries_id;
@endphp
<div class="post-block post-block-holiday" data-iso_code="{{$post->iso_code}}">
    <div class="popover bottom tagging-popover white fade bs-popover-bottom holiday_popover_{{$post->id}} d-none" role="tooltip" id="popover790920" style="position: absolute;transform: translate3d(-28px, 73px, 0px);top: 0px;left: 0px;will-change: transform;" x-placement="bottom">
        <div class="arrow" style="left: 145px;"></div>
        <div class="popover-body">
            <div class="holiday-hero">
                <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/620/5e9c1c38fed3b1b6ed2196d116d8f6c85902f1da.jpg" alt="" class="cover">
                
                <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($post->iso_code)}}.png" alt="" class="flag">
            </div>
            <div class="holiday-desc">
                <div class="holiday-desc-country">{{$trans['title']}}</div>
                @if ($post->city_trans)
                <div class="holiday-desc-location">City in {{$trans['country_title']}}</div>
                @endif
                                                                                                                                                                        
                @if(Auth::check())
                    <?php
                        if ($post->type == "city") {
                            $placefollow = \App\Models\City\CitiesFollowers::where('users_id', Auth::user()->id)->where('cities_id', $post->cities_id)->get();
                        }
                        elseif ($post->type == "country") {
                            $placefollow = \App\Models\Country\CountriesFollowers::where('users_id', Auth::user()->id)->where('countries_id', $post->countries_id)->get();
                        }
                    ?>
                    @if(count($placefollow) > 0)
                    <button type="button" class="btn btn-light-grey btn-bordered follow_{{$holId}}" onclick="newsfeed_city_country_following('unfollow', '{{$post->type}}', {{$holId}})">Unfollow
                    </button>
                    @else
                    <button type="button" class="btn btn-light-primary btn-bordered follow_{{$holId}}" onclick="newsfeed_city_country_following('follow', '{{$post->type}}', {{$holId}})">@lang('home.follow')
                    </button>
                    @endif
                @else
                    <button type="button" class="btn btn-light-primary btn-bordered open-login">@lang('home.follow')</button>
                @endif                                                                            
                <div class="holiday-desc-followers"><span class="foll_{{$holId}}">{{($post->count_city_folower)?$post->count_city_folower:$post->count_country_folower}}</span> Followers</div>
                <a href="{{route($post->type.'.index', $holId)}}" class="btn btn-link holiday-desc-page-link">View Page</a>
            </div>
        </div>
    </div>
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">

                <span class="avatar-icon primary">
                    <i class="trav-calendar-icon"></i>
                </span>

            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="#">Independence day</a>
                </div>
                <div class="post-info">

                    <span class="badge badge-light">National Holiday</span> in <a
                        class="link-place post-holiday-popover" data-show_holiday="{{$post->id}}">{{$trans['title']}}</a>


                </div>
            </div>
        </div>

    </div>
    <div class="post-image-container post-follow-container">
        <ul class="post-image-list">
            <li>
                <img class="lazy" alt=""
                    src="{{url($post->media)}}"
                    style="">
            </li>
        </ul>
        <div class="post-follow-block">
            <div class="follow-txt-wrap">

                <div class="holiday-follow-text">
                    <div class="date">
                        <div class="month">{{\Carbon\Carbon::parse($post->date)->format('M')}}</div>
                        <div class="day">{{\Carbon\Carbon::parse($post->date)->format('d')}}</div>
                    </div>
                    <div class="desc">
                        {!!$post->trans['description']!!}
                    </div>
                </div>

            </div>
            <div class="follow-btn-wrap check-follow-holiday" data-id="{{$holId}}">
                
                @if(Auth::check())
                    <?php
                        if ($post->type == "city") {
                            $placefollow = \App\Models\City\CitiesFollowers::where('users_id', Auth::user()->id)->where('cities_id', $post->cities_id)->get();
                        }
                        elseif ($post->type == "country") {
                            $placefollow = \App\Models\Country\CountriesFollowers::where('users_id', Auth::user()->id)->where('countries_id', $post->countries_id)->get();
                        }
                    ?>
                    @if(count($placefollow) > 0)
                    <button type="button" class="btn btn-light-grey btn-bordered follow_{{$holId}}" onclick="newsfeed_city_country_following('unfollow', '{{$post->type}}', {{$holId}})">Unfollow
                    </button>
                    @else
                    <button type="button" class="btn btn-light-primary btn-bordered follow_{{$holId}}" onclick="newsfeed_city_country_following('follow', '{{$post->type}}', {{$holId}})">@lang('home.follow')
                    </button>
                    @endif
                @else
                    <button type="button" class="btn btn-light-primary btn-bordered open-login">@lang('home.follow')</button>
                @endif
            </div>
        </div>
    </div>
</div>