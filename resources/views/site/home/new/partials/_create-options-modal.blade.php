<div class="modal white-style" data-backdrop="false"  id="createNewActions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style" role="document">
        <div class="post-block">
            <div class="modal-header p-20">
                <h4 class="side-ttl f-18">Create</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="trav-close-icon"></i>
                </button>
            </div>
            <div class="modal-body p-0">
                <ul class="cp-list-block">
                    <li>
                        <a href="javascript:;" class="cp-action-link cp-add-post" data-type="home" data-url="{{url('/home#create-post')}}">
                            <span class="cp-icon-wrap"><img src="{{asset('assets2/image/icons/new-pen-icon.png')}}"></span>
                            <div class="cp-text-layer">
                                <p class="cp-text-title">Write a Post</p>
                                <p class="cp-text-subtitle">Speack your mind <strong>{{Auth::user()->name}}</strong></p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('trip.plan', 0) }}"  class="cp-action-link">
                            <span class="cp-icon-wrap"><img src="{{asset('assets2/image/icons/new-trip-plan.png')}}"></span>
                            <div class="cp-text-layer">
                                <p class="cp-text-title">Create a Trip Plan</p>
                                <p class="cp-text-subtitle">Plan your next trip or create new one</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" class="cp-action-link cp-add-post" data-type="report" data-url="{{url('reports/list#create')}}">
                            <span class="cp-icon-wrap"><img src="{{asset('assets2/image/icons/new-travelog-icon.png')}}"></span>
                            <div class="cp-text-layer">
                                <p class="cp-text-title">Post a Travelog</p>
                                <p class="cp-text-subtitle">Tell your stories and show your creativity</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;"  class="cp-action-link cp-add-post" data-type="discussions" data-url="{{url('discussions#create')}}">
                            <span class="cp-icon-wrap"><img src="{{asset('assets2/image/icons/new-ask-icon.png')}}"></span>
                            <div class="cp-text-layer">
                                <p class="cp-text-title">Ask a Question</p>
                                <p class="cp-text-subtitle">Get answers from people with unique insights</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $(document).on('click', '.open-create-actions', function(){
            $('#createNewActions').modal('show')
        })
    })

    $(document).on('click', '.cp-add-post', function(){
        var current_url =  window.location.pathname.split('/')[1]
        var type = $(this).attr('data-type')

        switch (current_url){
            case 'home':
                if(type == 'home'){
                    $('#createNewActions').modal('hide')
                    $('.create-new-post-form-trigger').click()
                }else{
                    window.location.href = $(this).attr('data-url')
                }
                break;
            case 'reports':
                if(type == 'report'){
                    $('#createNewActions').modal('hide')
                    $('#create_new_report').click()
                }else{
                    window.location.href = $(this).attr('data-url')
                }
                break;
            case 'discussions':
                if(type == 'discussions'){
                    $('#createNewActions').modal('hide')
                    $('.btn-add-discussion').click()
                }else{
                    window.location.href = $(this).attr('data-url')
                }
                break;
            case 'place':
                if(type == 'home'){
                    $('#createNewActions').modal('hide')
                    $('html, body').animate({
                        scrollTop: ($('#add-post').offset().top-70)
                    },500);
                }else{
                    window.location.href = $(this).attr('data-url')
                }
                break;
                default:
                    window.location.href = $(this).attr('data-url')
        }
    })
</script>
