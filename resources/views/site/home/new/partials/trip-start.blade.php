<?php 
$followers = [];
if(Auth::check()){
    $following = \App\Models\User\User::find(Auth::user()->id);
    foreach($following->get_followers as $f){
        $followers[] = $f->followers_id;
    }
}
    
    $me = false;
    $places = [];
    $countries = [];
    if(isset($my_friend_started_trip)){
        $trip_place_details =  \App\Models\TripPlaces\TripPlaces::find($my_friend_started_trip);
        if(is_object($trip_place_details) && isset($trip_place_details,$trip_place_details->trip) ){
            $trip = $trip_place_details->trip;
            $place = $trip_place_details->place;

            if(isset($trip->published_places) && count($trip->published_places)>0){
                $intial_latlng =  [$trip->published_places[0]->place->lng, $trip->published_places[0]->place->lat];
                foreach ($trip->published_places as $key => $value) {
                    $places[] = ['cities_id'=>$value->place->cities_id,'country_id'=>$value->place->countries_id,'place_id'=>$value->place->id,'lat'=>$value->place->lat,'lng'=>$value->place->lng,'title'=>$value->place->transsingle->title,'medias'=>@$value->place->medias[0]->url,'media'=>@$value->place->medias[0]->url];
                    $country = \App\Models\Country\Countries::find($value->place->countries_id);
                    if($country->iso_code != '00')
                        $countries[$country->iso_code] ="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/".strtolower($country->iso_code).".png";
                }
            }
            if(isset($trip->users_id)){
                $me = $trip->author;
            }
        }
    }
    $mapindex = rand(1, 5000);

?>


@if(isset($trip_place_details) && isset( $trip) && $me && isset($countries) && isset($place) && isset($places))

<div class="post-block post-block-notification travlog">
    <div class="post-top-info-layer">
        <div class="post-info-line">
            The trip <a class="post-name-link" href="/trip/plan/{{@$trip->id}}?do=edit">{{@$trip->title}}</a> has
            started
        </div>
    </div>
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap ava-50">
                <img src="{{check_profile_picture($me->profile_picture)}}" alt="">
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{ url_with_locale('profile/'.@$me->id)}}">{{@$me->name}}</a>{!! get_exp_icon(@$me) !!}
                </div>
                <div class="post-info">
                    <strong>Started</strong> his <strong>trip</strong>
                </div>
            </div>
        </div>
        <div class="post-top-info-action">
            <div class="dropdown">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <i class="trav-angle-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion">
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg"
                        onclick="injectData(3458,this)">
                        <span class="icon-wrap">
                            <i class="trav-user-plus-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Unfollow User</b></p>
                            <p>Stop seeing posts from User</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg"
                        onclick="injectData(3458,this)">
                        <span class="icon-wrap">
                            <i class="trav-share-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Share</b></p>
                            <p>Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg"
                        onclick="injectData(3458,this)">
                        <span class="icon-wrap">
                            <i class="trav-link"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Copy Link</b></p>
                            <p>Paste the link anyywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg"
                        onclick="injectData(3458,this)">
                        <span class="icon-wrap">
                            <i class="trav-share-on-travo-icon"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Send to a Friend</b></p>
                            <p>Share with your friends</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg"
                        onclick="injectData(3458,this)">
                        <span class="icon-wrap">
                            <i class="trav-flag-icon-o"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Report</b></p>
                            <p>Help us understand</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg"
                        onclick="injectData(3458,this)">
                        <span class="icon-wrap">
                            <i class="far fa-edit"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b>Edit Post</b></p>
                            <p>Edit the text or add the media</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlg"
                        onclick="injectData(3458,this)">
                        <span class="icon-wrap">
                            <i class="far fa-trash-alt"></i>
                        </span>
                        <div class="drop-txt">
                            <p><b style="color: red;">Delete</b></p>
                            <p>Delete this post forever</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- New elements -->
    <div class="trip-plan-update">
        @if(isset($trip->cover) && $trip->cover!='')
            <img src="{{$trip->cover}}"
            alt="" class="trip-plan-update-cover">
        @else 
        <img src="https://s3.amazonaws.com/travooo-images2/discussion-photo/1795_0_1600076118_09a64fea2933f6da77ab07d671d1f678-south-korea.jpg"
        alt="" class="trip-plan-update-cover">
        {{-- <div class="trip-plan-update-map" id="start_plan_map{{$mapindex}}" style="width:100%;height:500px;"></div> --}}

        @endif
        <div class="trip-plan-update-inner">
            <div class="trip-plan-update-badge">
                <span><i class="trav-dot-circle-icon"></i></span> Traveling
            </div>
            <div class="trip-plan-update-details">
                <div class="flags">
                    @foreach ($countries as $key=>$item)
                    <img src="{{$item}}" alt="">
                    @endforeach
                </div>
                <h3 class="ttl" style="color: white">{{@$trip->title}}</h3>
                <div class="stats">
                    <b>&middot;</b> <b>${{@$trip->budget}}</b> Budget <b>&middot;</b> <b>{{count(@$places)}}</b>
                    Destinations
                </div>
                <div class="desc">
                    <p>{{$trip->description}}</p>
                    <a href="/trip/plan/{{@$trip->id}}?do=edit" class="btn btn-light-primary">View Plan</a>
                </div>
                <div class="post-image-container">
                    <!-- New element -->
                    @if(count(@$places)> 0)
                    <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                        @php
                        $countable = 0;
                        @endphp
                        @foreach ($places as $item)
                        @php
                        $countable++;
                        $media_url = '';
                        if(isset($item['media']))
                            $media_url = "https://s3.amazonaws.com/travooo-images2/".$item['media'];
                        else 
                            $media_url = 'https://www.travooo.com/assets2/image/placeholders/place.png';
                        @endphp
                        @if($countable<=4)

                         <li
                            style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                            <a href="{{ $media_url}}"
                                data-lightbox="media__post{{@item['id']}}">
                                <img src="{{ $media_url}}" alt=""
                                    style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                            </a>
                            </li>
                            @elseif($countable ==4 && count(@$places) >4)
                            <li
                                style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                <a class="more-photos"
                                    href="{{ $media_url}}"
                                    data-lightbox="media__post199366">+{{count(@$places)-$countable}}</a>
                                <a href="{{ $media_url}}"
                                    data-lightbox="media__post199366">
                                    <img src="{{ $media_url}}" alt=""
                                        style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                                </a>
                            </li>
                            @endif
                            @endforeach


                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- New elements END -->
    @php
        $variable = $trip->id;
             $trip_plan=$trip;
            $flag_liked  =false;
           if(count($trip_plan->likes->where('places_id',null))>0){
               foreach($trip_plan->likes as $like){
                   if(Auth::check() && $like->users_id == Auth::user()->id)
                        $flag_liked = true;
               }
           }
        @endphp
        <div class="post-footer-info">
            @include('site.home.new.partials._comments-posts', ['post'=>$trip_plan, 'post_type'=>'trip'])
        </div>
        <div class="post-comment-wrapper" id="following{{$trip_plan->id}}">
            @if(count($trip_plan->comments)>0)
                @foreach($trip_plan->comments AS $comment)
                    @php
                    if(!in_array($comment->users_id, $followers))
                        continue;
                    @endphp
                     @include('site.home.partials.new-post_comment_block', ['post_id'=>$trip_plan->id, 'type'=>'trip', 'comment'=>$comment])
                @endforeach
            @endif
        </div>
        <div class="post-comment-layer" data-content="comments{{$trip_plan->id}}" style='display:none;'>

            <div class="post-comment-top-info">
                <ul class="comment-filter">
                    <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                    <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
                </ul>
                <div class="comm-count-info">
                   <strong class="{{$trip_plan->id}}-opened-comments-count">{{count($trip_plan->comments) > 3? 3 : count($trip_plan->comments)}}</strong> / <span class="{{$trip_plan->id}}-comments-count">{{count($trip_plan->comments)}}</span>
                </div>
            </div>
            <div class="ss post-comment-wrapper sortBody" id="comments{{$trip_plan->id}}">
                @if(count($trip_plan->comments)>0)
                    @foreach($trip_plan->comments()->take(3)->get() AS $comment)
                        @if(!user_access_denied($comment->author->id))
                            @include('site.home.partials.new-post_comment_block', ['post_id'=>$trip_plan->id, 'type'=>'trip', 'comment'=>$comment])
                        @endif
                    @endforeach
                @endif
            </div>
        @if(count($trip_plan->comments)>3)
            <a href="javascript:;" class="load-more-comment-link" data-type="trip" pagenum="0" comskip="3" data-id="{{$variable}}">Load more...</a>
        @endif

        @include('site.home.partials.new-comment_form_block', ['post_type'=>'trip', 'post_id'=>$variable])
        </div>
        <!-- Removed comments block -->
</div>
<script>
    var mapBounds = [
            [-180, -85],
            [180, 85]
        ];
        intial_latlng = jQuery.parseJSON('{!! json_encode( @$intial_latlng) !!}');
    
        mapboxgl.accessToken = 'pk.eyJ1IjoiZHh0cmlhbiIsImEiOiJja2Zta3hoN24wNmN4MnpwbHd0aHRvOWt3In0.XuP_qBknLJLeMMSjsL2Wsg';
        var map = new mapboxgl.Map({
            container: 'stasrt_plan_map{{$mapindex}}', 
            style: 'mapbox://styles/mapbox/satellite-streets-v11',
            center:intial_latlng,
            interactive: false
        }).on('load', function (e) {
    
            places = jQuery.parseJSON('{!! json_encode( $places,JSON_HEX_APOS) !!}');
            lat_lng= [];
            $.each(places,function(index,place){
                var el = $(document.createElement('div'));
                    var style = 'background: url(\' https://s3.amazonaws.com/travooo-images2/'+place.media+' \');' +
                        'background-size: cover;' +
                        'background-repeat: no-repeat;' +
                        'background-position: center center;' +
                        'width: 36px;' +
                        'height: 36px;' +
                        'border-radius: 50px;' +
                        'border: 2px solid #4080ff !important;' +
                        'box-shadow: inset 0 0 0 2px #fff;' +
                        'cursor: pointer;';
                $(el).attr('style', style);
                $(el).addClass('marker');
                lat_lng.push([place['lng'], place['lat']]);
                marker = new mapboxgl.Marker($(el)[0]).setLngLat([place['lng'], place['lat']]).addTo(e.target);
            });
                var geojson = {
                    'type': 'FeatureCollection',
                    'features': [
                        {
                            'type': 'Feature',
                            'geometry': {
                                'type': 'LineString',
                                'properties': {},
                                'coordinates':lat_lng
                            }
                        }
                    ]
                };
    
            this.addSource('LineString{{$mapindex}}', {
                'type': 'geojson',
                'data': geojson
                });
            this.addLayer({
                'id': 'LineString{{$mapindex}}',
                'type': 'line',
                'source': 'LineString{{$mapindex}}',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#5487E9',
                    'line-width': 5,
                }
            });
            var coordinates = geojson.features[0].geometry.coordinates;
    
            // Pass the first coordinates in the LineString to `lngLatBounds` &
            // wrap each coordinate pair in `extend` to include them in the bounds
            // result. A variation of this technique could be applied to zooming
            // to the bounds of multiple Points or Polygon geomteries - it just
            // requires wrapping all the coordinates with the extend method.
            var bounds = coordinates.reduce(function (bounds, coord) {
            return bounds.extend(coord);
            }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));
            
            this.fitBounds(bounds, {
            padding: 20
            });
        });;
        
        
    
</script>
@endif