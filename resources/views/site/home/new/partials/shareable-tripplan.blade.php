<style>
    .mapboxgl-ctrl mapboxgl-ctrl-attrib mapboxgl-compact {
        display: none;
    }
    .mapboxgl-ctrl-bottom-left,.mapboxgl-ctrl-top-right{
        display: none;
    }
    .mapboxgl-ctrl-bottom-right{
        display: none;
    }
</style>
<?php 
    $mapindex                   = rand(1, 5000);
    $variable                   = $post['variable'];
    $post_type                  = $post['type'];
    $trip_plan                  = \App\Models\TripPlans\TripPlans::find($post['variable']);
    $places                     = [];
    $countries                  = [];
    $cities                     = [];
    $total_destination          = 0;
    $trip_data                  = [];
    $intial_latlng              = [];
    $trip_card_type             = 'single-city';
    $_totalDestination          = ['city_wise' => [], 'country_wise' => []];
    $is_valid_trip              = true;
    $locationData               = [];
    $contributer                = [];
    $post_action                = \App\Models\ActivityLog\ActivityLog::where(['variable'=>$variable ,'type'=> $post_type ])->count() >= 2;

    if (!($trip_plan->published_places && count($trip_plan->published_places))) {
        $is_valid_trip = false;
    }
    
    if($is_valid_trip){
        if(isset($trip_plan->published_places) && count($trip_plan->published_places)>0){
            $intial_latlng =  [$trip_plan->published_places[0]->place->lng, $trip_plan->published_places[0]->place->lat];

            foreach ($trip_plan->published_places as $tripPlace) {
                if (!$tripPlace->place or !$tripPlace->country or !$tripPlace->city) {
                    continue;
                }
                //addslashes
                if (!isset($places[$tripPlace->places_id])) {
                    $total_destination++;
                    $_totalDestination['city_wise'][$tripPlace->cities_id][$tripPlace->places_id] = $tripPlace->place;
                    $_totalDestination['country_wise'][$tripPlace->countries_id][$tripPlace->places_id] = $tripPlace->place;
                    $places[$tripPlace->places_id] = [
                        'type' => 'place',
                        'id' => $tripPlace->places_id,
                        'country_id' => $tripPlace->countries_id,
                        'city_id' => $tripPlace->cities_id,
                        'title' => isset($tripPlace->place->transsingle->title) ? str_replace("'", '',$tripPlace->place->transsingle->title)  : null,
                        'lat' => $tripPlace->place->lat,
                        'lng' => $tripPlace->place->lng,
                        'image' => check_place_photo($tripPlace->place),
                    ];
                }
                if (!isset($countries[$tripPlace->countries_id])) {
                    $countries[$tripPlace->countries_id] = [
                        'type' => 'country',
                        'id' => $tripPlace->countries_id,
                        'title' => isset($tripPlace->country->trans[0]->title) ? str_replace("'", '',$tripPlace->country->trans[0]->title) : null,
                        'lat' => $tripPlace->country->lat,
                        'lng' => $tripPlace->country->lng,
                        'image' => get_country_flag($tripPlace->country),
                        'total_destination' => 0,
                    ];
                }
                if (!isset($cities[$tripPlace->cities_id])) {
                    $cities[$tripPlace->cities_id] = [
                        'type' => 'city',
                        'id' => $tripPlace->cities_id,
                        'country_id' => $tripPlace->countries_id,
                        'title' => isset($tripPlace->city->trans[0]->title) ? str_replace("'", '',$tripPlace->city->trans[0]->title) : null,
                        'lat' => $tripPlace->city->lat,
                        'lng' => $tripPlace->city->lng,
                        'image' => isset($tripPlace->city->medias[0]->media->path) ?
                            check_city_photo($tripPlace->city->medias[0]->media->path) :
                            url('assets2/image/placeholders/pattern.png'),
                        'total_destination' => 0,
                    ];
                }
            }

            foreach ($countries as $country_id => &$country) {
                $country['total_destination'] = isset($_totalDestination['country_wise'][$country_id]) ? count($_totalDestination['country_wise'][$country_id]) : 0;
            }
            foreach ($cities as $city_id =>  &$city) {
                $city['total_destination'] = isset($_totalDestination['city_wise'][$city_id]) ? count($_totalDestination['city_wise'][$city_id]) : 0;
            }

            $country_title =  isset(array_values($countries)[0]['title']) ? array_values($countries)[0]['title'] : null;
            if (count($countries) > 1) {
                $trip_card_type = "multiple-country";
                $locationData   = array_values($countries);
                $country_title  = null;
            } else {
                if (count($cities) > 1) {
                    $trip_card_type = "multiple-city";
                    $locationData   = array_values($cities);
                } else {
                    $trip_card_type = "single-city";
                    $locationData   = array_values($places);
                }
            }
    
            if(isset($auth_user)){
                $following = \App\Models\User\User::find($auth_user->id);
                $author = $auth_user;
            }else{
                $following = \App\Models\User\User::find(Auth::user()->id);
                $author = Auth::user();
            }
    
            $followers = [];
            foreach($following->get_followers as $f):
                $followers[] = $f->followers_id;
            endforeach;
        }
        if($trip_plan) $contributer = $trip_plan->contribution_requests->pluck('users_id')->toArray();
    
        if(isset($trip_plan->author->id)){
            if($author->id != $trip_plan->author->id ){
                if(in_array ($author->id ,$contributer)){
                    $author = $trip_plan->author;
                }else {
                    $following = \App\Models\UsersFollowers\UsersFollowers::where('followers_id',$author->id)->pluck('users_id')->toArray();
                    $ans=  array_values(array_intersect($following,$contributer));
                    if(count($ans)>0){
                        $user_find =\App\Models\User\User::find($ans[0]);
                        if(isset($user_find)){
                            $author = $user_find;
                        }
                    }else{
                        $author = $trip_plan->author;
                    }
                }
            }
        }

        $tripPlanDisplayTime = $trip_plan->created_at;
        if($post_action){
            $tripPlanDisplayTime = \App\Models\ActivityLog\ActivityLog::query()->where([
                'type' => 'Trip',
                'variable' => $trip_plan->id
            ])->whereIn('action', ['create', 'update','plan_updated'])->orderBy('time', 'DESC')->first()->time;
        }
    }

?>
@if($is_valid_trip && count($places)>0)
    <div class="post-block" id="trip_{{$trip_plan->id}}">
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap">
                    <a class="post-name-link" href="{{url('profile/'.$author->id)}}">
                        <img src="{{check_profile_picture($author->profile_picture)}}" alt="">
                    </a>
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link" href="{{url('profile/'.$author->id)}}">{{$author->name}}</a>{!! get_exp_icon($author->name) !!}
                    </div>
                    <div class="post-info">
                        {{-- 33333 || id : {{$trip_plan->id}} --}}
                        @if($trip_plan->memory !=0)
                            @if($post_action) Edited @else Posted a @endif <strong>Memory Trip</strong> 
                        @else 
                            @if($post_action) Edited @else Planning a @endif <strong>Upcoming Trip</strong>
                        @endif
                        {{ count($places) }} Destinations @if($country_title) in <strong>{{$country_title}} </strong> @else in <strong>{{ count($countries) }} countries</strong> @endif on {{diffForHumans($tripPlanDisplayTime)}}
                    </div>
                </div>
            </div>
            <div class="post-top-info-action" dir="auto">
                
            </div>
        </div>
        <div class="post-image-container">
            <!-- New element -->
            <div class="trip-plan-container">
                <div class="trip-plan-map post-modal" data-shared="{{isset($sharing_flag)}}" post-type="trip" data-id="{{@$trip_plan->id}}" data-shared_id="" style="cursor:pointer">
                    <img style='width:100%;height:400px;' src="{{tripPlanThumb($trip_plan, 615, 400)}}">
                    <a href="/trip/plan/{{$trip_plan->id}}"  class="btn btn-light-primary trip-plan-btn" style="color:white">View Plan</a>
                    {{-- <div class="forecast-map-marker map-popover" type="button" data-placement="top" data-toggle="popover"
                        data-html="true" data-original-title="" title="">
                        <i class="fas fa-map-marker-alt"></i>
                        <span class="count-label">3</span>
                    </div>
                    <div class="forecast-map-marker transfer" style="top: 20%; left: 80%;">
                        <i class="fas fa-plane"></i>
                    </div>
                    <div class="forecast-map-marker transfer" style="top: 40%; left: 60%;">
                        <i class="fas fa-bus"></i>
                    </div>
                    <div class="map-marker-img multiple" style="top: 20%; left: 30%;">
                        <span class="count-label">3</span>
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1597941078_image.png" alt="">
                    </div>
                    <div class="map-marker-img" style="top: 60%; left: 20%;">
                        <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1597941078_image.png" alt="">
                    </div> --}}
                </div>
                <div class="trip-plan-slider-container">
                    <a href="" class="trip-plan-home-slider{{$mapindex}}prev trip-plan-home-slider-prev"></a>
                    <a href="" class="trip-plan-home-slider{{$mapindex}}next trip-plan-home-slider-next"></a>
                    <div class="trip-plan-home-slider{{$mapindex}}" style="margin-left: 5px;">
                        @foreach ($locationData as $key=>$item)
                            @if($key==0)
                                <div class="trip-plan-home-slider-item active">
                                    <img src="{{ $item['image'] }}" alt="">
                                    <div class="title"> 
                                        {{$item['title']}} 
                                        @if ($item['type'] != 'place')
                                           <br> {{ $item['total_destination'] }}  Destination
                                        @endif
                                    </div>
                                </div>
                            @elseif($key == array_key_last($places))
                                <div class="trip-plan-home-slider-item ss" style="margin-right: 0px !important">
                                    <img src="{{ $item['image'] }}" alt="">
                                    <div class="title"> 
                                        {{$item['title']}} 
                                        @if ($item['type'] != 'place')
                                           <br> {{ $item['total_destination'] }}  Destination
                                        @endif
                                    </div>
                                </div>
                            @else 
                                <div class="trip-plan-home-slider-item">
                                    <img src="{{ $item['image'] }}" alt="">
                                    <div class="title"> 
                                        {{$item['title']}} 
                                        @if ($item['type'] != 'place')
                                           <br> {{ $item['total_destination'] }}  Destination
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- New element END -->
        </div>
        @php
            $flag_liked  =false;
            if(count($trip_plan->likes->where('places_id',null))>0){
                foreach($trip_plan->likes as $like){
                    if(Auth::check() && $like->users_id == Auth::user()->id)
                        $flag_liked = true;
                }
            }
        @endphp
        <!-- Removed comments block -->
    </div>
    @include('site.home.common_trip_script')
@endif