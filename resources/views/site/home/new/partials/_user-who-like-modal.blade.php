<div class="modal white-style users-who-like-modal" data-backdrop="false" id="usersWhoLike" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document" >
        <div class="modal-custom-block" >
            <div class="post-block post-travlog post-create-travlog" >
                <div class="top-title-layer" >
                    <h3 class="title" >
                        <span class="txt">
                            Poeple who liked this post <span class="total_count">0</span>
                        </span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon" ></i>
                    </button>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar" id="userWhoLikeContainer" >
                </div>
            </div>
        </div>
    </div>
</div>