<div class="modal opacity--wrap share-post-modal" data-backdrop="false" id="sendToFriendModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-670" role="document" >
        <div class='friend-share-modal'>
            <div class='modal-content'>
                <div class='friend-share-modal__header'>
                    <div>
                        <h2>Send to a Friend</h2>
                    </div>
                    <button class="modal--close" type="button" data-dismiss="modal" aria-label="Close" >
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="shared-post-comment">
                    <div class="shared-post-comment-details">
                        <div class="shared-post-comment-author">
                            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                            {{Auth::user()->name}}
                        </div>
                    </div>
                    <div class="shered-post-comment-text">
                        <textarea name="" id="message_popup_text" cols="30" rows="1" placeholder="Write something..."></textarea>
                    </div>     
                    <div class="module-colapse">
                        <div aria-expanded="false" class="shared-post-wrap collapse demo" id="message_popup_content">
                        
                        </div>
                        <a role="button" class='button--colapse collapsed' class="collapsed" data-toggle="collapse" data-target=".demo"
                        aria-expanded="false" aria-controls="collapseExample"></a>
                    </div>
                    <div class="friend-search">
                        <i class="trav-search-icon" ></i>
                        <input type="text" placeholder="Search in your friends list...">
                    </div>
                </div>
                <div class="friend-search-results">
                    @php
                        $friends_users = App\Models\User\User::whereIn('id',get_friendlist())->distinct()->get();

                    @endphp
                    @if(count($friends_users)>0)
                        @foreach ($friends_users as $f_user)
                            <div class="friend-search-results-item">
                                <img class="user-img" src="{{check_profile_picture($f_user->profile_picture)}}" alt="">
                                <div class="text">
                                    <div class="location-title">{{$f_user->name}}</div>
                                </div>
                                <button class="btn btn-light send_message" id="send_message" data-user="{{$f_user->id}}">Send</button>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>

    </div>
</div>