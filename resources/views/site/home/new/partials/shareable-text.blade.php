<?php
use App\Models\Posts\PostsViews;
$post_type = $post['type'];
$variable = $post['variable'];
$post = \App\Models\Posts\Posts::find($post['variable']);
$me = Auth::user();
$media_array = [];
if(isset($post->medias)){
    foreach($post->medias as $media){
        $media_array[] =$media->medias_id;
    }
    $media_array = App\Models\ActivityMedia\Media::whereIn('id',$media_array)->get();
}

$following = \App\Models\User\User::find($me->id);
$followers = [];
foreach($following->get_followers as $f):
    $followers[] = $f->followers_id;
endforeach;
?>
@if(is_object($post))
<?php
    $pv = new PostsViews;
    $pv->posts_id = $post->id;
    $pv->users_id = Auth::guard('user')->user()->id;
    $pv->gender = Auth::guard('user')->user()->gender;
    $pv->nationality = Auth::guard('user')->user()->nationality;
    $pv->save();
    ?>
@php
$today_flag = false;
$subtitle_checkin = '';
$info_title = '';
if(isset($post->checkin[0])){
if(strtotime($post->checkin[0]->checkin_time) == strtotime(date('Y-m-d'))){
$time_checking = date('H:i', strtotime($post->checkin[0]->checkin_time));
$today_flag = true;
$info_title = ' is checking-in '.@$post->checkin[0]->place->transsingle->title. '
'.diffForHumans($post->checkin[0]->checkin_time);
$subtitle_checkin = '<a class="post-name-link" href="'.url('profile/'.$post->author->id).'">'.@$post->author->name.
    '</a> is checking-in '.@$post->checkin[0]->place->transsingle->title. ' Today at '.$time_checking ;
}else if(strtotime($post->checkin[0]->checkin_time) > strtotime(date('Y-m-d'))){
$subtitle_checkin = ' will check-in to '.@$post->checkin[0]->place->transsingle->title. '
'.diffForHumans($post->checkin[0]->checkin_time) ;
$info_title = ' will check-in to '.@$post->checkin[0]->place->transsingle->title. '
'.diffForHumans($post->checkin[0]->checkin_time);

}else{
$info_title = ' Checked-in to '.@$post->checkin[0]->place->transsingle->title. '
'.diffForHumans($post->checkin[0]->checkin_time);
$subtitle_checkin = ' Checked-in to '.@$post->checkin[0]->place->transsingle->title. '
'.diffForHumans($post->checkin[0]->checkin_time) ;
}
//dateDiffernceInDays($post->checkin[0]);
}
$text = $post->text;
$taggidPlacesData = [];
$placeText = '/place/';
$placePos = strpos($text, $placeText);
while ($placePos != false) {
    $end = strpos($text, ' class', $placePos);
    $id = substr($text, $placePos+strlen($placeText), $end);
    $place = \App\Models\Place\Place::find($id);
    if (isset($place)) {
        $followersCount = count($place->followers);
        $taggidPlacesData[] = [
            'id' => $place->id,
            'title' => @$place->transsingle->title,
            'description' => !empty(@$place->transsingle->address) ? @$place->transsingle->address : '',
            'img' => check_country_photo(@$place->getMedias[0]->url, 180),
            'followers' => $followersCount > 1000 ? floor($followersCount/1000) . 'K' : $followersCount,
            'type' => 'place'
        ];
    }
        $placePos = strpos($text, $placeText, $placePos + 1);
}
$cityText = '/city/';
$cityPos = strpos($text, $cityText);
while ($cityPos != false) {
    $end = strpos($text, ' class', $cityPos);
    $id = substr($text, $cityPos+strlen($cityText), $end);
    $city = \App\Models\City\Cities::find($id);
    if (isset($city)) {
        $followersCount = count($city->followers);
        $taggidPlacesData[] = [
            'id' => $city->id,
            'title' => @$city->transsingle->title,
            'description' => !empty(@$city->transsingle->description) ? @$city->transsingle->description : '',
            'img' => check_country_photo(@$city->getMedias[0]->url, 180),
            'followers' => $followersCount > 1000 ? floor($followersCount/1000) . 'K' : $followersCount,
            'type' => 'city'
        ];
    }
    $cityPos = strpos($text, $cityText, $cityPos + 1);
}
$countryText = '/country/';
$countryPos = strpos($text, $countryText);
while ($countryPos != false) {
    $end = strpos($text, ' class', $countryPos);
    $id = substr($text, $countryPos+strlen($countryText), $end);
    $country = \App\Models\Country\Countries::find($id);
    if (isset($country)) {
        $followersCount = count($country->followers);
        $taggidPlacesData[] = [
            'id' => $country->id,
            'title' => @$country->transsingle->title,
            'description' => !empty(@$country->transsingle->description) ? @$country->transsingle->description : '',
            'img' => check_country_photo(@$country->getMedias[0]->url, 180),
            'followers' => $followersCount > 1000 ? floor($followersCount/1000) . 'K' : $followersCount,
            'type' => 'country'
        ];
    }
        $countryPos = strpos($text, $countryText, $countryPos + 1);
}
$taggidProfileData = [];
$tagProfileIDs = '';
$profileText = '/profile/';
$profilePos = strpos($text, $profileText);
while ($profilePos != false) {
    $end = strpos($text, ' class', $profilePos);
    $id = substr($text, $profilePos+strlen($profileText), $end);
    $profile = \App\Models\User\User::find($id);
    if (isset($profile)) {
        $taggidProfileData[] = [
            'id' => $profile->id,
            'cover_photo' => isset($profile->cover_photo) ? $profile->cover_photo : asset('assets2/image/preloader.gif'),
            'profile_picture' => !empty($profile->profile_picture) ? check_profile_picture($profile->profile_picture) : asset('assets2/image/placeholders/male.png'),
            'name' => $profile->name,
            'nationality' => !empty($profile->nationality) ? @$profile->country_of_nationality->transsingle->title : '',
            'flag' => !empty(@$profile->country_of_nationality->iso_code) ? 'https://ipdata.co/flags/'.strtolower(@$profile->country_of_nationality->iso_code).'.png' : '',
            'type' => 'profile'
        ];
        $tagProfileIDs .= $profile->id.',';
    }
        $profilePos = strpos($text, $profileText, $profilePos + 1);
}
@endphp
<div class="post-block {{($today_flag == true)?"post-block-notification":"" }}"  id="post_{{$post->id}}">


    @php
        $uniqueurl = url('post/' . $post->author->username, $post->id);
    @endphp

    @if(isset($post->checkin[0]) && $today_flag)
        <div class="post-top-info-layer">
            <div class="post-info-line">
                {!! $subtitle_checkin !!}
            </div>
        </div>
    @endif
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <a class="post-name-link" href="{{url('profile/'.$post->author->id)}}">
                    <img src="{{check_profile_picture($post->author->profile_picture)}}" alt="">
                </a>
                
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{url('profile/'.$post->author->id)}}">{{$post->author->name}}</a>
                    @if($post->author)
                        {!! get_exp_icon($post->author) !!}
                    @endif
                </div>
                <div class="post-info">
                    @if(isset($post->checkin[0]))
                        {!! $info_title !!}
                    @else
                        <a href="{{$uniqueurl}}" target="_blank" class="post-title-link">
                        @if(count($media_array) == 1)
                            @php
                                $post_object = $media_array[0];
                                $file_url = $post_object->url;
                                $file_url_array = explode(".", $file_url);
                                $ext = end($file_url_array);
                                $allowed_video = array('mp4');
                            @endphp
                            @if(in_array($ext, $allowed_video))
                                Uploaded a <strong>video</strong> {{diffForHumans($post->created_at)}}
                            @else 
                                Uploaded a <strong>photo</strong> {{diffForHumans($post->created_at)}}
                            @endif
                        @elseif(count($media_array) >1)
                            Uploaded <strong> media </strong> {{diffForHumans($post->created_at)}}
                        @else
                            {{diffForHumans($post->created_at)}}
                        @endif
                        </a>
                    @endif
                    @php
                        $admin_post_flag = false;
                        if($post->author->id == Auth::user()->id)
                            $admin_post_flag=true;
                    @endphp
                    @if($post->permission == 1)
                        <i class="trav-users-icon"></i>
                    @elseif($post->permission == 2)
                        <i class="trav-lock-icon"></i>
                    @else
                        <i class="trav-globe-icon"></i>
                    @endif
                        
                   
                </div>
            </div>
        </div>
        
    </div>

    <div class="post-txt-wrap post-modal" data-shared="{{isset($sharing_flag)}}" post-type="{{$post_type}}" data-id="{{@$shared_variable ?? @$post->id}}" data-shared_id="{{@$shared_variable ? $post->id : '' }}" style="cursor:pointer">
        <div>
            <span class="less-content disc-ml-content">{!! substr($post->text,0, 2500) !!}
                @if(strlen($post->text)>2500)
                    <span class="moreellipses">...</span>
                    <a href="javascript:;" class="read-more-link">More</a>
                @endif
            <span>
            <span class="more-content disc-ml-content" style="display: none;">{!! $post->text !!} <a href="javascript:;"  class="read-less-link">Less</a></span>
        </div>
    </div>
    {{-- checking --}}
    @if(false)
        <div class="check-in-point">
            <i class="trav-set-location-icon"></i> <strong>{{@$post->checkin[0]->place->transsingle->title}}</strong>
            @if(isset($post->checkin[0]->city->transsingle->title))
                , {{$post->checkin[0]->city->transsingle->title}}, {{$post->checkin[0]->country->transsingle->title}}
            @endif
            <i class="trav-clock-icon"></i>{{diffForHumans($post->checkin[0]->checkin_time)}}
        </div>
    @endif
    <!-- Post text tag tooltip content -->
    <div id="popover-content" style="display: none;">
        <div class="user-hero">
            <img src="https://s3.amazonaws.com/travooo-images2/users/profile/831/1597941078_image.png" alt="">
            <div class="user-hero-text">
                <div class="name">Sue Perez</div>
                <div class="location">
                    <i class="trav-set-location-icon"></i>
                    United States
                </div>
            </div>
            <button type="button" class="btn btn-light-primary btn-bordered">Follow </button>
        </div>
        <div class="user-badges">
            <div class="badges-ttl">Badges</div>
            <div class="badges-list">
                <img src="https://travooo.com/assets2/image/badges/1_1.png" alt="Socializer">
                <img src="https://travooo.com/assets2/image/badges/5_1.png" alt="Personality">
                <img src="https://travooo.com/assets2/image/badges/1_1.png" alt="Socializer">
                <img class="fade" src="https://travooo.com/assets2/image/badges/5_1.png" alt="Personality">
                <img class="fade" src="https://travooo.com/assets2/image/badges/1_1.png" alt="Socializer">
            </div>
            <button type="button" class="btn btn-light-primary btn-bordered">Follow </button>
        </div>
        <div class="actions">
            <a class="btn btn-light-grey">Follow</a>
            <a class="btn btn-light-grey popover-msg-btn">Message</a>
        </div>
    </div>
     <!-- Post text tag tooltip content END -->
    @php 
    $post_counter= 0;
    @endphp
    <div class="post-image-container post-modal" data-shared="{{isset($sharing_flag)}}" post-type="{{$post_type}}" data-id="{{@$shared_variable ?? @$post->id}}" data-shared_id="{{@$shared_variable ? $post->id : '' }}" style="cursor:pointer">
        @if(count($media_array) == 1)
            @php 
                $post_object = $media_array[0];
                $file_url = $post_object->url;
                $file_url_array = explode(".", $file_url);
                $ext = end($file_url_array);
                $allowed_video = array('mp4');
            @endphp
            <!-- New element -->
            <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;">
                @if(in_array($ext, $allowed_video))
                    <li style="padding: 0;position: relative;margin:0 0 0 0;overflow: hidden;">
                        <video width="100%" height="auto" class="fullwidth" id="{{'video_tmp'.$post_object->id}}" data-play={{"play_".$post_object->id}}>
                            <source src="{{$post_object->url}}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        <a href="javascript:;" class="v-play-btn play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                    </li>
                @else
                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;">
                        {{-- <a href="{{$post_object->url}}" data-lightbox="media__post210172"> --}}
                            <img src="{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                        {{-- </a> --}}
                    </li>
                @endif
            </ul>

        @elseif(count($media_array) == 2)
            <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;">
                @foreach ($media_array as $post_object)
                    @php
                        $file_url = $post_object->url;
                        $file_url_array = explode(".", $file_url);
                        $ext = end($file_url_array);
                        $allowed_video = array('mp4');
                    @endphp
                    @if(in_array($ext, $allowed_video))
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                            <video width="100%" id="{{'video_tmp'.$post_object->id}}" data-play={{"play_".$post_object->id}}>
                                <source src="{{$post_object->url}}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            <a href="javascript:;" class="v-play-btn "><i class="fa fa-play" aria-hidden="true"></i></a>
                        </li>
                    @else
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                            {{-- <a href="{{$post_object->url}}" data-lightbox="media__post199366"> --}}
                                <img src="{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                            {{-- </a> --}}
                        </li>
                    @endif
                @endforeach
                
                
            </ul>
        @elseif(count($media_array)  >2)
        <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;">
            @foreach ($media_array as $post_object)
                @php
                    $file_url = $post_object->url;
                    $file_url_array = explode(".", $file_url);
                    $ext = end($file_url_array);
                    $allowed_video = array('mp4');
                    $post_counter++;
                @endphp
                <!-- New element -->
                    @if(in_array($ext, $allowed_video))
                        @if( $post_counter  <=3)
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                <video width="100%" id="{{'video_tmp'.$post_object->id}}" data-play={{"play_".$post_object->id}}>
                                    <source src="{{$post_object->url}}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                                <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                            </li>
                        @endif
                    @else
                        @if( $post_counter  <=3)
                                <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                    @if(count($media_array)>3 &&  $post_counter ==3)
                                        <a class="more-photos" href="" data-lightbox="media__post199366">{{count($media_array) -3 . ' More Photos'}}</a>
                                    @endif
                                        {{-- <a  href="{{$post_object->url}}" data-lightbox="media__post199366"> --}}
                                            <img src="{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                                        {{-- </a> --}}
                                </li>
                        @endif
                    @endif
                <!-- New element END -->
            @endforeach
        </ul>
        @endif
    </div>
    @if (false)
        <div class="shared-place-slider-wrap">
            <div class="lSSlideOuter ">
                <div class="lSSlideWrapper usingCss">
                    <div class="shared-place-slider lightSlider lsGrab lSSlide" style="width: 1716px; height: 390px; padding-bottom: 0%; transform: translate3d(0px, 0px, 0px);">
                    @foreach($taggidPlacesData as $place)
                    <div class="shared-place-slider-card lslide active" style="margin-right: 22px;">
                        <img src="{{$place['img']}}" alt="">
                        <div class="shared-place-slider-card-description">
                            <div class="details">
                                <img src="{{$place['img']}}" alt="">
                                <div class="text">
                                    <h3 style="margin-top: 0;">{{$place['title']}}</h3>
                                    <p>{!!$place['description']!!}</p>
                                    <div class="users">
                                        <button class="post__comment-btn">
                                        {{--        <div class="user-list">--}}
                                        {{--        <div class="user-list__item">--}}
                                        {{--        <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/579/1593878106_image.png" alt="Josh Harper" title="Josh Harper" role="presentation"></div>--}}
                                        {{--        <div class="user-list__user"><img class="user-list__avatar" src="https://s3.amazonaws.com/travooo-images2/users/profile/559/1593873048_image.png" alt="Kobayashi Seiesnsui" title="Kobayashi Seiesnsui" role="presentation"></div>--}}
                                        {{--        </div>--}}
                                        {{--         </div>--}}
                                            <i class="trav-comment-icon icon"></i>
                                            <span><strong>{{$place['followers']}}</strong> Talking about this</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="actions">
                                <button class="btn btn-light-primary orange">Book</button>
                                <div class="follow-btn-wrap check-follow-{{$place['type']}}" data-id="{{$place['id']}}">
                                    <button class="btn btn-light-primary" data-id="{{$place['id']}}" id="button_follow_{{$place['type']}}">Follow</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    @php
        $flag_liked  =false;
            if(count($post->likes)>0){
                foreach($post->likes as $like){
                    if($like->users_id == Auth::user()->id)
                        $flag_liked = true;
                }
            }
        @endphp

</div>
    @if (!empty($taggidProfileData))
    @foreach($taggidProfileData as $profile)
        <div id="popover-content-{{$profile['id']}}" style="display: none;">
            <div class="user-hero">
                <img src="{{$profile['profile_picture']}}" alt="">
                <div class="user-hero-text">
                    <div class="name">{{$profile['name']}}</div>
                    <div class="location">
                        <img src="{{$profile['flag']}}" alt="" class="country-flag">
                        {{$profile['nationality']}}
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    @endif
@endif
<script>
    var _variable= '{{$variable}}';
    $(document).ready(function(){
        // Post text tag popover
        var tagProfileIDs = '{{isset($tagProfileIDs) ? $tagProfileIDs : ""}}';
        var profileids = tagProfileIDs.split(',');
        if (profileids.length > 0) {
            $.each(profileids, function (i, v) {
                $(`.post-text-tag-${v}`).popover({
                    html: true,
                    content: $(`#popover-content-${v}`).html(),
                    template: '<div class="popover bottom tagging-popover" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
            })
        }
        $(document).on('click', ".read-more-link", function () {
            $(this).closest('.post-txt-wrap').find('.less-content').hide()
            $(this).closest('.post-txt-wrap').find('.more-content').show()
            $(this).hide()
        });
        $(document).on('click', ".read-less-link", function () {
            $(this).closest('.more-content').hide()
            $(this).closest('.post-txt-wrap').find('.less-content').show()
            $(this).closest('.post-txt-wrap').find('.read-more-link').show()
        });

        // Post type - shared place, slider
        $('.shared-place-slider').lightSlider({
            autoWidth: true,
            slideMargin: 22,
            pager: false,
            controls: false,
        });
        // Video
        $('.v-play-btn, .video-play-btn').click(function(){
            $(this).siblings('video').attr('controls', true);
            $(this).siblings('video').get(0).play();
            $(this).toggleClass('hide');
        });
        $('.post-image-container video, .post-block-trending-videos video').on('playing', function () {
            $(this).siblings('.v-play-btn, .video-play-btn').addClass('hide')
        });
        $('.post-image-container video, .post-block-trending-videos video').on('pause', function () {
            $(this).siblings('.v-play-btn, .video-play-btn').removeClass('hide')
        });
        // Trim more text
        if($('.less-content .trim')) {
            let content = $('.less-content .trim');
            let trim = function(content) {
                content.html(content.text().trim())
            }
            trim(content);
        }

        @if(count($media_array) == 1)
            @php 
                $post_object = $media_array[0];
                $file_url = $post_object->url;
                $file_url_array = explode(".", $file_url);
                $ext = end($file_url_array);
                $allowed_video = array('mp4');
            @endphp
            @if(in_array($ext, $allowed_video))
                $(".post-image-container").on('inview', '{{'video[data-play=play_'.$post_object->id.']'}}', function(event, isInView) {
                    if(isInView) {
                        console.log(event)
                        event.currentTarget.muted = true
                        event.target.controls = true // enable video controls
                        event.currentTarget.play()
                    } else {
                        // event.currentTarget.currentTime = 0
                        event.currentTarget.pause()
                    }
                    $(event.currentTarget).off('inview');
                })
            @endif
        @endif
    })
</script>
