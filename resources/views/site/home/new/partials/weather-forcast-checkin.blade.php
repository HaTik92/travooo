@if(isset($weather['weather']) && isset($weather['place']) && isset($weather['checkin']))
    <div class="post-block post-block-notification" >
        <div class="post-top-info-layer">
            <div class="post-info-line">
                <i class="fas fa-cloud-sun" rel="tooltip" data-toggle="tooltip" data-animation="false" title=""  data-original-title="The total number of views your posts have garnered"></i>
                Weather forcast about <a  onclick="return false" class="link-place primary"><i class="trav-link-out-icon"></i> your visit</a> to <a href="place/{{@$weather['place']->id}}" class="link-place primary">{{@$weather['place']->transsingle->title}}</a>  {{diffForHumans(@$weather['checkin']->checkin_time)}}
            </div>
        </div>
        <div class="post-weather-inner">
            <div class="top-line">
                <i class="far fa-lightbulb"></i>it will be  {{@$weather['weather'][0]->Day->IconPhrase}} in <a href="place/{{@$weather['place']->id}}">{{@$weather['place']->transsingle->title}}</a> 
            </div>
            <div class="forecast-box">
                <div class="forecast-map">
                    <img src="https://api.mapbox.com/styles/v1/mapbox/light-v10/static/{{@$weather['place']->lng}},{{@$weather['place']->lat}},10/500x300?access_token=pk.eyJ1IjoiZHh0cmlhbiIsImEiOiJja2Zta3hoN24wNmN4MnpwbHd0aHRvOWt3In0.XuP_qBknLJLeMMSjsL2Wsg" alt="Map of the Edmund Pettus Bridge in Selma, Alabama.">
                    {{-- <img src="https://api.mapbox.com/styles/v1/mapbox/light-v10/static/{{@$weather['place']->lng}},{{@$weather['place']->lat}}/500x300?access_token=pk.eyJ1IjoiZHh0cmlhbiIsImEiOiJja2Zta3hoN24wNmN4MnpwbHd0aHRvOWt3In0.XuP_qBknLJLeMMSjsL2Wsg" alt="{{@$weather['place']->transingle->title}}."> --}}
                </div>
                <div class="forecast-table">
                    @php
                        $counter =0;
                    @endphp
                    @foreach (@$weather['weather'] as $item)
                        @php
                            $current = strtotime(date("Y-m-d"));
                            $date    = $item->EpochDate;
                            $datediff = $date - $current;
                            $difference = floor($datediff/(60*60*24));
                        @endphp
                        @if($difference>=0 && $counter<5)
                            <div class="forecast-table-day @if($counter == 0) visit-day @endif">
                                <div>
                                    <div class="temp">{{@$item->Temperature->Minimum->Value}} C<sup>&deg;</sup></div>
                                    <div class="day">{{date('l',$date)}} {{date('d F',$date)}}</div>
                                </div>
                                <i class="fas fa-cloud-sun"></i>
                            </div>
                            @php
                                $counter++;
                            @endphp
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif