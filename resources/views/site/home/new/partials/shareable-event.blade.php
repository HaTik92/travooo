<?php

$evt = $post['variable'];
$evt = App\Models\Events\Events::find($evt );
$me = Auth::user();
$post_type = 'Event';
$rand = rand(1, 10);
$loc = $evt->place ? $evt->place:
    ($evt->city ? $evt->city:
        ($evt->country ? $evt->country : []));
$post = $evt; 
$variable = $post->id;
$json= json_decode($post->variable);
$followers = [];
if(Auth::check()){
    $following = \App\Models\User\User::find(Auth::user()->id);
    foreach($following->get_followers as $f){
        $followers[] = $f->followers_id;
    }
	$username = $me->username;
}else{
	$username = 'event';
}
?>

<div class="post-block post-block-notification" id="event_{{$post->id}}">

    <div class="post-top-info-layer">
        <div class="post-info-line">
            {{-- <a class="post-name-link" href="{{url('newsfeed/'. $username .'/event', $post->id)}}" target="_blank"> Event in {{ @$loc->trans[0]->title }}</a> --}}
           <a class="post-name-link" href="" target="_blank"> Event in {{ @$loc->trans[0]->title }}</a>
        </div>
        <div class="post-top-info-action" dir="auto">
            <div class="dropdown" dir="auto">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"      aria-expanded="false" dir="auto">
                    <i class="trav-angle-down" dir="auto"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Share</b></p>
                            <p dir="auto">Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item copy-newsfeed-link" data-text="" data-link="#">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-link" dir="auto"></i>
                        </span>
                        <div class="drop-txt">
                            <p dir="auto"><b dir="auto">Copy Link</b></p>
                            <p dir="auto">Paste the link anywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#sendToFriendModal" data-id="event_{{$post->id}}" data-toggle="modal" data-target="" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-on-travo-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                            <p dir="auto">Share with your friends</p>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#spamReportDlgNew" data-toggle="modal" data-target="#" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-flag-icon-o" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Report</b></p>
                            <p dir="auto">Help us understand</p>
                        </div>
                    </a>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="post-event-hero">
        {{-- <img class="post-event-hero-img" src="{{asset('assets2/image/events/'.$evt->category.'/'.$rand.'.jpg')}}" alt=""> --}}
        <img class="post-event-hero-img" src="{{$json->cover_image_url}}" alt="">
        <div class="post-event-hero-caption">
            <div class="event-theme-badge">{{$evt->category}}</div>
            <h3 class="event-title" style="color: white;">
                {{-- <span>{{date('j', strtotime($evt->end)) - date('j', strtotime($evt->start))}} days event, <strong>{{date('j M', strtotime($evt->start))}} - {{date('j M', strtotime($evt->end))}}</strong></span> --}}
                {{$json->title}}
            </h3>
        </div>
    </div>
    <div class="post-event-details">
        <img src="https://travooo.com/assets2/image/placeholders/place.png" alt="" class="post-event-details-map">
        <div class="post-event-details-text">
            <p><i class="trav-set-location-icon"></i> <strong>{{ @$loc->trans[0]->title }}</strong></p>
            <p><i class="trav-star-icon"></i> <strong> {{$evt->reviews }}</strong> Reviews</p>
        </div>
        @if($json->url !='')
            <div class="actions" style="margin-left: 180px;">            
                <a href="{{$json->url}}?aid=Travooo" target="_blank" style="    background: rgb(255, 133, 33); border-color: rgb(255, 133, 33);" class="btn btn-light-primary orange">BOOK</a>
            </div>
        @endif
    </div>
    @php
    $is_event = true;
    $flag_liked  =false;
        if(count($post->likes)>0){
            foreach($post->likes as $like){
                if(Auth::check() && $like->users_id == Auth::user()->id)
                    $flag_liked = true;
            }
        }
    @endphp
