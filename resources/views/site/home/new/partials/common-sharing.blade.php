@php
use App\Models\ActivityLog\ActivityLog;

    $post = $data['log'];
    $original_content = $data['share'];  
    $sharing_users_list  = $data['other']->pluck('users_id')->toArray();
    $sharing_all_posts_list = $data['other']->pluck('id')->toArray();
    $sharing_all_posts_list = ActivityLog::where(['type'=>'Share','action'=>$post->action])->whereIn('variable',$sharing_all_posts_list)->pluck('id')->toArray();
    $sharing_users_list [] = $original_content->users_id;
    $additional = [];
    if (session('loaded_post_ids')) {
        $additional = session('loaded_post_ids');
    }
    $next_additional = array_merge($sharing_all_posts_list, $additional);
    if (session('loaded_post_ids')) {
            session()->forget(['loaded_post_ids']);
    }
    $value = session(['loaded_post_ids' =>  $next_additional]);
@endphp
@if($post->action == 'review')
@php
 $original_post = $original_content->posts_type_id;
$original_post = ActivityLog::where(['type'=>'Place','action'=>'review','variable'=>$original_post])->first();
@endphp
@if(is_object( $original_post))
    @include('site.home.new.partials.place-review', array('post' => $original_post,'shareable_users'=>$sharing_users_list,'shareable_flag'=>true))
@endif

@elseif($post->action == 'post')

    @php 
    $original_post = $original_content->posts_type_id;

    $original_post = ActivityLog::where(['type'=>'Post','action'=>'publish','variable'=>$original_post])->first();
    @endphp
    @if(is_object( $original_post))
        {{-- @include('site.home.new.partials.primary-text-only-post', array('post' => $original_post, 'me' => Auth::guard('user')->user(),'shareable_users'=>$sharing_users_list,'shareable_flag'=>true)) --}}
    @endif
@elseif($post->action == 'trip')
@php 
$original_post = $original_content->posts_type_id;

$original_post = ActivityLog::where(['type'=>'Trip','action'=>'create','variable'=>$original_post])->first();
@endphp
@if(is_object( $original_post))
    @include('site.home.new.partials.trip', array('post' => $original_post, 'me' => Auth::guard('user')->user(),'shareable_users'=>$sharing_users_list,'shareable_flag'=>true))
@endif
@elseif($post->action == 'discussion')
    @php 
        $original_post = $original_content->posts_type_id;

        $original_post = ActivityLog::where(['type'=>'discussion','action'=>'create','variable'=>$original_post])->first();
    @endphp
     @include('site.home.new.partials.discussion', array('post' => $original_post, 'me' => Auth::guard('user')->user(),'shareable_users'=>$sharing_users_list,'shareable_flag'=>true))

@elseif($post->action == 'report')
    @php 
        $original_post = $original_content->posts_type_id;

        $original_post = ActivityLog::where(['type'=>'Report','action'=>'create','variable'=>$original_post])->first();
    @endphp
    @include('site.home.new.partials.travlog', array('post' => $original_post, 'me' => Auth::guard('user')->user(),'shareable_users'=>$sharing_users_list,'shareable_flag'=>true))

@endif
