<div class="modal opacity--wrap spam-report-modal" data-backdrop="false"  id="spamReportDlgNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style" role="document">
        <div class="post-block">
            <div class="share-modal-header">
                <div>
                    <h4>Report Post</h4>
                </div>
                <button type="button" data-dismiss="modal" aria-label="Close">
                    <i class="trav-close-icon" dir="auto"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-check mb-2">
                        <i class="fa fa-exclamation-circle report-exclamation" aria-hidden="true"></i>
                        <h5 class="d-inline-block">What is the problem with this post?</h5>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="container">
                                    <div class="radio-title-group">
                                        <div class="input-container">
                                            <input id="spam" class="radio-button" type="radio" name="spam_post_type" value="0" checked="checked">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Spam
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="fake-news" class="radio-button" type="radio" name="spam_post_type" value="2">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Fake News
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="harassment" class="radio-button" type="radio" name="spam_post_type" value="3">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Harassment
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="hate-speech" class="radio-button" type="radio" name="spam_post_type" value="4">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Hate Speech
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="nudity" class="radio-button" type="radio" name="spam_post_type" value="5">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Nudity
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="terrorism" class="radio-button" type="radio" name="spam_post_type" value="6">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Terrorism
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="violence" class="radio-button" type="radio" name="spam_post_type" value="7">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Violence
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-container">
                                            <input id="other" class="radio-button" type="radio" name="spam_post_type" value="1">
                                            <div class="radio-title">
                                                <div class="icon fly-icon">
                                                    Other
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control report-span-input" id="spamText" placeholder="Something Else" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="share-footer-modal modal-footer">
                <div  class="share-footer-modal__icons">
                </div>
                <button style="display: block;" class="btn place_cancel_btn share-modal__close" type="button">Cancel</button>
                <button class="btn" type="submit" id="spamSend_new">Send</button>
            </div>
            <input type="hidden" name="dataid" id="dataid">
            <input type="hidden" name="posttype" id="posttype">
        </div>
    </div>
</div>
