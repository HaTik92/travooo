<?php
$post_type = $post['type'];
$discussion = \App\Models\Discussion\Discussion::find($post['variable']);
$flag = false;
if(isset($discussion->experts) && count($discussion->experts)>0){
    foreach ($discussion->experts as $key => $value) {
    if(Auth::check() && $value->expert_id == Auth::user()->id)
            $flag = true;
    }
}
$followers = [];
if(Auth::check()){
    $following = \App\Models\User\User::find(Auth::user()->id);
    foreach($following->get_followers as $f):
        $followers[] = $f->followers_id;
    endforeach;
}
?>

@if(isset($discussion) AND is_object($discussion))
    @php
        $uniqueurl = url('discussion/'. $discussion->author->username, $discussion->id);
    @endphp
    <div class="post-block discussion @if($flag)post-block-notification @endif" id="discussion_{{$discussion->id}}">
        @if($flag)
        <div class="post-top-info-layer">
            <div class="post-info-line">
                <i class="fa fa-question-circle" rel="tooltip" data-toggle="tooltip" data-animation="false" title="" data-original-title="The total number of views your posts have garnered"></i> <strong>Question</strong>
                for you
            </div>
        </div>
        @endif
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap ava-50">
                    <img src="{{check_profile_picture($discussion->author->profile_picture)}}" alt="">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link" href="{{ url_with_locale('profile/'.$discussion->author->id)}}">{{$discussion->author->name}}</a>{!! get_exp_icon($discussion->author) !!}
                    </div>
                    <div class="post-info">
                        <a href="{{$uniqueurl}}" target="_blank" class="post-title-link">Asked for <strong>tips</strong> about
                        <a href="{{url_with_locale(strtolower($discussion->destination_type)."/".$discussion->destination_id)}}" class="link-place">{{getDiscussionDestination($discussion->destination_type, $discussion->destination_id)}}</a>
                        on {{diffForHumans($discussion->created_at)}}
                        </a>
                    </div>
                </div>
            </div>
            <div class="post-top-info-action" dir="auto">
              
            </div>
        </div>
        <div class="topic-inside-panel-wrap discussion-modal" post-type="{{$post_type}}" data-uname="{{$discussion->author->username}}" data-dis_id="{{$discussion->id}}" data-tab={{'comments'.$discussion->id}} style="cursor: pointer;">
            <div class="topic-inside-panel">
                <div class="panel-txt">
                    <h3 class="panel-ttl">  
                        <a style="text-decoration: none;color:black" href="#" post-type="{{$post_type}}" data-uname="{{$discussion->author->username}}" data-dis_id="{{$discussion->id}}" data-tab={{'comments'.$discussion->id}}>{!! str_limit($discussion->question, 25, ' ...') !!}</a>
                    </h3>
                    <div class="post-txt-wrap">
                        <div>
                            <span class="less-content disc-ml-content">{!! str_limit($discussion->description, 85, '') !!}<span
                                    class="moreellipses">...</span> 
                                    @if(strlen($discussion->description)>100)<a href="javascript:;" class="read-more-link">More</a>@endif</span>
                            <span class="more-content disc-ml-content" style="display: none;">{{$discussion->description}} <a href="javascript:;"
                                    class="read-less-link">Less</a></span>
                        </div>
                    </div>
                </div>
                <div class="panel-img disc-panel-img">
                    @if(count(@$discussion->media)>0)
                    <img src="{{@$discussion->media[0]->media_url}}" alt="" style="width: 130px; height: 140px;">
                    @endif
                </div>
            </div>
        </div>
       
    </div>
@endif
<script>
    $(document).ready(function (){
        $(document).on('click', ".read-more-link", function () {
            $(this).closest('.post-txt-wrap').find('.less-content').hide()
            $(this).closest('.post-txt-wrap').find('.more-content').show()
            $(this).hide()
        });
        $(document).on('click', ".read-less-link", function () {
            $(this).closest('.more-content').hide()
            $(this).closest('.post-txt-wrap').find('.less-content').show()
            $(this).closest('.post-txt-wrap').find('.read-more-link').show()
        });

    });
    </script>
