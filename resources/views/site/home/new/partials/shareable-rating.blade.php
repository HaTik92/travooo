<?php
$review = \App\Models\Reviews\Reviews::find($post['variable']);
$avg = 0;
$total = 0;
// dd($review->author);
// dd($review->place);
if(isset($review->place) && isset($review->places_id)){
    $avg = \App\Models\Reviews\Reviews::where('places_id',$review->places_id)->avg('score');
    $total = $review->place->reviews()->count();
}
?>
@if(is_object($review) && $review->author && isset($review->places_id))
    <div class="post-block post-block-review @if(isset($sharedStr) && $sharedStr !='') post-block-notification @endif" >
        @if(isset($sharedStr) && $sharedStr !='')
        {!! $sharedStr!!}
        @endif
        <div class="post-top-info-layer" >
            <div class="post-top-info-wrap" >
                <div class="post-top-avatar-wrap" >
                    <img src="{{check_profile_picture($review->author->profile_picture)}}" alt="" >
                </div>
                <div class="post-top-info-txt" >
                    <div class="post-top-name" >
                        <a class="post-name-link" href="{{url('profile/'.$review->author->id)}}" >{{$review->author->name}}</a>{!! get_exp_icon($review->author) !!}
                    </div>
                    <div class="post-info" >
                        Reviewed <a href="{{ route('place.index', @$review->place->id) }}" class="link-place" >{{@$review->place->transsingle->title}}</a> <span class="review-rating">{{@ (int) $review->score}}  <i class="trav-star-icon" ></i></span>  {{diffForHumans($review->created_at)}}
                    </div>
                </div>
            </div>
        </div>
        <div class="post-txt-wrap">
            <div>
                <span class="less-content disc-ml-content">{{substr($review->text,0, 200) }}
                    @if(strlen($review->text)>200)
                        <span class="moreellipses">...</span>
                        <a href="javascript:;" class="read-more-link">More</a>
                    @endif
            </span>
                <span class="more-content disc-ml-content" style="display: none;">{{ $review->text }}<a href="javascript:;" class="read-less-link">Less</a></span>
         
                </div>
        </div>
        @php 
        $media_array = $review->medias;
        // dd($media_array);
        $post_counter= 0;
       @endphp
        <div class="post-image-container post-follow-container wide" style="border-bottom: none;">
            @if(count($media_array) == 0) 
                <ul class="post-image-list" style="margin:0px 0px 1px 0px;" >
                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;" >
                        <a href="https://s3.amazonaws.com/travooo-images2/{{@$review->place->medias[0]->url}}" data-lightbox="media__post210172" >
                            <img src="https://s3.amazonaws.com/travooo-images2/{{@$review->place->medias[0]->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);" >
                        </a>
                    </li>
                </ul>
            @elseif(count($media_array) == 1)
                @php 
                    $post_object = $media_array[0];
                    $file_url = $post_object->url;
                    $file_url_array = explode(".", $file_url);
                    $ext = end($file_url_array);
                    $allowed_video = array('mp4');
                @endphp
                <!-- New element -->
                <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                    @if(in_array($ext, $allowed_video))
                        <li style="padding: 0;position: relative;margin:0 0 0 0;overflow: hidden;">
                            <video width="100%" height="auto" controls="">
                                <source src="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            <a href="javascript:;" class="v-play-btn play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </li>
                    @else
                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;">
                            <a href="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" data-lightbox="media__post210172">
                                <img src="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                            </a>
                        </li>
                    @endif
                </ul>

            @elseif(count($media_array) == 2)
                <ul class="post-image-list" style="margin:0px 0px 1px 0px;">
                    @foreach ($media_array as $post_object)
                        @php
                            $file_url = $post_object->url;
                            $file_url_array = explode(".", $file_url);
                            $ext = end($file_url_array);
                            $allowed_video = array('mp4');
                        @endphp
                        @if(in_array($ext, $allowed_video))
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                <video width="100%" height="auto" >
                                    <source src="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                                <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                            </li>
                        @else
                            <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                <a href="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" data-lightbox="media__post199366">
                                    <img src="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                                </a>
                            </li>
                        @endif
                    @endforeach
                   
                    
                </ul>
            @elseif(count($media_array)  >2)
                <ul class="post-image-list rounded" style="margin:0px 0px 1px 0px;">
                    @foreach ($media_array as $post_object)
                        @php
                            $file_url = $post_object->url;
                            $file_url_array = explode(".", $file_url);
                            $ext = end($file_url_array);
                            $allowed_video = array('mp4');
                            $post_counter++;
                        @endphp
                        <!-- New element -->
                            @if(in_array($ext, $allowed_video))
                                @if( $post_counter  <=3)
                                    <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                        <video width="100%" >
                                            <source src="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                                    </li>
                                @endif
                            @else
                                @if( $post_counter  <=3)
                                        <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;  height:200px;  padding:0;">
                                            @if(count($media_array)>3 &&  $post_counter ==3)
                                                <a class="more-photos" href="" data-lightbox="media__post199366">{{count($media_array) -3 . ' More Photos'}}</a>
                                            @endif
                                                <a  href="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" data-lightbox="media__post199366">
                                                    <img src="https://s3.amazonaws.com/travooo-images2/{{$post_object->url}}" alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                                                </a>
                                        </li>
                                @endif
                            @endif
                        <!-- New element END -->
                    @endforeach
                </ul>
            @endif

             <!-- New Elements - Review-bottom -->
             <div class="review-inner-layer review-inner-content" >
                <div class="review-block" datesort="1600247687" scoresort="5.0" >
                    <div class="review-top" >
                        <div class="top-main" >
                            <div class="location-icon" >
                                <i class="trav-set-location-icon" ></i>
                            </div>
                            <div class="review-txt" >
                                <a style="text-decoration: none;" href="/place/{{@$review->place->id}}" target="_blank"><h3 class="review-ttl" >{{@$review->place->transsingle->title}}</h3></a>
                                <div class="sub-txt" >
                                    <div class="rate-label" >
                                        <b >{{round(@$avg, 1)}}</b>
                                        <i class="trav-star-icon" ></i>
                                    </div>&nbsp;
                                    <span >from <b>{{$total}} reviews</b></span>
                                </div>
                            </div>
                        </div>
                        <div class="btn-wrap" >
                            <?php
                                $placefollow = \App\Models\Place\PlaceFollowers::where('users_id', Auth::user()->id)->where('places_id', @$review->place->id)->get();
                            ?>
                            @if(count($placefollow) > 0)
                                <button class="btn btn-light-grey btn-bordered place-follow-btn" onclick="newsfeed_place_following('unfollow', {{@$review->place->id}}, this)" data-id="4263691" data-type="follow" >
                                    Unfollow </button>
                            @else
                                <button class="btn btn-light-primary btn-bordered place-follow-btn" onclick="newsfeed_place_following('follow', {{@$review->place->id}}, this)" data-id="4263691" data-type="follow" >
                                Follow </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- New Elements - Review bottom END -->

        </div>


       
    </div>
@endif
<script>
    $(document).ready(function(){
        // Post text tag popover
        var tagProfileIDs = '{{isset($tagProfileIDs) ? $tagProfileIDs : ""}}';
        var profileids = tagProfileIDs.split(',');
        if (profileids.length > 0) {
            $.each(profileids, function (i, v) {
                $(`.post-text-tag-${v}`).popover({
                    html: true,
                    content: $(`#popover-content-${v}`).html(),
                    template: '<div class="popover bottom tagging-popover" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
                    trigger: 'hover',
                });
            })
        }
        $(document).on('click', ".read-more-link", function () {
            $(this).closest('.post-txt-wrap').find('.less-content').hide()
            $(this).closest('.post-txt-wrap').find('.more-content').show()
            $(this).hide()
        });
        $(document).on('click', ".read-less-link", function () {
            $(this).closest('.more-content').hide()
            $(this).closest('.post-txt-wrap').find('.less-content').show()
            $(this).closest('.post-txt-wrap').find('.read-more-link').show()
        });

        // Post type - shared place, slider
        $('.shared-place-slider').lightSlider({
            autoWidth: true,
            slideMargin: 22,
            pager: false,
            controls: false,
        });
        $('.play-btn, .video-play-btn').click(function(){
            $(this).siblings('video').get(0).play();
            $(this).toggleClass('hide');
        });
    })
</script>