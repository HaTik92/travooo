<?php
$comment_childs = \App\Models\Events\EventsComments::where('parents_id', '=', $comment->id)->orderby('created_at','DESC')->get();
?>
<div topSort="{{count($comment->likes) + count($comment_childs)}}" newSort="{{strtotime($comment->created_at)}}">
    <div class="post-comment-row news-feed-comment" id="commentRow{{$comment->id}}">
        <div class="post-com-avatar-wrap">
            <img src="{{check_profile_picture($comment->author->profile_picture)}}" alt="">
        </div>
        <div class="post-comment-text">
            <div class="post-com-name-layer">
                <a href="#" class="comment-name">{{$comment->author->name}}</a>
                {!! get_exp_icon($comment->author) !!}
                <div class="post-com-top-action">
                    <div class="dropdown ">
                        <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <i class="trav-angle-down"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Postcomment">
                            @if($comment->author->id==Auth::user()->id)
                            <a href="javascript:;" class="dropdown-item post-edit-comment" data-id="{{$comment->id}}"
                                data-post="{{$post->id}}">
                                <span class="icon-wrap">
                                    <i class="trav-pencil" aria-hidden="true"></i>
                                </span>
                                <div class="drop-txt comment-edit__drop-text">
                                    <p><b>@lang('profile.edit')</b></p>
                                </div>
                            </a>
                            @else
                            <a href="#" class="dropdown-item" data-toggle="modal" data-target="#spamReportDlg"
                                onclick="injectData({{$comment->id}},this)">
                                <span class="icon-wrap">
                                    <i class="trav-flag-icon-o"></i>
                                </span>
                                <div class="drop-txt comment-report__drop-text">
                                    <p><b>@lang('profile.report')</b></p>
                                </div>
                            </a>
                            @endif
                            @if($comment->author->id==Auth::user()->id && count($comment_childs) == 0)
                            <a href="javascript:;" class="dropdown-item postCommentDelete" id="{{$comment->id}}"
                                data-post="{{$post->id}}" data-type="1">
                                <div class="drop-txt comment-delete__drop-text">
                                    <p><b>Delete</b></p>
                                </div>
                            </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="comment-txt comment-text-{{$comment->id}}">
                @php
                $showChar = 100;
                $ellipsestext = "...";
                $moretext = "more";
                $lesstext = "less";

                $comment_content = $comment->text;
                $convert_content = strip_tags($comment_content);

                if(mb_strlen($convert_content, 'UTF-8') > $showChar) {

                $c = mb_substr($comment_content, 0, $showChar, 'UTF-8');

                $html = '<span class="less-content">'.$c . '<span class="moreellipses">' . $ellipsestext .
                        '&nbsp;</span> <a href="javascript:;" class="read-more-link">' . $moretext . '</a></span> <span
                    class="more-content" style="display:none">' . $comment_content . ' &nbsp;&nbsp;<a
                        href="javascript:;" class="read-less-link">' . $lesstext . '</a></span>';

                $comment_content = $html;
                }
                @endphp
                <p>{!!$comment_content!!}</p>
                <form class="commentEditForm{{$comment->id}} comment-edit-form d-none" method="POST"
                    action="{{url("new-comment") }}" autocomplete="off" enctype="multipart/form-data">
                    {{ csrf_field() }}<input type="hidden" data-id="pair{{$comment->id}}" name="pair"
                        value="{{uniqid()}}" />
                    <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0">
                        <div class="post-create-input p-create-input b-whitesmoke">
                            <textarea name="text" data-id="text{{$comment->id}}"
                                class="textarea-customize post-comment-emoji"
                                style="display:inline;vertical-align: top;min-height:50px; height:auto;"
                                placeholder="@lang('comment.write_a_comment')">{!!$comment->text!!}</textarea>
                        </div>
                        <div class="post-create-controls b-whitesmoke d-none">
                            <div class="post-alloptions">
                                <ul class="create-link-list">
                                    <li class="post-options">
                                        <input type="file" name="file[]" class="commenteditfile"
                                            id="commenteditfile{{$comment->id}}" style="display:none" multiple>
                                        <i class="fa fa-camera click-target"
                                            data-target="commenteditfile{{$comment->id}}"></i>
                                    </li>
                                </ul>
                            </div>
                            <div class="comment-edit-action">
                                <a href="javascript:;" class="edit-cancel-link"
                                    data-comment_id="{{$comment->id}}">Cancel</a>
                                <a href="javascript:;" class="edit-post-link"
                                    data-comment_id="{{$comment->id}}">Post</a>
                            </div>
                        </div>
                        <div class="medias p-media b-whitesmoke">
                            @if(is_object($comment->medias))
                            @foreach($comment->medias AS $photo)
                            @php
                            $file_url = $photo->media->url;
                            $file_url_array = explode(".", $file_url);
                            $ext = end($file_url_array);
                            $allowed_video = array('mp4');
                            @endphp
                            <div class="img-wrap-newsfeed">
                                <div>
                                    @if(in_array($ext, $allowed_video))
                                    <video style="object-fit: cover" class="thumb" controls>
                                        <source src="{{$file_url}}" type="video/mp4">
                                    </video>
                                    @else
                                    <img class="thumb" src="{{$file_url}}" alt="">
                                    @endif
                                </div>
                                <span class="close remove-media-file" data-media_id="{{$photo->media->id}}">
                                    <span>×</span>
                                </span>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <input type="hidden" name="post_id" value="{{$post->id}}" />
                    <input type="hidden" name="comment_id" value="{{$comment->id}}">
                    <input type="hidden" name="comment_type" value="1">
                    <button type="submit" class="d-none"></button>
                </form>
            </div>
            <div class="post-image-container">
                @if(is_object($comment->medias))
                @php
                $index = 0;

                @endphp
                @foreach($comment->medias AS $photo)
                @php
                $index++;
                $file_url = $photo->media->url;
                $file_url_array = explode(".", $file_url);
                $ext = end($file_url_array);
                $allowed_video = array('mp4');
                @endphp
                @if($index % 2 == 1)
                <ul class="post-image-list" style=" display: flex;margin-bottom: 20px;">
                    @endif
                    <li style="overflow: hidden;margin:1px;">
                        @if(in_array($ext, $allowed_video))
                        <video style="object-fit: cover" width="192" height="210" controls>
                            <source src="{{$file_url}}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        @else
                        <a href="{{$file_url}}" data-lightbox="comment__media{{$comment->id}}">
                            <img src="{{$file_url}}" alt="" style="width:192px;height:210px;object-fit: cover;">
                        </a>
                        @endif
                    </li>
                    @if($index % 2 == 0)
                </ul>
                @endif
                @endforeach
                @endif

            </div>


            <div class="comment-bottom-info">
                <div class="comment-info-content">
                    <div class="dropdown">
                        <a href="#" class="postCommentLikes like-link dropbtn" id="{{$comment->id}}"><i
                                class="trav-heart-fill-icon {{($comment->likes()->where('users_id', Auth::user()->id)->first())?'fill':''}}"
                                aria-hidden="true"></i> <span
                                class="comment-like-count">{{count($comment->likes)}}</span></a>
                        @if(count($comment->likes))
                        <div class="dropdown-content comment-likes-block" data-id="{{$comment->id}}">
                            @foreach($comment->likes()->orderBy('created_at', 'DESC')->take(7)->get() as $like)
                            <span>{{$like->author->name}}</span>
                            @endforeach
                            @if(count($comment->likes)>7)
                            <span>...</span>
                            @endif
                        </div>
                        @endif
                    </div>
                    <a href="#" class="postCommentReply reply-link" id="{{$comment->id}}">Reply</a>
                    <span class="com-time"><span class="comment-dot"> ·
                        </span>{{diffForHumans($comment->created_at)}}</span>
                </div>
            </div>
        </div>
    </div>
    @if(count($comment_childs) > 0)
    @foreach($comment_childs as $child)
    @include('site.home.partials.post_comment_reply_block')
    @endforeach
    @endif
    @if(Auth::user())
        <div class="post-add-comment-block" id="replyForm{{$comment->id}}"
            style="padding-top:0px;padding-left: 59px;display:none;">
            
            <div class="post-add-com-inputs">
                <div class="post-add-comment-block" dir="auto">
                    <div class="avatar-wrap">
                        <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                            style="width:50px !important;">
                    </div>
                    <!-- New elements -->
                    <div class="add-comment-input-group n-post-comment-block">
                        <form class="postCommentForm " method="POST" data-id="{{$comment->id}}" data-type="event-comment-reply" autocomplete="off" enctype="multipart/form-data">
                            {{ csrf_field() }}<input type="hidden" data-id="pair{{$comment->id}}" name="pair" value="{{uniqid()}}"/>
                            <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0">
                                <div class="post-create-input n-post-create-input b-whitesmoke">
                                    <textarea name="text" class="comment-reply-txt post-comment-emoji"></textarea>
                                    <div class="n-post-img-wrap">
                                        <input type="file" name="file" class="commentfile" id="commentreplyfile{{$comment->id}}" data-id="{{$comment->id}}"  style="display:none">
                                        <i class="fa fa-camera click-target" data-target="commentreplyfile{{$comment->id}}"></i>
                                    </div>
                                </div>
                                <div class="medias b-whitesmoke">
                                </div>
                            </div>
                            <input type="hidden" name="post_id" value="{{$comment->id}}">
                            <input type="hidden" name="post_type" value="event-comment-reply">
                            <input type="hidden" name="child" value="{{$comment->id}}">
                            <button type="submit"  class="d-none"></button>
                        </form>
                        <div class="comment-rseply-media"></div>
                    </div>
                    <!-- New elements END  -->
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
           
    
            });
        </script>
    @endif

</div>