@foreach ($data as $item)
    <div class="people-row" >
        <div class="main-info-layer" >
            <div class="img-wrap" >
                <img class="ava mCS_img_loaded" src="{{check_profile_picture($item['profile'])}}" style="width:50px;height:50px" >
            </div>
            <div class="txt-block" >
                <div class="name" >
                    <a href="{{url('profile/'.$item['user_id'])}}" class="tm-user-link" target="_blank" >{{$item['name']}} </a>
                    @if(is_friend($item['user_id']))
                        <span class="user-status">Friend</span>
                    @endif
                </div>
            </div>
            <div class="going-action-block going-648" >
                @if(Auth::user()->id != $item['user_id'])
                    <button type="button" class="btn btn-light-grey btn-bordered" data-dismiss="modal" data-image="{{check_profile_picture($item['profile'])}}" data-name="{{$item['name']}}" data-id="{{$item['user_id']}}" data-toggle="modal" data-target="#messageToUser">Message</button>
                    <button type="button" data-id="{{ $item['user_id'] }}" class="btn button_follow_from_post_likes  btn-bordered {{ check_follow($item['user_id']) ? 'btn-light-primary' : 'btn-light-grey js-followed' }}">{{ check_follow($item['user_id']) ? __('profile.follow') : __('profile.unfollow') }}</button>
                @endif
            </div>
        </div>
    </div>    
@endforeach
