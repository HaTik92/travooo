    <div class="modal white-style share-post-modal" data-backdrop="false" id="shareablePost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog share-post-new modal-670" role="document" >
            <div class="modal-custom-block" >
                <div class="post-block post-travlog post-create-travlog" style="padding-bottom: 40px;" >
                    <div class="share-modal-header">
                        <div>
                        <h4>Share this post</h4>
                        </div>
                        <button type="button" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times"></i>
                        </button>
                    </div>
                    <div class="shared-post-comment">
                        <div class="shared-post-comment-details share-permission-list">
                            <div class="dropdown privacy-settings">
                                <button id="add_share_permission" class="btn btn--sm btn--outline dropdown-toggle " data-toggle="dropdown">
                                    <i class="trav-globe-icon"></i> PUBLIC
                                </button>
                                <div class="dropdown-menu dropdown-menu-left permissoin_show hided">
                                    <a class="dropdown-item share-permission-drop-down" href="javascript:;" permtype="0">
                                        <span class="icon-wrap">
                                            <i class="trav-globe-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Public</b></p>
                                            <p>Anyone can see this post</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item share-permission-drop-down"  href="javascript:;" permtype="1">
                                        <span class="icon-wrap">
                                            <i class="trav-users-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Friends Only</b></p>
                                            <p>Only you and your friends can see this post</p>
                                        </div>
                                    </a>
                                    <a class="dropdown-item share-permission-drop-down" href="javascript:;" permtype="2">
                                        <span class="icon-wrap">
                                            <i class="trav-lock-icon"></i>
                                        </span>
                                        <div class="drop-txt">
                                            <p><b>Only Me</b></p>
                                            <p>Only you can see this post</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="share-modal-action">
                        <div class='user--info'>
                        <img class="ava" src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                        <p>{{Auth::user()->name}}</p>
                        </div>
                    </div>
                    <textarea name="" style="resize: none;" id="message_popup_text_for_share" cols="30" rows="2" placeholder="Write something..."></textarea>
                    <div class="shared-post-wrap" id="shared-post-wrap-content">
                    </div>
                    <div class="share-footer-modal">
                        <div  class="share-footer-modal__icons">
                            <a href="javascript:;" class="ico--ws" onclick="__postShare('whatsapp')"><i class="fab fa-whatsapp"></i></a>
                            <a href="javascript:;" class="ico--face" onclick="__postShare('facebook')"><i class="fab fa-facebook-f"></i></a>
                            <a href="javascript:;" class="ico--twitter" onclick="__postShare('twitter')"><i class="fab fa-twitter"></i></a>
                            <a href="javascript:;" class="ico--pin" onclick="__postShare('pintrest')"><i class="fab fa-pinterest-p"></i></a>
                            <a href="javascript:;" class="ico--tumb" onclick="__postShare('tumblr')"><i class="fab fa-tumblr"></i></a>
                            <a href="javascript:;" class="ico--link" onclick="__postShare('link')"><i class="fa fa-link"></i></a>
                        </div>
                        <button class="btn place_cancel_btn share-modal__close" data-dismiss="modal" type="button">Cancel</button>
                        <button class="btn place_share_btn share_post_btn">Share</button> 
                        <input type="hidden" name="share-post-id" id="share-post-id">
                        <input type="hidden" name="share-post-type" id="share-post-type">
                    </div>

                </div>

            </div>
        </div>
    </div>

    <script>
        var permission = 0;
        $(document).on('click', '.share-permission-drop-down', function(){
            var s_type = $(this).attr('permtype')
            if(s_type == 0){
                $('#add_share_permission').html('<i class="trav-globe-icon"></i> PUBLIC');
            }else if(s_type==1){
                $('#add_share_permission').html('<i class="trav-users-icon"></i> Friends Only');
            }else if(s_type==2){
                $('#add_share_permission').html('<i class="trav-lock-icon"></i> Only Me');
            }

            permission = s_type;
        })


        //share portion
        var uniquePostUrl = null;
        $('#shareablePost').on('show.bs.modal',function (event){
            $('#shared-post-wrap-content').empty();
            $("#message_popup_text_for_share").val('')
            $('#add_share_permission').html('<i class="trav-globe-icon"></i> PUBLIC');
            permission = 0

            id = event.relatedTarget.dataset.id;
            type = event.relatedTarget.dataset.type;
            $('#share-post-id').val(id);
            $('#share-post-type').val(type);
            $.ajax({
                method: "GET",
                url: '{{  url("get-shareable-post")}}',
                data: {id: id,'type':type}
            })
                .done(function (res) {
                    if(res !=0)
                        $('#shared-post-wrap-content').html(res);
                });

            uniquePostUrl = event.relatedTarget.dataset.uniqueurl;
            console.log(uniquePostUrl);
        });
        $('.share_post_btn').on('click',function(){
            var id = $('#share-post-id').val()
            var type = $('#share-post-type').val();

            var shared_count = $('.'+ type + '-shared-count-'+id)

            $.ajax({
                method: "POST",
                url: '{{url("share-post")}}',
                data: {id: id,'type':type,'text':$('#message_popup_text_for_share').val(), permission: permission}
            })
                .done(function (res) {
                    if(res !=0){
                        res = JSON.parse(res);

                        $('#shareablePost').modal('hide');
                        toastr.success('Post has been shared');
                        $('#share-post-id').val('');
                        $('#share-post-type').val('');
                        $('#message_popup_text_for_share').val('');
                        if(res.status == 1){
                            shared_count.html(parseInt(shared_count.html()) + 1)
                        }
                    }
                });
        });

        function __postShare(type){
            var socialUrl = null,
                shareText = $("#message_popup_text_for_share").val() || '';

            if(uniquePostUrl){
                switch(type){
                    case 'whatsapp':
                        /*  https://web.whatsapp.com/send?text=
                            https://wa.me/(phone)?text=hello
                            https://api.whatsapp.com/send/?phone&text=hello */
                        socialUrl = `https://web.whatsapp.com/send?text=${shareText} ${uniquePostUrl}`;
                        break;

                    case 'facebook':
                        //t= // t for text
                        socialUrl = `https://www.facebook.com/sharer.php?u=${uniquePostUrl}&t=${shareText}`;
                        break;

                    case 'twitter':
                        socialUrl = `https://twitter.com/intent/tweet?text=${shareText} ${uniquePostUrl}`;
                        break;

                    case 'pintrest':
                        //&media=&description=
                        socialUrl = `https://pinterest.com/pin/create/button/?url=${uniquePostUrl}&description=${shareText}`;
                        break;

                    case 'tumblr':
                        //t= // t for text
                        socialUrl = `https://www.tumblr.com/share?t=hello&v=3&u=${uniquePostUrl}&t=${shareText}`;
                        break;
                    case 'link':
                        var copy_link = uniquePostUrl;
                        var dummy = document.createElement("input");
                        dummy.value = copy_link;
                        document.body.appendChild(dummy);
                        dummy.select();
                        dummy.focus();
                        document.execCommand("copy");
                        document.body.removeChild(dummy);

                        toastr.success('The link copied!');
                    break;
                }

                if(socialUrl){
                    window.open(socialUrl, '_blank', 'location=yes,scrollbars=yes,status=yes');
                }
            }
        }
    </script>
