@if(!user_access_denied($reply->users_id))
<div class="post-tips-row" id="repliesRow{{$reply->id}}">
    <div class="tips-top">
        <div class="tip-avatar">
            <img src="{{check_profile_picture($reply->author->profile_picture)}}"
                 alt="" style="width:25px;height:25px;">
        </div>
        <div class="tip-content ov-wrap" style="width: 89%;display: grid;">
            <div style="position: relative;">
                <div class="top-content-top">
                    <a href="{{url_with_locale('profile/'.$reply->author->id)}}"
                       class="name-link" target="_blank">{{$reply->author->name}}</a>
                    {!! get_exp_icon($reply->author) !!}
                    <span>said</span><span class="dot"> · </span>
                    <span>{{diffForHumans($reply->created_at)}}</span>

                    <div class="post-com-top-action">
                    <div class="dropdown ">
                        <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="trav-angle-down"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="DiscussionReplies">
                             @if (Auth::check() && $reply->author->id === Auth::user()->id)
                            <a href="javascript:;" class="dropdown-item edit-reply"  data-id="{{$reply->id}}">
                                <span class="icon-wrap">
                                   <i class="trav-pencil" aria-hidden="true"></i>
                                </span>
                                <div class="drop-txt comment-edit__drop-text">
                                    <p><b>@lang('profile.edit')</b></p>
                                </div>
                            </a>

                            <a href="javascript:;" class="dropdown-item delete-reply" id="{{$reply->id}}" reportid="{{$reply->id}}">
                                <span class="icon-wrap">
                                    <img src="{{asset('assets2/image/delete.svg')}}" style="width:20px;">
                                </span>
                                <div class="drop-txt comment-delete__drop-text">
                                    <p><b>Delete</b></p>
                                </div>
                            </a>
                             @else
                                <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#" data-toggle="modal"  data-target="#spamReportDlg" onclick="injectReplyData({{$reply->id}},this)">
                                <span class="icon-wrap">
                                    <i class="trav-flag-icon-o"></i>
                                </span>
                                    <div class="drop-txt comment-report__drop-text">
                                        <p><b>@lang('profile.report')</b></p>
                                    </div>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                </div>
                <div class="tip-txt ov-wrap">
                    <p class="dis_reply_{{$reply->id}} ov-wrap disc-text-block dis-reply-text">
                        @php
                        $showChar = 100;
                        $ellipsestext = "...";
                        $moretext = "more";
                        $lesstext = "less";

                        $content = $reply->reply;
                        $convert_content = strip_tags($content);

                        if(mb_strlen($convert_content, 'UTF-8') > $showChar) {

                        $c = mb_substr($content, 0, $showChar, 'UTF-8');

                        $html = '<span class="less-content">'.$c . '<span class="moreellipses">' . $ellipsestext .
                                '&nbsp;</span> <a href="javascript:;" class="read-more-link">' . $moretext . '</a></span> <span
                            class="more-content" style="display:none">' . $content . ' &nbsp;&nbsp;<a
                                href="javascript:;" class="read-less-link">' . $lesstext . '</a></span>';

                        $content = $html;
                
                        }
                        @endphp
                        {!!$content!!}</p>
                        <form method='post' class="discussionReplyEditForm{{$reply->id}} d-none">
                            <input type="hidden" class="media-type"  name="media_type" value="{{($reply->medias_type)?$reply->medias_type:''}}"/>
                            <input type="hidden" class="media-url"  name="media_url" value="{{($reply->medias_url)?$reply->medias_url:''}}"/>
                            <div class="reply-text-inner edit-reply-text">
                                <textarea name="text" class="disc-reply-edit-textarea disc-reply-emoji" maxlength="1000" style="height: 76px;" id="disc_edit_reply_textarea_{{$reply->id}}"  placeholder="@lang('discussion.write_a_tip_dots')">{{$reply->reply}}</textarea>
                                <div class="reply-media-error"></div>
                                <div class="reply-media-block">
                                    <div class="reply-media-list">
                                        <div class="reply-media-inner">
                                             @if($reply->medias_type)
                                                <div class="img-wrap-newsfeed">
                                                    <div>
                                                        @if($reply->medias_type == 'image')
                                                            <img class="disc-media-thumb" src="{{$reply->medias_url}}">
                                                        @endif
                                                    </div>
                                                    <span class="disc-media-close removeFile remove-reply-media" dataFId="{{$reply->id}}">
                                                        <span>×</span>
                                                    </span>
                                                </div>
                                             @endif
                                        </div>
                                         <div class="replies-loader d-none">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="reply-media-wrap">
                                        <input type="file" name="file[]" class="replyeditfile{{$reply->id}} replyfile{{$reply->id}}" data-disc-id="{{$reply->id}}" data-id="replyfile{{$reply->id}}"  style="display:none">
                                        <button class="click-target replies-media-btn" type="button" data-target="replyfile{{$reply->id}}">
                                         <i class="fa fa-camera"></i>
                                    </button>
                                    </div>
                                </div>
                            </div>
                            <div class="replay-edit-action-block">
                                <input type="hidden" name="reply_id" value="{{$reply->id}}"/>
                                <a href="javascript:;" class="reply-edit-cancel-link" dataId="{{$reply->id}}">Cancel</a>
                                <a href="javascript:;"  class="reply-edit-post-link" dataId="{{$reply->id}}">Post</a>
                                <input type="submit" name="submit" class="d-none"/>
                            </div>
                        </form>

                </div>
            </div>
            <div class="rp-media-block">
               @if($reply->medias_type == 'image')
                <a href="{{$reply->medias_url}}" data-lightbox="ask-reply__media-{{$reply->id}}">
                   <img src="{{$reply->medias_url}}" class="rp-image">
                </a>
               @endif
           </div>
        </div>
    </div>
    <div style="display: flex; justify-content: space-between; margin-top: 15px;">
        <div class="tips-footer updownvote updown_{{$reply->id}}" id="{{$reply->id}}">
            <a href="#" class="upvote-link {{Auth::check()?'up':'open-login'}} {{(Auth::check() && $reply->upvotes()->where('users_id', Auth::id())->first())?'':'disabled'}}" id="{{$reply->id}}">
                <span class="arrow-icon-wrap"><i class="trav-angle-up"></i></span>
            </a>
            <span style="font-size:85%"><b class="upvote-count">{{optimize_counter(count($reply->upvotes))}}</b> Upvotes</span>
            &nbsp;&nbsp;
            <a href="#" class="upvote-link {{Auth::check()?'down':'open-login'}}  {{(Auth::check() && $reply->downvotes()->where('users_id', Auth::id())->first())?'':'disabled'}}" id="{{$reply->id}}">
                <span class="arrow-icon-wrap"><i class="trav-angle-down"></i></span>
            </a>
            <span style="font-size:85%"><b class="downvote-count">{{optimize_counter(count($reply->downvotes))}}</b> Downvotes</span>
        </div>
    </div>
</div>
@endif
