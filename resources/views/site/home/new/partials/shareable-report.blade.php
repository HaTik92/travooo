<?php
    $post_type = $post['type'] ;
    $variable = $post['variable'] ;
    $report = \App\Models\Reports\Reports::find($post['variable'] );
    $str = '';
    
        if(isset($report->infos)){
            foreach($report->infos AS $ri){
                if($ri->var=='text'){
                    $str .= $ri->val; 
                }
            }
        }
        $word = str_word_count(strip_tags($str));
        $m = floor($word / 200);
        $s = floor($word % 200 / (200 / 60));
        if($m>0)
            $est = $m . ' minute' . ($m == 1 ? '' : 's') ;
        else
            $est =  $s . ' second' . ($s == 1 ? '' : 's');
    $followers = [];
    if(Auth::check()){
        $following = \App\Models\User\User::find(Auth::user()->id);
        foreach($following->get_followers as $f):
            $followers[] = $f->followers_id;
        endforeach;
    }
?>
@if(isset( $report ))
    
    <div class="post-block @if(@$post->action == 'update') post-block-notification @endif travlog">
        @if(@$post->action == 'update')
        <div class="post-top-info-layer">
            <div class="post-info-line">
                <a class="post-name-link" href="{{url('profile/'.$report->author->id)}}">{{$report->author->name}}</a>{!! get_exp_icon($report->author) !!}
                updated travelog {{diffForHumans_2($report->created_at)}}
            </div>
        </div>
        @endif
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap ava-50">
                    <img src="{{check_profile_picture($report->author->profile_picture)}}" alt="">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link"
                            href="{{url('profile/'.$report->author->id)}}">{{$report->author->name}}</a>
                    </div>
                    <div class="post-info">
                        wrote a <strong>travelog</strong>
                        {{-- about
                        @if(isset($report->cities_id) && $report->cities_id !='')
                        <a href="/city/{{$report->cities_id}}" target="_blank"
                            class="link-place">{{$report->city->transsingle->title}}</a>
                        @elseif(isset($report->countries_id) && $report->countries_id !='')
                        <a href="/country/{{$report->countries_id}}" target="_blank"
                            class="link-place">{{$report->country->transsingle->title}}</a>
                        @endif --}}
                        on {{diffForHumans_2($report->created_at)}}
                    </div>
                </div>
            </div>
            <div class="post-top-info-action" dir="auto" id="report_{{$report->id}}" >
                
            </div>
        </div>
        <div class="topic-inside-panel-wrap"  onclick=" window.open('/reports/{{$report->id}}'); " style="cursor: pointer;">
            <div class="topic-inside-panel">
                <div class="panel-txt">
                    <h3 class="panel-ttl"><a style="text-decoration: underline;    color: black;" target="_blank" href="/reports/{{$report->id}}">{{$report->title}}</a></h3>
                    <div class="post-txt-wrap">
                        <div>

                            <span class="less-content disc-ml-content">{!! substr($report->description,0, 200) !!}<span class="moreellipses">...</span>
                                @if(strlen($report->description)>200)
                                <a href="javascript:;" class="read-more-link">More</a>@endif
                            </span>
                            <span class="more-content disc-ml-content" style="display: none;">{!! $report->description !!}
                                <a href="javascript:;" class="read-less-link"> Less</a>
                            </span>
                        </div>
                        @if($est != '0 seconds')
                        <div class="read-time-badge">{{$est}} read</div>
                        @endif
                    </div>
                </div>
                <div class="panel-img disc-panel-img">
                    <img src="@if(isset($report->cover[0])){{$report->cover[0]->val}}@else {{asset('assets2/image/placeholders/no-photo.png')}} @endif"
                        alt="{{$report->title}}" style="width: 170px; height: 120px;">
                </div>
            </div>
        </div>
        @php
        // dd($report);
            $flag_liked  =false;
            if(count($report->likes)>0){
                foreach($report->likes as $like){
                    if(Auth::check() && $like->users_id == Auth::user()->id)
                            $flag_liked = true;
                }
            }
        @endphp
        
    </div>

@endif
