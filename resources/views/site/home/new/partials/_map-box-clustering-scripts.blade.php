<script src="https://npmcdn.com/@turf/turf@5.1.6/turf.min.js"></script>
<script>
    function getDistanceInOnePixel(map) {
        var lat = map.getCenter().lat;
        var zoom = map.getZoom();

        return 40075.016686 * Math.abs(Math.cos(lat * Math.PI/180)) / Math.pow(2, zoom + 9);
    }

    function clusterCustomMarkers(placesCoordinatesFlat, map) {
        var clusters = [];
        var markersClusters = [];
        var pixelDistance = getDistanceInOnePixel(map);
        var savedLines = [];
        var savedLength = [];

        var i = undefined;
        var k = undefined;
        var marker = undefined;

        for (var i1 = 0; i1 < placesCoordinatesFlat.length; i1++) {
            i = i1;
            var iCoordinates = placesCoordinatesFlat[i1].lnglat;
            for (var k1 = 0; k1 < placesCoordinatesFlat.length; k1++) {
                k = k1;
                var kCoordinates = placesCoordinatesFlat[k1].lnglat;
                if (i === k || (markersClusters[i] === markersClusters[k] && markersClusters[i] !== undefined)) {
                    continue;
                }

                if (savedLines[i] === undefined) {
                    savedLines[i] = [];
                    savedLength[i] = [];
                }

                if (savedLines[k] === undefined) {
                    savedLines[k] = [];
                    savedLength[k] = [];
                }

                if (savedLines[i][k] === undefined) {
                    savedLines[i][k] = turf.lineString([iCoordinates, kCoordinates]);
                    savedLines[k][i] = savedLines[i][k]
                    savedLength[i][k] = turf.length(savedLines[i][k], {units: 'kilometers'});
                    savedLength[k][i] = savedLength[i][k]
                }

                var length = savedLength[i][k];

                if (length <= (pixelDistance * 50)) {
                    if (markersClusters[i] === undefined && markersClusters[k] === undefined) {
                        clusters.push([i, k]);

                        markersClusters[i] = clusters.length - 1;
                        markersClusters[k] = clusters.length - 1;
                    } else if (markersClusters[i] !== undefined) {
                        if (markersClusters[k] === undefined) {
                            clusters[markersClusters[i]].push(k);
                            markersClusters[k] = markersClusters[i];
                        } else {
                            var clusterToRemove = markersClusters[k];
                            for (var l = 0; l < clusters[clusterToRemove].length; l++) {
                                if (clusters[clusterToRemove][l] === undefined) {
                                    continue;
                                }
                                marker = clusters[clusterToRemove][l];
                                markersClusters[marker] = markersClusters[i];
                                clusters[markersClusters[i]].push(marker);
                            }

                            clusters[clusterToRemove] = undefined;
                        }
                    } else {
                        clusters[markersClusters[k]].push(i);
                        markersClusters[i] = markersClusters[k];
                    }
                }
            }
        }

        var formattedClusters = [];
        var formattedMarkersCluster = [];

        for (i = 0; i < markersClusters.length; i++) {
            var cluster = markersClusters[i];
            if (cluster === undefined) {
                continue;
            }
            var id = placesCoordinatesFlat[i].id;
            formattedMarkersCluster[id] = cluster;
            if (formattedClusters[cluster] === undefined) {
                formattedClusters[cluster] = [];
            }

            formattedClusters[cluster].push(id.toString());
        }

        return [formattedClusters, formattedMarkersCluster];
    }

    function sortCoords(point1, point2, city1, city2, country1, country2, coordinates) {
        var area = 'world';

        if (city1 === city2) {
            area = 'city';
        } else if (country1 === country2) {
            area = 'country';
        }

        coordinates[area].push([point1, point2]);
    }

    var paints = {
        'world': {
            'line-color': '#4080ff',
            'line-width': 3,
            'line-dasharray': [1, 1.5]
        },
        'country': {
            'line-color': '#4080ff',
            'line-width': 3,
            'line-dasharray': [0.0001, 1.3]
        },
        'city': {
            'line-color': '#4080ff',
            'line-width': 3
        }
    };

    function setRouteSources(map, coordinates)
    {
        var layers = map.getStyle().layers;

        for (var layer in layers) {
            var layerKey = layers[layer].id;
            if (layerKey.includes('route')) {
                map.removeLayer(layerKey);
            }
        }

        var routeId = 0;

        for (var area in coordinates) {
            for (var i in coordinates[area]) {
                var route = coordinates[area][i];
                var source = map.getSource('route' + routeId);
                var data = {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': route
                    }
                };

                if (source) {
                    source.setData(data);
                } else {
                    map.addSource('route' + routeId, {
                        'type': 'geojson',
                        'data': data
                    });
                }

                map.addLayer({
                    'id': 'route' + routeId,
                    'type': 'line',
                    'source': 'route' + routeId,
                    'layout': {
                        'line-join': 'round',
                        'line-cap': 'round'
                    },
                    'paint': paints[area]
                });

                routeId++;
            }
        }
    }

    function initMap(container, places) {
        if (typeof places !== 'undefined') {
            var placesCoordinates = [];
            var placesCoordinatesFlat = [];
            var place = null;
            var placeCoordinates = null;
            var tempBounds = new mapboxgl.LngLatBounds();

            for (var i = 0; i < places.length; i++) {
                place = places[i];
                placeCoordinates = [parseFloat(place.place.lng), parseFloat(place.place.lat)];

                placesCoordinates[place.id] = placeCoordinates;
                placesCoordinatesFlat.push({
                    lnglat: placeCoordinates,
                    id: place.id
                });

                tempBounds.extend(placeCoordinates);
            }

            mapboxgl.accessToken = '{{config('mapbox.token')}}';
            var map = new mapboxgl.Map({
                container: container,
                style: 'mapbox://styles/mapbox/satellite-streets-v11',
                //center:intial_latlng,
                maxZoom: 13,
                interactive: false
            });

            var coordinates = {
                'city': [],
                'country': [],
                'world': []
            };

            map.on('load', function() {
                map.fitBounds(tempBounds, {
                    padding: {top: 50, bottom:50, left: 50, right: 50},
                    linear: true,
                    duration: 0
                });

                var clusters = [];
                var markersClusters = [];

                var bounds = new mapboxgl.LngLatBounds();
                var renderedClusters = [];

                var beforeCity = null;
                var beforeCountry = null;
                var beforePoint = null;

                [clusters, markersClusters] = clusterCustomMarkers(placesCoordinatesFlat, map);

                var cluster = null;
                var count = null;

                for (i = 0; i < places.length; i++) {
                    count = 0;
                    place = places[i];
                    cluster = markersClusters[place.id];

                    var el = $(document.createElement('div'));
                    var style = 'background: url(\' '+place.media+' \');' +
                        'background-size: cover;' +
                        'background-repeat: no-repeat;' +
                        'background-position: center center;' +
                        'width: 36px;' +
                        'height: 36px;' +
                        'border-radius: 50px;' +
                        'border: 2px solid #4080ff !important;' +
                        'box-shadow: inset 0 0 0 2px #fff;' +
                        'cursor: pointer;';
                    $(el).attr('style', style);
                    $(el).addClass('marker');
                    $(el).attr('style', style);
                    placeCoordinates = placesCoordinates[place.id];

                    if (cluster !== undefined) {
                        if (renderedClusters[cluster] !== undefined) {
                            continue;
                        } else {
                            renderedClusters[cluster] = 1;
                        }

                        var count_span = $("<span style='position: absolute;right: -3px;top: -3px; background: #000000;color: #ffffff; width: 12px;height: 12px;border-radius: 50%;    font-size: 9px;    text-align: center;    display: flex;    align-items: center;    justify-content: center;'></span>").text(clusters[cluster].length);
                        $(el).append(count_span);
                    }

                    bounds.extend(placeCoordinates);

                    var marker = new mapboxgl.Marker($(el)[0])
                        .setLngLat(placeCoordinates)
                        .addTo(map);

                    if (beforePoint) {
                        sortCoords(beforePoint, placeCoordinates, beforeCity, place.cities_id, beforeCountry, place.countries_id, coordinates);
                    }

                    beforeCity = place.cities_id;
                    beforeCountry = place.countries_id;
                    beforePoint = placeCoordinates;
                }

                setRouteSources(map, coordinates);

                map.fitBounds(bounds, {
                    padding: {top: 50, bottom:50, left: 50, right: 50},
                    maxZoom: 13
                });
            });
        }
    }
</script>