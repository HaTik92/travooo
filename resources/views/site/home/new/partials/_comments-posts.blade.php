<!-- Updated elements -->
<?php
$types = ["Post", "media", "text", "trip"];
?>
<div class="post-foot-block post-reaction">
    <span class="{{Auth::check()?'post_like_button':'open-login liked'}} @if($flag_liked) liked @endif ">
        <a href="#" data-type="{{isset($post_type)?$post_type:"text"}}" id="{{$post->id}}" data-name="{{$post_type.$post->id}}">
            <i class="trav-heart-fill-icon"></i>
        </a>
    </span>
    <span id="post_like_count_{{$post->id}}" class=" {{isset($post_type)?$post_type:"text"}}_like_count_{{$post->id}}">
        <b>{{is_object($post->likes)?count($post->likes):0}}</b> <a class='mobie-comment--move' href="#usersWhoLike" data-toggle="modal" data-type="{{isset($post_type)?$post_type:"text"}}"
            data-id="{{$post->id}}">Likes</a>
    </span>
</div>
<!-- Updated element END -->
<div class="post-foot-block comments--wrap">
    <a target="_blank" href='#' class="comment-btn" data-shared="{{isset($sharing_flag)}}" post-type="{{$post_type}}" data-id="{{@$shared_variable ?? @$post->id}}" data-shared_id="{{@$shared_variable ? $post->id : '' }}" data-tab="{{'comments'.$post->id}}">
        <i class="trav-comment-icon"></i>
    </a>
    <div class='row-reverse-mobile'>
        @if(is_object($post->comments) && count($post->comments) > 0)
        <ul class="foot-avatar-list">
            @php $us = array(); @endphp
            @foreach($post->comments()->orderBy('created_at', 'desc')->get() AS $pc)
                @if(!in_array($pc->users_id, $us))
                @php $us[] = $pc->users_id; @endphp
                    @if(count($us)<4)
                        <li><img class="small-ava" src="{{check_profile_picture($pc->author->profile_picture)}}" alt="{{$pc->author->name}}" title="{{$pc->author->name}}"></li>
                        @else
                        @php break; @endphp
                        @endif
                @endif
            @endforeach
        </ul>
        @endif
        @php
                $count_comment = is_object($post->comments)?count($post->comments):0;
                $count_comment_reply = 0;
                foreach($post->comments as $comment) {
                    $count_comment_reply += count(\App\Models\Posts\PostsComments::where('parents_id', '=', $comment->id)->orderby('created_at','DESC')->get());
                }
                $total_count_comment = $count_comment + $count_comment_reply;
            @endphp
        <span><a href='#' class="comment-btn" data-shared="{{isset($sharing_flag)}}" post-type="{{$post_type}}" data-id="{{@$shared_variable ?? @$post->id}}" data-shared_id="{{@$shared_variable ? $post->id : '' }}" data-tab="{{'comments'.$post->id}}"><span class="{{$post->id}}-comments-count" >{{$total_count_comment}}</span>
                <span class='mobie-comment--move'>Comments</span></a></span>
    </div>
</div>
@php 
    if(isset($sharing_flag)){
        $shared_id = $shared_variable;
        $share_count =  \App\Models\Posts\PostsShares::where(['type'=>$post_type,'posts_type_id'=>$shared_id])->distinct('users_id')->pluck('users_id');
        $total_count = $share_count->count();

    }
    else{ 
        $shared_id = $post->id;
        $total_count  = is_object($post->shares)?count($post->shares):0;
    }
@endphp 
<div class="post-foot-block ml-auto">
    <span class="post_share_buttons"  id="{{$shared_id}}">
        @if(Auth::check())
        <a  href="#shareablePost" data-uniqueurl="{{($uniqueurl ?? '')}}" data-id="{{$shared_id}}"  data-type="{{$post_type}}"  data-toggle="modal" >
            <img class='share--image' src="{{asset('assets2/image/share-mobile.png')}}">
        </a>
        @else
        <a  href="javascript:;" class="open-login">
            <img class='share--image' src="{{asset('assets2/image/share-mobile.png')}}">
        </a>
        @endif
    </span>
    <span id="post_share_count_{{$shared_id}} {{Auth::check()?'':'open-login'}}">
        @if(Auth::check())
            <a href="#shareablePost" data-uniqueurl="{{($uniqueurl ?? '')}}" data-toggle="modal" data-id="{{$shared_id}}" data-type="{{$post_type}}"><strong>{{$total_count}}</strong>Shares</a>
        @else
            <a href="javascript:;" class="open-login"><strong>{{$total_count}}</strong>Shares</a>
        @endif
        
    </span>
</div>
