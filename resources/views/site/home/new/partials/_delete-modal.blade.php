<div class="modal white-style spam-report-modal" data-backdrop="false" id="deletePostNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style" role="document">
        <div class="post-block">
            <div class="modal-header">
                <h4 class="side-ttl">Delete Post</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="trav-close-icon"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group mb-3">
                    <div class="form-check mb-2">
                        <h5 class="d-inline-block">Are you sure you want to delete this post?</h5>
                    </div>
                </div>
            </div>
            <input type="hidden" name="delete-type" >
            <input type="hidden" name="id" >
            <div class="modal-footer">
                <button  data-dismiss="modal" class="send-report-btn link">Cancel</button>
                <button  class="send-report-btn red del_post_btn">Delete</button>
            </div>
            <input type="hidden" name="delete_id" id="delete_id">
            <input type="hidden" name="element_id" id="element_id">
            <input type="hidden" name="deletePostType" id="deletePostType">
        </div>
    </div>
</div>