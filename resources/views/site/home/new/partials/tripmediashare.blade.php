<?php 
$followers = [];
    $followers= \App\Models\UsersFollowers\UsersFollowers::where('followers_id', Auth::user()->id)->where('follow_type', 1)->pluck('users_id')->toArray();
$me = false;
$trip=[];
$post_type = $post->type;
$trip_share_media  = \App\Models\TripPlans\TripsMediasShares::find($post->variable);
if(is_object($trip_share_media) && isset($trip_share_media->trip_media_id)){
    $trip_media_details = \App\Models\TripMedias\TripMedias::find($trip_share_media->trip_media_id);
    $media = $trip_media_details->media;
    $trip = $trip_media_details->trip;


   $place = \App\Models\Place\Place::find($trip_media_details->places_id);

   if(is_object($trip)){
        if(isset($trip->users_id) && isset($trip_share_media->user_id) && $trip_share_media->user_id ==$trip->users_id){
            $me = Auth::user();
        }else{
            $me = \App\Models\User\User::find($trip_share_media->user_id);
        }
    }
}
$trip_plan =  \App\Models\TripPlaces\TripPlaces::find($trip_media_details->trip_place_id);
$variable  = $media->id;
$media_comment = @$trip_media_details->comments;
$file_url = $media->url;
$file_url_array = explode(".", $file_url);
$ext = end($file_url_array);
$allowed_video = array('mp4');
$contributer =[];
if(is_object($trip)) $contributer = $trip->contribution_requests->where('status',1)->pluck('users_id')->toArray();
$validPlan = false; 
$contributer[] = @$trip->users_id;

if(is_object($trip) && isset($trip->id) && in_array(Auth::user()->id,$contributer) || in_array(@$trip->id,get_triplist4me()))
    $validPlan =true;



?>
@if(isset($trip_media_details) && isset( $trip) && $me && isset($place) && $validPlan)
    @php
        $uniqueurl = url('trip-media/'. @$trip->author->username, $post->variable);
    @endphp
<div class="post-block post-block-notification" id="trip_media_{{$trip_plan->id}}">
    <!-- New element -->
    <div class="post-top-info-layer">
        <div class="post-info-line">
            <a class="post-name-link" href="{{ url_with_locale('profile/'.$me->id)}}">
            @if( $me->id == Auth::user()->id)    
               You
            @else
                {{$me->name}}
            @endif
            </a> shared a @if(in_array($ext, $allowed_video)) video @else photo @endif from <a
                class="post-name-link" href="{{ url_with_locale('profile/'.@$trip->author->id)}}">
                    @if( $me->id == Auth::user()->id)    
                    Your
                @else
                    {{@$trip->author->name}}'s
                @endif

            </a> trip plan  {{diffForHumans(@$post->time)}}
        </div>
    </div>
    <!-- New element END -->
    <div class="post-top-info-layer">
        <div class="post-top-info-wrap">
            <div class="post-top-avatar-wrap">
                <a class="post-name-link" href="{{ url_with_locale('profile/'.@$trip->author->id)}}">
                    <img src="{{check_profile_picture(@$trip->author->profile_picture)}}" alt="">
                </a>
            </div>
            <div class="post-top-info-txt">
                <div class="post-top-name">
                    <a class="post-name-link" href="{{ url_with_locale('profile/'.@$trip->author->id)}}">{{@$trip->author->name}}</a>{!! get_exp_icon($trip->author) !!}
                </div>
                <div class="post-info">
                     <a href="{{$uniqueurl}}" target="_blank" class="post-title-link">
                    Uploaded a <strong> @if(in_array($ext, $allowed_video)) video @else photo @endif</strong> in <a href="{{ route('place.index', @$place->id) }}" class="link-place">{{ @$place->trans[0]->title}}</a> {{diffForHumans(@$trip_media_details->created_at)}}
                    {{-- <i class="trav-globe-icon"></i> --}}
                    </a>
                </div>
            </div>
        </div>
        <div class="post-top-info-action" dir="auto">
            <div class="dropdown" dir="auto">
                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown" aria-haspopup="true"      aria-expanded="false" dir="auto">
                    <i class="trav-angle-down" dir="auto"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Trip" dir="auto" x-placement="bottom-end"  style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-251px, 31px, 0px);">
                    @if(Auth::user()->id != @$trip->author->id)
                        @if(in_array(@$trip->author->id,$followers))
                            <a  class="dropdown-item follow_unfollow_post_users" data-type="unfollow" data-user_id="{{ $trip->author->id}}" dir="auto">
                                <span class="icon-wrap" dir="auto">
                                    <i class="trav-user-plus-icon" dir="auto"></i>
                                </span>
                                <div class="drop-txt" dir="auto">
                                    <p dir="auto"><b dir="auto">Unfollow {{ $trip->author->name ?? 'User' }}</b></p>
                                    <p dir="auto">Stop seeing posts from {{ $trip->author->name ?? 'User' }}</p>
                                </div>
                            </a>
                        @endif
                    @endif
                    <a class="dropdown-item {{!Auth::check()? 'open-login':''}}" href="#" data-toggle="modal" data-target="#spamReportDlgNew" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Share</b></p>
                            <p dir="auto">Spread the word</p>
                        </div>
                    </a>
                    <a class="dropdown-item" data-text="" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-link" dir="auto"></i>
                        </span>
                        <div class="drop-txt copy-newsfeed-link" data-link="{{$uniqueurl}}">
                            <p dir="auto"><b dir="auto">Copy Link</b></p>
                            <p dir="auto">Paste the link anyywhere you want</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{!Auth::check()? 'open-login':''}}" href="#sendToFriendModal" data-id="trip_media_{{$trip_plan->id}}" data-toggle="modal" data-target="" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-share-on-travo-icon" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                            <p dir="auto">Share with your friends</p>
                        </div>
                    </a>
                    <a class="dropdown-item {{!Auth::check()? 'open-login':''}}" href="#" data-toggle="modal" data-target="#spamReportDlg" onclick="injectData({{$trip_plan->id}},this)" dir="auto">
                        <span class="icon-wrap" dir="auto">
                            <i class="trav-flag-icon-o" dir="auto"></i>
                        </span>
                        <div class="drop-txt" dir="auto">
                            <p dir="auto"><b dir="auto">Report</b></p>
                            <p dir="auto">Help us understand</p>
                        </div>
                    </a>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="post-image-container">
        <!-- New element -->
        <ul class="post-image-list wide" style="margin:0px 0px 1px 0px;">
            @if(in_array($ext, $allowed_video)) 
                <li style="padding: 0;position: relative;margin:0 0 0 0;overflow: hidden;">
                    <video width="100%" height="auto" controls="">
                    <source src="{{@$media->url}}" type="video/mp4">
                    Your browser does not support the video tag.
                    </video>
                    <a href="javascript:;" class="v-play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                </li> 
            @else 
                <li style="padding: 0;position: relative;margin:0 0 0 1px;width: 595px;overflow: hidden;height:297px;">
                    <a href=""
                        data-lightbox="media__post210172">
                            <img src="{{@$media->url}}"  alt="" style="position: absolute;left: 50%;top: 50%;width: auto; -webkit-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);transform: translate(-50%,-50%);">
                    </a>
                </li>
            @endif

        </ul>
        <div class="original-trip-plan">
            @if(isset($place->lat) && isset($place->lng))
                <img alt='Place' src='https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/static/{{$place->lng}},{{@$place->lat}},15,0.0,0/1000x600@2x?access_token=pk.eyJ1IjoiZHh0cmlhbiIsImEiOiJja2Zta3pyZzYwODkzMnZtZTMwZjlqbmhqIn0.BWDQFwk0-Mk6qZw59BgTtQ'>
            @else
                <img src="https://travooo.com/assets2/image/placeholders/place.png" alt="">
            @endif
        </div>
        <!-- New element END -->
    </div>
    @php 
        $variable  = $trip_plan->id;
        $flag_liked  =false;
           if(count($media->likes)>0){
               foreach($trip->likes as $like){
                   if($like->users_id == Auth::user()->id)
                        $flag_liked = true;
               }
           }
    @endphp
    <div class="post-footer-info">
        <!-- Updated elements -->
        <div class="post-foot-block post-reaction">
            <span class="post_like_button @if($flag_liked) liked @endif">
                <a href="#" class="btn-trip-like" data-id="{{$media->id}}" data-type="trip-media"  id="{{$media->id}}">
                    <i class="trav-heart-fill-icon"></i>
                </a>
            </span>
            <span id="trip_like_count_{{$media->id}}" class="trip_place_like_count_{{$media->id}}" >
                <b>{{count($media->likes)}}</b> <a href="#usersWhoLike" data-toggle="modal" data-type="trip-media" data-id="{{$media->id}}">Likes</a>
            </span>
    </div>
        <!-- Updated element END -->
        <div class="post-foot-block">
            <a href="javascript:;" class="comment-btn" data-id="{{$trip_media_details->id}}" data-tab="comments{{$trip_media_details->id}}">
                <i class="trav-comment-icon"></i>
            </a>
            @if(count($media_comment) > 0)
            <ul class="foot-avatar-list">
                @php $us = array(); @endphp
                @foreach($media_comment AS $pc)
                    @if(!in_array($pc->users_id, $us))
                        @php $us[] = $pc->users_id; @endphp
                            @if(count($us)<4)
                            <li><img class="small-ava" src="{{check_profile_picture($pc->author->profile_picture)}}" alt="{{$pc->author->name}}" title="{{$pc->author->name}}"></li>
                            @else
                        breack;
                        @endif
                    @endif
                @endforeach
            </ul>
            @endif
            <span><a href="#" class="comment-btn" data-id="{{$trip_media_details->id}}" data-tab="comments{{$trip_media_details->id}}"><span class="{{$trip_media_details->id}}-comments-count" >{{count($media_comment)}}</span>
                    Comments</a></span>
        </div>
     
    </div>
    <div class="post-comment-wrapper" id="following{{$trip_media_details->id}}">
         @if(count($media_comment)>0)
            @foreach($media_comment AS $comment)
                @if(!user_access_denied($comment->author->id))
                    @php
                    if(!in_array($comment->users_id, $followers))
                        continue;
                    @endphp
                     @include('site.home.partials.new-post_comment_block', ['post_id'=>$trip_media_details->id, 'type'=>'trip-media', 'comment'=>$comment])
                @endif
            @endforeach
        @endif
    </div>
    <div class="post-comment-layer" data-content="comments{{$trip_media_details->id}}"  style='display:none;'>

        <div class="post-comment-top-info">
            <ul class="comment-filter">
                <li onclick="commentSort('Top', this, $(this).closest('.post-block'))">@lang('comment.top')</li>
                <li class="active" onclick="commentSort('New', this, $(this).closest('.post-block'))">@lang('home.new')</li>
            </ul>
            <div class="comm-count-info">
                <strong class="{{$trip_media_details->id}}-opened-comments-count">{{count($media_comment) > 3? 3 : count($media_comment)}}</strong> / <span class="{{$trip_media_details->id}}-comments-count">{{count($media_comment)}}</span>
            </div>
        </div>
        <div class="ss post-comment-wrapper sortBody" id="comments{{$trip_media_details->id}}">
           @if(count($media_comment)>0)
                @foreach($trip_media_details->comments()->take(3)->get() AS $comment)
                    @if(!user_access_denied($comment->author->id))
                        @include('site.home.partials.new-post_comment_block', ['post_id'=>$trip_media_details->id, 'type'=>'trip-media', 'comment'=>$comment])
                    @endif
                @endforeach
            @endif
        </div>
         @if(count($media_comment)>3)
            <a href="javascript:;" class="load-more-comment-link" pagenum="0" comskip="3" data-type="trip-media" data-id="{{$trip_media_details->id}}">Load more...</a>
        @endif

        @include('site.home.partials.new-comment_form_block', ['post_type'=>'trip-media', 'post_id'=>$trip_media_details->id])
    </div>
    <!-- Removed comments block -->
</div>
@endif
