<script src="{{ asset('assets/js/lightslider.min.js') }}"></script>

<script>
    $(document).ready(function(){
        $('body').find('.trip-plan-home-slider{{$mapindex}}').each(function (index) {
            var slider =  $(this).lightSlider({
                autoWidth: true,
                slideMargin: 10,
                pager: false,
                controls: true,
                onBeforeSlide: function (el) {
                    console.log(el.getCurrentSlideCount());
                }
            });
            $('.trip-plan-home-slider{{$mapindex}}prev').click(function(e) { 
                e.preventDefault();
                slider.goToPrevSlide();
            }); 
            $('.trip-plan-home-slider{{$mapindex}}next').click(function(e) { 
                e.preventDefault();
                slider.goToNextSlide(); 
            });
        });
    })
</script>