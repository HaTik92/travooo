<script type="text/javascript">
    $(function () {
        $('.rating_stars').barrating({
            theme: 'fontawesome-stars',
            readonly: true
        });
    });
    $(function () {
        $('.rating_stars_active').barrating({
            theme: 'fontawesome-stars',
            readonly: false
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('#writeReviewForm').submit(function (e) {
            $.ajax({
                method: "POST",
                url: "{{ route('place.ajax_post_review', $place->id) }}",
                data: $('#writeReviewForm').serialize()
            })
                .done(function (res) {
                    res = JSON.parse(res);
                    if (res.success == true) {
                        $('#reviewsContainer').prepend(res.prepend);
                        $('#reviewAddCommentBlock').hide();
                    }

                });
            e.preventDefault(); // avoid to execute the actual submit of the form.

        });


    });

    $(document).ready(function (e) {
        $.ajax({
            method: "POST",
            url: "{{ route('place.check_follow', $place->id) }}",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    $('#followContainer1').html('<a class="btn btn-light-primary button_unfollow" href="#">@lang("place.unfollow")</a>');
                    $('#followContainer2').html('<a class="btn btn-light-primary button_unfollow" href="#">@lang("place.unfollow")</a>');
                } else if (res.success == false) {
                    $('#followContainer1').html('<a class="btn btn-light-primary button_follow" href="#">@lang("place.follow")</a>');
                    $('#followContainer2').html('<a class="btn btn-light-primary button_follow" href="#">@lang("place.follow")</a>');
                }
            });

    });

    $(document).ready(function () {
        $('body').on('click', '.button_follow', function (e) {
            $.ajax({
                method: "POST",
                url: "{{ route('place.follow', $place->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#followContainer1').html('<a class="btn btn-light-primary button_unfollow" href="#">@lang("place.unfollow")</a>');
                        $('#followContainer2').html('<a class="btn btn-light-primary button_unfollow" href="#">@lang("place.unfollow")</a>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
            e.preventDefault();
        });

        $('body').on('click', '.button_unfollow', function (e) {
            $.ajax({
                method: "POST",
                url: "{{ route('place.unfollow', $place->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#followContainer1').html('<a class="btn btn-light-primary button_follow" href="#">@lang("place.follow")</a>');
                        $('#followContainer2').html('<a class="btn btn-light-primary button_follow" href="#">@lang("place.follow")</a>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
            e.preventDefault();
        });
    });


    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{ route('place.now_in_place', $place->id) }}",
            data: {name: "test"}
        })
            .done(function (res) {
                data = JSON.parse(res);
                if (data.success == true) {
                    console.log(data['live_checkins'].length);
                    var someone_is_there = false;

                    var output = '<ul class="city-avatar-list">';

                    for (var i = 0; i < data.live_checkins.length; i++) {
                        var border_style;
                        if(data.live_checkins[i].live==1) {
                            border_style = 'border:2px solid #6ee655;';
                        }
                        else {
                            border_style = 'border:2px solid lightgrey';
                        }
                        output += '<li><a href="#" data-toggle="modal" data-target="#goingPopup"><img class="ava" src="' + data.live_checkins[i]['profile_picture'] + '" style="width:60px;height:60px;'+border_style+'" alt="' + data.live_checkins[i]['name'] + '" title="' + data.live_checkins[i]['name'] + '"></a></li>';
                        someone_is_there = true;
                    }
                    output += '</ul><div class="city-txt"><p>'

                    for (var ii = 0; ii < data.live_checkins.length; ii++) {
                        output += '<a href="#" class="link" data-toggle="modal" data-target="#goingPopup">' + data.live_checkins[ii]['name'] + '</a>, ';
                    }
                    console.log(someone_is_there);
                    if (someone_is_there) {
                        output += ' are now in {{$place->trans[0]->title}}</p></div>';
                        $('#nowInPlace').html(output);
                    }

                    //$('#nowInCountry').html(', <a href="#" class="link">Jeffrey Walters</a> and <a href="#" class="link">2 others</a>');

                }
            });
    });

    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{ route('place.ajax_happening_today', $place->id) }}",
            data: {name: "test"}
        })
            .done(function (res) {
                data = JSON.parse(res);
                console.log(data);
                if (data.success == true) {
                    data = JSON.parse(res);
                    var happening_today = false;

                    var output = '';

                    for (var i = 0; i < data.happenings.length; i++) {
                        output += '<div class="post-happen-block"><div class="happen-info"><div class="img-wrap"><img src="' + data.happenings[i].profile_picture + '" alt="ava" style="width:30px;height:30px;"></div><div class="happen-txt"><a href="/profile/' + data.happenings[i].id + '" class=\"post-name-link\">' + data.happenings[i].name + '</a> <span>@lang("place.posted")</span> <a href=\"/post/' + data.happenings[i].post_id + '\" class=\"photo-link\">@lang("lace.an_update")</a></div></div><div class=\"happen-time\"><span>' + data.happenings[i].date + '</span></div></div>';
                    }
                    output += ''


                    $('#happeningToday').html(output);

                    //$('#nowInCountry').html(', <a href="#" class="link">Jeffrey Walters</a> and <a href="#" class="link">2 others</a>');

                }
            });
    });

    $(document).ready(function () {
        $.ajax({
            method: "POST",
            url: "{{ route('country.check_follow', $place->countries_id) }}",
            data: {name: "test"}
        })
            .done(function (res) {
                if (res.success == true) {
                    $('#country_follow_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_country_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang("place.unfollow")</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    //$('#follow_botton2').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_unfollow2"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.unfollow')<</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                } else if (res.success == false) {
                    $('#country_follow_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_country_follow"><i class="trav-comment-plus-icon"></i><span>@lang("place.follow")</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    //$('#follow_botton2').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow2"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                }
            });

    });

    $(document).ready(function () {

        $('body').on('click', '#button_country_follow', function () {
            $.ajax({
                method: "POST",
                url: "{{  route('country.follow', $place->country->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#country_follow_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_country_unfollow"><i class="trav-comment-plus-icon"></i><span>@lang("place.unfollow")</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });

        $('body').on('click', '#button_country_unfollow', function () {
            $.ajax({
                method: "POST",
                url: "{{ route('country.unfollow', $place->country->id) }}",
                data: {name: "test"}
            })
                .done(function (res) {
                    console.log(res);
                    if (res.success == true) {
                        $('#country_follow_button').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_country_follow"><i class="trav-comment-plus-icon"></i><span>@lang("place.follow")</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    } else if (res.success == false) {
                        //$('#follow_botton').html('<button type="button" class="btn btn-light-grey btn-bordered btn-icon-side btn-icon-right" id="button_follow"><i class="trav-comment-plus-icon"></i><span>@lang('buttons.general.follow')</span><span class="icon-wrap"><i class="trav-view-plan-icon"></i></span></button>');
                    }
                });
        });
    });

</script>

<script>

    $('#placePopupTrigger,#placePopupTrigger2').on('click', function () {

        let $lg = $(this).lightGallery({
            dynamic: true,
            dynamicEl: [
                    @foreach($place->getMedias AS $photo)
                {


                    "src": 'https://s3.amazonaws.com/travooo-images2/th1100/{{$photo->url}}',
                    'thumb': 'https://s3.amazonaws.com/travooo-images2/th180/{{$photo->url}}',
                    'subHtml': `
            <div class='cover-block' style='display:none;'>
              <div class='cover-block-inner comment-block'>
                <ul class="modal-outside-link-list white-bg">
                  <li class="outside-link">
                    <a href="#">
                      <div class="round-icon">
                        <i class="trav-angle-left"></i>
                      </div>
                      <span>@lang("other.back")</span>
                    </a>
                  </li>
                  <li class="outside-link">
                    <a href="#">
                      <div class="round-icon">
                        <i class="trav-flag-icon"></i>
                      </div>
                      <span>@lang("place.report")</span>
                    </a>
                  </li>
                </ul>
                <div class='gallery-comment-wrap'>
                  <div class='gallery-comment-inner mCustomScrollbar'>

                    @if(isset($photo->users[0]) && is_object($photo->users[0]))
                        <div class="top-gallery-content gallery-comment-top">
                          <div class="top-info-layer">
                            <div class="top-avatar-wrap">
@if(isset($photo->users[0]) && is_object($photo->users[0]) && $photo->users[0]->profile_picture!='')
                        <img src="{{url($photo->users[0]->profile_picture)}}" alt="" style="width:50px;hright:50px;">
                        @endif
                        </div>
                        <div class="top-info-txt">
                          <div class="preview-txt">
                        @if(isset($photo->users[0]) && is_object($photo->users[0]) && $photo->users[0]->name!='')
                        <a class="dest-name" href="#">{{$photo->users[0]->name}}</a>
                        @endif
                        <p class="dest-place">@lang("place.uploaded_a") <b>@lang("place.photo")</b> <span class="date">{{$photo->uploaded_at}}</span></p>
                          </div>
                        </div>
                      </div>
                      <div class="gallery-comment-txt">
                        <p>This is an amazing street to walk around and do some shopping</p>
                      </div>
                      <div class="gal-com-footer-info">
                        <div class="post-foot-block post-reaction">
                          <i class="trav-heart-fill-icon"></i>
                          <span><b>185</b></span>
                        </div>
                        <div class="post-foot-block post-comment-place">
                          <i class="trav-location"></i>
                          <span class="place-name">510 LaGuardia Pl, Paris, France</span>
                        </div>
                      </div>
                    </div>
                    @endif
                        <div class="post-comment-layer">
                          <div class="post-comment-top-info">
                            <div class="comm-count-info">
{{@count($photo->comments)}} @lang('comment.comments')
                        </div>
                        <div class="comm-count-info">
                          {{ $loop->iteration }} / {{ $loop->count }}
                        </div>
                      </div>
                      <div class="post-comment-wrapper">
                        @if(isset($photo->comments))
                            @foreach($photo->comments AS $comment)
                        <div class="post-comment-row">
                          <div class="post-com-avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                          </div>
                          <div class="post-comment-text">
                            <div class="post-com-name-layer">
                              <a href="#" class="comment-name">Katherin</a>
                              <a href="#" class="comment-nickname">@katherin</a>
                            </div>
                            <div class="comment-txt">
                              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                            </div>
                            <div class="comment-bottom-info">
                              <div class="com-reaction">
                                <img src="./assets/image/icon-smile.png" alt="">
                                <span>21</span>
                              </div>
                              <div class="com-time">6 hours ago</div>
                            </div>
                          </div>
                        </div>
                            @endforeach
                            @endif
                        </div>
                      </div>
                    </div>
@if(Auth::user())
                        <div class="post-add-comment-block">
                          <div class="avatar-wrap">
                            <img src="{{url(Auth::user()->profile_picture)}}" style="width:45px;height:45px;">
                    </div>
                    <div class="post-add-com-input">
                      <input type="text" name="comment" placeholder="@lang("comment.write_a_comment")">
                    </div>
                    <input type="hidden" name="medias_id" value="{{$photo->id}}" />
                    <input type="hidden" name="users_id" value="{{Auth::user()->id}}" />
                  </div>
                  @endif
                        </div>
                      </div>
                    </div>
`
                },

                    @endforeach
                {
                    test
                }
            ],
            addClass: 'main-gallery-block',
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
            $.each(galleryItem, function (i, val) {
                // itemArr.push(val);
            });

        });
        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });
        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');
            let currentCommentIs = $(subTtl).hasClass('comment-block');
            let currentImgPos = $(currentImg).position().top;
            setTimeout(function () {
                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                if (currentCommentIs) {
                    // console.log('yes');
                    $(currentImgWrap).css('padding-right', commentWidth);
                    $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    $(subTtl).css('width', currentWidth);
                }
                $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();
            }, 500);
        }

        $lg.on('onSlideItemLoad.lg', function (e) {

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            })
        });
        $lg.on('onAfterSlide.lg', function () {
            setWidth();
        });
    });


    // pages of places popup
    $('#nearByPlacesTrigger').on('click', function () {

        let $lg = $(this).lightGallery({
            dynamic: true,
            dynamicEl: [
                    @foreach($places_nearby AS $np)

                {
                    "src": '{{check_place_photo($np)}}',
                    'thumb': '{{check_place_photo($np)}}',
                    'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner comment-block'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-angle-left"></i>
                  </div>
                  <span>@lang("other.back")</span>
                </a>
              </li>
            </ul>
            <div class="top-gallery-content">
              <div class="sub-post-info">
                <ul class="sub-list">
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-popularity-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">n/a</div>
                      <div class="sub-txt">@lang("place.popularity")</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-safety-big-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">n/a</div>
                      <div class="sub-txt">@lang("place.safety")</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-user-rating-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">{{$np->rating}}/5</div>
                      <div class="sub-txt">@lang("place.user_rating")</div>
                    </div>
                  </li>
                </ul>
                <div class="follow-btn-wrap">

                </div>
              </div>
            </div>
            <div class="map-preview">
              <img src="https://maps.googleapis.com/maps/api/staticmap?maptype=satellite&center={{$np->lat}},{{$np->lng}}&markers=color:red%7Clabel:C%7C{{$np->lat}},{{$np->lng}}&zoom=13&size=150x150&key={{env('GOOGLE_MAPS_KEY')}}&language=en" alt="map">
            </div>
            <div class='gallery-comment-wrap'>
              <div class='gallery-comment-inner mCustomScrollbar'>
                <div class="top-gallery-content gallery-comment-top">
                  <div class="top-info-layer">
                    <div class="top-avatar-wrap">

                    </div>
                    <div class="top-info-txt">
                      <div class="preview-txt">
                        <p class="dest-name"><a href="{{ route('place.index', $np->id) }}">{{$np->trans[0]->title}}</a></p>
                        <p class="dest-place">{{do_placetype($np->place_type)}} - {{$np->trans[0]->address}}</p>
                      </div>
                    </div>
                  </div>
                  <div class="gal-com-footer-info">
                    <div class="post-foot-block post-reaction">
                      <span><b>{{count($np->reviews)}}</b> @lang("place.reviews")</span>
                    </div>
                  </div>
                </div>
                <div class="post-comment-layer">
                  <div class="post-comment-top-info">
                    <div class="comm-count-info">
                      0 @lang("place.reviews")
                    </div>
                    <div class="comm-count-info">

                    </div>
                  </div>
                  <div class="post-comment-wrapper">


                  </div>
                </div>
              </div>
              <div class="post-add-comment-block">

              </div>
            </div>
          </div>
        </div>`
                },

                @endforeach
            ],
            addClass: 'main-gallery-block',
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
            $.each(galleryItem, function (i, val) {
                // itemArr.push(val);
            });
            $.each(galleryThumb, function (i, val) {
                // thumbArr.push(val);
                // let startCnt = `<div class="thumb-txt"><i class="trav-flag-icon"></i> start</div>`;
                // let startCntEmpty = `<div class="thumb-txt">&nbsp;</div>`;
                // let placetxt = 'rabar-sale airport'
                // let placeName = `<div class="thumb-txt">${placetxt}</div>`;
                // if(i == 0){
                //   $(val).addClass('place-thumb');
                //   $(val).append(placeName).prepend(startCnt);
                // }
                // if(i == 2){
                //   $(val).addClass('place-thumb');
                //   $(val).append(placeName).prepend(startCntEmpty);
                // }
            });
        });
        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });
        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');
            let currentCommentIs = $(subTtl).hasClass('comment-block');
            let currentImgPos = $(currentImg).position().top;
            setTimeout(function () {
                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                if (currentCommentIs) {
                    // console.log('yes');
                    $(currentImgWrap).css('padding-right', commentWidth);
                    $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    $(subTtl).css('width', currentWidth);
                }
                $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();
            }, 500);
        }

        $lg.on('onSlideItemLoad.lg', function (e) {

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            })
        });
        $lg.on('onAfterSlide.lg', function () {
            setWidth();
        });
    });


    // nearby events
    $('#nearByEventsTrigger').on('click', function () {

        let $lg = $(this).lightGallery({
            dynamic: true,
            dynamicEl: [
                    @foreach($events AS $ev)
                    <?php
                    $rand = rand(1, 10);
                    ?>
                {
                    "src": "{{asset('assets2/image/events/'.$ev->category.'/'.$rand.'.jpg')}}",
                    'thumb': "{{asset('assets2/image/events/'.$ev->category.'/'.$rand.'.jpg')}}",
                    'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner comment-block'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-angle-left"></i>
                  </div>
                  <span>@lang("other.back")</span>
                </a>
              </li>
            </ul>
            <div class="top-gallery-content">
              <div class="sub-post-info">

                <div class="follow-btn-wrap">

                </div>
              </div>
            </div>
            <div class="map-preview">
              <img src="https://maps.googleapis.com/maps/api/staticmap?maptype=satellite&center={{$ev->location[1]}},{{$ev->location[0]}}&markers=color:red%7Clabel:C%7C{{$ev->location[1]}},{{$ev->location[0]}}&zoom=13&size=150x150&key={{env('GOOGLE_MAPS_KEY')}}" alt="map">
            </div>
            <div class='gallery-comment-wrap'>
              <div class='gallery-comment-inner mCustomScrollbar'>
                <div class="top-gallery-content gallery-comment-top">
                  <div class="top-info-layer">
                    <div class="top-avatar-wrap">

                    </div>
                    <div class="top-info-txt">
                      <div class="preview-txt">
                        <p class="dest-name">{{$ev->title}}</p>
                        <p class="dest-place">
                                {{ucfirst($ev->category)}}
                        <br />@lang("time.from_to", ['from' => $ev->start, 'to' => $ev->end])</p>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="post-comment-layer">

                  <div class="post-comment-wrapper" style="color: black;padding: 20px;line-height: 1.3;">
                    {!!nl2br($ev->description)!!}
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>`
                },

                @endforeach
            ],
            addClass: 'main-gallery-block',
            pager: false,
            hideControlOnEnd: true,
            loop: false,
            slideEndAnimatoin: false,
            thumbnail: true,
            toogleThumb: false,
            thumbHeight: 100,
            thumbMargin: 20,
            thumbContHeight: 180,
            actualSize: false,
            zoom: false,
            autoplayControls: false,
            fullScreen: false,
            download: false,
            counter: false,
            mousewheel: false,
            appendSubHtmlTo: 'lg-item',
            prevHtml: '<i class="trav-angle-left"></i>',
            nextHtml: '<i class="trav-angle-right"></i>',
            hideBarsDelay: 100000000
        });

        $lg.on('onAfterOpen.lg', function () {
            $('body').css('overflow', 'hidden');
            let itemArr = [], thumbArr = [];
            let galleryBlock = $('.main-gallery-block');
            let galleryItem = $(galleryBlock).find('.lg-item');
            let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
            $.each(galleryItem, function (i, val) {
                // itemArr.push(val);
            });
            $.each(galleryThumb, function (i, val) {
                // thumbArr.push(val);
                // let startCnt = `<div class="thumb-txt"><i class="trav-flag-icon"></i> start</div>`;
                // let startCntEmpty = `<div class="thumb-txt">&nbsp;</div>`;
                // let placetxt = 'rabar-sale airport'
                // let placeName = `<div class="thumb-txt">${placetxt}</div>`;
                // if(i == 0){
                //   $(val).addClass('place-thumb');
                //   $(val).append(placeName).prepend(startCnt);
                // }
                // if(i == 2){
                //   $(val).addClass('place-thumb');
                //   $(val).append(placeName).prepend(startCntEmpty);
                // }
            });
        });
        $lg.on('onBeforeClose.lg', function () {
            $('body').removeAttr('style');
        });
        let setWidth = function () {
            let mainBlock = $('.main-gallery-block');
            let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
            let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

            let slide = $('.main-gallery-block .lg-item');
            let currentItem = $('.main-gallery-block .lg-current');
            let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
            let currentImg = $('.main-gallery-block .lg-current .lg-image');
            let currentCommentIs = $(subTtl).hasClass('comment-block');
            let currentImgPos = $(currentImg).position().top;
            setTimeout(function () {
                let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
                let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
                if (currentCommentIs) {
                    // console.log('yes');
                    $(currentImgWrap).css('padding-right', commentWidth);
                    $(subTtl).css('width', currentWidth + commentWidth);
                } else {
                    $(currentImgWrap).removeAttr('style');
                    $(subTtl).css('width', currentWidth);
                }
                $(subTtlWrp).show();
                $('.mCustomScrollbar').mCustomScrollbar();
            }, 500);
        }

        $lg.on('onSlideItemLoad.lg', function (e) {

            setWidth();
            $(window).on('resize', function () {
                setWidth();
            })
        });
        $lg.on('onAfterSlide.lg', function () {
            setWidth();
        });
    });
</script>

<script>

    //595x360
    var map;

    function initMap() {
        map = new google.maps.Map(document.getElementById('happeningTodayMap'), {
            zoom: 16,
            center: new google.maps.LatLng({{$place->lat}}, {{$place->lng}}),
            mapTypeId: 'roadmap'
        });


        function HTMLMarker(lat, lng, img) {
            this.lat = lat;
            this.lng = lng;
            this.img = img;
            this.pos = new google.maps.LatLng(lat, lng);
        }

        HTMLMarker.prototype = new google.maps.OverlayView();
        HTMLMarker.prototype.onRemove = function () {
        }

        //init your html element here


        HTMLMarker.prototype.onAdd = function () {
            div = document.createElement('DIV');
            div.className = "dest-img-block";
            div.innerHTML = "<img class='dest-img' src='" + this.img + "' alt='' style='width:30px;height:30px;'><div class='icon-wrap'><ul class='icon-list'><li><i class='trav-comment-icon'></i></li></ul></div>";
            var panes = this.getPanes();
            panes.overlayImage.appendChild(div);
        }

        HTMLMarker.prototype.draw = function () {
            var overlayProjection = this.getProjection();
            var position = overlayProjection.fromLatLngToDivPixel(this.pos);
            var panes = this.getPanes();
            panes.overlayImage.style.left = position.x + 'px';
            panes.overlayImage.style.top = position.y - 30 + 'px';
        }

        //to use it
                @foreach($checkins AS $checkin)
        var htmlMarker = new HTMLMarker({{$checkin->place->lat}}, {{$checkin->place->lng}}, '{{check_profile_picture($checkin->post_checkin->post->author->profile_picture)}}');
        htmlMarker.setMap(map);
        @endforeach


    }
</script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&callback=initMap&language=en">
</script>
