<!-- write-review popup -->
<div class="modal fade white-style" data-backdrop="false" id="writeReviewPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-650" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-modal-review">
                <div class="review-main-block">
                    <div class="review-img-wrap">
                        <img class="main-photo" src="@if(isset($place->getMedias[0]->url)) https://s3.amazonaws.com/travooo-images2/{{$place->getMedias[0]->url}} @else asset('assets2/image/placeholders/pattern.png') @endif" alt="photo" style='width:595px;height:205px;'>
                        <div class="review-logo-wrap">

                        </div>
                    </div>
                    <div class="review-place-block">
                        <h3 class="place-title">{{$place->trans[0]->title}}</h3>
                        <p class="sub-txt">{{do_placetype($place->place_type)}} @lang('place.in') {{$place->trans[0]->address}}</p>
                        <div class="com-star-block">

                            <select class="rating_stars">
                                <option value="1" @if(round($reviews_avg)==1) selected @endif>1</option>
                                <option value="2" @if(round($reviews_avg)==2) selected @endif>2</option>
                                <option value="3" @if(round($reviews_avg)==3) selected @endif>3</option>
                                <option value="4" @if(round($reviews_avg)==4) selected @endif>4</option>
                                <option value="5" @if(round($reviews_avg)==5) selected @endif>5</option>
                            </select>
                        </div>
                        <a href="#" class="write-review-link" id="writeReviewLink">@lang('place.write_a_review') <i
                                    class="fa fa-pencil"></i></a>
                    </div>
                    <div class="review-add-comment-block" id="reviewAddCommentBlock" style="display:none;">
                        <form method="post" id="writeReviewForm">
                        <div class="post-add-comment-block">
                            <div class="avatar-wrap">
                                <img src="{{check_profile_picture(Auth::guard('user')->user()->profile_picture)}}" alt="" style="width:45px;height:45px;">
                            </div>
                            <div class="post-add-com-input">
                                <input type="text" name="text" placeholder="@lang('place.write_a_review')">
                            </div>
                        </div>
                        
                        <div class="review-foot-block">
                            <div class="com-star-block">
                                <select class="rating_stars_active" name="score" id="score">
                                            <option value="1" >1</option>
                                            <option value="2" >2</option>
                                            <option value="3" >3</option>
                                            <option value="4" >4</option>
                                            <option value="5" >5</option>
                                        </select>
                                <div class="com-add-btn-wrap">
                                    <button class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                                    <button class="btn btn-light-primary">@lang('buttons.general.post')</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div> 
                </div>
                <div class="post-footer-info">
                    
                    <div class="post-foot-block">
                        <i class="trav-star-circle-icon icon-grey"></i>
                        <span>{{count($reviews)}} @lang('place.reviews')</span>
                    </div>
                </div>
                <div class="post-comment-layer">
                    <div class="post-comment-top-info">
                        <ul class="comment-filter">
                            <li class="active">@lang('comment.top')</li>
                           <!-- <li>New</li>
                            <li>Worse</li>-->
                        </ul>
                        <div class="comm-count-info">
                            <!--3 / 20-->
                        </div>
                    </div>
                    <div class="post-comment-wrapper" id="reviewsContainer">
                        @foreach($reviews AS $review)
                        <div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="{{$review->google_profile_photo_url}}" alt="" style="width:45px;height:45px;">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">{{$review->google_author_name}}</a>
                                    <a href="#" class="comment-nickname"></a>
                                </div>
                                <div class="comment-txt">
                                    <p>{{$review->text}}</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-star-block">
                                        <select class="rating_stars">
                                            <option value="1" @if($review->score==1) selected @endif>1</option>
                                            <option value="2" @if($review->score==2) selected @endif>2</option>
                                            <option value="3" @if($review->score==3) selected @endif>3</option>
                                            <option value="4" @if($review->score==4) selected @endif>4</option>
                                            <option value="5" @if($review->score==5) selected @endif>5</option>
                                        </select>
                                        <span class="count">
                                            <b>{{$review->score}}</b> / 5
                                        </span>
                                    </div>
                                    <div class="dot">·</div>
                                    <div class="com-time">{{diffForHumans(date("Y-m-d", $review->google_time))}}</div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>