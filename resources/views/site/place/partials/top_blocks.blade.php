<div class="top-board-wrap">
    @if(count($place->getMedias))

        <div class="post-block post-board">
            <div class="post-side-top">
                <h3 class="side-ttl">@lang('place.photos')</h3>
                <div class="side-right-control">
                    <div class="side-count">{{count($place->getMedias)}}</div>
                </div>
            </div>
            <div class="post-side-inner" id="placePopupTrigger" style="cursor:pointer;">
                <div class="board-photo-list">
                    @for ($i = 0; $i < 3; $i++)
                        @if ($i==0)
                            <div class="board-image full-image">
                                <img src="https://s3.amazonaws.com/travooo-images2/th230/{{@$place->getMedias[$i]->url}}"
                                     alt="photo" style="width:185px;height:115px;">
                            </div>
                        @else
                            <div class="board-image half-image">
                                <img src="https://s3.amazonaws.com/travooo-images2/th230/{{@$place->getMedias[$i]->url}}"
                                     alt="photo" style="width:84px;height:89px;">
                            </div>
                        @endif
                    @endfor
                </div>
            </div>
        </div>
    @endif
    <div class="post-block post-board">
        <div class="post-side-top">
           <h3 class="side-ttl">@lang('place.plans')</h3>
           <div class="side-right-control">
                <div class="side-count">{{count($place->versions)}}</div>
            </div>
        </div>
        <div class="post-side-inner">
            <div class="board-photo-list">
                @if(count($place->versions))
                    @foreach($place->versions()->take(3)->get() AS $pv)
                        @if ($loop->first)
                            <div class="board-image full-image">
                                <img src="{{get_plan_map($pv->trip, 185, 115, $pv->trip->active_version)}}" alt="photo"
                                     style="width:185px;height:115px;">
                            </div>
                        @else
                            <div class="board-image half-image">
                                <img src="{{get_plan_map($pv->trip, 84, 89, $pv->trip->active_version)}}" alt="photo"
                                     style="width:84px;height:89px;">
                            </div>
                        @endif
                    @endforeach
                @else
                   @lang('place.no_plans')
               @endif
            </div>
        </div>
    </div>
    @if(count($places_nearby))
       <div class="post-block post-board">
            <div class="post-side-top">
                <h3 class="side-ttl">@lang('place.nearby_places')</h3>
                <div class="side-right-control">
                    <div class="side-count">{{count($places_nearby)}}</div>
                </div>
           </div>
            <div class="post-side-inner" id="nearByPlacesTrigger" style="cursor:pointer;">
                <div class="board-photo-list">
                    @foreach($places_nearby->slice(0, 3) AS $np)
                        @if ($loop->first)
                            <div class="board-image full-image">
                                <img src="{{check_place_photo($np)}}" alt="photo" style="width:185px;height:115px;">
                            </div>
                        @else
                            <div class="board-image half-image">
                                <img src="{{check_place_photo($np)}}" alt="photo" style="width:84px;height:89px;">
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    @if(count($events))
       <div class="post-block post-board">
            <div class="post-side-top">
                <h3 class="side-ttl">@lang('place.nearby_events')</h3>
                <div class="side-right-control">

                </div>
            </div>
            <div class="post-side-inner" id="nearByEventsTrigger" style="cursor:pointer;">
                <div class="board-photo-list">
                    <?php
                    $ev = array_slice($events, 0, 3);
                    ?>
                    @foreach($ev AS $event)

                        <?php
                        $rand = rand(1, 10);
                        ?>

                        @if ($loop->first)
                            <div class="board-image full-image">
                                <img src="{{asset('assets2/image/events/'.$event->category.'/'.$rand.'.jpg')}}"
                                     alt="photo" style="width:185px;height:115px;">
                            </div>
                        @else
                            <div class="board-image half-image">
                                <img src="{{asset('assets2/image/events/'.$event->category.'/'.$rand.'.jpg')}}"
                                     alt="photo" style="width:84px;height:89px;">
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    
</div>