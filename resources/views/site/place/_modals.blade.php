<!-- Modals -->

@include('site/place/partials/write-review-popup')
<!-- about comment popup -->
<div class="modal fade white-style" data-backdrop="false" id="aboutCommentPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-650" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-modal-comment">
                <div class="post-comment-head">
                    <div class="c-head-txt-wrap">
                        <div class="c-head-txt">
                            <div class="dest-name">
                                <span>Disneyland</span>
                                <span class="block-tag">@lang('place.park')</span>
                            </div>
                        </div>
                    </div>
                    <div class="c-head-btn-wrap">
                        <button type="button" class="btn btn-light-primary">
                            @lang('place.follow')
                        </button>
                    </div>
                </div>
                <div class="post-comment-main">
                    <div class="dest-pic">
                        <img src="http://placehold.it/590x320" alt="">
                    </div>
                    <div class="discussion-block">
                        <span class="disc-ttl">@lang('place.discussion')</span>
                        <span class="disc-count">129</span>
                        <ul class="disc-ava-list">
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="post-comment-layer">
                    <div class="post-comment-top-info">
                        <ul class="comment-filter">
                            <li class="active">@lang('comment.top')</li>
                            <li>@lang('comment.new')</li>
                        </ul>
                        <div class="comm-count-info">
                            3 / 20
                        </div>
                    </div>
                    <div class="post-comment-wrapper">
                        <div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="http://placehold.it/45x45" alt="">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">Katherin</a>
                                    <a href="#" class="comment-nickname">@katherin</a>
                                </div>
                                <div class="comment-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore
                                        tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-reaction">
                                        <img src="./assets/image/icon-smile.png" alt="">
                                        <span>21</span>
                                    </div>
                                    <div class="com-time">6 hours ago</div>
                                </div>
                            </div>
                        </div>
                        <div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="http://placehold.it/45x45" alt="">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">Amine</a>
                                    <a href="#" class="comment-nickname">@ak0117</a>
                                </div>
                                <div class="comment-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-reaction">
                                        <img src="./assets/image/icon-like.png" alt="">
                                        <span>19</span>
                                    </div>
                                    <div class="com-time">6 hours ago</div>
                                </div>
                            </div>
                        </div>
                        <div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="http://placehold.it/45x45" alt="">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">Katherin</a>
                                    <a href="#" class="comment-nickname">@katherin</a>
                                </div>
                                <div class="comment-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore
                                        tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-reaction">
                                        <img src="./assets/image/icon-smile.png" alt="">
                                        <span>15</span>
                                    </div>
                                    <div class="com-time">6 hours ago</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="load-more-link">@lang('buttons.general.load_more')...</a>
                    </div>
                    <div class="post-add-comment-block post-add-foot">
                        <div class="avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-add-com-input">
                            <input type="text" placeholder="@lang('comment.write_a_comment')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- place one day popup -->
<div class="modal fade white-style" data-backdrop="false" id="placeOneDayPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="story-bottom-slider-wrapper">
        <div class="bottom-slider" id="storiesModeSlider">
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Suzanne</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('place.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Lonnie Nelson</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('place.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Barbara Hafer</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('place.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">David</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('place.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Suzanne</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('place.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Barbara Hafer</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('place.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">David</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('place.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Suzanne</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.visited_places') <b>6 km</b></li>
                            <li>@lang('place.spent') <b>$ 610</b></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-dialog modal-custom-style modal-1140 bottom-slider-height" role="document">
        <ul class="modal-outside-link-list white-bg">
            <li class="outside-link">
                <a href="#">
                    <div class="round-icon">
                        <i class="trav-flag-icon"></i>
                    </div>
                    <span>@lang('place.report')</span>
                </a>
            </li>
        </ul>
        <div class="modal-custom-block">
            <div class="post-block post-place-plan-block">

                <div class="post-image-container post-follow-container mCustomScrollbar">
                    <div class="post-image-inner">
                        <div class="post-map-wrap">
                            <div class="post-place-top-info">
                                <a href="#" class="time-link">@lang('time.day_value', ['value' => 2]) </a>
                                <span>@lang('place.of_the_trip_plan')</span>
                                <a href="#"
                                   class="place-link">@lang('place.visiting_place_for_a_day', ['place' => 'California'])</a>
                            </div>
                            <img src="./assets/image/trip-plan-image.jpg" alt="map">
                            <div class="destination-point" style="top:80px;left: 145px;">
                                <img class="map-marker" src="./assets/image/marker.png" alt="marker">
                            </div>
                            <div class="post-map-info-caption map-blue">
                                <div class="map-avatar">
                                    <img src="http://placehold.it/25x25" alt="ava">
                                </div>
                                <div class="map-label-txt">
                                    @lang('place.checking_on') <b>2 Sep</b> at <b>8:30
                                        am</b> @lang('place.and_will_stay') <b>30 min</b>
                                </div>
                            </div>
                        </div>
                        <div class="post-destination-block slide-dest-hide-right-margin">
                            <div class="post-dest-slider" id="postDestSliderInner4">
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">Grififth</div>
                                            <div class="dest-count">@lang('place.observatory')</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">Hearst Castle</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">SeaWorld San</div>
                                            <div class="dest-count">Diego</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">United Arab Emirates</div>
                                            <div class="dest-count">@choice('trip.count_destination', 2, ['count' => 2])</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">SeaWorld San</div>
                                            <div class="dest-count">Diego</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">United Arab Emirates</div>
                                            <div class="dest-count">@choice('trip.count_destination', 2, ['count' => 2])</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="modal-place-block">

                </div> -->
                <div class="gallery-comment-wrap" id="galleryCommentWrap">
                    <div class="gallery-comment-inner mCustomScrollbar">
                        <div class="gallery-comment-top">
                            <div class="top-info-layer">
                                <div class="top-avatar-wrap">
                                    <img src="http://placehold.it/50x50" alt="">
                                </div>
                                <div class="top-info-txt">
                                    <div class="preview-txt">
                                        <b>@lang('chat.by')</b>
                                        <a class="dest-name" href="#">Elijah Hughes</a>
                                        <p class="dest-date">30 Aug 2017 at 10:00 pm</p>
                                    </div>
                                </div>
                            </div>
                            <div class="gal-com-footer-info">
                                <div class="post-foot-block post-reaction">
                                    <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                                    <span><b>2</b> @lang('place.reactions')</span>
                                </div>
                            </div>
                        </div>
                        <div class="post-comment-layer">
                            <div class="post-comment-top-info">
                                <div class="comm-count-info">
                                    @choice('comment.count_comment', 5, ['count' => 5])
                                </div>
                                <div class="comm-count-info">
                                    3 / 20
                                </div>
                            </div>
                            <div class="post-comment-wrapper">
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                delectus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>21</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Amine</a>
                                            <a href="#" class="comment-nickname">@ak0117</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                doloribus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-like.png" alt="">
                                                <span>19</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                delectus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>15</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="load-more-link">@lang('buttons.general.load_more')...</a>
                            </div>
                        </div>
                    </div>
                    <div class="post-add-comment-block">
                        <div class="avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-add-com-input">
                            <input type="text" placeholder="@lang('comment.write_a_comment')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- place stories popup -->
<div class="modal fade white-style" data-backdrop="false" id="storiesModePopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="story-bottom-slider-wrapper">
        <div class="bottom-slider" id="storiesModeSlider">
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Suzanne</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('place.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Lonnie Nelson</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('place.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Barbara Hafer</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('place.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">David</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('place.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Suzanne</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('place.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Barbara Hafer</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('place.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">David</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('place.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="story-slide">
                <div class="story-slide-inner">
                    <div class="img-wrap">
                        <img src="http://placehold.it/42x42" alt="ava" class="slide-ava">
                    </div>
                    <div class="slide-txt">
                        <a href="#" class="name-link">Suzanne</a>
                        <ul class="slider-info-list">
                            <li>@lang('place.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></li>
                            <li>@lang('place.spent') <b>$ 310</b></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-dialog modal-custom-style modal-bottom-slider modal-1070" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-story-block">
                <div class="modal-story-top">
                    <div class="top-txt">
                        <span>@lang('place.this_is') <b>@lang('time.day_value', ['value' => 2])</b> @lang('place.of_the_trip_plan')</span>
                        <a class="post-link" href="#">@lang('place.what_to_see_in_place', ['place' => 'California'])</a>
                        <i class="trav-open-video-in-window"></i>
                    </div>
                </div>
                <div class="story-content-block">
                    <div class="place-over">
                        <div class="place-over_top">
                            <div class="place-over-txt">
                                <b>@lang('place.name_airport', ['name' => 'Dubai international'])</b>
                                <i class="trav-set-location-icon"></i>
                                <span class="place-txt">@lang('place.airport_in_place', ['place' => 'Dubai, United Arab Emirates'])</span>
                            </div>
                            <div class="follow-btn-wrap">
                                <button type="button" class="btn btn-white-bordered">@lang('place.follow')</button>
                            </div>
                        </div>
                        <div class="trip-photos">
                            <div class="photo-wrap">
                                <img src="http://placehold.it/356x365" alt="photo">
                            </div>
                            <div class="photo-wrap">
                                <img src="http://placehold.it/356x365/f5f5f5" alt="photo">
                            </div>
                            <div class="photo-wrap">
                                <img src="http://placehold.it/356x365" alt="photo">
                            </div>
                            <a href="#" class="see-more-link">@lang('place.see_more_photos')</a>
                        </div>
                    </div>
                    <div class="story-content-inner">
                        <div class="story-date-layer">
                            <div class="follow-avatar">
                                <img src="http://placehold.it/68x68" alt="avatar">
                            </div>
                            <div class="follow-by">
                                <span>@lang('chat.by')</span>
                                <b>Suzanne</b>
                            </div>
                            <div class="s-date-line">
                                <div class="s-date-badge">@lang('time.day') 1 / 7</div>
                            </div>
                            <div class="s-date-line">
                                <div class="s-date-badge">Mon, 18 Jun 2017</div>
                            </div>
                            <div class="s-date-line">
                                <div class="s-date-badge">@ 10:00am</div>
                            </div>
                        </div>
                        <div class="info-block">
                            <div class="info-top-layer">
                                <ul class="info-list">
                                    <li>
                                        <i class="trav-clock-icon"></i><span
                                                class="info-txt"> @lang('place.stayed') <b>@choice('time.count_hours', 2, ['count' => '1.2'])</b></span>
                                    </li>
                                    <li>
                                        <i class="trav-budget-icon"></i><span class="info-txt"> @lang('place.spent') <b>$310</b></span>
                                    </li>
                                    <li>
                                        <i class="trav-weather-cloud-icon"></i><span
                                                class="info-txt"> @lang('place.weather') <b>12° C</b></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="info-content">
                                <span class="good-label">@lang('place.good_for'):</span>
                                <ul class="block-tag-list">
                                    <li><span class="block-tag">@lang('place.food')</span></li>
                                    <li><span class="block-tag">@lang('place.relaxation')</span></li>
                                    <li><span class="block-tag">@lang('place.shopping')</span></li>
                                    <li><span class="block-tag">@lang('place.photography')</span></li>
                                </ul>
                            </div>
                            <div class="info-bottom-layer">
                                <p>“It was an amazing experience, I’m happy I selected this place to be in my trip”</p>
                            </div>
                        </div>
                        <div class="info-main-layer">
                            <p>Cras ac tortor orci a liquam fermentum leo faucibus tellus blandit dapibus ras convallis
                                placerat est, tempor volutpat urna sempernec.</p>
                            <h3>Headline Here</h3>
                            <p>Consectetur adipiscing elit nam consequat ligula non mi rutrum suscipit cras ac tortor
                                orci.</p>
                            <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est, tempor
                                volutpat urna sempernec.</p>
                            <ul class="img-list">
                                <li><img src="http://placehold.it/700x460" alt="image"></li>
                            </ul>
                            <p>Liquam fermentum leo faucibus tellus blandit dapibus ras convallis placerat est, tempor
                                volutpat urna sempernec.</p>
                            <div class="flex-image-wrap fh-460">
                                <div class="image-column col-66">
                                    <div class="image-inner"
                                         style="background-image:url(http://placehold.it/475x460)"></div>
                                </div>
                                <div class="image-column col-33">
                                    <div class="image-inner img-h-66"
                                         style="background-image:url(http://placehold.it/225x310)"></div>
                                    <div class="image-inner img-h-33"
                                         style="background-image:url(http://placehold.it/225x150)"></div>
                                </div>
                            </div>
                        </div>
                        <div class="post-tips">
                            <div class="post-tips-top">
                                <h4 class="post-tips_ttl">@lang('place.tips_about_place', ['place' => 'Dubai Airport'])</h4>
                                <a href="#" class="see-all-tip">@lang('trip.see_all_tips_count', ['count' => '(7)'])</a>
                            </div>
                            <div class="post-tips_block-wrap">
                                <div class="post-tips_block">
                                    <div class="tips-content">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                            architecto vel magnam accusamus nemo odio unde autem, provident eos itaque
                                            quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                    </div>
                                    <div class="tips-footer">
                                        <div class="tip-by-info">
                                            <div class="tip-by-img">
                                                <img src="http://placehold.it/25x25" alt="">
                                            </div>
                                            <div class="tip-by-txt">
                                                @lang('chat.by') <span class="tag blue">Suzanne</span> <span
                                                        class="date-info">5 days ago</span>
                                            </div>
                                        </div>
                                        <ul class="tip-right-info-list">
                                            <li class="round">
                                                <i class="trav-bookmark-icon"></i>
                                            </li>
                                            <li class="round">
                                                <i class="trav-flag-icon"></i>
                                            </li>
                                            <li class="round blue">
                                                <i class="trav-chevron-up"></i>
                                            </li>
                                            <li class="count">
                                                <span>6</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="foot-reaction">
                            <ul class="post-round-info-list">
                                <li>
                                    <div class="map-reaction">
                      <span class="dropdown">
                        <i class="trav-heart-fill-icon" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false"></i>
                        <div class="dropdown-menu dropdown-menu-middle-right dropdown-arrow dropdown-emoji-wrap">
                          <ul class="dropdown-emoji-list">
                            <li>
                              <a href="#" class="emoji-link">
                                <div class="emoji-wrap like-icon"></div>
                                <span class="emoji-name">@lang('place.like')</span>
                              </a>
                            </li>
                            <li>
                              <a href="#" class="emoji-link">
                                <div class="emoji-wrap haha-icon"></div>
                                <span class="emoji-name">@lang('place.haha')</span>
                              </a>
                            </li>
                            <li>
                              <a href="#" class="emoji-link">
                                <div class="emoji-wrap wow-icon"></div>
                                <span class="emoji-name">@lang('place.wow')</span>
                              </a>
                            </li>
                            <li>
                              <a href="#" class="emoji-link">
                                <div class="emoji-wrap sad-icon"></div>
                                <span class="emoji-name">@lang('place.sad')</span>
                              </a>
                            </li>
                            <li>
                              <a href="#" class="emoji-link">
                                <div class="emoji-wrap angry-icon"></div>
                                <span class="emoji-name">@lang('place.angry')</span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </span>
                                        <span class="count">6</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="round-icon">
                                        <i class="trav-comment-icon"></i>
                                    </div>
                                    <span class="count">3</span>
                                </li>
                                <li>
                                    <div class="round-icon">
                                        <i class="trav-light-icon"></i>
                                    </div>
                                    <span class="count">37</span>
                                </li>
                                <li>
                                    <a href="#" class="info-link">
                                        <div class="round-icon">
                                            <i class="trav-share-fill-icon"></i>
                                        </div>
                                        <span>@lang('place.share')</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="post-comment-layer-wrap">
                        <div class="post-comment-layer">
                            <div class="post-comment-top-info">
                                <ul class="comment-filter">
                                    <li class="active">@lang('comment.top')</li>
                                    <li>@lang('comment.new')</li>
                                </ul>
                                <div class="comm-count-info">
                                    3 / 20
                                </div>
                            </div>
                            <div class="post-comment-wrapper">
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>21</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Amine</a>
                                            <a href="#" class="comment-nickname">@ak0117</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                doloribus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-like.png" alt="">
                                                <span>19</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                delectus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>15</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="load-more-link">@lang('buttons.general.load_more')...</a>
                            </div>
                            <div class="post-add-comment-block">
                                <div class="avatar-wrap">
                                    <img src="http://placehold.it/45x45" alt="">
                                </div>
                                <div class="post-add-com-input">
                                    <input type="text" placeholder="@lang('comment.write_a_comment')">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- happen question popup -->
<div class="modal fade white-style" data-backdrop="false" id="happenQuestionPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-850" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-happen-question">
                <div class="post-top-info-layer">
                    <div class="post-top-info-wrap">
                        <div class="post-top-avatar-wrap">
                            <img src="http://placehold.it/50x50" alt="">
                        </div>
                        <div class="post-top-info-txt">
                            <div class="post-top-name">
                                <a class="post-name-link" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">Michael Powell</a>
                            </div>
                            <div class="post-info">
                                @lang('place.asked_a_question')
                            </div>
                        </div>
                    </div>
                    <div class="post-top-info-action">
                        <a class="post-collapse" href="#">
                            <i class="trav-angle-down"></i>
                        </a>
                    </div>
                </div>
                <div class="post-content-inner">
                    <div class="post-main-content">
                        <h3>What is the best part in Disneyland?</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi aliquid rem dolore
                            explicabo exercitationem. Porro recusandae reiciendis labore aliquam laborum?</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam doloribus maxime soluta,
                            deserunt ipsam quia dicta, ex dolor rem quibusdam laborum, quidem eos dolorem eum dolore
                            saepe facere aperiam iure.</p>

                        <div class="post-modal-content-foot">
                            <div class="post-modal-foot-list">
                                <span>Today at <b>5:26 pm</b></span>
                                <span class="dot"> · </span>
                                <span>@lang('other.views') <b>931</b></span>
                                <span class="dot"> · </span>
                                <span>@lang('place.follow') <b>27</b></span>
                                <ul class="disc-ava-list">
                                    <li><a href="#"><img src="http://placehold.it/25x25" alt="avatar"></a></li>
                                    <li><a href="#"><img src="http://placehold.it/25x25" alt="avatar"></a></li>
                                    <li><a href="#"><img src="http://placehold.it/25x25" alt="avatar"></a></li>
                                    <li><a href="#"><img src="http://placehold.it/25x25" alt="avatar"></a></li>
                                    <li><a href="#"><img src="http://placehold.it/25x25" alt="avatar"></a></li>
                                    <li><a href="#"><img src="http://placehold.it/25x25" alt="avatar"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="post-add-comment-block">
                        <div class="avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-add-com-input">
                            <input type="text" placeholder="@lang('comment.write_a_comment')">
                        </div>
                    </div>
                    <div class="post-tips-top-layer">
                        <div class="post-tips-top">
                            <h4 class="post-tips-ttl">@lang('place.top_tips')</h4>
                            <a href="#" class="top-first-link">@lang('place.top_first') <i class="trav-caret-down"></i></a>
                        </div>
                        <div class="post-tips-main-block">
                            <div class="post-tips-row">
                                <div class="tips-top">
                                    <div class="tip-avatar">
                                        <img src="http://placehold.it/25x25" alt="">
                                    </div>
                                    <div class="tip-content">
                                        <div class="top-content-top">
                                            <a href="#" class="name-link">Suzanne</a>
                                            <span>@lang('place.said')</span><span
                                                    class="dot"> · </span> <span>6 hours ago</span>
                                        </div>
                                        <div class="tip-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                                architecto vel magnam accusamus nemo odio unde autem, provident eos
                                                itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tips-footer">
                                    <a href="#" class="upvote-link">
                                        <span class="arrow-icon-wrap"><i class="trav-chevron-up"></i></span>
                                        <span><b>13</b> @lang('place.upvotes')</span>
                                    </a>
                                </div>
                            </div>
                            <div class="post-tips-row">
                                <div class="tips-top">
                                    <div class="tip-avatar">
                                        <img src="http://placehold.it/25x25" alt="">
                                    </div>
                                    <div class="tip-content">
                                        <div class="top-content-top">
                                            <a href="#" class="name-link">Suzanne</a>
                                            <span>@lang('place.said')</span><span
                                                    class="dot"> · </span> <span>6 hours ago</span>
                                        </div>
                                        <div class="tip-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                                architecto vel magnam accusamus nemo odio unde autem, provident eos
                                                itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tips-footer">
                                    <a href="#" class="upvote-link disabled">
                                        <span class="arrow-icon-wrap"><i class="trav-chevron-up"></i></span>
                                        <span><b>13</b> @lang('place.upvotes')</span>
                                    </a>
                                </div>
                            </div>
                            <div class="post-tips-row">
                                <div class="tips-top">
                                    <div class="tip-avatar">
                                        <img src="http://placehold.it/25x25" alt="">
                                    </div>
                                    <div class="tip-content">
                                        <div class="top-content-top">
                                            <a href="#" class="name-link">Suzanne</a>
                                            <span>@lang('place.said')</span><span
                                                    class="dot"> · </span> <span>6 hours ago</span>
                                        </div>
                                        <div class="tip-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit eveniet,
                                                architecto vel magnam accusamus nemo odio unde autem, provident eos
                                                itaque quas at, vitae ducimus. Earum magnam quod a tempore.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tips-footer">
                                    <a href="#" class="upvote-link">
                                        <span class="arrow-icon-wrap"><i class="trav-chevron-up"></i></span>
                                        <span><b>13</b> @lang('place.upvotes')</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- your friends popup -->
<div class="modal fade white-style" data-backdrop="false" id="goingPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-700" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-going-block post-mobile-full">
                <div class="post-top-topic topic-tabs">
                    <div class="topic-item active">@lang('place.your_friends') <span
                                class="count">{{count($checkins)}}</span></div>
                    <div class="topic-item">@lang('place.experts_users') <span class="count">{{count($checkins)}}</span>
                    </div>
                </div>
                <div class="post-people-block-wrap mCustomScrollbar">
                    @foreach($checkins AS $checkin)
                        <div class="people-row">
                            <div class="main-info-layer">
                                <div class="img-wrap">
                                    <a href='{{url('profile/'.$checkin->post_checkin->post->author->id)}}'>
                                        <img class="ava"
                                             src="{{check_profile_picture($checkin->post_checkin->post->author->profile_picture)}}"
                                             alt="ava" style="width:50px;height:50px;">
                                    </a>
                                </div>
                                <div class="txt-block">
                                    <div class="name">
                                        <a href='{{url('profile/'.$checkin->post_checkin->post->author->id)}}'>
                                            {{$checkin->post_checkin->post->author->name}}
                                            {!! get_exp_icon($checkin->post_checkin->post->author) !!}
                                        </a>
                                    </div>
                                    <div class="info-line">
                                        <div class="info-part">@lang('place.checked_in') -
                                            <b>{{$checkin->post_checkin->post->created_at}}</b></div>
                                    </div>
                                </div>
                            </div>
                            <div class="button-wrap">
                                <button class="btn btn-light-grey btn-bordered"
                                        onclick='window.location.replace("https://www.travooo.com/chat/new/{{$checkin->post_checkin->post->author->id}}");'>
                                    @lang('place.message')
                                </button>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
