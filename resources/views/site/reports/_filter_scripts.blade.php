<script data-cfasync="false" type="text/javascript">

//------------------- START Filters Script ---------------------

var order = 'popular';
var type = 'all';
var searched_text = '';
var repots_filters = {};
var filter_cities = [];
var filter_countries = [];
var locations = [];
var dest_type = [];

    // Mobile layout
    $('.mobile-filter-toggler').on('click', function() {
        $(this).siblings('.post-collapse-block').toggleClass('show')
    })


    $('body').on('DOMNodeInserted', '#travelog_orderbox', function () {
        $(this).find("#remove_temp_order").remove();
    });


    //Top slider click filter action
    $(document).on('click', '.rep-location-item', function (e) {
        var location_id = $(this).attr('data-loc_id');
        var location_type = $(this).attr('data-loctype');
        var location_name = $(this).find('.dest-name').text();

        $('.report-loc-filter-block').hide();
        $('#travelMateAccordion').find('.travlogSelected').removeClass('current-tab')

        filter_cities = [];
        filter_countries = [];
        locations = [];
        dest_type = [];

        if(location_type === 'city'){
            if ($.inArray(location_id, filter_cities) == -1) {
                filter_cities.push(location_id)
                repots_filters.cities = filter_cities;

                $('#travlogSelectCities').addClass('current-tab').find('input[type=radio]').prop("checked", true);
                $('.filter-city-block').html('').append(getSelectedHtml(location_name, 'close-city-filter', 'city-id', location_id));
                $('.filter-country-block').html('')
                $('#reportsCities').show()
            }
        }else if(location_type === 'country'){
            if ($.inArray(location_id, filter_countries) == -1) {
                filter_countries.push(location_id)
                repots_filters.countries = filter_countries;

                $('#travlogSelectCountries').addClass('current-tab').find('input[type=radio]').prop("checked", true);
                $('.filter-country-block').html('').append(getSelectedHtml(location_name, 'close-country-filter', 'country-id', location_id));
                $('.filter-city-block').html('')
                $('#reportsCountries').show()
            }
        }

        if ($.inArray(location_name, dest_type) == -1) {
            dest_type.push(location_name)
        }

        repots_filters.cities = filter_cities;
        repots_filters.countries = filter_countries;

        if ($.inArray(locations, location_name) == -1) {
            locations.push(location_name)
        }

        showSearchResult(searched_text, dest_type.join(', '))
        getReportFilter();

    });

    function handleRepType(elem) {
        var $travelogOrderTab = $('#travelog_orderTab');
        var $travelogOrder = $('#travelog_order');
        var travelogOrderValues = [];

        $travelogOrder.find('option').each(function () {
            if (this.value === 'draft') {
                return;
            }
            travelogOrderValues.push(this.value);
        });

        elem.closest('.rep-type-block').find('.rep-type-item').removeClass('current-tab');
        elem.addClass('current-tab');
        type = elem.attr('reptype');


        if(type == 'mine'){
            if($travelogOrder.find('[value="draft"]').length == 0){
                $travelogOrder.append('<option value="draft"> My Drafts</option>');
                $travelogOrderTab.append('\n' +
                    '                        <li class="tab-list-item" role="presentation">\n' +
                    '                            <a href="#draft" class="tab" role="tab" id="tabdraft" data-attr="tab">\n' +
                    '                                <span class="tab-text text-truncate">My Drafts</span>\n' +
                    '                            </a>\n' +
                    '                        </li>');
            }
        }else{
            $travelogOrder.find('[value="draft"]').remove();
            $travelogOrderTab.find('#tabdraft').parent().remove();

            if (!travelogOrderValues.includes(order)) {
                order = travelogOrderValues[0];
            }

            $('#report_pagination').removeClass('d-none')
            $('#report_loader').show()
        }
    }

    //Select report type
    $(document).on('click', '.rep-type-item', function() {
        var rep_type = $(this).attr('reptype')
        if (rep_type == 'mine'){
            @if(Auth::check())
                handleRepType($(this));
                getReportFilter();

                localStorage.setItem('report_filters', JSON.stringify({filters: repots_filters, order: order, type: type, dest_type: dest_type}));
            @else
                $('#signUp').modal('show')
            @endif
        }else{
            handleRepType($(this));
            getReportFilter();

            localStorage.setItem('report_filters', JSON.stringify({filters: repots_filters, order: order, type: type, dest_type: dest_type}));
        }
    })


    //Select Destination
    $(document).on('click', '#destination_searchbox .dest-searched-result', function (){
        var id = $(this).data('select-id')
        var text = $(this).data('select-text')

        if (id != '') {
            var destination_type = id.split(',')
            $('.report-loc-filter-block').hide();
            $('#travelMateAccordion').find('.travlogSelected').removeClass('current-tab')
            var destination_id = parseInt(destination_type[0])

            filter_cities = [];
            filter_countries = [];
            dest_type = [];
            locations = [];

            if ($.inArray(locations, text) == -1) {
                locations.push(text)
            }

            if(destination_type[1] === 'city'){
                if ($.inArray(destination_id, filter_cities) == -1) {
                    filter_cities.push(destination_id)
                    repots_filters.cities = filter_cities;
                    repots_filters.countries = filter_countries;

                    $('#travlogSelectCities').addClass('current-tab').find('input[type=radio]').prop("checked", true);
                    $('.filter-city-block').html('').append(getSelectedHtml(text, 'close-city-filter', 'city-id', destination_id));
                    $('#reportsCities').show()
                }
            }else if(destination_type[1] === 'country'){
                if ($.inArray(destination_id, filter_countries) == -1) {
                    filter_countries.push(destination_id)
                    repots_filters.countries = filter_countries;
                    repots_filters.cities = filter_cities;

                    $('#travlogSelectCountries').addClass('current-tab').find('input[type=radio]').prop("checked", true);
                    $('.filter-country-block').html('').append(getSelectedHtml(text, 'close-country-filter', 'country-id', destination_id));
                    $('#reportsCountries').show()
                }
            }

            if ($.inArray(text, dest_type) == -1) {
                dest_type.push(text)
            }

            showSearchResult(searched_text, dest_type.join(', '))
            getReportFilter();
        }

        $('#destination_searchbox').find('.dest-search-result-block').html('')
        $('#destination_searchbox').find('.dest-search-result-block').removeClass('open')
        $('#destination_search').val(text)
    });

    //Select order
    $('#travelog_order').select2({
        minimumResultsForSearch: Infinity,
                placeholder: '@lang('report.newest')',
        debug: true,
                containerCssClass: "travelog-select2-input",
        }
    );

    // Tabs order selection
    $('#travelog_orderTab').on('click', '[data-attr="tab"]', function (e) {
        e.preventDefault();
        var data = $(this).attr('href');

        if(!data) return;

        order = data.toString().replace(/#/g, '');

        $('[data-attr="tab"]').removeClass('active');
        $(this).addClass('active');

        getReportFilter();
        localStorage.setItem('report_filters', JSON.stringify({filters: repots_filters, order: order, type: type, dest_type: dest_type}));
    });

    $('#travelog_order').on('select2:select', function (e) {
        var data = e.params.data;
        if (data.id != '') {
            order = data.id;

            getReportFilter();
            localStorage.setItem('report_filters', JSON.stringify({filters: repots_filters, order: order, type: type, dest_type: dest_type}));
        }
    });

    //Clear search result
    $(document).on('click', '.clear-search', function () {
        $('#search_travelog_input').val('')
        $(this).closest('.rep-search-result-block').html('')

        $('.filter-country-block').html('')
        $('.filter-city-block').html('')
        $('#travelMateAccordion').find('.travlogSelected').removeClass('current-tab')
        $('#travlogSelectAll').addClass('current-tab').find('input[type=radio]').prop("checked", true);
        $('.report-loc-filter-block').hide();

        $("#destination_search").val('').change();

        filter_cities = [];
        filter_countries = [];
        dest_type = []
        locations = [];
        repots_filters = {};
        searched_text = '';

        localStorage.removeItem("report_filters");

        getReportFilter();
    })

    $(document).ready(function(){
        loadFirstReports();

        //Search destination
        $('#destination_search').keyup(delay(function (e) {
            let value = $(e.target).val();
            getDestinationSearch(value, $(this), '#destination_searchbox', 'dest-searched-result');
        }, 500));

        $(document).click(function(event) {
            var $target = $(event.target);
            if(!$target.closest('.dest-searchbox').length && $('.dest-searchbox').is(":visible")) {
                $('.dest-search-result-block').html('')
                $('.dest-search-result-block').removeClass('open')
            }
        });

        //Search report by keyboards
        $(document).on('keyup', '#search_travelog_input', delay(function () {
            searched_text = $(this).val();
            repots_filters.search_text = searched_text;

            getReportFilter();
            showSearchResult(searched_text, dest_type.join(', '))
        }, 500))
    })

    //LHS destination select
    $(document).on('click', '.rep-selected-block', function (e) {
        $('.clear-search').click();

        $('.filter-city-block').html('')
        $('.filter-country-block').html('')
        filter_countries = [];
        filter_cities = [];

        repots_filters.cities = filter_cities;
        repots_filters.countries = filter_countries;

        var loc_type = $(this).attr('ftype');

        $('#travelMateAccordion').find('.travlogSelected').removeClass('current-tab')

        $(this).find('input[type=radio]').prop("checked", true);
        $('.report-loc-filter-block').hide();
        $(this).addClass('current-tab')

        $('#' + loc_type).show()

    });

    //filter by country
    $('#filter_country').autocomplete({
        source: function (request, response) {
            jQuery.get("{{url('reports/ajaxSearchLocation')}}", {
                q: request.term
            }, function (data) {
                if (data) {
                    var items = [];
                    data = JSON.parse(data);

                    if (data.length > 0) {
                        data.forEach(function (s_item, index) {
                            if (s_item.type == '') {
                                items.push({
                                    value: s_item.text,
                                    image: s_item.image,
                                    query: s_item.query,
                                    id: s_item.id,
                                    country_name: s_item.type
                                });
                            }
                        });
                        response(items);
                    } else {
                        var result = [
                            {
                                label: 'No result found',
                                value: '',
                                country_name: ' '
                            }
                        ];
                        response(result);
                    }
                }
            });
        },
        classes: {"ui-autocomplete": "report-filter-autocomplate"},
        focus: function (event, ui) {
            return false;
        },
        select: function (event, ui) {
            $('#filter_country').val('');
             var destination_type = ui.item.id.split('-')
            if ($.inArray(parseInt(destination_type[0]), filter_countries) == -1) {
                filter_countries.push(destination_type[0])
                repots_filters.countries = filter_countries;

                if ($.inArray(ui.item.value, dest_type) == -1) {
                    dest_type.push(ui.item.value)
                    showSearchResult(searched_text, dest_type.join(', '))
                }

                if ($.inArray(locations, ui.item.value) == -1) {
                    locations.push(ui.item.value)
                }

                getReportFilter();
                $('.filter-country-block').append(getSelectedHtml(ui.item.value, 'close-country-filter', 'country-id', destination_type[0]));
            }
            return false;
        },
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
               .append(getFilterMarkupHtml(item))
               .appendTo(ul);
    };

    //Close filtered country
    $(document).on('click', '.close-country-filter', function(){
        var country_id = $(this).attr('data-country-id');
        var country_title = $(this).parent().find('.filter-item-title').text();
        filter_countries = $.grep(filter_countries, function (value) {
            return value != country_id;
        });

        dest_type = $.grep(dest_type, function (title) {
            return title != country_title;
        });

        locations = $.grep(locations, function (title) {
            return title != country_title;
        });

        repots_filters.countries = filter_countries;
        repots_filters.locations = locations;

        showSearchResult(searched_text, dest_type.join(', '))
        getReportFilter();

        $(this).closest('li').remove()
    });


    //Filter by city
    $('#filter_city').autocomplete({
        source: function (request, response) {
            jQuery.get("{{url('reports/ajaxSearchLocation')}}", {
                q: request.term
            }, function (data) {
                if(data) {
                    var items = [];
                    data = JSON.parse(data);

                    if (data.length > 0) {
                        data.forEach(function (s_item, index) {
                            if (s_item.type != '') {
                                items.push({
                                    value: s_item.text,
                                    image: s_item.image,
                                    query: s_item.query,
                                    id: s_item.id,
                                    country_name: s_item.type
                                });
                            }
                        });
                        response(items);
                    } else {
                        var result = [
                            {
                                label: 'No result found',
                                value: '',
                                country_name: ' '
                            }
                        ];
                        response(result);
                    }
                }
            });
        },
        classes: {"ui-autocomplete": "report-filter-autocomplate"},
        focus: function (event, ui) {
            return false;
        },
        select: function (event, ui) {
            $('#filter_city').val('');
            var destination_type = ui.item.id.split('-')
            if ($.inArray(parseInt(destination_type[0]), filter_cities) == -1) {
                filter_cities.push(destination_type[0])
                repots_filters.cities = filter_cities;

                if ($.inArray(ui.item.value, dest_type) == -1) {
                    dest_type.push(ui.item.value)
                    showSearchResult(searched_text, dest_type.join(', '))
                }

                if ($.inArray(locations, ui.item.value) == -1) {
                    locations.push(ui.item.value)
                }

                getReportFilter();
                $('.filter-city-block').append(getSelectedHtml(ui.item.value, 'close-city-filter', 'city-id', destination_type[0]));
            }
            return false;
        },
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
               .append(getFilterMarkupHtml(item))
               .appendTo(ul);
    };

    //Close filtered city
    $(document).on('click', '.close-city-filter', function(){
        var city_id = $(this).attr('data-city-id');
        var city_title = $(this).parent().find('.filter-item-title').text();

        filter_cities = $.grep(filter_cities, function (value) {
            return value != city_id;
        });

        dest_type = $.grep(dest_type, function (title) {
            return title != city_title;
        });

        locations = $.grep(locations, function (title) {
            return title != city_title;
        });

        repots_filters.cities = filter_cities;
        repots_filters.locations = locations;

        showSearchResult(searched_text, dest_type.join(', '))
        getReportFilter();

        $(this).closest('li').remove()
    });



    //Get report filter
    function getReportFilter() {
        $('#report_list').html(loaderHtml);
        $('#report_pagination').addClass('d-none')
        $('#pagenum').val(1)
        if(locations.length > 0){
            repots_filters.locations = locations;
        }


        $.ajax({
            type: 'POST',
            url: "{{route('reports.get_filter')}}",
            data: {filters: repots_filters, order: order, type: type},
            success: function (data) {
                if (data.view != '') {
                    $('#report_list').html(data.view);

                    if ($('#report_list').find('.travlog-info-block').length < 3) {
                        $('#report_loader').hide();
                    }

                    $('#report_pagination').removeClass('d-none')
                } else {
                    $('#report_list').html('<div class="no-report-block">No reports..</div>')
                }
                $('.rep-counts').html(data.count)
            }
        });
    }


// load more reports
    var check_view = true;
    $(window).on('scroll', function(){
        check_scrolling = Math.floor($(document).height() - $(document).scrollTop() - $(window).height());

        if( $('#report_loader').length > 0 && ( check_scrolling <2500) && check_view && ($('#report_loader').css('display') != 'none')){
            check_view = false;
            var nextPage = parseInt($('#pagenum').val())+1;
            var url = '{{url("reports/updatelist")}}';

            $.ajax({
                type: 'GET',
                url: url,
                data: {pagenum: nextPage, filters: repots_filters, order: order, type: type},
                success: function(data){
                    check_view = true;
                    if(data.view){
                        if(data.trending_view){
                            setTimeout(function () {
                                new_slider =  $(".reportTrendingDescription").last().lightSlider({
                                    autoWidth: true,
                                    pager: false,
                                    controls: false,
                                    slideMargin: 18,
                                    addClass: 'rep-slider-wrap',
                                    loop: false,
                                    onSliderLoad: function (el) {
                                        $(el).closest('.report-trending-dest-block').find('.report-slide-prev').bind('click', function(){
                                            el.goToPrevSlide();
                                        })
                                        $(el).closest('.report-trending-dest-block').find('.report-slide-next').bind('click', function(){
                                            el.goToNextSlide();
                                        })
                                        $('.loadReportDestAnimation').addClass('d-none')
                                        $('.reportTrendingDescription').removeClass('d-none')
                                        $(el).height(275);
                                        $(el).width(2500);
                                    }, });
                            }, 1000);
                        }

                        $('#report_list').append(data.view);
                        $('#pagenum').val(nextPage);

                    } else {
                        $("#report_loader").hide();
                    }
                }
            });

        }
    })



function loadFirstReports(){
    let defaultFilters = {filters: [], order: 'popular', type: 'all'};
    let localStorageFilters = JSON.parse(localStorage.getItem('report_filters'));
    let data =  localStorageFilters ? localStorageFilters : defaultFilters;

    if(localStorageFilters && (localStorageFilters.dest_type.length > 0 || (localStorageFilters.filters.length > 0 && localStorageFilters.filters.search_text != ''))){

        dest_type = localStorageFilters.dest_type;
        search_text = (typeof localStorageFilters.filters.search_text !== "undefined") ? localStorageFilters.filters.search_text : '';

        showSearchResult(search_text, dest_type.join(', '))
    }


    $('#pagenum').val(1);

    $.ajax({
        type: 'POST',
        url: "{{route('reports.get_filter')}}",
        data: data,
        success: function (data) {
            if (data.view != '') {
                $('#report_list').html(data.view);

                $('#report_pagination').removeClass('d-none')
            } else {
                $('#report_list').html('<div class="no-report-block">No reports..</div>')
            }
            $('.rep-counts').html(data.count)
        }
    });
}

   //Get filter loader
    function loaderHtml() {
        return `<div class="report-animation-content travlog-info-inner">
                        <div class="info-wrapper">
                            <div class="report-top-info-layer">
                               <div class="report-animation-top-text animation"></div>
                            </div>
                            <div class="report-animation-conteiner">
                                <div class="report-animation-text animation w-75"></div>
                                <div class="report-animation-text animation"></div>
                            </div>
                            <div class="report-animation-footer">
                                 <div class="report-animation-wrap">
                                    <div class="report-footer-avatar-wrap animation"></div>
                                    <div class="report-animation-info-txt">
                                        <div class="report-animation-footer-name animation"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="img-wrap">
                                <div class="report-animation-img animation"></div>
                        </div>
                    </div>`;
    }

    //Show search report result
    function showSearchResult(search_type, destination_title){
        console.log('search_type', search_type)
        var search_type_text = '';
        if(search_type != '' && destination_title == ''){
            search_type_text = '<b>' + search_type + '</b>';
        }else if(destination_title != '' && search_type == ''){
            search_type_text = '<b>' + destination_title + '</b>';
        }else {
            search_type_text = '<span><b>' + search_type + '</b> in <b>' + destination_title + '</b></span>';
        }

        var search_text ='<span class="text-nowrap">Results for</span>'+ search_type_text;

        if(search_type !='' || destination_title !=''){
            $('.rep-search-result-block').html('<div class="post-block post-top-bordered">' +
                   search_text +
                   '<span class="clear-search"><i class="fa fa-times" aria-hidden="true"></i></span>' +
                   '</div>')
        }else{
           $('.rep-search-result-block').html('');
        }
    }

    function getFilterMarkupHtml(response) {
        if (response) {
            var markup = '<div class="search-country-block">';
            if (response.image) {
                markup += '<div class="search-country-imag"><img src="' + response.image + '" /></div>';
            }
            markup += '<div class="span10">' +
                    '<div class="search-country-info">' +
                    '<div class="search-country-text">' + markLocetionMatch(response.value, response.query) + '</div>';
            if (response.country_name != null && response.country_name.trim() !='') {
                markup += '<div class="search-country-type">City in ' + response.country_name + '</div>';
            } else if(response.country_name != null && response.country_name =='') {
                markup += '<div class="search-country-type">Country</div>';
            }
            markup += '</div></div>';

            return markup;
        }
    }

    function getSelectedHtml(title, class_name, data_attr, id) {
        var html = '<li><span class="filter-item-title">' + title + '</span>' +
                '<span class="close-filter-item ' + class_name + '" data-' + data_attr + '="' + id + '"> <i class="trav-close-icon"></i></span></li>'

        return html;
}



//------------------- END Filters Script ---------------------
</script>

