@extends('site.layouts.site')
@php $title = 'Travooo - Reports'; @endphp
@section('after_styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{url('assets2/js/jquery-loading-master/dist/jquery.loading.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
<link href="{{asset('/dist-parcel/travlogs-web-upd.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .select2-input {
        /* height:49px !important; */
        padding:8px !important;
        border: 1px solid #e6e6e6 !important;
    }  
    .select2-container .select2-selection--single {
        height:49px !important;
    }
    .select2-selection__choice {
        background: #f7f7f7 !important;
    padding: 5px 10px !important;
    position: relative !important;
    border-radius: 0 !important;
        color: #4080ff !important;
        margin-top:2px !important;
    }
    .ui-state-highlight {
        height:25px;
        background-color:#c4d9ff;
    }
    em {
        font-style: italic;
    }
    .modal-custom-style .post-block.post-travlog.post-create-travlog .main-content .create-row .side
    {
        position: unset;
    }
    .ql-container
    {
        height: auto;
    }
    .ql-snow .ql-editor a
    {
        color: #06c;
        text-decoration: underline;
    }
   
    .travlogSelected {
        display: inline-block;
        position: relative;
        padding: 0 0px;
        
    }

    .travlogSelected input[type='radio'] {
        display: none;
    }

    .travlogSelected label {
        direction: ltr;
        color: #666;
        font-weight: normal;
    }  
    .current-tab #trav-log-icon{
        color:#4080ff;
        font-size: 24px;
    }
    #trav-log-icon {
        color: #b2b2b2;
        font-size: 24px;
    }
    .travlogSelected label:before {
        content: " ";
        display: inline-block;
        position: relative;
        top: 5px;
        margin: 0 15px 0 0;
        width: 19px;
        height: 19px;
        border-radius: 11px;
        border: 1px solid #4080ff;
        background-color: transparent;
    }

    .travlogSelected input[type=radio]:checked + label:after {
        border-radius: 11px;
        width: 9px;
        height: 9px;
        position: absolute;
        top: 10px;
        left: 5px;
        content: " ";
        display: block;
        background: #4080ff;
    }
</style>

<link href="{{asset('/dist-parcel/reports-upd.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('/dist-parcel/assets/Travelog/re-polishing/aea-2029.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('dist-parcel/assets/Travelog/main.css') }}" rel="stylesheet"/>
@endsection

@section('content-area')
@if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
@include('site.layouts._left-menu')
@include('site.home.partials._spam_dialog')
<div class="report-offline-status">
    <div class="rep-ofline-block"></div>
    No internet Connection...
</div>
<div class="post-block post-flight-style report-top-slide">

    <div class="post-side-inner rep-top-slider-inner">
        <div class="rep-top-slider-info">
            <i class="trav-trending-destination-icon"></i>
            <span class="rep-slider-text">@lang('report.trending_location')</span>
        </div>
        <div class="post-slide-wrap post-destination-block">
            <div class="lSSlideOuter post-dest-slider-wrap">
                <div class="lSSlideWrapper usingCss">
                    <ul id="reportHeaderSlider" class="post-slider lightSlider lsGrab lSSlide"
                        style="width: 3700px;/*transform: translate3d(-1110px, 0px, 0px); height: 153px;*/ padding-bottom: 0%;">
                        @foreach($top_locations AS $tc)
                        <li class="clone left country-images rep-location-item" style="margin-right: 20px;" data-loctype="{{$tc['type']}}" data-loc_id="{{$tc['ob']->id}}">
                            <div class="post-dest-inner">
                                <div class="img-wrap report-top-slide-img">
                                    <img src="{{@$tc['ob']->getMedias[0]->url?'https://s3.amazonaws.com/travooo-images2/'.@$tc['ob']->getMedias[0]->url:asset('assets2/image/placeholders/place.png')}}" alt="{{$tc['ob']->transsingle->title}}" style='width:165px;height:150px'>
                                    <div class="dest-name">{{$tc['ob']->transsingle->title}}</div>
                                </div>
                            </div>
                            <div class="rp-cities-count">
                                <span class="rp-count">{{$tc['report_count']}}</span>
                                <span class="rp-text">{{$tc['report_count']>1?'Reports':'Report'}}</span>
                            </div>
                        </li>
                        @endforeach
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="custom-row report-custom-row">
    <!-- MAIN-CONTENT -->
    <div class="main-content-layer">
        <div class="post-block post-trav-header-block">
            <div class="post-side-top padding-10 hidden-less-sm">
                <div class="col-md-5 pr-0 search-trav-input-wrapper" id="travelog_searchbox">
                    <i class="trav-search-icon"></i>
                    <input type="text" class="search-travelog-input" id="search_travelog_input" placeholder="Search in travelogs...">
                </div>
                <div class="col-md-4 dest-searchbox" id="destination_searchbox">
                    <input type="search" class="search-destination-input" id="destination_search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="@lang('discussion.strings.place_city_country')">
                    <span class="dest-search-result-block dest-search" style="left: 15px !important;"></span>
                </div>
                <div class="col-md-3 pl-0" id="travelog_orderbox">
                    <span class="select2 select2-container select2-container--default" id="remove_temp_order" style="width: 100%;">
                        <span class="selection">
                            <span class="select2-selection select2-selection--single travelog-select2-input">
                                <span class="select2-selection__rendered">
                                    <span class="select2-selection__placeholder">@lang('report.newest')</span>
                                </span>
                                <span class="select2-selection__arrow">
                                    <b></b>
                                </span>
                            </span>
                        </span>
                        <span class="dropdown-wrapper"></span>
                    </span>
                    <select  id="travelog_order"
                             placeholder="@lang('report.most_popular')"
                             style="width: 100%;display:none">
                        <option value="popular">@lang('report.most_popular')</option>
                        <option value="newest">@lang('report.newest')</option>
                        <option value="oldest"> @lang('report.oldest')</option>
                    </select>
                </div>
            </div>
        </div>
         <div class="rep-search-result-block">
        </div>
        <div class="post-block post-tab-block" style="margin-bottom:0px;">
            <div class="post-tab-inner" id="postTabBlock">
                <div class="rep-count-block">
                    <div class="rep-count-wrap">
                        <div class="title">Travelogs</div>
                        <div class="rep-counts text-truncate"></div>
                    </div>
                </div>
                <nav class="tab-container hidden-more-sm">
                    <ul class="tab-list list-style" id="travelog_orderTab" role="tablist" data-attr="tablist">
                        <li class="tab-list-item" role="presentation">
                            <a href="#popular" class="tab active" role="tab" id="tabpopular" data-attr="tab">
                                <span class="tab-text text-truncate">@lang('report.most_popular')</span>
                            </a>
                        </li>
                        <li class="tab-list-item" role="presentation">
                            <a href="#newest" class="tab" role="tab" id="tabnewest" data-attr="tab">
                                <span class="tab-text text-truncate">@lang('report.newest')</span>
                            </a>
                        </li>
                        <li class="tab-list-item" role="presentation">
                            <a href="#oldest" class="tab" role="tab" id="taboldest" data-attr="tab">
                                <span class="tab-text text-truncate">@lang('report.oldest')</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="more-travlog-block" style="background-color:white">
                <div class="travlog-list" id="report_list" style="margin:0px;">
                    </div>
                    <div id='report_pagination' style='text-align:center;'>
                    <input type="hidden" id="pagenum" value="1">
                    <div id="report_loader" class="report-animation-content travlog-info-inner">
                        <div class="info-wrapper">
                            <div class="report-top-info-layer">
                               <div class="report-animation-top-text animation"></div>
                            </div>
                            <div class="report-animation-conteiner">
                                <div class="report-animation-text animation w-75"></div>
                                <div class="report-animation-text animation"></div>
                            </div>
                            <div class="report-animation-footer">
                                 <div class="report-animation-wrap">
                                    <div class="report-footer-avatar-wrap animation"></div>
                                    <div class="report-animation-info-txt">
                                        <div class="report-animation-footer-name animation"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="img-wrap">
                                <div class="report-animation-img animation"></div>
                        </div>
                    </div>
                </div>
                
            <div class="bottom-load-mark">
                <div class="text-xs-center" style="display:-webkit-inline-box;">
                </div>
            </div>
            
            
        </div>

    </div>

    <!-- SIDEBAR -->
    <div class="sidebar-layer" id="sidebarLayer" style="top: 70px;">
        <aside class="sidebar">

            <div class="post-block post-side-block hidden-less-sm">
                <div class="post-side-inner">
                    @if(Auth::check())
                        <button type="button" class="btn btn-light-primary btn-bordered btn-full" id="create_new_report" data-toggle="modal" data-target="#createTravlogPopup">
                            Create a Travelog
                        </button>
                    @else
                        <button type="button" class="btn btn-light-primary btn-bordered btn-full open-login">Create a Travelog</button>
                    @endif
                </div>
            </div>
            <form method='get' action='' autocomplete="off">
                <div class="post-block post-side-block hidden-less-sm">
                    <div class="post-side-trip-inner">
                        <div class="side-trip-tab rep-type-block">
                            <div class="trip-tab-block rep-type-item current-tab" reptype="all">
                                <i id="trav-log-icon" class="fa fa-newspaper-o"></i>
                                <div class="trip-tab-ttl">@lang('other.all')</div>
                            </div>
                            <div class="trip-tab-block rep-type-item" reptype="mine">
                                <i id="trav-log-icon" class="fa fa-newspaper-o"></i>
                                <div class="trip-tab-ttl">My Travelogs</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-block-group hidden-more-sm">
                    <div class="post-block post-side-block flex">
                        @if(Auth::check())
                            <button type="button" class="button button-primary" id="create_new_report" data-toggle="modal" data-target="#createTravlogPopup">
                                <span class="text-truncate">Create a Travelog</span>
                            </button>
                        @else
                            <button type="button" class="button button-primary open-login">
                                <span class="text-truncate">Create a Travelog</span>
                            </button>
                        @endif
                    </div>
                    <div class="post-block post-side-block col">
                        <div class="side-trip-tab rep-type-block">
                            <div class="rep-type-item current-tab" reptype="all">
                                <i id="trav-log-icon" class="fa fa-newspaper-o"></i>
                                <div class="trip-tab-ttl"><b>@lang('other.all')</b> <span class="text-truncate">Travelogs</span></div>
                            </div>
                            <div class="rep-type-item" reptype="mine">
                                <i id="trav-log-icon" class="fa fa-newspaper-o"></i>
                                <div class="trip-tab-ttl"><b>My</b> <span class="text-truncate">Travelogs</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="post-trav-header-block">
                        <div class="post-side-top padding-10">
                            <div class="col-md-5 pr-0 search-trav-input-wrapper" id="travelog_searchbox">
                                <i class="trav-search-icon"></i>
                                <input type="text" class="search-travelog-input" id="search_travelog_input" placeholder="Search in travelogs...">
                            </div>
                            <div class="col-md-4" id="destination_searchbox">
                    <span class="select2 select2-container select2-container--default" id="remove_temp_destination" style="width: 100%;">
                        <span class="selection">
                            <span class="select2-selection select2-selection--single travelog-select2-input">
                                <span class="select2-selection__rendered">
                                    <span class="select2-selection__placeholder">City or Country</span>
                                </span>
                                <span class="select2-selection__arrow">
                                    <b role="presentation"></b>
                                </span>
                            </span>
                        </span>
                        <span class="dropdown-wrapper"></span>
                    </span>
                                <select id="destination_search"
                                        placeholder="@lang('discussion.strings.search_place_city_country')"
                                        style="width: 100%;display:none">
                                </select>
                            </div>
                            <div class="col-md-3 pl-0" id="travelog_orderbox">
                    <span class="select2 select2-container select2-container--default" id="remove_temp_order" style="width: 100%;">
                        <span class="selection">
                            <span class="select2-selection select2-selection--single travelog-select2-input">
                                <span class="select2-selection__rendered">
                                    <span class="select2-selection__placeholder">@lang('report.newest')</span>
                                </span>
                                <span class="select2-selection__arrow">
                                    <b></b>
                                </span>
                            </span>
                        </span>
                        <span class="dropdown-wrapper"></span>
                    </span>
                                <select  id="travelog_order"
                                         placeholder="@lang('report.newest')"
                                         style="width: 100%;display:none">
                                    <option value="newest">@lang('report.newest')</option>
                                    <option value="popular">@lang('report.most_popular')</option>
                                    <option value="oldest"> @lang('report.oldest')</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-block post-side-block" style="margin-top:30px">
                    <h3 class="mobile-filter-toggler" style="display: none;">Filter</h3>
                    <div class="post-collapse-block">
                        <div class="post-collapse-inner report-collapse-inner" id="travelMateAccordion" role="tablist"
                            aria-multiselectable="true">
                            <div class="travlogSelected rep-selected-block trip-tab-block current-tab" id='travlogSelectAll' ftype="all">
                                <input type='radio' name='order' value='' checked>
                                <label >@lang('other.all')</label>
                            </div>

                            <div class="card rep-selected-block" ftype="reportsCountries">
                                <div class="card-header" role="tab" id="reports_countries">
                                    <div class="collapsed travlogSelected trip-tab-block"  id='travlogSelectCountries'>
                                        <input type='radio' name='order' value=''>
                                        <label >@lang('report.countries')</label>
                                    </div>
                                </div>
                                <div id="reportsCountries" class="report-loc-filter-block">
                                    <div class="card-block">
                                        <div class="report-input-wrap" id="filterForCountry">
                                            <input name="filter_country" id="filter_country" placeholder="Filter by countries..." class="input filter-country-input report-filter-input">
                                            <div class="filtered-country-list">
                                                <ul class="filter-country-block">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card rep-selected-block" ftype="reportsCities">
                                <div class="card-header" role="tab" id="reports_cities">
                                    <div class="collapsed travlogSelected trip-tab-block" id='travlogSelectCities'>
                                        <input type='radio' name='order' value=''  id='travlogInputSelectCities' >
                                        <label >@lang('report.cities')</label>
                                    </div>
                                </div>
                                <div id="reportsCities" class="report-loc-filter-block">
                                    <div class="card-block">
                                        <div class="report-input-wrap">
                                            <input name="filter_city" id="filter_city" placeholder="Filter by cities..." class="input filter-city-input report-filter-input" >
                                            <div class="filtered-city-list">
                                                <ul class="filter-city-block">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            @include('site.layouts._sidebar')
        </aside>
    </div>
</div>
@endsection

@section('before_scripts')
@if(Auth::check())
    @include('site/reports/partials/modal-create-report-step-1')
    @include('site/reports/partials/modal-create-report-step-2')
    @include('site/reports/partials/modal-add-trip-plan')
    @include('site/reports/partials/modal-add-info')
    @include('site.home.new.partials._create-options-modal')
@endif
@endsection

@section('after_scripts')
 @include('site.reports._scripts')
 @include('site.reports._filter_scripts')

 <script data-cfasync="false" src="{{asset('/dist-parcel/travlogs-web-upd.js')}}"></script>
 <script data-cfasync="false" src="{{asset('dist-parcel/assets/Travelog/main.js')}}"></script>
@endsection
