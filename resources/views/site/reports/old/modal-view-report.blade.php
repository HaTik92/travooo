<!-- travlog popup -->
<div class="modal fade white-style" data-backdrop="false" id="travlogPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
      <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-1030" role="document">
      <div class="modal-custom-block">
        <div class="post-block post-travlog">
          <div class="post-top-info-layer post-footer-info">
            <div class="info-side">
              <a href="#" class="back-link"><i class="fa fa-angle-left"></i> Back</a>
              <div class="post-foot-block post-reaction">
                <img src="../assets2/image/reaction-icon-smile-only.png" alt="smile">
                <span><b>6</b> Reactions</span>
              </div>
              <div class="post-foot-block">
                <i class="trav-comment-icon"></i>
                <ul class="foot-avatar-list">
                  <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                  --><li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                  --><li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                </ul>
                <span>20 Comments</span>
              </div>
            </div>
            <div class="actions-side">
              <div class="dropdown">
                <a href="#" class="top-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="trav-share-icon"></i>
                  <span>Share</span>
                </a>
                <div class="dropdown-menu dropdown-menu-left dropdown-arrow">
                  <a class="dropdown-item with-collapse" data-toggle="collapse" href="#collapseShareOnline" role="button" aria-expanded="false" aria-controls="collapseShareOnline">
                    <span class="icon-wrap">
                      <i class="trav-share-online-icon"></i>
                    </span>
                    <div class="drop-txt">
                      <p><b>Share online</b></p>
                      <p>Spread the word</p>
                    </div>
                  </a>
                  <div class="collapse" id="collapseShareOnline">
                    <ul class="share-link-list">
                      <li>
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                      </li>
                      <li>
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                      </li>
                      <li>
                        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                      </li>
                      <li>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                      </li>
                      <li>
                        <a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
                      </li>
                      <li>
                        <a href="#" class="tumblr"><i class="fa fa-tumblr"></i></a>
                      </li>
                    </ul>
                  </div>
                  <a class="dropdown-item" href="#">
                    <span class="icon-wrap">
                      <i class="trav-embed-icon"></i>
                    </span>
                    <div class="drop-txt">
                      <p><b>Embed</b></p>
                      <p>Save it for later</p>
                    </div>
                  </a>
                  <a class="dropdown-item" href="#">
                    <span class="icon-wrap">
                      <i class="trav-share-on-travo-icon"></i>
                    </span>
                    <div class="drop-txt">
                      <p><b>Share on Travooo</b></p>
                      <p>Help your friends</p>
                    </div>
                  </a>
                </div>
              </div>
              <a href="#" class="top-action">
                <i class="trav-save-icon"></i>
                <span>Save</span>
              </a>
            </div>
          </div>
          <div class="top-banner-block top-travlog-banner" style="background-image: url(../assets2/image/travlog-banner.jpg)">
            <div class="top-banner-inner">
              <div class="travel-left-info">
                <h3 class="travel-title">From Moroco to the Amazing Japan in 7 Days</h3>
                <div class="travlog-user-info">
                  <div class="user-info-wrap">
                    <div class="avatar-wrap">
                      <img src="http://placehold.it/50x50" alt="">
                    </div>
                    <div class="info-txt">
                      <div class="top-name">
                        By <a class="post-name-link" href="#">Thomas Wright</a> 
                      </div>
                      <div class="sub-info">
                        Expert in <a href="#" class="place-link">France</a>
                      </div>
                    </div>
                  </div>
                  <div class="info-action">
                    <button class="btn btn-white-follow">Follow</button>
                  </div>
                </div>
              </div>
              <div class="travel-right-info">
                <ul class="sub-list">
                  <li>
                    <div class="top-txt">Country capital</div>
                    <div class="sub-txt">Tokyo</div>
                  </li>
                  <li>
                    <div class="top-txt">Population</div>
                    <div class="sub-txt">127 million</div>
                  </li>
                  <li>
                    <div class="top-txt">Currency</div>
                    <div class="sub-txt">Japanese yen</div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="travlog-main-block">
            <div class="travlog-side">
              <div class="user-top-info">
                <div class="avatar-wrap">
                  <img class="ava" src="http://placehold.it/70x70" alt="">
                </div>
                <p class="by-name">By <a href="#" class="name-link">Thomas</a></p>
              </div>
              <ul class="action-list">
                <li class="dropdown">
                  <a href="#" class="top-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="trav-heart-icon"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-top-right dropdown-arrow">
                    <ul class="smile-list">
                      <li>
                        <a href="#">
                          <img class="smile" src="../assets2/image/smile-smiley.svg" alt="smile">
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img class="smile" src="../assets2/image/smile-eek.svg" alt="smile">
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img class="smile" src="../assets2/image/smile-eye-heart.svg" alt="smile">
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img class="smile" src="../assets2/image/smile-happy-cry.svg" alt="smile">
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img class="smile" src="../assets2/image/smile-angry.svg" alt="smile">
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img class="smile" src="../assets2/image/smile-finger-up.svg" alt="smile">
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img class="smile" src="../assets2/image/smile-finger-down.svg" alt="smile">
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
                <li>
                  <a href="#">
                    <i class="trav-side-comment-icon"></i>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="trav-share-icon"></i>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="trav-save-icon"></i>
                  </a>
                </li>
              </ul>
            </div>
            <div class="main-content">
              <div class="content-wrapper">
                <div class="quote-block">
                  Fusce id lacinia odio curabitur tincidunt in diam nec gravida vivamus semper purus justo, vitae suscipit nisi posuere curabitur.
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu, efficitur metus.</p>
                <h3>Sed sollicitudin tempor:</h3>
                <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin non sem feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis ullamcorper lorem efficitur, sodales volutpat mi.</p>
                <p>Cras dignissim euismod tempor.</p>
              </div>
              <div class="image-block">
                <img src="http://placehold.it/670x520" alt="image">
                <p class="sub-txt">“Mauris sed felis in nulla” eleifend imperdiet</p>
              </div>
              <div class="content-wrapper">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu, efficitur metus.</p>
                <h3>Sed sollicitudin tempor:</h3>
                <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin non sem feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis ullamcorper lorem efficitur, sodales volutpat mi.</p>
                <p>Cras dignissim euismod tempor.</p>
                <div class="post-image-container post-follow-container travlog-map-wrap">
                  <div class="post-map-wrap">
                    <ul class="post-time-trip breadcrumb-out">
                      <li class="day">Day</li>
                      <li class="count">1</li>
                      <li class="count active">2</li>
                    </ul>
                    <div class="post-map-inner">
                      <img src="../assets2/image/trip-plan-image.jpg" alt="map">
                      <div class="destination-point" style="top:80px;left: 20%;">
                        <img class="map-marker mCS_img_loaded" src="{{asset('assets2/image/marker.png')}}" alt="marker">
                      </div>
                    </div>
                  </div>
                  <div class="post-bottom-block">
                    <div class="bottom-txt-wrap">
                      <p class="bottom-ttl">Suspendisse mauris felis 5 nulla eleifend</p>
                      <div class="bottom-sub-txt">
                        <span>Trip Plan by <a href="#" class="name-link">Tim</a> to 7 Destinations in Tokyo on 1 Sep 2019</span>
                      </div>
                    </div>
                    <div class="bottom-btn-wrap">
                      <button type="button" class="btn btn-light-grey btn-bordered">
                        View plan
                      </button>
                    </div>
                  </div>
                </div>
                <div class="travlog-card">
                  <div class="text-block">
                    <div class="title-card">
                      <span class="tag">Hotel</span>
                      <a href="#" class="place-link">Suamcorper scelerisque</a>
                    </div>
                    <p>Maecenas eget convallis elit, at commodo velit</p>
                    <div class="visited">
                      <ul class="foot-avatar-list">
                        <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li><!--
                        --><li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li><!--
                        --><li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li>
                      </ul>
                      <span>+21 others have visited this place</span>
                    </div>
                    <div class="rate-info">
                      <div class="rate-inner">
                        <span class="label">4.7 <i class="trav-star-icon"></i></span>
                        <p><b>Excellent:</b></p>
                        <p>723 reviews</p>
                      </div>
                      <div class="location">
                        <i class="trav-set-location-icon"></i>
                        <span>Tokyo, Japan</span>
                      </div>
                    </div>
                  </div>
                  <div class="img-wrap">
                    <img src="http://placehold.it/165x140" alt="image">
                  </div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu, efficitur metus.</p>
                <h3>Sed sollicitudin tempor:</h3>
                <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin non sem feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis ullamcorper lorem efficitur, sodales volutpat mi.</p>
                <div class="flex-image-wrap fh-485">
                  <div class="image-column col-50">
                    <div class="image-inner" style="background-image:url(http://placehold.it/285x485)"></div>
                  </div>
                  <div class="image-column col-50">
                    <div class="image-inner img-h-60" style="background-image:url(http://placehold.it/285x291)"></div>
                    <div class="image-inner img-h-40" style="background-image:url(http://placehold.it/285x195)"></div>
                  </div>
                </div>

                <div class="currency-line-block">
                  <div class="block-name">Currency</div>
                  <div class="currency-desc">
                    <div class="cur-name">Japanese yen</div>
                    <div class="cur-symbol">Symbol: ¥</div>
                    <div class="cur-code">Code: JPY</div>
                  </div>
                </div>

                <div class="travlog-image-card">
                  <div class="card-part img-wrap">
                    <img class="back" src="http://placehold.it/295x180" alt="image">
                    <div class="avatar-img">
                      <img src="http://placehold.it/70x70?text=avatar" alt="ava" class="ava">
                    </div>
                  </div>
                  <div class="card-part info-card">
                    <div class="info-top">
                      <div class="info-txt">
                        <div class="name">Stephanie Small</div>
                        <div class="place">United States</div>
                      </div>
                      <div class="btn-wrap">
                        <button type="button" class="btn btn-light-grey btn-bordered">
                          Follow
                        </button>
                      </div>
                    </div>
                    <ul class="preview-list">
                      <li><img src="http://placehold.it/80x80" alt="image"></li>
                      <li><img src="http://placehold.it/80x80" alt="image"></li>
                      <li><img src="http://placehold.it/80x80" alt="image"></li>
                    </ul>
                  </div>
                </div>
                <div class="travlog-image-card">
                  <div class="card-part img-wrap">
                    <img class="back" src="http://placehold.it/295x180" alt="image">
                    <div class="avatar-img">
                      <img src="http://placehold.it/70x70?text=avatar" alt="ava" class="ava">
                    </div>
                  </div>
                  <div class="card-part info-card">
                    <div class="info-top">
                      <div class="info-txt">
                        <div class="name">Stephanie Small</div>
                        <div class="place">United States</div>
                      </div>
                      <div class="btn-wrap">
                        <button type="button" class="btn btn-light-grey btn-bordered">
                          Follow
                        </button>
                      </div>
                    </div>
                    <ul class="preview-list">
                      <li><img src="http://placehold.it/80x80" alt="image"></li>
                      <li><img src="http://placehold.it/80x80" alt="image"></li>
                      <li><img src="http://placehold.it/80x80" alt="image"></li>
                    </ul>
                  </div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu, efficitur metus.</p>
                <h3>Sed sollicitudin tempor:</h3>
                <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin non sem feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis ullamcorper lorem efficitur, sodales volutpat mi.</p>
                <div class="post-image-container post-follow-container travlog-map-wrap">
                  <div class="post-map-wrap">
                    <div class="post-map-inner">
                      <img src="../assets2/image/trip-plan-image.jpg" alt="map">
                      <div class="destination-point" style="top:80px;left: 20%;">
                        <img class="map-marker mCS_img_loaded" src="{{asset('assets2/image/marker.png')}}" alt="marker">
                      </div>
                    </div>
                    <div class="info-caption">
                      <div class="caption-txt">
                        <div class="info-block">
                          <div class="info-flag">
                            <img class="flag" src="http://placehold.it/60x42?text=flag" alt="flag">
                          </div>
                          <div class="info-txt">
                            <div class="info-name">Japan</div>
                            <div class="info-sub">Country in Asia</div>
                          </div>
                        </div>
                        <div class="info-block">
                          <div class="info-icon">
                            <i class="trav-popularity-icon"></i>
                          </div>
                          <div class="info-txt">
                            <div class="info-ttl">321.4 M</div>
                            <div class="info-sub">Population</div>
                          </div>
                        </div>
                      </div>
                      <div class="btn-wrap">
                        <button type="button" class="btn btn-light-grey btn-bordered">
                          Follow
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu, efficitur metus.</p>
                <div class="post-top-info-layer post-footer-info foot-place">
                  <div class="info-side">
                    <div class="post-foot-block post-reaction">
                      <img src="../assets2/image/reaction-icon-smile-only.png" alt="smile">
                      <span><b>6</b> Reactions</span>
                    </div>
                    <div class="post-foot-block">
                      <i class="trav-comment-icon"></i>
                      <ul class="foot-avatar-list">
                        <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                        --><li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                        --><li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                      </ul>
                      <span>20 Comments</span>
                    </div>
                  </div>
                  <div class="actions-side">
                    <div class="dropdown">
                      <a href="#" class="top-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="trav-share-icon"></i>
                        <span>Share</span>
                      </a>
                      <div class="dropdown-menu dropdown-menu-left dropdown-arrow">
                        <a class="dropdown-item with-collapse" data-toggle="collapse" href="#collapseShareOnline" role="button" aria-expanded="false" aria-controls="collapseShareOnline">
                          <span class="icon-wrap">
                            <i class="trav-share-online-icon"></i>
                          </span>
                          <div class="drop-txt">
                            <p><b>Share online</b></p>
                            <p>Spread the word</p>
                          </div>
                        </a>
                        <div class="collapse" id="collapseShareOnline">
                          <ul class="share-link-list">
                            <li>
                              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                            </li>
                            <li>
                              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li>
                              <a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
                            </li>
                            <li>
                              <a href="#" class="tumblr"><i class="fa fa-tumblr"></i></a>
                            </li>
                          </ul>
                        </div>
                        <a class="dropdown-item" href="#">
                          <span class="icon-wrap">
                            <i class="trav-embed-icon"></i>
                          </span>
                          <div class="drop-txt">
                            <p><b>Embed</b></p>
                            <p>Save it for later</p>
                          </div>
                        </a>
                        <a class="dropdown-item" href="#">
                          <span class="icon-wrap">
                            <i class="trav-share-on-travo-icon"></i>
                          </span>
                          <div class="drop-txt">
                            <p><b>Share on Travooo</b></p>
                            <p>Help your friends</p>
                          </div>
                        </a>
                      </div>
                    </div>
                    <a href="#" class="top-action">
                      <i class="trav-save-icon"></i>
                      <span>Save</span>
                    </a>
                  </div>
                </div>
                <div class="foot-author-info">
                  <div class="info-block">
                    <div class="img-wrap">
                      <img class="ava" src="http://placehold.it/50x50" alt="avatar">
                    </div>
                    <div class="info-txt">
                      <div class="name">
                        <span>By </span>
                        <b>Thomas Wright</b>
                      </div>
                      <div class="location">
                        <span>Expert in </span>
                        <b>France</b>
                      </div>
                    </div>
                  </div>
                  <div class="btn-wrap">
                    <button type="button" class="btn btn-light-grey btn-bordered">
                      Follow
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div class="post-comment-layer-wrap">
              <div class="post-comment-layer">
                <div class="post-add-comment-block">
                  <div class="avatar-wrap">
                    <img src="http://placehold.it/45x45" alt="">
                  </div>
                  <div class="post-add-com-input">
                    <input type="text" placeholder="Write a comment">
                  </div>
                </div>
                <div class="post-comment-top-info">
                  <ul class="comment-filter">
                    <li class="active">Top</li>
                    <li>New</li>
                  </ul>
                  <div class="comm-count-info">
                    3 / 20
                  </div>
                </div>
                <div class="post-comment-wrapper">
                  <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                      <img src="http://placehold.it/45x45" alt="">
                    </div>
                    <div class="post-comment-text">
                      <div class="post-com-name-layer">
                        <a href="#" class="comment-name">Katherin</a>
                        <a href="#" class="comment-nickname">@katherin</a>
                      </div>
                      <div class="comment-txt">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                      </div>
                      <div class="comment-bottom-info">
                        <div class="com-reaction">
                          <img src="../assets2/image/icon-smile.png" alt="">
                          <span>21</span>
                        </div>
                        <div class="com-time">6 hours ago</div>
                      </div>
                    </div>
                  </div>
                  <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                      <img src="http://placehold.it/45x45" alt="">
                    </div>
                    <div class="post-comment-text">
                      <div class="post-com-name-layer">
                        <a href="#" class="comment-name">Amine</a>
                        <a href="#" class="comment-nickname">@ak0117</a>
                      </div>
                      <div class="comment-txt">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                      </div>
                      <div class="comment-bottom-info">
                        <div class="com-reaction">
                          <img src="../assets2/image/icon-like.png" alt="">
                          <span>19</span>
                        </div>
                        <div class="com-time">6 hours ago</div>
                      </div>
                    </div>
                  </div>
                  <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                      <img src="http://placehold.it/45x45" alt="">
                    </div>
                    <div class="post-comment-text">
                      <div class="post-com-name-layer">
                        <a href="#" class="comment-name">Katherin</a>
                        <a href="#" class="comment-nickname">@katherin</a>
                      </div>
                      <div class="comment-txt">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                      </div>
                      <div class="comment-bottom-info">
                        <div class="com-reaction">
                          <img src="../assets2/image/icon-smile.png" alt="">
                          <span>15</span>
                        </div>
                        <div class="com-time">6 hours ago</div>
                      </div>
                    </div>
                  </div>
                  <a href="#" class="load-more-link">Load more...</a>
                </div>
              </div>
            </div>
          </div>
          
        </div>

        <div class="more-travlog-block">
          <h3 class="title">More travelogs</h3>
          <div class="travlog-list">
            <div class="travlog-info-block">
              <div class="travlog-info-inner">
                <div class="img-wrap">
                  <img src="http://placehold.it/335x170" alt="photo">
                </div>
                <div class="info-wrapper">
                  <div class="country-name">Japan</div>
                  <div class="info-title">10 Things You Must Know Before...</div>
                  <div class="foot-block">
                    <div class="author-side">
                      <div class="img-wrap">
                        <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                      </div>
                      <div class="author-info">
                        <div class="author-name">
                          <a href="#" class="author-link">Thomas Wright</a>
                        </div>
                        <div class="time">
                          <span>Today at </span>
                          <b>5:29 pm</b>
                        </div>
                      </div>
                    </div>
                    <div class="like-side">
                      <div class="post-foot-block">
                        <i class="trav-heart-fill-icon"></i>&nbsp;
                        <span>6</span>
                      </div>
                      <div class="post-foot-block">
                        <i class="trav-comment-icon"></i>&nbsp;
                        <span>20</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="travlog-info-block">
              <div class="travlog-info-inner">
                <div class="img-wrap">
                  <img src="http://placehold.it/335x170" alt="photo">
                </div>
                <div class="info-wrapper">
                  <div class="country-name">Japan</div>
                  <div class="info-title">10 Things You Must Know Before...</div>
                  <div class="foot-block">
                    <div class="author-side">
                      <div class="img-wrap">
                        <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                      </div>
                      <div class="author-info">
                        <div class="author-name">
                          <a href="#" class="author-link">Thomas Wright</a>
                        </div>
                        <div class="time">
                          <span>Today at </span>
                          <b>5:29 pm</b>
                        </div>
                      </div>
                    </div>
                    <div class="like-side">
                      <div class="post-foot-block">
                        <i class="trav-heart-fill-icon"></i>&nbsp;
                        <span>6</span>
                      </div>
                      <div class="post-foot-block">
                        <i class="trav-comment-icon"></i>&nbsp;
                        <span>20</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="travlog-info-block">
              <div class="travlog-info-inner">
                <div class="img-wrap">
                  <img src="http://placehold.it/335x170" alt="photo">
                </div>
                <div class="info-wrapper">
                  <div class="country-name">Japan</div>
                  <div class="info-title">10 Things You Must Know Before...</div>
                  <div class="foot-block">
                    <div class="author-side">
                      <div class="img-wrap">
                        <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                      </div>
                      <div class="author-info">
                        <div class="author-name">
                          <a href="#" class="author-link">Thomas Wright</a>
                        </div>
                        <div class="time">
                          <span>Today at </span>
                          <b>5:29 pm</b>
                        </div>
                      </div>
                    </div>
                    <div class="like-side">
                      <div class="post-foot-block">
                        <i class="trav-heart-fill-icon"></i>&nbsp;
                        <span>6</span>
                      </div>
                      <div class="post-foot-block">
                        <i class="trav-comment-icon"></i>&nbsp;
                        <span>20</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="travlog-info-block">
              <div class="travlog-info-inner">
                <div class="img-wrap">
                  <img src="http://placehold.it/335x170" alt="photo">
                </div>
                <div class="info-wrapper">
                  <div class="country-name">Japan</div>
                  <div class="info-title">10 Things You Must Know Before...</div>
                  <div class="foot-block">
                    <div class="author-side">
                      <div class="img-wrap">
                        <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                      </div>
                      <div class="author-info">
                        <div class="author-name">
                          <a href="#" class="author-link">Thomas Wright</a>
                        </div>
                        <div class="time">
                          <span>Today at </span>
                          <b>5:29 pm</b>
                        </div>
                      </div>
                    </div>
                    <div class="like-side">
                      <div class="post-foot-block">
                        <i class="trav-heart-fill-icon"></i>&nbsp;
                        <span>6</span>
                      </div>
                      <div class="post-foot-block">
                        <i class="trav-comment-icon"></i>&nbsp;
                        <span>20</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="travlog-info-block">
              <div class="travlog-info-inner">
                <div class="img-wrap">
                  <img src="http://placehold.it/335x170" alt="photo">
                </div>
                <div class="info-wrapper">
                  <div class="country-name">Japan</div>
                  <div class="info-title">10 Things You Must Know Before...</div>
                  <div class="foot-block">
                    <div class="author-side">
                      <div class="img-wrap">
                        <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                      </div>
                      <div class="author-info">
                        <div class="author-name">
                          <a href="#" class="author-link">Thomas Wright</a>
                        </div>
                        <div class="time">
                          <span>Today at </span>
                          <b>5:29 pm</b>
                        </div>
                      </div>
                    </div>
                    <div class="like-side">
                      <div class="post-foot-block">
                        <i class="trav-heart-fill-icon"></i>&nbsp;
                        <span>6</span>
                      </div>
                      <div class="post-foot-block">
                        <i class="trav-comment-icon"></i>&nbsp;
                        <span>20</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="travlog-info-block">
              <div class="travlog-info-inner">
                <div class="img-wrap">
                  <img src="http://placehold.it/335x170" alt="photo">
                </div>
                <div class="info-wrapper">
                  <div class="country-name">Japan</div>
                  <div class="info-title">10 Things You Must Know Before...</div>
                  <div class="foot-block">
                    <div class="author-side">
                      <div class="img-wrap">
                        <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                      </div>
                      <div class="author-info">
                        <div class="author-name">
                          <a href="#" class="author-link">Thomas Wright</a>
                        </div>
                        <div class="time">
                          <span>Today at </span>
                          <b>5:29 pm</b>
                        </div>
                      </div>
                    </div>
                    <div class="like-side">
                      <div class="post-foot-block">
                        <i class="trav-heart-fill-icon"></i>&nbsp;
                        <span>6</span>
                      </div>
                      <div class="post-foot-block">
                        <i class="trav-comment-icon"></i>&nbsp;
                        <span>20</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>