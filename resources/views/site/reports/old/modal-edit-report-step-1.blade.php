<!-- create travlog step 1 popup -->
<div class="modal fade white-style" data-backdrop="false" id="createTravlogPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                <form method='post' id='form_report_step1' autocomplete="off">
                    <div class="top-title-layer">
                        <h3 class="title">
                            <span class="circle">1</span>
                            <span class="txt">Travelog Details</span>
                        </h3>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="travlog-details-block">
                        <div class="detail-row">
                            <div class="title-block">
                                <div class="title">Report Title</div>
                                <div class="subtitle">Lorem ipsut dolor amet</div>
                            </div>
                            <div class="input-layer">
                                <div class="input-block">
                                    <input class="detail-input" name="title" id="title" type="text" placeholder="USA, Canada or Japan? Where to go this summer" required>
                                </div>
                            </div>
                            <div class="post-title-label">
                                <div class="label-txt" id='titleLength'>MAX characters: 50</div>
                                <label for="title" class="error" style="color:red"></label>
                            </div>
                        </div>
                        <div class="detail-row">
                            <div class="title-block">
                                <div class="title">Description</div>
                                <div class="subtitle">Pellentesque id felis quis turpis volutpat molestie quis in est</div>
                            </div>
                            <div class="input-layer">
                                <div class="input-block">
                                    <textarea class="detail-input mentionsInput" name="description" id="description" cols="30" rows="5" placeholder="Short description (optional)" style="height:100px;"></textarea>
                                </div>
                            </div>
                            <div class="post-title-label">
                                <div class="label-txt" id='descriptionLength'>MAX characters: 260</div>
                            </div>
                        </div>
                        <div class="detail-row">
                            <div class="title-block">
                                <div class="select-wrap">
                                    <select name="report_for" id="report_for">
                                        <option value="country">Country</option>
                                        <option value="city">City</option>
                                    </select>
                                </div>
                                <div class="subtitle">Pellentesque id felis quis turpis volutpat molestie quis in est</div>
                            </div>
                            <div class="input-layer">
                                <div>
                                    <div class="select-country-block">
                                        <div class="search-country-input" id="forCountry">
                                            <select name="countries_id" id="countries_id" data-placeholder="Type to search for a Country..." class="input groupOne" style="width:100%">
                                                
                                            </select>
                                        </div>
                                        <label for="countries_id" class="error" style="color:red"></label>
                                        <div class="search-country-input" id="forCity" style="display:none;">    
                                            <select name="cities_id" id="cities_id" data-placeholder="Type to search for a City..." class="input groupOne" style="width:100%">
                                                
                                            </select>
                                        </div>
                                        <div class="country-map-block" id="gmap" style="display:none;width:100%;height:350px;">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-title-label">

                            </div>
                        </div>
                    </div>
                    <div class="post-foot-btn">
                        <button class="btn btn-transp btn-clear">Cancel</button>
                        <button class="btn btn-light-primary btn-bordered" id="step1_next">Next</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

