<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - travelog page</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">
        <div class="container-fluid">

            @include('site/layouts/left_menu')


            <div class="post-block post-flight-style">

                <div class="post-side-inner">
                    <div class="post-slide-wrap post-destination-block">
                        <div class="lSSlideOuter post-dest-slider-wrap">
                            <div class="lSSlideWrapper usingCss">
                                <ul id="trendingTravelDestinations" class="post-slider lightSlider lsGrab lSSlide"
                                    style="width: 3700px; transform: translate3d(-1110px, 0px, 0px); height: 153px; padding-bottom: 0%;">
                                    <li class="clone left" style="margin-right: 20px;">
                                        <div class="post-dest-inner">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x95" alt="">
                                                <div class="dest-name">@lang('other.central_part')</div>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="clone left" style="margin-right: 20px;">
                                        <div class="post-dest-inner">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x95" alt="">
                                                <div class="dest-name">Japan</div>
                                            </div>
                                            <div class="dest-txt">
                                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                                <p><span>12K</span> Travel mates</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="clone left" style="margin-right: 20px;">
                                        <div class="post-dest-inner">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x95" alt="">
                                                <div class="dest-name">Grand Canyon National park</div>
                                            </div>
                                            <div class="dest-txt">
                                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                                <p><span>12K</span> Travel mates</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="clone left" style="margin-right: 20px;">
                                        <div class="post-dest-inner">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x95" alt="">
                                                <div class="dest-name">Big ben</div>
                                            </div>
                                            <div class="dest-txt">
                                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                                <p><span>12K</span> Travel mates</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="clone left" style="margin-right: 20px;">
                                        <div class="post-dest-inner">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x95" alt="">
                                                <div class="dest-name">@lang('other.central_part')</div>
                                            </div>
                                            <div class="dest-txt">
                                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                                <p><span>12K</span> Travel mates</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="clone left" style="margin-right: 20px;">
                                        <div class="post-dest-inner">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x95" alt="">
                                                <div class="dest-name">Japan</div>
                                            </div>
                                            <div class="dest-txt">
                                                <p><span>1.3K</span> @choice('trip.trip_plan', 2)</p>
                                                <p><span>12K</span> Travel mates</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="lslide active" style="margin-right: 20px;">
                                        <div class="post-dest-inner">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x95" alt="">
                                                <div class="dest-name">France</div>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="lslide" style="margin-right: 20px;">
                                        <div class="post-dest-inner">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x95" alt="">
                                                <div class="dest-name">Washington</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="lslide" style="margin-right: 20px;">
                                        <div class="post-dest-inner">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x95" alt="">
                                                <div class="dest-name">@lang('other.central_part')</div>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="lslide" style="margin-right: 20px;">
                                        <div class="post-dest-inner">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x95" alt="">
                                                <div class="dest-name">Japan</div>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="lslide" style="margin-right: 20px;">
                                        <div class="post-dest-inner">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x95" alt="">
                                                <div class="dest-name">Grand Canyon National park</div>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="lslide" style="margin-right: 20px;">
                                        <div class="post-dest-inner">
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x95" alt="">
                                                <div class="dest-name">Big ben</div>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                                <div class="lSAction"><a class="lSPrev"><i class="trav-angle-left"></i></a><a
                                            class="lSNext"><i class="trav-angle-right"></i></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    <div class="post-block post-tab-block">
                        <div class="post-tab-inner" id="postTabBlock">
                            <div class="tab-item active-tab">
                                Most popular
                            </div>
                            <div class="tab-item">
                                @lang('navs.frontend.newest')
                            </div>

                            <div class="tab-item">
                                Oldest
                            </div>

                        </div>
                    </div>

                    <div class="more-travlog-block">
                        <div class="travlog-list">
                            <div class="travlog-info-block" style="width:50%">
                                <div class="travlog-info-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/335x170" alt="photo">
                                    </div>
                                    <div class="info-wrapper">
                                        <div class="country-name">Japan</div>
                                        <div class="info-title">
                                            <a href="{{route('report.show', 1)}}">
                                                10 Things You Must Know Before...
                                            </a>
                                        </div>
                                        <div class="foot-block">
                                            <div class="author-side">
                                                <div class="img-wrap">
                                                    <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                                </div>
                                                <div class="author-info">
                                                    <div class="author-name">
                                                        <a href="#" class="author-link">Thomas Wright</a>
                                                    </div>
                                                    <div class="time">
                                                        <span>Today at </span>
                                                        <b>5:29 pm</b>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="like-side">
                                                <div class="post-foot-block">
                                                    <i class="trav-heart-fill-icon"></i>&nbsp;
                                                    <span>6</span>
                                                </div>
                                                <div class="post-foot-block">
                                                    <i class="trav-comment-icon"></i>&nbsp;
                                                    <span>20</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="travlog-info-block" style="width:50%">
                                <div class="travlog-info-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/335x170" alt="photo">
                                    </div>
                                    <div class="info-wrapper">
                                        <div class="country-name">Japan</div>
                                        <div class="info-title">
                                            <a href="{{route('report.show', 1)}}">
                                                10 Things You Must Know Before...
                                            </a>
                                        </div>
                                        <div class="foot-block">
                                            <div class="author-side">
                                                <div class="img-wrap">
                                                    <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                                </div>
                                                <div class="author-info">
                                                    <div class="author-name">
                                                        <a href="#" class="author-link">Thomas Wright</a>
                                                    </div>
                                                    <div class="time">
                                                        <span>Today at </span>
                                                        <b>5:29 pm</b>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="like-side">
                                                <div class="post-foot-block">
                                                    <i class="trav-heart-fill-icon"></i>&nbsp;
                                                    <span>6</span>
                                                </div>
                                                <div class="post-foot-block">
                                                    <i class="trav-comment-icon"></i>&nbsp;
                                                    <span>20</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="travlog-info-block" style="width:50%">
                                <div class="travlog-info-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/335x170" alt="photo">
                                    </div>
                                    <div class="info-wrapper">
                                        <div class="country-name">Japan</div>
                                        <div class="info-title">
                                            <a href="{{route('report.show', 1)}}">
                                                10 Things You Must Know Before...
                                            </a>
                                        </div>
                                        <div class="foot-block">
                                            <div class="author-side">
                                                <div class="img-wrap">
                                                    <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                                </div>
                                                <div class="author-info">
                                                    <div class="author-name">
                                                        <a href="#" class="author-link">Thomas Wright</a>
                                                    </div>
                                                    <div class="time">
                                                        <span>Today at </span>
                                                        <b>5:29 pm</b>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="like-side">
                                                <div class="post-foot-block">
                                                    <i class="trav-heart-fill-icon"></i>&nbsp;
                                                    <span>6</span>
                                                </div>
                                                <div class="post-foot-block">
                                                    <i class="trav-comment-icon"></i>&nbsp;
                                                    <span>20</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="travlog-info-block" style="width:50%">
                                <div class="travlog-info-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/335x170" alt="photo">
                                    </div>
                                    <div class="info-wrapper">
                                        <div class="country-name">Japan</div>
                                        <div class="info-title">
                                            <a href="{{route('report.show', 1)}}">
                                                10 Things You Must Know Before...
                                            </a>
                                        </div>
                                        <div class="foot-block">
                                            <div class="author-side">
                                                <div class="img-wrap">
                                                    <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                                </div>
                                                <div class="author-info">
                                                    <div class="author-name">
                                                        <a href="#" class="author-link">Thomas Wright</a>
                                                    </div>
                                                    <div class="time">
                                                        <span>Today at </span>
                                                        <b>5:29 pm</b>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="like-side">
                                                <div class="post-foot-block">
                                                    <i class="trav-heart-fill-icon"></i>&nbsp;
                                                    <span>6</span>
                                                </div>
                                                <div class="post-foot-block">
                                                    <i class="trav-comment-icon"></i>&nbsp;
                                                    <span>20</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="travlog-info-block" style="width:50%">
                                <div class="travlog-info-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/335x170" alt="photo">
                                    </div>
                                    <div class="info-wrapper">
                                        <div class="country-name">Japan</div>
                                        <div class="info-title">
                                            <a href="{{route('report.show', 1)}}">
                                                10 Things You Must Know Before...
                                            </a>
                                        </div>
                                        <div class="foot-block">
                                            <div class="author-side">
                                                <div class="img-wrap">
                                                    <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                                </div>
                                                <div class="author-info">
                                                    <div class="author-name">
                                                        <a href="#" class="author-link">Thomas Wright</a>
                                                    </div>
                                                    <div class="time">
                                                        <span>Today at </span>
                                                        <b>5:29 pm</b>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="like-side">
                                                <div class="post-foot-block">
                                                    <i class="trav-heart-fill-icon"></i>&nbsp;
                                                    <span>6</span>
                                                </div>
                                                <div class="post-foot-block">
                                                    <i class="trav-comment-icon"></i>&nbsp;
                                                    <span>20</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="travlog-info-block" style="width:50%">
                                <div class="travlog-info-inner">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/335x170" alt="photo">
                                    </div>
                                    <div class="info-wrapper">
                                        <div class="country-name">Japan</div>
                                        <div class="info-title">
                                            <a href="{{route('report.show', 1)}}">
                                                10 Things You Must Know Before...
                                            </a>
                                        </div>
                                        <div class="foot-block">
                                            <div class="author-side">
                                                <div class="img-wrap">
                                                    <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                                </div>
                                                <div class="author-info">
                                                    <div class="author-name">
                                                        <a href="#" class="author-link">Thomas Wright</a>
                                                    </div>
                                                    <div class="time">
                                                        <span>Today at </span>
                                                        <b>5:29 pm</b>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="like-side">
                                                <div class="post-foot-block">
                                                    <i class="trav-heart-fill-icon"></i>&nbsp;
                                                    <span>6</span>
                                                </div>
                                                <div class="post-foot-block">
                                                    <i class="trav-comment-icon"></i>&nbsp;
                                                    <span>20</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer" style="top: 70px;">
                    <aside class="sidebar">

                        <div class="post-block post-side-block">
                            <div class="post-side-inner">
                                <button type="button" class="btn btn-light-primary btn-bordered btn-full">
                                    <i class="trav-add-trip-icon"></i>
                                    @lang('report.create_a_report')
                                </button>
                            </div>
                        </div>
                        <div class="post-block post-side-block">

                            <div class="post-side-trip-inner">
                                <div class="side-trip-tab">
                                    <div class="trip-tab-block current-tab">
                                        <i class="trav-trip-plans-p-icon"></i>
                                        <div class="trip-tab-ttl">@lang('other.all')</div>
                                    </div>
                                    <div class="trip-tab-block">
                                        <i class="trav-trip-plans-share-icon"></i>
                                        <div class="trip-tab-ttl">My reports</div>
                                    </div>
                                </div>
                                <div class="post-collapse-block">
                                    <div class="post-collapse-inner" id="travelMateAccordion" role="tablist"
                                         aria-multiselectable="true">

                                        <div class="card">
                                            <div class="card-header" role="tab" id="reports_all">
                                                <a class="collapsed" data-toggle="collapse"
                                                   data-parent="#travelMateAccordion" href="#reportsAll"
                                                   aria-expanded="false" aria-controls="reportsAll">
                                                    <i class="trav-angle-up"></i>
                                                    <span>@lang('other.all')</span>
                                                </a>
                                            </div>
                                            <div id="reportsAll" class="collapse" role="tabpanel"
                                                 aria-labelledby="reports_all">
                                                <div class="card-block">
                                                    <div class="input-wrap">
                                                        <input type="text"
                                                               placeholder="@lang('region.country_name_dots')">
                                                    </div>
                                                    <div class="alert alert-dismissible fade show" role="alert">
                                                        <button type="button" class="close-link" data-dismiss="alert"
                                                                aria-label="Close">
                                                            <i class="trav-close-icon"></i>
                                                        </button>
                                                        <span>Morocco</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-header" role="tab" id="reports_countries">
                                                <a class="collapsed" data-toggle="collapse"
                                                   data-parent="#travelMateAccordion" href="#reportsCountries"
                                                   aria-expanded="false" aria-controls="reportsCountries">
                                                    <i class="trav-angle-up"></i>
                                                    <span>Countries</span>
                                                </a>
                                            </div>
                                            <div id="reportsCountries" class="collapse" role="tabpanel"
                                                 aria-labelledby="reports_countries">
                                                <div class="card-block">
                                                    <div class="input-wrap">
                                                        <input type="text"
                                                               placeholder="@lang('region.country_name_dots')">
                                                    </div>
                                                    <div class="alert alert-dismissible fade show" role="alert">
                                                        <button type="button" class="close-link" data-dismiss="alert"
                                                                aria-label="Close">
                                                            <i class="trav-close-icon"></i>
                                                        </button>
                                                        <span>Morocco</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="reports_cities">
                                                <a class="collapsed" data-toggle="collapse"
                                                   data-parent="#travelMateAccordion" href="#reportsCities"
                                                   aria-expanded="false" aria-controls="reportsCities">
                                                    <i class="trav-angle-up"></i>
                                                    <span>Cities</span>
                                                </a>
                                            </div>
                                            <div id="reportsCities" class="collapse" role="tabpanel"
                                                 aria-labelledby="reports_cities">
                                                <div class="card-block">
                                                    <div class="input-wrap">
                                                        <input type="text"
                                                               placeholder="@lang('region.country_name_dots')">
                                                    </div>
                                                    <div class="alert alert-dismissible fade show" role="alert">
                                                        <button type="button" class="close-link" data-dismiss="alert"
                                                                aria-label="Close">
                                                            <i class="trav-close-icon"></i>
                                                        </button>
                                                        <span>Morocco</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                                <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                                <li><a href="{{url('/')}}">Advertising</a></li>
                                <li><a href="{{url('/')}}">Cookies</a></li>
                                <li><a href="{{url('/')}}">More</a></li>
                            </ul>
                            <p class="copyright">Travooo © 2017</p>
                        </div>
                    </aside>
                </div>
            </div>

        </div>

    </div>


</div>


<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script src="{{url('assets2/js/script.js')}}"></script>
</body>
</html>