<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - travelog page</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">
        <div class="container-fluid">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mapPopup">
                Map popup
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#commentsPopup">
                @lang('comment.comments') popup
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#travlogPopup">
                Travelog popup
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createTravlogPopup">
                Create Travelog popup
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#shareOnTravPopup">
                @lang('report.share_on_travooo') popup
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tripPlanPopup">
                Trip plan popup
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#embedPopup">
                @lang('report.embed') popup
            </button>

            <div class="post-block post-travlog">
                <div class="post-top-info-layer post-footer-info">
                    <div class="info-side">
                        <a href="#" class="back-link"><i class="fa fa-angle-left"></i> Back</a>
                        <div class="post-foot-block post-reaction">
                            <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                            <span><b>6</b> @lang('report.reactions')</span>
                        </div>
                        <div class="post-foot-block">
                            <i class="trav-comment-icon"></i>
                            <ul class="foot-avatar-list">
                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                  -->
                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                  -->
                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                            </ul>
                            <span>@choice('comment.count_comment', 20, ['count' => 20])</span>
                        </div>
                    </div>
                    <div class="actions-side">
                        <div class="dropdown">
                            <a href="#" class="top-action" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                <i class="trav-share-icon"></i>
                                <span>@lang('profile.share')</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-left dropdown-arrow">
                                <a class="dropdown-item with-collapse" data-toggle="collapse"
                                   href="#collapseShareOnline" role="button" aria-expanded="false"
                                   aria-controls="collapseShareOnline">
                    <span class="icon-wrap">
                      <i class="trav-share-online-icon"></i>
                    </span>
                                    <div class="drop-txt">
                                        <p><b>@lang('report.share_online')</b></p>
                                        <p>Spread the word</p>
                                    </div>
                                </a>
                                <div class="collapse" id="collapseShareOnline">
                                    <ul class="share-link-list">
                                        <li>
                                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" class="tumblr"><i class="fa fa-tumblr"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <a class="dropdown-item" href="#">
                    <span class="icon-wrap">
                      <i class="trav-embed-icon"></i>
                    </span>
                                    <div class="drop-txt">
                                        <p><b>@lang('report.embed')</b></p>
                                        <p>@lang('report.save_it_for_later')</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                    <span class="icon-wrap">
                      <i class="trav-share-on-travo-icon"></i>
                    </span>
                                    <div class="drop-txt">
                                        <p><b>@lang('report.share_on_travooo')</b></p>
                                        <p>@lang('report.help_your_friends')</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <a href="#" class="top-action">
                            <i class="trav-@lang('buttons.general.save')-icon"></i>
                            <span>@lang('buttons.general.save')</span>
                        </a>
                    </div>
                </div>
                <div class="top-banner-block top-travlog-banner"
                     style="background-image: url(./assets/image/travlog-banner.jpg)">
                    <div class="top-banner-inner">
                        <div class="travel-left-info">
                            <h3 class="travel-title">From Moroco to the Amazing Japan in 7 Days</h3>
                            <div class="travlog-user-info">
                                <div class="user-info-wrap">
                                    <div class="avatar-wrap">
                                        <img src="http://placehold.it/50x50" alt="">
                                    </div>
                                    <div class="info-txt">
                                        <div class="top-name">
                                            @lang('other.by') <a class="post-name-link" href="#">Thomas Wright</a>
                                        </div>
                                        <div class="sub-info">
                                            @lang('report.expert_in') <a href="#" class="place-link">France</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="info-action">
                                    <button class="btn btn-white-follow">@lang('buttons.general.follow')</button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-right-info">
                            <ul class="sub-list">
                                <li>
                                    <div class="top-txt">@lang('region.country_capital')</div>
                                    <div class="sub-txt">Tokyo</div>
                                </li>
                                <li>
                                    <div class="top-txt">@lang('region.population')</div>
                                    <div class="sub-txt">127 million</div>
                                </li>
                                <li>
                                    <div class="top-txt">@lang('region.currency')</div>
                                    <div class="sub-txt">Japanese yen</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="travlog-main-block">
                    <div class="travlog-side">
                        <div class="user-top-info">
                            <div class="avatar-wrap">
                                <img class="ava" src="http://placehold.it/70x70" alt="">
                            </div>
                            <p class="by-name">@lang('other.by') <a href="#" class="name-link">Thomas</a></p>
                        </div>
                        <ul class="action-list">
                            <li class="dropdown">
                                <a href="#" class="top-action" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    <i class="trav-heart-icon"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-top-right dropdown-arrow">
                                    <ul class="smile-list">
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-smiley.svg" alt="smile">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-eek.svg" alt="smile">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-eye-heart.svg" alt="smile">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-happy-cry.svg" alt="smile">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-angry.svg" alt="smile">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-finger-up.svg" alt="smile">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-finger-down.svg"
                                                     alt="smile">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="trav-side-comment-icon"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="trav-share-icon"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="trav-@lang('buttons.general.save')-icon"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="main-content">
                        <div class="content-wrapper">
                            <div class="quote-block">
                                Fusce id lacinia odio curabitur tincidunt in diam nec gravida vivamus semper purus
                                justo, vitae suscipit nisi posuere curabitur.
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed
                                finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu,
                                efficitur metus.</p>
                            <h3>Sed sollicitudin tempor:</h3>
                            <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit
                                bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin non sem
                                    feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis ullamcorper
                                lorem efficitur, sodales volutpat mi.</p>
                            <p>Cras dignissim euismod tempor.</p>
                        </div>
                        <div class="image-block">
                            <img src="http://placehold.it/670x520" alt="image">
                            <p class="sub-txt">“Mauris sed felis in nulla” eleifend imperdiet</p>
                        </div>
                        <div class="content-wrapper">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed
                                finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu,
                                efficitur metus.</p>
                            <h3>Sed sollicitudin tempor:</h3>
                            <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit
                                bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin non sem
                                    feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis ullamcorper
                                lorem efficitur, sodales volutpat mi.</p>
                            <p>Cras dignissim euismod tempor.</p>
                            <div class="post-image-container post-follow-container travlog-map-wrap">
                                <div class="post-map-wrap">
                                    <ul class="post-time-trip breadcrumb-out">
                                        <li class="day">@lang('time.day')</li>
                                        <li class="count">1</li>
                                        <li class="count active">2</li>
                                    </ul>
                                    <div class="post-map-inner">
                                        <img src="./assets/image/trip-plan-image.jpg" alt="map">
                                        <div class="destination-point" style="top:80px;left: 20%;">
                                            <img class="map-marker mCS_img_loaded" src="./assets/image/marker.png"
                                                 alt="marker">
                                        </div>
                                    </div>
                                </div>
                                <div class="post-bottom-block">
                                    <div class="bottom-txt-wrap">
                                        <p class="bottom-ttl">Suspendisse mauris felis 5 nulla eleifend</p>
                                        <div class="bottom-sub-txt">
                                            <span>@lang('trip.trip_plan_by') <a href="#" class="name-link">Tim</a> to 7 Destinations in Tokyo on 1 Sep 2019</span>
                                        </div>
                                    </div>
                                    <div class="bottom-btn-wrap">
                                        <button type="button" class="btn btn-light-grey btn-bordered">
                                            @lang('report.view_plan')
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="travlog-card">
                                <div class="text-block">
                                    <div class="title-card">
                                        <span class="tag">@lang('place.hotel')</span>
                                        <a href="#" class="place-link">Suamcorper scelerisque</a>
                                    </div>
                                    <p>Maecenas eget convallis elit, at commodo velit</p>
                                    <div class="visited">
                                        <ul class="foot-avatar-list">
                                            <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li><!--
                        -->
                                            <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li><!--
                        -->
                                            <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li>
                                        </ul>
                                        <span>@lang('report.count_others_have_visited_this_place', ['count' => '+21'])</span>
                                    </div>
                                    <div class="rate-info">
                                        <div class="rate-inner">
                                            <span class="label">4.7 <i class="trav-star-icon"></i></span>
                                            <p><b>@lang('report.excellent')</b></p>
                                            <p>@lang('report.count_review', ['count' => 723])</p>
                                        </div>
                                        <div class="location">
                                            <i class="trav-set-location-icon"></i>
                                            <span>Tokyo, Japan</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="img-wrap">
                                    <img src="http://placehold.it/165x140" alt="image">
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed
                                finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu,
                                efficitur metus.</p>
                            <h3>Sed sollicitudin tempor:</h3>
                            <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit
                                bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin non sem
                                    feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis ullamcorper
                                lorem efficitur, sodales volutpat mi.</p>
                            <div class="flex-image-wrap fh-485">
                                <div class="image-column col-50">
                                    <div class="image-inner"
                                         style="background-image:url(http://placehold.it/285x485)"></div>
                                </div>
                                <div class="image-column col-50">
                                    <div class="image-inner img-h-60"
                                         style="background-image:url(http://placehold.it/285x291)"></div>
                                    <div class="image-inner img-h-40"
                                         style="background-image:url(http://placehold.it/285x195)"></div>
                                </div>
                            </div>

                            <div class="currency-line-block">
                                <div class="block-name">@lang('region.currency')</div>
                                <div class="currency-desc">
                                    <div class="cur-name">Japanese yen</div>
                                    <div class="cur-symbol">@lang('region.symbol'): ¥</div>
                                    <div class="cur-code">Code: JPY</div>
                                </div>
                            </div>

                            <div class="travlog-image-card">
                                <div class="card-part img-wrap">
                                    <img class="back" src="http://placehold.it/295x180" alt="image">
                                    <div class="avatar-img">
                                        <img src="http://placehold.it/70x70?text=avatar" alt="ava" class="ava">
                                    </div>
                                </div>
                                <div class="card-part info-card">
                                    <div class="info-top">
                                        <div class="info-txt">
                                            <div class="name">Stephanie Small</div>
                                            <div class="place">United States</div>
                                        </div>
                                        <div class="btn-wrap">
                                            <button type="button" class="btn btn-light-grey btn-bordered">
                                                @lang('buttons.general.follow')
                                            </button>
                                        </div>
                                    </div>
                                    <ul class="preview-list">
                                        <li><img src="http://placehold.it/80x80" alt="image"></li>
                                        <li><img src="http://placehold.it/80x80" alt="image"></li>
                                        <li><img src="http://placehold.it/80x80" alt="image"></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="travlog-image-card">
                                <div class="card-part img-wrap">
                                    <img class="back" src="http://placehold.it/295x180" alt="image">
                                    <div class="avatar-img">
                                        <img src="http://placehold.it/70x70?text=avatar" alt="ava" class="ava">
                                    </div>
                                </div>
                                <div class="card-part info-card">
                                    <div class="info-top">
                                        <div class="info-txt">
                                            <div class="name">Stephanie Small</div>
                                            <div class="place">United States</div>
                                        </div>
                                        <div class="btn-wrap">
                                            <button type="button" class="btn btn-light-grey btn-bordered">
                                                @lang('buttons.general.follow')
                                            </button>
                                        </div>
                                    </div>
                                    <ul class="preview-list">
                                        <li><img src="http://placehold.it/80x80" alt="image"></li>
                                        <li><img src="http://placehold.it/80x80" alt="image"></li>
                                        <li><img src="http://placehold.it/80x80" alt="image"></li>
                                    </ul>
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed
                                finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu,
                                efficitur metus.</p>
                            <h3>Sed sollicitudin tempor:</h3>
                            <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit
                                bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin non sem
                                    feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis ullamcorper
                                lorem efficitur, sodales volutpat mi.</p>
                            <div class="post-image-container post-follow-container travlog-map-wrap">
                                <div class="post-map-wrap">
                                    <div class="post-map-inner">
                                        <img src="./assets/image/trip-plan-image.jpg" alt="map">
                                        <div class="destination-point" style="top:80px;left: 20%;">
                                            <img class="map-marker mCS_img_loaded" src="./assets/image/marker.png"
                                                 alt="marker">
                                        </div>
                                    </div>
                                    <div class="info-caption">
                                        <div class="caption-txt">
                                            <div class="info-block">
                                                <div class="info-flag">
                                                    <img class="flag" src="http://placehold.it/60x42?text=flag"
                                                         alt="flag">
                                                </div>
                                                <div class="info-txt">
                                                    <div class="info-name">Japan</div>
                                                    <div class="info-sub">@lang('region.country_in_name', ['name' => 'Asia'])</div>
                                                </div>
                                            </div>
                                            <div class="info-block">
                                                <div class="info-icon">
                                                    <i class="trav-popularity-icon"></i>
                                                </div>
                                                <div class="info-txt">
                                                    <div class="info-ttl">321.4 M</div>
                                                    <div class="info-sub">@lang('region.population')</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-wrap">
                                            <button type="button" class="btn btn-light-grey btn-bordered">
                                                @lang('buttons.general.follow')
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed
                                finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu,
                                efficitur metus.</p>
                            <div class="post-top-info-layer post-footer-info foot-place">
                                <div class="info-side">
                                    <div class="post-foot-block post-reaction">
                                        <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                                        <span><b>6</b> @lang('report.reactions')</span>
                                    </div>
                                    <div class="post-foot-block">
                                        <i class="trav-comment-icon"></i>
                                        <ul class="foot-avatar-list">
                                            <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                        -->
                                            <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                        -->
                                            <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                        </ul>
                                        <span>@choice('comment.count_comment', 20, ['count' => 20])</span>
                                    </div>
                                </div>
                                <div class="actions-side">
                                    <div class="dropdown">
                                        <a href="#" class="top-action" data-toggle="dropdown" aria-haspopup="true"
                                           aria-expanded="false">
                                            <i class="trav-share-icon"></i>
                                            <span>@lang('profile.share')</span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-left dropdown-arrow">
                                            <a class="dropdown-item with-collapse" data-toggle="collapse"
                                               href="#collapseShareOnline" role="button" aria-expanded="false"
                                               aria-controls="collapseShareOnline">
                          <span class="icon-wrap">
                            <i class="trav-share-online-icon"></i>
                          </span>
                                                <div class="drop-txt">
                                                    <p><b>@lang('report.share_online')</b></p>
                                                    <p>Spread the word</p>
                                                </div>
                                            </a>
                                            <div class="collapse" id="collapseShareOnline">
                                                <ul class="share-link-list">
                                                    <li>
                                                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="instagram"><i
                                                                    class="fa fa-instagram"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="google-plus"><i
                                                                    class="fa fa-google-plus"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="pinterest"><i
                                                                    class="fa fa-pinterest"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="tumblr"><i class="fa fa-tumblr"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a class="dropdown-item" href="#">
                          <span class="icon-wrap">
                            <i class="trav-embed-icon"></i>
                          </span>
                                                <div class="drop-txt">
                                                    <p><b>@lang('report.embed')</b></p>
                                                    <p>@lang('report.save_it_for_later')</p>
                                                </div>
                                            </a>
                                            <a class="dropdown-item" href="#">
                          <span class="icon-wrap">
                            <i class="trav-share-on-travo-icon"></i>
                          </span>
                                                <div class="drop-txt">
                                                    <p><b>@lang('report.share_on_travooo')</b></p>
                                                    <p>@lang('report.help_your_friends')</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <a href="#" class="top-action">
                                        <i class="trav-@lang('buttons.general.save')-icon"></i>
                                        <span>@lang('buttons.general.save')</span>
                                    </a>
                                </div>
                            </div>
                            <div class="foot-author-info">
                                <div class="info-block">
                                    <div class="img-wrap">
                                        <img class="ava" src="http://placehold.it/50x50" alt="avatar">
                                    </div>
                                    <div class="info-txt">
                                        <div class="name">
                                            <span>@lang('other.by') </span>
                                            <b>Thomas Wright</b>
                                        </div>
                                        <div class="location">
                                            <span>@lang('report.expert_in') </span>
                                            <b>France</b>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-grey btn-bordered">
                                        @lang('buttons.general.follow')
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-comment-layer-wrap">
                        <div class="post-comment-layer">
                            <div class="post-add-comment-block">
                                <div class="avatar-wrap">
                                    <img src="http://placehold.it/45x45" alt="">
                                </div>
                                <div class="post-add-com-input">
                                    <input type="text" placeholder="@lang('comment.write_a_comment')">
                                </div>
                            </div>
                            <div class="post-comment-top-info">
                                <ul class="comment-filter">
                                    <li class="active">@lang('other.top')</li>
                                    <li>@lang('other.new')</li>
                                </ul>
                                <div class="comm-count-info">
                                    3 / 20
                                </div>
                            </div>
                            <div class="post-comment-wrapper">
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>21</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Amine</a>
                                            <a href="#" class="comment-nickname">@ak0117</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                doloribus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-like.png" alt="">
                                                <span>19</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                delectus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>15</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="more-travlog-block">
                <h3 class="title">More travelogs</h3>
                <div class="travlog-list">
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="country-name">Japan</div>
                                <div class="info-title">10 Things You Must Know Before...</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="img-wrap">
                                            <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                        </div>
                                        <div class="author-info">
                                            <div class="author-name">
                                                <a href="#" class="author-link">Thomas Wright</a>
                                            </div>
                                            <div class="time">
                                                <span>Today at </span>
                                                <b>5:29 pm</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>6</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <span>20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="country-name">Japan</div>
                                <div class="info-title">10 Things You Must Know Before...</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="img-wrap">
                                            <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                        </div>
                                        <div class="author-info">
                                            <div class="author-name">
                                                <a href="#" class="author-link">Thomas Wright</a>
                                            </div>
                                            <div class="time">
                                                <span>Today at </span>
                                                <b>5:29 pm</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>6</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <span>20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="country-name">Japan</div>
                                <div class="info-title">10 Things You Must Know Before...</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="img-wrap">
                                            <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                        </div>
                                        <div class="author-info">
                                            <div class="author-name">
                                                <a href="#" class="author-link">Thomas Wright</a>
                                            </div>
                                            <div class="time">
                                                <span>Today at </span>
                                                <b>5:29 pm</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>6</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <span>20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="country-name">Japan</div>
                                <div class="info-title">10 Things You Must Know Before...</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="img-wrap">
                                            <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                        </div>
                                        <div class="author-info">
                                            <div class="author-name">
                                                <a href="#" class="author-link">Thomas Wright</a>
                                            </div>
                                            <div class="time">
                                                <span>Today at </span>
                                                <b>5:29 pm</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>6</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <span>20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="country-name">Japan</div>
                                <div class="info-title">10 Things You Must Know Before...</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="img-wrap">
                                            <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                        </div>
                                        <div class="author-info">
                                            <div class="author-name">
                                                <a href="#" class="author-link">Thomas Wright</a>
                                            </div>
                                            <div class="time">
                                                <span>Today at </span>
                                                <b>5:29 pm</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>6</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <span>20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="country-name">Japan</div>
                                <div class="info-title">10 Things You Must Know Before...</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="img-wrap">
                                            <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                        </div>
                                        <div class="author-info">
                                            <div class="author-name">
                                                <a href="#" class="author-link">Thomas Wright</a>
                                            </div>
                                            <div class="time">
                                                <span>Today at </span>
                                                <b>5:29 pm</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>6</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <span>20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- map popup -->
<div class="modal fade white-style" data-backdrop="false" id="mapPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1080" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog inner-map-travlog">
                <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                    <i class="trav-close-icon"></i>
                </button>

                <div class="post-image-container post-follow-container travlog-map-wrap">
                    <div class="post-map-wrap">
                        <div class="post-map-inner">
                            <img src="./assets/image/popup-map.jpg" alt="map">
                            <div class="destination-point" style="top:80px;left: 20%;">
                                <img class="map-marker mCS_img_loaded" src="./assets/image/marker.png" alt="marker">
                            </div>
                        </div>
                        <div class="info-caption">
                            <div class="caption-txt">
                                <div class="info-block">
                                    <div class="info-flag">
                                        <img class="flag" src="http://placehold.it/60x42?text=flag" alt="flag">
                                    </div>
                                    <div class="info-txt">
                                        <div class="info-name">Japan</div>
                                        <div class="info-sub">@lang('region.country_in_name', ['name' => 'Asia'])</div>
                                    </div>
                                </div>
                                <div class="info-block">
                                    <div class="info-icon">
                                        <i class="trav-popularity-icon"></i>
                                    </div>
                                    <div class="info-txt">
                                        <div class="info-ttl">321.4 M</div>
                                        <div class="info-sub">@lang('region.population')</div>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-wrap">
                                <button type="button" class="btn btn-light-grey btn-bordered">
                                    @lang('buttons.general.follow')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- comments popup -->
<div class="modal fade white-style" data-backdrop="false" id="commentsPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-765" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-modal-comment travlog-comment">
                <div class="post-comment-main">
                    <div class="top-banner-block top-travlog-banner"
                         style="background-image: url(./assets/image/travlog-banner.jpg)">
                        <div class="top-banner-inner">
                            <div class="travel-left-info">
                                <h3 class="travel-title">From Moroco to the Amazing Japan in 7 Days</h3>
                                <div class="travlog-user-info">
                                    <div class="user-info-wrap">
                                        <div class="avatar-wrap">
                                            <img src="http://placehold.it/50x50" alt="">
                                        </div>
                                        <div class="info-txt">
                                            <div class="top-name">
                                                @lang('other.by') <a class="post-name-link" href="#">Thomas Wright</a>
                                            </div>
                                            <div class="sub-info">
                                                @lang('report.expert_in') <a href="#" class="place-link">France</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="travel-right-info">
                                <ul class="sub-list">
                                    <li>
                                        <div class="top-txt">@lang('region.country_capital')</div>
                                        <div class="sub-txt">Tokyo</div>
                                    </li>
                                    <li>
                                        <div class="top-txt">@lang('region.population')</div>
                                        <div class="sub-txt">127 million</div>
                                    </li>
                                    <li>
                                        <div class="top-txt">@lang('region.currency')</div>
                                        <div class="sub-txt">Japanese yen</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="discussion-block">
                        <span class="disc-ttl">@lang('comment.comments')</span>
                        <ul class="disc-ava-list">
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                            <li><a href="#"><img src="http://placehold.it/32x32" alt="avatar"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="post-comment-layer">
                    <div class="post-comment-top-info">
                        <ul class="comment-filter">
                            <li class="active">@lang('other.top')</li>
                            <li>@lang('other.new')</li>
                        </ul>
                        <div class="comm-count-info">
                            3 / 20
                        </div>
                    </div>
                    <div class="post-comment-wrapper">
                        <div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="http://placehold.it/45x45" alt="">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">Katherin</a>
                                    <a href="#" class="comment-nickname">@katherin</a>
                                </div>
                                <div class="comment-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore
                                        tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-reaction">
                                        <img src="./assets/image/icon-smile.png" alt="">
                                        <span>21</span>
                                    </div>
                                    <div class="com-time">6 hours ago</div>
                                </div>
                            </div>
                        </div>
                        <div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="http://placehold.it/45x45" alt="">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">Amine</a>
                                    <a href="#" class="comment-nickname">@ak0117</a>
                                </div>
                                <div class="comment-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-reaction">
                                        <img src="./assets/image/icon-like.png" alt="">
                                        <span>19</span>
                                    </div>
                                    <div class="com-time">6 hours ago</div>
                                </div>
                            </div>
                        </div>
                        <div class="post-comment-row">
                            <div class="post-com-avatar-wrap">
                                <img src="http://placehold.it/45x45" alt="">
                            </div>
                            <div class="post-comment-text">
                                <div class="post-com-name-layer">
                                    <a href="#" class="comment-name">Katherin</a>
                                    <a href="#" class="comment-nickname">@katherin</a>
                                </div>
                                <div class="comment-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore
                                        tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                                </div>
                                <div class="comment-bottom-info">
                                    <div class="com-reaction">
                                        <img src="./assets/image/icon-smile.png" alt="">
                                        <span>15</span>
                                    </div>
                                    <div class="com-time">6 hours ago</div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                    </div>
                    <div class="post-add-comment-block post-add-foot grey-style">
                        <div class="avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-add-com-input">
                            <input type="text" placeholder="@lang('comment.write_a_comment')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- travlog popup -->
<div class="modal fade white-style" data-backdrop="false" id="travlogPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-1030" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog">
                <div class="post-top-info-layer post-footer-info">
                    <div class="info-side">
                        <a href="#" class="back-link"><i class="fa fa-angle-left"></i> Back</a>
                        <div class="post-foot-block post-reaction">
                            <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                            <span><b>6</b> @lang('report.reactions')</span>
                        </div>
                        <div class="post-foot-block">
                            <i class="trav-comment-icon"></i>
                            <ul class="foot-avatar-list">
                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                  -->
                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                  -->
                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                            </ul>
                            <span>@choice('comment.count_comment', 20, ['count' => 20])</span>
                        </div>
                    </div>
                    <div class="actions-side">
                        <div class="dropdown">
                            <a href="#" class="top-action" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                <i class="trav-share-icon"></i>
                                <span>@lang('profile.share')</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-left dropdown-arrow">
                                <a class="dropdown-item with-collapse" data-toggle="collapse"
                                   href="#collapseShareOnline" role="button" aria-expanded="false"
                                   aria-controls="collapseShareOnline">
                    <span class="icon-wrap">
                      <i class="trav-share-online-icon"></i>
                    </span>
                                    <div class="drop-txt">
                                        <p><b>@lang('report.share_online')</b></p>
                                        <p>Spread the word</p>
                                    </div>
                                </a>
                                <div class="collapse" id="collapseShareOnline">
                                    <ul class="share-link-list">
                                        <li>
                                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" class="tumblr"><i class="fa fa-tumblr"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <a class="dropdown-item" href="#">
                    <span class="icon-wrap">
                      <i class="trav-embed-icon"></i>
                    </span>
                                    <div class="drop-txt">
                                        <p><b>@lang('report.embed')</b></p>
                                        <p>@lang('report.save_it_for_later')</p>
                                    </div>
                                </a>
                                <a class="dropdown-item" href="#">
                    <span class="icon-wrap">
                      <i class="trav-share-on-travo-icon"></i>
                    </span>
                                    <div class="drop-txt">
                                        <p><b>@lang('report.share_on_travooo')</b></p>
                                        <p>@lang('report.help_your_friends')</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <a href="#" class="top-action">
                            <i class="trav-@lang('buttons.general.save')-icon"></i>
                            <span>@lang('buttons.general.save')</span>
                        </a>
                    </div>
                </div>
                <div class="top-banner-block top-travlog-banner"
                     style="background-image: url(./assets/image/travlog-banner.jpg)">
                    <div class="top-banner-inner">
                        <div class="travel-left-info">
                            <h3 class="travel-title">From Moroco to the Amazing Japan in 7 Days</h3>
                            <div class="travlog-user-info">
                                <div class="user-info-wrap">
                                    <div class="avatar-wrap">
                                        <img src="http://placehold.it/50x50" alt="">
                                    </div>
                                    <div class="info-txt">
                                        <div class="top-name">
                                            @lang('other.by') <a class="post-name-link" href="#">Thomas Wright</a>
                                        </div>
                                        <div class="sub-info">
                                            @lang('report.expert_in') <a href="#" class="place-link">France</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="info-action">
                                    <button class="btn btn-white-follow">@lang('buttons.general.follow')</button>
                                </div>
                            </div>
                        </div>
                        <div class="travel-right-info">
                            <ul class="sub-list">
                                <li>
                                    <div class="top-txt">@lang('region.country_capital')</div>
                                    <div class="sub-txt">Tokyo</div>
                                </li>
                                <li>
                                    <div class="top-txt">@lang('region.population')</div>
                                    <div class="sub-txt">127 million</div>
                                </li>
                                <li>
                                    <div class="top-txt">@lang('region.currency')</div>
                                    <div class="sub-txt">Japanese yen</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="travlog-main-block">
                    <div class="travlog-side">
                        <div class="user-top-info">
                            <div class="avatar-wrap">
                                <img class="ava" src="http://placehold.it/70x70" alt="">
                            </div>
                            <p class="by-name">@lang('other.by') <a href="#" class="name-link">Thomas</a></p>
                        </div>
                        <ul class="action-list">
                            <li class="dropdown">
                                <a href="#" class="top-action" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    <i class="trav-heart-icon"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-top-right dropdown-arrow">
                                    <ul class="smile-list">
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-smiley.svg" alt="smile">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-eek.svg" alt="smile">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-eye-heart.svg" alt="smile">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-happy-cry.svg" alt="smile">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-angry.svg" alt="smile">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-finger-up.svg" alt="smile">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img class="smile" src="./assets/image/smile-finger-down.svg"
                                                     alt="smile">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="trav-side-comment-icon"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="trav-share-icon"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="trav-@lang('buttons.general.save')-icon"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="main-content">
                        <div class="content-wrapper">
                            <div class="quote-block">
                                Fusce id lacinia odio curabitur tincidunt in diam nec gravida vivamus semper purus
                                justo, vitae suscipit nisi posuere curabitur.
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed
                                finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu,
                                efficitur metus.</p>
                            <h3>Sed sollicitudin tempor:</h3>
                            <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit
                                bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin non sem
                                    feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis ullamcorper
                                lorem efficitur, sodales volutpat mi.</p>
                            <p>Cras dignissim euismod tempor.</p>
                        </div>
                        <div class="image-block">
                            <img src="http://placehold.it/670x520" alt="image">
                            <p class="sub-txt">“Mauris sed felis in nulla” eleifend imperdiet</p>
                        </div>
                        <div class="content-wrapper">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed
                                finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu,
                                efficitur metus.</p>
                            <h3>Sed sollicitudin tempor:</h3>
                            <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit
                                bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin non sem
                                    feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis ullamcorper
                                lorem efficitur, sodales volutpat mi.</p>
                            <p>Cras dignissim euismod tempor.</p>
                            <div class="post-image-container post-follow-container travlog-map-wrap">
                                <div class="post-map-wrap">
                                    <ul class="post-time-trip breadcrumb-out">
                                        <li class="day">@lang('time.day')</li>
                                        <li class="count">1</li>
                                        <li class="count active">2</li>
                                    </ul>
                                    <div class="post-map-inner">
                                        <img src="./assets/image/trip-plan-image.jpg" alt="map">
                                        <div class="destination-point" style="top:80px;left: 20%;">
                                            <img class="map-marker mCS_img_loaded" src="./assets/image/marker.png"
                                                 alt="marker">
                                        </div>
                                    </div>
                                </div>
                                <div class="post-bottom-block">
                                    <div class="bottom-txt-wrap">
                                        <p class="bottom-ttl">Suspendisse mauris felis 5 nulla eleifend</p>
                                        <div class="bottom-sub-txt">
                                            <span>@lang('trip.trip_plan_by') <a href="#" class="name-link">Tim</a> to 7 Destinations in Tokyo on 1 Sep 2019</span>
                                        </div>
                                    </div>
                                    <div class="bottom-btn-wrap">
                                        <button type="button" class="btn btn-light-grey btn-bordered">
                                            @lang('report.view_plan')
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="travlog-card">
                                <div class="text-block">
                                    <div class="title-card">
                                        <span class="tag">@lang('place.hotel')</span>
                                        <a href="#" class="place-link">Suamcorper scelerisque</a>
                                    </div>
                                    <p>Maecenas eget convallis elit, at commodo velit</p>
                                    <div class="visited">
                                        <ul class="foot-avatar-list">
                                            <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li><!--
                        -->
                                            <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li><!--
                        -->
                                            <li><img class="small-ava" src="http://placehold.it/27x27" alt="ava"></li>
                                        </ul>
                                        <span>@lang('report.count_others_have_visited_this_place', ['count' => '+21'])</span>
                                    </div>
                                    <div class="rate-info">
                                        <div class="rate-inner">
                                            <span class="label">4.7 <i class="trav-star-icon"></i></span>
                                            <p><b>@lang('report.excellent')</b></p>
                                            <p>@lang('report.count_review', ['count' => 723])</p>
                                        </div>
                                        <div class="location">
                                            <i class="trav-set-location-icon"></i>
                                            <span>Tokyo, Japan</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="img-wrap">
                                    <img src="http://placehold.it/165x140" alt="image">
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed
                                finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu,
                                efficitur metus.</p>
                            <h3>Sed sollicitudin tempor:</h3>
                            <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit
                                bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin non sem
                                    feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis ullamcorper
                                lorem efficitur, sodales volutpat mi.</p>
                            <div class="flex-image-wrap fh-485">
                                <div class="image-column col-50">
                                    <div class="image-inner"
                                         style="background-image:url(http://placehold.it/285x485)"></div>
                                </div>
                                <div class="image-column col-50">
                                    <div class="image-inner img-h-60"
                                         style="background-image:url(http://placehold.it/285x291)"></div>
                                    <div class="image-inner img-h-40"
                                         style="background-image:url(http://placehold.it/285x195)"></div>
                                </div>
                            </div>

                            <div class="currency-line-block">
                                <div class="block-name">@lang('region.currency')</div>
                                <div class="currency-desc">
                                    <div class="cur-name">Japanese yen</div>
                                    <div class="cur-symbol">@lang('region.symbol'): ¥</div>
                                    <div class="cur-code">Code: JPY</div>
                                </div>
                            </div>

                            <div class="travlog-image-card">
                                <div class="card-part img-wrap">
                                    <img class="back" src="http://placehold.it/295x180" alt="image">
                                    <div class="avatar-img">
                                        <img src="http://placehold.it/70x70?text=avatar" alt="ava" class="ava">
                                    </div>
                                </div>
                                <div class="card-part info-card">
                                    <div class="info-top">
                                        <div class="info-txt">
                                            <div class="name">Stephanie Small</div>
                                            <div class="place">United States</div>
                                        </div>
                                        <div class="btn-wrap">
                                            <button type="button" class="btn btn-light-grey btn-bordered">
                                                @lang('buttons.general.follow')
                                            </button>
                                        </div>
                                    </div>
                                    <ul class="preview-list">
                                        <li><img src="http://placehold.it/80x80" alt="image"></li>
                                        <li><img src="http://placehold.it/80x80" alt="image"></li>
                                        <li><img src="http://placehold.it/80x80" alt="image"></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="travlog-image-card">
                                <div class="card-part img-wrap">
                                    <img class="back" src="http://placehold.it/295x180" alt="image">
                                    <div class="avatar-img">
                                        <img src="http://placehold.it/70x70?text=avatar" alt="ava" class="ava">
                                    </div>
                                </div>
                                <div class="card-part info-card">
                                    <div class="info-top">
                                        <div class="info-txt">
                                            <div class="name">Stephanie Small</div>
                                            <div class="place">United States</div>
                                        </div>
                                        <div class="btn-wrap">
                                            <button type="button" class="btn btn-light-grey btn-bordered">
                                                @lang('buttons.general.follow')
                                            </button>
                                        </div>
                                    </div>
                                    <ul class="preview-list">
                                        <li><img src="http://placehold.it/80x80" alt="image"></li>
                                        <li><img src="http://placehold.it/80x80" alt="image"></li>
                                        <li><img src="http://placehold.it/80x80" alt="image"></li>
                                    </ul>
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed
                                finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu,
                                efficitur metus.</p>
                            <h3>Sed sollicitudin tempor:</h3>
                            <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit
                                bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin non sem
                                    feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis ullamcorper
                                lorem efficitur, sodales volutpat mi.</p>
                            <div class="post-image-container post-follow-container travlog-map-wrap">
                                <div class="post-map-wrap">
                                    <div class="post-map-inner">
                                        <img src="./assets/image/trip-plan-image.jpg" alt="map">
                                        <div class="destination-point" style="top:80px;left: 20%;">
                                            <img class="map-marker mCS_img_loaded" src="./assets/image/marker.png"
                                                 alt="marker">
                                        </div>
                                    </div>
                                    <div class="info-caption">
                                        <div class="caption-txt">
                                            <div class="info-block">
                                                <div class="info-flag">
                                                    <img class="flag" src="http://placehold.it/60x42?text=flag"
                                                         alt="flag">
                                                </div>
                                                <div class="info-txt">
                                                    <div class="info-name">Japan</div>
                                                    <div class="info-sub">@lang('region.country_in_name', ['name' => 'Asia'])</div>
                                                </div>
                                            </div>
                                            <div class="info-block">
                                                <div class="info-icon">
                                                    <i class="trav-popularity-icon"></i>
                                                </div>
                                                <div class="info-txt">
                                                    <div class="info-ttl">321.4 M</div>
                                                    <div class="info-sub">@lang('region.population')</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-wrap">
                                            <button type="button" class="btn btn-light-grey btn-bordered">
                                                @lang('buttons.general.follow')
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem. Sed
                                finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc eu,
                                efficitur metus.</p>
                            <div class="post-top-info-layer post-footer-info foot-place">
                                <div class="info-side">
                                    <div class="post-foot-block post-reaction">
                                        <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                                        <span><b>6</b> @lang('report.reactions')</span>
                                    </div>
                                    <div class="post-foot-block">
                                        <i class="trav-comment-icon"></i>
                                        <ul class="foot-avatar-list">
                                            <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                        -->
                                            <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li><!--
                        -->
                                            <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava"></li>
                                        </ul>
                                        <span>@choice('comment.count_comment', 20, ['count' => 20])</span>
                                    </div>
                                </div>
                                <div class="actions-side">
                                    <div class="dropdown">
                                        <a href="#" class="top-action" data-toggle="dropdown" aria-haspopup="true"
                                           aria-expanded="false">
                                            <i class="trav-share-icon"></i>
                                            <span>@lang('profile.share')</span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-left dropdown-arrow">
                                            <a class="dropdown-item with-collapse" data-toggle="collapse"
                                               href="#collapseShareOnline" role="button" aria-expanded="false"
                                               aria-controls="collapseShareOnline">
                          <span class="icon-wrap">
                            <i class="trav-share-online-icon"></i>
                          </span>
                                                <div class="drop-txt">
                                                    <p><b>@lang('report.share_online')</b></p>
                                                    <p>Spread the word</p>
                                                </div>
                                            </a>
                                            <div class="collapse" id="collapseShareOnline">
                                                <ul class="share-link-list">
                                                    <li>
                                                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="instagram"><i
                                                                    class="fa fa-instagram"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="google-plus"><i
                                                                    class="fa fa-google-plus"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="pinterest"><i
                                                                    class="fa fa-pinterest"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="tumblr"><i class="fa fa-tumblr"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a class="dropdown-item" href="#">
                          <span class="icon-wrap">
                            <i class="trav-embed-icon"></i>
                          </span>
                                                <div class="drop-txt">
                                                    <p><b>@lang('report.embed')</b></p>
                                                    <p>@lang('report.save_it_for_later')</p>
                                                </div>
                                            </a>
                                            <a class="dropdown-item" href="#">
                          <span class="icon-wrap">
                            <i class="trav-share-on-travo-icon"></i>
                          </span>
                                                <div class="drop-txt">
                                                    <p><b>@lang('report.share_on_travooo')</b></p>
                                                    <p>@lang('report.help_your_friends')</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <a href="#" class="top-action">
                                        <i class="trav-@lang('buttons.general.save')-icon"></i>
                                        <span>@lang('buttons.general.save')</span>
                                    </a>
                                </div>
                            </div>
                            <div class="foot-author-info">
                                <div class="info-block">
                                    <div class="img-wrap">
                                        <img class="ava" src="http://placehold.it/50x50" alt="avatar">
                                    </div>
                                    <div class="info-txt">
                                        <div class="name">
                                            <span>@lang('other.by') </span>
                                            <b>Thomas Wright</b>
                                        </div>
                                        <div class="location">
                                            <span>@lang('report.expert_in') </span>
                                            <b>France</b>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button type="button" class="btn btn-light-grey btn-bordered">
                                        @lang('buttons.general.follow')
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-comment-layer-wrap">
                        <div class="post-comment-layer">
                            <div class="post-add-comment-block">
                                <div class="avatar-wrap">
                                    <img src="http://placehold.it/45x45" alt="">
                                </div>
                                <div class="post-add-com-input">
                                    <input type="text" placeholder="@lang('comment.write_a_comment')">
                                </div>
                            </div>
                            <div class="post-comment-top-info">
                                <ul class="comment-filter">
                                    <li class="active">@lang('other.top')</li>
                                    <li>New</li>
                                </ul>
                                <div class="comm-count-info">
                                    3 / 20
                                </div>
                            </div>
                            <div class="post-comment-wrapper">
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>21</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Amine</a>
                                            <a href="#" class="comment-nickname">@ak0117</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                doloribus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-like.png" alt="">
                                                <span>19</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                delectus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>15</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="more-travlog-block">
                <h3 class="title">More travelogs</h3>
                <div class="travlog-list">
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="country-name">Japan</div>
                                <div class="info-title">10 Things You Must Know Before...</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="img-wrap">
                                            <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                        </div>
                                        <div class="author-info">
                                            <div class="author-name">
                                                <a href="#" class="author-link">Thomas Wright</a>
                                            </div>
                                            <div class="time">
                                                <span>Today at </span>
                                                <b>5:29 pm</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>6</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <span>20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="country-name">Japan</div>
                                <div class="info-title">10 Things You Must Know Before...</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="img-wrap">
                                            <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                        </div>
                                        <div class="author-info">
                                            <div class="author-name">
                                                <a href="#" class="author-link">Thomas Wright</a>
                                            </div>
                                            <div class="time">
                                                <span>Today at </span>
                                                <b>5:29 pm</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>6</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <span>20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="country-name">Japan</div>
                                <div class="info-title">10 Things You Must Know Before...</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="img-wrap">
                                            <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                        </div>
                                        <div class="author-info">
                                            <div class="author-name">
                                                <a href="#" class="author-link">Thomas Wright</a>
                                            </div>
                                            <div class="time">
                                                <span>Today at </span>
                                                <b>5:29 pm</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>6</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <span>20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="country-name">Japan</div>
                                <div class="info-title">10 Things You Must Know Before...</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="img-wrap">
                                            <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                        </div>
                                        <div class="author-info">
                                            <div class="author-name">
                                                <a href="#" class="author-link">Thomas Wright</a>
                                            </div>
                                            <div class="time">
                                                <span>Today at </span>
                                                <b>5:29 pm</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>6</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <span>20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="country-name">Japan</div>
                                <div class="info-title">10 Things You Must Know Before...</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="img-wrap">
                                            <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                        </div>
                                        <div class="author-info">
                                            <div class="author-name">
                                                <a href="#" class="author-link">Thomas Wright</a>
                                            </div>
                                            <div class="time">
                                                <span>Today at </span>
                                                <b>5:29 pm</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>6</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <span>20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="country-name">Japan</div>
                                <div class="info-title">10 Things You Must Know Before...</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="img-wrap">
                                            <img class="ava" src="http://placehold.it/45x45" alt="avatar">
                                        </div>
                                        <div class="author-info">
                                            <div class="author-name">
                                                <a href="#" class="author-link">Thomas Wright</a>
                                            </div>
                                            <div class="time">
                                                <span>Today at </span>
                                                <b>5:29 pm</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>6</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <span>20</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- create travlog step 2 popup -->
<div class="modal fade white-style" data-backdrop="false" id="createTravlogPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                <div class="top-title-layer">
                    <h3 class="title">
                        <span class="circle">1</span>
                        <span class="txt">Travelog Details</span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="travlog-details-block">
                    <div class="detail-row">
                        <div class="title-block">
                            <div class="title">@lang('other.report') Title</div>
                            <div class="subtitle">Lorem ipsut dolor amet</div>
                        </div>
                        <div class="input-layer">
                            <div class="input-block">
                                <input class="detail-input" type="text"
                                       placeholder="USA, Canada or Japan? Where to go this summer">
                            </div>
                        </div>
                        <div class="post-title-label">
                            <div class="label-txt">MAX characters: 50</div>
                        </div>
                    </div>
                    <div class="detail-row">
                        <div class="title-block">
                            <div class="title">Description</div>
                            <div class="subtitle">Pellentesque id felis quis turpis volutpat molestie quis in est</div>
                        </div>
                        <div class="input-layer">
                            <div class="input-block">
                                <textarea class="detail-input" name="" id="" cols="30" rows="5"
                                          placeholder="Short description (optional)"></textarea>
                            </div>
                        </div>
                        <div class="post-title-label">
                            <div class="label-txt">MAX characters: 260</div>
                        </div>
                    </div>
                    <div class="detail-row">
                        <div class="title-block">
                            <div class="select-wrap">
                                <select name="" id="">
                                    <option value="">Country</option>
                                    <option value="usa">USA</option>
                                </select>
                            </div>
                            <div class="subtitle">Pellentesque id felis quis turpis volutpat molestie quis in est</div>
                        </div>
                        <div class="input-layer">
                            <div class="input-block">
                                <div class="select-country-block">
                                    <div class="search-country-input">
                                        <input class="detail-input" type="text"
                                               placeholder="Type to search for a country...">
                                    </div>
                                    <div class="country-map-block">
                                        <img class="map" src="http://placehold.it/520x310?text=country map" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="post-title-label">
                            <div class="label-txt">Selected countries:</div>
                            <ul class="selected-country">
                                <li>Japan</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="post-foot-btn">
                    <button class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                    <button class="btn btn-light-primary btn-bordered" data-toggle="modal"
                            data-target="#createTravlogNextPopup">Next
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- create travlog popup -->
<div class="modal fade modal-child white-style" data-modal-parent="#createTravlogPopup" data-backdrop="false"
     id="createTravlogNextPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                <div class="top-title-layer">
                    <h3 class="title">
                        <span class="circle">2</span>
                        <span class="txt">Creating a Travelog</span>
                    </h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="top-travlog-banner">
                    <div class="top-banner-inner">
                        <div class="file-wrap">
                            <input type="file" class="file-input" id="fileInput">
                            <a href="#" class="file-input-handler" id="fileInputHandler">
                                <i class="trav-upload-file-icon"></i>
                            </a>
                        </div>
                        <p>Drag or click to add cover photo for your Travelog</p>
                    </div>
                </div>
                <div class="travlog-main-block">
                    <div class="main-content">
                        <div class="create-row">
                            <div class="side left">
                                <div class="label">Title</div>
                            </div>
                            <div class="content">
                                <div class="input-block">
                                    <input type="text" class="input" value="10 Tips to Experience Tokyo to the Fullest"
                                           placeholder="Type a title">
                                </div>
                            </div>
                        </div>
                        <div class="create-row">
                            <div class="content">
                                <div class="img-wrap">
                                    <img src="http://placehold.it/590x400" alt="image">
                                </div>
                            </div>
                        </div>
                        <div class="create-row">
                            <div class="side left">
                                <div class="close-handle">
                                    <i class="trav-close-icon"></i>
                                </div>
                            </div>
                            <div class="content">
                                <ul class="action-list">
                                    <li><a href="#"><i class="trav-text-icon"></i></a></li>

                                    <li class="dropdown dropup">
                                        <a href="#" class="" aria-haspopup="true" aria-expanded="false">
                                            <i class="trav-camera"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-arrow-bottom dropdown-top-location">
                                            <button class="dropdown-item" type="button">Upload image</button>
                                            <button class="dropdown-item" type="button" data-toggle="modal"
                                                    data-target="#selectFromLibraryPopup">Select from library
                                            </button>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="trav-play-video-icon"></i></a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#addTripPlanPopup"><i
                                                    class="trav-create-destination-icon"></i></a></li>
                                    <li><a href="#"><i class="trav-popularity-icon"></i></a></li>
                                    <li><a href="#"><i class="trav-map-o"></i></a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#addTravoooInfoPopup"><i
                                                    class="trav-info-icon"></i></a></li>
                                    <li><a href="#"><i class="trav-map-marker-icon"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="create-row">
                            <div class="side left">
                                <div class="label">Video</div>
                            </div>
                            <div class="content">
                                <div class="input-block">
                                    <input type="text" class="input"
                                           placeholder="Past a video link from YouTube, Vimeo and press Enter">
                                </div>
                            </div>
                        </div>
                        <div class="create-row">
                            <div class="side left">
                                <div class="label">Text</div>
                            </div>
                            <div class="content">
                                <div class="input-block">
                                    <input type="text" class="input" placeholder="Start writing your text here...">
                                </div>
                            </div>
                        </div>

                        <div class="create-row">
                            <div class="image-block">
                                <ul class="image-control-list">
                                    <li><a href="#" class="first-view"><span></span></a></li>
                                    <li><a href="#" class="second-view active"><span></span></a></li>
                                    <li><a href="#" class="third-view"><span></span></a></li>
                                </ul>
                                <img src="http://placehold.it/670x520" alt="image">
                                <p class="sub-txt">“Mauris sed felis in nulla” eleifend imperdiet</p>
                            </div>
                        </div>
                        <div class="create-row">
                            <div class="side left">
                                <div class="label">Text</div>
                            </div>
                            <div class="content">
                                <div class="content-wrapper">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat lorem.
                                        Sed finibus rutrum commodo. Pellentesque dignissim lorem convallis, commodo nunc
                                        eu, efficitur metus. <span class="select-text">select this text</span></p>
                                    <h3>Sed sollicitudin tempor:</h3>
                                    <p>suspendisse commodo erat a aliquam placerat. Vestibulum congue tellus in blandit
                                        bibendum. Donec ut lorem molestie, pulvinar purus eget, pulvinar ex. <i>Proin
                                            non sem feugiat</i>, rutrum magna at, commodo enim. Donec nunc sem, iaculis
                                        ullamcorper lorem efficitur, sodales volutpat mi.</p>
                                    <p>Cras dignissim euismod tempor. <span class="add-name">@Mi</span></p>
                                </div>
                            </div>
                        </div>

                        <div class="create-row">
                            <div class="side left">
                                <div class="label">Video</div>
                            </div>
                            <div class="content">
                                <div class="video-block">
                                    <img src="http://placehold.it/590x335" alt="video">
                                </div>
                            </div>
                            <div class="side right">
                                <ul class="video-upload-control">
                                    <li><a href="#">
                                            <i class="trav-move-icon"></i>
                                            <span>Move</span>
                                        </a></li>
                                    <li><a href="#">
                                            <div class="close-handle red">
                                                <i class="trav-close-icon"></i>
                                            </div>
                                            <span>@lang('buttons.general.crud.delete')</span>
                                        </a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="create-row">
                            <div class="side left">
                                <div class="label">Map</div>
                            </div>
                            <div class="content">
                                <div class="input-block">
                                    <input type="text" class="input" placeholder="City, Country or Place...">
                                </div>
                            </div>
                        </div>

                        <div class="create-row">
                            <div class="side left">
                                <div class="label">@lang('trip.trip_plan')</div>
                            </div>
                            <div class="content">
                                <div class="post-image-container post-follow-container travlog-map-wrap">
                                    <div class="post-map-wrap">
                                        <ul class="post-time-trip breadcrumb-out">
                                            <li class="day">@lang('time.day')</li>
                                            <li class="count">1</li>
                                            <li class="count active">2</li>
                                        </ul>
                                        <div class="post-map-inner">
                                            <img src="./assets/image/trip-plan-image.jpg" alt="map">
                                            <div class="destination-point" style="top:80px;left: 20%;">
                                                <img class="map-marker mCS_img_loaded" src="./assets/image/marker.png"
                                                     alt="marker">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-bottom-block">
                                        <div class="bottom-txt-wrap">
                                            <p class="bottom-ttl">Suspendisse mauris felis 5 nulla eleifend</p>
                                            <div class="bottom-sub-txt">
                                                <span>@lang('trip.trip_plan_by') <a href="#" class="name-link">Tim</a> to 7 Destinations in Tokyo on 1 Sep 2019</span>
                                            </div>
                                        </div>
                                        <div class="bottom-btn-wrap">
                                            <button type="button" class="btn btn-light-grey btn-bordered">
                                                @lang('report.view_plan')
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="create-row">
                            <div class="side left">
                                <div class="label">People</div>
                            </div>
                            <div class="content">
                                <div class="input-block">
                                    <input type="text" class="input" placeholder="Type a name...">
                                </div>
                            </div>
                        </div>

                        <div class="create-row">
                            <div class="side left">
                                <div class="label">Travooo Info</div>
                            </div>
                            <div class="content">
                                <div class="currency-line-block">
                                    <div class="block-name">@lang('region.currency')</div>
                                    <div class="currency-desc">
                                        <div class="cur-name">Japanese yen</div>
                                        <div class="cur-symbol">@lang('region.symbol'): ¥</div>
                                        <div class="cur-code">Code: JPY</div>
                                    </div>
                                </div>
                                <div class="input-block">
                                    <input type="text" class="input" placeholder="Add another Travooo info...">
                                </div>
                            </div>
                        </div>

                        <div class="create-row">
                            <div class="side left">
                                <div class="label">People</div>
                            </div>
                            <div class="content">
                                <div class="travlog-image-card">
                                    <div class="card-part img-wrap">
                                        <img class="back" src="http://placehold.it/295x180" alt="image">
                                        <div class="avatar-img">
                                            <img src="http://placehold.it/70x70?text=avatar" alt="ava" class="ava">
                                        </div>
                                    </div>
                                    <div class="card-part info-card">
                                        <div class="info-top">
                                            <div class="info-txt">
                                                <div class="name">Stephanie Small</div>
                                                <div class="place">United States</div>
                                            </div>
                                            <div class="btn-wrap">
                                                <button type="button" class="btn btn-light-grey btn-bordered">
                                                    @lang('buttons.general.follow')
                                                </button>
                                            </div>
                                        </div>
                                        <ul class="preview-list">
                                            <li><img src="http://placehold.it/80x80" alt="image"></li>
                                            <li><img src="http://placehold.it/80x80" alt="image"></li>
                                            <li><img src="http://placehold.it/80x80" alt="image"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="create-row">
                            <div class="side left">
                                <div class="label">Map</div>
                            </div>
                            <div class="content">
                                <div class="post-image-container post-follow-container travlog-map-wrap">
                                    <div class="post-map-wrap">
                                        <div class="post-map-inner">
                                            <img src="./assets/image/trip-plan-image.jpg" alt="map">
                                            <div class="destination-point" style="top:80px;left: 20%;">
                                                <img class="map-marker mCS_img_loaded" src="./assets/image/marker.png"
                                                     alt="marker">
                                            </div>
                                        </div>
                                        <div class="info-caption">
                                            <div class="caption-txt">
                                                <div class="info-block">
                                                    <div class="info-flag">
                                                        <img class="flag" src="http://placehold.it/60x42?text=flag"
                                                             alt="flag">
                                                    </div>
                                                    <div class="info-txt">
                                                        <div class="info-name">Japan</div>
                                                        <div class="info-sub">@lang('region.country_in_name', ['name' => 'Asia'])</div>
                                                    </div>
                                                </div>
                                                <div class="info-block">
                                                    <div class="info-icon">
                                                        <i class="trav-popularity-icon"></i>
                                                    </div>
                                                    <div class="info-txt">
                                                        <div class="info-ttl">321.4 M</div>
                                                        <div class="info-sub">@lang('region.population')</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="btn-wrap">
                                                <button type="button" class="btn btn-light-grey btn-bordered">
                                                    @lang('buttons.general.follow')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="create-row">
                            <div class="side left">
                                <div class="label">@choice('trip.place', 2)</div>
                            </div>
                            <div class="content">
                                <div class="search-place">
                                    <div class="input-block">
                                        <input type="text" class="input" id="searchTravlogCard"
                                               placeholder="Type a place...">
                                    </div>
                                    <div class="travlog-card-wrap mCustomScrollbar">
                                        <div class="travlog-card">
                                            <div class="text-block">
                                                <div class="title-card">
                                                    <span class="tag">@lang('place.hotel')</span>
                                                    <a href="#" class="place-link">Suamcorper scelerisque</a>
                                                </div>
                                                <p>Maecenas eget convallis elit, at commodo velit</p>
                                                <div class="visited">
                                                    <ul class="foot-avatar-list">
                                                        <li><img class="small-ava" src="http://placehold.it/27x27"
                                                                 alt="ava"></li><!--
                              -->
                                                        <li><img class="small-ava" src="http://placehold.it/27x27"
                                                                 alt="ava"></li><!--
                              -->
                                                        <li><img class="small-ava" src="http://placehold.it/27x27"
                                                                 alt="ava"></li>
                                                    </ul>
                                                    <span>@lang('report.count_others_have_visited_this_place', ['count' => '+21'])</span>
                                                </div>
                                                <div class="rate-info">
                                                    <div class="rate-inner">
                                                        <span class="label">4.7 <i class="trav-star-icon"></i></span>
                                                        <p><b>@lang('report.excellent')</b></p>
                                                        <p>@lang('report.count_review', ['count' => 723])</p>
                                                    </div>
                                                    <div class="location">
                                                        <i class="trav-set-location-icon"></i>
                                                        <span>Tokyo, Japan</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x140" alt="image">
                                            </div>
                                        </div>
                                        <div class="travlog-card">
                                            <div class="text-block">
                                                <div class="title-card">
                                                    <span class="tag">@lang('place.hotel')</span>
                                                    <a href="#" class="place-link">Suamcorper scelerisque</a>
                                                </div>
                                                <p>Maecenas eget convallis elit, at commodo velit</p>
                                                <div class="visited">
                                                    <ul class="foot-avatar-list">
                                                        <li><img class="small-ava" src="http://placehold.it/27x27"
                                                                 alt="ava"></li><!--
                              -->
                                                        <li><img class="small-ava" src="http://placehold.it/27x27"
                                                                 alt="ava"></li><!--
                              -->
                                                        <li><img class="small-ava" src="http://placehold.it/27x27"
                                                                 alt="ava"></li>
                                                    </ul>
                                                    <span>@lang('report.count_others_have_visited_this_place', ['count' => '+21'])</span>
                                                </div>
                                                <div class="rate-info">
                                                    <div class="rate-inner">
                                                        <span class="label">4.7 <i class="trav-star-icon"></i></span>
                                                        <p><b>@lang('report.excellent')</b></p>
                                                        <p>@lang('report.count_review', ['count' => 723])</p>
                                                    </div>
                                                    <div class="location">
                                                        <i class="trav-set-location-icon"></i>
                                                        <span>Tokyo, Japan</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x140" alt="image">
                                            </div>
                                        </div>
                                        <div class="travlog-card">
                                            <div class="text-block">
                                                <div class="title-card">
                                                    <span class="tag">@lang('place.hotel')</span>
                                                    <a href="#" class="place-link">Suamcorper scelerisque</a>
                                                </div>
                                                <p>Maecenas eget convallis elit, at commodo velit</p>
                                                <div class="visited">
                                                    <ul class="foot-avatar-list">
                                                        <li><img class="small-ava" src="http://placehold.it/27x27"
                                                                 alt="ava"></li><!--
                              -->
                                                        <li><img class="small-ava" src="http://placehold.it/27x27"
                                                                 alt="ava"></li><!--
                              -->
                                                        <li><img class="small-ava" src="http://placehold.it/27x27"
                                                                 alt="ava"></li>
                                                    </ul>
                                                    <span>@lang('report.count_others_have_visited_this_place', ['count' => '+21'])</span>
                                                </div>
                                                <div class="rate-info">
                                                    <div class="rate-inner">
                                                        <span class="label">4.7 <i class="trav-star-icon"></i></span>
                                                        <p><b>@lang('report.excellent')</b></p>
                                                        <p>@lang('report.count_review', ['count' => 723])</p>
                                                    </div>
                                                    <div class="location">
                                                        <i class="trav-set-location-icon"></i>
                                                        <span>Tokyo, Japan</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="img-wrap">
                                                <img src="http://placehold.it/165x140" alt="image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-foot-btn">
                    <button class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                    <button class="btn btn-light-primary btn-bordered">Publish</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- add trip plan popup -->
<div class="modal fade modal-child white-style" data-modal-parent="#createTravlogNextPopup" data-backdrop="false"
     id="addTripPlanPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-780" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-top-bordered post-request-modal-block post-mobile-full">
                <div class="post-side-top">
                    <div class="post-top-txt travlog-event">
                        <h3 class="side-ttl">@lang('trip.my_trip_plan') <span class="count">16</span></h3>
                        <div class="right-actions">
                            <div class="search-block">
                                <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
                                <input type="text" class="" id="" placeholder="@lang('buttons.general.search_dots')">
                            </div>
                            <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                                <i class="trav-close-icon"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="trip-plan-inner mCustomScrollbar">
                    <div class="trip-plan-row">
                        <div class="trip-check-layer"></div>
                        <div class="trip-plan-inside-block">
                            <div class="trip-plan-map-img">
                                <img src="http://placehold.it/140x150" alt="map-image">
                            </div>
                        </div>
                        <div class="trip-plan-inside-block trip-plan-txt">
                            <div class="trip-plan-txt-inner">
                                <div class="trip-txt-ttl-layer">
                                    <h2 class="trip-ttl">New york city the right way</h2>
                                    <p class="trip-date">18 to 21 Sep 2017</p>
                                </div>
                                <div class="trip-txt-info">
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>4 Days</b></p>
                                            <p>Duration</p>
                                        </div>
                                    </div>
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>237 km</b></p>
                                            <p>@lang('trip.distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-plan-inside-block">
                            <div class="dest-trip-plan">
                                <h3 class="dest-ttl">@lang('trip.destination') <span>6</span></h3>
                                <ul class="dest-image-list">
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="trip-plan-row checked-plan">
                        <div class="trip-check-layer"></div>
                        <div class="trip-plan-inside-block">
                            <div class="trip-plan-map-img">
                                <img src="http://placehold.it/140x150" alt="map-image">
                            </div>
                        </div>
                        <div class="trip-plan-inside-block trip-plan-txt">
                            <div class="trip-plan-txt-inner">
                                <div class="trip-txt-ttl-layer">
                                    <h2 class="trip-ttl">Over the mediterranean sea</h2>
                                    <p class="trip-date">18 to 21 Sep 2017</p>
                                </div>
                                <div class="trip-txt-info">
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>16 Days</b></p>
                                            <p>Duration</p>
                                        </div>
                                    </div>
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>16K km</b></p>
                                            <p>@lang('trip.distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-plan-inside-block">
                            <div class="dest-trip-plan">
                                <h3 class="dest-ttl">@lang('trip.destination') <span>13</span></h3>
                                <ul class="dest-image-list">
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li class="more-photo-link">
                                        <img src="http://placehold.it/52x52" alt="photo">
                                        <span class="cover">+8</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="trip-plan-row">
                        <div class="trip-check-layer"></div>
                        <div class="trip-plan-inside-block">
                            <div class="trip-plan-map-img">
                                <img src="http://placehold.it/140x150" alt="map-image">
                            </div>
                        </div>
                        <div class="trip-plan-inside-block trip-plan-txt">
                            <div class="trip-plan-txt-inner">
                                <div class="trip-txt-ttl-layer">
                                    <h2 class="trip-ttl">New york city the right way</h2>
                                    <p class="trip-date">18 to 21 Sep 2017</p>
                                </div>
                                <div class="trip-txt-info">
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>4 Days</b></p>
                                            <p>Duration</p>
                                        </div>
                                    </div>
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>237 km</b></p>
                                            <p>@lang('trip.distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-plan-inside-block">
                            <div class="dest-trip-plan">
                                <h3 class="dest-ttl">@lang('trip.destination') <span>6</span></h3>
                                <ul class="dest-image-list">
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="trip-plan-row">
                        <div class="trip-check-layer"></div>
                        <div class="trip-plan-inside-block">
                            <div class="trip-plan-map-img">
                                <img src="http://placehold.it/140x150" alt="map-image">
                            </div>
                        </div>
                        <div class="trip-plan-inside-block trip-plan-txt">
                            <div class="trip-plan-txt-inner">
                                <div class="trip-txt-ttl-layer">
                                    <h2 class="trip-ttl">New york city the right way</h2>
                                    <p class="trip-date">18 to 21 Sep 2017</p>
                                </div>
                                <div class="trip-txt-info">
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>4 Days</b></p>
                                            <p>Duration</p>
                                        </div>
                                    </div>
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>237 km</b></p>
                                            <p>@lang('trip.distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-plan-inside-block">
                            <div class="dest-trip-plan">
                                <h3 class="dest-ttl">@lang('trip.destination') <span>6</span></h3>
                                <ul class="dest-image-list">
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li class="more-photo-link">
                                        <img src="http://placehold.it/52x52" alt="photo">
                                        <span class="cover">+2</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="trip-plan-row">
                        <div class="trip-check-layer"></div>
                        <div class="trip-plan-inside-block">
                            <div class="trip-plan-map-img">
                                <img src="http://placehold.it/140x150" alt="map-image">
                            </div>
                        </div>
                        <div class="trip-plan-inside-block trip-plan-txt">
                            <div class="trip-plan-txt-inner">
                                <div class="trip-txt-ttl-layer">
                                    <h2 class="trip-ttl">New york city the right way</h2>
                                    <p class="trip-date">18 to 21 Sep 2017</p>
                                </div>
                                <div class="trip-txt-info">
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-clock-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>4 Days</b></p>
                                            <p>Duration</p>
                                        </div>
                                    </div>
                                    <div class="trip-info">
                                        <div class="icon-wrap">
                                            <i class="trav-distance-icon"></i>
                                        </div>
                                        <div class="trip-info-inner">
                                            <p><b>237 km</b></p>
                                            <p>@lang('trip.distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="trip-plan-inside-block">
                            <div class="dest-trip-plan">
                                <h3 class="dest-ttl">@lang('trip.destination') <span>6</span></h3>
                                <ul class="dest-image-list">
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li>
                                        <img src="http://placehold.it/52x52" alt="photo">
                                    </li>
                                    <li class="more-photo-link">
                                        <img src="http://placehold.it/52x52" alt="photo">
                                        <span class="cover">+2</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-modal-foot">
                    <button class="btn btn-transp btn-clear">@lang('buttons.general.cancel')</button>
                    <button class="btn btn-light-primary btn-bordered">Insert</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- add trip plan popup -->
<div class="modal fade modal-child white-style" data-modal-parent="#createTravlogNextPopup" data-backdrop="false"
     id="selectFromLibraryPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-875" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-top-bordered post-request-modal-block post-mobile-full">
                <div class="post-side-top">
                    <div class="post-top-txt travlog-event">
                        <h3 class="side-ttl">Add Photo To Travelog</h3>
                        <div class="right-actions">
                            <div class="search-block">
                                <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
                                <input type="text" class="" id="" placeholder="@lang('buttons.general.search_dots')">
                            </div>
                            <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                                <i class="trav-close-icon"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="library-inner mCustomScrollbar">
                    <div class="library-wrapper">
                        <div class="img">
                            <img src="http://placehold.it/195x165" alt="image">
                        </div>
                        <div class="img">
                            <img src="http://placehold.it/195x135" alt="image">
                        </div>
                        <div class="img">
                            <img src="http://placehold.it/195x165" alt="image">
                        </div>
                        <div class="img">
                            <img src="http://placehold.it/195x135" alt="image">
                        </div>
                        <div class="img">
                            <img src="http://placehold.it/195x205" alt="image">
                        </div>
                        <div class="img">
                            <img src="http://placehold.it/195x165" alt="image">
                        </div>
                        <div class="img">
                            <img src="http://placehold.it/195x205" alt="image">
                        </div>
                        <div class="img">
                            <img src="http://placehold.it/195x135" alt="image">
                        </div>
                        <div class="img">
                            <img src="http://placehold.it/195x135" alt="image">
                        </div>
                        <div class="img">
                            <img src="http://placehold.it/195x205" alt="image">
                        </div>
                        <div class="img">
                            <img src="http://placehold.it/195x165" alt="image">
                        </div>
                        <div class="img">
                            <img src="http://placehold.it/195x205" alt="image">
                        </div>
                        <div class="img">
                            <img src="http://placehold.it/195x135" alt="image">
                        </div>
                    </div>
                </div>
                <div class="trip-modal-foot">
                    <button class="btn btn-transp btn-clear">Close</button>
                    <button class="btn btn-light-grey">Upload New Photo</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- add trip plan popup -->
<div class="modal fade modal-child white-style" data-modal-parent="#createTravlogNextPopup" data-backdrop="false"
     id="addTravoooInfoPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-780" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-top-bordered post-request-modal-block post-mobile-full">
                <div class="post-side-top">
                    <div class="post-top-txt travlog-event">
                        <h3 class="side-ttl">Add Travooo Info</h3>
                        <div class="right-actions">
                            <div class="search-block">
                                <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
                                <input type="text" class="" id="" placeholder="@lang('buttons.general.search_dots')">
                            </div>
                            <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                                <i class="trav-close-icon"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="post-info-inner">
                    <div class="post-tabs-block">
                        <div class="detail-list">
                            <div class="detail-block current">
                                <b>General facts</b>
                            </div>
                            <div class="detail-block">
                                <b>Emergency Numbers</b>
                            </div>
                            <div class="detail-block">
                                <b>Indexes</b>
                            </div>
                            <div class="detail-block">
                                <b>Daily Costs</b>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-info-wrapper">
                        <div class="travlog-info-block">
                            <div class="title">Nationality</div>
                            <div class="subtitle">American</div>
                        </div>
                        <div class="travlog-info-block">
                            <div class="title">Languages spoken</div>
                            <div class="subtitle">English +3 more</div>
                        </div>
                        <div class="travlog-info-block">
                            <div class="title">Currencies</div>
                            <div class="subtitle">US Dollar (1USD = 9.23MAD)</div>
                        </div>
                        <div class="travlog-info-block">
                            <div class="title">Timings</div>
                            <div class="subtitle">8:03 AM Washington +5 more</div>
                        </div>
                        <div class="travlog-info-block">
                            <div class="title">Religions</div>
                            <div class="subtitle">Christianity @lang('other.count_more', ['count' => '+2'])</div>
                        </div>
                        <div class="travlog-info-block">
                            <div class="title">Phone Code</div>
                            <div class="subtitle">+33</div>
                        </div>
                        <div class="travlog-info-block">
                            <div class="title">Units of measurement</div>
                            <div class="subtitle">Mph, Litre, inch</div>
                        </div>
                        <div class="travlog-info-block active">
                            <div class="title">Working Days</div>
                            <div class="subtitle">Monday to Saturday</div>
                        </div>
                        <div class="travlog-info-block">
                            <div class="title">Transportation Methods</div>
                            <div class="subtitle">Car, Taxi, +2 More</div>
                        </div>
                        <div class="travlog-info-block">
                            <div class="title">Speed Limit</div>
                            <div class="subtitle">Highways 100-120 km/h +6 more</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- share on trav popup -->
<div class="modal fade" data-backdrop="true" id="shareOnTravPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-650" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog-share-style">
                <div class="post-side-top">
                    <h3 class="side-ttl">@lang('report.share_on_travooo')</h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="post-content-inner">
                    <div class="say-block">
                        <div class="img-wrap">
                            <img src="http://placehold.it/38x38" alt="avatar" class="ava">
                        </div>
                        <div class="txt-block">Say something...</div>
                    </div>
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="img-wrap">
                                <img src="http://placehold.it/335x170" alt="photo">
                            </div>
                            <div class="info-wrapper">
                                <div class="info-title">From Moroco to the Amazing Japan in 7 Days</div>
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="author-info">
                                            <div class="author-name">
                                                <span>@lang('other.by')</span>
                                                <a href="#" class="author-link">Thomas</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>12</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <ul class="foot-avatar-list">
                                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava">
                                                </li><!--
                          -->
                                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava">
                                                </li><!--
                          -->
                                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava">
                                                </li>
                                            </ul>
                                            <span>189</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post-foot-btn">
                    <button class="btn btn-transp btn-clear">
                        <i class="trav-earth"></i>
                    </button>
                    <button class="btn btn-light-primary btn-bordered">Post</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- place one day popup -->
<div class="modal fade white-style" data-backdrop="false" id="tripPlanPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-1140 full-height" role="document">
        <ul class="modal-outside-link-list white-bg">
            <li class="outside-link">
                <a href="#">
                    <div class="round-icon">
                        <i class="trav-angle-left"></i>
                    </div>
                    <span>@lang("other.back")</span>
                </a>
            </li>
            <li class="outside-link">
                <a href="#">
                    <div class="round-icon">
                        <i class="trav-flag-icon"></i>
                    </div>
                    <span>@lang('other.report')</span>
                </a>
            </li>
        </ul>
        <div class="modal-custom-block">
            <button class="btn btn-mobile-side comment-toggler" id="commentToggler">
                <i class="trav-comment-icon"></i>
            </button>
            <div class="post-block post-place-plan-block">

                <div class="post-image-container post-follow-container mCustomScrollbar">
                    <div class="post-image-inner">
                        <div class="post-map-wrap">
                            <div class="post-map-breadcrumb">
                                <ul class="breadcrumb-list">
                                    <li><a href="#">@lang('trip.trip_overview')</a></li>
                                    <li><a href="#">Japan</a></li>
                                    <li>@choice('count_day_in', 1, ['count' => 1]) <b>Tokyo</b></li>
                                </ul>
                            </div>
                            <ul class="post-time-trip">
                                <li class="day">@lang('time.day')</li>
                                <li class="count active">6</li>
                                <li class="all">@lang('other.all')</li>
                            </ul>
                            <div class="post-map-inner">
                                <img src="./assets/image/trip-plan-image.jpg" alt="map">
                                <div class="destination-point" style="top:80px;left: 20%;">
                                    <img class="map-marker" src="./assets/image/marker.png" alt="marker">
                                </div>
                                <div class="post-map-info-caption map-blue">
                                    <div class="map-avatar">
                                        <img src="http://placehold.it/25x25" alt="ava">
                                    </div>
                                    <div class="map-label-txt">
                                        Checking on <b>2 Sep</b> at <b>8:30 am</b> and will stay <b>30 min</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="post-destination-block slide-dest-hide-right-margin">
                            <div class="post-dest-slider" id="postDestSliderInner4">
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">Grififth</div>
                                            <div class="dest-count">Observatory</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">Hearst Castle</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">SeaWorld San</div>
                                            <div class="dest-count">Diego</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">United Arab Emirates</div>
                                            <div class="dest-count">2 Destinations</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">SeaWorld San</div>
                                            <div class="dest-count">Diego</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-dest-card">
                                    <div class="post-dest-card-inner">
                                        <div class="dest-image">
                                            <img src="http://placehold.it/68x68" alt="">
                                        </div>
                                        <div class="dest-info">
                                            <div class="dest-name">United Arab Emirates</div>
                                            <div class="dest-count">2 Destinations</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="gallery-comment-wrap" id="galleryCommentWrap">
                    <div class="gallery-comment-inner mCustomScrollbar">
                        <div class="gallery-comment-top">
                            <div class="top-info-layer">
                                <div class="top-avatar-wrap">
                                    <img src="http://placehold.it/50x50" alt="">
                                </div>
                                <div class="top-info-txt">
                                    <div class="preview-txt">
                                        <p><a class="dest-name" href="#">Elijah Hughes</a> Went to a <a href="#"
                                                                                                        class="dest-name">Trip</a>
                                            to <a href="#" class="dest-name">16</a></p>
                                        <p class="dest-date"><a href="#" class="desn-name">Destinations</a> in <a
                                                    href="#" class="desn-name">Japan</a> on 5 Sept 2017</p>
                                    </div>
                                </div>
                            </div>
                            <div class="gal-com-footer-info">
                                <div class="post-foot-block post-reaction">
                                    <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                                    <span><b>2</b> @lang('report.reactions')</span>
                                </div>
                            </div>
                        </div>
                        <div class="post-comment-layer">
                            <div class="post-comment-top-info">
                                <div class="comm-count-info">
                                    @choice('comment.count_comment', 5, ['count' => 5])
                                </div>
                                <div class="comm-count-info">
                                    3 / 20
                                </div>
                            </div>
                            <div class="post-comment-wrapper">
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                delectus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>21</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Amine</a>
                                            <a href="#" class="comment-nickname">@ak0117</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                doloribus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-like.png" alt="">
                                                <span>19</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-comment-row">
                                    <div class="post-com-avatar-wrap">
                                        <img src="http://placehold.it/45x45" alt="">
                                    </div>
                                    <div class="post-comment-text">
                                        <div class="post-com-name-layer">
                                            <a href="#" class="comment-name">Katherin</a>
                                            <a href="#" class="comment-nickname">@katherin</a>
                                        </div>
                                        <div class="comment-txt">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                delectus.</p>
                                        </div>
                                        <div class="comment-bottom-info">
                                            <div class="com-reaction">
                                                <img src="./assets/image/icon-smile.png" alt="">
                                                <span>15</span>
                                            </div>
                                            <div class="com-time">6 hours ago</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="load-more-link">@lang('buttons.general.load_more_dots')</a>
                            </div>
                        </div>
                    </div>
                    <div class="post-add-comment-block">
                        <div class="avatar-wrap">
                            <img src="http://placehold.it/45x45" alt="">
                        </div>
                        <div class="post-add-com-input">
                            <input type="text" placeholder="@lang('comment.write_a_comment')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- embed popup -->
<div class="modal fade white-style" data-backdrop="false" id="embedPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-800" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog-share-style">
                <div class="post-side-top">
                    <h3 class="side-ttl">@lang('report.embed')</h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="post-content-inner">
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="top-banner-block top-travlog-banner"
                                 style="background-image: url(./assets/image/travlog-banner.jpg)">
                                <div class="top-banner-inner">
                                    <div class="travel-left-info">
                                        <h3 class="travel-title">From Moroco to the Amazing Japan in 7 Days</h3>
                                    </div>
                                    <div class="travel-right-info">
                                        <ul class="sub-list">
                                            <li>
                                                <div class="top-txt">@lang('region.country_capital')</div>
                                                <div class="sub-txt">Tokyo</div>
                                            </li>
                                            <li>
                                                <div class="top-txt">@lang('region.population')</div>
                                                <div class="sub-txt">127 million</div>
                                            </li>
                                            <li>
                                                <div class="top-txt">@lang('region.currency')</div>
                                                <div class="sub-txt">Japanese yen</div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="info-wrapper">
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="author-info">
                                            <div class="author-name">
                                                <span>@lang('place.from')</span>
                                                <b>Travooo</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span>12</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            <ul class="foot-avatar-list">
                                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava">
                                                </li><!--
                          -->
                                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava">
                                                </li><!--
                          -->
                                                <li><img class="small-ava" src="http://placehold.it/20x20" alt="ava">
                                                </li>
                                            </ul>
                                            <span>189</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="travlog-user-info">
                                    <div class="user-info-wrap">
                                        <div class="avatar-wrap">
                                            <img src="http://placehold.it/50x50" alt="">
                                        </div>
                                        <div class="info-txt">
                                            <div class="top-name">
                                                @lang('other.by') <a class="post-name-link" href="#">Thomas Wright</a>
                                            </div>
                                            <div class="sub-info">
                                                @lang('report.expert_in') <a href="#" class="place-link">France</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-action">
                                        <button type="button" class="btn btn-light-grey btn-bordered">
                                            @lang('buttons.general.follow')
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-modal-txt">
                        <p>Copy and past this code into your page where you want your add-on to appear:</p>
                    </div>
                    <div class="travlog-copy-link">
                        <form class="copy-link" autocomplete="off">
                            <input type="text" class="copy-input"
                                   value="<a data-pin-do='buttonBookmark' href='https://codepen.io/she_codes/pen/OgrJJe/'>https://codepen.io/she_codes/pen/OgrJJe/</a>">
                            <button type="button" class="copy-btn"><i class="fa fa-copy"></i> Copy</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script src="{{url('assets2/js/script.js')}}"></script>
</body>
</html>