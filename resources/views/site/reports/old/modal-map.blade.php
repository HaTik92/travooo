<!-- map popup -->
  <div class="modal fade white-style" data-backdrop="false" id="mapPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1080" role="document">
      <div class="modal-custom-block">
        <div class="post-block post-travlog inner-map-travlog">
          <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
            <i class="trav-close-icon"></i>
          </button>

          <div class="post-image-container post-follow-container travlog-map-wrap">
            <div class="post-map-wrap">
              <div class="post-map-inner">
                <img src="{{asset('assets2/image/popup-map.jpg')}}" alt="map">
                <div class="destination-point" style="top:80px;left: 20%;">
                  <img class="map-marker mCS_img_loaded" src="{{asset('assets2/image/marker.png')}}" alt="marker">
                </div>
              </div>
              <div class="info-caption">
                <div class="caption-txt">
                  <div class="info-block">
                    <div class="info-flag">
                      <img class="flag" src="http://placehold.it/60x42?text=flag" alt="flag">
                    </div>
                    <div class="info-txt">
                      <div class="info-name">Japan</div>
                      <div class="info-sub">Country in Asia</div>
                    </div>
                  </div>
                  <div class="info-block">
                    <div class="info-icon">
                      <i class="trav-popularity-icon"></i>
                    </div>
                    <div class="info-txt">
                      <div class="info-ttl">321.4 M</div>
                      <div class="info-sub">Population</div>
                    </div>
                  </div>
                </div>
                <div class="btn-wrap">
                  <button type="button" class="btn btn-light-grey btn-bordered">
                    Follow
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>