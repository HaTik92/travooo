<!-- place one day popup -->
	<div class="modal fade white-style" data-backdrop="false" id="tripPlanPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
      <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-1140 full-height" role="document">
      <ul class="modal-outside-link-list white-bg">
        <li class="outside-link">
          <a href="#">
            <div class="round-icon">
              <i class="trav-angle-left"></i>
            </div>
            <span>Back</span>
          </a>
        </li>
        <li class="outside-link">
          <a href="#">
            <div class="round-icon">
              <i class="trav-flag-icon"></i>
            </div>
            <span>Report</span>
          </a>
        </li>
      </ul>
      <div class="modal-custom-block">
        <button class="btn btn-mobile-side comment-toggler" id="commentToggler">
          <i class="trav-comment-icon"></i>
        </button>
        <div class="post-block post-place-plan-block">

          <div class="post-image-container post-follow-container mCustomScrollbar">
            <div class="post-image-inner">
              <div class="post-map-wrap">
                <div class="post-map-breadcrumb">
                  <ul class="breadcrumb-list">
                    <li><a href="#">Trip overview</a></li>
                    <li><a href="#">Japan</a></li>
                    <li>1 Day in <b>Tokyo</b></li>
                  </ul>
                </div>
                <ul class="post-time-trip">
                  <li class="day">Day</li>
                  <li class="count active">6</li>
                  <li class="all">All</li>
                </ul>
                <div class="post-map-inner">
                  <img src="../assets2/image/trip-plan-image.jpg" alt="map">
                  <div class="destination-point" style="top:80px;left: 20%;">
                    <img class="map-marker" src="{{asset('assets2/image/marker.png')}}" alt="marker">
                  </div>
                  <div class="post-map-info-caption map-blue">
                    <div class="map-avatar">
                      <img src="http://placehold.it/25x25" alt="ava">
                    </div>
                    <div class="map-label-txt">
                      Checking on <b>2 Sep</b> at <b>8:30 am</b> and will stay <b>30 min</b>
                    </div>
                  </div>
                </div>
              </div>
              <div class="post-destination-block slide-dest-hide-right-margin">
                <div class="post-dest-slider" id="postDestSliderInner4">
                  <div class="post-dest-card">
                    <div class="post-dest-card-inner">
                      <div class="dest-image">
                        <img src="http://placehold.it/68x68" alt="">
                      </div>
                      <div class="dest-info">
                        <div class="dest-name">Grififth</div>
                        <div class="dest-count">Observatory</div>
                      </div>
                    </div>
                  </div>
                  <div class="post-dest-card">
                    <div class="post-dest-card-inner">
                      <div class="dest-image">
                        <img src="http://placehold.it/68x68" alt="">
                      </div>
                      <div class="dest-info">
                        <div class="dest-name">Hearst Castle</div>
                      </div>
                    </div>
                  </div>
                  <div class="post-dest-card">
                    <div class="post-dest-card-inner">
                      <div class="dest-image">
                        <img src="http://placehold.it/68x68" alt="">
                      </div>
                      <div class="dest-info">
                        <div class="dest-name">SeaWorld San</div>
                        <div class="dest-count">Diego</div>
                      </div>
                    </div>
                  </div>
                  <div class="post-dest-card">
                    <div class="post-dest-card-inner">
                      <div class="dest-image">
                        <img src="http://placehold.it/68x68" alt="">
                      </div>
                      <div class="dest-info">
                        <div class="dest-name">United Arab Emirates</div>
                        <div class="dest-count">2 Destinations</div>
                      </div>
                    </div>
                  </div>
                  <div class="post-dest-card">
                    <div class="post-dest-card-inner">
                      <div class="dest-image">
                        <img src="http://placehold.it/68x68" alt="">
                      </div>
                      <div class="dest-info">
                        <div class="dest-name">SeaWorld San</div>
                        <div class="dest-count">Diego</div>
                      </div>
                    </div>
                  </div>
                  <div class="post-dest-card">
                    <div class="post-dest-card-inner">
                      <div class="dest-image">
                        <img src="http://placehold.it/68x68" alt="">
                      </div>
                      <div class="dest-info">
                        <div class="dest-name">United Arab Emirates</div>
                        <div class="dest-count">2 Destinations</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="gallery-comment-wrap" id="galleryCommentWrap">
            <div class="gallery-comment-inner mCustomScrollbar">
              <div class="gallery-comment-top">
                <div class="top-info-layer">
                  <div class="top-avatar-wrap">
                    <img src="http://placehold.it/50x50" alt="">
                  </div>
                  <div class="top-info-txt">
                    <div class="preview-txt">
                      <p><a class="dest-name" href="#">Elijah Hughes</a> Went to a <a href="#" class="dest-name">Trip</a> to <a href="#" class="dest-name">16</a></p>
                      <p class="dest-date"><a href="#" class="desn-name">Destinations</a> in <a href="#" class="desn-name">Japan</a> on 5 Sept 2017</p>
                    </div>
                  </div>
                </div>
                <div class="gal-com-footer-info">
                  <div class="post-foot-block post-reaction">
                    <img src="../assets2/image/reaction-icon-smile-only.png" alt="smile">
                    <span><b>2</b> Reactions</span>
                  </div>
                </div>
              </div>
              <div class="post-comment-layer">
                <div class="post-comment-top-info">
                  <div class="comm-count-info">
                    5 Comments
                  </div>
                  <div class="comm-count-info">
                    3 / 20
                  </div>
                </div>
                <div class="post-comment-wrapper">
                  <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                      <img src="http://placehold.it/45x45" alt="">
                    </div>
                    <div class="post-comment-text">
                      <div class="post-com-name-layer">
                        <a href="#" class="comment-name">Katherin</a>
                        <a href="#" class="comment-nickname">@katherin</a>
                      </div>
                      <div class="comment-txt">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                      </div>
                      <div class="comment-bottom-info">
                        <div class="com-reaction">
                          <img src="../assets2/image/icon-smile.png" alt="">
                          <span>21</span>
                        </div>
                        <div class="com-time">6 hours ago</div>
                      </div>
                    </div>
                  </div>
                  <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                      <img src="http://placehold.it/45x45" alt="">
                    </div>
                    <div class="post-comment-text">
                      <div class="post-com-name-layer">
                        <a href="#" class="comment-name">Amine</a>
                        <a href="#" class="comment-nickname">@ak0117</a>
                      </div>
                      <div class="comment-txt">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                      </div>
                      <div class="comment-bottom-info">
                        <div class="com-reaction">
                          <img src="../assets2/image/icon-like.png" alt="">
                          <span>19</span>
                        </div>
                        <div class="com-time">6 hours ago</div>
                      </div>
                    </div>
                  </div>
                  <div class="post-comment-row">
                    <div class="post-com-avatar-wrap">
                      <img src="http://placehold.it/45x45" alt="">
                    </div>
                    <div class="post-comment-text">
                      <div class="post-com-name-layer">
                        <a href="#" class="comment-name">Katherin</a>
                        <a href="#" class="comment-nickname">@katherin</a>
                      </div>
                      <div class="comment-txt">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                      </div>
                      <div class="comment-bottom-info">
                        <div class="com-reaction">
                          <img src="../assets2/image/icon-smile.png" alt="">
                          <span>15</span>
                        </div>
                        <div class="com-time">6 hours ago</div>
                      </div>
                    </div>
                  </div>
                  <a href="#" class="load-more-link">Load more...</a>
                </div>
              </div>
            </div>
            <div class="post-add-comment-block">
              <div class="avatar-wrap">
                <img src="http://placehold.it/45x45" alt="">
              </div>
              <div class="post-add-com-input">
                <input type="text" placeholder="Write a comment">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>