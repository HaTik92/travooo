<!-- create travlog popup -->
<div class="modal fade modal-child white-style" data-backdrop="false" id="createTravlogNextPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                <form method='post' id="createReportStep2" action="{{url('reports/create')}}" enctype='multipart/form-data' autocomplete="off">
                    <div class="top-title-layer">
                        <h3 class="title">
                            <span class="circle">2</span>
                            <span class="txt">Creating a Travelog</span>
                        </h3>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="top-travlog-banner">
                        <div class="top-banner-inner" id="cover">
                            <div class="file-wrap">
                                <input type="file" class="file-input" id="coverImage" name="cover_image">
                                <a href="#" class="file-input-handler" id="coverImageHandler">
                                    <i class="trav-upload-file-icon"></i>
                                </a>
                            </div>
                            <p id="drag">Drag or click to add cover photo for your Travelog</p>
                            <div id="message"></div>
                        </div>
                    </div>
                    <div class="travlog-main-block">
                        <div class="main-content">
                            <div class="create-row">
                                <div class="side left">
                                    <div class="label">Title</div>
                                </div>
                                <div class="content">
                                    <div class="input-block">
                                        <input type="text" name="title" id="step2_title" class="input" value="" placeholder="Type a title">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="create-row">
                                <div class="side left">
                                    <div class="close-handle">
                                        <i class="trav-close-icon"></i>
                                    </div>
                                </div>
                                <div class="content">
                                    <div id="message2"></div>
                                    <ul class="action-list">
                                        <li><a href="#" title="Insert Text" id="text_cont"><i class="trav-text-icon"></i></a></li>
                                        <li><a href="#" title="Insert Image" id="image_cont"><i class="trav-camera"></i></a></li>
                                        <!--<li class="dropdown dropup">
                                            <input type="file" class="file-input" id="reportImage" name="report_image" style="display:none;">
                                            <a href="#" class="" id="reportImageHandler">
                                                <i class="trav-camera"></i>
                                            </a>
                                        </li>-->
                                        <li><a href="#" title="Insert Video" id="video_cont"><i class="trav-play-video-icon"></i></a></li>
                                        <li><a href="#" title="Insert Trip Plan" data-toggle="modal" data-target="#addTripPlanPopup"><i class="trav-create-destination-icon"></i></a></li>
                                        <li><a href="#" title="Insert People" id="people_cont"><i class="trav-popularity-icon"></i></a></li>
                                        <li><a href="#" title="Insert Map" id="map_cont"><i class="trav-map-o"></i></a></li>
                                        <li><a href="#" title="Insert Travooo Information" data-toggle="modal" data-target="#addTravoooInfoPopup"><i class="trav-info-icon"></i></a></li>
                                        <li><a href="#" title="Insert Place" id="place_cont"><i class="trav-map-marker-icon"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                            
                            <div id="sortable">
                            @include('site/reports/partials/form_add_info')
                            @include('site/reports/partials/form_add_plan')
                            </div>
                            
                            
                        </div>
                    </div>
                    <div class="post-foot-btn">
                        <input type="hidden" name="description" id="step2_description" />
                        
                        <input type="hidden" name="countries_id" id="step2_countries_id" />
                        <input type="hidden" name="cities_id" id="step2_cities_id" />
                        <input type="hidden" name="report_for" id="step2_report_for" />
                        <input type="hidden" name="sort" id="step2_sort" />
                        <button class="btn btn-transp btn-clear">Cancel</button>
                        <button class="btn btn-light-primary btn-bordered">Publish</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>