<!-- add trip plan popup -->
<div class="modal fade modal-child white-style" data-modal-parent="#createTravlogNextPopup" data-backdrop="false" id="addTripPlanPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-780" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-top-bordered post-request-modal-block post-mobile-full">
                <div class="post-side-top">
                    <div class="post-top-txt travlog-event">
                        <h3 class="side-ttl"><span class="report-plan-title">My Trip plans</span> - <span class="report-plan-count-block">0</span></h3>
                        <div class="right-actions">
                            <div class="search-block">
                                <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
                                <input type="text" class="" id="searchInput" onkeyup="searchTripPlan()" placeholder="Search...">
                            </div>
                            <button class="modal-close  close-plan-modal" type="button" data-dismiss="modal" aria-label="Close">
                                <i class="trav-close-icon"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="rep-plan-tab-title">
                    <ul class="rep-plan-tab-list">
                        <li class="rep-plan-tab-item r-plan-item active" data-tab="mine">My Trip Plans</li>
                        <li class="rep-plan-tab-item r-plan-item" data-tab="invited">Invited Trip Plans</li>
                    </ul>
                </div>
                <div class="trip-plan-inner add-report-plan-inner">          
                    <div class="report-plan-block">
                    </div>
                    <div id="loadMorePlan" class="load-more-plans">
                        <input type="hidden" id="report_plan_pagenum" value="1">
                        <div id="reportplanloader" class="trip-plan-row">
                            <div class="plan-inside-animation animation"></div>
                            <div class="plan-block-text-animation">
                                <div class="plan-block-title-animation animation"></div>
                                <div class="plan-desc-animation animation"></div>
                                <div class="plan-dest-info">
                                    <div class="plan-hr-animation" id="hide_repPlan_loader"></div>
                                    <div class="plan-dest-left-animation animation"></div>
                                    <div class="plan-dest-right-animation animation" ></div>
                                </div>
                            </div>
                            <div class="plan-place-info">
                                <div class="plan-place-animation animation"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trip-modal-foot">
                    <button class="btn btn-transp btn-clear close-plan-modal" data-dismiss="modal" aria-label="Close">Cancel</button>
                    <button class="btn btn-light-primary btn-bordered closePlansModal" type="button" data-dismiss="modal">Insert</button>
                </div>
            </div>
        </div>
    </div>
</div>