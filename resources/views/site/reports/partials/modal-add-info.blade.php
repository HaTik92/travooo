<!-- add trip plan popup -->
<div class="modal fade modal-child white-style" id="notificationPopup" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-custom-style" role="document">
        <div class="report-notification" style="padding: 10px; text-align: center; background: #32c984; color: #fff; border: 0; box-shadow: 0 4px 10px rgba(0,0,0,0.1); border-radius: 4px; padding: 18px 30px;">
            Info added
        </div>
    </div>
</div>
<div class="modal fade modal-child white-style" data-modal-parent="#createTravlogNextPopup" data-backdrop="false" id="addTravoooInfoPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-780" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-top-bordered post-request-modal-block post-mobile-full">
                <div class="post-side-top">
                    <div class="post-top-txt travlog-event">
                        <h3 class="side-ttl">Add Travooo Info</h3>
                        <div class="right-actions">
                            <div class="search-block">

                            </div>
                            <button class="modal-close close-modal-info" type="button" data-dismiss="modal" aria-label="Close">
                                <i class="trav-close-icon"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="post-info-inner">
                    <div class="select-location-block">
                        <div class="info-location-list">
                        </div>
                    </div>
                    <div class="travooo-info-content main-info-content">
                        <div class="t-log-info-wrapper">

                                <div class="travlog-info-block Nationality">
                                    <div class="title">
                                        <span><span class="t-title-content">Nationality</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_nationality"></div>
                                </div>
                                <div class="travlog-info-block Languages-spoken">
                                    <div class="title">
                                        <span><span class="t-title-content">Languages spoken</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_languages"></div>
                                </div>
                                <div class="travlog-info-block Currencies">
                                    <div class="title">
                                        <span><span class="t-title-content">Currencies</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_currencies"></div>
                                </div>
                                <div class="travlog-info-block Timings">
                                    <div class="title">
                                        <span><span class="t-title-content">Timings</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_timings"></div>
                                </div>
                                <div class="travlog-info-block Religions">
                                    <div class="title">
                                        <span><span class="t-title-content">Religions</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_religions"></div>
                                </div>
                                <div class="travlog-info-block Phone-Code">
                                    <div class="title">
                                        <span><span class="t-title-content">Phone Code</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_code"></div>
                                </div>
                                <div class="travlog-info-block Units-of-measurement">
                                    <div class="title">
                                        <span><span class="t-title-content">Units of measurement</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_metrics"></div>
                                </div>
                                <div class="travlog-info-block Working-Days">
                                    <div class="title">
                                        <span><span class="t-title-content">Working Days</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_working_days"></div>
                                </div>
                                <div class="travlog-info-block Transportation-Methods">
                                    <div class="title">
                                        <span><span class="t-title-content">Transportation Methods</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_transportation"></div>
                                </div>
                                <div class="travlog-info-block Speed-Limit">
                                    <div class="title">
                                        <span><span class="t-title-content">Speed Limit</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_speed"></div>
                                </div>
                           
                                <div class="travlog-info-block Emergency-Numbers">
                                    <div class="title">
                                        <span><span class="t-title-content">Emergency Numbers</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_emergency"></div>
                                </div>
                           
                                <div class="travlog-info-block Cost-of-Living">
                                    <div class="title">
                                        <span><span class="t-title-content">Cost of Living</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_cost_of_living"></div>
                                </div>
                                <div class="travlog-info-block Crime-Rate">
                                    <div class="title">
                                        <span><span class="t-title-content">Crime Rate</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_crime_rate"></div>
                                </div>
                                <div class="travlog-info-block Quality-of-Life">
                                    <div class="title">
                                        <span><span class="t-title-content">Quality of Life</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_quality_of_life"></div>
                                </div>
                         
                                <div class="travlog-info-block Holidays">
                                    <div class="title">
                                        <span><span class="t-title-content">Holidays</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_holidays"></div>
                                </div>
                                 <div class="travlog-info-block Internet">
                                    <div class="title">
                                        <span><span class="t-title-content">Internet</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_internet"></div>
                                </div>

                                <div class="travlog-info-block Sockets">
                                    <div class="title">
                                        <span><span class="t-title-content">Sockets</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_sockets"></div>
                                </div>

                                <div class="travlog-info-block Population">
                                    <div class="title">
                                        <span><span class="t-title-content">Population</span> - <span class="info-dest-name"></span></span>
                                        <span class="t-info-add-icon"><img src="{{asset('assets2/image/icons/add-info.svg')}}"></span>
                                    </div>
                                    <div class="subtitle" id="info_population"></div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
