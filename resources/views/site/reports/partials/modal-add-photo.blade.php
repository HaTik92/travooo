<!-- add trip plan popup -->
  <div class="modal fade modal-child white-style" data-modal-parent="#createTravlogNextPopup" data-backdrop="false" id="selectFromLibraryPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-875" role="document">
      <div class="modal-custom-block">
        <div class="post-block post-top-bordered post-request-modal-block post-mobile-full">
          <div class="post-side-top">
            <div class="post-top-txt travlog-event">
              <h3 class="side-ttl">Add Photo To Travelog</h3>
              <div class="right-actions">
                <div class="search-block">
                  <a class="search-btn" href="#"><i class="trav-search-icon"></i></a>
                  <input type="text" class="" id="" placeholder="Search...">
                </div>
                <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                  <i class="trav-close-icon"></i>
                </button>
              </div>
            </div>
          </div>
          <div class="library-inner mCustomScrollbar">
            <div class="library-wrapper">
              <div class="img">
                <img src="http://placehold.it/195x165" alt="image">
              </div>
              <div class="img">
                <img src="http://placehold.it/195x135" alt="image">
              </div>
              <div class="img">
                <img src="http://placehold.it/195x165" alt="image">
              </div>
              <div class="img">
                <img src="http://placehold.it/195x135" alt="image">
              </div>
              <div class="img">
                <img src="http://placehold.it/195x205" alt="image">
              </div>
              <div class="img">
                <img src="http://placehold.it/195x165" alt="image">
              </div>
              <div class="img">
                <img src="http://placehold.it/195x205" alt="image">
              </div>
              <div class="img">
                <img src="http://placehold.it/195x135" alt="image">
              </div>
              <div class="img">
                <img src="http://placehold.it/195x135" alt="image">
              </div>
              <div class="img">
                <img src="http://placehold.it/195x205" alt="image">
              </div>
              <div class="img">
                <img src="http://placehold.it/195x165" alt="image">
              </div>
              <div class="img">
                <img src="http://placehold.it/195x205" alt="image">
              </div>
              <div class="img">
                <img src="http://placehold.it/195x135" alt="image">
              </div>
            </div>
          </div>
          <div class="trip-modal-foot">
            <button class="btn btn-transp btn-clear">Close</button>
            <button class="btn btn-light-grey">Upload New Photo</button>
          </div>
        </div>
      </div>
    </div>
  </div>