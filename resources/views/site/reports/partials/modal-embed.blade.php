<!-- embed popup -->
<div class="modal fade white-style" data-backdrop="false" id="embedPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-800" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog-share-style">
                <div class="post-side-top">
                    <h3 class="side-ttl">Embed</h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="post-content-inner">
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner">
                            <div class="top-banner-block top-travlog-banner" style="background-image: url({{@$report->cover[0]->val}})">
                                <div class="top-banner-inner">
                                    <div class="travel-left-info">
                                        <h3 class="travel-title embed-travel-title">{{$report->title}}</h3>
                                    </div>
                                    <div class="travel-right-info">
                                        <ul class="sub-list">
                                            @php $number_output = 0; @endphp
                                            @if(count(explode(',', $report->cities_id)) > 1 || count(explode(',', $report->countries_id)) > 1)
                                            <li>
                                                <div class="top-txt">Countries and Cities</div>
                                                @foreach(get_report_locations_info($report->id, 'publish') as $locations)
                                                @if($number_output < 3)
                                                <div class="sub-txt">{{$locations['title']}}</div>
                                                @elseif($number_output == 3)
                                                <a href="javascript:;" class="more-link more-location-modal" data-report-id="{{$report->id}}">+ {{count(get_report_locations_info($report->id, 'publish'))-3}} More</a>
                                                @endif
                                                @php $number_output ++;@endphp
                                                @endforeach
                                            </li>
                                            @else
                                            @if($report->countries_id !='')
                                            @if(isset($report->country->capitals))
                                            <li>
                                                <div class="top-txt">@lang('region.country_capital')</div>
                                                <div class="sub-txt">{{@$report->country->capitals[0]->city->transsingle->title}}</div>
                                            </li>
                                            @endif
                                            <li>
                                                <div class="top-txt">@lang('region.population')</div>
                                                <div class="sub-txt">{{$report->country->transsingle->population}}</div>
                                            </li>
                                            <li>
                                                <div class="top-txt">Currency</div>
                                                <div class="sub-txt">{{@$report->country->currencies[0]->transsingle->title}}</div>
                                            </li>
                                            @elseif($report->cities_id !='')
                                            <li>
                                                <div class="sub-txt">{{@$report->city->transsingle->title}}</div>
                                            </li>
                                            <li>
                                                <div class="top-txt">@lang('region.population')</div>
                                                <div class="sub-txt">{{@$report->city->transsingle->population}}</div>
                                            </li>
                                            <li>
                                                <div class="top-txt">Currency</div>
                                                <div class="sub-txt">{{@$report->city->country->currencies[0]->transsingle->title}}</div>
                                            </li>
                                            @endif
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="info-wrapper">
                                <div class="foot-block">
                                    <div class="author-side">
                                        <div class="author-info">
                                            <div class="author-name">
                                                <span>From</span>
                                                <b>Travooo</b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-side">
                                        <div class="post-foot-block">
                                            <i class="trav-heart-fill-icon"></i>&nbsp;
                                            <span class="rep-embed-like-count">{{count($report->likes)}}</span>
                                        </div>
                                        <div class="post-foot-block">
                                            <i class="trav-comment-icon"></i>&nbsp;
                                            @if(count($report->comments))
                                            <ul class="foot-avatar-list">
                                                <?php $us = array(); ?>
                                                @foreach($report->comments AS $repc)
                                                @if(!in_array($repc->users_id, $us))
                                                <?php $us[] = $repc->users_id; ?>
                                                <li><img class="small-ava" src="{{check_profile_picture($repc->author->profile_picture)}}" alt="{{$repc->author->name}}" title="{{$repc->author->name}}" width='20px' height='20px'></a></li>
                                                @endif
                                                @endforeach

                                            </ul>
                                            @endif
                                            <span class="rep-embed-comment-count">{{count($report->comments)}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="travlog-user-info">
                                    <div class="user-info-wrap">
                                        <div class="avatar-wrap">
                                            <img src="{{check_profile_picture($report->author->profile_picture)}}" alt="{{$report->author->name}}" title="{{$report->author->name}}" width='50px' height="50px">
                                        </div>
                                        <div class="info-txt">
                                            <div class="top-name">
                                                @lang('other.by') <a class="post-name-link" href="{{url('profile/'.$report->author->id)}}">{{$report->author->name}}</a>
                                                {!! get_exp_icon($report->author) !!}
                                            </div>
                                            @if(count($expertise)>0)
                                            <div class="sub-info">
                                                @lang('report.expert_in')
                                                @foreach($expertise as $k=>$exp)
                                                @if($k<3)
                                                <a href="javascript:;" class="place-link">{{$exp['text']}}</a>
                                                @endif
                                                {{($k < 2 && $k+1 < count($expertise))?',':''}}
                                                {{($k == 3)?'...':''}}
                                                @endforeach
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    @if($report->author->id != Auth::user()->id)
                                    <div class="info-action">
                                        <button type="button" class="btn {{($follower && $follower->follow_type == 1)?'btn-light-grey':'btn-blue-follow'}} btn-bordered follow-button follow-button_{{$report->author->id}}" data-user-id="{{$report->author->id}}" data-route-follow="{{ route('profile.follow', $report->author->id) }}"  data-route-unfollow="{{ route('profile.unfolllow', $report->author->id) }}" data-type="{{($follower && $follower->follow_type == 1)?'unfollow':'follow'}}">
                                            @if($follower && $follower->follow_type == 1)
                                            @lang('buttons.general.unfollow')
                                            @else
                                            @lang('buttons.general.follow')
                                            @endif
                                        </button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="travlog-modal-txt">
                        <p>Copy and paste this HTML code where you want the Travelog to appear...</p>
                    </div>
                    <div class="travlog-copy-link">
                        <form class="copy-link" autocomplete="off">
                            <input type="text" class="copy-input" id="copy-input" value='<iframe src="{{url('reports/embed')}}/{{$report->id}}/{{url_encode($report->author->id)}}" width="756" height="500"></iframe>'>
                            <button type="button" class="copy-btn" id="copy-btn" onclick="copyMe()"><i class="fa fa-copy"></i> Copy</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
