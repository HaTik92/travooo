@if(Auth::guard('user')->user())
<!-- share on trav popup -->
<div class="modal fade" data-backdrop="true" id="shareOnTravPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-650" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog-share-style">
                <form method="post" id="shareReportForm" autocomplete="off">
                    <div class="post-side-top">
                        <h3 class="side-ttl">Share on Travooo</h3>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="post-content-inner">
                        <div class="say-block">
                            <div class="img-wrap">
                                <img src="{{check_profile_picture(Auth::guard('user')->user()->profile_picture)}}" alt="avatar" class="ava" width="38px" height="38px">
                            </div>
                            <textarea name="text" class="report-share-textarea" id="text" placeholder="Say something..."  oninput="share_textarea_auto_height(this)"></textarea>
                        </div>
                        <div class="travlog-info-block">
                            <div class="travlog-info-inner">
                                <div class="img-wrap">
                                    @if(@$report->cover[0]->val != "")
                                    <img src="{{@$report->cover[0]->val}}" alt="photo" width="335px" height="170px">
                                    @endif
                                </div>
                                <div class="info-wrapper">
                                    <div class="info-title" style="direction: rtl !important;"> <span>{!! str_limit($title, 55, '<span>...</span>') !!}</span></div>
                                    <div class="foot-block">
                                        <div class="author-side">
                                            <div class="author-info">
                                                <div class="author-name">
                                                    <span>By</span>
                                                    <a href="{{url('profile/'.$report->author->id)}}" class="author-link">{{$report->author->name}}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="like-side">
                                            <div class="post-foot-block">
                                                <i class="trav-heart-fill-icon"></i>&nbsp;
                                                <span>{{count($report->likes)}}</span>
                                            </div>
                                            <div class="post-foot-block">
                                                <i class="trav-comment-icon"></i>&nbsp;
                                                @if(count($report->comments))
                                                <ul class="foot-avatar-list">
                                                    <?php $us = array(); ?>
                                                    @foreach($report->comments AS $repc)
                                                    @if(!in_array($repc->users_id, $us))
                                                    <?php $us[] = $repc->users_id; ?>
                                                    <li><img class="small-ava" src="{{check_profile_picture($repc->author->profile_picture)}}" alt="{{$repc->author->name}}" title="{{$repc->author->name}}" width='20px' height='20px'></a></li>
                                                    @endif
                                                    @endforeach

                                                </ul>
                                                @endif
                                                <span>{{count($report->comments)}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-foot-btn">
                        <div class="dropdown ">
                            <a href="javascript:;" class="btn btn-transp btn-clear rep-permission-btn" data-toggle="dropdown" aria-expanded="false">
                                <i class="trav-earth"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-left dropdown-arrow rep-privacy-arrow">
                                <li class="dropdown-item">
                                    <a href="javascript:;" class="btn pravicy-item" data-pravicytype="0">
                                        <i class="trav-earth"></i> Public
                                    </a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="javascript:;" class="btn pravicy-item d-flex follow-content" data-pravicytype="1">
                                        <i class="trav-user-plus-icon"></i> Followers
                                    </a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="javascript:;" class="btn pravicy-item" data-pravicytype="2">
                                        <i class="fa fa-user-o" aria-hidden="true"></i> Friends
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <input type="hidden" name="post_id" value="{{$report->id}}" />
                        <input type="hidden" name="permission" class="rep-permission" value="0" />
                        <button type="submit" class="btn btn-light-primary btn-bordered">Post</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
