<!-- start plan -->
<?php
$index = rand(1, 5000);
$plan = App\Models\TripPlans\TripPlans::find($ri->val);

?>
@if($plan)
<div class="content-wrappers">
    <div class="post-image-container post-follow-container travlog-map-wrap">
        <div class="post-map-wrap">
            <div class="post-map-inner">
                <img src="{{get_plan_map($plan, '590', '380')}}" alt="map">
                
            </div>
        </div>
        <div class="post-bottom-block">
            <div class="bottom-txt-wrap">
                <p class="bottom-ttl">{{$plan->title}}</p>
                <div class="bottom-sub-txt">
                    <span>@lang('trip.trip_plan_by') <a href="{{url('profile/'.$plan->author->id)}}" target="_blank" class="name-link">{{$plan->author->name}}</a>
                         for {{count($plan->trips_places)}} Destinations
                    </span>
                </div>
            </div>
            <div class="bottom-btn-wrap">
                <a type="button" class="btn btn-light-grey btn-bordered" target="_blank"  href="{{url_with_locale('trip/plan/'.$plan->id)}}">
                    @lang('report.view_plan')
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end plan -->
@endif
