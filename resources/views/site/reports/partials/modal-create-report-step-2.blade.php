<!-- create travlog popup -->
<div class="step2-loader stepLoader">
    <span class="step-preloader">
    </span>
</div>

<div class="modal fade modal-child white-style" data-backdrop="false" id="createTravlogNextPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <button class="scroll-up-btn d-none" type="button">
        <i class="trav-angle-up"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-1030" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                    <div class="top-title-layer">
                        <h3 class="title">
                            <span class="circle">2</span>
                            <span class="txt">Creating a Travelog</span>
                        </h3>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="top-travlog-banner rep-cover-block" style="overflow: hidden;position: relative;">
                        <div class="top-banner-inner" id="cover">
                            <div class="file-wrap">
                                <input type="file" class="file-input" id="coverImage" name="cover_image">
                                <a href="#" class="file-input-handler" id="coverImageHandler"> 
                                    <i class="trav-upload-file-icon"></i>
                                </a>
                            </div>
                            <p id="drag">Add a cover image for your Travelog</p>
                            <div id="message"></div>
                        </div>
                        <div id="cropimage"></div>
                        <div id="coverLoader" class="d-none loader-spin">
                        </div>
                    </div>
                 <form method='post' id="createReportStep2" action="{{url('reports/create-info')}}" enctype='multipart/form-data' autocomplete="off">
                    <div class="travlog-main-block">
                        <div class="main-content" style="width:80%">
                            <div class="create-row">
                                <div class="side left">
                                    <div class="label">Title</div>
                                </div>
                                <div class="content text-content">
                                    <div class="input-block">
                                        <input type="text" name="title" maxlength="100" id="step2_title" class="input" value="" placeholder="Type a title">
                                    </div>
                                    <div class="label-txt d-none remaining-content" id='titleRemainigLength'><span></span> Characters remaining...</div>
                                    <div class="report-title-error-block d-none">The report title is required.</div>
                                </div>
                                <div class="side right" dir="auto"></div>
                            </div>
                            
                            <div class="create-row sticky">
                                <div class="side left">
                                    <div class="close-handle">
                                        <i class="trav-close-icon"></i>
                                    </div>
                                </div>
                                <div class="content">
                                    <div id="message2"></div>
                                    <div id="dest_info" data-lat='' data-lng=''></div>
                                    <ul class="action-list rep-action-list">
                                        <li class="" dataId="text_cont"><a href="javascript:;" data-title="Insert Text" id="text_cont"><i class="trav-text-icon"></i></a></li>
                                        <li  class="" dataId="image_cont"><a href="javascript:;" data-title="Insert Image" id="image_cont"><i class="trav-camera"></i></a></li>
                                        <li>
                                            <div class="dropdown video-dropdown " dataId="video_dropdown">
                                                <a href="javascript:;" data-title="Insert Video" id="rep_video_cont" class="video-dropdown-link" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false"><i class="trav-play-video-icon p-l-3"></i></a>
                                                <div class="dropdown-menu dropdown-menu-center video-dropdown-menu">
                                                    <span id="local_video_cont" class="dropdown-item noDrag" dataId="local_video_cont">
                                                      Upload Video
                                                    </span>
                                                    <span id="video_cont" class="dropdown-item noDrag" dataId="video_cont">
                                                     Add Video Url
                                                    </span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="" dataId="plan_modal_cont"><a href="javascript:;" data-title="Insert Trip Plan" data-toggle="modal" id="plan_modal_cont" data-target="#addTripPlanPopup"><i class="trav-create-destination-icon"></i></a></li>
                                        <li class="" dataId="people_cont"><a href="javascript:;" data-title="Insert People" id="people_cont"><i class="trav-popularity-icon"></i></a></li>
                                        <li class="" dataId="map_cont"><a href="javascript:;" data-title="Insert Map" id="map_cont"><i class="trav-map-o"></i></a></li>
                                        <li class="" dataId="travooo_info_modal"><a href="javascript:;" data-title="Insert Travooo Information" id="travooo_info_modal"><i class="trav-info-icon p-r-2"></i></a></li>
                                        <li class="" dataId="place_cont"><a href="javascript:;" data-title="Insert Place" id="place_cont"><i class="trav-map-marker-icon"></i></a></li>
                                    </ul>
                                </div>
                                <div class="side right" dir="auto"></div>
                            </div>
                            
                            <div id="sortable"></div>
                            
                        </div>
                    </div>
                    <div class="post-foot-btn">
                        <input type="hidden" name="report_id" id="step2_report_id" value="0" />
                        <input type="hidden" name="flag" id="step2_flag" value="0" />
                        <button class="btn btn-transp btn-clear report-cancel" type="cancel" data-dismiss="modal" aria-label="Close">Cancel</button>
                        <button class="btn btn-light-grey btn-bordered save-draft" type="submit" onclick="saveReport('draft')">Draft</button>&nbsp;
                        <button class="btn btn-light-primary btn-bordered save-publish" >Publish</button>
                    </div>
                </form>
            </div>
           
        </div>
    </div>
</div>
