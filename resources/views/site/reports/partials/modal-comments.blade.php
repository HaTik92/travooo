<!-- comments popup -->
<div class="modal fade white-style" data-backdrop="false" id="commentsPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-765" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-modal-comment travlog-comment">
                <div class="post-comment-main">
                    <div class="top-banner-block top-travlog-banner" style="background-image: url({{@$report->cover[0]->val}})">
                        <div class="top-banner-inner">
                            <div class="travel-left-info">
                                <h3 class="travel-title">{{$report->title}}</h3>
                                <div class="travlog-user-info">
                                    <div class="user-info-wrap">
                                        <div class="avatar-wrap">
                                            <img src="{{check_profile_picture($report->author->profile_picture)}}" alt="{{$report->author->name}}" title="{{$report->author->name}}" width='50px' height="50px">
                                        </div>
                                        <div class="info-txt">
                                            <div class="top-name">
                                                @lang('other.by') <a class="post-name-link" href="{{url('profile/'.$report->author->id)}}">{{$report->author->name}}</a>
                                                {!! get_exp_icon($report->author) !!}
                                            </div>
                                            <div class="sub-info">
                                                Expert in <a href="#" class="place-link">France</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="travel-right-info">
                                <ul class="sub-list">
                                    <li>
                                        <div class="top-txt">Country capital</div>
                                        <div class="sub-txt">{{@$report->countryOrCity->capitals[0]->city->transsingle->title}}</div>
                                    </li>
                                    <li>
                                        <div class="top-txt">Population</div>
                                        <div class="sub-txt">{{$report->countryOrCity->transsingle->population}}</div>
                                    </li>
                                    <li>
                                        <div class="top-txt">Currency</div>
                                        <div class="sub-txt">{{@$report->countryOrCity->currencies[0]->transsingle->title}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="discussion-block">
                        <span class="disc-ttl">
                            <span class="{{$report->id}}-all-comments-count"> {{count($report->comments)}}</span> Comments</span>
                        <ul class="disc-ava-list">
                            @if(count($report->comments))
                            <?php $us = array(); ?>
                            @foreach($report->comments()->orderBy('created_at', 'desc')->get() AS $repc)
                            @if(!in_array($repc->users_id, $us))
                            <?php $us[] = $repc->users_id; ?>
                                @if(count($us)<6)
                                    <li><a href="{{url('profile/'.$repc->author->id)}}"><img src="{{check_profile_picture($repc->author->profile_picture)}}" alt="{{$repc->author->name}}" title="{{$repc->author->name}}" width='32px' height='32px'></a></li>
                                @endif
                            @endif
                            @endforeach

                            @endif
                        </ul>
                    </div>
                </div>
                @if(Auth::guard('user')->user())
                    <div class="post-add-comment-block report-add-comment-block">
                        <div class="avatar-wrap">
                            <img src="{{check_profile_picture(Auth::guard('user')->user()->profile_picture)}}" alt="" width="45px" height="45px">
                        </div>
                        <div class="post-add-com-inputs">
                            <form class="reportCommentForm" method="POST" data-id="{{$report->id}}"  data-type="1" autocomplete="off" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" data-id="pair{{$report->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                                <div class="post-create-block post-comment-create-block report-comment-block post-regular-block" tabindex="0">
                                    <div class="post-create-input">
                                        <textarea name="text" data-id="text"  class="textarea-customize"
                                                  style="display:inline;vertical-align: top;min-height:50px;" oninput="comment_textarea_auto_height(this, 'regular')" placeholder="@lang('comment.write_a_comment')"></textarea>
                                        <div class="medias report-media">
                                        </div>
                                    </div>
                                    <div class="post-create-controls">
                                        <div class="post-alloptions">
                                            <ul class="create-link-list">
                                                <li class="post-options">
                                                    <input type="file" name="file[]" class="report-modal-media-input" data-report_id="{{$report->id}}" id="commentmediafile{{$report->id}}" style="display:none" multiple>
                                                    <i class="fa fa-camera click-target" aria-hidden="true" data-target="commentmediafile{{$report->id}}"></i>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                                <input type="hidden" name="report_id" value="{{$report->id}}"/>
                                <button type="submit" class="btn btn-primary d-none"></button>
                            </form>

                        </div>
                    </div>
                @endif
                <div class="post-comment-layer report-modal-comment-content">
                   <div class="post-comment-top-info" id="{{$report->id}}" {{(count($report->comments)>0)?'':'style=display:none'}} >
                        <ul class="comment-filter">
                            <li class="comment-filter-type" data-type="Top" data-comment-type="2">@lang('other.top')</li>
                            <li class="comment-filter-type active" data-type="New" data-comment-type="2">@lang('other.new')</li>
                        </ul>
                        <div class="comm-count-info">
                            <span class="{{$report->id}}-loading-count"><strong>5</strong></span> / <span class="{{$report->id}}-all-comments-count">{{count($report->comments)}}</span>
                        </div>
                    </div>
                    <div class="post-comment-wrapper sortBody report-comment-wraper" id="{{$report->id}}">
                        @if(count($report->comments)>0)
                        <div class="report-comment-main">
                            @foreach($report->comments()->orderBy('created_at', 'desc')->take(5)->get() AS $comment)
                                @include('site.reports.partials.report_comment_block')
                            @endforeach
                        </div>
                        <div id="loadMore" class="loadMore" reportId="{{$report->id}}" style="{{count($report->comments)< 6?'display:none;':''}}">
                            <input type="hidden" id="pagenum" value="1">
                            <div id="reportloader" class="post-animation-content post-comment-row">
                                <div class="report-com-avatar-wrap">
                                    <div class="post-top-avatar-wrap animation post-animation-avatar"></div>
                                </div>
                                <div class="post-comment-text">
                                    <div class="report-top-name animation"></div>
                                    <div class="report-com-info animation"></div>
                                    <div class="report-com-sec-info animation"></div>
                                    <div id="modal_hide_loader" class="report-com-thr-info animation"></div>
                                </div>
                            </div>
                        </div>
                        @else
                           <div class="post-comment-top-info comment-not-fund-info" id="{{$report->id}}">
                               <ul class="comment-filter">
                                   <li>No comments yet ..</li>
                                   <li></li>
                               </ul>
                               <div class="comm-count-info">
                               </div>
                           </div>
                       @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
