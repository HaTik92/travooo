<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>Travooo</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta property="og:url"           content="{{url('reports/'.$report->id.'/'.url_encode($report->author->id))}}" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="{{$report->title}} - Travooo.com" />
        <meta property="og:description"   content="{{$report->description}}" />
        <meta property="og:image"         content="@if($report->cover){{@$report->cover[0]->val}}@endif" />

        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
         <link rel="stylesheet" href="{{asset('assets2/css/style-15102019-9.css')}}">
        <link rel="stylesheet" href="{{asset('assets2/css/travooo-embed.css?0.6.4'.time())}}">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="{{url('assets2/js/jquery-loading-master/dist/jquery.loading.min.css')}}">


        <script data-cfasync="false" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    </head>
    <body @if(session('lang-rtl')) class="rtl-direction" @endif>
           <div class="em-main-wrapper" >
            <div class="content-wrap">
                <div class="em-travlog-info-block">
                    <div class="em-travlog-info-inner">
                        <div class="em-top-banner-block em-top-travlog-banner" style="background-image: url({{@$report->cover[0]->val}})">
                            <div class="em-top-banner-inner">
                                <div class="travel-left-info">
                                    <a href="{{url('/reports', $report->id)}}" class="travel-title embed-travel-title" target="_blank">{{$report->title}}</a>
                                </div>
                                <div class="travel-right-info">
                                    <ul class="sub-list">
                                        @php $number_output = 0; @endphp
                                        @if(count(explode(',', $report->cities_id)) > 1 || count(explode(',', $report->countries_id)) > 1)
                                        <li>
                                            <div class="top-txt">Countries and Cities</div>
                                            @foreach(get_report_locations_info($report->id, 'publish') as $locations)
                                            @if($number_output < 3)
                                            <div class="sub-txt">{{$locations['title']}}</div>
                                            @elseif($number_output == 3)
                                            <a href="javascript:;" class="em-more-link more-location-modal">+ {{count(get_report_locations_info($report->id, 'publish'))-3}} More</a>
                                            @endif
                                            @php $number_output ++;@endphp
                                            @endforeach
                                        </li>
                                        @else
                                        @if($report->countries_id !='')
                                        @if(isset($report->country->capitals))
                                        <li>
                                            <div class="top-txt">@lang('region.country_capital')</div>
                                            <div class="sub-txt">{{@$report->country->capitals[0]->city->transsingle->title}}</div>
                                        </li>
                                        @endif
                                        <li>
                                            <div class="top-txt">@lang('region.population')</div>
                                            <div class="sub-txt">{{$report->country->transsingle->population}}</div>
                                        </li>
                                        <li>
                                            <div class="top-txt">Currency</div>
                                            <div class="sub-txt">{{@$report->country->currencies[0]->transsingle->title}}</div>
                                        </li>
                                        @elseif($report->cities_id !='')
                                        <li>
                                            <div class="sub-txt">{{@$report->city->transsingle->title}}</div>
                                        </li>
                                        <li>
                                            <div class="top-txt">@lang('region.population')</div>
                                            <div class="sub-txt">{{@$report->city->transsingle->population}}</div>
                                        </li>
                                        <li>
                                            <div class="top-txt">Currency</div>
                                            <div class="sub-txt">{{@$report->city->country->currencies[0]->transsingle->title}}</div>
                                        </li>
                                        @endif
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="em-info-wrapper">
                            <div class="foot-block">
                                <div class="author-side">
                                    <div class="author-info">
                                        <div class="author-name">
                                            <span>From</span>
                                            <b>Travooo</b>
                                        </div>
                                    </div>
                                </div>
                                <div class="like-side">
                                    <div class="post-foot-block">
                                        <i class="trav-heart-fill-icon"></i>&nbsp;
                                        <span>{{count($report->likes)}}</span>
                                    </div>
                                    <div class="post-foot-block">
                                        <i class="trav-comment-icon"></i>&nbsp;
                                        <ul class="foot-avatar-list">
                                            @if(count($report->comments))
                                            <?php $us = array(); ?>
                                            @foreach($report->comments AS $repc)
                                            @if(!in_array($repc->users_id, $us))
                                            <?php $us[] = $repc->users_id; ?>
                                            <li><img class="small-ava" src="{{check_profile_picture($repc->author->profile_picture)}}" alt="{{$repc->author->name}}" title="{{$repc->author->name}}" width='20px' height='20px'></a></li>
                                            @endif
                                            @endforeach

                                            @endif
                                        </ul>
                                        <span>{{count($report->comments)}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="travlog-user-info">
                                <div class="user-info-wrap">
                                    <div class="avatar-wrap">
                                        <img src="{{check_profile_picture($report->author->profile_picture)}}" alt="{{$report->author->name}}" title="{{$report->author->name}}" width='50px' height="50px">
                                    </div>
                                    <div class="info-txt">
                                        <div class="top-name">
                                            @lang('other.by') <a class="post-name-link" href="{{url('profile/'.$report->author->id)}}">{{$report->author->name}}</a>
                                            {!! get_exp_icon($report->author) !!}
                                        </div>
                                        @if(count($expertise)>0)
                                            <div class="sub-info">
                                                @lang('report.expert_in')
                                                @foreach($expertise as $k=>$exp)
                                                @if($k<3)
                                                <a href="javascript:;" class="place-link">{{$exp['text']}}</a>
                                                @endif
                                                {{($k < 2 && $k+1 < count($expertise))?',':''}}
                                                {{($k == 3)?'...':''}}
                                                @endforeach
                                            </div>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
