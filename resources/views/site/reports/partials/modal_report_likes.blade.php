<div class="modal fade white-style" data-backdrop="false" id="reportlikesModal" tabindex="-1" role="dialog" aria-labelledby="reportlikesModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="comment-like-top-info">
                    <i class="fa fa-heart fill" aria-hidden="true"></i> 
                    <span class="report-modal-like-count">0</span>
                </div>
                <div  class="report-like-users">
                    <div  class="report-like-modal-block"></div>

                    <div id="loadMoreLikeUsers" reportId="" class="load-more-report-link-user d-none">
                        <input type="hidden" id="report_like_pagenum" value="1">
                        <div id="reportlikeloader" class="rep-like-users-animation-content people-row">
                            <div class="com-like-avatar-wrap" id="hide_replike_loader">
                                <div class="like-top-avatar-wrap animation"></div>
                                <div class="like-top-name animation"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>