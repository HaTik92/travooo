<div class="modal fade white-style" id="reportCountryList" tabindex="-1" role="dialog" aria-labelledby="reportlikesModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="list-top-info">
                    Report Countries and Cities
                </div>
                <div  class="report-location-list">
                    <div  class="location-list-modal-block">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>