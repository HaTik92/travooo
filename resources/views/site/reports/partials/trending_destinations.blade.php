@if(count($top_places) > 0)
<div class="post-block report-trending-dest-block">
    <div class="post-side-top">
        <h3 class="side-ttl"><i class="trav-trending-destination-icon"></i>
            What People Are Writing About
            <span class="count">{{count($top_places)}}</span>
        </h3>
        <div class="side-right-control">
            <a href="javascript:;" class="slide-link report-slide-prev"><i class="trav-angle-left"></i></a>
            <a href="javascript:;" class="slide-link report-slide-next"><i class="trav-angle-right"></i></a>
        </div>
    </div>
    <div class="post-side-inner" style="overflow: hidden;">
        <ul class="loadReportDestAnimation discover-dest-animation-content">
            <li>
                <div class="dest-animation-card-inner">
                    <div class="dest-animation-wrap">
                        <p class="animation dest-animation-title"></p>
                        <p class="animation dest-animation-text"></p>
                    </div>
                </div>
            </li>
            <li>
                <div class="dest-animation-card-inner">
                    <div class="dest-animation-wrap">
                        <p class="animation dest-animation-title"></p>
                        <p class="animation dest-animation-text"></p>
                    </div>
                </div>
            </li>
            <li>
                <div class="dest-animation-card-inner">
                    <div class="dest-animation-wrap">
                        <p class="animation dest-animation-title"></p>
                        <p class="animation dest-animation-text"></p>
                    </div>
                </div>
            </li>
        </ul>
        <div class="post-slide-wrap slide-hide-right-margin">
            <ul class="reportTrendingDescription post-slider d-none">
                @foreach($top_places as $tplace)
                <li>
                    <a href="{{url('place/'.$tplace->id)}}" target="_blank">
                        <img class="lazy" src="{{(isset($tplace->getMedias[0]->url))?'https://s3.amazonaws.com/travooo-images2/th1100/'.$tplace->getMedias[0]->url:'http://s3.amazonaws.com/travooo-images2/placeholders/place.png'}}"
                             alt="{{@$tplace->transsingle->title}}" style="width:210px;height:250px;">
                        <div class="post-slider-caption transparent-caption">
                            <p class="post-slide-name">{{@$tplace->transsingle->title}}</p>
                            <div class="post-slide-description">
                                <span class="tag">{{do_placetype(@$tplace->place_type)}}</span>
                                in {{@$tplace->city->transsingle->title}}
                            </div>
                        </div>
                    </a>
                    <div class="dest-stories-count">
                    {{@$tplace->visits}} Stories
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif
