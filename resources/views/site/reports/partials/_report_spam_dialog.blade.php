<!-- Spam Report Box Strat -->
<div class="modal white-style fade" tabindex="-1" role="dialog" id="spamReportModal" aria-hidden="true" style="z-index:1051">
    <div class="modal-dialog modal-custom-style modal-700" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog-share-style">
                <div class="post-side-top">
                    <h3 class="side-ttl">Report Post</h3>
                    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                        <i class="trav-close-icon"></i>
                    </button>
                </div>
                <div class="post-content-inner" data-toggle="buttons">
                    <div class="form-group d-flex">
                        <i class="fa fa-exclamation-circle report-exclamation" aria-hidden="true"></i>
                        <h5 class="d-inline-block">What is the problem with this post?</h5>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="btn report-spam-btn active">
                                    <input type="radio" name="type" class="d-none" id="gridRadios1" value="harassment" autocomplete="off" checked> Harassment
                                </label>
                            </div>
                            <div class="col-sm-6">
                                 <label class="btn report-spam-btn">
                                     <input type="radio" name="type" class="d-none" id="gridRadios1" value="spam" autocomplete="off"> Spam
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="btn report-spam-btn">
                                    <input type="radio" name="type" class="d-none" id="gridRadios1" value="violence" autocomplete="off"> Violence
                                </label>
                            </div>
                            <div class="col-sm-6">
                                 <label class="btn report-spam-btn">
                                     <input type="radio" name="type" class="d-none" id="gridRadios1" value="hate_speech" autocomplete="off"> Hate Speech
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                 <input type="text" class="form-control report-span-input" id="spamText"  placeholder="Something Else" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="row">
                            <div class="col-sm-12">
                                 <input type="submit" class="btn send-report-btn" id="spamSend" value="Send">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="dataid" id="dataid">
                    <input type="hidden" name="posttype" id="posttype">
                </div>
            </div>

        </div>
    </div>

    <!-- Spam Report Box End -->

    <script>
        // report spam......


        $("#spamReportModal").on('hidden.bs.modal', function (event) {
            var modal = $(this);
            modal.find("#dataid").val('');
            modal.find("#posttype").val('');
            modal.find("input[name=type]")[0].click();
            modal.find("#spamText").val("");
        });
        $("#spamSend").click(function () {
            var modal = $("#spamReportModal");
   
            var data = {
                type: modal.find("input[name=type]:checked").val(),
                text: modal.find("#spamText").val(),
                data_id: modal.find("#dataid").val(),
                post_type: modal.find("#posttype").val()
            };
            
            $.ajax({
                url: "{{route('post.spam')}}",
                type: "POST",
                data: data,
                dataType: "json",
                success: function (data, status) {
                    modal.find(".modal-close").click();
                $.alert({
                    icon: 'fa fa fa-check',
                    title: '',
                    type: 'green',
                    offsetTop: 0,
                    offsetBottom: 500,
                    scrollToPreviousElement:false,
                    content: data,
                });
                },
                error: function () {}
            });
        });
    </script>