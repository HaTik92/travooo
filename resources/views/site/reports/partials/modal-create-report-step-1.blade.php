<!-- create travlog step 1 popup -->
<div class="modal fade white-style" data-backdrop="false" id="createTravlogPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-1030" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog post-create-travlog">
                <form method='post' id='form_report_step1' autocomplete="off">
                    <div class="top-title-layer">
                        <h3 class="title">
                            <span class="circle">1</span>
                            <span class="txt">Travelog Details</span>
                        </h3>
                        <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="trav-close-icon"></i>
                        </button>
                    </div>
                    <div class="travlog-details-block">
                        <div class="detail-row text-content">
                            <div class="title-block">
                                <div class="title">Travelog Title</div>
                                <div class="subtitle">Where to go this summer...</div> 
                            </div>
                            <div class="input-layer">
                                <div class="input-block">
                                    <input class="detail-input create-rep-title-input" name="title" id="title" maxlength="100" type="text" placeholder="What is the Travelog about? (Ex: Top 10 Must-See Places in the USA!)">
                                </div>
                                <label for="title" class="error" style="color:red"></label>
                            </div>
                            <div class="post-title-label">
                                <div class="label-txt" id='titleLength'>MAX characters: 100</div>
                                <div class="label-txt d-none remaining-content" id='titleRemainigLength'><span></span> Characters remaining...</div>
                            </div>
                        </div>
                        <div class="detail-row text-content">
                            <div class="title-block">
                                <div class="title">Description</div>
                                <div class="subtitle">Short description...</div>
                            </div>
                            <div class="input-layer">
                                <div class="input-block">
                                    <textarea class="detail-input" name="description" maxlength="260" id="description" cols="30" rows="6" placeholder="Short description (optional)" style="height:125px;line-height: 1.2;resize:none"></textarea>
                                </div>
                            </div>
                            <div class="post-title-label">
                                <div class="label-txt" id='descriptionLength'>MAX characters: 260</div>
                                <div class="label-txt d-none remaining-content" id='descRemainingLength'><span></span> Characters remaining...</div>
                            </div>
                        </div>
                        <div class="detail-row">
                            <div class="title-block">
                                <div class="title">Locations</div>
                                <div class="subtitle">Select Countries or Cities Where you go...</div>
                            </div>
                            <div class="input-layer">
                                <div>
                                    <div class="select-country-block">
                                        <div class="search-country-input" id="forLocation">
                                            <select name="locations_ids[]" id="locations_id"  data-placeholder="What countries or cities are part of this Travelog?"  class="" style="width:100%">
                                            </select>
                                        </div>
                                        <label for="locations_id" class="error country-label" style="color:red"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="post-title-label">
                                <div class="label-txt" id='descriptionLength'>Selected locations:</div>
                                <div class="selected-locetion-block">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-foot-btn">
                        <input type="hidden" name="report_id" value="0" id="step1_report_id"  />
                        <button type="button" class="btn btn-transp btn-clear report-cancel" type="cancel" data-dismiss="modal" aria-label="Close">Cancel</button>
                        <button class="btn btn-light-primary btn-bordered" id="step1_next" disabled="disabled">Next</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

