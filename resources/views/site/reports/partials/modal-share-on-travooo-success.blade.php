<!-- share on trav popup -->
<div class="modal fade" data-backdrop="true" id="shareOnTravSuccessPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-custom-style modal-650" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-travlog-share-style">

                <div class="post-content-inner">
                    <div class="travlog-info-block">
                        <div class="travlog-info-inner share-modal-content">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>