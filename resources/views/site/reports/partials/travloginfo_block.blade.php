@foreach($reports AS $report)
    @if(@$report && @$report->author)
        @php
            if($is_draft){
                $rep_cover = @$report->draft_report->cover[0];

                $title = @$report->draft_report->title;
                $description = @$report->draft_report->description;
            }else{
                $rep_cover = @$report->cover[0];
                $title = $report->title;
                $description = @$report->description;
            }

            if(isset($rep_cover)){
                $cover = $rep_cover->val;
                $cover_data = $rep_cover->val;
            }else{
                $cover = asset('assets2/image/placeholders/no-photo.png');
                $cover_data = '';
            }
        @endphp
        <div class="travlog-info-block" style="width:100%;padding-bottom: 0px;">
            <div class="travlog-info-inner" style="border:none;flex-direction:row;display:flex;border-bottom: 1px solid #e6e6e6;border-radius:0px;">
                <div class="info-wrapper" style="width:70%;">
                    <div class="country-name" style="padding-bottom: 8px;">
                        @php
                            $num_output = 0; 
                            $flags =get_report_locations_info($report->id, 'publish');

                            if(isset($f_locations) && count($f_locations) > 0){
                                foreach($f_locations as $f_location){
                                      $found_key = array_search($f_location, array_column($flags, 'title'));
                                       $spec_value = $flags[$found_key];
                                       unset($flags[$found_key]);
                                       array_unshift($flags,$spec_value);
                                }
                            }

                        @endphp
                        @foreach($flags as $locations)
                            @if(isMobileDevice())
                                <span class="more-location-content">
                                    @if($locations['iso_code'] !='')
                                                <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($locations['iso_code'])}}.png"
                                                     alt="flag" style="width: 24px;vertical-align: baseline;">
                                            @endif
                                    <a style="color:#4080ff;" href="{{$locations['link']}}">
                                        {{$locations['title']}}
                                    </a> @if(!$loop->last)<span class="separator">, </span> @endif
                                </span>
                            @else
                                @if($num_output < 2)
                                @if($locations['iso_code'] !='')
                                <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($locations['iso_code'])}}.png"
                                    alt="flag" style="width: 24px;vertical-align: baseline;">
                                @endif
                                <a style="color:#4080ff;" href="{{$locations['link']}}">
                                    {{$locations['title']}}
                                </a> @if(!$loop->last)<span class="separator">, </span> @endif
                                @elseif($num_output == 2)
                                <a href="javascript:;" class="form-link show-more-location">+ {{count($flags)-2}} More</a>
                                <span class="more-location-content d-none">
                                    @if($locations['iso_code'] !='')
                                    <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($locations['iso_code'])}}.png"
                                        alt="flag" style="width: 24px;vertical-align: baseline;">
                                    @endif
                                    <a style="color:#4080ff;" href="{{$locations['link']}}">
                                        {{$locations['title']}}
                                    </a> @if(!$loop->last)<span class="separator">, </span> @endif
                                </span>
                                @else
                                <span class="more-location-content d-none">
                                    @if($locations['iso_code'] !='')
                                    <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($locations['iso_code'])}}.png"
                                        alt="flag" style="width: 24px;vertical-align: baseline;">
                                    @endif
                                    <a style="color:#4080ff;" href="{{$locations['link']}}">
                                        {{$locations['title']}}
                                    </a> @if(!$loop->last)<span class="separator">, </span> @endif
                                </span>
                                @endif
                                @php $num_output ++;@endphp
                        @endif
                        @endforeach
                    </div>
                    <div class="info-title" style="font-family: inherit;font-weight: bold;text-overflow:initial;">
                        <a style="color:black;" href="{{url('reports/'.$report->id)}}">
                            @if(isset($searched_text) && !empty($searched_text))
                                {!! markSearchTerm($searched_text, str_limit($title, 43, '<span>...</span>')) !!}
                            @else
                                {!! str_limit($title, 43, '<span>...</span>') !!}
                            @endif
                        </a>
                    </div>
                    <div class="info-title" style="height: 25px;font-family: inherit;font-size: 16px;text-overflow:initial;">
                        <a href="{{url('reports/'.$report->id)}}">
                            @if(isset($searched_text) && !empty($searched_text))
                                {!! markSearchTerm($searched_text, str_limit($description, 54, '<span>...</span>')) !!}
                            @else
                                {!! str_limit($description, 54, '<span>...</span>') !!}
                            @endif
                        </a>
                    </div>
                    <div class="foot-block-grid">
                        <div class="foot-block">
                            <div class="author-side">
                                <div class="img-wrap">
                                    <img class="ava" src="{{check_profile_picture($report->author->profile_picture)}}" alt="avatar" style="width:25px!important;height:25px;">
                                </div>
                                <div class="author-info report-author-info">
                                    <div class="author-name">
                                        @php $limitation = @$report->author->expert?12:19;  @endphp
                                        <a style="color:black;font-size:15px;align-content:center" href="{{url('profile/'.$report->author->id)}}"
                                        class="author-link">{{str_limit($report->author->name, $limitation, '...')}} </a>
                                        {!! get_exp_icon($report->author) !!}
                                    </div>
                                    @if(Auth::check() && $report->author->id == Auth::user()->id)
                                        <div class="dot-info-block"><span class="comment-dot">&bull;</span><span class="your-trav-info"> YOUR TRAVELOG</span></div>
                                    @endif
                                </div>

                            </div>

                            <div class="like-side">
                                <div class="post-foot-block">
                                    <i class="trav-heart-fill-icon"></i>&nbsp;
                                    <span>{{optimize_counter($report->likes()->count())}}</span>
                                </div>
                                <div class="post-foot-block">
                                    <i class="trav-comment-icon"></i>&nbsp;
                                    <span>{{optimize_counter($report->comments()->count())}}</span>
                                </div>
                            </div>

                        </div>
                        <div class="time" style="font-size: 14px;">
                            {{diffForHumans(($report->published_date != NULL)?$report->published_date:$report->created_at)}}
                        </div>
                    </div>

                </div>
                <div class="img-wrap report-img-wrap" style="width:30%; text-align: center; padding: 10px; max-height: 170px;">
                    <a href="{{url('reports/'.$report->id)}}">
                        <img src="{{$cover}}" alt="{{$report->title}}" style="width:100%;height:130px; max-width: 100%; border-radius: 0px !important;">
                        @if(Auth::check() && @$report->author->id == Auth::user()->id)
                        <span class="your-trav-info" style="display: none">YOUR TRAVELOG</span>
                        @endif
                    </a>

                    <div class="report-edit-action">
                        <div class="dropdown ">
                        <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-caret-down"></i>
                                </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow report-edit-dropdown" posttype="Report">
                                @if(Auth::check() && @$report->author->id == Auth::user()->id)
                                <a href="javascript:;" data-id="{{$report->id}}" data-type="from-list" data-cover_img="{{$cover_data !=''?$cover_data.'?t='.time():''}}" class="editReport dropdown-item report-edit-link rep-link-{{$report->id}}">
                                    <span class="icon-wrap panel-icon-wrap">
                                        <i class="trav-pencil" aria-hidden="true"></i>
                                    </span>
                                    <div class="drop-txt report-edit__drop-text">
                                        <p><b>Edit Travelog</b></p>
                                    </div>
                                </a>
                                @if($report->flag == 0)
                                <a href="javascript:;"  data-id="{{$report->id}}" class="dropdown-item publish-report" data-ftype="list" data-hascover="{{$cover_data}}">
                                    <span class="icon-wrap panel-icon-wrap">
                                        <i class="fa fa-bullhorn"></i>
                                    </span>
                                    <div class="drop-txt report-edit__drop-text">
                                        <p><b>Publish</b></p>
                                    </div>
                                </a>
                                @endif
                                <a href="javascript:;" class="dropdown-item delete-report" deltype="1" data-id="{{$report->id}}">
                                    <span class="icon-wrap close-icon-wrap">
                                        <i class="trav-close-icon"></i>
                                    </span>
                                    <div class="drop-txt report-delete__drop-text">
                                        <p><b>Delete</b></p>
                                    </div>
                                </a>
                                @else
                                @include('site.home.partials._info-actions', ['post'=>$report])
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        @if((($loop->index == 4 && !$from_last) || ($loop->last && $from_last)))
            @include('site.reports.partials.trending_destinations')
        @endif
    @endif
@endforeach
