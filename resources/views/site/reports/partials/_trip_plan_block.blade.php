<div class="tp-main-block pos-relative" plan_id="{{$plan->id}}">
    @if($plan->privacy != 0)
        <div class="trip-has-private"></div>
    @endif
        <div class="private-error-block" @if($plan->privacy != 0) style="display:block;" @endif>You Can not select friends/private plan.</div>
    <div class="trip-plan-row" title="{{$plan->title}}">
        <div style='width:5%'>
            <input type='radio' name='plan_id' value="{{$plan->id}}" class="insert-plan-input" data-type="{{($plan->privacy != 0)?'private':''}}" required />
        </div>
        <div class="trip-plan-inside-block">
            <div class="trip-plan-map-img">
                <img src="{{get_plan_map($plan, 140, 150)}}"
                     alt="map-image" style='width:140px;height:150px;'>
            </div>
        </div>
        <div class="trip-plan-inside-block trip-plan-txt">
            <div class="trip-plan-txt-inner">
                <div class="trip-txt-ttl-layer">
                    <h2 class="trip-ttl t-overflow">{{$plan->title}}</h2>
                    <p class="trip-date">{{weatherDate($plan->version->start_date)}}
                        @lang('chat.to') {{weatherDate($plan->version->end_date)}}</p>
                    @if(Auth::user()->id!=$plan->users_id)
                    <p class="trip-date" style='margin:0px'>@lang('profile.invited')
                        @lang('chat.by'): {{@$plan->author->name}}</p>
                    @endif
                </div>
                <div class="trip-txt-info">
                    <div class="trip-info">
                        <div class="icon-wrap">
                            <i class="trav-clock-icon"></i>
                        </div>
                        <div class="trip-info-inner">
                            <p>
                                <b>{{calculate_duration($plan->id, 'all')}}</b>
                            </p>
                            <p>@lang('trip.duration')</p>
                        </div>
                    </div>
                    <div class="trip-info">
                        <div class="icon-wrap">
                            <i class="trav-distance-icon"></i>
                        </div>
                        <div class="trip-info-inner">
                            <p><b>{{calculate_distance($plan->id)}}</b></p>
                            <p>@lang('trip.distance')</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="trip-plan-inside-block" style='width:25%'>
            <div class="dest-trip-plan">
                <h3 class="dest-ttl">@choice('trip.destination', 2)
                    <span>{{$plan->trips_places()->count()}}</span></h3>
                <ul class="dest-image-list">
                    @if($plan->trips_places()->count())
                    @foreach($plan->trips_places AS $places)
                    <li>
                        <img src="{{@check_place_photo2($places->place)}}"
                             alt="photo" style='width:52px;height:52px;'>
                        @if($plan->trips_places()->count() > 6 && $loop->iteration === 6)
                        <div style="color: #fff;
                             position: relative;
                             width: 52px;
                             height: 52px;
                             background: #000;
                             opacity: .6;
                             bottom: 52px;
                             text-align: center;
                             padding-top: 19px;
                             ">+{{$plan->trips_places()->count() - 6}}</div>
                        @break
                        @endif
                    </li>
                    @endforeach
                    @else
                    <li style='font-size:14px;'>
                        @lang('trip.no_destinations_added_yet')
                    </li>
                    @endif

                </ul>
            </div>
        </div>
    </div>
</div>
