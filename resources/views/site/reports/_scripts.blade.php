<script data-cfasync="false" src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js' type="text/javascript"></script>
<script data-cfasync="false" src="{{url("assets2/js/choosen/chosen.jquery.min.js") }}" type="text/javascript"></script>

<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link href="{{asset('/plugins/croppie/croppie.css')}}" rel="stylesheet" type="text/css" />
<script data-cfasync="false" src="{{asset('assets2/js/plugin/quill/quill.js')}}" type="text/javascript"></script>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script> -->
<script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.js" type="text/javascript" integrity="sha512-IrOddgcDXibfbd7y5H6IbkvWOPNTrtSvRmscZpoLk0bNuOZb7LLvsjxmnzolh+5zNO9lgPKghPJbLIFJ5a3VYQ==" crossorigin="anonymous"></script>
<script data-cfasync="false" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
<script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js" type="text/javascript"></script>
<script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.js" type="text/javascript"></script>

<script data-cfasync="false" src="{{url('assets2/js/jquery-loading-master/dist/jquery.loading.min.js')}}" type="text/javascript"></script>

<script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js" type="text/javascript"></script>
<script data-cfasync="false" src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}" type="text/javascript"></script>
<script data-cfasync="false" src="{{asset('/plugins/croppie/croppie.js')}}" type="text/javascript"></script>
<script data-cfasync="false" src="{{asset('assets2/js/lightbox2/src/js/lightbox.js')}}" type="text/javascript"></script>
<script data-cfasync="false" src="{{ asset('assets2/js/summernote/summernote.js') }}" type="text/javascript"></script>
<script data-cfasync="false" src="{{ asset('/plugins/summernote-master/tam-emoji/js/config.js') }}" type="text/javascript"></script>
<script data-cfasync="false" src="{{ asset('/plugins/summernote-master/tam-emoji/js/tam-emoji.js?v='.time()) }}" type="text/javascript"></script>
<script data-cfasync="false" src="{{ asset('/plugins/summernote-master/tam-emoji/js/textcomplete.js?v='.time()) }}" type="text/javascript"></script>
<script data-cfasync="false" src="{{ asset('/plugins/quill-mention/quill.mention.min.js?v='.time()) }}" type="text/javascript"></script>
<script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>
<script src="{{asset('assets2/js/plugin/touch-punch-master/jquery.ui.touch-punch.min.js')}}"></script>


<!--<script src="{{ asset('js/sort.js?v=0.2') }}"></script>-->
<script data-cfasync="false" type="text/javascript">
    var isTouchDevice = "ontouchstart" in window || navigator.msMaxTouchPoints > 0;
    //save draft
    var draft_report_id;
    var draft_data;
    var changeTimer = false;
    var report_maps;
    var report_map_func;
    var show_map_init = (show_map_init)?show_map_init:'';
    var slider;
    var loader_item = false;
    var check_video_link = false;
    var plan_filter = '';
    var plan_type = 'mine';
    var check_el = 3;
    var check_uploader = true;
    var check_image_uploader = true;
    var check_video_uploader = true;
    var check_cover_uploader = true;
    var change_titleTimer = false;
    var trav_info_list = [];
    var quill_list = [];

    var connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;

    if(typeof connection !== 'undefined'){
        connection.addEventListener('change', updateOnlineStatus);
    }

    window.addEventListener('load', updateOnlineStatus);

    window.addEventListener('online',  updateOnlineStatus);
    window.addEventListener('offline', updateOnlineStatus);


    function updateOnlineStatus(event) {
        if(navigator.onLine){
            $('.report-offline-status').css('display', 'none');
        }else{
            $('.report-offline-status').css('display', 'flex');
        }

        if(typeof connection !== 'undefined' && connection.effectiveType === '2g' && !navigator.onLine){
            $('.report-offline-status').css('display', 'flex');
        }
    }


    
    $(document).on('click', '#createTravlogNextPopup', function(e){
        var video_container = $(".video-dropdown.video-draggable"); 
        if(!video_container.is(e.target) && video_container.has(e.target).length === 0 && video_container.parent().hasClass('droppable-here')){
            video_container.parent().removeClass('droppable-here')
            $('#sortable').removeClass('for-draggable')
            video_container.closest('.drug-icon-block').find('.video-dropdown.video-draggable').remove()
        }
      
    })

    // For spam reporting
    function injectData(id, obj)
    {
        // var button = $(event.relatedTarget);
        var posttype = $(obj).parent().attr("posttype");
        $("#spamReportDlg").find("#dataid").val(id);
        $("#spamReportDlg").find("#posttype").val(posttype);
    }


    lazyloader();
    function lazyloader(){
        $('.rep-lazy').lazy();
    }
    $(document).on('DOMNodeInserted', 'img.rep-lazy', function() {
        $(this).lazy();
    });
    
    
    //report edit icon show/hide
    $('body').on('mouseover', '.report-img-wrap', function () {
       $(this).find('.report-edit-action').show()
    });

    $('body').on('mouseout', '.report-img-wrap', function () {
       $(this).find('.report-edit-action').hide()
    });
    
    $('body').on('click', '.report_likes_modal', function (e) {
            reportId = $(this).attr('id');
            var report_like_user_count = $(this).find('.report-like-count b').html();
            $.ajax({
                method: "POST",
                url: "{{ route('reportlikes4modal') }}",
                data: {report_id: reportId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
            
                    if(report_like_user_count > 10){
                        $('#loadMoreLikeUsers').removeClass('d-none');
                        $('#loadMoreLikeUsers').attr('reportId', reportId);
                    }
                        
                    $('#reportlikesModal').find('.report-modal-like-count').html(report_like_user_count+ ' Likes')
                    $('#reportlikesModal').find('.report-like-modal-block').html(result);
                    $('#reportlikesModal').modal('show');
                });
            e.preventDefault();
        });
     
        
        $('body').on('click', '.report_comment_likes_modal', function (e) {
            commentId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('reportcommentlikes4modal') }}",
                data: {comment_id: commentId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    
                    $('#reportlikesModal').find('.modal-body').html(result);
                    $('#reportlikesModal').modal('show');
                });
            e.preventDefault();
        });
        
    var redirect_url = '{{route("report.list")}}';
    
     //Delete report
    $('body').on('click', '.delete-report', function (e) {
        var report_id = $(this).attr('data-id');
        var del_type = $(this).attr('deltype');
        var _this = $(this);
        
            if(report_id != ''){
                 var url = '{{ route("report.delete", ":reportId") }}';
                 url = url.replace(':reportId', report_id);
                 
                $.confirm({
                    title: 'Are you sure?',
                    content: '<span style="font-size: 18px;line-height: 1.3;">If you delete this Report, you will lose all your progress and any setting you added!</span> <div class="mb-3"></div>',
                    columnClass: 'col-md-5 col-md-offset-5',
                    closeIcon: true,
                    offsetTop: 0,
                    offsetBottom: 500,
                    scrollToPreviousElement:false,
                    scrollToPreviousElementAnimate:false,
                    buttons: {
                        cancel: function () {},
                        confirm: {
                            text: 'Confirm',
                            btnClass: 'btn-danger',
                            keys: ['enter', 'shift'],
                            action: function(){
                                $.ajax({
                                    method: "POST",
                                    url: url,
                                    data: {type: 'delete'}
                                })
                                .done(function (res) {
                                      var result = JSON.parse(res);

                                      if(result.status == 'yes'){
                                            if(del_type == '1'){
                                                _this.closest('.travlog-info-block').fadeOut();
                                            }else{
                                                window.location.href = redirect_url;
                                            }
                                      }


                                });
                            }
                        }
                    }
                });
            }
        });
       
       //publish report
        $('body').on('click', '.publish-report', function(){
           var report_id = $(this).data('id');
           var type = $(this).data('ftype');
           var has_cover = false;

           if(type == 'single' &&  $('#editReport').data('cover_img').length < 15){
               has_cover = true;
           }

            if(type == 'list' &&  $(this).data('hascover') == ''){
                has_cover = true;
            }

            if (has_cover){
                $.alert({
                    icon: 'fa fa-warning',
                    title: '',
                    type: 'orange',
                    offsetTop: 0,
                    offsetBottom: 500,
                    scrollToPreviousElement:false,
                    scrollToPreviousElementAnimate:false,
                    content: 'For publish please upload cover image.',
                });
                return false;
            }

           $.ajax({
                   method: 'POST',
                   url: '{{route("report.publish")}}',
                   data: {report_id: report_id}
               })
               .done(function (res) {
                     var result = JSON.parse(res);

                     if(result.status == 'yes'){
                         if(type == 'list'){
                             window.location.href = redirect_url;
                         }else{
                             window.location.reload()
                         }
                     }


               });
       })

    var check_error = true;
       //publish report from modal
       $('body').on('click', '.save-publish', function(e){
            e.preventDefault();
           check_el = 0;
           check_error = true;

            var status = navigator.onLine;
                if (!status) {
                    return;
                }

            $('.ql-editor').each(function (index, value) {
                var HTML = $(this).html();
                $(this).parent().siblings("input").each(function (index2, value2) {
                    if(removeHtmlTags(HTML) != ''){
                        $(this).val(HTML);
                    }else{
                        $(this).val('');
                    }

                });
            });

            var cover_img = $('#editReport').data('cover_img');

            if (((typeof cover_img != "undefined" && cover_img.length < 15) || typeof cover_img == "undefined") && $('#coverImage').val() == '') {
                $("#message").html("<p style='color:red'>The cover image is required.</p>");
                $('#createTravlogNextPopup').animate({ scrollTop: 0 }, 400);
                return;
            }


              disableButtons();
                    
                    if(validations(".videoUrl", 'Please insert a valid video URL from Youtube, Dailymotion or Vimeo...') ){
                        if( $('.videoUrl').closest('.input-block').find('.items-error').length > 1){
                            $('.videoUrl').closest('.input-block').find('.items-error:first').remove()
                        }
                        check_error = false;
                    }

                    if(validations(".rep-people", 'The users filed is required.')){
                        check_error = false;
                    }
                    if(validations(".rep-maps", 'The maps filed is required.')){
                        check_error = false;
                    }
                    if(validations(".rep-places", 'The places filed is required.')){
                        check_error = false;
                    }

                     if(validations(".rep-text-input", 'The text is required.', 'text') ){
                        check_error = false;
                    }
                    
                    scrollToSowError('.rep-text-input')
                    scrollToSowError('.videoUrl')
                    scrollToSowError('.rep-places')
                    scrollToSowError('.rep-maps')
                    scrollToSowError('.rep-people')
                    checkMediaUrl(e)
                    
                    // Proceed if no errors
                    if(check_error){
                        $('.side.right ul').hide();
                        var report_id = $('#step2_report_id').val();
                        if(report_id != 0){
                                $.ajax({
                                method: 'POST',
                                url: '{{route("report.publish")}}',
                                data: {report_id: report_id}
                            })
                            .done(function (res) {
                                    var result = JSON.parse(res);

                                    if(result.status == 'yes'){
                                        window.location.href = "{{url('reports')}}/" + report_id;
                                    }
                            });
                        }
                    }
        })

        
        //info modal tabs
        $(document).on('click', '.travinfo-tab', function () {
            var current_tub = $(this).attr('data-tab');
            
            $('.travlog-info-wrapper .info-tab-content').each(function(){
                if($(this).hasClass(current_tub)){
                    $(this).removeClass('d-none');
                }else{
                     $(this).addClass('d-none');
                }
            })
            
            $(this).parent().find('.current').removeClass('current')
            $(this).addClass('current')
        })


    //report trending destinetion script
          setTimeout(function () {
           slider =  $(".reportTrendingDescription").lightSlider({
                autoWidth: true,
                pager: false,
                controls: false,
                slideMargin: 25,
                loop: false,
                onSliderLoad: function (el) {
                    $('.loadReportDestAnimation').addClass('d-none')
                    $('.reportTrendingDescription').removeClass('d-none')
                        slider.refresh();
                    $(el).height(275);
                }, });
        }, 2500);
        
        
        $(document).on('click', '.report-slide-prev', function () {
            slider.goToPrevSlide(); 
        })
    
        $(document).on('click', '.report-slide-next', function () {
            slider.goToNextSlide(); 
        })
   
   //show more location
    $(document).on('click', '.show-more-location', function(){
       $(this).closest('.country-name').find('.more-location-content').removeClass('d-none')
       $(this).addClass('d-none')
    })
    
    //Get Mine reports
    $('#formSelectMine').click(function () {
        var href =  $(this).attr('redurl');
        window.location.href = href;

        $('#formSelectAll').removeClass('current-tab');
        $('#formSelectMine').addClass('current-tab');
        $("#inputSelectMine").prop("checked", true);

    });

    //Get All reports
    $('#formSelectAll').click(function () {
        var href =  $(this).attr('redurl');
        window.location.href = href;

        $('#formSelectMine').removeClass('current-tab');
        $('#formSelectAll').addClass('current-tab');
        $("#inputSelectAll").prop("checked", true);
    });
    
  
//---START--- create/edit first step

 $(document).on('click', '#create_new_report', function () {
     $('#title').val('')
     $('#description').val('')
     $('#locations_id').val('')
     $('#locations_id').html(''); 
     $('#step1_report_id').val(0); 
     $('.selected-locetion-block').html('')
     $('#descRemainingLength').addClass('d-none')
     $('#titleRemainigLength').addClass('d-none')
     $('#form_report_step1 .error').html('')
   
     $('#cropimage').croppie('destroy');
     $('#cover').show()
    
           $('#step1_next').removeAttr('disabled')
        })


   //Get first step data
    function init_first_step_modal(id) {
        if(id != 0) {
           
            $.get("{{url('reports/ajaxGetInfoByID')}}",{id: id})
                .done(function(data){
                    if(data !== undefined){
                        $('.selected-locetion-block').html('');     
                        $('#locations_id').html('');  
                        $('#step1_report_id').val(id)
                            for(var i in data.locations) {
                                   
                                    var option = new Option(data.locations[i].text,data.locations[i].id, true, true);
                                    $('#locations_id').append(option).trigger('change');
                                     addSelectedLocationHtml(data.locations[i].id, data.locations[i].info);
                                }
                           
                                if(data.description != null){
                                    $('#description').val(data.description);
                                    $('#descRemainingLength').removeClass('d-none')
                                    $('#descRemainingLength span').html(260 - data.description.length)
                                    
                                }
                                if(data.title != null){
                                    $('#title').val(data.title);
                                    $('#step2_title').val(data.title);
                                    $('#titleRemainigLength').removeClass('d-none')
                                    $('#titleRemainigLength span').html(100 - data.title.length)
                                }
                     
                    }
                    
                }) 
                .always(function() {
                   $('#step1_next').removeAttr('disabled')
                  });
        }
    }
    var location_selected_values = [];
    var isSelectedCheck = false;
    //select locations
    $('#locations_id').select2({
        placeholder: 'What countries or cities are part of this Travlog?',
        minimumInputLength: 3,
         multiple: true,
        ajax: {
            url: "{{url('reports/ajaxSearchLocation')}}",
            dataType: 'json',
            processResults: function (data) {
                isSelectedCheck = true
                return {
                        results: $.map(data, function(obj) {
                            return { id: obj.id, text: obj.text, image:obj.image, type:obj.type, query:obj.query };
                        })
                    };
            }
        },
         templateSelection: function (data, container) {
     
             if($.inArray(data.id , location_selected_values) != -1){
                $(data.element).attr('data-custom-attribute', data.text);
                return data.text;
             }else{
                  location_selected_values.push(data.id);
             }
          },
      
        templateResult: function (response) {
        if(response){
                var markup = '<div class="search-country-block">';
                if(response.image){
                    markup +='<div class="search-country-imag"><img src="' + response.image + '" /></div>';
                }
                 markup +='<div class="span10">' +
                    '<div class="search-country-info">' +
                    '<div class="search-country-text">' + markLocetionMatch(response.text, response.query) + '</div>';
            
                    if(typeof response.type != 'undefined'){
                        if(response.type != ''){
                           markup += '<div class="search-country-type">City in '+ response.type +'</div>';
                        }else{
                            markup += '<div class="search-country-type">Country</div>';
                        }
                    }
                     markup +='</div></div>';

              return markup;
        }
   },
    
        dropdownParent: $('#forLocation'),
        containerCssClass: "select2-input location-select2",
        dropdownCssClass: "bigdrop",
        escapeMarkup: function (m) { return m; }
    });
    
         
      $('#locations_id').on('select2:select', function (e) {
            var data = e.params.data;
            addSelectedLocationRow(data.id);
            isSelectedCheck = false
            $('.error.country-label').html('');
         });
            
           
    $('#locations_id').on('select2:unselecting', function (e) {
         var selected_id = e.params.args.data.id;
            if(isSelectedCheck){
                if($.inArray(selected_id, location_selected_values) != -1){
                $('#locations_id').select2('close');
                e.preventDefault();
            }
        }
        isSelectedCheck = false
    }).trigger('change');
    
    $('#locations_id').on('select2:unselect', function (e) {
         var selected_id = e.params.data.id;
            $(this).find("option[value='"+ selected_id +"']").remove();
            removeSelectedLocationRow(selected_id)
    }).trigger('change');


    TextLimit($('#description'), 260);
    TextLimit($('#title'), 100);
    TextLimit($('#step2_title'), 100);

//create/edit step 1
   $('#step1_next').click(function (e) {
       
       if(checkconnection()){
            e.preventDefault();
            return;
        }

        $('#form_report_step1').validate({
            rules: {
                title: {
                required: true
            },
                'locations_ids[]': {
                required: true
                },
            },
            submitHandler: function(form) {
                var data = new FormData(form);
               $('#step1_next').prop('disabled', true)
                $.ajax({
                    method: "POST",
                    url: "{{url('reports/create')}}",
                    data: data,
                    cache:false,
                    contentType: false,
                    processData: false,
                })
                .done(function (res) {
                    $('#step2_title').val($('#title').val());
                    if($('#title').val().length != 0){
                        $('#step2_title').closest('.text-content').find('.remaining-content').removeClass('d-none')
                        $('#step2_title').closest('.text-content').find('.remaining-content span').html(100 - $('#title').val().length)
                    }
                
                    $('#step2_report_id').val(res.id);
                    $('#step1_report_id').val(res.id);
                    $('.step2-loader').show()
                    init_second_step_modal(res.id, res.info_count_step)
                    
                    $('#createTravlogPopup').modal();
                    $('#createTravlogNextPopup').modal();
                    $('#step1_next').prop('disabled', false)
                }); 
                
                e.preventDefault();
            }
        });


    });
    
    
//---END--- create/edit first step   


//---START--- create/edit second step
    var gmap;
    var image_element_flag = false;
    var drag_files = null;
    var image_file_triger = false;
    
    var video_element_flag = false;
    var drag_video_files = null;
    var video_file_triger = false;
    var points = [];
    var before_load = true;
    var selected_plan_ids = [];
    
    //get report info
    function init_second_step_modal(id, info_count_step){
        if(id != 0) {
            if (typeof $('#editReport').data('cover_img') != "undefined") {
                buindCroppeImage($('#editReport').attr('data-cover_img'));
            }
            if (typeof $('.rep-link-' + id).data('cover_img') != "undefined") {
                buindCroppeImage($('.rep-link-' + id).attr('data-cover_img'));
            }
            $('#sortable').html('');
            num_images = 0;
            num_texts = 0;
            num_videos = 0;
            num_local_video = 0;
            num_plans = 0;
            num_peoples = 0;
            num_maps = 0;
            num_infos = 0;
            num_places = 0;
            var image_id = 0;
            var text_id = 0;
            var video_id = 0;
            var local_video_id = 0;
            var plan_id = 0;
            var map_id = 0;
            var user_id = 0;
            var place_id = 0;
            var new_count_step = 0;

            $('#sortable').append('<div><div class="drug-icon-block first-droppable-1"></div></div>')
            $(".first-droppable-1").droppable({
                drop: function (event, ui) {
                    $(this).addClass('droppable-here')
                    var dragged_id = ui.draggable.attr('dataId')
                    $('#sortable').addClass('for-draggable')
                    if (dragged_id == 'video_dropdown') {
                        $(ui.draggable).clone().appendTo(this);
                        $(this).find('#rep_video_cont').click()
                    } else {
                        $('#' + dragged_id).click()
                    }
                }
            });


            var getData = function (rep_id, new_count_step) {
                $.get("{{url('reports/ajaxGetDraftByID')}}", {id: rep_id, step: new_count_step})
                    .done(function (data) {
                        if (data !== undefined) {

                            for (var i in data) {
                                var item = data[i]
                                if (item['var'] == 'photo') {
                                    image_id++;
                                    image_file_triger = true;
                                    $("#image_cont").click();
                                    var id = "add_image_cont" + image_id;
                                    var img_info = item['val'].split("-:");

                                    $('#' + id).find('img').attr('src', img_info[0]);
                                    $('#' + id).removeClass('d-none')
                                    $('#rep_image' + image_id).val(img_info[0])
                                    $('#image_hidden_' + image_id).val(JSON.stringify({'photo': item['val']}))

                                    $('#info_' + id).val(img_info[1]);
                                    if (img_info[2]) {
                                        $('#checkLink_' + id).click();
                                    }
                                    $('#link_' + id).val(img_info[2]);

                                } else if (item['var'] == 'text') {
                                    before_load = false;
                                    $("#text_cont").click();
                                    text_id++;
                                    var id = "add_text_cont" + text_id;
                                    $('#' + id).find('.ql-editor').html(item['val']);
                                    check_el = 3;

                                } else if (item['var'] == 'video') {
                                    check_draft = 2;
                                    before_load = false;
                                    $("#video_cont").click();
                                    video_id++;
                                    var tmp = item['val'].split('|');
                                    var id = "add_video_cont" + video_id;

                                    if(tmp.length > 1){
                                        var videoSrc = 'https://www.youtube.com/embed/' + tmp[1];
                                    }else{
                                        var videoSrc = tmp[0];
                                    }

                                    $("#" + id).find(".videoUrl").val(videoSrc).trigger("blur");
                                    $('#video_url_hidden_' + video_id).val(JSON.stringify({'video': videoSrc}))

                                    check_el = 3;
                                } else if (item['var'] == 'local_video') {
                                    video_file_triger = true;
                                    local_video_id++;
                                    $("#local_video_cont").click();
                                    var vodeo_info = item['val'].split("-:");
                                    var id = "add_local_video_cont" + local_video_id;
                                    $('#' + id).removeClass('d-none')
                                    $('#rep_video' + local_video_id).val(vodeo_info[0])
                                    $('#local_video_hidden_' + local_video_id).val(JSON.stringify({'local_video': item['val']}))

                                    $('#' + id).find('#add_video_src' + local_video_id).html('<video style="height:352px;" width="632" class="thumb" controls onplay="playLocalVideo()"><source src="' + vodeo_info[0] + '" type="video/mp4"></video>')
                                    $('#info_' + id).val(vodeo_info[1]);
                                    if (vodeo_info[2]) {
                                        $('#checkLink_' + id).click();
                                    }
                                    $('#link_' + id).val(vodeo_info[2]);
                                } else if (item['var'] == 'plan') {
                                    $("#plan_cont").click();
                                    plan_id++;
                                    var plan_div_id = 'add_plan_cont';
                                    var pNum = plan_id;
                                    plan_div_id = 'add_plan_cont' + (parseInt(pNum));
                                    var selected_plan = item['val'];

                                    if ($.inArray(selected_plan, selected_plan_ids) == -1) {
                                        selected_plan_ids.push(selected_plan)
                                    }

                                    var plan_get = function (plan_div_id, selected_plan, pNum) {
                                        $.ajax({
                                            method: "GET",
                                            url: "{{url('reports/ajaxGetPlanByID')}}",
                                            data: {plan_id: selected_plan, current_number:pNum},
                                            async: false,
                                        })
                                            .done(function (data) {
                                                var my_plan = JSON.parse(data);
                                                $('#sortable').append(planHtmlContent(plan_div_id, pNum, selected_plan, my_plan))
                                                $(".plan-droppable-" + pNum).droppable({
                                                    drop: function (event, ui) {
                                                        $(this).addClass('droppable-here')
                                                        $('#sortable').addClass('for-draggable')
                                                        var dragged_id = ui.draggable.attr('dataId')
                                                        if (dragged_id == 'video_dropdown') {
                                                            $(ui.draggable).clone().appendTo(this);
                                                            $(this).find('#rep_video_cont').click()
                                                        } else {
                                                            $('#' + dragged_id).click()
                                                        }
                                                    }
                                                });

                                                reloadSortable();
                                            });
                                    }
                                    plan_get(plan_div_id, selected_plan, pNum);
                                } else if (item['var'] == 'map') {
                                    before_load = false;
                                    $("#map_cont").click();
                                    map_id++;
                                    addMapCountryInfo('mapCountryInfo' + map_id, item['val']);

                                    var maps = item['val'].split("-:");
                                    if (maps.length > 2) {
                                        var zoom = (maps[3]) ? maps[3] : 10;
                                        var option = new Option(maps[2], item['val'], true, true);
                                        $('.report_map-add_map_cont' + map_id).val(maps[2]);
                                        insertMap('added_plan_map' + map_id, maps[0], maps[1], zoom, maps[2]);
                                    }
                                    $('#map_hidden_' + map_id).val(JSON.stringify({'map': item['val']}))

                                    check_el = 3;

                                } else if (item['var'] == 'user') {
                                    before_load = false;
                                    $("#people_cont").click();
                                    user_id++;
                                    var user_id_list = [];

                                    for (var ii in item['val']) {
                                        var option = new Option(item['val'][ii][1], item['val'][ii][0], true, true);
                                        $('.report_user' + user_id).append(option).trigger('change');
//                                     addPeopleRow(item['val'][ii][0], user_id);
                                        addPeopleExistingRow(item['val'][ii][2], user_id);
                                        user_id_list.push(item['val'][ii][0])
                                    }

                                    $('#people_hidden_' + user_id).val(JSON.stringify({'user': user_id_list.join(',')}))
                                    check_el = 3;
                                } else if (item['var'] == 'place') {
                                    if (item['val'][2].length > 0) {
                                        before_load = false;
                                        $("#place_cont").click();
                                        place_id++;

                                        $('#place_hidden_' + place_id).val(JSON.stringify({'place': item['val'][0]}))

                                        var option = new Option(item['val'][1], item['val'][0], true, true);
                                        $('.report-place-' + place_id).append(option).trigger('change');
                                        //                                     addPlaceRow(item['val'][0], place_id);
                                        addPlaceExistingRow(item['val'][2], place_id);
                                        check_el = 3;
                                    }
                                } else if (item['var'] == 'info') {
                                    before_load = false;
                                    var repinfo = item['val'].split(":-")
                                    addInfo(repinfo[0], repinfo[1], repinfo[2], repinfo[3])
                                    if(repinfo[2]){
                                        if(!(repinfo[2] in trav_info_list)){
                                            trav_info_list[repinfo[2]] = [repinfo[0]];
                                        }else{
                                            trav_info_list[repinfo[2]].push(repinfo[0]);

                                        }
                                    }
                                }
                            }



                            lazyloader();
                            reloadSortable();

                            if(new_count_step < info_count_step){
                                new_count_step++;
                                getData(rep_id, new_count_step)
                            }
                        }

                    })
                    .always(function () {

                        $('#createTravlogNextPopup').animate({scrollTop: 0}, 0);
                        $('.step2-loader').hide()
                    });
            }

            getData(id, new_count_step);
        }
    }
    
    
//Upload Cover Image    
    function uploadCoverImage(image, report_id){
        if(checkconnection()){
            return;
        }
    var fdata = new FormData();
    fdata.append('cover_image',image);
    fdata.append('report_id',report_id);
    check_cover_uploader = false;

        $.ajax({
            method: "POST",
            url: "{{route('report.upload.cover')}}",
            data: fdata,
            contentType: false,
            processData:false
          }).done(function(data){
                check_cover_uploader = true;
                if(check_cover_uploader && check_image_uploader && check_video_uploader){
                    enableButtons();
                }
              buindCroppeImage(data);
              $('#cropped_cover').val(data);
              $('#editReport').attr('data-cover_img', data)
              $('.rep-link-'+report_id).attr('data-cover_img', data)
              

          })
    }
    
//Upload local image    
    function uploadLocalImage(image_num, image, report_id){
        if(checkconnection()){
            return;
        }

        var fd = new FormData();
        fd.append('local_image',image[0]);
        fd.append('report_id',report_id);
        check_image_uploader = false;
        
        $.ajax({
            method: "POST",
            url: "{{route('report.upload.image')}}",
            data: fd,
            contentType: false,
            processData:false,
            async: true,
            xhr: function() {
                var xhr = $.ajaxSettings.xhr() ;
                xhr.upload.addEventListener("progress", function(evt) {
                    
                    if (evt.lengthComputable) {
                        var percentComplete = ((evt.loaded / evt.total) * 100);
                        var $progressText = $("."+ image_num +"-loader [data-progress-value]");

                        $("."+ image_num +"-loader .progress-bar").width(percentComplete + '%');
                        updateUploadProgressValueText($progressText, percentComplete);
                    }
                }, false);
                return xhr;
            },
            beforeSend: function(){
                $("."+ image_num +"-loader .progress-bar").width('0%');
            },
          }).done(function(data){
               check_image_uploader = true;
                if(check_cover_uploader && check_image_uploader && check_video_uploader){
                    enableButtons();
                }
              $("."+ image_num +"-loader").remove()
              $('#rep_image'+image_num).val(data)
              $('#image_hidden_'+image_num).val(JSON.stringify({'photo':data}))
               $("#createReportStep2").submit();

          })
    }
    //Update progress value text
    function updateUploadProgressValueText($elem, value) {
        $elem.text(parseInt(value, 10) + '%');
    }
    
//Upload local video    
    function uploadLocalVideo(video_num, video, report_id){
        if(checkconnection()){
            return;
        }
        var fd = new FormData();
        fd.append('local_video',video[0]);
        fd.append('report_id',report_id);
        check_video_uploader = false;
        $.ajax({
            method: "POST",
            url: "{{route('report.upload.video')}}",
            data: fd,
            contentType: false,
            processData:false,
            async: true,
            xhr: function() {
                var xhr = $.ajaxSettings.xhr() ;
                xhr.upload.addEventListener("progress", function(evt) {
                    
                    if (evt.lengthComputable) {
                        var percentComplete = ((evt.loaded / evt.total) * 100);
                        var $progressText = $("."+ video_num +"-video-loader [data-progress-value]");

                        $("."+ video_num +"-video-loader .progress-bar").width(percentComplete + '%');
                        updateUploadProgressValueText($progressText, percentComplete);
                    }
                }, false);
                return xhr;
            },
            beforeSend: function(){
                $("."+ video_num +"-video-loader .progress-bar").width('0%');
            },
          }).done(function(data){
                check_video_uploader = true;
                if(check_cover_uploader && check_image_uploader && check_video_uploader){
                    enableButtons();
                }
              $("."+ video_num +"-video-loader").remove()
              $('#rep_video'+video_num).val(data)
              $('#local_video_hidden_'+video_num).val(JSON.stringify({'local_video':data}))
              $("#createReportStep2").submit();
          })
    }
    
//disable draft/publish buttons
    function disableButtons(){
        $('.save-draft').attr('disabled', 'disabled')
        $('.save-publish').attr('disabled', 'disabled')
    }
    
//enable draft/publish buttons
    function enableButtons(){
        $('.save-draft').removeAttr('disabled')
        $('.save-publish').removeAttr('disabled')
    }


//Update report title
$(document).on('keyup', '#step2_title', function(e){
    if(e.keyCode != 13 && !e.shiftKey){
        var report_title = $(this).val();
        
       if(report_title != ''){
           enableButtons();
           $('#title').val(report_title)
            $('.report-title-error-block').addClass('d-none')
            if(change_titleTimer !== false) clearTimeout(change_titleTimer);
            change_titleTimer = setTimeout(function(){
                $("#createReportStep2").submit();
                change_titleTimer = false;
            },300);
       }else{
           $('.report-title-error-block').removeClass('d-none')
           disableButtons();
       }
   }
});


//Add report text    
 var num_texts = $('.add_text_cont').length;

    $('#text_cont').click(function () {
        
        if(checkconnection()){
            return;
        }

        var text_div_id = 'add_text_cont';
        var thisNum = parseInt(num_texts)+1;
        text_div_id = 'add_text_cont'+(parseInt(thisNum));

        if(!$('#sortable').hasClass('for-draggable')){
            $('#sortable').append(textHtmlContent(text_div_id, thisNum));
        }else{
            $('.droppable-here').parent().after(textHtmlContent(text_div_id, thisNum));
        }
            
            $(".text-droppable-" +thisNum).droppable({
                drop: function(event, ui) {
                    $(this).addClass('droppable-here')
                    var dragged_id = ui.draggable.attr('dataId')
                    $('#sortable').addClass('for-draggable')
                    if(dragged_id == 'video_dropdown'){
                        $(ui.draggable).clone().appendTo(this);
                        $(this).find('#rep_video_cont').click()
                    }else{
                        $('#' + dragged_id).click()
                    }
                }
            });


                            var formats = [
                                        'background',
                                        'bold',
                                        'color',
                                        'font',
                                        'code',
                                        'italic',
                                        'link',
                                        'size',
                                        'strike',
                                        'script',
                                        'underline',
                                        'blockquote',
                                        'header',
                                        'indent',
                                        'list',
                                        'align',
                                        'direction',
                                        'mention'
                                      ];
                            var toolbarOptions = [
                                ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                                ['blockquote', 'link'],

                                [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                                [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                                [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
                                [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
                                [{ 'direction': 'rtl' }],                         // text direction

                                [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

                                [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                                [{ 'font': [] }],
                                [{ 'align': [] }],
                                ['clean']                                         // remove formatting button
                            ];

        var quill = new Quill('#' + text_div_id + ' #text', {
                        formats: formats,
                        modules: {
                            toolbar: toolbarOptions,
                            mention: {
                                  allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
                                  mentionDenotationChars: ["@"],
                                  dataAttributes: ["id", "value", "image", "link"],
                                  showDenotationChar: false,
                                  source: function(searchTerm, renderList, mentionChar) {
                                       $.getJSON("{{url('reports/ajaxSearchMentionUser')}}", { q: searchTerm}, function(data){
                                            renderList(data, searchTerm);
                                        });
                                  },
                                    renderItem(item) {
                                        return `<div class="report-search-user-block">
                                                    <div class="report-search-user-avatar">
                                                        <img src="`+item.image+`">
                                                    </div>
                                                    <div>
                                                            <p class="mention-item-title">`+ item.value +`</p>
                                                            <p class="mention-item-nationality">`+ item.nationality +`</p>
                                                    </div>
                                                </div>`
                                  },
                                }

                        },
                        theme: 'snow',
                        placeholder: 'Please enter your content here , it will be appearing just like this!',
            });
            // Adding toolbar links titles
            $(`#${text_div_id} button.ql-bold`).attr('title', 'Bold');
            $(`#${text_div_id} button.ql-italic`).attr('title', 'Italic');
            $(`#${text_div_id} button.ql-italic`).attr('title', 'Italic');
            $(`#${text_div_id} button.ql-underline`).attr('title', 'Underline');
            $(`#${text_div_id} button.ql-strike`).attr('title', 'Strike');
            $(`#${text_div_id} button.ql-blockquote`).attr('title', 'Blockquote');
            $(`#${text_div_id} button.ql-header`).attr('title', 'Header');
            $(`#${text_div_id} span.ql-header`).attr('title', 'Header style');
            $(`#${text_div_id} button.ql-list[value=ordered]`).attr('title', 'Ordered List');
            $(`#${text_div_id} button.ql-list[value=bullet]`).attr('title', 'Bullet List');
            $(`#${text_div_id} button.ql-link`).attr('title', 'Link');
            $(`#${text_div_id} button.ql-script[value=sub]`).attr('title', 'Subscript');
            $(`#${text_div_id} button.ql-script[value=super]`).attr('title', 'Superscript');
            $(`#${text_div_id} button.ql-indent`).attr('title', 'Indent');
            $(`#${text_div_id} button.ql-direction`).attr('title', 'Direction');
            $(`#${text_div_id} button.ql-clean`).attr('title', 'Clear Formatting');
            $(`#${text_div_id} span.ql-color`).attr('title', 'Text color');
            $(`#${text_div_id} span.ql-background`).attr('title', 'Background color');
            $(`#${text_div_id} span.ql-font`).attr('title', 'Font family');
            $(`#${text_div_id} span.ql-align`).attr('title', 'Text align');


        if(isMobileDevice()){
            quill_list.push(quill)
            quill.blur();

            if(!$('#sortable').hasClass('for-draggable')){
                scrollDown($('#'+text_div_id).position(), before_load);
                before_load = true;
            }


        }else{
            quill.focus();
        }


        num_texts = thisNum;
            reloadSortable();

            $('#' + text_div_id + ' #text')[0].firstChild.onkeyup = () => {

                if(removeHtmlTags($('#' + text_div_id + ' #text .ql-editor').html()) != ''){
                    $('#' + text_div_id + ' #text').closest('.content').removeClass('error')
                    $('#' + text_div_id + ' #text').closest('.content').find('.items-error').remove()
                    if(changeTimer !== false) clearTimeout(changeTimer);
                    changeTimer = setTimeout(function(){
                        $("#createReportStep2").submit();
                        changeTimer = false;
                    },400);
                }
             }

            $(document).on('click', '#' + text_div_id + ' .ql-toolbar .ql-formats button, #' + text_div_id + ' .ql-toolbar .ql-formats .ql-picker-item', function() {
                if(removeHtmlTags($('#' + text_div_id + ' #text .ql-editor').html()) != ''){
                    if(changeTimer !== false) clearTimeout(changeTimer);
                        changeTimer = setTimeout(function(){
                            $("#createReportStep2").submit();
                            changeTimer = false;
                    },300);
                }

            });
          
             
        $('#sortable').removeClass('for-draggable')
       var droped_block = $('#sortable').find('.droppable-here')
       droped_block.removeClass('droppable-here')
       check_el = 0;
    });
    
    function removeText(id, num){
         if(checkconnection()){
            return;
        }
        document.getElementById(id).remove();
         $(".text-droppable-" + num).remove()
        $("#createReportStep2").submit();
    }

    $(document).on('drop', '.ql-container', function(e){
        e.preventDefault();
        return false;
    });
    
    
//Add report image
   var num_images = $('.add_image_cont').length; 
    $('#image_cont').click(function () {
        
        if(checkconnection()){
            return;
        }
    
        var image_div_id = 'add_image_cont';
        var thisNum = parseInt(num_images)+1;
        image_div_id = 'add_image_cont'+(parseInt(thisNum));
        if(!$('#sortable').hasClass('for-draggable')){
            $('#sortable').append(imageHtmlContent(image_div_id, thisNum));
        }else{
            $('.droppable-here').parent().after(imageHtmlContent(image_div_id, thisNum));
        }
        
           $(".image-droppable-" +thisNum).droppable({
                drop: function(event, ui) {
                    image_element_flag = false;
                    $(this).addClass('droppable-here')
                    $('#sortable').addClass('for-draggable')
                    var dragged_id = ui.draggable.attr('dataId')
                    if(dragged_id == 'video_dropdown'){
                        $(ui.draggable).clone().appendTo(this);
                        $(this).find('#rep_video_cont').click()
                    }else{
                        $('#' + dragged_id).click()
                    }
                }
            });

        
        num_images =  thisNum;             
            if(image_element_flag === true)
            {
               
                document.getElementById('reportImage'+thisNum).files = drag_files;
                $('#reportImage'+thisNum).change();
                image_element_flag = false;
                drag_files = null;

            }
            else
            {
                if(image_file_triger == false){
                    $('#reportImage'+thisNum).click();

                    document.body.onfocus = () => {
                        setTimeout(_=>{
                            let file_input = document.getElementById('reportImage'+thisNum);
                            if (!file_input.value){
                                $("#"+image_div_id).remove();
                                $(".image-droppable-" +thisNum).remove()
                            } 
                            document.body.onfocus = null
                        }, 500)
                    }
                }else{
                    image_file_triger = false;
                }
            }

            reloadSortable();
            
            $('#sortable').removeClass('for-draggable')
            var droped_block = $('#sortable').find('.droppable-here')
            droped_block.removeClass('droppable-here')
            
    });
    
    function removeImage(id, num){
        if(checkconnection()){
            return;
        }
        document.getElementById(id).remove();
        $(".image-droppable-" +num).remove()
        $("#createReportStep2").submit();
    }
    
    // save draft after image info chnage
    $("body").on("keyup", ".image-info", function(){
            var c_number = $(this).attr('data-typenum')
            var c_id = $(this).attr('data-typeid')
            var info_val = $(this).val()
            if(changeTimer !== false) clearTimeout(changeTimer);
            changeTimer = setTimeout(function(){
                if( $('#link_'+c_id).val() != ''){
                    var file_full_name = $('#rep_image'+c_number).val()+'-:'+ info_val +'-:'+ $('#link_'+c_id).val(); 
                 }else{
                    var file_full_name = $('#rep_image'+c_number).val()+'-:' +info_val;  
                 }
                 $('#image_hidden_'+c_number).val(JSON.stringify({'photo':file_full_name}))
                $("#createReportStep2").submit();
                changeTimer = false;
            },500);
    });
    
    
    //check image text link
    $("body").on("click", ".image-link-checkbox.add-link-checkbox", function(){
        if($(this).prop('checked') == true){

           $(this).closest('.content').find('.report-item-hyperlink').removeClass('d-none');
           $(this).closest('.content').find('.report-item-hyperlink').prop('disabled', false);
        }else{
            var c_number = $(this).closest('.content').find('.report-item-hyperlink').attr('data-typenum')
            var c_id = $(this).closest('.content').find('.report-item-hyperlink').attr('data-typeid')
            if( $('#info_'+c_id).val() != ''){
                var file_full_name = $('#rep_image'+c_number).val()+'-:'+ $('#info_'+c_id).val();
            }else{
                var file_full_name = $('#rep_image'+c_number).val();
            }
            $('#image_hidden_'+c_number).val(JSON.stringify({'photo':file_full_name}))
            $("#createReportStep2").submit();
              
            $(this).closest('.content').find('.report-item-hyperlink').val('').addClass('d-none');
            $(this).closest('.content').find('.report-item-hyperlink').prop('disabled', true);
            $(this).closest('.content').find('.url-error').addClass('d-none');
        }    
    });


    //hide image description error
    $(document).on("keyup", ".image-info", function(){
        if($(this).val() == '' && $(this).closest('.content').find('.video-hyperlink.report-item-hyperlink').val() !=''){
            check_el = 0;
            $(this).parent().find('.url-error').html('Please insert description ...')
            $(this).parent().find('.url-error').removeClass('d-none')
        }else{
            $(this).closest('.content').find('.url-error').html('');
            $(this).closest('.content').find('.url-error').addClass('d-none');
        }
    })
    
    //add link on image desc
    $("body").on("keyup", ".image-hyperlink.report-item-hyperlink", function(){
        if($(this).val().length > 0){
            check_el = 0;
            if(urlExists($(this).val())) {
                $(this).parent().find('.url-error').addClass('d-none');
                var c_number = $(this).attr('data-typenum')
                var c_id = $(this).attr('data-typeid')
                var link_val = $(this).val();
                if ($('#info_' + c_id).val() != '') {
                    if (changeTimer !== false) clearTimeout(changeTimer);
                    changeTimer = setTimeout(function () {

                        var file_full_name = $('#rep_image' + c_number).val() + '-:' + $('#info_' + c_id).val() + '-:' + link_val;
                        $('#image_hidden_' + c_number).val(JSON.stringify({'photo': file_full_name}))
                        $("#createReportStep2").submit();

                        changeTimer = false;
                    }, 500);
                }else{
                    $(this).parent().find('.url-error').html('Please insert description ...')
                    $(this).parent().find('.url-error').removeClass('d-none')
                }
            }else{
                $(this).parent().find('.url-error').html('Please insert a valid URL...')
                $(this).parent().find('.url-error').removeClass('d-none')
            }
        }else{
            $(this).parent().find('.url-error').addClass('d-none');
        }
    });
    
    
    //check local video text link 
    $("body").on("click", ".video-link-checkbox.add-link-checkbox", function(){
        if($(this).prop('checked') == true){

           $(this).closest('.content').find('.report-item-hyperlink').removeClass('d-none');
           $(this).closest('.content').find('.report-item-hyperlink').prop('disabled', false);
        }else{
            var c_number = $(this).closest('.content').find('.report-item-hyperlink').attr('data-typenum')
            var c_id = $(this).closest('.content').find('.report-item-hyperlink').attr('data-typeid')
            if( $('#info_'+c_id).val() != ''){
                var file_full_name = $('#rep_video'+c_number).val()+'-:'+ $('#info_'+c_id).val();
            }else{
                var file_full_name = $('#rep_video'+c_number).val();
            }
            $('#local_video_hidden_'+c_number).val(JSON.stringify({'local_video':file_full_name}))
            $("#createReportStep2").submit();
              
            $(this).closest('.content').find('.report-item-hyperlink').val('').addClass('d-none');
            $(this).closest('.content').find('.report-item-hyperlink').prop('disabled', true);
            $(this).closest('.content').find('.url-error').addClass('d-none');
        }    
    });

    //hide local video  description error
    $(document).on("keyup", ".video-info", function(){
        if($(this).val() == '' && $(this).closest('.content').find('.video-hyperlink.report-item-hyperlink').val() !=''){
            check_el = 0;
            $(this).parent().find('.url-error').html('Please insert description ...')
            $(this).parent().find('.url-error').removeClass('d-none')
        }else{
            $(this).closest('.content').find('.url-error').html('');
            $(this).closest('.content').find('.url-error').addClass('d-none');
        }
    })
    
    //add link on local video desc
    $("body").on("keyup", ".video-hyperlink.report-item-hyperlink", function(){
        if($(this).val().length > 0){
            check_el = 0;
            if(urlExists($(this).val())){
               $(this).parent().find('.url-error').addClass('d-none');
               var c_number = $(this).attr('data-typenum')
               var c_id = $(this).attr('data-typeid')
               var link_val = $(this).val();
                if( $('#info_'+c_id).val() != ''){
                    if(changeTimer !== false) clearTimeout(changeTimer);
                    changeTimer = setTimeout(function(){
                            var file_full_name = $('#rep_video'+c_number).val()+'-:'+ $('#info_'+c_id).val()+'-:'+ link_val;
                            $('#local_video_hidden_'+c_number).val(JSON.stringify({'local_video':file_full_name}))
                            $("#createReportStep2").submit();

                        changeTimer = false;
                    },500);
                }else{
                    $(this).parent().find('.url-error').html('Please insert description ...')
                    $(this).parent().find('.url-error').removeClass('d-none')
                }
            }else{
                $(this).parent().find('.url-error').html('Please insert a valid URL...')
                $(this).parent().find('.url-error').removeClass('d-none')
            }
        }else{
            $(this).parent().find('.url-error').addClass('d-none');
        }
    });
    
    //Image validation and upload
    $(document).on('change', '.reportImage', function(e) {

        if(checkconnection()){
           return;
        }

     var match = ["image/jpeg", "image/png", "image/jpg", "image/webp"];
     var thisNum = $(this).attr('data-id');
     if(!this.files.length){

         $("#add_image_cont" + thisNum).remove();
         $(".image-droppable-" +thisNum).remove()
         return;
     }
     $("."+ thisNum +"-loader").removeClass('d-none')
     var files = this.files;
     for( var i = 0; i < files.length; i++){
         var imagefile = files[i].type;
         if( i != 0 )
         {
             image_file_triger = true;
             $('#image_cont').click();

         }

         if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]) || (imagefile == match[3])))
         {
             $("#message2").show();
             $("#message2").html("<p style='color:red'>Please select a valid Image File</p>");
             $("#add_image_cont" + (parseInt(thisNum) + i)).remove();
             $(".image-droppable-" +(parseInt(thisNum) + i)).remove()

             return false;
         } else
         {
             var file = files[i];
             
            if(file.size > 10485760){
                 $("#message2").show();
                $("#message2").html("<p style='color:red'>The maximum size of image 10MB.</p>");
                return false;
            }else{
                $("#message2").html("");
            }
             
             disableButtons();

             uploadLocalImage(thisNum, files, $('#step2_report_id').val())
             var a = function (thisNum, i, file){

             var output = $('#add_image_src' + (parseInt(thisNum) + i))[0];

                 output.src = URL.createObjectURL(e.target.files[0]);
                 output.onload = function() {
                     URL.revokeObjectURL(output.src) // free memory
                 }



                 $('#add_image_cont'+parseInt(thisNum)).removeClass('d-none');
                 scrollDown($('#add_image_cont'+parseInt(thisNum)).position(), true);

                 reloadSortable();

                 $("#message2").hide();

             };
             a(thisNum, i, file);
         }
     }
 });
 
 
 //Add Video url
     $(document).on('keydown', '.videoUrl', function(e){
       if(e.keyCode == 13 && !e.shiftKey){
           e.preventDefault();
       }
    });
    
    $(document).on('keyup', '.videoUrl', function(e){
        check_video_link = true;
        if(e.keyCode == 13 && !e.shiftKey){
            var val = $(this).val();

            if(parseVideo(val) != '') {
              check_draft = 1;
              $(this).blur();
            }
       }
    });
    
    $(document).on('blur', '.videoUrl', function(e) {
        if(checkconnection()){
            return;
        }

        var urlValue = $(this).val();
        var thisNum = $(this).attr('data-id');

        if(parseVideo(urlValue) != '')
        {
            var videoUrl = createVideoUrl(urlValue);
            $('#videoID'+thisNum).val(videoUrl);

            $('#video_iframe'+thisNum).attr('src', videoUrl);
            $('.video_embed'+thisNum).removeClass('d-none');
            $('.video_embed'+thisNum).show('slow');
            check_draft = 1;
            if(check_video_link){
                $('#video_url_hidden_'+thisNum).val(JSON.stringify({'video': videoUrl}))
                $("#createReportStep2").submit();
            }

             $(this).closest('.input-block').find('.items-error').remove()
             $(this).closest('.input-block').removeClass('.error')
        }else{
            if( $(this).closest('.input-block').find('.items-error').length == 0){
                $(this).closest('.input-block').append('<p class="items-error">Please insert a valid video URL from Youtube, Dailymotion or Vimeo...</p>')
                $(this).closest('.input-block').addClass('.error')
            }
            $(this).val('')
        }
    });


    //Parse video url
    function parseVideo (url) {

     url.match(/(http:|https:|)\/\/(player.|www.|m.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|dailymotion.com)\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+|\?\S+)?/);

        if (RegExp.$3.indexOf('youtu') > -1) {
            var timeing = '';
            if(typeof RegExp.$7 !== "undefined" && RegExp.$7.charAt(0) == '?'){
                timeing = RegExp.$7
            }

            return {
                type: 'youtube',
                id: RegExp.$6 + timeing
            };
        } else if (RegExp.$3.indexOf('vimeo') > -1) {
            return {
                type: 'vimeo',
                id: RegExp.$6
            };
        }else if (RegExp.$3.indexOf('dailym') > -1) {
            return {
                type: 'dailymotion',
                id: RegExp.$6
            };
        }else{
            return '';
        }
    }


    function createVideoUrl (url) {

        var video_obj = parseVideo(url);
        if (video_obj.type == 'youtube') {
            var videoID = video_obj.id;
            videoID = videoID.replace('?t=', '?start=')
            var videoUrl = 'https://www.youtube.com/embed/'+videoID;

        } else if (video_obj.type == 'vimeo') {
            var videoID = video_obj.id;

            var videoUrl = 'https://player.vimeo.com/video/'+videoID +'?portrait=0';

        }else if (video_obj.type == 'dailymotion') {
            var n = url.split("/video/");
            var videoID = n[1].split("/");

            var videoUrl = 'https://www.dailymotion.com/embed/video/'+videoID[0]+'/';
        }

        return videoUrl;
    }


    var num_videos = $('.add_video_cont').length;
     
    $(document).on('click', '#video_cont', function () {
        
        if(checkconnection()){
            return;
        }
      
        var video_div_id = 'add_video_cont';
        var thisNum = parseInt(num_videos)+1;
        video_div_id = 'add_video_cont'+(parseInt(thisNum));

        if(!$('#sortable').hasClass('for-draggable')){
            $('#sortable').append(videoHtmlContent(video_div_id, thisNum));
        }else{
             before_load = false;
            $('.droppable-here').parent().after(videoHtmlContent(video_div_id, thisNum));
            $(this).closest('.video-draggable').remove()
        }
        
        $(".videos-droppable-" +thisNum).droppable({
             drop: function(event, ui) {
                 $(this).addClass('droppable-here')
                 $('#sortable').addClass('for-draggable')
                 var dragged_id = ui.draggable.attr('dataId')
                if(dragged_id == 'video_dropdown'){
                    $(ui.draggable).clone().appendTo(this);
                    $(this).find('#rep_video_cont').click()
                }else{
                    $('#' + dragged_id).click()
                }
             }
         });
         
        num_videos = thisNum;
        
        reloadSortable();
        $('#sortable').removeClass('for-draggable')
        var droped_block = $('#sortable').find('.droppable-here')
        droped_block.removeClass('droppable-here')
        check_el = 0;    
        scrollDown($('#'+video_div_id).position(), before_load);
        before_load = true;
        
    });
    
    function removeVideoContent(video_div_id, num){
          if(checkconnection()){
            return;
        }
        
        video_div_id.remove();
        $('.video_embed'+num).remove();
        $(".videos-droppable-" +num).remove();
        $("#createReportStep2").submit();
    }
    
    
    
//add Video from local
    var num_local_video = $('.add_local_video_cont').length;
    $(document).on('click', '#local_video_cont', function () {
        
        if(checkconnection()){
            return;
        }
        
        var local_video_div_id = 'add_local_video_cont';
        var thisNum = parseInt(num_local_video)+1;
        local_video_div_id = 'add_local_video_cont'+(parseInt(thisNum));
        
        
        if(!$('#sortable').hasClass('for-draggable')){
            $('#sortable').append(localVideoHtmlContent(local_video_div_id, thisNum));
        }else{
            $('.droppable-here').parent().after(localVideoHtmlContent(local_video_div_id, thisNum));
            $(this).closest('.video-draggable').remove()
        }
        
           $(".l-video-droppable-" +thisNum).droppable({
                drop: function(event, ui) {
                    $(this).addClass('droppable-here')
                     video_element_flag = false;
                    $('#sortable').addClass('for-draggable')
                    var dragged_id = ui.draggable.attr('dataId')
                    if(dragged_id == 'video_dropdown'){
                        $(ui.draggable).clone().appendTo(this);
                        $(this).find('#rep_video_cont').click()
                    }else{
                        $('#' + dragged_id).click()
                    }
                }
            });
            
            
        num_local_video =  thisNum;             
            if(video_element_flag === true)
            {
                document.getElementById('reportLocalVideo'+thisNum).files = drag_video_files;
                $('#reportLocalVideo'+thisNum).change();
                video_element_flag = false;
                drag_video_files = null;

            }
            else
            {
                if(video_file_triger == false){
                    $('#reportLocalVideo'+thisNum).click();

                    document.body.onfocus = () => {
                        setTimeout(_=>{
                            let file_input = document.getElementById('reportLocalVideo'+thisNum);
                            if (!file_input.value){
                                $("#"+local_video_div_id).remove();
                                $(".l-video-droppable-" +thisNum).remove()
                            }
                            document.body.onfocus = null
                        }, 500)
                    }
                }else{
                    video_file_triger = false;
                }
            }

            reloadSortable();
            
            $('#sortable').removeClass('for-draggable')
            var droped_block = $('#sortable').find('.droppable-here')
            droped_block.removeClass('droppable-here')
    });
    
    //remove Local video
    function removeLocalVideo(id, num){
        if(checkconnection()){
            return;
        }
        document.getElementById(id).remove();
        $(".l-video-droppable-" +num).remove()
        $("#createReportStep2").submit();
    }
    
// save draft after video info chnage
    $("body").on("keydown", ".video-info", function(){
            if(changeTimer !== false) clearTimeout(changeTimer);
            changeTimer = setTimeout(function(){
                $("#createReportStep2").submit();
                changeTimer = false;
            },500);
    });
 
 
    // Preview local video after upload
    $(document).on('change', '.reportLocalVideo', function(e) {
        if(checkconnection()){
            return;
        }
        var match = ["video/mp4", "video/mpg", "video/avi", "video/m4v"];
        var thisNum = $(this).attr('data-id');
        if(!this.files.length){

            $("#add_local_video_cont" + thisNum).remove();
            $(".l-video-droppable-" +thisNum).remove()
            return;
        }
        var files = this.files;
        $("."+ thisNum +"-video-loader").removeClass('d-none')
        for( var i = 0; i < files.length; i++){
            var imagefile = files[i].type;
            if( i != 0 )
            {
                video_file_triger = true;
                $('#local_video_cont').click();

            }

            if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]) || (imagefile == match[3])))
            {
                $("#message2").show();
                $("#message2").html("<p style='color:red'>Please insert a valid video file...</p>");
                $("#add_local_video_cont" + (parseInt(thisNum) + i)).remove();
                $(".l-video-droppable-" +(parseInt(thisNum) + i)).remove()

                return false;
            } else
            {
                var file = files[i];
                
                if(file.size > 26214400){
                    $("#message2").show();
                    $("#message2").html("<p style='color:red'>The maximum size of video 25MB.</p>");
                    return;
                }else{
                    $("#message2").html("");
                }
                 disableButtons();

                uploadLocalVideo(thisNum, files, $('#step2_report_id').val())
                var a = function (thisNum, i, file){

                $('#add_video_src' + (parseInt(thisNum) + i)).html('<video style="height:352px;" width="632" class="thumb" controls onplay="playLocalVideo()"><source src="" type="video/mp4"></video>');

                    var $source = $('#add_video_src' + (parseInt(thisNum) + i)).find('source');
                    $source[0].src = URL.createObjectURL(e.target.files[0]);
                    $source.parent()[0].load();

                    $('#add_local_video_cont'+parseInt(thisNum)).removeClass('d-none');
                    scrollDown($('#add_local_video_cont'+parseInt(thisNum)).position(), true);

                    reloadSortable();
                    $("#message2").hide();

                };
                a(thisNum, i, file);
            }
        }
    });


    //Play local video
    function playLocalVideo(){
        if(isMobileDevice()) {
            document.activeElement.blur();
            $("input").blur();

            if(quill_list.length > 0){
                $.each(quill_list, function( index, value ) {
                    value.blur();
                });
            }
        }
    }
    
 
 //Add trip plan
 $(document).on('click', '.insert-plan-input', function(){
 var plan_type = $(this).attr('data-type');
  // $('.report-plan-block').find('.private-error-block').hide()
  
 if(plan_type == 'private'){
    $(this).closest('.tp-main-block').find('.private-error-block').show()
    $('.closePlansModal').prop('disabled', true)
 }else{
     $('.closePlansModal').prop('disabled', false)
 }
 
 })
 
  
    $('.closePlansModal').click(function () {
    
    if(checkconnection()){
        return;
    }
    
    var num_plans = $('.add_plan_cont').length;
       var selected_plan = $("input[name='plan_id']:checked").val();
       var check_plan=true;
        before_load = true;

        if ($.inArray(selected_plan, selected_plan_ids) == -1) {
            selected_plan_ids.push(selected_plan)
        }
      

       $('.add_plan_cont').each(function(){
   
           if($(this).data('plan_id') == selected_plan){
               check_plan=false;
               scrollDown($('#'+$(this).attr('id')).position(),  before_load);

           }
       })
        if(check_plan){
           $.get("{{url('reports/ajaxGetPlanByID')}}", {plan_id: selected_plan, current_number:parseInt(num_plans)+1})
               .done(function (data) {
                   
                   var plan_div_id = 'add_plan_cont';
                   var thisNum = parseInt(num_plans)+1;
                   plan_div_id = 'add_plan_cont'+(parseInt(thisNum));
                   var my_plan = JSON.parse(data);
                   
                    if(!$('#sortable').hasClass('for-draggable')){
                         $('#sortable').append(planHtmlContent(plan_div_id, thisNum, selected_plan, my_plan))
                    }else{
                        before_load = false;
                        $('.droppable-here').parent().after(planHtmlContent(plan_div_id, pNum, selected_plan, my_plan));
                    }
                    
                    
                    $(".plan-droppable-" +thisNum).droppable({
                        drop: function(event, ui) {
                            $(this).addClass('droppable-here')
                            $('#sortable').addClass('for-draggable')
                            var dragged_id = ui.draggable.attr('dataId')
                            if(dragged_id == 'video_dropdown'){
                                $(ui.draggable).clone().appendTo(this);
                                $(this).find('#rep_video_cont').click()
                            }else{
                                $('#' + dragged_id).click()
                            }
                        }
                    });
            
                   num_plans = thisNum;
                   reloadSortable();
                   scrollDown($('#'+plan_div_id).position(), before_load);
                   
                    $('#sortable').removeClass('for-draggable')
                    var droped_block = $('#sortable').find('.droppable-here')
                    droped_block.removeClass('droppable-here')
            
                   $("#createReportStep2").submit();
               });
       }

   });
   
//Remove trip plan
    function reomovePlan(id, num, plan_id){
        if(checkconnection()){
            return;
        }

        selected_plan_ids = $.grep(selected_plan_ids, function (value) {
            return value != plan_id;
        });

        id.remove();
        $(".plan-droppable-" +num).remove();
        $("#createReportStep2").submit();
    }
    
    
//Cancel trip plan
$(document).on('click', '.close-plan-modal',function () {
    $('#sortable').removeClass('for-draggable')
    var droped_block = $('#sortable').find('.droppable-here')
    droped_block.removeClass('droppable-here')
});
   
   
   //Search trip plan
    function searchTripPlan(){
        var input, filter;
        input = document.getElementById("searchInput");
        filter = input.value;
        
        if(filter.length != ''){
            plan_filter = filter
        }else{
             plan_filter = ''
        }
            
             setTimeout(function(){
               getSearchedPlan(filter)
            }, 500);
  
    }

//Add Users
 var num_peoples = $('.add_people_cont').length;
    $('#people_cont').click(function () {
        
        if(checkconnection()){
            return;
        }

        var people_div_id = 'add_people_cont';
        var thisNum = parseInt(num_peoples)+1;
        people_div_id = 'add_people_cont'+(parseInt(thisNum));
        
            if(!$('#sortable').hasClass('for-draggable')){
                $('#sortable').append(peopleHtmlContent(people_div_id, thisNum))
            }else{
                before_load = false;
                $('.droppable-here').parent().after(peopleHtmlContent(people_div_id, thisNum));
            }


            $(".people-droppable-" +thisNum).droppable({
                drop: function(event, ui) {
                    $(this).addClass('droppable-here')
                    $('#sortable').addClass('for-draggable')
                    var dragged_id = ui.draggable.attr('dataId')
                    if(dragged_id == 'video_dropdown'){
                        $(ui.draggable).clone().appendTo(this);
                        $(this).find('#rep_video_cont').click()
                    }else{
                        $('#' + dragged_id).click()
                    }
                }
            });
                    
    
            $('.report_user'+thisNum).select2({
                minimumInputLength: 3,
                placeholder: 'Type a name...',
                ajax: {
                    url: "{{url('reports/ajaxSearchUser')}}",
                    dataType: 'json',
                    processResults: function (data) {
                        return {
                                results: $.map(data, function(obj) {
                                    return { id: obj.id, text: obj.text, image:obj.image, nationality:obj.nationality, query:obj.query};
                                })
                            };
                        }
                },
                templateResult: function (response) {
                    if(response){
                            var markup = '<div class="search-country-block">';
                            if(response.image){
                                markup +='<div class="search-country-imag"><img src="' + response.image + '" /></div>';
                            }
                             markup +='<div class="span10">' +
                                '<div class="search-country-info">' +
                                '<div class="search-country-text">' + markLocetionMatch(response.text, response.query) + '</div>';

                                if(response.nationality && response.nationality !== ''){
                                       markup += '<div class="search-country-type">From '+ response.nationality +'</div>';
                                }
                                markup +='</div></div>';

                          return markup;
                    }
                },
                dropdownParent: $('.add_people-'+thisNum),
                containerCssClass: "select2-input",
                dropdownCssClass: "bigdrop",
                escapeMarkup: function (m) { return m; }
            });
            
            $('.report_user'+thisNum).on('select2:select', function (e) {
                var data = e.params.data;
                $(this).closest('div').removeClass('error')
                $(this).closest('div').find('.items-error').remove()
                addPeopleRow(data.id, thisNum);
                
                $('#people_hidden_'+thisNum).val(JSON.stringify({'user': $(this).val().join(',')}))
                $("#createReportStep2").submit();
            });

            $('.report_user'+thisNum).on('select2:unselect', function (e) {
                var selected_id = e.params.data.id;
                $(this).find("option[value='"+ selected_id +"']").remove();
                
                $('#people_hidden_'+thisNum).val(JSON.stringify({'user': $(this).val().join(',')}))
                $('.people-'+selected_id).remove();
                $("#createReportStep2").submit();
            }).trigger('change');


            num_peoples = thisNum;
            check_el = 0;
            reloadSortable();
            
            $('#sortable').removeClass('for-draggable')
            var droped_block = $('#sortable').find('.droppable-here')
            droped_block.removeClass('droppable-here')

            scrollDown($('#'+people_div_id).position(), before_load);
            before_load = true;
    });
    
    function removeUsers(id, num){
        if(checkconnection()){
            return;
        }
        document.getElementById(id).remove();
        $(".people-droppable-" +num).remove();
        $("#createReportStep2").submit();
    }
    
//Add people rows
    function addPeopleRow(id, num){
        if(id){
            $.ajax({
                method: "GET",
                url: "{{url('reports/ajaxGetUserInfo')}}?id="+id,
                data: {'data':'data'},
                async: false
            }).done(function(data){
                var result = JSON.parse(data);
                 var cover_photo = '';
                 var media = '';

                if(result.cover_photo){
                    cover_photo = '<img class="back" src="'+ result.cover_photo +'" alt="image" style="height:183px;">';
                }

                for(var i = 0; i < 3; i++){
                    if(result.media[i]) {
                        media+='<li><img src="'+ result.media[i] +'" alt="image" style="width:90px;height:90px;"></li>';
                    } else {
                        media+='<li><img src="http://s3.amazonaws.com/travooo-images2/placeholders/photo.png" alt="image" style="width:90px;height:90px;"></li>';
                    }
                };
                $('#add_people_row_'+num).append(`<div class="mb-3 people-`+ result.id +`" style="width:100%; display:flex;margin-left:15px;">
                                                <div class="content col-md-9 plr-0 max-w-632">
                                                    <div class="travlog-image-card" style="height:185px;">
                                                        <div class="card-part img-wrap" style="padding:0;">`+ cover_photo +`
                                                            <div class="avatar-img"><img src="`+ result.profile_picture +`" alt="ava" class="ava" style="width:70px;height:70px;"></div></div>
                                                <div class="card-part info-card">
                                                <div class="info-top"><div class="info-txt">
                                                    <div class="name">
                                                        <a href="{{url('profile')}}/`+ result.id +`">` + result.text + `</a>
                                                    </div>
                                                    <div class="country">` + result.country + `</div>
                                                </div>
                                                <div class="btn-wrap">
                                                `+result.follow+`
                                                </div></div>
                                                <ul class="preview-list">
                                                    `+ media +`
                                                </ul>
                                            </div></div></div></div>`);
                $('#add_people_row_'+num).show();
            })

        }
    }
    
//Add existing people row
    function addPeopleExistingRow(user, num){
        if(user){
            var cover_photo = '';
            var media = '';

           if(user.cover_photo){
               cover_photo = '<img class="back" src="'+ user.cover_photo +'" alt="image" style="height:183px;">';
           }

           if(user.media.length > 0){
               for(var i = 0; i<user.media.length;i++){
                  media+='<li><img src="'+ user.media[i] +'" alt="image" style="width:90px;height:90px;"></li>';
               };
           }
           $('#add_people_row_'+num).append(`<div class="mb-3 people-`+ user.id +`" style="width:100%; display:flex;margin-left:15px;">
                                           <div class="content col-md-9 plr-0 max-w-632">
                                               <div class="travlog-image-card" style="height:185px;">
                                                   <div class="card-part img-wrap" style="padding:0;">`+ cover_photo +`
                                                       <div class="avatar-img"><img src="`+ user.profile_picture +`" alt="ava" class="ava" style="width:70px;height:70px;"></div></div>
                                           <div class="card-part info-card">
                                           <div class="info-top"><div class="info-txt">
                                               <div class="name"><a href="{{url('profile')}}/`+ user.id +`" target="_blank">` + user.text + `</a></div>
                                               <div class="country">` + user.country + `</div>
                                           </div>
                                           <div class="btn-wrap">
                                           `+user.follow+`
                                           </div></div>
                                           <ul class="preview-list">
                                               `+ media +`
                                           </ul>
                                       </div></div></div></div>`);
            $('#add_people_row_'+num).show();
        }
    }

    //Follow user feature
    $(document).on('click', '.follow-button', function(){
        var _this = $(this);
        var type = $(this).attr('data-type');
        var user_id = $(this).attr('data-user-id');
        var route_follow = $(this).attr('data-route-follow');
        var route_unfollow = $(this).attr('data-route-unfollow');
        var fwhere = 'report';
        var url;
        if(type == 'follow'){
            url = route_follow;
        }else{
            url = route_unfollow;
        }

        $.ajax({
            method: "POST",
            url: url,
            data: {type: type, fwhere: fwhere}
        })
            .done(function (res) {
                if (res.success == true) {
                    if(type == 'follow'){
                        $(`.follow-button_${user_id}`).html("@lang('buttons.general.unfollow')")
                        $(`.follow-button_${user_id}`).attr('data-type', 'unfollow')

                        $(`.follow-button_${user_id}`).removeClass('btn-blue-follow')
                        $(`.follow-button_${user_id}`).addClass('btn-light-grey')
                        $('.cover-follow').addClass('btn-white-follow')

                    }else{
                        $(`.follow-button_${user_id}`).html("@lang('buttons.general.follow')")
                        $(`.follow-button_${user_id}`).attr('data-type', 'follow')
                        $(`.follow-button_${user_id}`).addClass('btn-blue-follow')

                        $(`.follow-button_${user_id}`).removeClass('btn-white-follow')
                        $(`.follow-button_${user_id}`).removeClass('btn-light-grey')
                    }
                }
            });
    })
   
   
//Add report maps   
var num_maps = $('.add_map_cont').length;

    $('#map_cont').click(function () {
    
        if(checkconnection()){
            return;
        }
       
        var map_div_id = 'add_map_cont';
        var thisNum = parseInt(num_maps)+1;
        var lat = $('#dest_info').attr('data-lat')
        var lng = $('#dest_info').attr('data-lng')
        map_div_id = 'add_map_cont'+(parseInt(thisNum));

        if(!$('#sortable').hasClass('for-draggable')){
               $('#sortable').append(mapHtmlContent(map_div_id, thisNum))
           }else{
               before_load = false;
               $('.droppable-here').parent().after(mapHtmlContent(map_div_id, thisNum));
           }


           $(".map-droppable-" +thisNum).droppable({
               drop: function(event, ui) {
                   $(this).addClass('droppable-here')
                   $('#sortable').addClass('for-draggable')
                   var dragged_id = ui.draggable.attr('dataId')
                    if(dragged_id == 'video_dropdown'){
                        $(ui.draggable).clone().appendTo(this);
                        $(this).find('#rep_video_cont').click()
                    }else{
                        $('#' + dragged_id).click()
                    }
               }
           });

        num_maps = thisNum;

        $('.report_map-'+ map_div_id).keyup(delay(function (e) {
            let value = $(e.target).val();
            getDestinationSearch(value, $(this), '.search-map-'+map_div_id, 'dest-searched-item', 'map');
        }, 500));


        $(document).on('click', '.search-map-'+map_div_id + ' .dest-searched-item', function(){
            var lat = $(this).data('lat')
            var lng = $(this).data('lng')
            var zoom = $(this).data('zoom')
            var id = $(this).data('select-id')
            var text = $(this).data('select-text')

            if($(this).hasClass('place')){
                getPlaceCountryInfo(thisNum, id)
            }else {
                $('#map_hidden_'+thisNum).val(JSON.stringify({'map':id}))
                addMapCountryInfo('mapCountryInfo'+ thisNum, id);
            }

            $(this).closest('div').removeClass('error')
            $(this).closest('div').find('.items-error').remove()
            insertMap('added_plan_map'+thisNum, lat, lng, zoom, text)
            $('.search-map-'+map_div_id).find('.dest-search-result-block').html('')
            $('.search-map-'+map_div_id).find('.dest-search-result-block').removeClass('open')
            $('.report_map-'+ map_div_id).val(text)
            $("#createReportStep2").submit();

        })

        reloadSortable();
        
        $('#sortable').removeClass('for-draggable')
        var droped_block = $('#sortable').find('.droppable-here')
        droped_block.removeClass('droppable-here')
        
        scrollDown($('#'+map_div_id).position(), before_load);
        before_load = true;
        check_el = 0;

    });
    
    function removeMap(id, num){
        if(checkconnection()){
            return;
        }
        document.getElementById(id).remove();
        $(".map-droppable-" +num).remove();
        $("#createReportStep2").submit();
    }
    
    //map place country info
    function addMapCountryInfo(id, arr){
        var info_arr = arr.split("-:");
        var pop_info = '';

        if(info_arr.length > 4){
            if(info_arr[7]){
                var follow_btn_type =  $('.follow-info-'+info_arr[7]).attr('data-fol-type');
                var follow_btn = `<button class="btn btn-light-grey btn-bordered follow-country-btn" data-fol-type="`+ ((follow_btn_type == 'follow' || typeof follow_btn_type ==="undefined" )?'follow':'unfollow') +`" data-country-id="`+ info_arr[7] +`">`+ ((follow_btn_type == 'follow' || typeof follow_btn_type ==="undefined")?'follow':'unfollow') +`</button>`;
            }else{
                 var follow_btn = '<button class="btn btn-light-grey btn-bordered">Follow</button>';
            }

            if(info_arr[6] != ''){
                pop_info = ` <div class="country-population">
                            <div class="user-icon">
                                <i class="trav-popularity-icon"></i>
                            </div>
                            <div class="pop-info">
                                <p class="pop-count">`+ info_arr[6] +`</p>
                                <p class="pop-title">Population</p>
                            </div>
                        </div>`
            }
             var htmlStr = `
                    <div class="country-info">
                        <div class="country-name-block">
                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/`+ info_arr[5].toLowerCase() +`.png"
                                           alt="flag">
                            <div>
                                <p class="map-country-title">`+ info_arr[4] +`</p>
                                <p class="map-country-desc"></p>
                            </div>
                        </div>
                       `+ pop_info +`
                    </div>
                    <div class="country-follow">
                        `+ follow_btn +`
                    </div>`;

        $('#'+id).html(htmlStr)
    }
}

    var info_slider = '';
//open travooo info modal
    $('body').on('click', '#travooo_info_modal', function (e) {
        var report_id = $('#step2_report_id').val()
             $('.info-location-list').html('')
        $('#addTravoooInfoPopup').modal('show')

            $.ajax({
            method: "POST",
            url: "{{url('reports/ajaxGetLocationList')}}",
            data: {'id':report_id},
            async: false
                }).done(function(data){
                      var result = JSON.parse(data);
                        if(info_slider.lightSlider){
                            info_slider.destroy();
                        }
                        for(var j in result){
                            if( result[j].country_id == '')
                                var loc_type = 'country';
                            else
                                var loc_type = 'city';

                            $('.info-location-list').append('<div class="info-location-item '+ (j==0?'selected':'') +'" data-info_type="'+ loc_type +'" data-loc_id="'+ result[j].id +'" style="background:url(' + result[j].image + ') no-repeat center center #000;background-size: cover;">' +
                                                                    '<div class="info-location-wrap">'+
                                                                    '<div class="info-location-title">'+ result[j].title +'</div>'+
                                                            '</div></div>');
                        }



                            info_slider = $('.info-location-list').lightSlider({
                                autoWidth:true,
                                pager: false,
                                enableDrag: false,
                                // controls: true,
                                slideMargin: 20,
                                enableTouch: false,
                                prevHtml: '<i class="trav-angle-left"></i>',
                                nextHtml: '<i class="trav-angle-right"></i>',
                                addClass: 'rep-info-slider-block',
                                onSliderLoad: function (el) {
                                    $(el).parent().find('.lSPrev').addClass('s-arrow-disabled')
                                    el.css('width', '3000px')
                                },
                                onBeforeSlide: function (el) {
                                    var active_index = el.getCurrentSlideCount();
                                    if(active_index == 1){
                                        $(el).parent().find('.lSPrev').addClass('s-arrow-disabled')
                                    }else{
                                        $(el).parent().find('.lSPrev').removeClass('s-arrow-disabled')
                                    }

                                    if(active_index == el.parent().find('div.info-location-item').length - 4){
                                        $(el).parent().find('.lSNext').addClass('s-arrow-disabled')
                                    }else{
                                        $(el).parent().find('.lSNext').removeClass('s-arrow-disabled')
                                    }
                                },
                                responsive : [
                                    {
                                        breakpoint:767,
                                        settings: {
                                            slideMargin:9,
                                        }
                                    },
                                ],
                            })

                            if(result[0].country_id =='')
                                var type = 'country';
                            else
                                var type = 'city';

                            getLocationInfo(result[0].id, type)
                            $('.t-log-info-wrapper').attr('selectedid', result[0].id+'-'+type)

                })

        if(!isTouchDevice) {
            $('#addTravoooInfoPopup .info-tab-content').each(function() {
                $(this).outerHeight($('#addTravoooInfoPopup .info-tab-content.general-view').outerHeight())
            })
        }
    });
        
    //select location for travooo info
    $(document).on('click', '.info-location-item', function(){
            var loc_type = $(this).attr('data-info_type')
            var loc_id = $(this).attr('data-loc_id')

        $('.info-location-list').find('.info-location-item').removeClass('selected')
        $(this).addClass('selected')
        $('.t-log-info-wrapper').find('.travlog-info-block').removeClass('active')
        $('.t-log-info-wrapper').find('.t-info-add-icon').html('<img src="{{asset('assets2/image/icons/add-info.svg')}}">')

        getLocationInfo(loc_id, loc_type)
        $('.travooo-info-content').removeClass('d-none')
        $('.t-log-info-wrapper').attr('selectedid', loc_id + '-' + loc_type)
    })


    $('.t-log-info-wrapper .travlog-info-block').click(function (e) {
        if(!$(this).hasClass('active')){
        var infoVar = $(this).find(".t-title-content").html();
        var infoVal = $(this).children(".subtitle").html();
        var infoVarId = $(this).closest('.t-log-info-wrapper').attr('selectedid')
        var infoDest = $(this).find(".info-dest-name").html();

        if(!(infoVarId in trav_info_list)){
            trav_info_list[infoVarId] = [infoVar];
        }else{
            trav_info_list[infoVarId].push(infoVar);

        }



        $(this).addClass('active')
        addInfo(infoVar, infoVal, infoVarId, infoDest)

        var notificationPopup = $('#notificationPopup');
        notificationPopup.modal('show')

        setTimeout(function() {
            $('#addTravoooInfoPopup').modal('hide')
            $("#createReportStep2").submit();
        }, 100);

        setTimeout(function() {
            notificationPopup.modal('hide')
        }, 1000);
        
    }
    });

//Add travooo info
 var num_infos = $('.add_info_cont').length;
 
    function addInfo(infoVar, infoVal, infoVarId = '', infoDest = ''){
        
        if(checkconnection()){
            return;
        }
        
        var check_info=true;

        // $('.add_info_cont').each(function(){
        //     if($(this).data('info') == infoVar){
        //         check_info=false;
        //
        //     }
        // })

        if(check_info){
           
            var info_div_id = 'add_info_cont';
            var thisNum = parseInt(num_infos)+1;
            info_div_id = 'add_info_cont'+(parseInt(thisNum));
            
            if(!$('#sortable').hasClass('for-draggable')){
                $('#sortable').append(infoHtmlContent(info_div_id, thisNum, infoVar, infoVal, infoVarId, infoDest));
            }else{
                before_load = false;
                $('.droppable-here').parent().after(infoHtmlContent(info_div_id, thisNum, infoVar, infoVal, infoVarId, infoDest));
            }


            $(".info-droppable-" +thisNum).droppable({
                drop: function(event, ui) {
                    $(this).addClass('droppable-here')
                    $('#sortable').addClass('for-draggable')
                    var dragged_id = ui.draggable.attr('dataId')
                    if(dragged_id == 'video_dropdown'){
                        $(ui.draggable).clone().appendTo(this);
                        $(this).find('#rep_video_cont').click()
                    }else{
                        $('#' + dragged_id).click()
                    }
                }
            });
            
            num_infos = thisNum;
            reloadSortable();
            
            $('#sortable').removeClass('for-draggable')
            var droped_block = $('#sortable').find('.droppable-here')
            droped_block.removeClass('droppable-here')
            
            scrollDown($('#'+info_div_id).position(), before_load);
            before_load = true;
        }
    }

    function removeInfo(id, num, infoVarId){
        if(checkconnection()){
            return;
        }


        if(infoVarId != '' && (infoVarId in trav_info_list)){
            var removed_info = $('#' + id).find('.add_info_cont').data('info');
            trav_info_list[infoVarId] = $.grep(trav_info_list[infoVarId], function (value) {
                return value != removed_info;
            });
        }

        document.getElementById(id).remove()
        $(".info-droppable-" +num).remove()
        $("#createReportStep2").submit();
    }
    
    $(document).on('click', '.close-modal-info', function(){
        $('#sortable').removeClass('for-draggable')
        var droped_block = $('#sortable').find('.droppable-here')
        droped_block.removeClass('droppable-here')
    })
   
    function getLocationInfo(id, type){
        // console.log('trav_info_list', trav_info_list[id+'-'+type])
        if(typeof trav_info_list[id+'-'+type] !== 'undefined'){
            $.each(trav_info_list[id+'-'+type], function( index, sel_id ) {
                var replaced_id = sel_id.replace(/ /g, "-");

                $('.'+replaced_id).addClass('active')
                $('.'+replaced_id).find('.t-info-add-icon').html('')
            });
        }

       if (type == 'country') {
            $.get("{{url('reports/ajaxGetCountryByID')}}", {countries_id: id})
                .done(function (data) {
                    var info = JSON.parse(data);
                    appendLocationInfo(info)


                });
             } else if (type == 'city') {
                 $.get("{{url('reports/ajaxGetCityByID')}}", {cities_id: id})
                    .done(function (data) {
                    var info = JSON.parse(data);
                        appendLocationInfo(info)
                    });
             }
    }

    function appendLocationInfo(info){
        var transportation_html = '';

        $.each( info.transportation, function( key, value ) {
            transportation_html +='<div class="sub-list"><span>' + value +'</span></div>'
        });

        $('#info_nationality').html('<div class="sub-list"><span>' + info.nationality+'</span></div>');
        $('#info_languages').html(info.languages_spoken[1]);
        $('#info_speed').html(info.speed_limit[1]);
        $('#info_timings').html(info.timing[1]);
        $('#info_religions').html(info.religions[1]);
        $('#info_code').html('<div class="sub-list"><span>' + info.phone_code +'</span></div>');
        $('#info_currencies').html(info.currencies[1]);
        $('#info_metrics').html('<div class="sub-list"><span>' + info.metrics +'</span></div>');
        $('#info_working_days').html('<div class="sub-list"><span>' + info.working_days +'</span></div>');
        $('#info_transportation')
            .closest('.travlog-info-block')
            .find('.t-info-count').remove();
        $('#info_transportation').html(transportation_html)
            .closest('.travlog-info-block')
            .find('.title')
            .append(`<span class="t-info-count hidden-more-sm">(${info.transportation.length})</span>`);


        $('#info_emergency').html(info.emergency_numbers[1]);

        $('#info_cost_of_living').html('<div class="sub-list"><span>' + info.cost_of_living +'</span></div>');
        $('#info_crime_rate').html('<div class="sub-list"><span>' + info.crime_rate +'</span></div>');
        $('#info_quality_of_life').html('<div class="sub-list"><span>' + info.quality_of_life +'</span></div>');

        $('#info_holidays').html(info.holidays[1]);
        $('#info_internet').html('<div class="sub-list"><span>' + info.internet +'</span></div>');
        $('#info_sockets').html(info.sockets[1]);
        $('#info_population').html('<div class="sub-list"><span>' + info.population +'</span></div>');

        $('#dest_info').attr('data-lat', info.lat)
        $('#dest_info').attr('data-lng', info.lng)

        $('.travlog-info-block').find('.info-dest-name').html(info.location_name)

        new LocationInfoCount(info);
    }

    function LocationInfoCount(info) {
        info = info || {};
        this.items = [
            {
                $elem: $('#info_languages'),
                value: info.languages_spoken[0]
            },
            {
                $elem: $('#info_speed'),
                value: info.speed_limit[0]
            },
            {
                $elem: $('#info_timings'),
                value: info.timing[0]
            },
            {
                $elem: $('#info_religions'),
                value: info.religions[0]
            },
            {
                $elem: $('#info_currencies'),
                value: info.currencies[0]
            },
            {
                $elem: $('#info_emergency'),
                value: info.emergency_numbers[0]
            },
            {
                $elem: $('#info_holidays'),
                value: info.holidays[0]
            },
            {
                $elem: $('#info_sockets'),
                value: info.sockets[0]
            },
        ]
        this.init();
    }

    LocationInfoCount.prototype = Object.assign({}, {
        init: function() {
            new Promise((resolve, reject) => {
                this.deleteAll();
                resolve();
            }).then(() => {
                this.add();
            });
        },
        add: function () {
            for (const {$elem, value} of this.items) {
                var html = value ? this.getTemplate(value) : null;

                if ($elem.closest('.travlog-info-block').find('.t-info-count').length) return;

                if(html) {
                    $elem.closest('.travlog-info-block')
                        .find('.title')
                        .append(html);
                }
            }
        },
        deleteAll: function () {
            for (const {$elem, value} of this.items) {
                $elem.closest('.travlog-info-block')
                    .find('.t-info-count').remove();
            }
        },
        getTemplate: function (value) {
            return `<span class="t-info-count hidden-more-sm">(${value})</span>`
        }
    });
    
//Add places
var num_places = $('.add_place_cont').length;

    $('#place_cont').click(function () {
        
        if(checkconnection()){
            return;
        }
        
        var place_div_id = 'add_place_cont';
        var thisNum = parseInt(num_places)+1;
        place_div_id = 'add_place_cont'+(parseInt(thisNum));
        
        if(!$('#sortable').hasClass('for-draggable')){
            $('#sortable').append(placeHtmlContent(place_div_id, thisNum));
        }else{
            before_load = false;
            $('.droppable-here').parent().after(placeHtmlContent(place_div_id, thisNum));
        }


        $(".place-droppable-" +thisNum).droppable({
            drop: function(event, ui) {
                $(this).addClass('droppable-here')
                $('#sortable').addClass('for-draggable')
                var dragged_id = ui.draggable.attr('dataId')
                if(dragged_id == 'video_dropdown'){
                    $(ui.draggable).clone().appendTo(this);
                    $(this).find('#rep_video_cont').click()
                }else{
                    $('#' + dragged_id).click()
                }
            }
        });
                    
        
            $('#search_'+place_div_id).select2({
                minimumInputLength: 3,
                placeholder: 'Type to search a Places...',
                ajax: {
                    url: "{{url('reports/ajaxSearchPlace')}}",
                    dataType: 'json',
                    processResults: function (data) {
                        return {
                                results: $.map(data, function(obj) {
                                    return { id: obj.id, text: obj.title, place:obj };
                                })
                            };
                    }
                },

                templateResult: function (response) {
                if(response.place){
                     if(response.place.image!=='') {
                            imAGE = 'http://s3.amazonaws.com/travooo-images2/th180/'+response.place.image;
                        }
                        else {
                            imAGE = 'http://s3.amazonaws.com/travooo-images2/placeholders/place.png';
                        }

                        htML = `<div class="travlog-place-card places_result" data-id="`+response.id+`" data-title="`+response.text+`">
                            <div class="text-block">
                                                                <div class="title-card flex-row">
                                                                    <span class="tag">`+response.place.type+`</span>
                                                                    <div class="location report-place-location">
                                                                        <i class="trav-set-location-icon"></i>
                                                                        <span>`+ response.place.place_location +`</span>
                                                                    </div>
                                                                </div>
                                                                <div class="title-card">
                                                                    <a href="{{url_with_locale('place')}}/`+ response.id +`" class="place-link" target="_blank">`+markMatch(response.text, response.place.query)+`</a>
                                                                    <p class="report-place-address">`+response.place.address+`</p>
                                                                </div>
                                                            </div>
                                                            <div class="img-wrap repot-place-img">
                                                                <img src="`+imAGE+`" alt="image">
                                                            </div>
                                                            <div class="rate-block">
                                                                <div class="visited">`+response.place.visited_list+`</div>
                                                                <div class="rate-info">
                                                                    <div class="rate-inner report-rate-inner">
                                                                    <span class="label">`+response.place.rating+` <i class="trav-star-icon"></i></span>
                                                                    <p class="mb-0"><b>@lang('report.excellent')</b></p>
                                                                    <p class="mb-0">`+ response.place.reaview_count +` reviews</p>
                                                                </div>
                                                                </div>
                                                            </div>
                        </div>`;
                                    
                    return htML;
                }else{
                    return response.text;
                }
           },

                dropdownParent: $('#rep_place'+place_div_id),
                containerCssClass: "select2-input",
                dropdownCssClass: "bigdrop",
                escapeMarkup: function (m) { return m; }
            });
            
        $('.report-place-'+thisNum).on('select2:select', function (e) {
            var data = e.params.data;
            $(this).closest('div').removeClass('error')
            $(this).closest('div').find('.items-error').remove()
            addPlaceRow(data.id, thisNum);
            $('#place_hidden_'+thisNum).val(JSON.stringify({'place':data.id}))
            $("#createReportStep2").submit();
        });


        $('.report-place'+thisNum).on('select2:unselect', function (e) {
            var selected_id = e.params.data.id;
            $('.place-'+selected_id).remove();
            $("#createReportStep2").submit();
        }).trigger('change');


        num_places = thisNum;
        reloadSortable();
        
        $('#sortable').removeClass('for-draggable')
        var droped_block = $('#sortable').find('.droppable-here')
        droped_block.removeClass('droppable-here')
        
        scrollDown($('#'+place_div_id).position(), before_load);
        before_load = true;
        check_el = 0;

    });
    
    $(document).on("click",".places_result",function() {
        placeId = $(this).attr('data-id');
        placeTitle = $(this).attr('data-title');
        $(this).closest("#placesContent").find('#searchTravlogCard').val(placeTitle);
        $(this).closest("#placesContent").find('#place_id').val(placeId);
        $(this).closest("#placesContent").removeClass('show-result');
        $("#createReportStep2").submit();
    });
    
    function reomovePlaces(id, num){
        if(checkconnection()){
            return;
        }
        document.getElementById(id).remove();
        $(".place-droppable-" +num).remove();
        $("#createReportStep2").submit();
    }



    function getPlaceCountryInfo(num, place_id){
        $.ajax({
            method: "POST",
            url: "{{url('reports/ajaxGetPlaceInfo')}}",
            data: {'id':place_id, s_type:'country_info'},
        })
            .done(function (res) {
                res = JSON.parse(res);
                $('#map_hidden_'+num).val(JSON.stringify({'map':res}))
                addMapCountryInfo('mapCountryInfo'+ num, res);
            });
    }

    $(document).click(function(event) {
        var $target = $(event.target);
        if(!$target.closest('.report-search-map').length && $('.report-search-map').is(":visible")) {
            $('.dest-search-result-block').html('')
            $('.dest-search-result-block').removeClass('open')
        }
    });

    
//---END--- create/edit second step

    $('#createTravlogNextPopup').on('shown.bs.modal', function () {
        $('#step2_title').trigger('focus')
    });

    $('.create-row').on('mouseenter', function () {
        $(this).find('.target').fadeIn();
    });

    $('body').on('hidden.bs.modal', function () {
        if ($('.modal.in').length > 0)
        {
            $('body').addClass('modal-open');
        }
    });

    
    //follow/unfollow country
    $(document).on('click', '.follow-country-btn', function(){
        var _this = $(this);
        var country_id = $(this).attr('data-country-id')
        var follow_type = $(this).attr('data-fol-type')
        
       if(country_id){
           if(follow_type == 'follow'){
                var url = '{{ route("country.follow", ":countryId") }}';
                url = url.replace(':countryId', country_id);
           }else{
                var url = '{{ route("country.unfollow", ":countryId") }}';
                url = url.replace(':countryId', country_id);
           }
           
            $.ajax({
                method: "POST",
                url: url,
                data: {'data':'country'}
            })
                .done(function (res) {
                   if(res.success){
                       if(follow_type == 'follow'){
                           _this.attr('data-fol-type', 'unfollow')
                           _this.html('unfollow')
                       }else{
                           _this.attr('data-fol-type', 'follow')
                           _this.html('follow')
                       }
                   }
                });
       }
    })
    
    function reloadSortable(){
    var sortables = $( "#sortable" ).sortable({
        placeholder: "highlight",
         axis: "y",
         handle: ".handle",
         forceHelperSize: true,
         tolerance: "pointer",
         scrollSpeed: 45,
          stop: function( event, ui ) {
            var productOrder = $(this).sortable('toArray');
            $('#step2_sort').val(productOrder);
            $("#createReportStep2").submit();
              ui.item.css("top", "");
              ui.item.css("left", "");
          }
     });
     var sortedIDs = sortables.sortable('toArray')
       $('#step2_sort').val(sortedIDs);
    }
    

//Cange text aline using browser translation
var targetNode = $('html')[0];
var config = { attributes: true};
var callback = function(mutationsList, observer) {
    for(let mutation of mutationsList) {
        if (mutation.type === 'attributes' && mutation.attributeName == 'lang') {
           
            if(document.documentElement.lang != 'ar'){
                $(document).find('.ql-align-right').removeClass('ql-align-right').addClass('ql-align-left')
            }else{
               $(document).find('.ql-align-left').removeClass('ql-align-left').addClass('ql-align-right') 
            }
        }
    }
};
var observer = new MutationObserver(callback);
observer.observe(targetNode, config);


    $(document).ready(function (e) {

        var hash = window.location.hash;
        if (hash != "" && hash ==='#create') {
                setTimeout(function(){ $('#create_new_report').click()}, 100);
                window.history.pushState({}, document.title, "/reports/list");

        }
        
        $('.draggable').draggable({
          revert: false,
          helper: 'clone',
          cursor: 'pointer',
             start: function( event, ui ) {
                 $('.drug-icon-block').html('<div class="drug-icon-wrap"></div>')
             },
              stop: function( event, ui ) {
                  $('.drug-icon-block').html('') 
              }
        });
        
        $('.video-draggable').draggable({
            revert: false,
            cancel: '.noDrag',
            connectToSortable: '.drop-area',
            containment: 'document',
            helper: 'clone',
            cursor: 'pointer',
             start: function( event, ui ) {
                 $('.video-draggable').removeClass('show')
                 $('.video-dropdown-menu').removeClass('show')
                 $('.drug-icon-block').html('<div class="drug-icon-wrap"></div>')
             },
              stop: function( event, ui ) {
                    $('.drug-icon-block').find('.drug-icon-wrap').remove()
              }
        });
        
        //load more plan
         $(".add-report-plan-inner").scroll( 'scroll', function(){
            if(visibleY(document.getElementById('hide_repPlan_loader'))){
                loadMorePlan();
            }
        });
        
        
        $(document).on('click', '#plan_modal_cont',function () {
           plan_filter = '';
           getSearchedPlan(plan_filter)
           $('#searchInput').val('');
           $('#report_plan_pagenum').val(1)
        });
        
        
        //scroll top button
        $('#createTravlogNextPopup').scroll(function() {
            var scrollAmount = $('#createTravlogNextPopup').scrollTop();
            clearTimeout( $.data( this, "scrollCheck" ) );
            $.data( this, "scrollCheck", setTimeout(function() {
                if(scrollAmount > 720){
                    $('.scroll-up-btn').removeClass('d-none')
                }else{
                     $('.scroll-up-btn').addClass('d-none')
                }
            }, 100) );
          
        })
        
    $(document).on('click', '.scroll-up-btn', function(){
         $('#createTravlogNextPopup').animate({ scrollTop: 0 }, 400);
    })
        
    var scrollPosition = $(".jconfirm-content-pane").scrollTop();
        $(".jconfirm-content-pane").animate({
            scrollTop: scrollPosition
        },120);

     
    $("#reportHeaderSlider").lightSlider({
      autoWidth:true,
      pager: false,
      enableDrag: false,
      // controls: true,
      slideMargin: 20,
      prevHtml: '<i class="trav-angle-left"></i>',
      nextHtml: '<i class="trav-angle-right"></i>',
      addClass: 'post-dest-slider-wrap rep-top-slider',
      onBeforeStart: function (el) {
        el.removeAttr("style")
        
        
      },
      onSliderLoad: function (el) {
        $(el).parent().find('.lSPrev').addClass('arrow-disabled')
      },
        onBeforeSlide: function (el) {
        var active_index = el.getCurrentSlideCount();
          if(active_index == 1){
              $(el).parent().find('.lSPrev').addClass('arrow-disabled')
          }else{
              $(el).parent().find('.lSPrev').removeClass('arrow-disabled')
          }

        if(active_index == el.parent().find('li').length - 4){
              $(el).parent().find('.lSNext').addClass('arrow-disabled')
          }else{
              $(el).parent().find('.lSNext').removeClass('arrow-disabled')
          }    
        },
        responsive : [
            {
                breakpoint:767,
                settings: {
                    slideMargin:9,
                }
            },
        ],
    })


        // Function to preview cover image after validation
        $(function () {
            $("#coverImage").change(function () {
                if(checkconnection()){
                    return;
                }
                //$("#message").empty(); // To remove the previous error message
                var file = this.files[0];
                var imagefile = file.type;
                var match = ["image/jpeg", "image/png", "image/jpg", "image/webp"];
                if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]) || (imagefile == match[3])))
                {
                    $('#cover').css('background-image', 'url(noimage.png)');
                    $("#message").html("<p style='color:red'>Please upload a valid image file</p>");
                    $("#coverImage").val("");
                    return false;
                } else
                {
                    if(file.size > 10485760){
                         $("#message").html("<p style='color:red'>The maximum size of image 10MB.</p>");
                        return false;
                    }else{
                        $("#message").html("");
                    }
                    
                    var reader = new FileReader();
                    $('#coverLoader').removeClass('d-none');
                    disableButtons();
                    reader.onload = function (e){
                        $('#drage').html("");
                        uploadCoverImage(file, $('#step2_report_id').val())
                      
                    };
                    reader.readAsDataURL(this.files[0]);
                }
            });

        });

            
        //drag and drop cover image
    	$("#cover").on('dragenter', function (e){
            e.preventDefault();
            $(this).css('border', '#79bee6 2px dashed');
            $(this).css('background', '#f0f0f0');
           
	});


	$("#cover").on('dragover', function (e){
            e.preventDefault();
	});
        
        $("#cover").on('drop', function (e){
            e.preventDefault();
            var image = e.originalEvent.dataTransfer;
            
            setDropImg(image);
	});
        
        
    	$("#cropimage").on('dragenter', function (e){
            e.preventDefault();
            $(this).css('border', '#79bee6 2px dashed');
            $(this).css('background', '#f0f0f0');

	});


	$("#cropimage").on('dragover', function (e){
            e.preventDefault();
	});

        $("#cropimage").on('drop', function (e){
             $(this).css('border', 'none');
            $(this).css('background', 'none');
            e.preventDefault();
            var image = e.originalEvent.dataTransfer;

            setDropImg(image);
        });
    });

    $(document).on("click", "#button_report_cover",function() {
       $("#coverImage").click()
    });

    $('body').on('mouseover', '#cropimage', function () {
        $('#button_report_cover').show();
    });

    $('body').on('mouseout', '#cropimage', function () {
        $('#button_report_cover').hide();
    });


    function buindCroppeImage(url) {
        $('#cropimage').croppie('destroy');
        $("#cover").show();
        if (url && url.length > 15) {
            $("#cropimage").find('#button_report_cover').remove();
            $('#coverLoader').removeClass('d-none')
            var cropimg = $("#cropimage").croppie({
                viewport: {width: "100%", height: "100%"},
                boundary: {width: "1028", height: "410"},
                enforceBoundary: true,
                mouseWheelZoom: false
            });

            cropimg.croppie('bind', {
                url: url,
            }).then(function () {
                $("#cover").hide();
                if (urlExists(url)) {
                    $('.cr-slider').attr({'min': 1, 'max': 3});
                }
                $('#coverLoader').addClass('d-none')
                $("#cropimage").prepend('<button style="background-color: rgb(201, 201, 202); top: 20px; left: 10px; position: absolute; z-index: 1000; height: 30px; padding: 5px; width: 165px; font-family: inherit; font-size: 14px; display:none;" type="button" class="btn btn-light" id="button_report_cover" dir="auto">Change Cover Photo</button>');
            });
        }
    }
    function setDropImg(img){
        if(checkconnection()){
            return;
        }
        var imagefiletype = img.files[0].type;
        var cov_image_file =  img.files[0];
      
        var matchs = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefiletype == matchs[0]) || (imagefiletype == matchs[1]) || (imagefiletype == matchs[2])))
        {
            $("#message").html("<p style='color:red'>Please Select a valid Image File</p>");
          
             $("#cover").css('display', 'flex');
            $("#coverImage").html("");
            return false;
        } else{
            $("#message").html("");
            var reader = new FileReader();
            reader.onload = function (e) {
                uploadCoverImage(cov_image_file, $('#step2_report_id').val())
            if(changeTimer !== false) clearTimeout(changeTimer);
                changeTimer = setTimeout(function(){
                   
                    changeTimer = false;
                },500);
            }
             reader.readAsDataURL(img.files[0]);
        }
    }

     
    $('body').on('submit', '.discussionReplyForm', function (e) {
        var discussion_id = $(this).attr('id');
        var values = $(this).serialize();
        
        $.ajax({
            method: "POST",
            url: "{{route('discussion.reply')}}",
            data: values
        })
            .done(function (res) {
                var result = JSON.parse(res);
                $("#" + discussion_id + ".post-tips-row.empty").hide();
                $("#" + discussion_id + ".post-tips-main-block").append(result.reply).fadeIn('slow');

                $("input[name='text']").each(function (i, obj) {
                    $(this).val('');
                });

            });
        e.preventDefault();
    });
    
    function scrollDown(elem, type=null){
         if(typeof elem !== "undefined" && type){
            var height = elem.top + 500;
            $('#createTravlogNextPopup').animate({ scrollTop: height }, 400);
         }
    }

    function addPlaceRow(place_id, num){
        if(place_id){
            $.ajax({
                method: "POST",
                url: "{{url('reports/ajaxGetPlaceInfo')}}",
                data: {'id':place_id},
                async: false
            }).done(function(data){
                var result = JSON.parse(data);
                $('#add_place_row_'+num).html(`<div class="place-`+ result.id +`" style="width:100%; display:flex;margin-left:15px;">
                                                <div class="content col-md-9 plr-0 max-w-632">
                                                    <div class="travlog-card places_result mbt-20" data-id="`+result.id+`" data-title="`+result.title+`">
                                                            <div class="text-block">
                                                                <div class="title-card flex-row">
                                                                    <span class="tag">`+result.type+`</span>
                                                                    <div class="location report-place-location">
                                                                        <i class="trav-set-location-icon"></i>
                                                                        <span>`+ result.place_info.place_city +`</span>
                                                                    </div>
                                                                </div>
                                                                <div class="title-card">
                                                                    <a href="{{url_with_locale('place')}}/`+ result.id +`" class="place-link" target="_blank">`+result.title+`</a>
                                                                    <p class="report-place-address">`+result.address+`</p>
                                                                </div>
                                                            </div>
                                                            <div class="img-wrap repot-place-img">
                                                                <img src="`+result.image+`" alt="image">
                                                            </div>
                                                            <div class="rate-block">
                                                                <div class="visited">`+result.visited_list+`</div>
                                                                <div class="rate-info">
                                                                    <div class="rate-inner report-rate-inner">
                                                                    <span class="label">`+((result.rating !=null)?result.rating:'0.0')+` <i class="trav-star-icon"></i></span>
                                                                    <p class="mb-0"><b>@lang('report.excellent')</b></p>
                                                                    <p class="mb-0">`+ result.place_info.reaview_count +` reviews</p>
                                                                </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>`);
                $('#add_place_row_'+num).show();
            })

        }
    }
    
    
    function addPlaceExistingRow(place_info, num){
        if(place_info){
        $('#add_place_row_'+num).html(`<div class="place-`+ place_info[0].id +`" style="width:100%; display:flex;margin-left:15px;">
                                        <div class="content col-md-9 plr-0 max-w-632">
                                            <div class="travlog-card places_result mbt-20" data-id="`+place_info[0].id+`" data-title="`+place_info[0].title+`">

                                                <div class="text-block">
                                                                <div class="title-card flex-row">
                                                                    <span class="tag">`+place_info[0].type+`</span>
                                                                    <div class="location report-place-location">
                                                                        <i class="trav-set-location-icon"></i>
                                                                        <span>`+ place_info[0].place_info.place_city +`</span>
                                                                    </div>
                                                                </div>
                                                                <div class="title-card">
                                                                    <a href="{{url_with_locale('place')}}/`+ place_info[0].id +`" class="place-link" target="_blank">`+place_info[0].title+`</a>
                                                                    <p class="report-place-address">`+place_info[0].address+`</p>
                                                                </div>
                                                            </div>
                                                            <div class="img-wrap repot-place-img">
                                                                <img src="`+place_info[0].image+`" alt="image">
                                                            </div>
                                                            <div class="rate-block">
                                                                <div class="visited">`+place_info[0].visited_list+`</div>
                                                                <div class="rate-info">
                                                                    <div class="rate-inner report-rate-inner">
                                                                    <span class="label">`+((place_info[0].rating !=null)?place_info[0].rating:'0.0')+` <i class="trav-star-icon"></i></span>
                                                                    <p class="mb-0"><b>@lang('report.excellent')</b></p>
                                                                    <p class="mb-0">`+ place_info[0].place_info.reaview_count +` reviews</p>
                                                                </div>
                                                            </div>
                                                </div>
                                            </div>
                                        </div>`);
        $('#add_place_row_'+num).show();
        }
    }
    
    function addSelectedLocationRow(location_id){
        if(location_id){
            $.ajax({
                method: "POST",
                url: "{{url('reports/ajaxGetLocationInfo')}}",
                data: {'id':location_id},
                async: false
            }).done(function(data){
                var result = JSON.parse(data);
                addSelectedLocationHtml(location_id, result);
              
            })

        }
    }
    function addSelectedLocationHtml(location_id, location_info){
                if(location_info.country_id){
                    if($('.selected-locetion-block').find('.country-'+location_info.country_id).length>0){
                        $('.selected-locetion-block').find('.country-'+location_info.country_id).append('<div class="city-item" id="'+ location_id +'">'+
                                                                                                    '<span class="loc-city-name selected">'+ location_info.title +'</span><span class="loc-type">City</span>'+
                                                                                                '</div>')
                    }else{
                        $('.selected-locetion-block').append('</div>'+
                                    '<div class="selected-locetion-item country-'+ location_info.country_id +'">'+
                                        '<div class="country-item" id="'+ location_info.country_id +'-country">'+
                                            '<span class="loc-country-name">'+ location_info.country_name +'</span><span class="loc-type">Country</span>'+
                                        '</div>'+
                                        '<div class="city-item" id="'+ location_id +'">'+
                                            '<span class="loc-city-name selected">'+ location_info.title +'</span><span class="loc-type">City</span>'+
                                        '</div>'+
                                    '</div>')
                    }
                }else{
                    if($('.selected-locetion-block').find('.country-'+location_info.id).length>0){
                        $('.selected-locetion-block').find('.country-'+location_info.id).find('.loc-country-name').addClass('selected')
                    }else{
                        $('.selected-locetion-block').append('<div class="selected-locetion-item country-'+ location_info.id +'">'+
                                           '<div class="country-item" id="'+ location_id +'">'+
                                               '<span class="loc-country-name selected">'+ location_info.title +'</span><span class="loc-type">Country</span>'+
                                           '</div>'+
                                       '</div>')
                   }
                }
    }
    
    
    function removeSelectedLocationRow(location_id){
        if(location_id){
            var loc_info = location_id.split("-");
            if(loc_info[1] == 'country'){
                 if($('#'+location_id).closest('.selected-locetion-item').find('.city-item').length == 0)
                      $('#'+location_id).closest('.selected-locetion-item').remove();
                  else
                     $('#'+location_id).find('.loc-country-name').removeClass('selected');
            }else{
                 if($('#'+location_id).closest('.selected-locetion-item').find('.city-item').length == 1 && !$('#'+location_id).closest('.selected-locetion-item').find('.loc-country-name').hasClass('selected'))
                        $('#'+location_id).closest('.selected-locetion-item').remove();
                     else
                        $('.selected-locetion-item').find('#'+location_id).remove();
            }
        }
    }
    
    function TextLimit(elem, maxChars) {
        var permittedKeys = [8,37,38,39,40,46];
        var count = elem.val().length;
        elem.on('drop',function(e) {
            e.preventDefault();
            e.stopPropagation();
            if(count >= maxChars){
                elem.val(elem.val().substring(0, maxChars));
            }
        });
        elem.keyup(function(e) {
            if(elem.val().length < maxChars){
               elem.closest('.text-content').find('.remaining-content').removeClass('d-none')
               elem.closest('.text-content').find('.remaining-content span').html(maxChars- parseInt(elem.val().length));
            }else{
                elem.closest('.text-content').find('.remaining-content span').html(0);
            }

            if(elem.val().length == 0){
                 elem.closest('.text-content').find('.remaining-content').addClass('d-none')
            }
            if(elem.val().length >= maxChars){
                elem.val(elem.val().substring(0, maxChars));
            }
        });
    }

    function markMatch (text, term) {
        var regEx = new RegExp("(" + term + ")(?!([^<]+)?>)", "gi");
        var output = text.replace(regEx, "<span class='select2-rendered__match'>$1</span>");
        return output;
    }
    
    function markLocetionMatch (text, term) {
        var regEx = new RegExp("(" + term + ")(?!([^<]+)?>)", "gi");
        if(text != null){
            var output = text.replace(regEx, "<span class='select2-locetion-rendered'>$1</span>");
            return output;
        }
    }
    

     
  
    
    function insertMap(id, lat, lng, zoom, text){
        $('#'+id).css({'width':'640px', 'height':'420px'})
         mapboxgl.accessToken = 'pk.eyJ1IjoidHJhdm9vbyIsImEiOiJja2ZqbXE4dzMwZjd5MnBtcDYzZTBvdGF2In0.cP7Qz8lNHawWv_mUF4c7gw';

        id = new mapboxgl.Map({
            container: id,
            style: 'mapbox://styles/mapbox/satellite-streets-v11'
        });

        var marker = new mapboxgl.Marker()
            .setLngLat([parseFloat(lng), parseFloat(lat)])
            .addTo(id);

        id.setZoom(zoom);
        id.setCenter([parseFloat(lng), parseFloat(lat)]);
    }

    
        function replaceUrlParam(url, paramName) {
            var new_url;
            var beforParams = url.substr(url.indexOf(paramName)-1, 1)
            var afterParams = url.substr(url.indexOf(paramName)+paramName.length, 1)

              if(beforParams == '?' && (afterParams == '&' || afterParams == '')){
                  new_url = url.replace(beforParams+paramName+afterParams, "?");
              }
              
              if(beforParams == '&'){
                  new_url = url.replace(beforParams+paramName+afterParams, afterParams);
              }
              
              return new_url;
        }
        
        
        $(document).on('click', '.r-plan-item', function(){
            $('.rep-plan-tab-list .r-plan-item').removeClass('active')
            $(this).addClass('active')
            
            plan_type = $(this).attr('data-tab')
            
            getSearchedPlan(plan_filter)
            
        })
        
        
        //load more plan
        function loadMorePlan(){
            var nextPage = parseInt($('#report_plan_pagenum').val()) + 1;

            $.ajax({
                type: 'POST',
                url: "{{route('report.load_more_plan')}}",
                data: {pagenum: nextPage, plan_filter: plan_filter, type: plan_type},
                async: false,
                success: function (data) {
                    if(data != ''){
                          $('.report-plan-block').append(data);
                          $('#report_plan_pagenum').val(nextPage);
                      } else {
                          $("#loadMorePlan").addClass('d-none');
                      }
                }
            });
        }

        
        function getSearchedPlan(filter){
            $('#report_plan_pagenum').val(1)
             $.ajax({
                type: 'POST',
                url: "{{route('report.search_report_plan')}}",
                data: {filter: filter, type: plan_type},
                async: false,
                success: function (data) {
                    if(data.view != ''){
                        $('.report-plan-block').html(data.view)

                        $( ".report-plan-block .tp-main-block" ).each(function() {
                            var plan_id = $( this ).attr( "plan_id" );

                            if ($.inArray(plan_id, selected_plan_ids) !== -1) {
                                $( this ).prepend('<div class="trip-has-private"></div>')
                                $(this).find('.private-error-block').html('The trip plan is already inserted!')
                                $(this).find('.private-error-block').show()
                            }
                        });

                        $('.report-plan-count-block').html(data.count);
                        if(plan_type == 'mine'){
                            $('.report-plan-title').html('My Trip plans')
                        }else{
                            $('.report-plan-title').html('Invited Trip Plans')
                        }

                        if(data.count < 10){
                             $("#loadMorePlan").addClass('d-none');
                        }else{
                            $("#loadMorePlan").removeClass('d-none');
                        }
                      } else {
                        $('.report-plan-block').html('<p class="plan-not-found-block">Trip plan not found..</p>')
                        $("#loadMorePlan").addClass('d-none');
                      }

                    $('.report-plan-count-block').html(data.count)
                }
            });
        }
        
        function checkconnection() {
            var status = window.navigator.onLine;
            if (!status) {
                 return true;
            }

            if(isMobileDevice()) {
                if(quill_list.length > 0){
                    $.each(quill_list, function( index, value ) {
                        value.blur();
                    });
                }
            }
        }
        

    
</script>

<script data-cfasync="false" src="https://underscorejs.org/underscore-min.js" type="text/javascript"></script>
<script data-cfasync="false"  src="https://podio.github.io/jquery-mentions-input/lib/jquery.elastic.js" type="text/javascript"></script>
<script data-cfasync="false" type="text/javascript">

$(function () {
    $(document).on("click", "#editReport, .editReport",function() {
        
        if(checkconnection()){
             return;
        }
        var report_id = $(this).data('id');
        init_first_step_modal(report_id)
        $('#createTravlogPopup').modal('show');
    });
});


</script>

<script data-cfasync="false" type="text/javascript">
    lightbox.option({
        'disableScrolling': true
    });
</script>

<script data-cfasync="false" type="text/javascript">
   //save draft
   $(document).ready(function(){

       check_draft = 1;
           $('#createReportStep2').submit(function(e) {
               disableButtons();
               
                var status = navigator.onLine;
                if (!status) {
                    $('.report-offline-status').css('display', 'flex');
                     e.preventDefault();
                    return;
                }

               if(typeof connection !== 'undefined' && connection.effectiveType === '2g' && !navigator.onLine){
                   $('.report-offline-status').css('display', 'flex');
                   e.preventDefault();
                   return;
               }

                $('#save_draft').remove();
                $('#save_image').remove();
           
        $('.ql-editor').each(function (index, value) {
            var HTML = $(this).html();
            $(this).parent().siblings("input").each(function (index2, value2) {
                if(removeHtmlTags(HTML) != ''){
                    if($(this).attr('name') == 'rep_infos[]'){
                       $(this).val(JSON.stringify({'text':HTML})); 
                    }else{
                        $(this).val(HTML);
                    }
                }else{
                    $(this).val('');
                }
                
            });
        });

        reloadSortable();



        if(check_draft == 3){
            scrollToSowError('.rep-text-input')
            scrollToSowError('.videoUrl')
            scrollToSowError('.rep-places')
            scrollToSowError('.rep-maps')
            scrollToSowError('.rep-people')
            checkMediaUrl(e)
            
            $('.side.right ul').hide();
//            disableButtons();
        }
        
       
        if(validations(".videoUrl", 'Please insert a valid video URL from Youtube, Dailymotion or Vimeo...') ){
            if( $('.videoUrl').closest('.input-block').find('.items-error').length > 1){
                $('.videoUrl').closest('.input-block').find('.items-error:first').remove()
            }
            check_draft = 1;
            $('.side.right ul').show();
             e.preventDefault();
        }
       
        if(validations(".rep-people", 'The users field is required.')){
            check_draft = 1;
            $('.side.right ul').show();
             e.preventDefault();
        }
        if(validations(".rep-maps", 'The maps field is required.')){
            check_draft = 1;
            $('.side.right ul').show();
             e.preventDefault();
        }
        if(validations(".rep-places", 'The places field is required.')){
            check_draft = 1;
            $('.side.right ul').show();
             e.preventDefault();
        }
        
         if(validations(".rep-text-input", 'The text is required.', 'text') ){
            check_draft = 1;
             $('.side.right ul').show();
             e.preventDefault();
        }

        if($('.report-publish-link').hasClass('rep-disabled')){
            $('.report-publish-link').removeClass('rep-disabled')
            $('.report-publish-link').addClass('publish-report')
        }
       
        // $(this).submit();
        if(check_draft == 1){
            $('#createReportStep2').append('<input type="hidden" name="save_draft" id="save_draft" value="1" />')
        e.preventDefault();
         var data = new FormData(this);
   
        $.ajax({
            method: "POST",
            url: "{{url('reports/create-info')}}",
            data: data,
            cache:false,
            contentType: false,
            processData: false,
        })
            .done(function (res) {
                $('#save_draft').remove()
                if( $('#step2_report_id').val() == 0){
                    $('#step2_report_id').val(res);
                }

                if(check_cover_uploader && check_image_uploader && check_video_uploader){
                    enableButtons();
                }
            }); 
        }
        
        if(check_draft == 2){
             e.preventDefault();
        }
        
    });
    
   })

   function saveReport(type){
   check_draft = 3;
   check_el = 0;
    $('#save_draft').remove()
        if(type == 'draft'){
            $('#step2_flag').val('0')
        }else{
            $('#step2_flag').val('1')
        }

   }
   
   function urlExists(url){
       regexp =  /^(?:(?:http?|https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;

       if (regexp.test(url)) {
           return true;
       }else{
           return false;
       }

  }

   function checkMediaUrl(ev){
       var height = 0;
       $('.url-error').filter(function() {
           if(!$(this).hasClass('d-none')  && check_el == 0){
               check_el = 1;
               height = $(this).parent().offset().top + $('#createTravlogNextPopup').scrollTop() - 500;
           }
       });

       if(check_el == 1){
           check_el = 2
           $('#createTravlogNextPopup').animate({ scrollTop: height }, 400);

           check_error = false;
           check_draft = 1;
           enableButtons();
           ev.preventDefault();

       }



   }
  
  function removeHtmlTags(data) {
      if(data){
            var text = data;
             text = text.replace(/<p>|<br>|<\/p>|<p dir="auto">|<br dir="auto">/g, '');
             
         return text;
      }
}

 function scrollToSowError(element){
    var height = 0;
     $(element).filter(function() {
         console.log('this.value', this.value)
                if(this.value == '' && check_el == 0){
                    check_el = 1;
                    height = $(this).parent().offset().top + $('#createTravlogNextPopup').scrollTop() - 500;     
                }
    });

    if(check_el == 1){
         check_el = 2
         $('#createTravlogNextPopup').animate({ scrollTop: height }, 400);
    }
    
   
 }


 
 function validations(inputClass, message, type = ''){
    var isValid = '';
     $(inputClass).each(function (item) {
         var sel_val = $(this).val()
       
        if(type == 'text'){
            sel_val = removeHtmlTags(sel_val);
        }
         if(sel_val == '' || sel_val == null){
             
             if( !$(this).closest('div').hasClass('error')){
                 $(this).closest('div').addClass('error')
                 $(this).closest('div').append('<p class="items-error">'+ message +'</p>')
             }
            isValid = true; 
         } 
        });
         
         if(isValid){
            return true;
         }else{
            return false;
         }
 }

   //Return plan content
   function planHtmlContent(plan_div_id, thisNum, selected_plan, content){
       return `<div id="`+ plan_div_id +`" class="m-position-rel">
                    <div class="m-action-block">
                        <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-up"></i></a>
                        <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-down"></i></a>
                        <a href="javascript:;" onclick="reomovePlan('`+plan_div_id+`', '`+thisNum+`', `+ selected_plan +`)" class="d-inline-flex"><i class="trav-close-icon"></i></a>
                    </div>
                    <div class="create-row ui-state-default add_plan_cont mtop-20 m-items-block"  style="border:none;background:none;" data-plan_id="` + selected_plan + `">
                        ` + content + `
                        <input type="hidden"  value="` + selected_plan + `" />
                        <input type="hidden" id="plan_hidden_` + thisNum + `" name="rep_infos[]" value=` + JSON.stringify({'plan': selected_plan}) + `>
                    </div>
                    <div class="drug-icon-block plan-droppable-` + thisNum + `"></div>
            </div>`;
       }
 
 //Return place content
 function placeHtmlContent(place_div_id, thisNum){
 return `<!-- add place container -->
        <div id="`+place_div_id+`" class="rep-add-place-items m-position-rel">
             <div class="m-action-block">
                 <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-up"></i></a>
                 <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-down"></i></a>
                 <a href="javascript:;" onclick="reomovePlaces('`+place_div_id+`', '`+thisNum+`')" class="d-inline-flex"><i class="trav-close-icon"></i></a>
             </div>
            <div class="create-row add_place_cont pb-0 m-items-block"  style="border:none;background:none;margin-top:20px;margin-bottom:0;">
                <div class="side left w-70 pl-0">
                    <div class="label">Places</div>
                </div>
                <div class="content" id="placesContent">
                    <div class="search-place report-search-place">
                        <div class="input-block rep-place-block">
                            <div id="rep_place`+place_div_id+`">
                                <select id="search_`+place_div_id+`" data-placeholder="Type a place..." class="report-place-`+ thisNum +` rep-places" style="width:100%"></select>
                                <input type="hidden" id="place_hidden_`+ thisNum +`" name="rep_infos[]" value="">
                            </div>
                        </div>
                        <div class="travlog-card-wrap mCustomScrollbar" id="placesResults_`+place_div_id+`" style="overflow:auto;">

                        </div>
                                    
                        <!-- show place container -->
                        <div class="create-row row pb-0" id="add_place_row_`+thisNum+`" style="display:none;border:none;background:none;margin-top:20px;margin-bottom:0;"></div>
                    </div>
                </div>
                <div class="side right m-side-block">
                    <ul class="video-upload-control">
                        <li>
                            <a href="#" class="handle d-inline-flex">
                                <i class="trav-move-icon rep-move-icon"></i>
                                <span>Move</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="reomovePlaces('`+place_div_id+`', '`+thisNum+`')" class="d-inline-flex">
                                <div class="close-handle red rep-close-handle">
                                    <i class="trav-close-icon"></i>
                                </div>
                                <span>Delete</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="drug-icon-block place-droppable-`+ thisNum +`"></div>
            </div>`;
 }
 
 //Return travoo info content
 function infoHtmlContent(info_div_id, thisNum, infoVar, infoVal, infoVarId, infoDest){

     return `<div id="`+info_div_id+`" class="m-position-rel">
                 <div class="m-action-block">
                     <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-up"></i></a>
                     <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-down"></i></a>
                     <a href="javascript:;" onclick="removeInfo('`+info_div_id+`', '`+thisNum+`', '`+ infoVarId +`');" class="d-inline-flex"><i class="trav-close-icon"></i></a>
                 </div>
                <div class="create-row ui-state-default add_info_cont mtop-20 m-items-block"  style="background:none;border:none;" data-info="`+ infoVar.replace(/<(.|\n)*?>/g, '') +`">
                <div class="side left w-70">
                    <div class="label report-info-label">Travooo Info</div>
                </div>
                <div class="content w-632">
                    <div class="trav-currency-line-block">
                        <div class="block-name trev-info-name" id="infoBlockName">`+infoVar+`  `+(infoDest != ''?'- ' + infoDest:'')+`</div>
                        <div class="trev-info-desc" id="infoBlockValue">
                           `+ infoVal +`
                        </div>
                    </div>
                </div>
                <div class="side right target m-side-block">
                    <ul class="video-upload-control">
                        <li>
                            <a href="#" class="handle d-inline-flex">
                                <i class="trav-move-icon rep-move-icon"></i>
                                <span>Move</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="removeInfo('`+info_div_id+`', '`+thisNum+`', '`+ infoVarId +`');" class="d-inline-flex">
                                <div class="close-handle red rep-close-handle">
                                    <i class="trav-close-icon"></i>
                                </div>
                                <span>Delete</span>
                            </a>
                        </li>
                    </ul>
                    <input type="hidden" id="info_hidden_`+ thisNum +`" name="rep_infos[]" value='`+ JSON.stringify({'info':infoVar+':-'+infoVal.replace(/\'/g, "&#39;")+':-' + infoVarId+':-' + infoDest}) +`'>
                </div>
            </div>
                <div class="drug-icon-block info-droppable-`+ thisNum +`"></div>
            </div>`;
 }
  
  //Return User list content
 function peopleHtmlContent(people_div_id, thisNum){
 return `<!-- add people container -->
        <div id="`+people_div_id+`" class="m-position-rel">
             <div class="m-action-block">
                 <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-up"></i></a>
                 <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-down"></i></a>
                 <a href="javascript:;" onclick="removeUsers('`+people_div_id+`', '`+thisNum+`')" class="d-inline-flex"><i class="trav-close-icon"></i></a>
             </div>
            <div class="create-row ui-state-default add_people_cont add_people-`+thisNum+` pb-0 m-items-block"  style="border:none;background:none;margin-top:20px;margin-bottom:0px;">
                <div class="side left w-70">
                    <div class="label">People</div>
                </div>
                <div class="content">
                    <div class="input-block report-search-people">
                        <select class="input rep-people report_user`+thisNum+`"  id="report_user-`+thisNum+`" multiple="multiple" placeholder="Type a name..." style="width:100%">
                        </select>
                        <input type="hidden" id="people_hidden_`+ thisNum +`" name="rep_infos[]" value="">
                    </div>

                    <!-- show ppl container -->
                    <div class="create-row row" id="add_people_row_`+thisNum+`" style="display:none;border:none;background:none;margin-top:20px;margin-bottom:0px;padding-bottom: 8px;"></div>
                </div>
                <div class="side right m-side-block"><ul class="video-upload-control"><li><a href="#" class="handle d-inline-flex"><i class="trav-move-icon rep-move-icon"></i><span>Move</span></a></li><li><a href="#" onclick="removeUsers('`+people_div_id+`', '`+thisNum+`')" class="d-inline-flex"><div class="close-handle red rep-close-handle"><i class="trav-close-icon"></i></div><span>Delete</span></a></li></ul></div>
            </div>
            <div class="drug-icon-block people-droppable-`+ thisNum +`"></div>
        </div>`;
 }
 
 //Return Map content
 function mapHtmlContent(map_div_id, thisNum){
  return `<!-- add map container -->
            <div id="`+map_div_id+`" class="rep-maps-items m-position-rel">
                 <div class="m-action-block">
                     <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-up"></i></a>
                     <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-down"></i></a>
                     <a href="javascript:;" onclick="removeMap('`+map_div_id+`', '`+thisNum+`')" class="d-inline-flex"><i class="trav-close-icon"></i></a>
                 </div>
                <div class="create-row ui-state-default add_map_cont pb-0 m-items-block" style="border:none;background:none;margin-top:20px;margin-bottom:0px;">
                <div class="side left w-70">
                    <div class="label"> Map </div>
                </div>
                <div class="content w-632">
                    <div class="input-block report-search-map search-map-`+map_div_id+`">
                        <input type="text" class="search-rep-maps rep-maps report_map-`+map_div_id+`" id="report_map_`+map_div_id+`" placeholder="Search &zwnj;City, &zwnj;Country or &zwnj;Place..." autocomplete="false">
                        <span class="dest-search-result-block"></span>
                        <input type="hidden" id="map_hidden_`+ thisNum +`" name="rep_infos[]" value="">
                    </div>
                    <!-- show map container -->
                    <div class="create-row ui-state-default" style="border:none !important;background:none;margin-top:20px;margin-bottom:0px;">
                        <div class="content w-632">
                            <div class="post-image-container post-follow-container travlog-map-wrap report-map-wrap" style="border:none !important;">
                                <div class="post-map-wrap">
                                    <div class="post-map-inner p-rel">
                                         <div id="added_plan_map`+ thisNum +`"></div>
                                         <div class="map-country-info" id="mapCountryInfo`+ thisNum +`"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="side right m-side-block">
                    <ul class="video-upload-control">
                        <li>
                            <a href="#" class="handle d-inline-flex">
                                <i class="trav-move-icon rep-move-icon"></i>
                                <span>Move</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="removeMap('`+map_div_id+`', '`+thisNum+`')" class="d-inline-flex">
                                <div class="close-handle red rep-close-handle">
                                    <i class="trav-close-icon"></i>
                                </div>
                                <span>Delete</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
                <div class="drug-icon-block map-droppable-`+ thisNum +`"></div>
            </div>`;
 }
 
 

 //Return video content
 function videoHtmlContent(video_div_id, thisNum){
  return `<!-- add video container -->
        <div id="`+video_div_id+`" class="m-position-rel">
         <div class="m-action-block">
             <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-up"></i></a>
             <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-down"></i></a>
             <a href="javascript:;" onclick="removeVideoContent(`+video_div_id+`, `+thisNum+`)" class="d-inline-flex"><i class="trav-close-icon"></i></a>
         </div>
        <div class="create-row add_video_cont m-items-block"  style="border:none;background:none;margin-top:20px;">
            <div class="side left w-70">
                <div class="label">Video</div>
            </div>
            <div class="content">
                <div class="input-block">
                    <input type="text" class="input videoUrl" data-id="`+thisNum+`" placeholder="Paste a video link from Youtube, DailyMotion or Vimeo...">
                    <input type="hidden" class="youtube-videos" required  id="videoID`+thisNum+`">
                    <input type="hidden" id="video_url_hidden_`+ thisNum +`" name="rep_infos[]" value="">
                </div>
                        
                <!-- show video container -->
                <div class="create-row video_embed`+thisNum+` d-none" style="display:block;border:none;background:none;margin-top:20px;margin-bottom:0px;padding-bottom:0;">
                    <div class="content">
                        <div class="video-block">
                            <iframe width="632" height="350"
                            src="" id="video_iframe`+thisNum+`" webkitAllowFullScreen mozallowfullscreen allowFullScreen>
                            </iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="side right m-side-block">
                <ul class="video-upload-control">
                    <li>
                        <a href="#" class="handle d-inline-flex">
                            <i class="trav-move-icon rep-move-icon"></i>
                            <span>Move</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" onclick="removeVideoContent(`+video_div_id+`, `+thisNum+`)" class="d-inline-flex">
                            <div class="close-handle red rep-close-handle">
                                <i class="trav-close-icon"></i>
                            </div>
                            <span>Delete</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
            <div class="drug-icon-block videos-droppable-`+ thisNum +`"></div>
        </div>`;
 }
 

 
 
 function imageHtmlContent(image_div_id, thisNum){
   return `<div class="add_image_cont d-none m-position-rel" id="`+image_div_id+`" >
            <div class="m-action-block">
                <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-up"></i></a>
                <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-down"></i></a>
                <a href="javascript:;" onclick="removeImage('`+image_div_id+`', '`+thisNum+`')"  class="d-inline-flex"><i class="trav-close-icon"></i></a>
            </div>
            <div class="create-row ui-state-default m-items-block"  style="border:none;background:none;margin-top:20px;margin-bottom:20px;">
        <div class="side left w-70">
            <div class="label">Image</div>
        </div>
        <div class="content rep-img-content" style="text-align:right;position:relative;">
            <input type="file" class="file-input reportImage" data-id="`+thisNum+`" id="reportImage`+thisNum+`" value="" style="display:none;">
            <input type="hidden"  id="rep_image`+thisNum+`">
                    <img class="rep-lazy" id="add_image_src`+thisNum+`" src="" alt="image" width="592px" />
                        <label class="checkbox-inline add-link-label travcheckbox">
                            <input type="checkbox" value="" class="image-link-checkbox add-link-checkbox" id="checkLink_`+image_div_id+`">
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            Insert Link?
                        </label>
                        <div class="image-info-content">
                            <input class="image-info" id="info_`+image_div_id+`"  maxlength="55" type="text"  data-typenum="`+ thisNum +`" data-typeid="`+ image_div_id +`" placeholder="Insert image caption here...">
                            <input class="image-hyperlink report-item-hyperlink d-none" id="link_`+image_div_id+`" data-typeid="`+ image_div_id +`" data-typenum="`+ thisNum +`"  type="text" placeholder="Insert a valid URL here (Ex: https://example.com)">
                            <span class="url-error d-none"></span>
                            <input type="hidden" id="image_hidden_`+ thisNum +`" name="rep_infos[]" value="">
                        </div>
        <div class="loader `+thisNum+`-loader d-none">
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-text">Uploading... <span class="progress-value" data-progress-value>0%</span></div>
            </div>
        </div>
        </div>
        <div class="side right m-side-block">
            <ul class="video-upload-control">
                <li>
                    <a href="#" class="handle d-inline-flex">
                        <i class="trav-move-icon rep-move-icon"></i>
                        <span>Move</span>
                    </a>
                </li>
                <li>
                    <a href="#" onclick="removeImage('`+image_div_id+`', '`+thisNum+`')"  class="d-inline-flex">
                        <div class="close-handle red rep-close-handle">
                            <i class="trav-close-icon"></i>
                        </div>
                        <span>Delete</span>
                    </a>
                </li>
            </ul>
        </div>
        </div>
            <div class="drug-icon-block image-droppable-`+ thisNum +`"></div>
        </div>`;
 }
 
 function textHtmlContent(text_div_id, thisNum){
     return `<div id="`+text_div_id+`" class="m-position-rel">
                 <div class="m-action-block">
                     <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-up"></i></a>
                     <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-down"></i></a>
                     <a href="javascript:;" onclick="removeText('`+text_div_id+`', '`+thisNum+`')" class="d-inline-flex"><i class="trav-close-icon"></i></a>
                 </div>
                <div class="create-row ui-state-default add_text_cont m-items-block" style="border:none;background:none;margin-top:20px;margin-bottom:68px;">
                <div class="side left w-70">
                    <div class="label">Text</div>
                </div>
                <div class="content" style="width:76%;">
                    <div class="notranslate" id="text">

                    </div>
                    <input type="hidden" class="rep-text-input" id="step2_text`+thisNum+`" />
                    <input type="hidden" id="text_hidden_`+ thisNum +`" name="rep_infos[]" value="">
                </div>
                <div class="side right m-side-block">
                    <ul class="video-upload-control">
                        <li>
                            <a href="#" class="handle d-inline-flex">
                                <i class="trav-move-icon rep-move-icon"></i>
                                <span>Move</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="removeText('`+text_div_id+`', '`+thisNum+`')" class="d-inline-flex">
                                <div class="close-handle red rep-close-handle">
                                    <i class="trav-close-icon"></i>
                                </div>
                                <span>Delete</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
                <div class="drug-icon-block text-droppable-`+ thisNum +`"></div>
            </div>`;
 }
 
 function localVideoHtmlContent(local_video_div_id, thisNum){
     return `<div id="`+local_video_div_id+`" class="add_local_video_cont d-none m-position-rel">
                 <div class="m-action-block">
                     <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-up"></i></a>
                     <a href="javascript:;" class="handle d-inline-flex"><i class="trav-angle-down"></i></a>
                     <a href="javascript:;" onclick="removeLocalVideo('`+local_video_div_id+`', '`+thisNum+`')" class="d-inline-flex"><i class="trav-close-icon"></i></a>
                 </div>
                <div class="create-row ui-state-default m-items-block" style="border:none;background:none;margin-top:20px;margin-bottom:20px;">
                <div class="side left w-70">
                    <div class="label">Video</div>
                </div>
                <div class="content" style="text-align:right;position:relative;">
                    <input multiple type="file" class="file-input reportLocalVideo" data-id="`+thisNum+`" id="reportLocalVideo`+thisNum+`"  value="" style="display:none;">
                    <input  type="hidden" id="rep_video`+thisNum+`">
                        <div id="add_video_src`+thisNum+`"></div>
                         <label class="checkbox-inline add-link-label travcheckbox">
                            <input type="checkbox" value="" class="video-link-checkbox add-link-checkbox" id="checkLink_`+local_video_div_id+`">
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            Insert Link?
                        </label>
                        <div class="video-info-content">
                            <input class="video-info" id="info_`+local_video_div_id+`"  maxlength="55" type="text"  data-typeid="`+ local_video_div_id +`" data-typenum="`+ thisNum +`" placeholder="Insert video caption here...">
                            <input class="video-hyperlink report-item-hyperlink d-none" id="link_`+local_video_div_id+`"  type="text" data-typeid="`+ local_video_div_id +`" data-typenum="`+ thisNum +`" placeholder="Insert a valid URL here (Ex: https://example.com)">
                            <span class="url-error d-none"></span>
                            <input type="hidden" id="local_video_hidden_`+ thisNum +`" name="rep_infos[]" value="">
                        </div>

                <div class="loader `+thisNum+`-video-loader d-none">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-text">Uploading... <span class="progress-value" data-progress-value>0%</span></div>
                    </div>
                </div>
                </div>
                <div class="side right m-side-block">
                    <ul class="video-upload-control">
                        <li>
                            <a href="#" class="handle d-inline-flex">
                                <i class="trav-move-icon rep-move-icon"></i>
                                <span>Move</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="removeLocalVideo('`+local_video_div_id+`', '`+thisNum+`')" class="d-inline-flex">
                                <div class="close-handle red rep-close-handle">
                                    <i class="trav-close-icon"></i>
                                </div>
                                <span>Delete</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
                <div class="drug-icon-block l-video-droppable-`+ thisNum +`"></div>
            </div>`;
 }
 
 
 

  
</script>
@include('site.reports._mobile-scripts')

