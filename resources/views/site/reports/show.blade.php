<?php
use App\Models\Reports\ReportsComments;
use App\Models\Reports\Reports;
use App\Services\Reports\ReportsService;
?>
@extends('site.layouts.site')
@section('after_styles')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{url('assets2/js/jquery-loading-master/dist/jquery.loading.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<link href="{{ asset('assets2/js/lightbox2/src/css/lightbox.css') }}" rel="stylesheet"/>
<!--<link href="{{ asset('/plugins/emoji/emojionearea.css') }}" rel="stylesheet"/>-->
<link href="{{ asset('assets2/js/summernote/summernote.css') }}" rel="stylesheet">
<link href="{{ asset('/plugins/summernote-master/tam-emoji/css/emoji.css') }}" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
<link href="{{ asset('/dist-parcel/travlogs-web-upd.css') }}" rel="stylesheet"/>
<script data-cfasync="false" src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js' type="text/javascript"></script>

<script data-cfasync="false" src="https://npmcdn.com/@turf/turf@5.1.6/turf.min.js" type="text/javascript"></script>
 <style>
    .select2-input {
        /* height:49px !important; */
        padding:8px !important;
        border: 1px solid #e6e6e6 !important;
    }
    .select2-container .select2-selection--single {
        height:49px !important;
    }
    .select2-selection__choice {
        background: #f7f7f7 !important;
    padding: 5px 10px !important;
    position: relative !important;
    border-radius: 0 !important;
        color: #4080ff !important;
        margin-top:2px !important;
    }
    .ui-state-highlight {
        background: #C8EBFB !important;
        border-radius: 7px;
    }

    .row-drag {
        opacity: 0 !important;
    }

    em {
        font-style: italic;
    }
    .modal-custom-style .post-block.post-travlog.post-create-travlog .main-content .create-row .side
    {
        position: unset;
    }
    .ql-container
    {
        height: auto;
    }
    .ql-snow .ql-editor a
    {
        color: #06c;
        text-decoration: underline;
    }

    .com-time{
        float: right;
        color: #b2b2b2;
        font-size: 15px;
    }

    .report-comment-footer a{
        text-decoration:none !important;
    }

    .load-more-link{
        font-family: 'CircularAirPro Light';
        font-size: 14px;
        color: #b2b2b2;
    }

    .text-content p, .text-content h1, .text-content h2{
        overflow-wrap: break-word;
    }
</style>
<link href="{{asset('/dist-parcel/reports-upd.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('dist-parcel/assets/Travelog/main.css') }}" rel="stylesheet"/>
    @endsection

@section('before_styles')
<meta property="og:url"           content="{{url('reports/'.$report->id)}}" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="{{$report->title}} - Travooo.com" />
<meta property="og:description"   content="{{$report->description}}" />
<meta property="og:image"         content="@if($report->cover){{@$report->cover[0]->val}}@endif" />
@endsection

@section('content-area')

    @if (session('status'))
        <div class="alert alert-success report-alert">
            {{ session('status') }}
        </div>
    @endif
    
    <div class="report-offline-status">
        <div class="rep-ofline-block"></div>
        No internet Connection...
    </div>

    <div class="alert alert-success report-alert publushed-alert d-none">
            The report published successfully.
    </div>
    
     @php 
        if($is_draft){
            $existing_report = $current_report_info;
            $publish_type = 'draft';
        }else{
            $existing_report = $report;
            $publish_type = 'publish'; 
        }
        $totalComments= $report->comments()->count();
        $totalLikes = $report->likes()->count();
        $fetchLocations = $existing_report->prettyLocation();
        $flatLocations = collect($fetchLocations[Reports::LOCATION_COUNTRY])->merge($fetchLocations[Reports::LOCATION_CITY]);
    @endphp

    <div class="post-block post-travlog">
        <div class="post-top-info-layer post-footer-info">
            <div class="info-side report-info-side">
                <a href="{{url('reports/list')}}" class="back-link"><i class="trav-angle-left"></i> @lang('other.back')</a>
                <div class="post-foot-block post-reaction mr-5">
                    <a href="javascript:;" class="top-action {{Auth::check()?'like-button':'open-login'}}" data-id="{{$report->id}}" dir="auto">
                        <i class="{{(Auth::check() && $report->likes()->where('users_id', Auth::user()->id)->first())?'trav-heart-fill-icon':'trav-heart-icon'}}" dir="auto" style="color: rgb(255, 90, 121);"></i>
                    </a>
                    <a href="#" class="{{Auth::check()?'report_likes_modal' : 'open-login'}}" id='{{$report->id}}'>
                        <span class="report-like-count"><b>{{$totalLikes}}</b> Likes</span>
                    </a>
                </div>
                <span class="comment-dot report-com-dot mr-5"> · </span>
                <div class="post-foot-block report-comment-cnt">
                    <i class="trav-comment-icon"></i>
                    @if($totalComments)
                    <ul class="foot-avatar-list report-av-list">
                        <?php $us = array(); ?>
                        @foreach($report->comments()->orderBy('created_at', 'desc')->get() AS $repc)
                            @if(!in_array($repc->users_id, $us))
                            <?php $us[] = $repc->users_id; ?>
                                @if(count($us)<4)
                                    <li><img class="small-avat" src="{{check_profile_picture($repc->author->profile_picture)}}" alt="{{$repc->author->name}}" title="{{$repc->author->name}}" width='20px' height='20px'></li>
                                @endif
                            @endif
                        @endforeach
                    </ul>
                    @endif
                     <span> <strong class="{{$report->id}}-all-comments-count">{{$totalComments}}</strong> <b>@lang('comment.comments')</b></span>

                </div>
            </div>
            <div class="actions-side">
                @if($report->published_date == NULL)
                    <a href="javascript:;" class="top-action rep-disabled">
                            <i class="trav-share-icon"></i>
                            <span>@lang('report.share')</span>
                        </a>
                    <a href="javascript:;" class="top-action rep-disabled">
                        <i class="trav-save-icon"></i>
                        <span>Embed</span>
                    </a>
                @else
                    @if(Auth::check())
                            <a href="#shareablePost" class="top-action" data-uniqueurl="{{url('reports', $report->id)}}" data-toggle="modal" data-id="{{ $report->id}}" data-type="report" dir="auto">
                    @else
                        <a href="javascript:;" class="top-action open-login">
                    @endif
                        <i class="trav-share-icon"></i>
                        <span>@lang('report.share')</span>
                    </a>
                     @if(Auth::check())
                       <a href="#" class="top-action" data-toggle="modal" data-target="#embedPopup">
                    @else
                        <a href="javascript:;" class="top-action  open-login">
                    @endif
                        <i class="trav-save-icon"></i>
                        <span>Embed</span>
                    </a>
                 @endif
                @if(Auth::check() && Auth::user()->id == $report->users_id)
                    <a href="javascript:;" class="top-action {{$report->flag == 0?'publish-report':'rep-disabled'}} report-publish-link" data-id="{{$report->id}}" data-ftype="single">
                        <img src="{{asset('assets2/image/publish-icon.png')}}" style="width:22px;margin-right:12px;" />
                        <span >Publish</span>
                    </a>

                    <a href="javascript:;" class="top-action" id="editReport" disabled data-id="{{$report->id}}" data-cover_img="{{($existing_report->cover)?@$existing_report->cover[0]->val.'?t='.time():''}}">
                        <img src="{{asset('assets2/image/edit.svg')}}" style="width:20px;margin-right:15px;" />
                        <span >Edit</span>
                    </a>

                    <a href="javascript:;" class="top-action delete-report" deltype="2" data-id="{{$report->id}}" style="color:Red">
                        <img src="{{asset('assets2/image/delete.svg')}}" style="width:20px;margin-right:15px;" />
                        <span style="color:red">Delete</span>
                    </a>
                @else
                    <div class="d-inline-block pull-right">
                        <div class="post-top-info-action">
                            <div class="dropdown">
                                <button class="btn btn-drop-round-grey btn-drop-transparent" type="button" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    <i class="trav-angle-down"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Report">
                                    @include('site.home.partials._info-actions', ['post'=>$report])
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="top-banner-block top-travlog-banner"
             style="@if($existing_report->cover) background-image: url({{($existing_report->cover)?@$existing_report->cover[0]->val:''}}) @endif">
            <div class="top-banner-inner" style="direction: ltr;">
                <div class="travel-left-info">
                    <h3 class="travel-title intreport-title">{{$existing_report->title}}</h3>
                    <div class="travlog-user-info">
                        <div class="user-info-wrap">
                            <div class="avatar-wrap">
                                <img src="{{check_profile_picture($report->author->profile_picture)}}" alt="{{$report->author->name}}" style='width:50px;height:50px;'>
                            </div>
                            <div class="info-txt">
                                <div class="top-name">
                                    @lang('other.by') <a class="post-name-link" target="_blank"
                                                         href="{{url('profile/'.$report->author->id)}}">{{$report->author->name}}</a>
                                    {!! get_exp_icon($report->author) !!}
                                </div>
                                @if(count($expertise)>0)
                                <div class="sub-info">
                                    @lang('report.expert_in')
                                    @foreach($expertise as $k=>$exp)
                                    @php
                                        $exp_link_info = explode(',', $exp['id']);
                                        if($exp_link_info[0] == 'country'){
                                            $redirect_link = url('country', $exp_link_info[1]);
                                        }else{
                                            $redirect_link = url('city', $exp_link_info[1]);
                                        }
                                    @endphp
                                        @if($k<3)
                                        <a href="{{$redirect_link}}" target="_blank" class="place-link">{{$exp['text']}}</a>
                                        @endif
                                        {{($k < 2 && $k+1 < count($expertise))?',':''}}
                                        {{($k == 3)?'...':''}}
                                    @endforeach
                                </div>
                                @endif
                            </div>
                        </div>
                        @if(Auth::check() && $report->author->id != Auth::user()->id)
                            <div class="info-action">
                                    <button class="btn {{($follower && $follower->follow_type == 1)?'btn-white-follow':'btn-blue-follow'}} follow-button follow-button_{{$report->author->id}} cover-follow" data-user-id="{{$report->author->id}}" data-route-follow="{{ route('profile.follow', $report->author->id) }}"  data-route-unfollow="{{ route('profile.unfolllow', $report->author->id) }}" data-type="{{($follower && $follower->follow_type == 1)?'unfollow':'follow'}}">
                                        @if($follower && $follower->follow_type == 1)
                                            @lang('buttons.general.unfollow')
                                        @else
                                            @lang('buttons.general.follow')
                                        @endif
                                    </button>
                            </div>
                        @endif
                    </div>
                    @if($date_type != '')
                        <div class="p-date-content">
                            {{$date_type}}: <span class="p-date-wrap">{{$create_or_publish_date}}</span>
                        </div>
                    @endif
                </div>
                <div class="travel-right-info">
                    <ul class="sub-list">
                        @php $number_output = 0; @endphp
                        @if($fetchLocations['total'] > 1)
                            <li>
                                @if (count($fetchLocations[Reports::LOCATION_COUNTRY]) > 0 && count($fetchLocations[Reports::LOCATION_CITY])> 0)
                                    <div class="top-txt">Destination</div>
                                @elseif (count($fetchLocations[Reports::LOCATION_COUNTRY]) > 0)
                                    <div class="top-txt">Destination</div>
                                @else 
                                    <div class="top-txt">Destination</div> 
                                @endif
                                @foreach($flatLocations as $locations)
                                    @if($number_output < 2)
                                         <div class="sub-txt"><a style="color:white" href="{{ url_with_locale('city/'.@$locations->id)}}">{{$locations->transsingle->title}}</a></div>
                                    @elseif($number_output == 2)
                                        <a href="javascript:;" class="more-link more-location-modal" data-report-id="{{$report->id}}">+ {{$flatLocations->count()-2}} More</a>
                                    @endif
                                    @php $number_output ++;@endphp
                                @endforeach
                            </li>
                        @else
                            @if(count($fetchLocations[Reports::LOCATION_COUNTRY]))
                                <li>
                                    <div class="top-txt">Destination </div>
                                    <div class="sub-txt"><a style="color:white" href="{{ url_with_locale('country/'.@collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->id)}}">{{@collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->transsingle->title}}</a></div>
                                </li>
                                {{-- @if(isset(collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->capitals))
                                <li>
                                    <div class="top-txt">@lang('region.country_capital')</div>
                                    <div class="sub-txt">{{@collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->capitals[0]->city->transsingle->title}}</div>
                                </li>
                                @endif
                                <li>
                                    <div class="top-txt">@lang('region.population')</div>
                                    <div class="sub-txt">{{collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->transsingle->population}}</div>
                                </li>
                                @if(collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->currencies)
                                    <li>
                                        <div class="top-txt">Currency</div>
                                        @php $cur_output = 0; @endphp
                                        @foreach(collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->currencies as $curencies)
                                            @if($cur_output < 2)
                                                <div class="sub-txt">{{@$curencies->transsingle->title}}</div>
                                            @elseif($cur_output == 2)
                                                    <a href="javascript:;" class="more-link" >+ {{count(collect($fetchLocations[Reports::LOCATION_COUNTRY])->first()->currencies)-2}} More</a>
                                            @endif
                                        @php $cur_output ++;@endphp
                                        @endforeach
                                    </li>
                                @endif --}}
                            @elseif(count($fetchLocations[Reports::LOCATION_CITY]))
                                <li>
                                    <div class="top-txt">Destination</div>
                                    <div class="sub-txt"><a style="color:white" href="{{ url_with_locale('city/'.collect($fetchLocations[Reports::LOCATION_CITY])->first()->id)}}">{{@collect($fetchLocations[Reports::LOCATION_CITY])->first()->transsingle->title}}</a></div>
                                </li>
                                {{-- <li>
                                    <div class="sub-txt">{{@collect($fetchLocations[Reports::LOCATION_CITY])->first()->transsingle->title}}</div>
                                </li>
                                <li>
                                    <div class="top-txt">@lang('region.population')</div>
                                    <div class="sub-txt">{{@collect($fetchLocations[Reports::LOCATION_CITY])->first()->transsingle->population}}</div>
                                </li>
                                <li>
                                    <div class="top-txt">Currency</div>
                                    <div class="sub-txt">{{@collect($fetchLocations[Reports::LOCATION_CITY])->first()->country->currencies[0]->transsingle->title}}</div>
                                </li> --}}
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="travlog-main-block">
            <div class="travlog-side">
                <div class="user-top-info">
                    <div class="avatar-wrap">
                        <img class="ava" src="{{check_profile_picture($report->author->profile_picture)}}" alt="{{$report->author->name}}" style='width:70px;height:70px'>
                    </div>
                    <p class="by-name">@lang('other.by') <a href="{{url('profile/'.$report->author->id)}}" class="name-link">{{$report->author->name}}</a>{!! get_exp_icon($report->author) !!}</p>
                </div>

            </div>
            <div class="main-content" style="min-height: 323px;">
                <!-- START SHOW REPORT CONTENT -->

                <!-- start description -->
                <div class="content-wrapper">
                    <div class="quote-block" style="overflow-wrap: break-word;">
                        @if($existing_report->description !='')
                            {!!$existing_report->description!!}
                        @else
                        <p class="no-desc-content">No description available.</p>
                        @endif
                    </div>
                </div>
                <!-- end description -->
                @php $maps_arr = []; @endphp
                @foreach($existing_report->infos AS $ri)

                @if($ri->var==ReportsService::VAR_PLACE)
                
                <?php
                $place = App\Models\Place\Place::find($ri->val);
                $visited_user_count = 0;
                ?>
                @if(is_object($place))
                <!-- start place -->
                <div class="content-wrappers">
                    <div class="travlog-card report-place">
                        <div class="text-block">
                            <div class="title-card">
                                <span class="tag">{{do_placetype($place->place_type)}}</span>
                                <a href="{{url_with_locale('place/'.$place->id)}}" class="place-link" target="_blank">{{$place->transsingle->title}}</a>
                            </div>
                            <p class="report-place-address">{{$place->transsingle->address}}</p>
                            <div class="visited">
                            @if(count($place->reviews))
                                <ul class="foot-avatar-list report-place-avatar-list">
                                    @foreach($place->reviews()->orderBy('created_at', 'desc')->groupBy('by_users_id')->get() as $reviews)
                                        @if($reviews->author)
                                            @php $visited_user_count++; @endphp
                                            @if($visited_user_count < 4)
                                                <li><img class="small-ava" src="{{check_profile_picture($reviews->author->profile_picture)}}" alt="ava"></li>
                                            @endif
                                        @endif
                                    @endforeach
                                </ul>
                                @if($visited_user_count <= 3)
                                    <span>@lang('report.users_have_visited_this_place', ['count' => $visited_user_count])</span>
                                @else
                                    <span>@lang('report.count_others_have_visited_this_place', ['count' => ($visited_user_count-3).'+'])</span>
                                @endif
                            @else
                                <span>@lang('report.users_have_visited_this_place', ['count' => $visited_user_count])</span>
                            @endif
                            </div>
                            <div class="rate-info">
                                <div class="rate-inner report-rate-inner">
                                    <span class="label">{{round(@$place->reviews()->avg('score'), 1)}} <i class="trav-star-icon"></i></span>
                                    <p class="mb-0"><b>@lang('report.excellent')</b></p>
                                    <p class="mb-0">@lang('report.count_review', ['count' => count($place->reviews)])</p>
                                </div>
                                <div class="location report-place-location">
                                    <i class="trav-set-location-icon"></i>
                                    <span>{{@$place->city->transsingle->title}}, {{@$place->country->transsingle->title}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="img-wrap repot-place-img">
                            <img src="{{(isset($place->getMedias[0]->url))?'https://s3.amazonaws.com/travooo-images2/th1100/'.$place->getMedias[0]->url:'http://s3.amazonaws.com/travooo-images2/placeholders/place.png'}}" alt="image">
                        </div>
                    </div>
                </div>
                @endif
                <!-- end place -->

                @elseif($ri->var==ReportsService::VAR_INFO)
                
                <?php
                $inf = @explode(":-", $ri->val);
                @$info_label = $inf[0];
                @$info_text = $inf[1];
                @$info_dest = $inf[3];
                ?>
                <!-- start travooo info -->
                <div class="content-wrappers">
                    <div class="trav-currency-line-block mb-35">
                        <div class="trev-info-name">{!! $info_label !!} {{$info_dest != ''? '- '.$info_dest:''}}</div>
                        <div class="trev-info-desc">
                            {!! $info_text !!}
                        </div>
                    </div>
                </div>
                <!-- end travooo info -->



                @elseif($ri->var==ReportsService::VAR_USER)
                
                @foreach(explode(",", $ri->val) as $r_user)
                <?php
                $user = App\Models\User\User::find($r_user);
                if(Auth::check()){
                    $user_follower = App\Models\UsersFollowers\UsersFollowers::where('users_id', $user->id)
                                ->where('followers_id', Auth::guard('user')->user()->id)
                                ->first();
                }
                
                $id = $user->id;

                $usermedias = $user->my_medias()->pluck('medias_id');

                $photos_list  = App\Models\ActivityMedia\Media::select('id')->where(\DB::raw('RIGHT(url, 4)'), '!=', '.mp4')->whereIn('id', $usermedias)->pluck('id');
                $medias = App\Models\User\UsersMedias::where('users_id', $id)
                        ->whereIn('medias_id', $photos_list)
                        ->orderBy('id', 'DESC')
                        ->limit(3)->get();

                ?>
                <!-- start user -->
                <div class="content-wrappers">
                    <div class="travlog-image-card" style="height:185px;">
                        <div class="card-part img-wrap">
                                <img class="back" src="{{check_cover_photo($user->cover_photo)}}" height="180" alt="image">
                            <div class="avatar-img">
                                <img src="{{check_profile_picture($user->profile_picture)}}" alt="ava" class="ava" style='width:70px;height:70px;'>
                            </div>
                        </div>
                        <div class="card-part info-card">
                            <div class="info-top">
                                <div class="info-txt">
                                    <div class="name"><a href="{{url('profile/'.$user->id)}}" class="user-name-link" target="_blank">{{$user->name}}</a>
                                        {!! get_exp_icon($user) !!}
                                    </div>
                                    <div class="country">{{($user->nationality!='')?@$user->country_of_nationality->transsingle->title:''}}</div>
                                </div>
                                @if(Auth::check() && $user->id != Auth::user()->id)
                                    <div class="btn-wrap">
                                     <button type="button" class="btn btn-light-grey btn-bordered follow-button follow-button_{{$user->id}}" data-user-id="{{$user->id}}" data-route-follow="{{ route('profile.follow', $user->id) }}"  data-route-unfollow="{{ route('profile.unfolllow', $user->id) }}" data-type="{{($user_follower && $user_follower->follow_type == 1)?'unfollow':'follow'}}">
                                        @if($user_follower && $user_follower->follow_type == 1)
                                            @lang('buttons.general.unfollow')
                                        @else
                                            @lang('buttons.general.follow')
                                        @endif
                                    </button>
                                    </div>
                                @endif
                            </div>
                            <ul class="preview-list">
                                @if(count($medias) > 0)
                                    @foreach($medias as $media)
                                        <li><img src="{{get_profile_media($media->media->url)}}" alt="image" style="width:80px;height:80px;"></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
               
                <!-- end user -->
                    @endforeach
                 
                @elseif($ri->var=='plan')
                        @include('site/reports/partials/show_plan')

                @elseif($ri->var==ReportsService::VAR_VIDEO)
                    <?php
                    $vid = explode("|", $ri->val);
                        if(count($vid) > 1){
                            $video_url = 'https://www.youtube.com/embed/'.@$vid[1];
                        }else{
                            $video_url = @$vid[0];
                        }
                    ?>
                    <!-- start video -->
                    <div class="image-block video-block">
                        <iframe width="670" height="520"src="{{@$video_url}}" id="video_iframe" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                    </div>
                <!-- end video -->


                @elseif($ri->var==ReportsService::VAR_LOCAL_VIDEO)
                <!-- start local video -->
                    @php $video_info =explode("-:", $ri->val);  @endphp
                    <div class="image-block video-block">
                        <video style="height: 376px;margin-bottom: 35px;" width="670" class="thumb {{(isset($video_info[1]))?'video-content-img':''}}" controls>
                            <source src="{{$video_info[0]}}" type="video/mp4">
                        </video>

                        @if(isset($video_info[1]) && !isset($video_info[2]))
                            <p class="img-desc">{{$video_info[1]}}</p>
                        @endif

                        @if(isset($video_info[1]) && isset($video_info[2]))
                        <a href="{{$video_info[2]}}" class="img-desc-link" target="_blank">{{$video_info[1]}}</a>
                        @endif
                    </div>
                <!-- end local video -->

                @elseif($ri->var==ReportsService::VAR_TEXT)
                 <!-- start text -->
                    <div class="content-wrappers text-content report-text-content">
                       {!!$ri->val!!}
                   </div>
                <!-- end text -->



                @elseif($ri->var==ReportsService::VAR_MAP)
                 <!-- start map -->
                    <div class="image-block p-rel">
                        <?php
                            $temp = explode("-:", $ri->val);

                            if(count($temp) > 2){
                                $zoom = isset($temp[3])?$temp[3]:10;
                                $maps_arr[] = [
                                    'id'=>'show_map'.$ri->order,
                                    'lat'=>$temp[0],
                                    'lng'=>$temp[1],
                                    'zoom'=>$zoom,
                                    'text'=>$temp[2],
                                ];
                            }
                        ?>
                        <div id="show_map{{$ri->order}}" class="report-map-block"></div>
                        @if(count($temp) > 5)
                            <div class="map-country-info">
                                <div class="country-info">
                                    <div class="country-name-block">
                                        @if(isset($temp[5]) && !empty($temp[5]))
                                            <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower(@$temp[5])}}.png"
                                                 alt="flag">
                                        @endif
                                        <div>
                                            <p class="map-country-title">{{$temp[4]}}</p>
                                            <p class="map-country-desc"></p>
                                        </div>
                                    </div>
                                    @if(isset($temp[6]) && !empty($temp[6]))
                                        <div class="country-population">
                                            <div class="user-icon">
                                                <i class="trav-popularity-icon"></i>
                                            </div>
                                            <div class="pop-info">
                                                <p class="pop-count">{{@$temp[6]}}</p>
                                                <p class="pop-title">Population</p>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="country-follow">
                                    <button class="btn btn-light-grey btn-bordered follow-country-btn follow-info-{{@$temp[7]}}" data-fol-type="{{get_country_follow(@$temp[7])?'unfollow':'follow'}}" data-country-id="{{@$temp[7]}}">{{get_country_follow(@$temp[7])?'Unfollow':'Follow'}}</button>
                                </div>
                            </div>
                        @endif
                    </div>
                <!-- end map -->


                @elseif($ri->var==ReportsService::VAR_PHOTO)
                <!-- start image -->
                    @php $image_info =explode("-:",$ri->val)  @endphp 
                    <div class="image-block">
                        <a href="{{$image_info[0]}}" data-lightbox="report__media{{$loop->index}}">
                            <img src="{{$image_info[0]}}" alt="image" class="{{(isset($image_info[1]))?'photo-content-img':''}} rep-lazy" style='width:670px;height:auto;'>
                        </a>
                        @if(isset($image_info[1]) && !isset($image_info[2]))
                            <p class="img-desc">{{$image_info[1]}}</p>
                        @endif

                        @if(isset($image_info[1]) && isset($image_info[2]))
                        <a href="{{$image_info[2]}}" class="img-desc-link" target="_blank">{{$image_info[1]}}</a>
                        @endif
                    </div>
                <!-- end image -->

                @elseif($ri->var==ReportsService::VAR_COVER)
                @endif
                @endforeach

                  <div class="content-wrappers">
                      <div class="post-top-info-layer post-footer-info foot-place">
                        <div class="info-side report-info-side">
                            <div class="post-foot-block post-reaction mr-5">
                                <a href="javascript:;" class="top-action {{Auth::check()?'like-button':'open-login'}}" data-id="{{$report->id}}" dir="auto">
                                                                        <i class="{{(Auth::check() && $report->likes()->where('users_id', Auth::user()->id)->first())?'trav-heart-fill-icon':'trav-heart-icon'}}" dir="auto" style="color: rgb(255, 90, 121);"></i>
                                                                </a>
                                <a href="#" class="{{Auth::check()?'report_likes_modal' : 'open-login'}}" id='{{$report->id}}'>
                                    <span class="report-like-count"><b>{{$totalLikes}}</b> Likes</span>
                               </a>
                            </div>
                            <span class="comment-dot report-com-dot mr-5"> · </span>
                            <div class="post-foot-block report-comment-cnt">
                                <i class="trav-comment-icon"></i>
                                @if($totalComments)
                                <ul class="foot-avatar-list report-av-list">
                                    <?php $us = array(); ?>
                                    @foreach($report->comments()->orderBy('created_at', 'desc')->get() AS $repc)
                                        @if(!in_array($repc->users_id, $us))
                                        <?php $us[] = $repc->users_id; ?>
                                            @if(count($us)<4)
                                                <li><img class="small-avat" src="{{check_profile_picture($repc->author->profile_picture)}}" alt="{{$repc->author->name}}" title="{{$repc->author->name}}" width='20px' height='20px'></li>
                                            @endif
                                        @endif
                                    @endforeach
                                </ul>
                                @endif
                                 <span> <strong class="{{$report->id}}-all-comments-count">{{$totalComments}}</strong> <b>@lang('comment.comments')</b></span>

                            </div>
                            </div>
                          <div class="actions-side">
                              @if(Auth::check())
                                  <a href="#shareablePost" class="top-action" data-uniqueurl="{{url('reports', $report->id)}}" data-toggle="modal" data-id="{{ $report->id}}" data-type="report" dir="auto">
                              @else
                                  <a href="javascript:;" class="top-action open-login">
                              @endif
                                      <i class="trav-share-icon"></i>
                                      <span>@lang('report.share')</span>
                                  </a>

                              @if(Auth::check())
                                    <a href="#" class="top-action" data-toggle="modal" data-target="#embedPopup">
                              @else
                                    <a href="javascript:;" class="top-action  open-login">
                              @endif
                                    <i class="trav-save-icon"></i>
                                    <span>Embed</span>
                                </a>
                          </div>
                      </div>
                    <div class="foot-author-info">
                        <div class="info-block">
                            <div class="img-wrap">
                                <img class="ava" src="{{check_profile_picture($report->author->profile_picture)}}" alt="{{$report->author->name}}" style="width:50px;height:50px;">
                            </div>
                            <div class="info-txt">
                                <div class="name">
                                    <span>@lang('other.by') </span>
                                    <b>
                                        <a target="_blank" href="{{url('profile/'.$report->author->id)}}">
                                            {{$report->author->name}}
                                        </a>
                                    </b>
                                    {!! get_exp_icon($report->author) !!}
                                </div>
                                  @if(count($expertise)>0)
                                 <div class="location">
                                   <span>@lang('report.expert_in') </span>
                                    @foreach($expertise as $k=>$exp)
                                        @if($k<3)
                                        <a href="{{$redirect_link}}" target="_blank"><b>{{$exp['text']}}</b></a>
                                        @endif
                                        {{($k < 2 && $k+1 < count($expertise))?',':''}}
                                        {{($k == 3)?'...':''}}
                                    @endforeach
                                </div>
                                @endif
                            </div>
                        </div>
                        @if(Auth::check() && $report->author->id != Auth::user()->id)
                        <div class="btn-wrap">
                            <button type="button" class="btn {{($follower && $follower->follow_type == 1)?'btn-light-grey':'btn-blue-follow'}} btn-bordered follow-button follow-button_{{$report->author->id}}" data-user-id="{{$report->author->id}}" data-route-follow="{{ route('profile.follow', $report->author->id) }}"  data-route-unfollow="{{ route('profile.unfolllow', $report->author->id) }}" data-type="{{($follower && $follower->follow_type == 1)?'unfollow':'follow'}}">
                                @if($follower && $follower->follow_type == 1)
                                    @lang('buttons.general.unfollow')
                                @else
                                    @lang('buttons.general.follow')
                                @endif
                            </button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="post-comment-layer-wrap">
                <div class="post-comment-layer report-comment-content">
                    @if(Auth::guard('user')->user())
                    <div class="post-add-comment-block">
                        <div class="avatar-wrap">
                            <img src="{{check_profile_picture(Auth::guard('user')->user()->profile_picture)}}" alt="" width="45px" height="45px">
                        </div>
                        <div class="post-add-com-inputs">
                            <form class="reportCommentForm" method="POST" data-id="{{$report->id}}"  data-type="2" autocomplete="off" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" data-id="pair{{$report->id}}" name="pair" value="<?php echo uniqid(); ?>"/>
                                <div class="post-create-block reports-comment-block" tabindex="0">
                                    <div class="post-create-input report-create-input w-530">
                                        <textarea name="text" data-id="text" id="report_comment_text" class="textarea-customize report-comment-textarea report-comment-emoji"
                                                    placeholder="@lang('comment.write_a_comment')"></textarea>
                                    </div>
                                    <div class="post-create-controls d-none">
                                        <div class="post-alloptions">
                                            <ul class="create-link-list">
                                                <li class="post-options">
                                                    <input type="file" name="file[]" class="report-comment-media-input" data-report_id="{{$report->id}}" data-id="commentfile{{$report->id}}" style="display:none">
                                                    <i class="fa fa-camera click-target" aria-hidden="true" data-target="commentfile{{$report->id}}"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="comment-edit-action">
                                            <a href="javascript:;" class="report-comment-cancel-link">Cancel</a>
                                            <a href="javascript:;" class="post-report-comment-link report-comment-link">Post</a>
                                        </div>
                                    </div>
                                      <div class="medias report-media">
                                          <div class="spinner-border rep-spinner-border" role="status" style="display:none"><span class="sr-only">Loading...</span></div>
                                        </div>
                                </div>
                                <input type="hidden" name="report_id" value="{{$report->id}}"/>
                                <button type="submit" class="btn btn-primary d-none"></button>
                                <div class="rep-media-error-content d-none">You can upload only image file.</div>
                            </form>
                        </div>
                    </div>
                    @endif

                    <div class="post-comment-top-info" id="{{$report->id}}" {{$totalComments>0?"":'style=display:none'}} >
                        <ul class="comment-filter">
                            <li class="comment-filter-type" data-type="top" data-repid="{{$report->id}}" data-comment-type="1">@lang('other.top')</li>
                            <li class="comment-filter-type active" data-type="new" data-repid="{{$report->id}}" data-comment-type="1">@lang('other.new')</li>
                        </ul>
                        <div class="comm-count-info">
                            <span class="{{$report->id}}-loading-count"><strong>5</strong></span> / <span class="{{$report->id}}-all-comments-count">{{$totalComments}}</span>
                        </div>
                    </div>
                    <div class="post-comment-wrapper sortBody report-comment-wraper" id="{{$report->id}}">
                        @if($totalComments>0)
                        <div class="report-comment-main">
                            @foreach($report->comments()->orderBy('created_at', 'desc')->take(5)->get() AS $comment)
                                @include('site.reports.partials.report_comment_block2')
                            @endforeach
                        </div>
                        <div id="loadMore" class="loadMore" reportId="{{$report->id}}" style="{{$totalComments< 6?'display:none;':''}}">
                            <input type="hidden" id="pagenum" value="1">
                            <div id="reportloader" class="post-animation-content post-comment-row">
                                <div class="report-com-avatar-wrap">
                                    <div class="post-top-avatar-wrap animation post-animation-avatar"></div>
                                </div>
                                <div class="post-comment-text">
                                    <div class="report-top-name animation"></div>
                                    <div class="report-com-info animation"></div>
                                    <div class="report-com-sec-info animation"></div>
                                    <div id="hide_loader" class="report-com-thr-info animation"></div>
                                </div>
                            </div>
                        </div>
                        @else
                            <div class="post-comment-top-info comment-not-fund-info" id="{{$report->id}}">
                                <ul class="comment-filter">
                                    <li><span class="rep-not-comment-text">No comments yet ...</span></li>
                                    <li></li>
                                </ul>
                                <div class="comm-count-info">
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="more-travlog-block">
        <h3 class="title">More Travelogs</h3>
        <div class="travlog-list">
            @php
                $timezone = getLocalTimeZone();
            @endphp
            @foreach($more_reports AS $mr)
            <div class="travlog-info-block">
                <div class="travlog-info-inner">
                    <div class="img-wrap">
                        <a href="{{url('reports/'.$mr->id)}}">
                            <img src="@if(isset($mr->cover[0])){{$mr->cover[0]->val}}@else {{asset('assets2/image/placeholders/no-photo.png')}}@endif" alt="{{$mr->title}}" style='width:331px;height:190px;'>
                        </a>
                        </div>
                    <div class="info-wrapper">
                        <div class="country-name">
                            @php 
                                $num_output = 0; 
                                $tempLocs =get_report_locations_info($mr->id, 'publish');
                            @endphp
                            @foreach($tempLocs as $locations)
                                 @if($num_output < 2)
                                 <a style="color:#4080ff;" href="{{$locations['link']}}" target="_blank">{{$locations['title']}}</a>@if(!$loop->last && $num_output != 1) , @endif
                                @elseif($num_output == 2)
                                    <a href="javascript:;" class="form-link show-more-location text-truncate">+ {{count($tempLocs)-2}} More</a>
                                    <span class="more-location-content d-none">
                                    , <a style="color:#4080ff;" href="{{$locations['link']}}" target="_blank">{{$locations['title']}}</a>@if(!$loop->last) , @endif
                                    </span>
                                @else
                                    <span class="more-location-content d-none">
                                        <a style="color:#4080ff;" href="{{$locations['link']}}" target="_blank">{{$locations['title']}}</a>@if(!$loop->last) , @endif
                                    </span>
                                @endif
                                @php $num_output ++;@endphp
                            @endforeach
                        </div>
                        <div class="info-title">
                            <a href="{{url('reports/'.$mr->id)}}">
                                {{$mr->title}}
                            </a>
                        </div>
                        <div class="foot-block">
                            <div class="author-side">
                                <div class="img-wrap">
                                    <img class="ava" src="{{check_profile_picture($mr->author->profile_picture)}}" alt="avatar" style='width:45px;height:45px'>
                                </div>
                                <div class="author-info">
                                    <div class="author-name">
                                        <a href="{{url('profile/'.$mr->author->id)}}"
                                           class="author-link">{{$mr->author->name}}</a>
                                        {!! get_exp_icon($mr->author) !!}
                                    </div>
                                    <div class="time">
                                        {!! moreTravlogDateFormat($mr->published_date, $timezone)!!}
                                    </div>
                                </div>
                            </div>
                            <div class="like-side">
                                <div class="post-foot-block">
                                    <i class="trav-heart-fill-icon"></i>&nbsp;
                                    <span>{{count($mr->likes)}}</span>
                                </div>
                                <div class="post-foot-block">
                                    <i class="trav-comment-icon"></i>&nbsp;
                                    <span>{{count($mr->comments)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection

@section('before_scripts')
@if(Auth::check())
    @include('site/reports/partials/modal-create-report-step-1')
    @include('site/reports/partials/modal-create-report-step-2')
    @include('site/reports/partials/modal-embed')
    @include('site/reports/partials/modal-add-trip-plan')
    @include('site/reports/partials/modal-add-info')
    @include('site/home/partials/modal_comments_like')
    @include('site/reports/partials/modal-countries_list')
    @include('site.reports.partials.modal_report_likes')
    @include('site.home.partials._spam_dialog')
    @include('site.home.new.partials._share-modal')
  @endif
@endsection



@section('after_scripts')

<script data-cfasync="false" type="text/javascript">
//----START---- comments scripts
var order_type = 'new';

$(document).ready(function(){
    document.emojiSource = "{{ asset('plugins/summernote-master/tam-emoji/img') }}";
    document.textcompleteUrl = "{{ url('reports/get_search_info') }}";
    CommentEmoji($('.reportCommentForm'))
     
    $('div.report-alert').delay(3000).slideUp(300);

    if($('.{{$report->id}}-all-comments-count').html() <=5){
        $('.{{$report->id}}-loading-count strong').html($('.{{$report->id}}-all-comments-count').html())
    }
    
    $(document).on('click', '.report-comment-cancel-link', function(){
        var form = $(this).closest('form');
        form.find('.post-create-controls').addClass('d-none')
        form.find('.emojionearea-button').addClass('d-none');
        form.find('textarea').val('');
        form.find(".note-editable").html('');
        form.find(".note-placeholder").addClass('custom-placeholder')
        form.find('.medias').find('.removeFile').each(function(){
                      $(this).click();
                  });
        form.find('.medias').empty();
        form.find('.rep-media-error-content').addClass('d-none')

    })

    //order report comments
    $(document).on('click', '.comment-filter-type', function(){
        $('.comment-filter').find('.comment-filter-type').removeClass('active')
        $(this).addClass('active')

        order_type = $(this).data('type');
        var report_id = $(this).data('repid');

        var all_comment_count = $('.'+ report_id +'-all-comments-count').html();

        if(all_comment_count > 5){
            $('.'+ report_id + '-loading-count strong').html(0)
            $(".loadMore").show();
        }else{
            $('.'+ report_id + '-loading-count strong').html(all_comment_count)
        }

        $('#pagenum').val(0);
        $('.report-comment-main').html('');

        loadMoreComment();

    })

    $(document).on('click', '#commentsPopupBtn', function(){
        $('#commentsPopup').modal('show')
    })

    $(document).on('click', '.report-comment-cnt', function(){
       $('html, body').animate({ scrollTop:$('.post-comment-layer-wrap').position().top + 600}, 400);
    })

    //load more comment
    $(".report-comment-wraper").on( 'scroll', function(){

    if($('.{{$report->id}}-loading-count strong').html() == $('.{{$report->id}}-all-comments-count').html()){
        $('.loadMore').hide();
    }

    if(visibleY(document.getElementById('hide_loader'))){
        loadMoreComment();
    }


    });

    //load more comment like user
    $(".comment-like-people-block").on( 'scroll', function(){

        if(visibleY(document.getElementById('hide_comlike_loader'))){
            loadMoreCommentLikeUsers();
        }
    });

    //load more comment like user
    $(".report-like-users").on( 'scroll', function(){
        if(visibleY(document.getElementById('hide_replike_loader'))){
            loadMoreReportLikeUsers();
        }
    });
    
    //load more plan
     $(".add-report-plan-inner").scroll( 'scroll', function(){
        if(visibleY(document.getElementById('hide_repPlan_loader'))){
            loadMorePlan();
        }
    });
    // Enable edit button
    setTimeout(function() { 
        $('#editReport').removeAttr('disabled');
    }, 2000);
})

 // show countries/cities list modal
$(document).on('click', ".more-location-modal", function(){
    var report_id = $(this).attr('data-report-id')
    $('.location-list-modal-block').html('')
        $.ajax({
        method: "POST",
        url: "{{url('reports/ajaxGetLocationList')}}",
        data: {'id':report_id},
        async: false
            }).done(function(data){
                var result = JSON.parse(data);
                $.each(result, function (i, val) {
                    if(val.country_id !=''){
                        if($('.location-list-modal-block').find('.country-'+val.country_id).length>0){
                            $('.location-list-modal-block').find('.country-'+val.country_id).append('<div class="location-city-item">'+
                                                                                                        '<span class="loc-city-name selected"><img class="location-img" src="'+ val.image +'" alt="flag"> <a style="color:whilte" href="/city/'+val.id+'">'+ val.title +'</a></span><span class="loc-type">City</span>'+
                                                                                                    '</div>')
                        }else{
                            $('.location-list-modal-block').append('</div>'+
                                        '<div class="selected-locetion-item country-'+ val.country_id +'">'+
                                            '<div class="country-item" id="'+ val.country_id +'-country">'+
                                                '<span class="loc-country-name"><img class="location-img" src="'+ val.country_image +'" alt="flag"> <a style="color:whilte" href="/country/'+val.country_id+'">'+ val.country_name +'</a></span><span class="loc-type">Country</span>'+
                                            '</div>'+
                                            '<div class="location-city-item">'+
                                                '<span class="loc-city-name selected"><img class="location-img" src="'+ val.image +'" alt="flag"> <a style="color:whilte" href="/city/'+val.id+'">'+ val.title +'</a></span><span class="loc-type">City</span>'+
                                            '</div>'+
                                        '</div>')
                        }
                    }else{
                        if($('.location-list-modal-block').find('.country-'+val.id).length>0){
                            $('.location-list-modal-block').find('.country-'+val.id).find('.loc-country-name').addClass('selected')
                        }else{
                            $('.location-list-modal-block').append('<div class="selected-locetion-item country-'+ val.id +'">'+
                                               '<div class="country-item">'+
                                                   '<span class="loc-country-name selected"><img class="location-img" src="'+ val.image +'" alt="flag">  <a style="color:whilte" href="/country/'+val.id+'">'+ val.title +'</a></span><span class="loc-type">Country</span>'+
                                               '</div>'+
                                           '</div>')
                       }
                    }
                });
                
                 $('#reportCountryList').modal('show')
            })
 
});

 // More/less content
$(document).on('click', ".read-more-link", function(){
  $(this).closest('.comment-txt').find('.less-content').hide()
  $(this).closest('.comment-txt').find('.more-content').show()
  $(this).hide()
});

$(document).on('click', ".read-less-link", function(){
  $(this).closest('.more-content').hide()
  $(this).closest('.comment-txt').find('.less-content').show()
  $(this).closest('.comment-txt').find('.read-more-link').show()
});

  //Report comment like
        $('body').on('click', '.report_comment_like', function (e) {
            var _this = $(this);
            commentId = $(this).attr('id');
            $.ajax({
                method: "POST",
                url: "{{ route('report.commentlike') }}",
                data: {comment_id: commentId}
            })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.status == 'yes') {
                        _this.find('i').addClass('fill');
                    } else if (result.status == 'no') {
                        _this.find('i').removeClass('fill');
                        _this.parent().find('.comment-likes-block span').each(function () {
                        if ($(this).text() == result.name)
                            $(this).remove();
                        });
                    }
                    _this.parent().find('.'+ commentId +'-comment-like-count').html(result.count);

                });
            e.preventDefault();
        });


        $('body').on('click', '.report-comment-likes-block', function (e) {
            var _this = $(this);
            commentId = $(this).attr('data-id');
            var like_user_count = $('.'+commentId +'-comment-like-count').html();

            $.ajax({
                method: "POST",
                url: "{{ route('report.commentlikeusers') }}",
                data: {comment_id: commentId}
            })
                    .done(function (res) {
                        $('.post-comment-like-users').html(res)
                        if(like_user_count > 7){
                            $('#loadMoreUsers').removeClass('d-none');
                            $('#loadMoreUsers').attr('commentId', commentId);
                        }
                        $('#commentslikePopup').find('.comment-like-count').html(like_user_count+ ' Likes')
                        $('#commentslikePopup').modal('show')
                    });
            e.preventDefault();
        });

        $('body').on('mouseover', '.news-feed-comment', function () {
            $(this).find('.post-com-top-action .dropdown').show()
        });

        $('body').on('mouseout', '.news-feed-comment', function () {
            $(this).find('.post-com-top-action .dropdown').hide()
        });

        $('body').on('mouseover', '.doublecomment', function () {
            $(this).find('.post-com-top-action .dropdown').show()
        });

        $('body').on('mouseout', '.doublecomment', function () {
            $(this).find('.post-com-top-action .dropdown').hide()
        });


        $('body').on('click', '.remove-media-comment-file', function (e) {
            var media_id = $(this).attr('data-media_id');
            $(this).closest('.medias').append('<input type="hidden" name="existing_medias[]" value="' + media_id + '">')
            $(this).parent().remove();
        });



        $('body').on('click', '.post-report-comment-link', function (e) {
            $(this).closest('.reportCommentForm').find('button[type=submit]').click();
        });

    $('body').on('submit', '.reportCommentForm', function (e) {
        e.preventDefault();
        var form = $(this);
        var type = form.data('type');
        var report_id = form.attr('data-id');
        var values = form.serialize();
        var comment_text = form.find('textarea').val().trim();
        var all_comment_count = $('.'+ report_id +'-all-comments-count').html();
        var loading_count =  $('.'+ report_id +'-loading-count strong').html();
        var files = form.find('.medias').find('div.img-wrap-newsfeed').length;
        form.find('.rep-media-error-content').addClass('d-none')

        if(comment_text == '' && files == 0){
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        
        form.find('.report-comment-link').hide()

        $.ajax({
            method: "POST",
            url: "{{url('reports/comment')}}",
            data: values
        })
            .done(function (res) {
             form.find('.medias').find('.removeFile').each(function(){
                        $(this).click();
                    });
            form.find('.medias').empty();
            form.find(".note-editable").html('');
            form.find('textarea').val('')
             form.find('.report-comment-link').show()
            form.find(".note-placeholder").addClass('custom-placeholder')
            
            form.find('.post-create-controls').addClass('d-none');
            form.find('.emojionearea-button').addClass('d-none');

            $("#" + report_id + ".comment-not-fund-info").remove();
            $("#" + report_id + ".post-comment-top-info").show();
            $('.'+ report_id +'-all-comments-count').html(parseInt(all_comment_count) + 1);
            $('.rep-embed-comment-count').html(parseInt(all_comment_count) + 1);
            $('.'+ report_id +'-loading-count strong').html((parseInt(loading_count)<5)?parseInt(loading_count) + 1:parseInt(loading_count));
                if(all_comment_count == 0){
                    $("#" + report_id + ".post-comment-wrapper").prepend('<div class="report-comment-main">'+res+'</div>').fadeIn('slow');
                }else{
                    $(".report-comment-main").prepend(res).fadeIn('slow');
                }
            });

        });



    // Edit comment
    $('body').on('click', '.report-edit-comment', function (e) {
        var _this = $(this);

        var commentId = _this.attr('data-id');
        editReportCommentMedia(commentId)
        var postId = _this.attr('data-report');
        _this.closest('.post-comment-text').find('.commentEditForm' + commentId).removeClass('d-none')
        var text_obj = _this.closest('.post-comment-text').find('.comment-text-' + commentId + ' p')
        text_obj.addClass('d-none')
        _this.closest('.post-comment-text').find('.commentEditForm' + commentId + ' textarea.textarea-customize').val(text_obj.html())

        CommentEmoji($('.commentEditForm' + commentId));
        $($('.commentEditForm' + commentId)).find(".report-comment-emoji").summernote("code", text_obj.html());

        $('.commentEditForm' + commentId).find(".note-placeholder").removeClass('custom-placeholder')
        e.preventDefault();
    });

    $('body').on('click', '.edit-cancel-link', function (e) {
        var _this = $(this);

        var commentId = _this.attr('data-comment_id');
        $('.commentEditForm' + commentId).addClass('d-none')
        $('.comment-text-' + commentId + ' p').removeClass('d-none')

        $('.commentEditForm' + commentId).find('.rep-media-error-content').addClass('d-none')

        e.preventDefault();
    });


    $(document).on('change', '.report-comment-media-input', function (e) {
      var report_id = $(this).attr('data-report_id')
           $(this).closest('form').find('.medias').find('.removeFile').each(function(){
                        $(this).click();
                    });
          commentMediaUploadFile($(this), $(this).closest(".reportCommentForm"), report_id);

    })


    $(document).on('change', '.report-modal-media-input', function (e) {
      var report_id = $(this).attr('data-report_id')
            $(this).closest('form').find('.medias').find('.removeFile').each(function(){
                        $(this).click();
                    });
          commentMediaUploadFile($(this), $(this).closest(".reportCommentForm"), report_id);

    })


    $('body').on('click', '.edit-report-comment-link', function (e) {
        var _this = $(this);
        var commentId = _this.attr('data-comment_id');
        var form = _this.closest('.commentEditForm' + commentId);

        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div.img-wrap-newsfeed').length;
        var comment_type = form.find("input[name='comment_type']").val();

        form.find('.rep-media-error-content').addClass('d-none')

        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        form.find('.report-comment-link').hide()
        var values = form.serialize();
        $.ajax({
            method: "POST",
            url: "{{ route('report.commentedit') }}",
            data: values
        })
                .done(function (res) {
                    form.find('.medias').find('.removeFile').each(function () {
                        $(this).click();
                    });
                    form.find('.report-comment-link').show()
                    if (comment_type == 1) {
                        $('.commentRow' + commentId).parent().html(res);
                    } else {
                        $('.commentRow' + commentId).replaceWith(res);
                    }
                });
        e.preventDefault();
    });

    var check_comment_id;
    $('body').on('click', '.reportCommentReply', function (e) {
         commentId = $(this).attr('id');
          $('.'+ commentId +'-comment-reply-block').toggle();
         $('.replyForm' + commentId).show();

        if ( $('.'+ commentId +'-comment-reply-block').is(':visible')) {
            $('.report-comment-wraper').animate({ scrollTop: - $('.commentRow' + commentId).parent().parent().position().top + $('.replyForm' + commentId).position().top-200}, 400);
        }

        if (commentId != check_comment_id) {
            check_comment_id = commentId;
            CommentEmoji($('.replyForm' + commentId))
        }

         e.preventDefault();
     });

    // Reply comment
     $(document).on('change', '.report-comment-reply-media-input', function (e) {
      var comment_id = $(this).attr('data-comment-id')
       $(this).closest('form').find('.medias').find('.removeFile').each(function(){
                        $(this).click();
                    });
          commentMediaUploadFile($(this), $(this).closest(".commentReplyForm"+comment_id), comment_id);

    })

     $(document).on('change', '.report-modal-reply-media-input', function (e) {
      var comment_id = $(this).attr('data-comment-id')
       $(this).closest('form').find('.medias').find('.removeFile').each(function(){
                        $(this).click();
                    });
          commentMediaUploadFile($(this), $(this).closest(".commentModalReplyForm"+comment_id), comment_id);

    })

      $('body').on('click', '.report-reply-link', function(e){
                var comment_id = $(this).attr('data-id');
                var form = $(this).closest('.commentReplyForm' + comment_id);
                replyReportComment(form, comment_id);
                $(this).val('')
        });


    function replyReportComment(form, parent_id) {

        var text = form.find('textarea').val().trim();
        var files = form.find('.medias').find('div.img-wrap-newsfeed').length;
        var replies_count = $('.'+ parent_id+'-rep-reply span').text();
        var reply_int_count = (replies_count !='')?parseInt(replies_count):0;
        form.find('.rep-media-error-content').addClass('d-none')

      
        if (text == "" && files == 0)
        {
            $.alert({
                icon: 'fa fa-warning',
                title: '',
                type: 'orange',
                offsetTop: 0,
                offsetBottom: 500,
                content: 'Please input text or select files!',
            });
            return false;
        }
        var values = form.serialize();
        form.find('.report-comment-link').hide()
        $.ajax({
            method: "POST",
            url: "{{url('reports/comment')}}",
            data: values
        })
                .done(function (res) {
                    form.parent().parent().before(res);
                    form.find('.medias').find('.removeFile').each(function () {
                        $(this).click();
                    });
                    form.find('.report-comment-link').show()
                    form.find('textarea').val('');
                    form.find(".note-editable").html('');
                    form.find(".note-placeholder").addClass('custom-placeholder')
                    form.find('.post-create-controls').addClass('d-none');
                    form.find('.emojionearea-button').addClass('d-none');

                    form.find('.medias').empty();
                     $('.'+ parent_id+'-rep-reply').html('<span>'+ (reply_int_count+1) +'</span> Replies')

                });
    }

    //delete comment
    $('body').on('click', '.report-comment-delete', function (e) {
        var _this = $(this);
        var comment_id = _this.attr('id');
        var report_id = _this.attr('reportid');
        var allReportCommentCount = $('.'+ report_id +'-all-comments-count').html();
        var reload_obj = _this.closest('.report-comment-content');
        var type = _this.attr('data-del-type');
        var parent_id = _this.data('parent');
        var loading_count =  $('.'+ report_id +'-loading-count strong').html();
        var replies_count = $('.'+ parent_id+'-rep-reply span').text();
       
        $.confirm({
        title: 'Delete Comment!',
        content: 'Are you sure you want to delete this comment? <div class="mb-3"></div>',
        columnClass: 'col-md-5 col-md-offset-5',
        closeIcon: true,
        offsetTop: 0,
        offsetBottom: 500,
        scrollToPreviousElement:false,
        scrollToPreviousElementAnimate:false,
        buttons: {
            cancel: function () {},
            confirm: {
                text: 'Confirm',
                btnClass: 'btn-danger',
                keys: ['enter', 'shift'],
                action: function(){
                    $.ajax({
                        method: "POST",
                        url: "{{ route('report.commentdelete') }}",
                        data: {comment_id: comment_id}
                    })
                        .done(function (res) {
                            if(res){
                                $('.commentRow' + comment_id).fadeOut();
                                if((parseInt(allReportCommentCount) - 1) == 0){
                                    $("#" + report_id + ".post-comment-top-info").hide();
                                    $("#" + report_id + ".post-comment-wrapper").append('<div class="post-comment-top-info comment-not-fund-info" id="'+ report_id +'">'+
                                                                                    '<ul class="comment-filter">'+
                                                                                        '<li><span class="rep-not-comment-text">No comments yet ...</span></li><li></li>'+
                                                                                    '</ul>'+
                                                                                    '<div class="comm-count-info"></div></div>')
                                }

                                $('.'+ report_id +'-all-comments-count').html(parseInt(allReportCommentCount) - 1)
                                $('.'+ report_id +'-loading-count strong').html(parseInt(loading_count) - 1);
                                loadMoreComment();
                                if (type == 1) {
                                    $('.commentRow' + comment_id).parent().remove();

                                } else {
                                    if (_this.closest('.report-comment-content').find('*[data-parent_id="' + parent_id + '"]:visible').length == 1) {
                                        $('.commentRow' + parent_id).find('.report-comment-delete').removeClass('d-none')
                                        $('.commentRow' + parent_id).find('.delete-line').removeClass('d-none')
                                    }
                                    
                                    if(replies_count == 1){
                                        $('.'+ parent_id+'-rep-reply').html('<span></span> Reply')
                                    }else{
                                        console.log('rep', parent_id)
                                         $('.'+ parent_id+'-rep-reply').html('<span>'+ (parseInt(replies_count)-1) +'</span> Replies')
                                    }
                                }
                            }
                    });
                }
            }
        }
    });
            e.preventDefault();
    });



//----END---- comments scripts

    // Dropdown not be closed when we click on an collapse link
$('body').on('click', 'a[data-toggle="collapse"]', function (event) {
  event.stopPropagation();
   $($(this).attr('href')).collapse('toggle');
})

    //Add Report like
    $('body').on('click', '.like-button', function(){
        var _this = $(this);
        var report_id = $(this).data('id');
        var like_btn = $('.like-button')

        $.ajax({
                method: "POST",
                url: "{{ route('report.like') }}",
                data: {post_id: report_id}
            })
            .done(function (res) {
                var result = JSON.parse(res);
                if (result.status == 'yes') {
                    like_btn.find('i').attr('class', 'trav-heart-fill-icon')
                    like_btn.find('i').css('color', '#ff5a79')

                } else if (result.status == 'no') {
                    like_btn.find('i').attr('class', 'trav-heart-icon')
                    like_btn.find('i').removeAttr('style')
                }
                $('.report-like-count b').html(result.count);
                $('.rep-embed-like-count').html(result.count);
            });
    })


    //Shere pravicy script
    $('body').on("click", ".dropdown-menu li a", function(){
        var privacy_type = $(this).attr('data-pravicytype');
        $('.rep-permission').val(privacy_type)
        
       switch(privacy_type) {
            case '1':
              $(".rep-permission-btn").html('<i class="trav-user-plus-icon"></i>');
              break;
            case '2':
              $(".rep-permission-btn").html('<i class="fa fa-user-o" aria-hidden="true"></i>');
              break;
            default:
              $(".rep-permission-btn").html('<i class="trav-earth"></i>');
          }
   });



$('#shareablePost .share-modal-header  h4').html(' Share this report')
$('#shareablePost #dropdownMenuButton').css('display', 'none')

        var getWindowOptions = function() {
            var width = 500;
            var height = 350;
            var left = (window.innerWidth / 2) - (width / 2);
            var top = (window.innerHeight / 2) - (height / 2);

            return [
              'resizable,scrollbars,status',
              'height=' + height,
              'width=' + width,
              'left=' + left,
              'top=' + top,
            ].join();
          };



        $(document).on('click', 'i.fa-camera', function () {
            $("#createPosTxt").click();
            var target = $(this).attr('data-target');
            var target_el = $(this).parent().find('*[data-id="'+target+'"]');
            target_el.trigger('click');
        })


        function copyMe() {
        var copyText = document.getElementById("copy-input");
        copyText.select();
        document.execCommand("copy");
        document.getElementById('copy-btn').innerHTML = '<i class="fa fa-copy"></i> Copied!'
      }


       function editReportCommentMedia(id) {
        $('.commenteditfile' + id).on('change', function () {
            $(this).closest('form').find('.medias').find('.removeFile').each(function(){
                  $(this).click();
              });
            $(this).closest('form').find('.medias').find('.remove-media-comment-file').each(function(){
                  $(this).click();
              });
            commentMediaUploadFile($(this), $(".commentEditForm" + id), id);
        });
    }

    //load more comment
    function loadMoreComment(){
        var nextPage = parseInt($('#pagenum').val()) + 1;
        var report_id = $('.loadMore').attr('reportId');
        var all_comment_count = $('.'+ report_id +'-all-comments-count').html();
        var load_count = $('.'+ report_id + '-loading-count strong').html();

        $.ajax({
            type: 'POST',
            url: "{{route('report.load_more_comment')}}",
            data: {pagenum: nextPage, report_id: report_id, order: order_type},
            async: false,
            success: function (data) {
                if(data != ''){
                      $('.report-comment-main').append(data);
                      var append_count = parseInt(load_count) + 5;
                      if(append_count < parseInt(all_comment_count)){
                          $('.'+ report_id + '-loading-count strong').html(append_count)
                      }else{
                           $('.'+ report_id + '-loading-count strong').html(parseInt(all_comment_count))
                      }
                      $('#pagenum').val(nextPage);
                  } else {
                      $(".loadMore").hide();
                  }
            }
        });
    }

    //load more comment like users
    function loadMoreCommentLikeUsers(){
        var nextPage = parseInt($('#like_pagenum').val()) + 1;
        var comment_id = $('#loadMoreUsers').attr('commentId');

        $.ajax({
            type: 'POST',
            url: "{{route('report.load_more_comment_like_users')}}",
            data: {pagenum: nextPage, comment_id: comment_id},
            async: false,
            success: function (data) {
                if(data != ''){
                      $('.post-comment-like-users').append(data);
                      $('#like_pagenum').val(nextPage);
                  } else {
                      $("#loadMoreUsers").addClass('d-none');
                  }
            }
        });
    }

    //load more report like users
    function loadMoreReportLikeUsers(){
        var nextPage = parseInt($('#report_like_pagenum').val()) + 1;
        var report_id = $('#loadMoreLikeUsers').attr('reportId');
       
        $.ajax({
            type: 'POST',
            url: "{{route('report.load_more_report_like_users')}}",
            data: {pagenum: nextPage, report_id: report_id},
            async: false,
            success: function (data) {
                if(data != ''){
                      var result = JSON.parse(data);
                      $('.report-like-modal-block').append(result);
                      $('#report_like_pagenum').val(nextPage);
                  } else {
                      $("#loadMoreLikeUsers").addClass('d-none');
                  }
            }
        });
    }
    
    
    //Comment summernote emoji
    function CommentEmoji(element){
         element.find(".report-comment-emoji").summernote({
            airMode: false,
            focus: false,
            disableDragAndDrop: true,
            placeholder: 'Write a comment',
            toolbar: [
                ['insert', ['emoji']],
                ['custom', ['textComplate']],
            ],
            callbacks: {
                onFocus: function(e) {
                   $(e.target).closest('form').find(".note-placeholder").removeClass('custom-placeholder')
                   $(e.target).closest('form').find('.post-create-controls').removeClass('d-none');
                   $(e.target).closest('form').find('.emojionearea-button').removeClass('d-none');
                  
                },
                 onInit: function(e) {
                   e.editingArea.find(".note-placeholder").addClass('custom-placeholder')
                  }
            }
            });
    }
 
    var show_map_init = '';
    @if(count($maps_arr)>0)
        mapboxgl.accessToken = '{{config('mapbox.token')}}';

        @foreach($maps_arr as $map)
        var {{$map['id']}} = new mapboxgl.Map({
            container: '{{$map['id']}}',
            style: 'mapbox://styles/mapbox/satellite-streets-v11',
        });

        var marker = new mapboxgl.Marker()
            .setLngLat([parseFloat({{$map['lng']}}), parseFloat({{$map['lat']}})])
            .addTo({{$map['id']}});

        {{$map['id']}}.setZoom(8);
        {{$map['id']}}.setCenter([parseFloat({{$map['lng']}}), parseFloat({{$map['lat']}})]);
        @endforeach
    @endif

    
</script>

<script data-cfasync="false" src="{{ asset('assets2/js/posts-script.js?v='.time()) }}" type="text/javascript"></script>
@include('site.reports._scripts')
@endsection
