<style>
    .imgcontent {
        width: 100%;
        border-radius: 10px;
        box-shadow: 0px 1px 5px 0 rgba(0, 0, 0, 0.1), inset 0 0 0 11px rgba(0, 0, 0, 0.08);
        border: solid 10px #ffffff;
        background-color: #ffffff;
    }
</style>

<div class="sub_content_text content">
    
    <img class="imgcontent" src="{{$media}}" alt="">

</div>