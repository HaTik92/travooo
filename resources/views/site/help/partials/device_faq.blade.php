<div class="device_content content">
    <div class="d-flex">
        <div class="col-6 d-flex p-2 device_icon" data-type="desktop">
            
                <img src="{{url("assets2/image/icons/desktop-icon-active.svg")}}">
            
            <div class="device_title">
                <p class="mb-0 device_txt"> Desktop</p>
            </div>
        </div>
        <div class="col-6 d-flex p-2  device_icon" data-type="mobile" style="border-left: 1px solid #dae6ff;">
            
                <img src="{{url("assets2/image/icons/mobile-icon.svg")}}">
        
            <div class="device_title">
                <p class="mb-0 device_txt"> On Mobile</p>
            </div>
        </div>
    </div>
    <div class="faq_text p-4 sub_content_text" data-type="faq_desktop">
        <p>{!!$device_faq["desktop"]!!}</p>

    </div>
    <div class="faq_text d-none p-4 sub_content_text" data-type="faq_mobile">
        <p>{!!$device_faq["mobile"]!!}</p>
    </div>
</div>