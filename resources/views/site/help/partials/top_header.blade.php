<div class="signup-header-block" id="signup_header">
    <div class="register-block">
        <div class="trev-register-menu-block">
            <div class="signup-logo pull-left">
                <img src="/frontend_assets/image/main-logo.png" alt="">
            </div>
            <ul class="trev-register-menu-wrap signup-menu-list">
                <li class="active">
                    <a href="">Home</a>
                </li>
                <li>
                    <a href="">What is Travooo</a>
                </li>
                <li>
                    <a href="">Experts</a>
                </li>
                <li>
                    <a href="">Help</a>
                </li>
            </ul>
        </div>
        <div class="register-progress d-none">
            <div class="signup-header-progress">
                <div class="sh-progress-wrap">
                    <div class="prog-title">Start</div>
                    <div class="sh-progress-blog">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                        </div>
                    </div>
                    <div class="prog-title">Finish</div>
                </div>
            </div>
        </div>
        <div class="trev-register-action">
            <a href="{{url('/login')}}" class="btn top-login-btn">Log In</a>
            <a href="{{url('/signup?dt=regular')}}" class="btn top-signup-btn">Sign Up</a>
        </div>
        <button class="navbar-toggler trav-navbar-btn" type="button" data-toggle="collapse" data-target="#travooo_nav" aria-controls="travooo-nav" aria-expanded="false">
            <i class="fal fa-bars"></i>
        </button>
        <div class="responsive-menu-block collapse" id="travooo_nav">
            <ul class="trav-responsive-menu-list">
                <li class="active">
                    <a href="">Home</a>
                </li>
                <li>
                    <a href="">What is Travooo</a>
                </li>
                <li>
                    <a href="">Experts</a>
                </li>
                <li>
                    <a href="">Trip Plans</a>
                </li>
                <li>
                    <a href="">Travelogs</a>
                </li>
                <li>
                    <a href="{{url('/login')}}" class="btn top-login-btn">Log In</a>
                    <a href="{{url('/signup?dt=regular')}}" class="btn top-signup-btn">Sign Up</a>
                </li>
            </ul>
        </div>
    </div>
</div>