<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="/frontend_assets/css/style.css">
    <title>Travooo - Help Center</title>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css" rel="stylesheet" />
    <script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>
    <script src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
    <script src="/frontend_assets/js/script.js"></script>
</head>

<body>
    <header class="main-header">
        <nav class="navbar navbar-toggleable-md navbar-light bg-faded px-4">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <i class="trav-bars"></i>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="{{url('assets2/image/main-circle-logo.png')}}" alt="">
            </a>

            <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarSupportedContent">
                <ul class="nav navbar-nav ">
                    <li class="nav-item">What is travooo?</li>
                    <li class="nav-item"><b>Experts</b></li>
                    <li class="nav-item">Help</li>
                </ul>
                <ul class="navbar-custom-menu navbar-nav pull-right">
                    <li class="nav-item dropdown ">
                        @if(Auth::user())
                        <a class="profile-link" href="#" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
                            <span>{{Auth::user()->name}}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-use-menu">
                            <a class="dropdown-item" href="{{route('profile')}}">
                                <div class="drop-txt">
                                    <p>@lang('profile.profile')</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="{{route('dashboard')}}">
                                <div class="drop-txt">
                                    <p>@lang('dashboard.my_dashboard')</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="{{route('setting')}}">
                                <div class="drop-txt">
                                    <p>@lang('setting.settings')</p>
                                </div>
                            </a>
                            <a class="dropdown-item" href="{{url_with_locale('activitylog')}}">
                                <div class="drop-txt">
                                    <p>@lang('activity_log')</p>
                                </div>
                            </a>
                            <a class="dropdown-item logout-link" href="{{url('user/logout')}}">
                                <div class="drop-txt">
                                    <p>@lang('dashboard.log_out')</p>
                                </div>
                            </a>
                        </div>
                        @else
                        <div>
                            <a href="{{url('/login')}}" class="btn btn-xs btn-outline-primary text-primary">Log in</a>
                            <a href="{{url('/login')}}" class="btn btn-xs btn-primary bg-primary mx-1 text-white">Sign up</a>
                        </div>
                        @endif
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="blue_background">
        <div>

        </div>
        <div class="container">
            <div class="col-sm-12 py-5">
                <a href="" class="text-white main-help"><span class="d-none help_arrow">&#8592; </span>Help Center</a>
            </div>
            <div class="col-sm-6">
                <h1 style="color: #adc9ff"><b>Need Help?</b></h1>
            </div>
            <div class="col-sm-5 offset-sm-5 pb-4">
                <input id="search" type="text" style="border-bottom: 2px solid black" placeholder="Search"
                    aria-label="Search" autocomplete="off">
            </div>
        </div>
    </div>
    <div class="content_section " style="background-color: white">
        
            <div class="container">
                <div class="side_nav">
                    <div class="side_nav_menues">
                        <ul class="main_items list-group-flush">
                            @foreach ($menues as $menu)
                                <li class="list-group-item" data-category="{{strtolower(explode(" ", $menu->name)[0])}}">
                                    <div class="sub_menues_group menu_item" data-type="{{strtolower(explode(" ", $menu->name)[0])}}">
                                        <div class="sub_menu_icon d-flex">
                                            <span class="m-auto">
                                                <img src="{{url("assets2/image/icons/".strtolower(explode(" ", $menu->name)[0]).".svg")}}" >
                                            </span>
                                        </div>
                                            <div class="sub_menu_link">
                                                <a href="#" data-type="{{strtolower(explode(" ", $menu->name)[0])}}" class="menu_item" class="">{{$menu->name}}</a>
                                            </div>
                                    </div>
                                </li>
                                <div id="{{strtolower(explode(" ", $menu->name)[0])}}" class="sub_menu" data-id="sub_">
                                    @foreach ($menu->subMenues->sortBy('position') as $index => $subMenu)
                                        <div class="sub_menues {{$index == 0 ? 'active_sub_item' : ''}}" data-sub-item="item_{{$subMenu->id}}">
                                            <span><b>{{$subMenu->title}}</b></span>
                                        </div>
                                    @endforeach
                                    
                                </div>
                                
                            @endforeach
                          
                        </ul>
                    </div>
                </div>
                <div class="main-content">
                    <div class="all_sub">
                        @foreach ($menues as $menu)
                            <div class="help_content" id="content_{{strtolower(explode(" ", $menu->name)[0])}}">
                                <div class="sub-title mb-5">
                                    <h3>{{$menu->name}}</h3>
                                </div>
                                <div class="d-flex sub-content">
                                    <div class="col-sm-6 p-0">
                                        @for ($i = 0; $i < count($menu->subMenues); $i+=2)
                                            <p class="pb-4"><a href="#" class="sub_menu_item" data-type="{{strtolower(explode(" ", $menu->name)[0])}}" data-sub-item="item_{{$menu->subMenues[$i]->id}}">{{$menu->subMenues[$i]->title}}</a></p>
                                        @endfor
                                    </div>
                                    <div class="col-sm-6 p-0">
                                        @for ($i = 1; $i < count($menu->subMenues); $i+=2)
                                            <p class="pb-4"><a href="#" class="sub_menu_item" data-type="{{strtolower(explode(" ", $menu->name)[0])}}" data-sub-item="item_{{$menu->subMenues[$i]->id}}">{{$menu->subMenues[$i]->title}}</a></p>
                                        @endfor
                                    </div>
                                </div>
                            </div>
                            <div class="divider"></div>
                        @endforeach
                      
                    </div>
                    <div class="one_sub d-none">
                        
                        @foreach ($menues as $menu)
                            @foreach ($menu->subMenues as $subMenu)
                                @continue(count($subMenu->content) == 0)
                                <div class="d-none" data-sub-id="{{$subMenu->id}}">
                                    <div class="d-flex one_sub_content">
                                        <div class="col-sm-12 p-0">
                                            <h2><b>{{$subMenu->title}}</b></h2>
                                            @foreach ($subMenu->content->sortBy('position') as $data)
                                                @php
                                                    $type = $data->type;
                                                    $content = $data->content;
                                                    $device_faq = [
                                                        "desktop" => $data->desktop_faq, 
                                                        "mobile" => $data->mobile_faq
                                                    ];
                                                    $media = $data->media_url;
                                                @endphp
                                                @include('site.help.partials.'.$type, [ "$type" => ${$type}])
                                            @endforeach
                                            <div class="divider w-100"></div>
                                            @if (auth()->user())
                                                <div class="sub_content_text">
                                                    @php
                                                        $status = App\Models\Help\HelpCenterLIkeDislike::where([['user_id', auth()->user()->id],["sub_menu_id", $subMenu->id]])->first();
                                                    @endphp
                                                    <div class="content_action" data-sub_menu_id={{$subMenu->id}}>
                                                        <h5><b>Was this article helpful?</b></h5>
                                                        <button class="btn {{(@$status->like ? 'action_active' : '')}} "  data-type="like"><img src="{{url("assets2/image/icons/helpful.png")}}" alt=""><span> {{$subMenu->LikesDislikesCount()["likes"]}}</span></button>
                                                        <button class="btn {{(@$status->dislike ? 'action_active' : '')}} " data-type="dislike"><img src="{{url("assets2/image/icons/no-helpful.png")}}" alt=""><span> {{$subMenu->LikesDislikesCount()["dislikes"]}}</span></button>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div> 
                            @endforeach
                        @endforeach
                    </div>
                    <div class="search_result d-none">
                        <div class="search_resultes">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{-- <div class="foot-bottom blue_background">
    <div class="container">
        <footer class="page-footer font-small indigo">
            <!-- Footer Links -->
            <div class="container text-center text-md-left">
                <!-- Grid row -->
                <div class="row">
                </div>
                <!-- Grid row -->
            </div>
            <!-- Footer Links -->
        </footer>

    </div>
</div> --}}
    <footer class="footer text-white">
        <div class="foot-bottom">
            <div class="foot-top">
                <ul class="aside-foot-menu text-black">
                    <li><a href="{{url('/')}}">About</a></li>
                    <li><a href="{{url('/')}}">Careers</a></li>
                    <li><a href="{{url('/')}}">Sitemap</a></li>
                    <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                    <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                    <li><a href="{{url('/')}}">Contact</a></li>
                    <li><a href="{{route('help.index')}}">Help Center</a></li>
                </ul>
            </div>
            <p class="copyright">Travooo © 2020</p>
        </div>
    </footer>
    <style>
        html {
            scroll-behavior: smooth;
        }

        a {
            text-decoration: none !important;
        }

        .side_nav {
            /* width: 35%; */
            padding-top: 70px;

            display: inline-block;
            position: absolute;

        }
        .action_active {
            background-color: #4080ff !important;
            color: #fff !important;
        }
        
        .content_action > button {
            width: 98px;
            height: 48px;
            margin-bottom: 93px;
            border-radius: 24px;
            background-color: #f2f6ff;
            padding: 7px;
            color: #c3d0ec !important;
        }


        .device_icon {
            width: 100%;
            cursor: pointer;
            background-color: #f2f6ff;
        }

        /* .device_icon img {
            display: none;
        } */
        
        .divider {
            width: 213px;
            height: 2px;
            background-color: #e6e6e6;
            margin-bottom: 80px;
        }


        .content_section > div.container {
            position: relative;
            padding-bottom: 1px;
        }
        .main-content {
            padding-top: 70px;
            margin-left: 35%;
            min-height: 800px;
            /* position: absolute; */
            /* display: inline-block; */
        }

        .faq_text {
            max-height: 400px;
            overflow-y: auto;
        }
        a:focus {
            color: #404040;
        }

        .main_items>li {
            color: #cccccc;
            border: 0;
            font-size: 22px;
            padding: 0;
            /* line-height: 36px; */
        }

        .main_items>li>div>span>img {
            width: 57px;
        }


        .main_items>li>div>a:hover {
            color: #000;
            font-weight: bolder;
        }


        .main_items>li.active_item {
            color: #000;
            font-weight: bolder;
        }

        .sub_menu {
            display: none;
            padding-left: 57px;

        }

        .sub_menu > div {
            padding-bottom: 10px;
        }

        .sub_menu > div > span, .sub_menues_group {
            cursor: pointer;

        }

        .active_sub_item {
            color: #4080ff;
        }

        .sub-content a,.search_title > a {
            color: #4080ff; 
            font-weight: bolder;
            font-size: 20px;
        }

        
        .device_content {
            border: solid 1px #dae6ff;
            background-color: #ffffff;
            border-radius: 10px;    
        }
        h3 {
            font-weight: bold;
        }


        .device_txt {
            font-size: 18px;
            color: #4080ff;
            padding: 12px 10px;

        }

        .sub_content_text {
            word-break: break-all;
        }

        .sub_content_text > p {
            font-size: 18px;
            font-weight: 300;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.67;
            letter-spacing: normal;
            text-align: left;
            color: #1a1a1a;
        }

        .search_result_sub_menu {
            width: 100%;
            padding: 25px 39px 25px 22px;
            border-radius: 10px;
            border: solid 1px #e6e6e6;
            background-color: #ffffff;
        }
        .search_title {
            color: #4080ff;
            cursor: pointer;
        }

        .search_title > a {
            font-size: 18px;
            font-weight: normal;
        }
        .blue_background {
            background-color: rgb(64, 128, 255);
            height: 357px;
            z-index: 109;
            color: white;
            background-position: top;
            background-size: cover;
            background-image: url({{url("assets/image/copyright_logo.png")}});
        }

        .blue_background a,
        .blue_background li {
            color: white;
        }

        .active-cyan-2 input.form-control[type=text]:focus:not([readonly]) {
            border-bottom: 1px solid #4dd0e1;
            box-shadow: 0 1px 0 0 #4dd0e1;
        }

        .active-cyan-2 input[type=text]:focus:not([readonly]) {
            border-bottom: 1px solid #4dd0e1;
            box-shadow: 0 1px 0 0 #4dd0e1;
        }

        button,
        .btn-primary {
            background: #4080ff;
            background-color: #4080ff;
            font-family: inherit;
            color: white !important;
        }

        .aside-foot-menu li a {
            color: black !important;
        }

        .list-unstyled li a {
            color: white !important;
        }

        .custom-radio label {
            position: relative;
            padding-left: 36px;
            font-size: 16px;
            font-weight: 200;
            line-height: 20px;
            color: #000000;
            font-family: "Circular Std - Book";
        }

        .custom-radio label:after {
            content: "";
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            left: 0;
            width: 16px;
            height: 16px;
            border-radius: 4px;
            border: 2px solid #4080ff;
        }

        .custom-radio input:checked+label {
            font-weight: 400;
        }

        .custom-radio input:checked+label:before {
            content: "";
            position: absolute;
            top: 50%;
            left: 5px;
            transform: translateY(-50%);
            width: 6px;
            height: 6px;
            border-radius: 2px;
            background: #4080ff;

        }

        .custom-radio input {
            display: none;
        }

        .form-control {
            border-radius: 5px;
            background-color: rgb(247, 247, 247);
            border: none;
        }

        #notify-info {
            background-color: rgb(247, 247, 247);
            border: 1px solid #e6e6e6;
            border-radius: 3px;
        }

        .form-group {
            padding: 9px 0;
        }

        textarea {
            resize: none
        }

        #search::placeholder {
            font-family: inherit;
            color: #94b8ff
        }

        #search {
            background-color: transparent;
            border-bottom: 1px solid #94b8ff !important;
            color: inherit;
            width: 100%;
        }

        span {
            line-height: 1.6;
        }

        input.invalid,
        textarea.invalid {
            background-color: #ffdddd;
        }

        section {
            display: none;
        }

        #nextBtn {
            font-family: inherit;
            border-radius: 4px;
            padding: 10px 20px;
        }

        #original-link {
            max-lines: 10;
            resize: vertical;
            max-height: 10em;
            overflow: hidden;
            height: unset;
        }

        .profile {
            border-bottom: 1px solid #b3bfc8;
            border-radius: 0;
        }

        #select2-reported-url-container {
            border-bottom: 2px solid #b3bfc8;
        }

        .select2-selection__arrow {
            display: none;
        }

        .select2-selection--single {
            border: 0 !important;
        }

        .content {
            margin-top: 25px; 
        }
        
        .sub_menues_group {
            display: flex;
        }
        .sub_menu_link {
            padding: 20px 0;
        }
        .sub_menu_icon {
            width: 54px;
            height: 65px;
        }
        @media (max-width: 1000px) {
            /* .main_items {
                display: flex;
                flex-direction: row;
            } */
            .side_nav {
                padding-top: 0;
            }
            .list-group-item {
                font-size: 14px !important;
            }
            .sub_menues {
                border-bottom: 1px solid #e6e6e6;
            }
            .sub_menu {
                position: absolute;
                margin-top: 79px;
                padding-left: 0;
                width: 100%;
                background-color: #fff;
                max-height: 100px;
                overflow: auto;
                border-bottom: 2px solid black;
            }
            .sub_menues_group {
                display: contents;
                text-align: center;
                margin: 0 15px;
            }
            .sub_menu > div {
                white-space: nowrap;
            }
            .sub_menu_link {
                padding: 0;
                width: 100%;
            }
            .sub_menu_icon {
                margin: auto;
            }
            .container {
                width: 100%;
                margin: 0;
            }
            .main-content {
                padding-top: 110px;
                margin-left: 0;
            }
        }
        @media (max-width: 626px) {
            /* .sub_menu > div {
                width: 130px;
            } */
            .device_title {
                white-space: nowrap;
                overflow: hidden;
            }
            .sub_menu {
                margin-top: 92px;
            }
        }
        @media (max-width: 460px) {
            /* .sub_menu > div {
                width: 100px;
            } */
            .sub_menues_group {
                margin: 0;
            }
            .sub_menues_group {
                display: block;
            }
            
        }
        @media (max-width: 345px) {
            .sub_menu {
                margin-top: 107px;
            }

        }
        @media (max-height: 600px) {
            .sub_menu {
                max-height: 40vh;
                overflow-x: scroll;
            }
        }
    }
    </style>
    <script>
        $(document).ready(function() {

            if ($( window ).width() > 986) {
                $(".main_items").addClass("list-group")
                $(".main_items").removeClass("nav")
            }
            else {
                $(".main_items").addClass("nav")
                $(".main_items").removeClass("list-group")
                $(".sub_menues b").html().slice(0, 17)

            }
            $( window ).resize(function() {
                if ($( window ).width() > 986) {
                    $(".main_items").addClass("list-group")
                    $(".main_items").removeClass("nav")
                    $(".main-content").css({
                        paddingTop: "80px"
                    })
                }
                else {
                    $(".main_items").addClass("nav")
                    $(".main_items").removeClass("list-group")
                    $(".sub_menues b").html().slice(0, 17)
                    $(".main-content").css({
                        paddingTop: "200px"
                    })
                }
                // console.log($( window ).width())
                // $( "#log" ).append( "<div>Handler for .resize() called.</div>" );
            });



            disabledScroll = false

            $(".mein-help").click(function(){
                $(".help_arrow").addClass("d-none")
            })

            function arrowShow () { 
                $(".help_arrow").removeClass("d-none")
            }

            function arrowHide () { 
                $(".help_arrow").addClass("d-none")
            }

            $(document).on('click', '.menu_item', function(e){
                arrowShow()
                e.preventDefault()
                $(".search_resultes").empty()
                $(".search_result").addClass("d-none")
                let data_type = $(this).data("type")

                $('.main_items > li').removeClass('active_item')
                $(this).closest('li').addClass("active_item")
                
                $(".one_sub").addClass("d-none")
                $(".all_sub").removeClass("d-none")
                

                let contItemPos = document.getElementById(`content_${data_type}`).getBoundingClientRect()
                if (contItemPos.top != 0) {
                    disabledScroll = true
                    scrollTo(contItemPos.top - 100, () => {disabledScroll = false})
                }  
            })

            $(document).on('click', '.sub_menu_item', function(e) {
                arrowShow()
                e.preventDefault()

                let data_type = $(this).data("type")
                $(".sub_menu").hide()
                $(`#${data_type}`).show()
                if ($( window ).width() < 986) {
                    $(".main-content").css({
                        paddingTop: "200px"
                    })
                }
                else {
                    $(".main-content").css({
                        paddingTop: "80px"
                    })
                }
                $(`.sub_menues[data-sub-item]`).removeClass("active_sub_item")
                let selectedItem = $(this).attr('data-sub-item')
                $(`.sub_menues[data-sub-item=${selectedItem}]`).addClass("active_sub_item")

                $('.main_items > li').removeClass('active_item')
                $(`.main_items > li[data-category=${data_type}]`).closest('li').addClass("active_item")

                // if($(`#${data_type}`).css("display") == "none") {
                    // $(".sub_menu").hide()
                    // $(`#${data_type}`).show()
                // } else {
                //     $(`#${data_type}`).hide()
                // }
            })

            function scrollTo(offset, callback) {
                const fixedOffset = offset.toFixed(),
                    onScroll = function () {
                        if (window.pageYOffset.toFixed() === fixedOffset) {
                            window.removeEventListener('scroll', onScroll)
                            callback()
                        }
                    }

                window.addEventListener('scroll', onScroll)
                onScroll()
                window.scrollBy({
                    top: offset,
                    // behavior: 'smooth'
                })
            }

            $(document).on("click", "div[data-sub-item]", function () {
                arrowShow()
                $(".search_resultes").empty()
                $(".search_result").addClass("d-none")
                let sub_item = $(this).data("sub-item")
                $('div[data-sub-item]').removeClass('active_sub_item')
                $(this).addClass("active_sub_item")
            })
            
            let checker = true;
            $(document).on("scroll", function() {
                let customHeight = Number($(".navbar")[0].offsetHeight) + Number($(".blue_background")[0].offsetHeight + 70)
                if(window.scrollY >= customHeight && checker) {
                    checker = false
                    $(".side_nav").css({
                        position: "fixed",
                        zIndex: 1,
                        display: "block",
                        top: 0,
                        paddingTop: 0

                    })
                    $(".main-content").css("display", "block")

                } else if(window.scrollY <= customHeight) {
                    checker = true
                    if ($( window ).width() > 986) {
                        $(".side_nav").css({
                            paddingTop: "70px"
                        })
                    }
                    $(".side_nav").css({
                        position: "absolute",
                        display: "inline-block",
                    })
                }
                if(!disabledScroll) {
                    disabledScroll = true
                    $(".help_content").each(function (e, a) {
                        let contItemPos = a.getBoundingClientRect()

                        if( contItemPos.top <= 50 && contItemPos.top >= -50 ){
                            let data_type = $(a).attr('id').split("_")[1]

                            $('.main_items > li').removeClass('active_item')
                            $(`.menu_item[data-type=${data_type}]`).closest("li").addClass("active_item")
                            // if($(`#${data_type}`).css("display") == "none") {
                            //     $(".sub_menu").hide()
                            //     $(`#${data_type}`).show()
                            // }
                        }
                    })
                    disabledScroll = false
                }
            })
            $(document).on('click', '.sub_menu > div', function() {
                arrowShow()
                let subId = $(this).data("sub-item").match(/[0-9]+/)[0]
                $(".search_resultes").empty()
                $(".search_result").addClass("d-none")
                $(".all_sub").addClass("d-none")
                $(".one_sub").removeClass("d-none")
                $("div[data-sub-id]").addClass("d-none");
                $(`div[data-sub-id=${subId}]`).removeClass("d-none");
            })

            $(document).on('click', 'h3[data-sub-item]', function() {
                arrowShow()
                let subId = $(this).attr('data-sub-item').match(/[0-9]+/)[0]
                $(".search_resultes").empty()
                $(".search_result").addClass("d-none")
                $(".all_sub").addClass("d-none")
                $(".one_sub").removeClass("d-none")
                $("div[data-sub-id]").addClass("d-none");
                $(`div[data-sub-id=${subId}]`).removeClass("d-none");

            });

            $(document).on('click', '.device_icon', function() {
                arrowShow()
                let icon_type = $(this).data('type')
                switch (icon_type) {
                    case "desktop":
                        $('.device_icon[data-type=desktop]').children("img").attr("src", '{{url("assets2/image/icons/desktop-icon-active.svg")}}')
                        $('.device_icon[data-type=mobile]').children("img").attr("src", '{{url("assets2/image/icons/mobile-icon.svg")}}')
                        $(`.faq_text[data-type=faq_${icon_type}]`).removeClass("d-none")
                        $(`.faq_text[data-type=faq_mobile]`).addClass("d-none")
                    break;
                    
                    case "mobile":
                        $('.device_icon[data-type=mobile]').children("img").attr("src", '{{url("assets2/image/icons/mobile-icon-active.svg")}}')
                        $('.device_icon[data-type=desktop]').children("img").attr("src", '{{url("assets2/image/icons/desktop-icon.svg")}}')
                        $(`.faq_text[data-type=faq_${icon_type}]`).removeClass("d-none")
                        $(`.faq_text[data-type=faq_desktop]`).addClass("d-none")

                    break
                }
            })

            $(".content_action > button").click(function() {
                let type = $(this).data("type")
                let sub_menu_id = $(this).parent(".content_action").data("sub_menu_id")
                var baseUrl = window.location.protocol + "//" + window.location.host;
                $.ajax({
                    type: "post",
                    url: `${baseUrl}/help/help_content/${sub_menu_id}/likedislike`,
                    data: {type},
                }).done(data => {
                    if($(this).hasClass("action_active")) {
                        $(this).removeClass("action_active")
                    } else {
                        $(`.content_action[data-sub_menu_id=${sub_menu_id}] > button`).removeClass("action_active")
                        $(this).addClass("action_active")
                    }
                    $(`.content_action[data-sub_menu_id=${sub_menu_id}] > button[data-type=like] > span`).html(data.likes)
                    $(`.content_action[data-sub_menu_id=${sub_menu_id}] > button[data-type=dislike] > span`).html(data.dislikes)
                }).fail(error => {
                    alert(error.statusText)
                });
            });

            $(document).on('input', '#search', function(e) {
                arrowShow()
                let searching = $('#search').val()
                var lt = /</g, 
                    gt = />/g, 
                    ap = /'/g, 
                    ic = /"/g;
                searching = searching.toString().replace(lt, "&lt;").replace(gt, "&gt;").replace(ap, "&#39;").replace(ic, "&#34;");
                if (searching != "") {
                    $(".all_sub").addClass("d-none")
                    $(".one_sub").addClass("d-none")
                    $(".search_result").removeClass("d-none")
                    $.ajax({
                        type: "get",
                        url: "{{route('help.search')}}",
                        data: {
                            q: searching
                        },
                        
                    })
                    .done(function(response) {
                        $(".search_resultes").empty()
                        if(response.length == 0) {
                            $(".search_resultes").append(
                                `<h1>No results were found for "${searching}"</h1>`
                            )
                            return
                        }
                        
                        function searchBodyContent(subMenu, content, index, id) {
                            return `<div data-item="item_${id}" class="col-sm-12 p-4 my-4 search_result_sub_menu ${index > 2 ? 'd-none' : ''}">
                                        <h3 class="search_title" data-sub-item='item_${subMenu.id}'>${subMenu.title}</h3>
                                        <p class="m-0">${content}...</p>
                                    </div>`
                        }

                        response.map((category, index)=> {
                            let categoryBox = $(`
                                <div id="result_${category.category_id}">
                                    <div class="sub-title">
                                        <h3><img src="${category.iconURL}" >${category.menuName}</h3>
                                    </div>
                                    <div>
                                        <div class="sub-content"></div>
                                        ${category.result.length > 2 ? `<p class="search_title"><a href="#" data-id="${category.category_id}"> Show more results</a></p>` : ''}
                                    </div>
                                </div>
                            `);
                            category.result.map((subMenu, index) => {
                                console.log(index)
                                let filtered = subMenu.content.filter(elem => elem.type == "content")
                                let content = filtered.length > 0 ? filtered[0].content.replace(/(<([^>]+)>)/gi, " ").slice(0, 170) : '';
                                // let title = 
                                console.log(subMenu.title)

                                categoryBox.find(".sub-content").append(searchBodyContent(subMenu, content, index, category.category_id));
                            })
                            $(".search_resultes").append(categoryBox)
                        })
                    })
                    .fail(function(error) {
                        console.log("error");
                    })
                } else {
                    arrowHide()
                    $(".search_resultes").empty()
                    $(".search_result").addClass("d-none")
                    $(".all_sub").removeClass("d-none")

                }

            });

            $(document).on('click', '.search_title a', function(e) {
                arrowShow()
                e.preventDefault()
                let id = $(this).attr("data-id")
                $(`div[data-item=item_${id}]`).removeClass("d-none")
                $(this).addClass("d-none")
            });
            
        });
    </script>

</body>

</html>