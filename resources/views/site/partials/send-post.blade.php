<script>
    var send_post_type = null;
    var send_post_id = null;

    $('a[href$="#sendToFriendModal"]').click(function() {
        var data_text = $(this).data('id');

        if (data_text.indexOf('_') === 0) {
            data_text = data_text.substring(1);
        }

        var data_array = data_text.split('_');

        if (typeof data_array[0] !== 'undefined') {
            send_post_type = data_array[0];
        }

        if (typeof data_array[1] !== 'undefined') {
            send_post_id = data_array[1];
        }
        var sent_ids = [];
        $.ajax({
            method: "GET",
            url: '{{ url('/chat/post-sent-users/') }}' + '/' + send_post_type + '/' +  send_post_id,
            success:function(response){
                sent_ids = response.data;
                $('#sendToFriendModal').find('.friend-search-results-item').each(function() {
                    var send_message_button = $(this).find('.send_message');
                    if (jQuery.inArray( send_message_button.data('user'), sent_ids ) !== -1) {
                        send_message_button.html('Sent');
                        send_message_button.addClass('sent-button')
                    } else {
                        send_message_button.html('Send');
                        send_message_button.removeClass('sent-button')
                    }
                });
            }
        })
    });
</script>
