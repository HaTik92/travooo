<script src="{{url("assets2/js/choosen/chosen.jquery.min.js") }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="{{url('assets2/js/jquery-loading-master/dist/jquery.loading.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}"></script>

<script>
var order = '';
var searched_text = '';
var globalFilters = {};
var filter_cities = [];
var filter_countries = [];
var filter_places = [];
var dest_type = [];
var timer;

$(document).ready(function (e) {
    getPlansFilter();

    $('body').on('mouseover', '.trip-plan-row', function () {
        $(this).find('.edit-paln-dropdown').show()
    });

    $('body').on('mouseout', '.trip-plan-row', function () {
        $(this).find('.edit-paln-dropdown').hide()
    });
    
    //Top map slider
    var gl_top_map_slider = $("#gl_top_map_slider").lightSlider({
        autoWidth: true,
        pager: false,
        enableDrag: false,
        auto:true,
        loop:true,
        slideMargin: 20,
        speed: 600,
        pause: 3000,
        prevHtml: '<i class="trav-angle-left"></i>',
        nextHtml: '<i class="trav-angle-right"></i>',
        addClass: 'gl-dest-slider-wrap',
        onSliderLoad: function (el) {
                    $(el).width(8680);
                    $( window ).resize();
                },
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slideMargin: 9,
                }
            },
        ],
    })
    
    $('.lSNext, .lSPrev').on('click', function(){
        gl_top_map_slider.pause()
      
        if(typeof timer !== "undefined"){
            clearTimeout(timer);
        }
        
        timer = setTimeout(function(){ gl_top_map_slider.play()}, 30000);
    })

    //Top cities slider
    $("#globaltripHeaderSlider").lightSlider({
        autoWidth: true,
        pager: false,
        enableDrag: false,
        // controls: true,
        slideMargin: 20,
        prevHtml: '<i class="trav-angle-left"></i>',
        nextHtml: '<i class="trav-angle-right"></i>',
        addClass: 'post-dest-slider-wrap',
        onBeforeStart: function (el) {
            el.removeAttr("style")
        },
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slideMargin: 9,
                }
            },
        ],
    })



    //Filter by trending cities
    $('body').on('click', '.gt-cities-count, .city-images', function (e) {
        var city_id = $(this).attr('cityId');
        var city_name = $(this).attr('cityname');
        filter_cities = [];
        globalFilters = {};
        dest_type = []
        
        filter_cities.push(city_id)
        globalFilters.cities = filter_cities;

        $('#globalTripSelectCities').addClass('active').find('input').prop("checked", true);
        $('.gl-search-filtered-city').html(getSelectedHtml(city_name, 'close-city-filter', 'cities-id', city_id));
        $('#gl_city').show()

        if ($.inArray(city_name, dest_type) == -1) {
            dest_type.push(city_name)
        }

        showSearchResult(searched_text, dest_type.join(', '))
        getPlansFilter();
    });
    
    
    //Filter by trending cities
    $('body').on('click', '.gt-mates-count', function (e) {
        var city_id = $(this).attr('cityId');
        var url = "{{route('travelmate.newest')}}/?dest="+city_id
       
        window.open(url, '_blank');
    });


    //select filter type
    $('body').on('click', '.gl-trip-tab-block', function (e) {
        @if(Auth::check())
        $('#plan_pagenum').val(1)
        globalFilters.type = $(this).attr('tripType');

        getPlansFilter();
        @else
             $('#signUp').modal('show')
        @endif
    });


    //filter by countries and cities
    $('body').on('click', '.gl-trip-selected', function (e) {
        $('.clear-search').click();
        var type = $(this).attr('fType');
        
        $('.gl-collapse-inner').find('.gl-trip-selected').removeClass('active')

        $(this).find('input').prop("checked", true);
        $('.loc-filter-block').hide();
        $(this).addClass('active')
        
        $('#' + type).show()

            $('.gl-search-filtered-city').html('')
            $('.gl-search-filtered-country').html('')
            filter_countries = [];
            filter_cities = [];
        
            globalFilters.cities = filter_cities;
            globalFilters.countries = filter_countries;


    });

    //filter by city
    $('#filter_city').autocomplete({
        source: function (request, response) {
            jQuery.get("{{url('travelmates/ajaxGetSearchCity')}}", {
                q: request.term
            }, function (data) {
                var items = [];
                
                if(data.length > 0){
                    data.forEach(function (s_item, index) {
                        items.push({
                            value: s_item.text, image: s_item.image, query: s_item.query, id: s_item.id, country_name: s_item.country_name
                        });
                    });
                    response(items);
                }else{
                     var result = [
                        {
                            label: 'No result found', 
                            value: '',
                            country_name: ' '
                        }
                    ];
                    response(result);
                }
            });
        },
        classes: {"ui-autocomplete": "travel-mates-autocomplate"},
        focus: function (event, ui) {
            return false;
        },
        select: function (event, ui) {
            $('#filter_city').val('');
            if ($.inArray(ui.item.id, filter_cities) == -1) {
                filter_cities.push(ui.item.id)
                globalFilters.cities = filter_cities;
                
                if ($.inArray(ui.item.value, dest_type) == -1) {
                    dest_type.push(ui.item.value)
                    showSearchResult(searched_text, dest_type.join(', '))
                }
                
                getPlansFilter();
                $('.gl-search-filtered-city').append(getSelectedHtml(ui.item.value, 'close-city-filter', 'cities-id', ui.item.id));
            }
            return false;
        },
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
                .append(getMarkupHtml(item))
                .appendTo(ul);
    };


    //Remove cities
    $(document).on('click', '.close-city-filter', function () {
        var city_id = $(this).attr('data-cities-id');
        var city_title = $(this).parent().find('.search-selitem-title').text();
        
        filter_cities = $.grep(filter_cities, function (value) {
            return value != city_id;
        });
        
        dest_type = $.grep(dest_type, function (title) {
            return title != city_title;
        });

        globalFilters.cities = filter_cities;

        $("#destination_search").val('').change();
        
        showSearchResult(searched_text, dest_type.join(', '))
        getPlansFilter();

        $(this).closest('li').remove()

    });


    //filter by country
    $('#filter_country').autocomplete({
        source: function (request, response) {
            jQuery.get("{{route('globaltrip.get_search_countries')}}", {
                q: request.term
            }, function (data) {
                var items = [];

                if(data.length > 0){
                    data.forEach(function (s_item, index) {
                        items.push({
                            value: s_item.text, image: s_item.image, query: s_item.query, id: s_item.id, country_name: s_item.country_name
                        });
                    });
                    response(items);
                }else{
                     var result = [
                        {
                            label: 'No result found', 
                            value: '',
                            country_name: ' '
                        }
                    ];
                    response(result);
                }
            });
        },
        classes: {"ui-autocomplete": "travel-mates-autocomplate"},
        focus: function (event, ui) {
            return false;
        },
        select: function (event, ui) {
            $('#filter_country').val('');
            if ($.inArray(ui.item.id, filter_countries) == -1) {
                filter_countries.push(ui.item.id)
                globalFilters.countries = filter_countries;
                
                if ($.inArray(ui.item.value, dest_type) == -1) {
                    dest_type.push(ui.item.value)
                    showSearchResult(searched_text, dest_type.join(', '))
                }
               
                getPlansFilter();
                $('.gl-search-filtered-country').append(getSelectedHtml(ui.item.value, 'close-country-filter', 'countries-id', ui.item.id));
            }
            return false;
        },
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
                .append(getMarkupHtml(item))
                .appendTo(ul);
    };


    //Remove countries
    $(document).on('click', '.close-country-filter', function () {
        var country_id = $(this).attr('data-countries-id');
        var country_title = $(this).parent().find('.search-selitem-title').text();
        filter_countries = $.grep(filter_countries, function (value) {
            return value != country_id;
        });
        
        dest_type = $.grep(dest_type, function (title) {
            return title != country_title;
        });

        globalFilters.countries = filter_countries;
        
        showSearchResult(searched_text, dest_type.join(', '))
        getPlansFilter();

        $(this).closest('li').remove()

    });

    //Delete trip plan
    $(document).on('click', '.delete-my-plan', function () {
        var trip_id = $(this).attr('tripId');

        $.confirm({
            title: 'Confirm!',
            content: '<span class="disapprove-confirm-text">@lang("profile.if_you_canceled_this_trip_you_will_lose")</span> <div class="mb-3"></div>',
            columnClass: 'col-md-5 col-md-offset-5',
            closeIcon: true,
            offsetTop: 0,
            offsetBottom: 500,
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            buttons: {
                cancel: function () {},
                confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-red',
                    keys: ['enter', 'shift'],
                    action: function () {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('trip.ajax_delete_trip')}}",
                            data: {"trip_id": trip_id},
                        })
                                .done(function (res) {
                                  window.location.reload()
                                });
                    }
                }
            }
        });
    });


    $('body').on('DOMNodeInserted', '#global_trip_orderbox', function () {
        $(this).find("#remove_temp_order").remove();
    });

    $('#destination_search').keyup(delay(function (e) {
        let value = $(e.target).val();
        getDestinationSearch(value, $(this), '#destination_searchbox', 'dest-searched-result');
    }, 500));

    $(document).on('click', '#destination_searchbox .dest-searched-result', function (){
        var id = $(this).data('select-id')
        var text = $(this).data('select-text')

        if(id != ''){
            var destination_type = id.split(',')
            $('.loc-filter-block').hide();
            $('.gl-collapse-inner').find('.gl-trip-selected').removeClass('active')
            var destination_id = parseInt(destination_type[0])

            if(destination_type[1] === 'city'){
                if ($.inArray(destination_id, filter_cities) == -1) {
                    filter_cities.push(destination_id)
                    globalFilters.cities = filter_cities;

                    $('#globalTripSelectCities').addClass('active').find('input').prop("checked", true);
                    $('.gl-search-filtered-city').append(getSelectedHtml(text, 'close-city-filter', 'cities-id', destination_id));
                    $('#gl_city').show()
                }
            }else if(destination_type[1] === 'country'){
                if ($.inArray(destination_id, filter_countries) == -1) {
                    filter_countries.push(destination_id)
                    globalFilters.countries = filter_countries;

                    $('#globalTripSelectCountries').addClass('active').find('input').prop("checked", true);
                    $('.gl-search-filtered-country').append(getSelectedHtml(text, 'close-country-filter', 'countries-id', destination_id));
                    $('#gl_country').show()
                }
            }else{
                if ($.inArray(destination_id, filter_places) == -1) {
                    filter_places.push(destination_id)
                    globalFilters.places = filter_places;
                }
            }

            if ($.inArray(text, dest_type) == -1) {
                dest_type.push(text)
            }

            showSearchResult(searched_text, dest_type.join(', '))
            getPlansFilter();
        }

        $('#destination_searchbox').find('.dest-search-result-block').html('')
        $('#destination_searchbox').find('.dest-search-result-block').removeClass('open')
        $('#destination_search').val(text)

    })

    $(document).click(function(event) {
        var $target = $(event.target);
        if(!$target.closest('.dest-searchbox').length && $('.dest-searchbox').is(":visible")) {
            $('.dest-search-result-block').html('')
            $('.dest-search-result-block').removeClass('open')
        }
    });

    
    $('#global_trip_order').select2({
        minimumResultsForSearch: Infinity,
                placeholder: '@lang('globaltrip.newest')',
        debug: true,
                containerCssClass: "gt-select2-input",
        }
    );

    $('#global_trip_order').on('select2:select', function (e) {
        var data = e.params.data;
        if (data.id != '') {
            order = data.id;

            getPlansFilter();
        }
    });
    
    
    //Clear search result
    $(document).on('click', '.clear-search', function () {
        $('#search_global_trip_input').val('')
        $(this).closest('.gt-search-result-block').html('')

        $('.gl-search-filtered-country').html('')
        $('.gl-search-filtered-city').html('')
        $('.gl-collapse-inner').find('.gl-trip-selected').removeClass('active')
        $('#globalTripSelectAll').addClass('active').find('input').prop("checked", true);
        $('.loc-filter-block').hide();
        
        $("#destination_search").val('').change();

        filter_cities = [];
        filter_countries = [];
        filter_places = [];
        dest_type = []
        globalFilters = {};
        
        getPlansFilter();
    })
    
    //Search global trips
    $(document).on('keyup', '#search_global_trip_input', delay(function () {
        searched_text = $(this).val();
        globalFilters.search_text = searched_text;
    
        getPlansFilter();
        showSearchResult(searched_text, dest_type.join(', '))
    }, 500))


    $('.side-trip-tab .trip-tab-block').on('click', function(e){
      $('.side-trip-tab .trip-tab-block').removeClass('current-tab');
      $(this).addClass('current-tab');
    });

})


function markLocetionMatch(text, term) {
    var regEx = new RegExp("(" + term + ")(?!([^<]+)?>)", "gi");
    var output = text.replace(regEx, "<span class='select2-locetion-rendered'>$1</span>");
    return output;
}

var check_view = true;
$(window).on('scroll', function(){
    if(($('#loadMorePlan').offset().top - $(window).scrollTop() < 2500) && check_view && !$('#loadMorePlan').hasClass('d-none')){
        check_view = false

        var queryString = '?order=' + order;
        var nextPage = parseInt($('#plan_pagenum').val()) + 1;

        var url = "{{route('globaltrip.get_more_plan')}}" + queryString;

        $.ajax({
            type: 'GET',
            url: url,
            data: {pagenum: nextPage, filters: globalFilters},
            success: function (data) {
                check_view = true;
                if (data != '') {
                    $('.gt-plan-block').append(data);
                    $('#plan_pagenum').val(nextPage);
                } else {
                    $("#loadMorePlan").addClass('d-none');
                }
            }
        });
    }

})


function getPlansFilter() {
    $('.gt-plan-block').html(loaderHtml);
    $('#loadMorePlan').addClass('d-none')
    $('#plan_pagenum').val(1)
    $.ajax({
        type: 'GET',
        url: "{{route('globaltrip.get_filter')}}",
        data: {filters: globalFilters, order: order},
        success: function (data) {
            if (data.view != '') {
                $('.gt-plan-block').html(data.view);
                $('#loadMorePlan').removeClass('d-none')
            } else {
                $('.gt-plan-block').html('<p class="gt-not-found">Trip Plans not found...</p>')
            }
            $('.all-trip-plans-count').html(data.count);
        }
    });
}


function loaderHtml() {
    return `<div class="load-more-plan">
                    <div id="reportplanloader" class="trip-plan-row">
                        <div class="plan-inside-animation animation ml-0"></div>
                        <div class="plan-block-text-animation">
                            <div class="plan-block-title-animation animation"></div>
                            <div class="plan-desc-animation animation"></div>
                            <div class="plan-dest-info">
                                <div class="plan-hr-animation"></div>
                                <div class="plan-dest-left-animation animation"></div>
                                <div class="plan-dest-right-animation animation" ></div>
                            </div>
                        </div>
                        <div class="plan-place-info">
                            <div class="plan-place-animation animation"></div>
                        </div>
                    </div>
                </div>`;
}


function getMarkupHtml(response) {
    if (response) {
        var markup = '<div class="search-country-block">';
        if (response.image) {
            markup += '<div class="search-country-imag"><img src="' + response.image + '" /></div>';
        }
        markup += '<div class="span10">' +
                '<div class="search-country-info">' +
                '<div class="search-country-text">' + markDestinationMatch(response.value, response.query) + '</div>';
        if (response.country_name != null && response.country_name !=' ') {
            markup += '<div class="search-country-type">City in ' + response.country_name + '</div>';
        } else if(response.country_name == null) {
            markup += '<div class="search-country-type">Country</div>';
        }
        markup += '</div></div>';

        return markup;
    }
}

function markDestinationMatch(text, term) {
    var regEx = new RegExp("(" + term + ")(?!([^<]+)?>)", "gi");
    var output = text.replace(regEx, "<span class='destination-rendered'>$1</span>");
    return output;
}


function getSelectedHtml(title, class_name, data_attr, id) {
    var html = '<li><span class="search-selitem-title">' + title + '</span>' +
            '<span class="close-search-item ' + class_name + '" data-' + data_attr + '="' + id + '"> x</span></li>'

    return html;
}

function showSearchResult(search_type, destination_title){
     var search_text ='Results '+ ((search_type != '')?' for <b>' + search_type + '</b>':'') +' '+ ((destination_title != '')?'in <b>' + destination_title + '</b>':'');
                        
    if(search_type !='' || destination_title !=''){
         $('.gt-search-result-block').html('<div class="post-block post-top-bordered">' +
                search_text +
                '<span class="clear-search"><i class="fa fa-times" aria-hidden="true"></i></span>' +
                '</div>')
    }else{
        $('.gt-search-result-block').html('');
    }
}

</script>
