@extends('site.layouts.site')
@php $title = 'Travooo - Trip plans'; @endphp
@section('after_styles')
<link href="https://cdn.quilljs.com/1.2.6/quill.snow.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{url('assets2/js/jquery-loading-master/dist/jquery.loading.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

<link href="{{ asset('/dist-parcel/assets/GlobalTrip/main.css') }}" rel="stylesheet"/>
@endsection

@section('content-area')
@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif
@include('site.layouts._left-menu')
<div class="post-block post-flight-style mobile-plan report-top-slide globaltrips-top-slider" style="overflow: hidden;">
    @if(count($top_trips) > 0)
        <div class="gl-top-map-sl" id="gl_top_map_slider" style="padding-bottom: 0%;">
            @foreach($top_trips as $top_trip)
            <div class="gl-trip-map gl-top-map-sl">
                <div class="gl-trip-map_wrapper">
                    <img src="{{get_plan_map($top_trip, 1070, 560)}}" alt="map" style="width: 1070px;height: 560px;">
                </div>
                <div class="gl-trip-map_info-layer">
                    <div class="gl-trip-map_top-block">
                        <div class="gl-trip-map_top-info">
                            <i class="trav-trending-destination-icon"></i>
                            <span class="gl-tranding">@lang('globaltrip.trending')</span>
                        </div>
                    </div>
                    <div class="gl-trip-map_top-block">
                        <div class="gl-trip-map_top-info" style="direction: ltr;">
                            <span class="gl-map-top-info-text">@lang('globaltrip.upcoming_trip') </span>
                            <span class="gl-map-top-info-desc">@lang('globaltrip.started_in')</span>
                            <span class="gl-map-top-info-text">{{$top_trip->version->start_date->format('d M Y')}}</span>
                            <span class="gl-map-top-info-desc">@lang('globaltrip.by')</span>
                            <img class="small-ava"  src="{{check_profile_picture(@$top_trip->author->getAttributes()['profile_picture'])}}" alt="ava"  style='width:25px;height:25px;margin: 0 4px;border-radius: 50%;'>
                            <a href="{{url('profile/'.$top_trip->author->id)}}" class="gl-map-top-info-text">{{$top_trip->author->name}}</a>
                        </div>
                    </div>
                    <div class="gl-trip-map_title-block">
                        <div style="text-align: left;">
                            <span class="gl-trip-title">{{$top_trip->title}}</span>
                        </div>
                    </div>
                    <div class="gl-trip-map_desc-block">
                        <div class="gl-trip-map-right pl-0">
                            <div class="trip-map-info">
                                <div class="trip-map-info_block">
                                    <div class="trip-icon-wrap">
                                        <i class="trav-clock-icon"></i>
                                    </div>
                                    <div class="trip-info-txt">
                                        <p class="label-txt">{{$top_trip->_attribute('total_duration')}}</p>
                                        <p class="info-txt">@lang('trip.duration')</p>
                                    </div>
                                </div>
                                <div class="trip-map-info_block">
                                    <div class="trip-icon-wrap">
                                        <i class="trav-map-marker-icon"></i>
                                    </div>
                                    <div class="trip-info-txt">
                                        <p class="label-txt">{{$top_trip->published_places->count()}}</p>
                                        <p class="info-txt">@choice('trip.place', 2)</p>
                                    </div>
                                </div>
                                <div class="trip-map-info_block">
                                    <div class="trip-icon-wrap">
                                        <i class="trav-budget-icon"></i>
                                    </div>
                                    <div class="trip-info-txt">
                                        <p class="label-txt">${{$top_trip->_attribute('total_budget')}}</p>
                                        <p class="info-txt">@lang('trip.budget')</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="view-plan-block">
                                <a href="{{ route('trip.plan', $top_trip->id) }}" target="_blank" type="button" class="btn btn-light-primary btn-bordered view-plan-link">
                                   @lang('globaltrip.view_plan')
                                </a>
                            </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    @endif
    @if(count($top_cities) > 0)
        <div class="globaltrip-trending-block">
            <div class="gl-trending-top-info">
                <i class="trav-trending-destination-icon"></i>
                <span class="gl-trending-text">@lang('globaltrip.trending_destinations_for_trip_plans') <span class="gl-trending-time">(last 30 days)</span></span>
            </div>
            <div class="post-slide-wrap gl-destination-block post-destination-block">
                <div class="lSSlideOuter post-dest-slider-wrap">
                    <div class="lSSlideWrapper usingCss">
                        <ul id="globaltripHeaderSlider" class="post-slider lightSlider lsGrab lSSlide">
                            @foreach($top_cities AS $tc)
                                    <li class="clone left" style="margin-right: 20px;">
                                        <div class="post-dest-inner city-images" cityname="{{$tc['city_info']->transsingle->title}}" cityId="{{$tc['city_info']->id}}">
                                            <div class="img-wrap report-top-slide-img">
                                                <img src="{{check_city_photo(@$tc['city_info']->getMedias[0]->url)}}" alt="{{$tc['city_info']->transsingle->title}}" style='width:165px;height:95px'>
                                                <div class="dest-name">{{$tc['city_info']->transsingle->title}}</div>
                                            </div>
                                        </div>
                                        <div class="trip-dest-count-block gt-cities-count" cityId="{{$tc['city_info']->id}}">
                                            <span class="plan-count">{{optimize_counter($tc['count'])}}</span>
                                            <span class="plan-text">@lang('globaltrip.trip_plans')</span>
                                        </div>
                                    </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>

<div class="custom-row globaltrip-custom-row">
    <!-- MAIN-CONTENT -->
    <div class="main-content-layer mobile-layer">
            <div class="post-block post-gt-header-block d-none d-sm-block">
            <div class="post-side-top padding-10">
                <div class="col-md-5 pr-0 search-gt-input-wrapper" id="globaltrip_searchbox">
                    <i class="trav-search-icon"></i>
                    <input type="search" class="search-global-trip-input" id="search_global_trip_input" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="Search...">
                </div>
                <div class="col-md-4 dest-searchbox" id="destination_searchbox">
                    <input type="search" class="search-destination-input" id="destination_search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="@lang('discussion.strings.place_city_country')">
                    <span class="dest-search-result-block dest-search"></span>
                </div>
                <div class="col-md-3 pl-0" id="global_trip_orderbox">
                    <span class="select2 select2-container select2-container--default" id="remove_temp_order" style="width: 100%;">
                        <span class="selection">
                            <span class="select2-selection select2-selection--single gt-select2-input">
                                <span class="select2-selection__rendered">
                                    <span class="select2-selection__placeholder">@lang('globaltrip.newest')</span>
                                </span>
                                <span class="select2-selection__arrow">
                                    <b></b>
                                </span>
                            </span>
                        </span>
                        <span class="dropdown-wrapper"></span>
                    </span>
                    <select  id="global_trip_order"
                             placeholder="@lang('globaltrip.popular')"
                             style="width: 100%;display:none">
                        <option value="popular">@lang('globaltrip.popular')</option>
                        <option value="newest">@lang('globaltrip.newest')</option>
                        <option value="oldest">@lang('globaltrip.oldest')</option>
                        <option value="upcoming">@lang('globaltrip.upcoming')</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="gt-search-result-block">
        </div>
        <div class="post-block post-top-bordered">
            <div class="post-side-top top-tabs arrow-style">
                <div class="post-top-txt plan-tab-title">
                    <h3 class="side-ttl  current paln-tab-link" tab-url="{{route('profile.update.all_plan')}}">
                        <a href="javascript:;">
                            @lang('globaltrip.all_trip_plans')
                        </a>
                        <span class="count all-trip-plans-count"></span>
                    </h3>
                </div>
            </div>

            <div class="trip-plan-inner">
                <div class="gt-plan-block">
                </div>
                <div id="loadMorePlan" class="load-more-plan">
                    <input type="hidden" id="plan_pagenum" value="1">
                    <div id="reportplanloader" class="trip-plan-row">
                        <div class="plan-inside-animation animation ml-0"></div>
                        <div class="plan-block-text-animation">
                            <div class="plan-block-title-animation animation"></div>
                            <div class="plan-desc-animation animation"></div>
                            <div class="plan-dest-info">
                                <div class="plan-hr-animation" id="hide_plan_loader"></div>
                                <div class="plan-dest-left-animation animation"></div>
                                <div class="plan-dest-right-animation animation" ></div>
                            </div>
                        </div>
                        <div class="plan-place-info">
                            <div class="plan-place-animation animation"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- SIDEBAR -->
    <div class="sidebar-layer mobile-side gl-sidebar-layer" id="sidebarLayer" style="top: 70px;">
        <aside class="sidebar">
            <form method='get' action='' autocomplete="off">
                <div class="post-block post-side-block float--left">
                    <div class="post-side-inner">
                        @if(Auth::check())
                            <a href="{{ route('trip.plan', 0) }}" type="button" class="btn btn-light-primary btn-bordered btn-full">
                        @else
                            <a href="javascript:;" type="button" class="btn btn-light-primary btn-bordered btn-full open-login">
                        @endif
                            @lang('globaltrip.create_a_trip_plan')
                        </a>
                    </div>
                </div>
                <div class="post-block post-side-block float--right">
                    <div class="post-side-trip-inner">
                        <div class="side-trip-tab">
                            <div class="trip-tab-block gl-trip-tab-block current-tab" tripType="all"  id="formSelectAll">
                                 <i id="trav-log-icon" class="fa fa-newspaper-o"></i>

                                <div class="trip-tab-ttl">@lang('globaltrip.all_trip_plans')</div>
                                <input type='radio' name='order'  style='display:none;'  checked />
                            </div>
                            <div class="trip-tab-block gl-trip-tab-block" tripType="mine"  id='formSelectMine'>
                                 <i id="trav-log-icon" class="fa fa-newspaper-o"></i>
                                <div class="trip-tab-ttl">@lang('globaltrip.my_trip_plans')</div> 
                                <input type='hidden' name='type' value=''>

                                <input type='radio' name='order'  style='display:none;'/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="input-mobile d-sm-none" id="globaltrip_searchbox">
                    <i class="trav-search-icon"></i>
                    <input type="text" class="search-global-trip-input" id="search_global_trip_input" placeholder="Search for Trip Plans...">
                </div>
                <div class="post-block post-side-block">
                    <div class="panel-group mobile-panel" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <a class="filter-header collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Filter
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="">
                                        <div class="post-collapse-block">
                                            <div class="post-collapse-inner gl-collapse-inner" id="travelMateAccordion" role="tablist"
                                                 aria-multiselectable="true">
                                                <div>
                                                    <div class="gl-trip-selected trip-tab-block active" fType="all"  id='globalTripSelectAll' style="cursor: pointer;height:40px;width:100%">
                                                        <input type='radio' name='order' value='' checked="checked" >
                                                        <label >@lang('other.all')</label>
                                                    </div>
                                                </div>

                                                <div class="card" style="margin-top:10px">
                                                    <div class="card-header" role="tab" id="reports_countries" style="height:40px">
                                                        <div  class="collapsed gl-trip-selected trip-tab-block" fType="gl_country" id='globalTripSelectCountries'  style="cursor: pointer;">
                                                            <input type='radio' name='order' value=''  >
                                                            <label >@lang('report.countries')</label>
                                                        </div>
                                                    </div>
                                                    <div id="gl_country" class="loc-filter-block" style="display: none;">
                                                        <div class="card-block">
                                                            <div class="report-input-wrap" id="filterForCountry">
                                                                <input name="filter_country" id="filter_country" placeholder="@lang('globaltrip.filter_by_countries')" class="input filter-country-input report-filter-input">
                                                                <div class="filtered-country-list">
                                                                    <ul class="search-selected-block gl-search-filtered-country">
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card" style="margin-top:10px">
                                                    <div class="card-header" role="tab" id="reports_cities" style="height:40px">
                                                        <div  class="collapsed gl-trip-selected trip-tab-block" fType="gl_city"  id='globalTripSelectCities' style="cursor: pointer;">
                                                            <input type='radio' name='order' value=''>
                                                            <label >@lang('report.cities')</label>
                                                        </div>
                                                    </div>
                                                    <div id="gl_city" class="loc-filter-block" style="display: none;">
                                                        <div class="card-block pt-0">
                                                            <div class="report-input-wrap">
                                                                <input name="filter_city" id="filter_city" placeholder="@lang('globaltrip.filter_by_cities')" class="input filter-city-input report-filter-input" >
                                                                <div class="filtered-city-list">
                                                                    <ul class="search-selected-block gl-search-filtered-city">
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-container grid-order hidden-more-sm" id="global_trip_order_mobile">
                        <span class="text">Show</span>
                        <ul class="tab-list list-style">
                            <li class="tab-list-item">
                                <a href="#" class="tab tab-order active" data-tab-filter="popular">@lang('globaltrip.popular')</a>
                            </li>
                            <li class="tab-list-item">
                                <a href="#" class="tab tab-order" data-tab-filter="newest">@lang('globaltrip.newest')</a>
                            </li>
                            <li class="tab-list-item">
                                <a href="#" class="tab tab-order" data-tab-filter="oldest">@lang('globaltrip.oldest')</a>
                            </li>
                            <li class="tab-list-item">
                                <a href="#" class="tab tab-order" data-tab-filter="upcoming">@lang('globaltrip.upcoming')</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </form>

            @include('site.layouts._sidebar')
        </aside>
    </div>
</div>
@endsection

@section('before_scripts')

@endsection

@section('after_scripts')
    @include('site.globaltrips._scripts')
    @if(Auth::check())
        @include('site.home.new.partials._create-options-modal')
    @endif

    <script data-cfasync="false" src="{{asset('dist-parcel/assets/GlobalTrip/main.js')}}"></script>
@endsection
