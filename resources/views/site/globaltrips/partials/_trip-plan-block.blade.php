@php
$t1 = microtime(true);
$isAuth = Auth::check();
$isSuperAdmin = false;
$authId = null;

if ($isAuth) {
    $authId = Auth::user()->id;
    $isSuperAdmin = is_super_admin($authId);
}

@endphp

@foreach($all_plans as $inv)
    @if (!$inv->author)
        @continue
    @endif
    @php
        $inv->published_places = $inv->published_places()->with(['place.getMedias', 'place.transsingle'])->get();
        $places = $inv->published_places->sortBy('date');
        $cnt = $places->count();
        if (!$cnt) {
            continue;
        }
    @endphp
<div class="trip-plan-row">
    <div class="trip-plan-inside-block">
        <div class="trip-plan-map-img gt-map-main-block" style="width:140px;">
            <img src="{{tripPlanThumb($inv, 140, 150)}}" alt="map-image"
                 style="width:140px;height:150px;">
            <span class="gt-creation-time">{{\Carbon\Carbon::parse($inv->created_at)->format('M d, Y')}}</span>
        </div>

    </div>
    <div class="trip-plan-inside-block trip-plan-txt">
        <div class="trip-plan-txt-inner">
            <div class="trip-txt-ttl-layer">
                <h2 class="trip-ttl col-blue gt-text-el">
                    <a href="{{url_with_locale('trip/plan/'.$inv->id)}}">
                        @if(isset($searched_text) && !empty($searched_text))
                            {!! markSearchTerm($searched_text, $inv->title) !!}
                        @else
                            {{$inv->title}}
                        @endif
                    </a>
                </h2>
                <p class="trip-date">{{weatherDate(@$places->first()->date)}}
                    @lang('chat.to') {{weatherDate(@$places->last()->date)}}</p>

                 <p class="trip-date" style='margin:0px'>
                 @lang('globaltrip.by'): <a href="{{url('/profile/'.$inv->author->id)}}" class="gl-plan-author-link">{{$inv->author->name}}</a></p>
                @if($isAuth && ($authId === $inv->users_id  || $isSuperAdmin))
                    <div class="dropdown edit-paln-dropdown">
                        <button class="btn btn-custom-light-primary btn-bordered" type="button"
                                data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            <i class="fa fa-sort-desc" aria-hidden="true"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow l-16">
                            <a class="dropdown-item edit-trip-item"  href="{{ url_with_locale('trip/plan/'.$inv->id.'?do=edit') }}">
                                <span class="icon-wrap panel-icon-wrap">
                                    <img src="{{ asset('assets2/image/icon-pencil.png') }}" style="width: 32px;">
                                </span>
                                <div class="drop-txt">
                                    <p><b>@lang('globaltrip.edit_trip_plan')</b></p>
                                </div>
                            </a>
                            <a class="dropdown-item delete-trip delete-my-plan" href="javascript:;"  tripId="{{$inv->id}}">
                                <span class="icon-wrap close-icon-wrap">
                                    <i class="trav-close-icon"></i>
                                </span>
                                <div class="drop-txt">
                                    <p><b>@lang('globaltrip.delete')</b></p>
                                </div>
                            </a>
                        </div>
                    </div>
                @endif
            </div>
            <div class="trip-txt-info">
                <div class="trip-info">
                    <div class="icon-wrap">
                        <i class="trav-clock-icon"></i>
                    </div>
                    <div class="trip-info-inner">
                        <p><b>{{$inv->_attribute('total_duration')}}</b>
                        </p>
                        <p>@lang('book.duration')</p>
                    </div>
                </div>
                <div class="trip-info">
                    <div class="icon-wrap">
                        <i class="trav-distance-icon"></i>
                    </div>
                    <div class="trip-info-inner">
                        <p><b>{{$inv->_attribute('total_distance')}}</b></p>
                        <p>@lang('profile.distance')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="trip-plan-inside-block">
        <div class="dest-trip-plan">
            <h3 class="dest-ttl">@lang('profile.destination') <span>{{$cnt}}</span>
            </h3>
            <ul class="dest-image-list" style="width:170px;">
                @foreach($places AS $place)
                <li>
                    <img src="{{@check_place_photo($place->place)}}"
                         alt="photo" style="width:52px;height:52px;">
                    @if($cnt > 6 && $loop->iteration === 6)
                    <div style="color: #fff;
                         position: relative;
                         width: 52px;
                         height: 52px;
                         background: #000;
                         opacity: .6;
                         bottom: 52px;
                         text-align: center;
                         padding-top: 19px;
                         ">+{{$cnt - 6}}</div>
                    @break
                    @endif
                </li>
                @endforeach

            </ul>
        </div>
    </div>
</div>
@endforeach
