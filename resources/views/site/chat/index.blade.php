@php
    $title = 'Travooo - messages';

@endphp
{{-- <style>
    .main-blk {
        width:50px;
        height:50px;
        position:relative;
    }
    .half-circle {
        position:absolute;
        width:25px;
        top:0;
        left:25px;
        overflow:hidden;
    }
    .quarter-circle {
        position:absolute;
        width:25px;
        height:25px;
        top:25px;
        right:0;
        overflow:hidden;
    }
    .half-circle img {
        left: -25px;
        position: relative;
    }
    .quarter-circle img {
        left: -25px;
        top: -25px;
        position: relative;
    }
    .other-half-circle {
        position:absolute;
        width:25px;
        top:0;
        left:0;
    }
</style> --}}
<style>
    .avatar {
        display: inline-block;
        border-radius: 50%;
        overflow: hidden;
        width: 50px;
    }

    .avatar:not(:first-child) {
        margin-left: -35px;
        -webkit-mask:radial-gradient(circle 25px at 5px 50%,transparent 99%,#fff 100%);
            mask:radial-gradient(circle 55px at 5px 50%,transparent 99%,#fff 100%);
    }

    .avatar img {
        width: 100%;
        display: block;
    }

</style>
@extends('site.chat.template.chat')

@section('conversations')

    @foreach($conversations as $conversation)

        <?php $font_weight = @$conversation->is_unread(Auth::guard('user')->user()->id) ? 'bold' : 'normal'; ?>
        <div id="conversation-container-{{@$conversation->id}}" class="conv-block"
             data-unread="{{@$conversation->unread_count(Auth::guard('user')->user()->id)}}"
             data-participants="{{@$conversation->participants_id}}"
             onclick='selectConversation(this)'>

            @php
                $ps = $conversation->participants_names;
                $id = user_is_blocked($conversation->user_id) ? '#' : $conversation->user_id;
                $images = $conversation->participant_images;
                $exp = explode(',',$images);
                $ps_exp = explode(', ',$ps);
                $unread_count = $conversation->unread_messages_count;
                $isTripChat = $conversation->type === \App\Services\PlaceChat\PlaceChatsService::CHAT_TYPE_MAIN || \App\Services\PlaceChat\PlaceChatsService::CHAT_TYPE_PLACE;
            @endphp
            @if($exp && (count($exp) > 1 || $isTripChat))
            <div class="img-wrap" >
                <div class="{{count($exp) > 1 ? 'img-group' : ''}} {{count($exp) >= 2 ? 'main-blk' : ''}}" style="display: flex;flex-wrap: wrap;flex: 1 0 auto;width: 80px;">
                @if (count($exp) == 1)
                    <img src="{{ url($exp[0]) }}" style='width:50px;height:50px;'>
                @elseif (count($exp) == 2)
                    {{-- <div class="other-half-circle">  
                        <img src="{{ url($exp[0]) }}">
                    </div>
                    <div class="half-circle">
                        <img src="{{ url($exp[1]) }}">
                    </div> --}}
                    
                        <div class="avatar">
                            <img  src="{{ url($exp[0]) }}">
                        </div>
                        <div class="avatar">
                            <img src="{{ url($exp[1]) }}">
                        </div>
                        
                       
                      
                @elseif (count($exp) >= 3)
                    {{-- <div class="other-half-circle">
                        <img src="{{ url($exp[0]) }}">
                    </div>
                    <div class="half-circle" style="height:25px;">
                        <img src="{{ url($exp[1]) }}">
                    </div>
                    <div class="quarter-circle">
                        <img src="{{ url($exp[2]) }}">
                    </div>	 --}}
                    <div class="avatar">
                        <img  src="{{ url($exp[0]) }}">
                    </div>
                    <div class="avatar">
                        <img src="{{ url($exp[1]) }}">
                    </div>
                    <div class="avatar">
                        <img src="{{ url($exp[2]) }}">
                    </div>
                @endif
                </div>
            </div>
            @else
            <div class="img-wrap">
                <a href="/profile/{{$id}}">
                    <img src="{{ @$conversation->user_thumb }}" alt="image" style='width:50px;height:50px;'>
                </a>
            </div>
            @endif


            <div class="conv-txt">
                <div class="name">
                    <a id="conversation-participants-{{@$conversation->id}}" href="#" class="inner-link"
                       @if(count($ps_exp) > 4)
                               data-toggle="tooltip"
                               data-placement="bottom"
                                data-animation="false"
                                rel="tooltip"
                               title="{{$conversation->participant_names_chunk}}"
                        @endif
                    >
                        @if(count($ps_exp) > 4)
                            {{$conversation->participant_names_chunk_before}} <span class="unread-count-{{@$conversation->id}}">@if($unread_count) ({{$unread_count}}) @endif</span>
                        @else
                            {{$ps}}<span class="unread-count-{{@$conversation->id}}">@if($unread_count) ({{$unread_count}}) @endif</span>
                        @endif

                    </a>
                </div>

                <div class="msg-txt">
                    <p id="conversation-title-{{@$conversation->id}}"
                       style="font-weight: {{$font_weight}};">{{ @$conversation->conversation_title }}</p>
                </div>
            </div>

            <div class="conv-time">
                <span id="conversation-updated-at-{{@$conversation->id}}" class="time">
                    {{@$conversation->last_updated_at}}

                </span><?php //echo gmdate("Y/m/j H:i:s", (int)(join("",localtime()) + 3600*(4+date("I")))); ?>
                <a href="#" onclick="deleteConversation({{@$conversation->id}})" class="setting-link">
                    <i class="trav-cog"></i>
                </a>
            </div>
        </div>

    @endforeach

@endsection
