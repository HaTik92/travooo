<?php
$emoticons = new \App\Models\Chat\ChatEmoticons();
$emoticons = $emoticons->getEmoticonsMap();
?>

<div class="mobile-icon-wrap">
    <ul class="smile-list">

        @foreach($emoticons as $emoticon)

            <li>
                <a href="#" onclick="insertEmoticon('{{$emoticon['shortcode']}}')">
                    <img class="smile"
                         src="{{$emoticon['src']}}"
                         alt="smile">
                </a>
            </li>

        @endforeach

    </ul>
</div>