<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <link rel="stylesheet" href="{{url('css/chat-spinner.css')}}">
    <link href="{{ asset ("/js/choosen/chosen-bootstrap.css") }}" rel="stylesheet">

    <title>Travooo - messages</title>

    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <!-- Chosen -->
    <script src="{{ asset ("/js/choosen/chosen.jquery.min.js") }}"></script>

    <script>
        let conversation_id_query_string = {{$conversation_id}};
    </script>

</head>
<body>

<div class="main-wrapper" id="app">
    @include('site/layouts/header')
    <script src="{{ asset('js/chat-functions.js') }}"></script>

    <div class="content-wrap">
        <div class="container-fluid">
            <div class="message-wrap">
                <div class="message-block hide-side">
                    <div class="message-conversation">
                        <div class="m-top-block">
                            <?php
                            $chat_unread_count = \App\Models\User\User::find(\Illuminate\Support\Facades\Auth::guard('user')->user()->id)->unread_chat_messages_count;
                            ?>
                            <h4 class="block-ttl">@lang('chat.messages') <span id="chat-unread-counter-small"
                                                                               class="count">{{$chat_unread_count}}</span>
                            </h4>
                            <div class="icon-wrap" id="mobileSideToggler">
                                <a href='#' id="newChat" onclick="newChatConversation()"><i
                                            class="trav-message-edit-icon"></i></a>
                            </div>
                        </div>
                        <div class="m-content-block">
                            <div id="conversations-container" class="conversation-inner">

                                @yield('conversations')

                            </div>
                        </div>
                    </div>
                    <div class="message-chat">
                        <div class="m-top-block">
                            <h4 class="block-ttl" id="chat-party-name"></h4>
                            <div class="block-ttl" id="chat-party-to">

                                <label class="control-label">>@lang('chat.to_dd')</label>

                                <select id="chat-participants-new" name="chat-participants[]"
                                        class="form-control"
                                        multiple>
                                    <option value=""></option>
                                </select>
                                {{--<span class="friend-name">Friend name...</span>--}}
                            </div>
                        </div>
                        <div class="m-content-block">
                            <div class="chat-group-wrap">
                                <div id="chatContents" onclick="markCurrentAsRead()"
                                     style="height:400px;overflow-y:scroll;margin-bottom: 15px;max-width:638px;">

                                </div>

                                <div class="add-input-block">

                                    <form id="chat-message-form">
                                        <div class="input-wrap">
                                            <input id="chat_conversation_id" name="chat_conversation_id"
                                                   type="hidden"
                                                   value="0">
                                            <textarea id="message-text" name="message" rows="3"
                                                      placeholder="@lang('chat.type_a_message_here')"
                                                      style="width:100%;"></textarea>
                                        </div>

                                        <div class="add-input-action">
                                            <div class="smile-link-wrap">
                                                <a id="upload_link" href="" class="attachment">
                                                    <i class="trav-attachment"></i>
                                                    <span id="file-name">@lang('chat.add_attachment')</span>
                                                </a>
                                                <input id="upload" name="documents" type="file"
                                                       style="display: none;"/>

                                                @include('site.chat.template.emoticons')

                                            </div>
                                            <div style="display: none;" class="loader"></div>
                                            <button type="button" class="btn btn-light-primary btn-bordered"
                                                    onclick="sendChatMessage()">@lang('form.buttons.send')
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>