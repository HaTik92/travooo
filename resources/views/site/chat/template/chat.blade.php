@extends('site.layouts.site')

@section('after_styles')
    <link rel="stylesheet" href="{{url('css/chat-spinner.css')}}">
    <link href="{{ asset ("/css/chat-style.css") }}" rel="stylesheet">
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <!-- Chosen -->
    <link href="{{ asset('assets2/js/jquery.mentionsInput/jquery.mentionsInput.css') }}" rel="stylesheet"/>
    <link href="{{ asset ("/dist-parcel/assets/Chat/main.css") }}" rel="stylesheet">
    <script>
        let conversation_id_query_string = {{$conversation_id}};
    </script>
@endsection

@section('base')


    @include('site/layouts/_header')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    {{-- <script defer src="{{ asset('js/chat-functions.js') }}"></script> --}}

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <style>
        .select2-container .select2-search.select2-search--inline {
            /*width: 100% !important;*/
            height: 35px !important;
        }
        .select2-search__field {
            width: 100% !important;
        }
        .select2-selection__rendered{
            height: 35px !important;
            overflow-y: auto !important;
        }
        .select2-selection__choice{
            background-color: #fff !important;
        }
        .drag-over{
            border: 1px dashed;
            border-radius: 10px;
            opacity: 0.5;
        }

    </style>
    <script>
        $(function () {
            //$('[data-toggle="tooltip"]').tooltip();
        })
    </script>
    <div id="overlay-for-image"></div>
    <div class="content-wrap" data-AEA-384>
        <div class="container-fluid">
            <div class="message-wrap">
                <div class="message-block hide-side">
                    <div class="message-conversation">
                        <div class="m-top-block">
                            <?php
                            $chat_unread_count = \App\Models\User\User::find(\Illuminate\Support\Facades\Auth::guard('user')->user()->id)->unread_chat_messages_count;
                            ?>
                            <h4 class="block-ttl">@lang('chat.messages') <span id="chat-unread-counter-small"
                                                                               class="count">{{$chat_unread_count}}</span>
                            </h4>
                            <div class="icon-wrap" id="mobileSideToggler">
                                <a href='#' id="newChat" onclick="newChatConversation()"><i
                                            class="trav-message-edit-icon"></i></a>
                            </div>
                        </div>
                        <div class="m-content-block">
                            <div id="conversations-container" class="conversation-inner" style="height:550px;overflow-y:auto;margin-bottom: 15px;">

                                @yield('conversations')

                            </div>
                        </div>
                    </div>
                    <div class="message-chat">
                        <div class="m-top-block">
                            <a href="#" id="back-to-messages"><i class="fas fa-chevron-left"></i> Back</a>
                            <h4 class="block-ttl" id="chat-party-name"></h4>
                            <div class="block-ttl" id="chat-party-to" style="display: flex; width: 100%;">

                                <label class="control-label" style="display: flex;align-items: center;margin-right: 5px;margin-bottom: 0;">@lang('chat.to_dd')</label>

                                <select id="chat-participants-new" name="chat-participants[]" class="form-control" multiple style="display: none;">
                                    <option value=""></option>
                                </select>
                                {{--<span class="friend-name">Friend name...</span>--}}
                            </div>
                        </div>
                        <div class="m-content-block" id="drop_zone"  ondrop="dropHandler(event);" ondragover="dragStartHandler(event);" ondragend="dragEndHandler(event);">
                            <div class="chat-group-wrap">
                                <div id="chatContents" onclick="markCurrentAsRead()"
                                     style="height:400px;overflow-y:auto;margin-bottom: 15px;max-width:638px;">

                                </div>

                                <div class="add-input-block">

                                    <form id="chat-message-form">

                                        <div class="input-wrap">
                                            <input id="chat_conversation_id" name="chat_conversation_id"
                                                   type="hidden"
                                                   value="0">
                                            <textarea id="message-text" name="message" rows="3"
                                                      placeholder="@lang('chat.type_a_message_here')"
                                                      style="width:100%;height: 36px;" class="mentionsInput chat-textarea" oninput="textarea_auto_height(this, '36px')"></textarea>
                                        </div>

                                        <div class="add-input-action">
                                            <div class="smile-link-wrap">
                                                <a id="upload_link" href="" class="attachment text-truncate">
                                                    <i class="trav-attachment"></i>
                                                    <span id="file-name" class="text-truncate">@lang('chat.add_attachment')</span>
                                                </a>
                                                <input id="upload" multiple="true" name="documents[]" type="file"
                                                       style="display: none;"/>

                                                @include('site.chat.template.emoticons')

                                            </div>
                                            <div style="display: none;" class="loader"></div>
                                            <button type="button" class="btn btn-light-primary btn-bordered send-mess-btn"
                                                    onclick="sendChatMessage()" disabled>@lang('form.buttons.send')
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')
<script src="https://underscorejs.org/underscore-min.js"></script>
<script src="https://podio.github.io/jquery-mentions-input/lib/jquery.elastic.js"></script>
@endsection

@section('site_script')

@endsection

