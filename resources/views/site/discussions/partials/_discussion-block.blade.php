<div class="search-result-block disc-search-block">
</div>
@if(count($discussions))
    @php
        $timezone = getLocalTimeZone();
    @endphp
<div class="disc-inner">
    @foreach($discussions as $discussion)
        @php
            $uniqueurl = url('discussion/'. $discussion->author->username, $discussion->id);
            $location_info = getDiscussionLocationInfo($discussion->destination_type, $discussion->destination_id);

        $discussion_replies = $discussion->replies;
        $discussion_replies_count = count($discussion_replies);
        $replies_list = [];
                  if($discussion_replies_count > 0){
                    $repliesQuery = $discussion->replies()->whereNotIn('users_id', blocked_users_list())->orderBy('num_upvotes', 'desc');

                    if ($onlyAuthUser) {
                    $replies_list = $repliesQuery->byAuthUser()->get();
                    }else{
                        $replies_list[] = $repliesQuery->first();
                    }
                }

        @endphp
    <div class="post-block post-top-bordered" id="disc_{{$discussion->id}}">
        <div class="post-top-info-layer">
            <div class="post-info-line">
                {{$location_info['title']}}
            </div>
        </div>
        <div class="post-top-info-layer">
            <div class="post-top-info-wrap">
                <div class="post-top-avatar-wrap ava-50">
                    <img src="{{check_profile_picture($discussion->author->profile_picture)}}"
                         alt="" style="width:50px;height:50px;">
                </div>
                <div class="post-top-info-txt">
                    <div class="post-top-name">
                        <a class="post-name-link"
                           href="{{ url_with_locale('profile/'.$discussion->author->id)}}" target="_blank">{{$discussion->author->name}}</a>
                        {!! get_exp_icon($discussion->author) !!}
                    </div>
                    <div class="post-info">
                        <a href="{{$uniqueurl}}" target="_blank">posted a discussion about</a>
                        <a
                            href="{{url_with_locale(strtolower($discussion->destination_type)."/".$discussion->destination_id)}}"
                            class="link-place" target="_blank">{{$location_info['sub_title']}}</a>
                        <a href="{{$uniqueurl}}" target="_blank">on {!! moreTravlogDateFormat($discussion->created_at, $timezone) !!}</a>
                    </div>
                </div>
            </div>
            <div class="post-top-info-action">
                <div class="dropdown">
                    <button class="btn btn-drop-round-grey btn-drop-transparent" type="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="trav-angle-down"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion">
                        <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$discussion->id}}" data-toggle="modal" data-id="{{$discussion->id}}" data-type="discussion">
                           <span class="icon-wrap" dir="auto">
                               <i class="trav-share-icon" dir="auto"></i>
                           </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Share</b></p>
                                <p dir="auto">Spread the word</p>
                            </div>
                        </a>
                        <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                           <span class="icon-wrap" dir="auto">
                               <i class="trav-link" dir="auto"></i>
                           </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Copy Link</b></p>
                                <p dir="auto">Paste the link anywhere you want</p>
                            </div>
                        </a>
                        <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#sendToFriendModal" data-id="discussion_{{$discussion->id}}" data-toggle="modal" data-target="" dir="auto">
                           <span class="icon-wrap" dir="auto">
                               <i class="trav-share-on-travo-icon" dir="auto"></i>
                           </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                                <p dir="auto">Share with your friends</p>
                            </div>
                        </a>
                        <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlg" data-toggle="modal" onclick="injectData({{$discussion->id}},this)" dir="auto">
                           <span class="icon-wrap" dir="auto">
                               <i class="trav-flag-icon-o" dir="auto"></i>
                           </span>
                            <div class="drop-txt" dir="auto">
                                <p dir="auto"><b dir="auto">Report</b></p>
                                <p dir="auto">Help us understand</p>
                            </div>
                        </a>

                        @if(Auth::check() && Auth::user()->id==$discussion->author->id)
                            <a class="dropdown-item" href="#" onclick="discussion_delete('{{$discussion->id}}', event, false)">
                                <span class="icon-wrap">
                                    <img src="{{asset('assets2/image/delete.svg')}}" style="width:20px;">
                                </span>
                                <div class="drop-txt">
                                    <p><b>@lang('buttons.general.crud.delete')</b></p>
                                    <p style="color:red">@lang('home.remove_this_post')</p>
                                </div>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="topic-inside-panel-wrap">
            <div class="topic-inside-panel disc-post-modal" data-dis_id="{{$discussion->id}}" data-uname="{{$discussion->author->username}}">
                <div class="panel-txt {{count($discussion->media)>0?'w-75':'w-100'}}">
                    <h3 class="panel-ttl" >
                        @if(isset($search_param))
                        {!! markSearchTerm($search_param, str_limit($discussion->question, 50, ' ...')) !!}
                        @else
                        {!! str_limit($discussion->question, 50, ' ...') !!}
                        @endif
                    </h3>
                    <p class="dis_description_{{$discussion->id}}" style="overflow-wrap: break-word;">
                        {!! str_limit($discussion->description, 100, ' ...') !!}</p>
                </div>
                @if(count($discussion->medias)>0)
                @php
                    $file_url = @$discussion->medias[0]->media_url;
                    $file_url_array = explode(".", $file_url);
                    $ext = end($file_url_array);
                    $allowed_video = array('mp4');
                @endphp
                <div class="panel-img disc-panel-img">
                    @if(in_array($ext, $allowed_video))
                        <div class="disc-video-block">
                            <video >
                                <source src="{{$file_url}}" type="video/mp4">
                            </video>
                            <i class="fa fa-play" aria-hidden="true" dir="auto"></i>
                        </div>
                    @else
                        <img src="{{$file_url}}"
                             alt="" style="width:120px;height:120px;">
                    @endif
                </div>
                @endif
            </div>
        </div>

        <div class="post-tips-top-layer post-topic-wrapper bg-white {{(count($discussion_replies) > 0)?'':'dis-noncount-wrapper'}}">
            @if(count($discussion_replies) > 0)
            <div class="post-tips-top">
                <h4 class="post-tips-ttl hidden-less-sm">Top Reply</h4>
                <h4 class="post-tips-ttl hidden-more-sm">Top Recommendations</h4>
                    <a href="javascript:;" type="button" class="btn btn-light-primary btn-bordered {{Auth::check()?'dis-reply-link':'open-login'}}" data-uname="{{$discussion->author->username}}" data-dis_id="{{$discussion->id}}">
                        <i class="trav-reply-icon"></i> Reply
                    </a>
            </div>
            <div class="post-tips-main-block">
                @foreach($replies_list AS $reply)
                <div class="post-tips-row">
                    <div class="tips-top">
                        <div class="tip-avatar">
                            <img src="{{check_profile_picture($reply->author->profile_picture)}}"
                                 alt="" style="width:25px;height:25px;">
                        </div>
                        <div class="tip-content ov-wrap">
                            <div class="top-content-top">
                                <a href="{{url_with_locale('profile/'.$reply->author->id)}}"
                                   class="name-link">{{$reply->author->name}}</a>
                                {!! get_exp_icon($reply->author) !!}
                                <span>@lang('discussion.strings.said')</span><span
                                    class="dot"> · </span>
                                <span>{{diffForHumans($reply->created_at)}}</span>
                                @if (Auth::check() && $reply->author->id !== Auth::user()->id)
                                <div class="d-inline-block pull-right">
                                    <div class="post-top-info-action">
                                        <div class="dropdown">
                                            <button class="btn btn-drop-round-grey btn-drop-transparent disc-btn-border" type="button" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                <i class="trav-angle-down"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="DiscussionReplies">
                                                @include('site.home.partials._info-actions', ['post'=>$reply, 'page' => 'discussion_page'])
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="tip-txt ov-wrap">
                                <p class="dis_reply_{{$reply->id}} ov-wrap disc-text-block">
                                    @php
                                    $showChar = 100;
                                    $ellipsestext = "...";
                                    $moretext = "see more";
                                    $lesstext = "see less";

                                    $content = $reply->reply;

                                    if(mb_strlen($content, 'UTF-8') > $showChar) {

                                    $c = mb_substr($content, 0, $showChar, 'UTF-8');

                                    $html = '<span class="less-content disc-ml-content">'.$c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span> <a href="javascript:;" class="read-more-link">' . $moretext . '</a></span>  <span class="more-content disc-ml-content" style="display:none">' . $content . ' &nbsp;&nbsp;<a href="javascript:;" class="read-less-link">' . $lesstext . '</a></span>';

                                    $content = $html;
                                    }
                                    @endphp
                                    {!!$content!!}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            <div class="disc-foot-block disc-footer-wrapper {{($discussion_replies_count > 0)?'':'dis-noncount'}}">
                <div class="post-foot-block disc-post-reaction">

                    <div class="tips-footer updownvote updown_{{$discussion->id}}" id="updown_{{$discussion->id}}"  dir="auto">
                        <a href="#" class="discussion-vote-link {{Auth::check() ? 'up' : 'open-login'}} {{(Auth::check() && $discussion->likes->search(function ($item, $key) { return $item['users_id'] == Auth::user()->id && $item['vote'] == 1;}) !== false)?'':'disabled'}}  upvote_{{$discussion->id}}" data-id="{{$discussion->id}}" id="upvote_{{$discussion->id}}" data-type="up" dir="auto">
                            <span class="arrow-icon-wrap" dir="auto"><i class="trav-angle-up" style="margin-bottom: 1px;"  dir="auto"></i></span>
                        </a>
                        <span><b class="upvote-count  upvote-count_{{$discussion->id}}" dir="auto">{{ optimize_counter(count($discussion->likes->filter(function ($item, $key) { return $item['vote'] == 1;})))}}</b></span>

                        &nbsp;&nbsp;
                        <a  href="#" class="discussion-vote-link {{Auth::check() ? 'down' : 'open-login'}} {{(Auth::check() && $discussion->likes->search(function ($item, $key) { return $item['users_id'] == Auth::user()->id && $item['vote'] == 0;}) !== false)?'':'disabled'}} downvote_{{$discussion->id}}" data-id="{{$discussion->id}}" id="downvote_{{$discussion->id}}" data-type="down" dir="auto">
                            <span class="arrow-icon-wrap" dir="auto"><i class="trav-angle-down" style="margin-left: -0.04rem;" dir="auto"></i></span>
                        </a>
                        <span><b class="upvote-count downvote-count_{{$discussion->id}}" dir="auto">{{ optimize_counter(count($discussion->likes->filter(function ($item, $key) { return $item['vote'] == 0;})))}}</b></span>
                    </div>
                </div>

                @if($discussion_replies_count == 0)
                    <div class="post-foot-block answer-cta hidden-less-sm">
                        <a href="javascript:;"  class="btn btn-light click-disc {{Auth::check() ? 'dis-reply-link' : 'open-login'}}" data-dis_id="{{$discussion->id}}">
                            <i class="trav-pencil"></i> Answer
                        </a>
                        Be the first to answer this question
                    </div>
                @elseif($discussion_replies_count > 0)
                    <div class="post-foot-block hidden-less-sm">
                        <div class="tips-footer">
                            <div class="more-tips disc-post-modal"  data-dis_id="{{$discussion->id}}" data-uname="{{$discussion->author->username}}" >
                                <ul class="avatar-list">
                                    @php $us_list = array(); @endphp
                                    @foreach ($discussion_replies as $reply)
                                        @if(!in_array($reply->author->id, $us_list))
                                            @php $us_list[] = $reply->author->id; @endphp
                                            @if(count($us_list)<4)
                                                <li><img src="{{check_profile_picture($reply->author->profile_picture)}}" class="small-ava"></li>
                                            @else
                                                @php break; @endphp
                                            @endif
                                        @endif
                                    @endforeach
                                </ul>
                                <span class="disc-trip-count">
                                    <a href="javascript:;" class="dis-more-trip-link discussion-modal" data-shared="{{isset($sharing_flag)}}" post-type="{{@$post_type}}" data-dis_id="{{@$shared_variable ?? @$discussion->id}}" data-shared_id="{{@$shared_variable ? $discussion->id : '' }}" style="cursor: pointer;">
                                    <b>{{$discussion_replies_count-1}}</b> more Replies
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="post-foot-block ml-auto">
                <span class="post_share_buttons" id="{{$discussion->id}}">
                    <a  href="#shareablePost" class="{{Auth::check()?'':'open-login'}}"  data-id="{{$discussion->id}}" data-uniqueurl="{{$uniqueurl}}" data-type="discussion"  data-toggle="modal">
                        <i class="trav-share-icon"></i>
                    </a>
                </span>
                    <span id="post_share_count_{{$discussion->id}}">
                    <a href="#shareablePost" class="{{Auth::check()?'':'open-login'}}" data-id="{{$discussion->id}}" data-uniqueurl="{{$uniqueurl}}" data-type="discussion"  data-toggle="modal" ><strong class="discussion-shared-count-{{$discussion->id}}">{{get_shared_count('discussion', $discussion->id)}}</strong>
                        Shares</a>
                </span>
                </div>
            </div>
            @if(isMobileDevice())
                <div class="disc-foot-block hidden-more-sm">
                    @if($discussion_replies_count == 0)
                        <div class="post-foot-block answer-cta">
                            <a href="javascript:;"  class="btn btn-light click-disc dis-reply-link" data-dis_id="{{$discussion->id}}">
                                <i class="trav-pencil"></i> Answer
                            </a>
                            Be the first to answer this question
                        </div>
                    @elseif($discussion_replies_count > 0)
                        <div class="post-foot-block">
                            <div class="tips-footer">
                                <div class="more-tips disc-post-modal ml-0"  data-dis_id="{{$discussion->id}}" data-uname="{{$discussion->author->username}}">
                                    <ul class="avatar-list">
                                        @php $us_list = array(); @endphp
                                        @foreach ($discussion_replies as $reply)
                                            @if(!in_array($reply->author->id, $us_list))
                                                @php $us_list[] = $reply->author->id; @endphp
                                                @if(count($us_list)<4)
                                                    <li><img src="{{check_profile_picture($reply->author->profile_picture)}}" class="small-ava"></li>
                                                @else
                                                    @php break; @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                    </ul>
                                    <span class="disc-trip-count">
                                        <a href="javascript:;" class="dis-more-trip-link discussion-modal" data-shared="{{isset($sharing_flag)}}" post-type="{{@$post_type}}" data-dis_id="{{@$shared_variable ?? @$discussion->id}}" data-shared_id="{{@$shared_variable ? $discussion->id : '' }}" style="cursor: pointer;">
                                        {{count($discussion_replies)-1}} more recommendations
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @endif
        </div>

    </div>
        @if((($loop->index == 4 && !$from_last) || ($loop->last && $from_last)))
            @include('site.discussions.partials._trending-destinations')
        @endif
    @endforeach
</div>
@else

<div class="post-block post-top-bordered" style="padding:10px;">
    @lang('discussion.no_discussions_dots')
</div>

@endif
