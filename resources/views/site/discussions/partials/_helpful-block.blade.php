@foreach($most_helpful AS $mh)
        <?php
        $mhu = \App\Models\User\User::find($mh->users_id);
        ?>
        <div class="helpful-info-row">
            <div class="helpful-avatar-wrap">
                <img src="{{check_profile_picture($mhu->profile_picture)}}" alt=""
                     style="width:45px;height:45px;">
            </div>
            <div class="helpful-txt-wrap">
                <div class="helpful-info">
                    <div>
                        <a class="name-link"
                           href="{{url_with_locale('profile/'.$mhu->id)}}">{{$mhu->name}}</a>
                        {!! get_exp_icon($mhu) !!}
                    </div>
                    <div>
                        <a class="view-link disc-view-us-all-answer" href="javascript:;" userName="{{$mhu->name}}" userId="{{$mhu->id}}">@lang('discussion.view_all_answers')</a>
                    </div>
                </div>
                <div class="help-info-view">
                    <div class="count">{{$mh->total}}</div>
                    <div>@lang('discussion.strings.replies')</div>
                </div>
            </div>
        </div>
@endforeach