<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>

<script data-cfasync="false" type="text/javascript">


    var topic_list = [];
    var experts_list = [];
    var about_type = '';
    var is_paste = true;
    if (location.href.substring(location.href.lastIndexOf('/') + 1) != 'discussions') {
        type = location.href.substring(location.href.lastIndexOf('/') + 1);
    }
    
        window.addEventListener('offline', function(e) { 
             $('.report-offline-status').css('display', 'flex');
    }, false);

    window.addEventListener('online', function(e) {
         $('.report-offline-status').css('display', 'none');
    }, false);

    $(document).ready(function () {

            $('#discussion_create_form').validate({
                ignore:'',
                rules: {
                    destination_id: {
                        required: true
                    },
                    question: {
                        required: true,
                        maxlength: 150,
                    },
                    description: {
                        required: true,
                        maxlength: 1000,
                    },
                },
                messages: {
                    destination_id: {
                        required: "The about location is required."
                    },
                    question:{
                        required: "The discussion title is required.",
                        maxlength: "The discussion title may not be greater than 150 characters."
                    },
                    description:{
                        required: "The description is required.",
                        maxlength: "The description may not be greater than 1000 characters."
                    }
                },
                submitHandler: function(form) {
                    form.submit()
                }
            });

        $('#askingPopup').on('show.bs.modal',function (event){
            console.log('ddd')
            $('#destination_id').val('')
            $('#disc_destination_about_input').val('')
            $("#question").val('')
            $("#description").val('')
            $("#question_hint").find("span").html(150);
            $("#description_hint").find("span").html(1000);

            about_type = '';
            experts_list = [];
            $('#experts_list').val(experts_list)
            $('.experts-sel-block').html('')
            $('#ask_topic').val('')
            $('.topic-sel-block').html('')
            $('.btn-submit-disc').prop('disabled', 'disabled');

            $('.ask-medias .media-plus-block').each(function() {
                $(this).find('.removeFile').click()
            })

            $('#disc_file').val([])
        });

     
     //Keep modal scroll
    $('.disc-video-modal').on("hidden.bs.modal", function (e) { 
            $('body').addClass('modal-open');
    });
    
    //Show ask experts error
    $(document).on('click', '.disc-ask-expert-block', function () {
            if(about_type == ''){
                $('.disc-ask-expert-error').html('<p>Please choose about field first.</p>')
            }
    });
        
        
   $('form').submit(function() {
        $(this).find("button[type='submit']").prop('disabled',true);
    });
        
    $("#question").keyup(function (e) {
        var limit = 150;
        var txt = $(this).val();
        if (txt.length <= limit)
        {
            $("#question_hint").find("span").html(limit - txt.length);
        } else {
            $(this).val(txt.substring(0, limit));
            return false;
        }


        if ($("#discussion_create_form").valid()) {
            $('.btn-submit-disc').prop('disabled', false)
        } else {
            $('.btn-submit-disc').prop('disabled', 'disabled');
        }
    });
    $("#description").keyup(function (e) {
        var limit = 1000;
        var txt = $(this).val();
        if (txt.length <= limit)
        {
            $("#description_hint").find("span").html(limit - txt.length);
        } else {
            $(this).val(txt.substring(0, limit));
            return false;
        }

        if ($("#discussion_create_form").valid()) {
            $('.btn-submit-disc').prop('disabled', false)
        } else {
            $('.btn-submit-disc').prop('disabled', 'disabled');
        }
    });

    $('#disc_destination_about_input').keyup(delay(function (e) {
        let value = $(e.target).val();
        getDestinationSearch(value, $(this), '#destination_parent', 'dest-about-result');
    }, 500));

    $(document).on('blur', '#disc_destination_about_input', function(){
      var val = $(this).val()
        if(val.length < 3){
            $('#destination_id').val('')
            if ($("#discussion_create_form").valid()) {
                $('.btn-submit-disc').prop('disabled', false)
            } else {
                $('.btn-submit-disc').prop('disabled', 'disabled');
            }
        }
    });

    $(document).on('click', '#destination_parent .dest-about-result', function (){
        var id = $(this).data('select-id')
        var text = $(this).data('select-text')

        about_type = id;
        $('.ask-experts-block').find('.ask-experts-input').removeAttr('disabled')
        $('.disc-ask-expert-error').html('')

        experts_list = [];
        $('#experts_list').val(experts_list)
        $('.experts-sel-block').html('')
        $('#destination_id').val(id)

        if ($("#discussion_create_form").valid()) {
            $('.btn-submit-disc').prop('disabled', false)
        } else {
            $('.btn-submit-disc').prop('disabled', 'disabled');
        }

        $('#destination_parent').find('.dest-search-result-block').html('')
        $('#destination_parent').find('.dest-search-result-block').removeClass('open')
        $('#disc_destination_about_input').val(text)


    })



        $(document).click(function(event) {
            var $target = $(event.target);
            if(!$target.closest('.disc-destination-searchbox').length && $('.disc-destination-searchbox').is(":visible")) {
                $('.dest-search-result-block').html('')
                $('.dest-search-result-block').removeClass('open')
            }
        });





    //Select topic
    $(document).on('keydown', '#ask_topic', function (e) {
        if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 || e.keyCode == 190) && !e.shiftKey) {
            e.preventDefault();
        }
    });
    
    $("#ask_topic").bind("paste", function(e){
        var pastedData = e.originalEvent.clipboardData.getData('text');
        var inputVal = pastedData.replace(/,/g, '', pastedData);
        var pasteStr = inputVal.split(' ');
 
        $.each( pasteStr, function( key, value ) {
        if ($.inArray(value, topic_list) == -1) {
             if(topic_list.length < 10){
                topic_list.push(value)
                $('#topic_list').val(topic_list)
                $('.topic-sel-block').append(getSelectedHtml(value, 'close-topic-item', 'topic-value', value));
                $('.ask-topic-error-block').html('')
             }else{
               $('.ask-topic-error-block').html('The maximum tags count is 10')  
             }
               }else{
                $('.ask-topic-error-block').html('The topic is already selected!')
            }
              
        });
        var self = $(this);
          setTimeout(function(e) {
              self.val('');
          }, 100);
    } );
    
    $(document).on('keyup', '#ask_topic', function (e) {
            if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 || e.keyCode == 190) && !e.shiftKey) {
                $('.ask-topic-error-block').html('')
                var input_value = $(this).val().trim();
                if (input_value != '') {
                    input_value = input_value.replace(',', '', input_value)
                    input_value = input_value.replace('.', '', input_value)
                    if ($.inArray(input_value, topic_list) == -1) {
                        if(topic_list.length < 10){
                        topic_list.push(input_value)
                        $('#topic_list').val(topic_list)
                        $('.topic-sel-block').append(getSelectedHtml(input_value, 'close-topic-item', 'topic-value', input_value));
                        $('.ask-topic-error-block').html('')
                    }else{
                        $('.ask-topic-error-block').html('The maximum tags count is 10')
                    }
                }else{
                        $('.ask-topic-error-block').html('The topic is already selected!')
                    }

                    $(this).val('')
                    $('ul.travel-mates-autocomplate').hide()
                }
            }
    });
    
    
//Remove topic
    $(document).on('click', '.close-topic-item', function () {
        var topic = $(this).attr('data-topic-value');
        topic_list = $.grep(topic_list, function (value) {
            return value != topic;
        });
         if(topic_list.length < 10){
            $('.ask-topic-error-block').html('') 
         }
        $('#topic_list').val(topic_list)
        $(this).closest('li').remove()

    });
    
//Select Experts
   $(document).on('focus', '#ask_experts', function (e) {
        var val = $(this).val().trim();
        getExpertsList(val)
    });
    
     $(document).on('keyup', '#ask_experts', function(e){
            var val = $(this).val().trim();
             getExpertsList(val)
     })
    
     $(document).on('click', '.ask-expert-btn', function(e){
         e.preventDefault()
            var item_id = $(this).attr('data-item-id');
            var item_value = $(this).attr('data-item-value');
            
            $('#ask_experts').val('');
            $('.ask-experts-autocomplate').hide()
            if ($.inArray(item_id, experts_list) == -1) {
                experts_list.push(item_id)
                $('#experts_list').val(experts_list)
                $('.experts-sel-block').append(getSelectedHtml(item_value, 'close-experts-item', 'experts-value', item_id));
            }
     })
    
    $('body').click(function(e){    
        
        var exp_container = $(".ask-experts-autocomplate");
        var exp_input_container = $("#ask_experts");
      
        if(!exp_input_container.is(e.target) && exp_container.has(e.target).length === 0){
           exp_container.hide() 
        }

    });

    
//Remove experts
    $(document).on('click', '.close-experts-item', function () {
        var experts = $(this).attr('data-experts-value');
        experts_list = $.grep(experts_list, function (value) {
            return value != experts;
        });
        $('#experts_list').val(experts_list)
        $(this).closest('li').remove()

    });
    
//add media
    $(document).on('click', '.media-plus-block.active', function () {
        var target_el = $('#disc_file');
        target_el.trigger('click');
    })


$(document).on('change', '#disc_file', function(){

    var media = $(".ask-medias");
            var pair_id = media.closest('.ask-inner').find('*[data-id="disc_pair"]').val();

    if (media.find('.img-wrap-block').length > 4) {
    $('.disc-img-error-message').html('The maximum number of files to upload is 5.')
        return;
    }else{
        $('.disc-img-error-message').html('')
    }

    if (window.File && window.FileReader && window.FileList && window.Blob){ //check File API supported browser

            var data = $(this)[0].files;
            mov_type = false;
             
            $.each(data, function(index, file){ //loop though each file
                if(index > 4){
                    $('.disc-img-error-message').html('The maximum number of files to upload is 5.')
                    return;
                }else{
                    $('.disc-img-error-message').html('')
                }
        
            var uid = Math.floor(Math.random() * 1000000);

            if(file.name.substr(file.name.length - 3) == 'mov'){
                mov_type = true;
            }

            if (/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
                if(file.size > 10485760){
                    $('.disc-img-error-message').html('The maximum size of image 10MB.')
                    return;
                }else{
                    $('.disc-img-error-message').html('')
                }
                        
            media.find('.media-plus-block.active').append('<div class="ask-media-loader">' +
                '<div class="progress">' +
                '<div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="25" style="width:75%" aria-valuemax="100"></div>' +
                '</div>' +
                '</div>')
        
            $('.disc-img-error-message').html('')

            var fRead = new FileReader();
                    fRead.onload = (function(file){
                    return function(e) {

                    var img = $('<img/>').addClass('disc-thumb').attr('src', e.target.result); //create image element

                            var button = $('<span/>').addClass('close').addClass('removeFile').addClass('d-media-'+uid).one('click', function(e){     //create close button element
                    mediaFileDelete(file.name, this, pair_id);
                    e.preventDefault();
                    }).append('<span>&times;</span>');
                            var imgitem = $('<div>').attr('uid', uid).append(img); //create Image Wrapper element
                            imgitem = $('<div/>').addClass('img-wrap-block').append(imgitem).append(button);
                            var active_content = media.find('.media-plus-block.active');
                            active_content.removeClass('active')
                            active_content.html(imgitem)
                            active_content.next().addClass('active')
                            UploadMedia(uid, pair_id, file);
                    };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.

            } else if (/(\.|\/)(m4v|avi|mpg|mp4|mov)$/i.test(file.type) || mov_type){

                if(file.size > 26214400){
                    $('.disc-img-error-message').html('The maximum size of video 25MB.')
                    return;
                }else{
                     $('.disc-img-error-message').html('')
                }
            
            media.find('.media-plus-block.active').append('<div class="ask-media-loader">' +
                '<div class="progress">' +
                '<div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="25" style="width:75%" aria-valuemax="100"></div>' +
                '</div>' +
                '</div>')

            $('.disc-img-error-message').html('')
                
            var fRead = new FileReader();
                    fRead.onload = (function(file){
                    return function(e) {
                        console.log('file.type', file.type)
                        if(file.type == 'video/avi'){
                            var video = $('<img src="/assets2/image/video-pls.png" style="border: 1px solid #e0e0e0;border-radius: 10px;">');
                        }else{
                            var video = $('<video style="object-fit:cover;border-radius:6px;" class="disc-video" width="90" height="90">' +
                                '<source src=' + e.target.result + ' type="video/mp4">' +
                                '</video>');
                        }


                            var button = $('<span/>').addClass('close').addClass('removeFile').addClass('d-media-'+uid).one('click', function(e){     //create close button element
                    mediaFileDelete(file.name, this, pair_id);
                    e.preventDefault();
                    }).append('<span>&times;</span>');
                            var videoitem = $('<div/>').attr('attr', uid).append(video);
                            videoitem = $('<div/>').addClass('img-wrap-block').append(videoitem).append(button);
                            var active_content = media.find('.media-plus-block.active');
                            active_content.removeClass('active')
                            active_content.html(videoitem)
                            active_content.next().addClass('active')
                            UploadMedia(uid, pair_id, file);
                    };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.

            }else{
                $('.disc-img-error-message').html('<p style="color:red">Please select a valid Image/Video file.</p>');
            }
            });
    } else{
    alert("Your browser doesn't support File API!"); //if File API is absent
    }

    });
    }
);
    
//Ask experts item
    function getExpertsList(search_item){
        $('.ask-experts-autocomplate').html('')
        $('.ask-experts-autocomplate').show()
         $('.ask-experts-autocomplate').html('<li class="exp-empty-item">Searching...</li>')
        $.post("{{route('discussion.get_experts')}}" + '?type=' + about_type, {
            q: search_item
        }, function (data) {
            var items = {};
             $('.ask-experts-autocomplate').html('')
            if(data.length > 0){
               data.forEach(function (s_item, index) {
                items = {value: s_item.text, image: s_item.image, query: s_item.query, id: s_item.id, title: s_item.title, ask_count: s_item.ask_count};

                if ($.inArray(s_item.id.toString(), experts_list) == -1) {
                     $('.ask-experts-autocomplate').append('<li class="exp-list-item">'+ getExpertsMarkupHtml(items) +'</li>')
                }
            }); 
            }else{
               $('.ask-experts-autocomplate').html('<li class="exp-empty-item">No result found.</li>') 
            }
    });
    }
    
    function getExpertsMarkupHtml(response) {
        if (response) {
            var markup = '<div class="search-country-block experts-list-block">';
            if (response.image) {
                markup += '<div class="search-country-imag"><img src="' + response.image + '" /></div>';
            }
            markup += '<div class="span10">' +
                    '<div class="search-country-info">' +
                    '<div class="search-country-text"><b>' + response.value + '</b>, Expert in '+ response.title +'</div>';
            markup += '<div class="search-country-type answer-cnt">' + response.ask_count + ' Answers</div></div></div>';
            markup += '<button class="btn ask-expert-btn" data-item-id="'+ response.id +'" data-item-value="'+ response.value +'">Ask</button>';
            markup += '</div>';
            return markup;
        }
    }
    

    function getSelectedHtml(title, class_name, data_attr, id) {
        var html = '<li><span class="search-selitem-title">' + title + '</span>' +
                '<span class="close-search-item ' + class_name + '" data-' + data_attr + '="' + id + '"> <i class="fas fa-times-circle"></i></span></li>'

        return html;
    }



    function markLocetionMatch(text, term) {
        var regEx = new RegExp("(" + term + ")(?!([^<]+)?>)", "gi");
        if(text != null){
            var output = text.replace(regEx, "<span class='select2-locetion-rendered'>$1</span>");
            return output;
        }
    }

    function mediaFileDelete(fileName, obj, peir)
    {
        var media_obj = $('.ask-medias')
        $.ajax({
            method: 'POST',
            url: './tmpdelete',
            data: {pair: peir, fileName: fileName}
        })
                .done(function () {
                    $('.disc-img-error-message').html('')
            
                    $(obj).closest('.media-plus-block').remove();
                    if (media_obj.find('.media-plus-block.active').length == 1) {
                        media_obj.find('#disc_file').before('<div class="media-plus-block">' +
                                '<div class="ask-media-plus-icon">' +
                                '<span class="ask-close"><span class="plus-icon-wrap">+</span></span>' +
                                '</div>' +
                                '</div>')
                    } else {
                        media_obj.find('#disc_file').before('<div class="media-plus-block active">' +
                                '<div class="ask-media-plus-icon">' +
                                '<span class="ask-close"><span class="plus-icon-wrap">+</span></span>' +
                                '</div>' +
                                '</div>')
                    }
                });
    }
    
    checkconnection();


    function UploadMedia(uid, pair, file) {
        var that = this;
        var formData = new FormData();
       
        $('.btn-submit-disc').prop('disabled', true);
        var url = "/tmpupload";
        // add assoc key values, this will be posts values
        formData.append("file", file, file.name);
        formData.append("upload_file", true);
        formData.append("pair", pair);
        var loader_bar = $('.media-plus-block.active div.ask-media-loader');
        var media_count = $('.disc-media-count').html()
      
        $.ajax({
            type: "POST",
            url: url,
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {

                    if(checkconnection()){
                        mediaFileDelete(file.name, $('.d-media-' + uid)[0], pair)
                    }
                    if (evt.lengthComputable) {
                        var percentComplete = ((evt.loaded / evt.total) * 100);
                        loader_bar.find(".progress-bar").width(percentComplete + '%');
                    }
                }, false);
                return xhr;
            },
            success: function (data) {
                // your callback here
                $("[attr=" + uid + "]").parent().show();
                loader_bar.remove()
                $('.btn-submit-disc').prop('disabled', false);
                $('.disc-media-count').html(parseInt(media_count) + 1)
            },
            error: function (error) {
                $("[attr=" + uid + "]").parent().remove();
                alert("The file can't be uploaded.");
                // handle error
            },
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000
        });
    };

    function checkconnection() {
        var status = window.navigator.onLine;
        if (!status) {
             return true;
        }
    }
    
    
    function textarea_auto_height(element, default_height = 'auto') {
        element.style.height = default_height;
        element.style.height = (element.scrollHeight) + "px";
    }
    
</script>
