@if(count($most_discussed) > 0)
    <div class="post-block post-block-discussion-slider">
        <div class="post-side-top">
            <h3 class="side-ttl">@lang('discussion.strings.what_people_are_talking_about') <span
                        class="count">{{count($most_discussed)}}</span></h3>
            <div class="side-right-control">
                <a href="javascript:;" class="slide-link discussion-slide-prev"><i class="trav-angle-left"></i></a>
                <a href="javascript:;" class="slide-link discussion-slide-next"><i class="trav-angle-right"></i></a>
            </div>
        </div>
        <div class="post-side-inner" style="overflow:hidden">
            <div class="post-slide-wrap slide-hide-right-margin">
                <ul class="post-slider post-slider-asking people-asking-post-card" style="width:200%;height:286px;overflow:hidden">
                    @foreach($most_discussed AS $dis)
                            <li class="post-card" style="float:left;margin-right:20px">
                                <a class="disc-slider-link" href="{{url('place/'.$dis->id)}}" target="_blank">
                                    <div class="image-wrap disc-trending-img-wrap">
                                        <img class="lazy" src="{{($dis->getMedias && @$dis->getMedias[0]->url != '') ? S3_BASE_URL . @$dis->getMedias[0]->url : url(PLACE_PLACEHOLDERS)}}"
                                             alt="" style="width:230px;height:300px;">
                                    </div>
                                    <div class="post-slider-caption">
                                        <div class="post-caption-absolute disc-caption-absolute">
                                            <p class="post-card-name">
                                                {{@$dis->transsingle->title}}
                                            </p>
                                            <p class="post-card-placement">
                                                <b>Place</b>
                                                @lang('discussion.strings.in') {{@$dis->city->transsingle->title}}
                                            </p>
                                        </div>
                                        <div class="post-footer-info">
                                            <div class="post-foot-block">
                                                <span><b>{{$dis->total}}</b> @lang('discussion.strings.talking_about_this')</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
@endif
