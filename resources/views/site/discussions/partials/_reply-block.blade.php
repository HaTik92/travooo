@if(!user_access_denied($reply->users_id))
<div class="post-tips-row  @if(isset($is_sub)) sub-comment @endif" id="repliesRow{{$reply->id}}">
    <div class="tips-top">
        <div class="tip-avatar">
            <img src="{{check_profile_picture($reply->author->profile_picture)}}"
                 alt="" style="width:25px;height:25px;">
        </div>
        <div class="tip-content ov-wrap" style="width: 89%;display: grid;">
            <div style="position: relative;" class='discussion--wrap'>
                <div class="top-content-top">
                    <a href="{{url_with_locale('profile/'.$reply->author->id)}}"
                       class="name-link" target="_blank">{{$reply->author->name}}</a>
                    {!! get_exp_icon($reply->author) !!}
                    <span>said</span><span class="dot"> · </span>
                    <span>{{diffForHumans($reply->created_at)}}</span>

                    <div class="post-com-top-action">
                    <div class="dropdown ">
                        <a class="dropdown-link" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="trav-angle-down"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="DiscussionReplies">
                             @if (Auth::check() && $reply->author->id === Auth::user()->id)
                            <a href="javascript:;" class="dropdown-item edit-reply"  data-id="{{$reply->id}}">
                                <span class="icon-wrap">
                                   <i class="trav-pencil" aria-hidden="true"></i>
                                </span>
                                <div class="drop-txt comment-edit__drop-text">
                                    <p><b>@lang('profile.edit')</b></p>
                                </div>
                            </a>

                            <a href="javascript:;" class="dropdown-item delete-reply @if(isset($is_sub)) is-sub-reply-delete @endif" id="{{$reply->id}}" reportid="{{$reply->id}}">
                                <span class="icon-wrap">
                                    <img src="{{asset('assets2/image/delete.svg')}}" style="width:20px;">
                                </span>
                                <div class="drop-txt comment-delete__drop-text">
                                    <p><b>Delete</b></p>
                                </div>
                            </a>
                             @else
                                <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#" data-toggle="modal"  data-target="#spamReportDlg" onclick="injectData({{$reply->id}},this)">
                                <span class="icon-wrap">
                                    <i class="trav-flag-icon-o"></i>
                                </span>
                                    <div class="drop-txt comment-report__drop-text">
                                        <p><b>@lang('profile.report')</b></p>
                                    </div>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                </div>
                <div class="tip-txt ov-wrap">
                    <p class="dis_reply_{{$reply->id}} ov-wrap disc-text-block dis-reply-text">
                        @php                                
                        $showChar = 100;
                        $ellipsestext = "...";
                        $moretext = "see more";
                        $lesstext = "see less";

                        $content = $reply->reply;

                        if(mb_strlen($content, 'UTF-8') > $showChar) {

                        $c = mb_substr($content, 0, $showChar, 'UTF-8');

                        $html = '<span class="less-content disc-ml-content">'.$c . '<span class="moreellipses">' . $ellipsestext . '&nbsp;</span> <a href="javascript:;" class="read-more-link">' . $moretext . '</a></span>  <span class="more-content disc-ml-content" style="display:none">' . $content . ' &nbsp;&nbsp;<a href="javascript:;" class="read-less-link">' . $lesstext . '</a></span>';

                        $content = $html;
                        }
                        @endphp
                        {!!$content!!}</p>
                        <form method='post' class="discussionReplyEditForm{{$reply->id}} d-none">
                            <input type="hidden" class="media-type"  name="media_type" value="{{($reply->medias_type)?$reply->medias_type:''}}"/>
                            <input type="hidden" class="media-url"  name="media_url" value="{{($reply->medias_url)?$reply->medias_url:''}}"/>
                            <div class="reply-text-inner edit-reply-text">
                                <textarea name="text" class="disc-reply-edit-textarea disc-reply-emoji" maxlength="1000" style="height: 76px;" id="disc_edit_reply_textarea_{{$reply->id}}"  placeholder="@lang('discussion.write_a_tip_dots')">{{$reply->reply}}</textarea>
                                <div class="reply-media-error"></div>
                                <div class="reply-media-block">
                                    <div class="reply-media-list">
                                        <div class="reply-media-inner">
                                             @if($reply->medias_type)
                                                <div class="img-wrap-newsfeed">
                                                    <div>
                                                        @if($reply->medias_type == 'image')
                                                            <img class="disc-media-thumb" src="{{$reply->medias_url}}">
                                                        @endif
                                                    </div>
                                                    <span class="disc-media-close removeFile remove-reply-media" dataFId="{{$reply->id}}">
                                                        <span>×</span>
                                                    </span>
                                                </div>
                                             @endif
                                        </div>
                                         <div class="replies-loader d-none">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="reply-media-wrap">
                                        <input type="file" name="file[]" class="replyeditfile{{$reply->id}} replyfile{{$reply->id}}" data-disc-id="{{$reply->id}}" data-id="replyfile{{$reply->id}}"  style="display:none">
                                        <button class="click-target replies-media-btn" type="button" data-target="replyfile{{$reply->id}}">
                                         <i class="fa fa-camera"></i>
                                    </button>
                                    </div>
                                </div>
                            </div>
                            <div class="replay-edit-action-block">
                                <input type="hidden" name="reply_id" value="{{$reply->id}}"/>
                                @if(isset($is_single_page))
                                    <input type="hidden" name="is_single_page" value="true">
                                @endif
                                <a href="javascript:;" class="reply-edit-cancel-link" dataId="{{$reply->id}}">Cancel</a>
                                <a href="javascript:;"  class="reply-edit-post-link" dataId="{{$reply->id}}">Post</a>
                                <input type="submit" name="submit" class="d-none"/>
                            </div>
                        </form>
                </div>
            </div>
            <div class="rp-media-block">
               @if($reply->medias_type == 'image')
                <a href="{{$reply->medias_url}}" data-lightbox="ask-reply__media-{{$reply->id}}">
                   <img src="{{$reply->medias_url}}" class="rp-image">
                </a>
               @endif
           </div>

            <div style="display: flex; justify-content: space-between;">
                <div class="tips-footer updownvote updown_{{$reply->id}}" id="{{$reply->id}}">
                    <a href="#" class="upvote-link {{Auth::check()?'up':'open-login'}} {{(Auth::check() && $reply->upvotes()->where('users_id', Auth::id())->first())?'':'disabled'}}" id="{{$reply->id}}">
                        <span class="arrow-icon-wrap"><i class="trav-angle-up" style="line-height: 18px;"></i></span>
                    </a>
                    <span style="font-size:85%"><b class="upvote-count">{{optimize_counter(count($reply->upvotes))}}</b> Upvotes</span>
                    &nbsp;&nbsp;
                    <a href="#" class="upvote-link {{Auth::check()?'down':'open-login'}} {{(Auth::check() && $reply->downvotes()->where('users_id', Auth::id())->first())?'':'disabled'}}" id="{{$reply->id}}">
                        <span class="arrow-icon-wrap"><i class="trav-angle-down"></i></span>
                    </a>
                    <span style="font-size:85%"><b class="downvote-count">{{optimize_counter(count($reply->downvotes))}}</b> Downvotes</span>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    @if (!isset($is_sub))
                        <a class="{{Auth::check()?'add-sub-comment':'open-login'}} disc-sub-reply {{isset($is_single_page)?'postCommentReply':''}}" href="javascript:void(0)" id="{{$reply->id}}">Reply</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    {{-- Start Sub Reply Section --}}
    @if(Auth::check() && !isset($is_sub))
        @if(!isset($is_single_page))
        <div class="post-add-comment-block child-comment-add-section" style="display: none; padding: 20px;margin-top: 25px;">
            <div class="avatar-wrap">
                <img src="{{check_profile_picture(Auth::guard('user')->user()->profile_picture)}}" alt=""
                    style="width:45px;height:45px;">
            </div>
            <div class="post-add-com-inputs" style="position: relative;">
                <form method='post' class='postReply discussionReplyForm-{{$reply->id}}' id="{{$reply->discussion->id}}">
                    <input type="hidden" class="media-type"  name="media_type" value=""/>
                    <input type="hidden" class="media-url"  name="media_url" value=""/>
                    <div class="reply-text-inner">
                        <textarea name="text" class="disc-reply-textarea disc-reply-emoji" maxlength="1000" style="height: 76px;" id="disc_reply_textarea"  placeholder="@lang('discussion.write_a_tip_dots')"></textarea>
                        <div class="reply-media-error"></div>
                        <div class="reply-media-block d-none">
                            <div class="reply-media-list">
                                <div class="reply-media-inner"></div>
                                <div class="replies-loader d-none">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="reply-media-wrap">
                                <input type="file" name="file[]" class="disc-reply-media-input replyfile{{$reply->discussion->id}}" data-disc-id="{{$reply->discussion->id}}" data-id="replyfile{{$reply->discussion->id}}"  style="display:none">
                                <button class="click-target replies-media-btn" type="button" data-target="replyfile{{$reply->discussion->id}}">
                                <i class="fa fa-camera"></i>
                            </button>
                            </div>
                        </div>
                    </div>
                    <div class="replay-action-block d-none">
                        <input type="hidden" name="discussion_id" value="{{$reply->discussion->id}}"/>
                        <input type="hidden" name="parents_id" value="{{$reply->id}}"/>
                        <a href="javascript:;" class="disc-reply-cancel-link">Cancel</a>
                        <input type="submit" name="submit" value="Post" class="btn btn-light-primary btn-bordered disc-reply-input"/>
                    </div>
                </form>
            </div>
        </div>
        @else
            <div class="post-add-comment-block sub-comment-block" id="replyForm{{$reply->id}}"
                 style="padding-top:0px;padding-left: 59px;display:none;">

                <div class="post-add-com-inputs">
                    <div class="post-add-comment-block" dir="auto">
                        <div class="avatar-wrap">
                            <img src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                                 style="width:50px !important;">
                        </div>
                        <!-- New elements -->
                        <div class="add-comment-input-group n-post-comment-block">
                            <form class="postCommentForm " method="POST" data-id="{{$reply->id}}" data-type="discussion" autocomplete="off" enctype="multipart/form-data">
                                {{ csrf_field() }}<input type="hidden" data-id="pair{{$reply->id}}" name="pair" value="{{uniqid()}}"/>
                                <div class="post-create-block post-comment-create-block post-edit-block" tabindex="0">
                                    <div class="post-create-input n-post-create-input b-whitesmoke">
                                        <textarea name="text" class="comment-reply-txt post-comment-emoji"></textarea>
                                        <div class="n-post-img-wrap">
                                            <input type="file" name="file" class="commentfile" id="discussioncommentreplyfile{{$reply->id}}" data-id="{{$reply->id}}"  style="display:none">
                                            <i class="fa fa-camera click-target" data-target="discussioncommentreplyfile{{$reply->id}}"></i>
                                        </div>
                                    </div>
                                    <div class="medias b-whitesmoke">
                                    </div>
                                </div>
                                <input type="hidden" name="post_id" value="{{$reply->discussion->id}}">
                                <input type="hidden" name="post_type" value="discussion">
                                <input type="hidden" name="child" value="{{$reply->id}}">
                                <input type="hidden" name="is_single_page" value="true">
                                <button type="submit"  class="d-none"></button>
                            </form>
                            <div class="comment-rseply-media"></div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif
    @if (!isset($is_sub))
        <div class="sub-replies">
            @if ($reply->sub->count() > 0)
                @foreach($reply->sub()->orderBy('created_at', 'desc')->get() AS $subreply)
                    @include('site.discussions.partials._reply-block',[
                        'reply'=> $subreply,
                        'is_sub'=> true
                    ])
                @endforeach
            @endif
        </div>
    @endif
    {{-- End Sub Reply Section --}}
</div>
@endif
<script>
    /* Start Sub Reply Section */
    setTimeout(function () {
        repliesEmoji($('.discussionReplyForm-{{$reply->id}}'));
    },250)
    /* End Sub Reply Section */
</script>
