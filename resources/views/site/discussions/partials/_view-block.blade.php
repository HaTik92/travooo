<style>
    .emojionearea-button {
        background-image: unset !important;
    }
</style>
@php
$uniqueurl = url('discussion/'. $dis->author->username, $dis->id);
@endphp
<div class="post-top-info-layer has-img-fit">
    <picture class="has-img-fit img-holder img-bg hidden-more-sm">
        @if($dis->medias && check_media_type(@$dis->medias[0]->media_url) == 'image')
            <img src="{{@$dis->medias[0]->media_url}}" loading="lazy" decoding="async"/>
        @endif
    </picture>
    <input type="hidden" class="username" value="{{$dis->author->username}}">
    <div class="post-top-info-wrap">
        <div class="post-top-avatar-wrap">
            <img src="{{check_profile_picture($dis->author->profile_picture)}}" alt=""
                style="">
        </div>
        <div class="post-top-info-txt">
            <div class="post-top-name">
                <a href="{{ url_with_locale('profile/'.$dis->author->id)}}" class="post-name-link"
                    target="_blank">{{$dis->author->name}}</a>
                {!! get_exp_icon($dis->author) !!}
            </div>
            <div class="post-info">
                posted a discussion about <a href="{{url_with_locale($dis->destination_type.'/'.$dis->destination_id)}}"
                    class="link-place"
                    target="_blank">{{getDiscussionDestination($dis->destination_type, $dis->destination_id)}}</a>
            </div>
        </div>
    </div>
    <div class="dropdown">
        <div class="post-top-info-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <a class="post-collapse" href="#">
                <i class="trav-angle-down"></i>
            </a>
        </div>
        <div class="dropdown-menu dropdown-menu-right dropdown-arrow" posttype="Discussion">
            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#shareablePost" data-uniqueurl="{{$uniqueurl}}" data-tab="shares{{$dis->id}}" data-toggle="modal" data-id="{{$dis->id}}" data-type="discussion">
                           <span class="icon-wrap" dir="auto">
                               <i class="trav-share-icon" dir="auto"></i>
                           </span>
                <div class="drop-txt" dir="auto">
                    <p dir="auto"><b dir="auto">Share</b></p>
                    <p dir="auto">Spread the word</p>
                </div>
            </a>
            <a class="dropdown-item copy-newsfeed-link" data-text="" dir="auto" data-link="{{$uniqueurl}}">
                           <span class="icon-wrap" dir="auto">
                               <i class="trav-link" dir="auto"></i>
                           </span>
                <div class="drop-txt" dir="auto">
                    <p dir="auto"><b dir="auto">Copy Link</b></p>
                    <p dir="auto">Paste the link anywhere you want</p>
                </div>
            </a>
            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#sendToFriendModal" data-id="discussion_{{$dis->id}}" data-toggle="modal" data-target="" dir="auto">
                           <span class="icon-wrap" dir="auto">
                               <i class="trav-share-on-travo-icon" dir="auto"></i>
                           </span>
                <div class="drop-txt" dir="auto">
                    <p dir="auto"><b dir="auto">Send to a Friend</b></p>
                    <p dir="auto">Share with your friends</p>
                </div>
            </a>
            <a class="dropdown-item {{Auth::check()?'':'open-login'}}" href="#spamReportDlg" data-toggle="modal" onclick="injectData({{$dis->id}},this)" dir="auto">
                           <span class="icon-wrap" dir="auto">
                               <i class="trav-flag-icon-o" dir="auto"></i>
                           </span>
                <div class="drop-txt" dir="auto">
                    <p dir="auto"><b dir="auto">Report</b></p>
                    <p dir="auto">Help us understand</p>
                </div>
            </a>

            @if(Auth::check() && Auth::user()->id==$dis->author->id)
                <a class="dropdown-item" href="#" onclick="discussion_delete('{{$dis->id}}', event, true)">
                                <span class="icon-wrap">
                                    <img src="{{asset('assets2/image/delete.svg')}}" style="width:20px;">
                                </span>
                    <div class="drop-txt">
                        <p><b>@lang('buttons.general.crud.delete')</b></p>
                        <p style="color:red">@lang('home.remove_this_post')</p>
                    </div>
                </a>
            @endif

        </div>
    </div>
</div>
<div class="post-content-inner">
    <div class="post-main-content">
        <h3 class="simple dis_question_{{$dis->id}} disc-text-block">
            {!!$dis->question!!}
        </h3>
        <p class="dis_description_{{$dis->id}} disc-content-text disc-text-block">
            @php
            $showChar = 300;
            $ellipsestext = "...";
            $moretext = "see more";
            $lesstext = "see less";

            $content = $dis->description;

            if(mb_strlen($content, 'UTF-8') > $showChar) {

            $c = mb_substr($content, 0, $showChar, 'UTF-8');

            $html = '<span class="less-content disc-ml-content">'.$c . '<span class="moreellipses">' . $ellipsestext .
                    '&nbsp;</span> <a href="javascript:;" class="read-more-link">' . $moretext . '</a></span> <span
                class="more-content disc-ml-content" style="display:none">' . $content . ' &nbsp;&nbsp;<a
                    href="javascript:;" class="read-less-link">' . $lesstext . '</a></span>';

            $content = $html;
            }

            @endphp
            {!!$content!!}</p>

        @if($dis->medias)
        <div class="disc-media-block hidden-less-sm">
            @foreach($dis->medias as $media)
                @if(check_media_type(@$media->media_url) == 'video')
                    <a href="javascript:;" class="disc-media-wrap disc-video-wrap video-link" data-video="{{@$media->media_url}}"
                        data-toggle="modal" data-target="#videoModal">
                        <video width="122" height="65">
                            <source src="{{@$media->media_url}}" type="video/mp4">
                        </video>
                        <i class="fa fa-play" aria-hidden="true" dir="auto"></i>
                    </a>
                @else
                    <a href="{{@$media->media_url}}" data-lightbox="ask__media">
                        <img src="{{@$media->media_url}}" alt="image" class="disc-media-wrap">
                    </a>
                @endif
            @endforeach
        </div>
        @endif
        <div class="post-modal-content-foot">
            <div class="post-modal-foot-list">
                <span>{!!discussionDateFormat($dis->created_at)!!}</span>
                <span class="dot"> · </span>
                <span>@lang('other.views') <b><span
                            class="{{$dis->id}}-view-count">{{count($dis->views)}}</span></b></span>
                @if(count($dis->experts) > 0)
                <span class="dot"> · </span>
                <span>Tagged <b>{{count($dis->experts)}}</b></span>
                <div class="disc-experts-block">
                    @foreach($dis->experts()->take(12)->get() as $expert)
                    <a href="{{url('profile', $expert->user->id)}}" target="_blank" class="exp-user-link">
                        <img src="{{check_profile_picture($expert->user->profile_picture)}}" data-toggle="bs-tooltip"
                            title="{{$expert->user->name}}">
                    </a>
                    @endforeach
                </div>
                @endif
                @if(count($dis->replies) > 0)
                    <span class="dot hidden-more-sm"> · </span>
                    <div class="tips-footer hidden-more-sm">
                        <div class="more-tips">
                            <span class="disc-trip-count">Tips <b><span>{{count($dis->replies)}}</span></b></span>
                            <ul class="avatar-list">
                                @php $us_list = array(); @endphp
                                @foreach ($dis->replies as $reply)
                                    @if(!in_array($reply->author->id, $us_list))
                                        @php $us_list[] = $reply->author->id; @endphp
                                        @if(count($us_list)<4) <li><img
                                                    src="{{check_profile_picture($reply->author->profile_picture)}}"
                                                    class="small-ava"></li>
                                        @else
                                            @php break; @endphp
                                        @endif
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
            </div>
            @if($dis->topics)
                <div class="disc-topic-text hidden-less-sm">
                    @foreach($dis->topics as $topic)
                        <span>{{$topic->topics}}</span>
                    @endforeach
                </div>
                <div class="tag-wrap hidden-more-sm">
                    <ul class="tag-list list-style">
                        @foreach($dis->topics as $topic)
                            <li class="tag-list-item">
                                <a href="javascript:void(0);" class="tag tag-disabled">{{$topic->topics}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="disc-foot-block post-footer-info hidden-less-sm">
                <div class="post-foot-block disc-post-reaction">

                    <div class="tips-footer updownvote updown_{{$dis->id}} d-flex align-items-center" id="updown_{{$dis->id}}" dir="auto">
                        <a href="#"
                            class="discussion-vote-link {{Auth::check()?'up':'open-login'}} {{(Auth::check() && $dis->discussion_upvotes()->where('users_id', Auth::user()->id)->first())?'':'disabled'}}  upvote_{{$dis->id}}"
                            data-id="{{$dis->id}}"
                            id="upvote_{{$dis->id}}" data-type="up" dir="auto">
                            <span class="arrow-icon-wrap" dir="auto"><i class="trav-angle-up" style="margin-bottom: 3px;"
                                    dir="auto"></i></span>
                        </a>
                        <span class="mr-2"><b class="upvote-count  upvote-count_{{$dis->id}}"
                                dir="auto">{{ optimize_counter(count($dis->discussion_upvotes))}}</b></span>

                        &nbsp;&nbsp;
                        <a href="#"
                            class="discussion-vote-link {{Auth::check()?'down':'open-login'}} {{(Auth::check() &&  $dis->discussion_downvotes()->where('users_id', Auth::user()->id)->first())?'':'disabled'}} downvote_{{$dis->id}}"
                            data-id="{{$dis->id}}"
                            id="downvote_{{$dis->id}}" data-type="down" dir="auto">
                            <span class="arrow-icon-wrap" dir="auto"><i class="trav-angle-down"   dir="auto"></i></span>
                        </a>
                        <span><b class="upvote-count downvote-count_{{$dis->id}}"
                                dir="auto">{{ optimize_counter(count($dis->discussion_downvotes))}}</b></span>
                    </div>
                </div>

                @if(count($dis->replies) > 0)
                <div class="post-foot-block hidden-less-sm">
                    <div class="tips-footer">
                        <div class="more-tips">
                            <ul class="avatar-list">
                                @php $us_list = array(); @endphp
                                @foreach ($dis->replies as $reply)
                                @if(!in_array($reply->author->id, $us_list))
                                @php $us_list[] = $reply->author->id; @endphp
                                @if(count($us_list)<4) <li><img
                                        src="{{check_profile_picture($reply->author->profile_picture)}}"
                                        class="small-ava"></li>
                                    @else
                                    @php break; @endphp
                                    @endif
                                    @endif
                                    @endforeach
                            </ul>
                            <span class="disc-trip-count"><b>{{count($dis->replies)}}</b> Replies</span>
                        </div>
                    </div>
                </div>
                @endif

                <div class="post-foot-block ml-auto">
                    <span class="post_share_buttons" id="{{$dis->id}}">
                        <a href="#shareablePost" class="{{Auth::check()?'':'open-login'}}"
                            data-uniqueurl="{{$uniqueurl}}" data-id="{{$dis->id}}" data-type="discussion"
                            data-toggle="modal">
                            <i class="trav-share-icon"></i>
                        </a>
                    </span>
                    <span id="post_share_count_{{$dis->id}}">
                        <a href="#shareablePost" class="{{Auth::check()?'':'open-login'}}" data-uniqueurl="{{$uniqueurl}}" data-id="{{$dis->id}}"
                            data-type="discussion" data-toggle="modal"><strong
                                class="discussion-shared-count-{{$dis->id}}">{{get_shared_count('discussion', $dis->id)}}</strong>
                            Shares</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
    @if(Auth::check())
    <div class="post-add-comment-block">
        <div class="avatar-wrap">
            <img src="{{check_profile_picture(Auth::guard('user')->user()->profile_picture)}}" alt=""
                style="width:45px;height:45px;">
        </div>
        <div class="post-add-com-inputs" style="position: relative;">
            <form method='post' class='discussionReplyForm' id="{{$dis->id}}">
                <input type="hidden" class="media-type" name="media_type" value="" />
                <input type="hidden" class="media-url" name="media_url" value="" />
                <div class="reply-text-inner">
                    <textarea name="text" class="disc-reply-textarea disc-reply-emoji" maxlength="1000"
                        style="height: 76px;" id="disc_reply_textarea"
                        placeholder="Recommend a place or city..."></textarea>
                    <div class="reply-media-error"></div>
                    <div class="reply-media-block d-none">
                        <div class="reply-media-list">
                            <div class="reply-media-inner"></div>
                            <div class="replies-loader d-none">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0"
                                        aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                        <div class="reply-media-wrap">
                            <input type="file" name="file[]" class="disc-reply-media-input replyfile{{$dis->id}}"
                                data-disc-id="{{$dis->id}}" data-id="replyfile{{$dis->id}}" style="display:none">
                            <button class="click-target replies-media-btn" type="button"
                                data-target="replyfile{{$dis->id}}">
                                <i class="fa fa-camera"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="replay-action-block d-none">
                    <input type="hidden" name="discussion_id" value="{{$dis->id}}" />
                    <a href="javascript:;" class="disc-reply-cancel-link">Cancel</a>
                    <input type="submit" name="submit" value="Post"
                        class="btn btn-light-primary btn-bordered disc-reply-input" />
                </div>
            </form>
        </div>
    </div>
    @endif
    <div class="post-tips-top-layer">
        <div class="post-tips-top">
            <h4 class="post-tips-ttl">@lang('discussion.strings.recommendations_by_users')</h4>
            <div>
                <select class="discusson-answers-filter sort-select" placeholder="@lang('discussion.strings.top_first')"
                    data-dis_id="{{$dis->id}}">
                    <option value="top_first">@lang('discussion.strings.top_first')</option>
                    <option value="new_first">New first</option>
                    <option value="old_first">Old first</option>
                </select>
            </div>
        </div>
        <div id="preLoader{{$dis->id}}" class="text-center" style="display: none;">
            <img src='{{asset('assets2/image/preloader.gif')}}' width='50px' />
        </div>
        <div class="post-tips-main-block disc-reply-block reply-block-{{$dis->id}}" id="{{$dis->id}}">
            @if(count($dis->replies)>0)
            <div class="disc-reply-content">
                @foreach($dis->replies()->where('parents_id', 0)->orderBy('num_upvotes', 'desc')->take(10)->get() AS
                $reply)
                @include('site.discussions.partials._reply-block')
                @endforeach
            </div>
            <div id="moreReplies" class="loadMore" style="{{count($dis->replies)< 11?'display:none;':''}}">
                <input type="hidden" id="reply_pagenum" value="1">
                <div id="loadMoreDescReplies" discId="{{$dis->id}}" class="disc-reply-animation">
                    <div class="disc-reply-avatar-wrap">
                        <div class="animation disc-reply-animation-avatar"></div>
                        <div class="disc-reply-top-name animation"></div>
                    </div>
                    <div class="post-comment-text">
                        <div class="disc-reply-an-info animation"></div>
                        <div class="disc-reply-an-sec-info animation"></div>
                        <div class="disc-reply-an-thr-info animation"></div>
                    </div>
                </div>
            </div>
            @else
            <div class="post-tips-row empty" id="{{$dis->id}}">
                <div class="tips-top">
                    @lang('discussion.no_tips_for_now_dots')
                </div>

            </div>
            @endif


        </div>
    </div>
</div>
