@include('site.discussions.view')
<script data-cfasync="false" type="text/javascript">
    $.fn.bstooltip = $.fn.tooltip;
</script>
<script data-cfasync="false" type="text/javascript">
var type = '';

$(document).ready(function () {
    $('#shareablePost #dropdownMenuButton').css('display', 'none')

     document.emojiSource = "{{ asset('plugins/summernote-master/tam-emoji/img') }}";
    // More/less content
    $(document).on('click', ".read-more-link", function () {
        $(this).closest('.disc-text-block').find('.less-content').hide()
        $(this).closest('.disc-text-block').find('.more-content').show()
        $(this).hide()
    });

    $(document).on('click', ".read-less-link", function () {
        $(this).closest('.more-content').hide()
        $(this).closest('.disc-text-block').find('.less-content').show()
        $(this).closest('.disc-text-block').find('.read-more-link').show()
    });



    lightbox.option({
        'disableScrolling': true,
        'alwaysShowNavOnTouchDevices': true,
        'maxWidth': 740,
        'maxHeight': 540,
        'positionFromTop': 145,
        'resizeDuration': 350
    });

//Open video modal    
    $(document).on('click', ".video-link", function () {
        var theModal = $(this).data("target"),
                videoSRC = $(this).attr("data-video"),
                videoSRCauto = videoSRC + "";

        $(theModal + ' source').attr('src', videoSRCauto);
        $(theModal + ' video')[0].load();
        $(theModal + ' button.close').click(function () {
            $(theModal + ' source').attr('src', videoSRC);
        });
    });

    
    //upload media trigger click
    $(document).on('click', 'button.click-target', function () {
     var target = $(this).attr('data-target');
     var target_el = $(this).parent().find('*[data-id="'+target+'"]');
     target_el.trigger('click');
 })
 
 
 
    //reply media upload
    $(document).on('change', '.disc-reply-media-input', function (e) {
      $(this).closest("form").find('.reply-media-inner').html('');
          repliesMediaUploadFile($(this), $(this).closest("form"));

    })

    $(document).on("click",".add-sub-comment",function(){
        $(this).parents('.post-tips-row').find('.child-comment-add-section').toggle();
    });

    $(document).on('click', '.disc-post-modal, .discussion-modal, .dis-reply-link, .hashed-link', function (e) {
        var dis_id = $(this).data('dis_id');
        var username = $(this).data('uname');
        $('.post-happen-question').html(`<div class="dis-loader-bar">
                                                    <span>
                                                        <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                                                    </span>
                                                </div>`)
        if(isMobileDevice() && location.href.substring(location.href.lastIndexOf('/') + 1) != 'discussions'){
            var win = window.open("/discussion/" + username +"/" + dis_id);
            return;
        }else{
            $('#discussion_view').modal('show')
        }

         window.history.pushState({}, document.title,  "/discussion/" + username +"/" + dis_id);
        
        $.ajax({
            method: "POST",
            url: "{{route('discussion.get_discussion_view')}}",
            data: {discussion_id: dis_id},
        })
                .done(function (res) {
                    if (res != '') {
                        $('.post-happen-question').html(res);
                        $('[data-toggle="bs-tooltip"]').bstooltip();
                        answerFilterOption();
                        repliesEmoji($('.discussionReplyForm'));
                    }
                });

        $.ajax({
            method: "POST",
            url: "{{route('discussion.post_view_count')}}",
            data: {discussion_id: dis_id},
        })
                .done(function (res) {
                    var result = JSON.parse(res);
                    if (result.status == 1) {
                        $('.' + dis_id + '-view-count').html(result.count_views)
                    }
                });
    })

    $('body').on('submit', '.discussionReplyForm, .postReply', function (e) {
        var discussion_id = $(this).attr('id');
        var root = $(this).parents('.post-add-comment-block');
        var values = $(this).serialize();
        var isSub = root.hasClass("child-comment-add-section");
        var _block = null;
        if(isSub){
            _block = $(this).parents('.post-tips-row').find(".sub-replies");
        }

        $('.disc-reply-input').prop('disabled', true)
        
        // console.log(_block);
        // e.preventDefault();
        // return null;
        $.ajax({
            method: "POST",
            url: "{{route('discussion.reply')}}",
            data: values
        })
        .done(function (res) {
            $("input[name='text']").each(function (i, obj) {
                $(this).val('');
            });

            $('.emojionearea-editor').html('')
            root.find('.replay-action-block').addClass('d-none')
            root.find('.reply-media-block').addClass('d-none')
            root.find('.emojionearea-button').addClass('d-none')
            root.find('.reply-media-inner').html('')
            root.find('.media-type').val('')
            root.find('.media-url').val('')

            $('.disc-reply-input').prop('disabled', false)

            $("#" + discussion_id + ".post-tips-row.empty").hide();
            if(isSub){
                _block.prepend(res).fadeIn('slow');
            }else{
                $("#" + discussion_id + ".post-tips-main-block").prepend(res).fadeIn('slow');
            }
            
        });
        e.preventDefault();
    });



 $('body').on('mouseover', '.post-tips-row', function () {
            $(this).find('.post-com-top-action .dropdown').show()
        });

$('body').on('mouseout', '.post-tips-row', function () {
    $(this).find('.post-com-top-action .dropdown').hide()
});
        
        
    $('body').on('click', 'a.edit-reply', function (e) {
        var replyId = $(this).attr('data-id');
        editRepliesMedia(replyId)
 
        $('.discussionReplyEditForm' + replyId).removeClass('d-none')
        $('.dis_reply_' + replyId).addClass('d-none')
        repliesEmoji($('.discussionReplyEditForm' + replyId), 'edit');
    });
    
    
    $('body').on('click', '.remove-reply-media', function (e) {
        var replyId = $(this).attr('dataFId');
      
      var form = $('.discussionReplyEditForm' + replyId)
        form.find('.reply-media-inner').html('');
        form.find('.media-type').val('')
        form.find('.media-url').val('')
    });
    
    
    $('body').on('click', 'a.reply-edit-cancel-link', function (e) {
        var replyId = $(this).attr('dataId');
      
        $('.discussionReplyEditForm' + replyId).addClass('d-none')
        $('.dis_reply_' + replyId).removeClass('d-none')
    });

    $('body').on('click', 'a.reply-edit-post-link', function (e) {
        var replyId = $(this).attr('dataId');
        var form = $('.discussionReplyEditForm' + replyId)
        var text  = form.find('textarea').val().trim()
        
        if(text == ''){
            return false;
        }
        
        var values = form.serialize();
        
        $.ajax({
            method: "POST",
            url: "{{route('discussion.reply.edit')}}/" + replyId,
            data: values
        })
                .done(function (res) {
                    if (res != '') {
                        $('#repliesRow'+ replyId).replaceWith(res);
                    }
                });
        e.preventDefault()
    });

    $('body').on('click', 'a.delete-reply', function (e) {
        var replyId = $(this).attr('id');
        var isSub = $(this).hasClass('is-sub-reply-delete');
        var removableTag = $(this).parents('.post-tips-row');
        if(isSub){
            removableTag = $(this).parents('.sub-comment');
        }

        $.confirm({
            title: 'Confirm!',
            content: '<span style="font-size: 18px;line-height: 1.3;">Are you sure want to delete?</span> <div class="mb-3"></div>',
            columnClass: 'col-md-5 col-md-offset-5',
            closeIcon: true,
            offsetTop: 0,
            offsetBottom: 500,
            scrollToPreviousElement:false,
            scrollToPreviousElementAnimate:false,
            buttons: {
                cancel: function () {},
                confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'shift'],
                    action: function(){
                        $.ajax({
                            method: "DELETE",
                            url: "{{route('discussion.reply.delete')}}/" + replyId
                        })
                        .done(function (res) {
                            if (res.error === 0) {
                                removableTag.remove();
                            }
                        });
                    }
                }
            }
        });

        e.preventDefault()
    });


    
    // load more replies
    $(document).on('inview', '#discReplyLoader', function (event, isInView) {
        if (isInView) {
            var nextPage = parseInt($('#reply_pagenum').val()) + 1;
            var discussion_id = $(this).attr('discId')

            $.ajax({
                type: 'POST',
                url: "{{route('discussion.get_more_replies')}}",
                data: {pagenum: nextPage, discussion_id: discussion_id},
                success: function (data) {
                    if (data != '') {
                        $('.disc-reply-content').append(data)
                        $('#reply_pagenum').val(nextPage);
                    } else {
                        $('#loadMore').hide();
                    }
                }
            });
        }
    });


//Cancel reply text
    $(document).on('click', '.disc-reply-cancel-link', function () {
        $(this).closest('form').find('.emojionearea-editor').html('')
        $(this).closest('form').find('.replay-action-block').addClass('d-none')
        $(this).closest('form').find('.reply-media-block').addClass('d-none')
        $(this).closest('form').find('.emojionearea-button').addClass('d-none')
    })


});

function isMobileDevice(){
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {

        return true;
    }else{

        return false;
    }
}



function discussion_delete(param, e, d_type = false) {
    var parent_content = $('#disc_' + param)
    var my_disc_count = $('.my-disc-count').html();
    var disc_res_link = $('.disc-res-link-' + param)

    var url = '{{ route("discussion.delete", ":postId") }}';
    url = url.replace(':postId', param);
    $.confirm({
        title: 'Confirm!',
        content: '<span class="disapprove-confirm-text">Are you sure you want to delete this discussion?</span> <div class="mb-3"></div>',
        columnClass: 'col-md-5 col-md-offset-5',
        closeIcon: true,
        offsetTop: 0,
        offsetBottom: 500,
        scrollToPreviousElement: false,
        scrollToPreviousElementAnimate: false,
        buttons: {
            cancel: function () {},
            confirm: {
                text: 'Confirm',
                btnClass: 'btn-red',
                keys: ['enter', 'shift'],
                action: function () {
                    $.ajax({
                        method: "GET",
                        url: url
                    })
                            .done(function (res) {
                                if (res == 'success') {
                                    if(d_type){
                                        if(window.location.pathname.split('/')[1] == 'profile-posts')
                                            window.location.reload();
                                        else
                                            window.location.href = '{{url('discussions')}}'
                                    }else {
                                        parent_content.fadeOut()
                                        disc_res_link.fadeOut()
                                        $('.my-disc-count').html(parseInt(my_disc_count) - 1)
                                        $('.disc-modal-close').click()
                                    }
                                }
                            });
                }
            }
        }
    });

    e.preventDefault();
}

//Discussion upvote
$(document).on('click', '.discussion-vote-link.up', function (e) {
    var _this = $(this);
    var discussion_id = $(this).data('id');
    var parent_content = _this.closest('#updown_' + discussion_id)
    if(_this.hasClass('disabled')){
        is_add = false;
    }else{
        is_add = true;
    }

    $.ajax({
        method: "POST",
        url: "{{route('discussion.updownvote')}}",
        data: {discussion_id: discussion_id, type:1, is_add:is_add}
    })
    .done(function (res) {
        $('.upvote-count_' + discussion_id).html(res.count_upvotes)
        $('.downvote-count_' + discussion_id).html(res.count_downvotes)

            if(_this.hasClass('disabled')){
                $(`.upvote_${discussion_id}`).removeClass('disabled')
            }else{
                $(`.upvote_${discussion_id}`).addClass('disabled')
            }

            if(!$(`.downvote_${discussion_id}`).hasClass('disabled')){
                $(`.downvote_${discussion_id}`).addClass('disabled')
            }
        });

    e.preventDefault()
});

//Discussion downvote
$(document).on('click', '.discussion-vote-link.down', function (e) {

    var _this = $(this);
    var discussion_id = $(this).data('id');
    var parent_content = _this.closest('#updown_' + discussion_id)
    console.log('ddd', parent_content)
    if(_this.hasClass('disabled')){
        is_add = false;
    }else{
        is_add = true;
    }

    $.ajax({
        method: "POST",
        url: "{{route('discussion.updownvote')}}",
        data: {discussion_id: discussion_id, type:0, is_add:is_add}
    })
        .done(function (res) {
            $('.upvote-count_' + discussion_id).html(res.count_upvotes)
            $('.downvote-count_' + discussion_id).html(res.count_downvotes)

            if(_this.hasClass('disabled')){
                $('.downvote_' + discussion_id).removeClass('disabled')
            }else{
                $('.downvote_' + discussion_id).addClass('disabled')
            }

            if(!$(`.upvote_${discussion_id}`).hasClass('disabled')){
                $(`.upvote_${discussion_id}`).addClass('disabled')
            }

        });
    e.preventDefault()
});



function answerFilterOption() {
    //Answers Filter type select
    $(".sort-select").each(function () {
        var classes = $(this).attr("class"),
                id = $(this).attr("id"),
                name = $(this).attr("name"),
                select = $(this).attr("data-type"),
                type = $(this).attr("data-sorted_by"),
                disc_id = $(this).attr("data-dis_id");

        var template = '<div class="' + classes + '" data-type="' + disc_id + '">';
        template += '<span class="sort-select-trigger">' + $(this).attr("placeholder") + '</span>';
        template += '<div class="sort-options">';
        $(this).find("option").each(function () {
            template += '<span class="sort-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
        });
        template += '</div></div>';

        $(this).wrap('<div class="sort-select-wrapper"></div>');
        $(this).hide();
        $(this).after(template);
    });

    $(".sort-option:first-of-type").hover(function () {
        $(this).parents(".sort-options").addClass("option-hover");
    }, function () {
        $(this).parents(".sort-options").removeClass("option-hover");
    });
    $(".sort-select-trigger").on("click", function () {
        $('html').one('click', function () {
            $(".sel-select").removeClass("opened");
        });
        $(this).parents(".sort-select").toggleClass("opened");
        event.stopPropagation();
    });


    $(".sort-option").on("click", function () {
        $(this).parents(".sort-options").find(".sort-option").removeClass("selection");
        $(this).addClass("selection");
        $(this).parents(".sort-select").removeClass("opened");
        $(this).parents(".sort-select").find(".sort-select-trigger").html($(this).html());

        var discussion_id = $(this).parents(".sort-select").attr('data-type');
        var type = $(this).data("value");

        getFilterAnswers(discussion_id, type)

    });

}

function getFilterAnswers(discussion_id, type) {
    $("#preLoader" + discussion_id).show();
    $('.reply-block-' + discussion_id).html('');
    $.ajax({
        method: "POST",
        url: "{{route('discussion.filter_answers')}}",
        data: {discussion_id: discussion_id, type: type},
    })
            .done(function (res) {
                $("#preLoader" + discussion_id).hide()
                var result = JSON.parse(res);
                $('.reply-block-' + discussion_id).html(result);

            });
}



 function repliesEmoji(element, type=''){
    element.find("textarea.disc-reply-emoji").emojioneArea({
        autoHideFilters   : true,
        pickerPosition: "bottom",
         tonesStyle: "bullet",
        hideSource : true,
        autocomplete: true,
        events: {
                onLoad: function () {
                    if(type == ''){
                        element.find('.emojionearea-button').addClass('d-none')
                    }
                },
                emojibtn_click: function (button, event) {
                   $('.emojionearea-button.active').click()
                  },
                keyup: function (editor, event) {
                    element.find('.reply-media-error').html('')
                    if(type == ''){
                        if (editor.html() != '') {
                            element.find('.replay-action-block').removeClass('d-none')
                            element.find('.reply-media-block').removeClass('d-none')
                            element.find('.emojionearea-button').removeClass('d-none')
                        } else {
                            element.find('.replay-action-block').addClass('d-none')
                            element.find('.reply-media-block').addClass('d-none')
                            element.find('.emojionearea-button').addClass('d-none')
                        }
                    }
                },
            }
    });
    }



function repliesMediaUploadFile(_this, form_obj) {
    form_obj.find('.reply-media-error').html('')
    
    if (window.File && window.FileReader && window.FileList && window.Blob) { //check File API supported browser
        
        var data = _this[0].files;
        $.each(data, function (index, file) { //loop though each file

            if (/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)) {
                if(file.size > 10485760){
                    form_obj.find('.reply-media-error').html('The maximum size of image 10MB.')
                    return;
                }else{
                     form_obj.find('.reply-media-error').html('')
                }
 

                var fRead = new FileReader();
                fRead.onload = (function (file) {
                    return function (e) {

                        var img = $('<img/>').addClass('disc-media-thumb').attr('src', e.target.result); //create image element

                        var button = $('<span/>').addClass('disc-media-close').addClass('removeFile').click(function () {     //create close button element
                            form_obj.find('.reply-media-inner').html('');
                            form_obj.find('.replies-loader').addClass('d-none');
                            form_obj.find('.media-type').val('')
                            form_obj.find('.media-url').val('')
                        }).append('<span>&times;</span>');

                        var imgitem = $('<div>').append(img); //create Image Wrapper element
                        imgitem = $('<div/>').addClass('img-wrap-newsfeed').append(imgitem).append(button);
                        form_obj.find('.reply-media-inner').append(imgitem);
                        form_obj.find('.replies-loader').removeClass('d-none');
                        uploadRepliesMedia('image', file, form_obj)

                    };
                })(file);
                fRead.readAsDataURL(file); //URL representing the file's data.

            } else if (/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type)) {
                form_obj.find('.reply-media-error').html('Please select a valid Image File.')
                return false;
            }
        });
        form_obj.find('textarea').focus();
        _this.val("");

    } else {
        alert("Your browser doesn't support File API!"); //if File API is absent
    }
}


//Upload replies media  
function uploadRepliesMedia(type, media, form_obj){
    var fd = new FormData();
    fd.append('replies_media',media);
    fd.append('type', type);
    $('.disc-reply-input').prop('disabled', true)
    $.ajax({
        method: "POST",
        url: "{{route('discussion.upload_replies_media')}}",
        data: fd,
        contentType: false,
        processData:false,
        async: true,
        xhr: function() {
            var xhr = $.ajaxSettings.xhr() ;
            xhr.upload.addEventListener("progress", function(evt) {

                if (evt.lengthComputable) {
                    var percentComplete = ((evt.loaded / evt.total) * 100);
                    form_obj.find(".replies-loader .progress-bar").width(percentComplete + '%');
                }
            }, false);
            return xhr;
        },
        beforeSend: function(){
            form_obj.find(".replies-loader .progress-bar").width('0%');
        },
      }).done(function(data){
        
          if(data){
                $('.disc-reply-input').prop('disabled', false)
                form_obj.find(".replies-loader").addClass('d-none')
                form_obj.find('.media-type').val(data.type)
                form_obj.find('.media-url').val(data.replies_media)
          }

      })
}

//Edit reply media
function editRepliesMedia(id) {
     $('.replyeditfile' + id).on('change', function () {
    
       $('.discussionReplyEditForm' + id).find('.reply-media-inner').html('');
          repliesMediaUploadFile($(this), $('.discussionReplyEditForm' + id));
     });
 }

  // for spam reporting
 function injectReplyData(id, obj)
    {
        // var button = $(event.relatedTarget);
        var posttype = $(obj).parent().attr("posttype");

        $("#replySpamReportDlg").find("#dataid").val(id);
        $("#replySpamReportDlg").find("#posttype").val(posttype);
    }



</script>
