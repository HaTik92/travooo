<div class="post-block post-side-block">
    <div class="post-side-top">
        <h3 class="side-ttl">
            Trending Topics<span class="count topic-count"></span>
        </h3>
    </div>
    <div class="trending-topic-block">
        <div class="trending-topic-inner">
            <ul class="trending-topic-list">
                @foreach($trending_topics as $k=>$trending_t)
                    @if($k < 30)
                        <li class="trending-topic-wrap trending-topics" topicId="{{$trending_t}}">
                            <span>{{$trending_t}}</span>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</div>