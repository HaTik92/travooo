<script data-cfasync="false" src="https://underscorejs.org/underscore-min.js" type="text/javascript"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="{{ asset('assets2/js/lightbox2/src/css/lightbox.css') }}" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script data-cfasync="false" src="https://podio.github.io/jquery-mentions-input/lib/jquery.elastic.js" type="text/javascript"></script>
<script data-cfasync="false" src="{{ asset('assets2/js/jquery.inview/jquery.inview.min.js') }}" type="text/javascript"></script>

<script data-cfasync="false" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
<script data-cfasync="false" src="{{asset('assets2/js/lightbox2/src/js/lightbox.js')}}" type="text/javascript"></script>
<script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js" type="text/javascript"></script>
<script data-cfasync="false" src="{{ asset('/plugins/emoji/emojionearea.js?v='.time()) }}" type="text/javascript"></script>
<script data-cfasync="false" src="{{ asset('assets2/js/posts-script.js?v='.time()) }}" type="text/javascript"></script>

<style>
    .select2-input {
        height:49px !important;
        padding:8px !important;
        border: 1px solid #e6e6e6 !important;
    }
    .select2-selection__choice {
        background: #f7f7f7 !important;
        padding: 5px 10px !important;
        position: relative !important;
        border-radius: 0 !important;
        color: #4080ff !important;
        margin-top:0px !important;
    }
    .select2-selection__arrow{
        top:11px !important;
    }
    .ui-state-highlight {
        height:25px;
        background-color:#c4d9ff;
    }
    .padding-10{
        padding: 10px 0 !important;
    }
    em {
        font-style: italic;
    }

    .latest-link{
        padding-left: 10px;
    }
</style>
@include('site.discussions.partials._view-scripts')
<script data-cfasync="false" type="text/javascript">
    var upVoteText = '@lang('home.upvote')';
    var downVoteText = '@lang('home.downvote')';
    var globalFilters = {};
    var question_title = '';
    var discussion_type = '';
    var destination_title = '';
    var order = 'popular';
    var fil_type = '';
    var trending_topic = '';
    var answer_user_id = '';
    var answer_user_name = '';
    var this_week_answer = '';

if (location.href.substring(location.href.lastIndexOf('/') + 1) != 'discussions') {
    // type = location.href.substring(location.href.lastIndexOf('/') + 1);
}

if (history.scrollRestoration) {
    history.scrollRestoration = 'manual';
} else {
    window.onbeforeunload = function () {
        window.scrollTo(0, 0);
    }
}
$(document).ready(function () {
    loadDiscussion();

      var hash = window.location.hash;
        if (hash != "") {
            if(hash === '#create'){
                setTimeout(function(){ $('.btn-add-discussion').click()}, 100);
                window.history.pushState({}, document.title, "/discussions");
            }else{
             var disc_id = hash.split(/[-]+/).pop()
             if(disc_id){
                 $('.hashed-link').attr('data-dis_id', disc_id)
                 setTimeout(function(){ $('.hashed-link').click()}, 100);
             }
            }
        }

//share portion

    $('#shareablePost').on('show.bs.modal',function (event){
        $('#shared-post-wrap-content').empty();
        id = event.relatedTarget.dataset.id;
        fil_type = event.relatedTarget.dataset.type;
        $('#share-post-id').val(id);
        $('#share-post-type').val(type);
        $.ajax({
            method: "GET",
            url: '{{  url("get-shareable-post")}}',
            data: {id: id,'type':type}
        })
            .done(function (res) {
                if(res !=0)
                    $('#shared-post-wrap-content').html(res);
            });
    });


    lightbox.option({
        'disableScrolling': true,
        'alwaysShowNavOnTouchDevices': true,
        'maxWidth': 740,
        'maxHeight': 540,
        'positionFromTop': 145,
        'resizeDuration': 350
    });

    
    //Get my question     
    $(document).on('click', ".disc-my-questions-link", function () {
        fil_type = 3;
       answer_user_id = '';
       answer_user_name = '';
       loadSearchContent()
    });
    
    //Get my replies    
    $(document).on('click', ".disc-my-replies-link", function () {
        fil_type = 2;
       answer_user_id = '';
       answer_user_name = '';
       loadSearchContent()
    });


    $(document).on('click', '.upvote-link.up', function (e) {
        var reply_id = $(this).attr('id');
        var parent_content = $('.updown_' + reply_id);
        var _this = parent_content.find('.upvote-link.up');
        var vote_text = $('.vote-text');
        var vote_link = vote_text.closest('a.upvote-link');
        $.ajax({
            method: "POST",
            url: "{{route('discussion.upvote')}}",
            data: {replies_id: reply_id}
        }).done(function (res) {
            parent_content.find('.upvote-count').html(res.count_upvotes);
            parent_content.find('.downvote-count').html(res.count_downvotes);

            if (!_this.length || _this.hasClass('disabled')) {
                vote_text.html(downVoteText);
                vote_link.removeClass('up');
                vote_link.addClass('down')
            } else {
                vote_text.html(upVoteText);
                vote_link.removeClass('down');
                vote_link.addClass('up')
            }

            if(_this.hasClass('disabled')){
                _this.removeClass('disabled');
            }else{
                _this.addClass('disabled');
            }

            if(!parent_content.find('.upvote-link.down').hasClass('disabled')){
                parent_content.find('.upvote-link.down').addClass('disabled')
            }
        });

        e.preventDefault()
    });

    $(document).on('click', '.upvote-link.down', function (e) {
        var reply_id = $(this).attr('id');
        var parent_content = $('.updown_' + reply_id)
        var _this = parent_content.find('.upvote-link.down');
        $.ajax({
            method: "POST",
            url: "{{route('discussion.downvote')}}",
            data: {replies_id: reply_id}
        }).done(function (res) {
            parent_content.find('.upvote-count').html(res.count_upvotes)
            parent_content.find('.downvote-count').html(res.count_downvotes)

            if(_this.hasClass('disabled')){
                _this.removeClass('disabled')
            }else{
                _this.addClass('disabled')
            }

            if(!parent_content.find('.upvote-link.up').hasClass('disabled')){
                parent_content.find('.upvote-link.up').addClass('disabled')
            }

            $('.vote-text').html(upVoteText);
            $('.vote-text').closest('a.upvote-link').removeClass('down');
            $('.vote-text').closest('a.upvote-link').addClass('up');
        });
        e.preventDefault()
    });

    $(document).on('click', '.disc-modal-close', function(){
        window.history.pushState({}, document.title, "/discussions");
        $('#discussion_view').modal('hide')
    })

    //Get user all answers
    $(document).on('click', ".disc-view-us-all-answer", function () {
        answer_user_id = $(this).attr('userId');
        answer_user_name = $(this).attr('userName');
        fil_type = ''
        loadSearchContent()
    });



    $('body').on('DOMNodeInserted', '#destination_searchbox', function () {
        $(this).find("#remove_temp_destination").remove();
    });

    $('body').on('DOMNodeInserted', '#discussion_orderbox', function () {
        $(this).find("#remove_temp_order").remove();
    });

    function isEnlargedViewPage() {
        return window.location.href.indexOf("enlarged-views") > -1;
    }


    if (isEnlargedViewPage()) {
        $('.disc-post-modal').click();
    }

    $('#discussion_order').select2({
    minimumResultsForSearch: Infinity,
            placeholder: '@lang('discussion.strings.order_by') ...',
    debug: true,
            containerCssClass: "select2-input",
    }
    );

    $('#discussion_order').on('select2:select', function (e) {
        var data = e.params.data;
        if (data.id != '') {
            order = data.id;
            loadSearchContent();
        }
    });

    $('#disc_search_destination_input').keyup(delay(function (e) {
        let value = $(e.target).val();
        getDestinationSearch(value, $(this), '#destination_searchbox', 'dest-searched-result');
    }, 500));

    $(document).on('click', '#destination_searchbox .dest-searched-result', function (){
        var id = $(this).data('select-id')
        var text = $(this).data('select-text')

        destination_title = text;
        discussion_type = id;

        getTrendingTopics(id)
        loadSearchContent();
        getThisWeekAnswers(discussion_type)

        $('#destination_searchbox').find('.dest-search-result-block').html('')
        $('#destination_searchbox').find('.dest-search-result-block').removeClass('open')
        $('#disc_search_destination_input').val(text)

    })


var discussion_slider = $(".people-asking-post-card").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true,
      responsive: [{
            breakpoint:767,
                settings: {
                slideMargin: 9,
            },
        }],
    })
    
    
     var check_view = true;
    $(window).on('scroll', function(){
        if(($('#disc_pagination').offset().top - $(window).scrollTop() < 2500) && check_view && !$('#disc_pagination').hasClass('d-none')){
            check_view = false
            
            var queryString = '?q=' + question_title + '&dest_type=' + discussion_type + '&type=' + fil_type + '&order=' + order + '&trending_type=' + trending_topic + '&answers_user_id=' + answer_user_id;
            var nextPage = parseInt($('#disc_pagenum').val()) + 1;

            var url = "{{route('discussion.ajax_search_discussion')}}" + queryString;

            $.ajax({
                type: 'GET',
                url: url,
                data: {pagenum: nextPage},
                success: function (data) {
                     check_view = true;
                    if(data.view){

                        $('.disc-main-block').append(data.view)
                        $('#disc_pagenum').val(nextPage);

                        if(data.trending_view) {
                            $(".people-asking-post-card").last().lightSlider({
                                autoWidth: true,
                                pager: false,
                                controls: false,
                                slideMargin: 20,
                                loop: true,
                                onSliderLoad: function (el) {
                                    $(el).closest('.post-block-discussion-slider').find('.discussion-slide-prev').bind('click', function(){
                                        el.goToPrevSlide();
                                    })

                                    $(el).closest('.post-block-discussion-slider').find('.discussion-slide-next').bind('click', function(){
                                        el.goToNextSlide();
                                    })
                                },
                            })
                        }
                    } else {
                        $('#disc_pagination').addClass('d-none');
                    }
                }
            });
        }

})
    
    
$('.discussion-slide-prev').on('click', function () {
    discussion_slider.goToPrevSlide(); 
})
    
$('.discussion-slide-next').on('click', function () {
    discussion_slider.goToNextSlide(); 
})


// load more helpful  
    $(document).on('inview', '#hide_hf_loader', function (event, isInView) {
        if (isInView) {
            var nextPage = parseInt($('#helpful_pagenum').val()) + 1;

            $.ajax({
                type: 'POST',
                url: "{{route('discussion.get_more_helpful')}}",
                data: {pagenum: nextPage, this_week_answer:this_week_answer},
                success: function (data) {
                    if (data != '') {
                        $('.disc-helpful-block').append(data)
                        $('#helpful_pagenum').val(nextPage);
                    } else {
                        $('#helpful_pagination').hide();
                    }
                }
            });
        }
    });


//Search discussion via question
    $(document).on('keyup', '#search_discussion_input, #search_discussion_input_mobile', delay(function () {
        question_title = $(this).val();
        loadSearchContent();
    }, 500))



//Clear search result
    $(document).on('click', '.clear-search i', function () {
        $('#search_discussion_input, #search_discussion_input_mobile').val('')
        $(this).closest('.search-result-block').html('')
        question_title = '';
        trending_topic = '';
        fil_type = '';
        destination_title = '';
        discussion_type = '';
        $("#destination_search").val('').change();
        answer_user_id = '';
        answer_user_name = '';
        
        getTrendingTopics('')
        loadSearchContent()
        getThisWeekAnswers('')
    })


//Get trending topics
    $(document).on('click', '.trending-topics', function () {
        var topic_type = $(this).attr('topicId')
        $('.trending-topic-list').find('li').removeClass('active')
        $(this).addClass('active')
        trending_topic = topic_type;
        loadSearchContent()
    })


//shere discussion
    $(document).on('click', '.shere-discussion-link', function () {
        var discussion_id = $(this).attr('discussionId')

        $.ajax({
            type: 'POST',
            url: "{{route('discussion.shareunshare')}}",
            data: {post_id: discussion_id},
            success: function (data) {
                var result = JSON.parse(data);
                if (result.status == 'yes') {
                    $('.disc-shere-notification').html("You have successfully shared!");
                } else {
                    $('.disc-shere-notification').html("You have already shared!");
                }

                $('#shereInfoPopup').modal('show')
            }
        });
    })

});



function loadDiscussion(){
    var url = "{{route('discussion.ajax_search_discussion')}}";
    var nextPage = 1
    $.ajax({
        type: 'GET',
        url: url,
        data: {pagenum: nextPage, first_load:true},
        success: function (data) {
            if (data.view) {
                $('.disc-main-block').append(data.view)
                $('#disc_pagenum').val(nextPage);

            } else {
                $('#disc_pagination').addClass('d-none');
            }
        }
    });
}

$('body').on('click', '#forRecommendation', function (e) {
    $('#aboutRow').show();
    $('#tripRow').hide();
});
$('body').on('click', '#forTips', function (e) {
    $('#aboutRow').show();
    $('#tripRow').hide();
});
$('body').on('click', '#generalQuestion', function (e) {
    $('#aboutRow').show();
    $('#tripRow').hide();
});
$('body').on('click', '#aboutTripPlan', function (e) {
    $('#aboutRow').hide();
    $('#tripRow').show();
});


function loadSearchContent() {
    var queryString = '?q=' + question_title + '&dest_type=' + discussion_type + '&type=' + fil_type + '&order=' + order + '&trending_type=' + trending_topic + '&answers_user_id=' + answer_user_id;

    var url = "{{route('discussion.ajax_search_discussion')}}" + queryString;
    $('.disc-main-block').html('')
    $('#disc_pagination').removeClass('d-none');

    $.ajax({
    type: 'GET',
            url: url,
            data: {pagenum: 1, query:'search'},
            success: function (data) {
                if (data.view) {
                $('.disc-main-block').html(data.view)
                    $('#disc_pagination').removeClass('d-none');

                } else {
                    if($('.disc-inner').length > 1){
                        $('.disc-main-block .disc-inner').not(':first').remove()
                        $('.disc-main-block .search-result-block').not(':first').remove()
                        $('.disc-main-block .post-block-discussion-slider').not(':first').remove()
                    }

                    $('#disc_pagination').addClass('d-none');

                    $('.disc-inner').html('<div class="post-block post-top-bordered" style="padding:10px;">@lang('discussion.no_discussions_dots')</div>');
                }
                getSearchResult(question_title, discussion_type, fil_type, trending_topic, answer_user_id)
            }
    }
    );
}

function getSelectedHtml(title, class_name, data_attr, id) {
    var html = '<li><span class="search-selitem-title">' + title + '</span>' +
            '<span class="close-search-item ' + class_name + '" data-' + data_attr + '="' + id + '"> x</span></li>'

    return html;
}


function markLocetionMatch(text, term) {
    var regEx = new RegExp("(" + term + ")(?!([^<]+)?>)", "gi");
    if(text != null){
        var output = text.replace(regEx, "<span class='select2-locetion-rendered'>$1</span>");
        return output;
    }
}


function getLoadingHtml() {
    var html = `<div style='text-align:center;'>
            <div class="post-animation-content post-block">
                <div class="post-top-info-layer">
                    <div class="post-top-info-wrap">
                        <div class="post-top-avatar-wrap animation post-animation-avatar"></div>
                        <div class="post-animation-info-txt">
                            <div class="post-top-name animation post-animation-top-name"></div>
                            <div class="post-info animation post-animation-top-info"></div>
                        </div>
                    </div>
                </div>
                <div class="post-animation-conteiner">
                    <div class="post-animation-text animation pw-2"></div>
                    <div class="post-animation-text animation pw-3"></div>
                </div>
                <div class="post-animation-footer">
                    <div class="post-animation-text animation pw-4"></div>
                </div>
            </div>
        </div>`;


    return html;
}

function reply_textarea_auto_height(element) {
    element.style.height = "78px";
    element.style.paddingBottom = '8px';
    element.style.height = (element.scrollHeight) + "px";
}


function getTrendingTopics(destination_place) {
    $.ajax({
        method: "POST",
        url: "{{route('discussion.get_trensing_topic_by_place')}}",
        data: {destination_place: destination_place},
    })
            .done(function (res) {
                if (res != '') {
                    $('.side-topic-box').html(res)
                } else {
                    $('.side-topic-box').html('')
                }
            });
}

function getSearchResult(question_title, discussion_type, fil_type, trending_topic, answer_user_id){
    console.log('type', type)
    search_type = '';
    if(fil_type != ''){
        if(fil_type == 3)
            search_type = 'in <b>your discussions</b>';
        else
            search_type = 'in <b>your replies</b>';
    }
    
    if(answer_user_id != ''){
        search_type = 'in <b>'+ answer_user_name +'\'s replies</b>';
    }
    
    var search_text ='Results '+ search_type + ((question_title != '')?' for <b>' + question_title + '</b>':'') +' '+ ((discussion_type != '')?'in <b>' + destination_title + '</b>':'') + ' ' +
                                ((trending_topic != '')?' including <b>' + trending_topic + '</b> topic':'');


    if(question_title !='' || discussion_type !='' || fil_type !='' || trending_topic !='' || answer_user_id !=''){
         $('.search-result-block').html('<div class="post-block post-top-bordered" style="padding:10px;">' +
                search_text +
                '<span class="clear-search"><i class="fa fa-times" aria-hidden="true"></i></span>' +
                '</div>')
    }else{
        $('.search-result-block').html('');
    }
}


function getThisWeekAnswers (destination_type){
     $.ajax({
        type: 'POST',
        url: "{{route('discussion.get_more_helpful')}}",
        data: {destination_type:destination_type},
        success: function (data) {
            if (data != '') {
                $('.most-helpful-block').show()
                $('.disc-helpful-block').html('')
                $('.disc-helpful-block').append(data)
            } else {
               $('.most-helpful-block').hide()
            }
        }
    });
}



</script>
