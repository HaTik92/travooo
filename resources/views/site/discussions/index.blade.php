@extends('site.layouts.site')
@php $title = 'Travooo - Discussions'; @endphp
@section('content-area')

@include('site.layouts._left-menu')
@section('after_styles')
<link href="{{ asset('/plugins/emoji/emojionearea_custom.css') }}" rel="stylesheet"/>
<style type="text/css">
    .select2-container .select2-search.select2-search--inline {
        /*width: 100% !important;*/
        height: 35px !important;
    }
    .select2-search__field {
        width: 100% !important;
    }
    .select2-selection__rendered{
        height: 35px !important;
        overflow-y: auto !important;
    }
    .select2-selection__choice{
        background-color: #fff !important;
    }
    .drag-over{
        border: 1px dashed;
        border-radius: 10px;
        opacity: 0.5;
    }
    .disc-post-modal {
        color: #000;
        cursor: pointer;
        overflow-wrap: anywhere;
    }



</style>
<link href="{{ asset('/dist-parcel/assets/Discussion/main.css') }}" rel="stylesheet"/>
@endsection
<div class="custom-row discussions-custom-row-wrapper mobile-header__helper" data-AEA-385>
    <div class="report-offline-status">
        <div class="rep-ofline-block"></div>
        No internet Connection...
    </div>
    <!-- MAIN-CONTENT -->
    <div class="main-content-layer flex-first grid-layer hidden-more-sm">
        <div class="search-discussion-input-wrapper" id="destination_searchbox_mobile">
            <i class="trav-search-icon" dir="auto"></i>
            <input type="text" class="search-discussion-input" id="search_discussion_input_mobile" placeholder="Search in Q&A...">
        </div>
        <nav class="tab-container">
            <ul class="tab-list list-style" data-attr="tablist">
{{--                <li class="tab-list-item">--}}
{{--                    <a href="#" class="tab active" id="tab1" data-attr="tab">--}}
{{--                        Questions--}}
{{--                    </a>--}}
{{--                </li>--}}
                @if(count($trending_topics) > 5)
                    <li class="tab-list-item">
                        <a href="#discussionsTabPanel_Topics" class="tab" id="tab2" data-attr="tab">
                            Topics
                        </a>
                    </li>
                @endif
                <li class="tab-list-item">
                    <a href="#discussionsTabPanel_Activity" class="tab" id="tab3" data-attr="tab">
                        Activity
                    </a>
                </li>
                @if(count($most_helpful) > 0)
                    <li class="tab-list-item">
                        <a href="#discussionsTabPanel_MostHelpful" class="tab" id="tab4" data-attr="tab">
                            Most Helpful
                        </a>
                    </li>
                @endif
            </ul>
        </nav>
    </div>

    <div class="main-content-layer" id="discussionsTabPanel_Questions">
        <div class="hidden-more-sm">
            @if(Auth::check())
                <button type="button" class="btn btn-add-discussion btn-aq" data-toggle="modal" data-target="#askingPopup">
                    <img class="ava" src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt="">
            @else
                        <button type="button" class="btn btn-add-discussion open-login">
            @endif
                            Ask a question...
                        </button>
        </div>

        <div class="post-block post-country-block">
            <div class="post-side-top padding-10">
                <div class="col-md-5 pr-0 search-discussion-input-wrapper hidden-less-sm" id="">
                    <i class="trav-search-icon" dir="auto"></i>
                    <input type="text" class="search-discussion-input" id="search_discussion_input" placeholder="Search...">
                </div>

                <div class="col-md-4 hidden-less-sm disc-destination-searchbox" id="destination_searchbox">
                    <input type="search" class="disc-search-destination-input" id="disc_search_destination_input" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="@lang('discussion.strings.place_city_country')">
                    <span class="dest-search-result-block dest-search"></span>
                </div>

                <div class="col-md-3 pl-0 hidden-less-sm" id="discussion_orderbox">
                    <span class="select2 select2-container select2-container--default" id="remove_temp_order" style="width: 100%;">
                        <span class="selection">
                            <span class="select2-selection select2-selection--single select2-input">
                                <span class="select2-selection__rendered">
                                    <span class="select2-selection__placeholder">@lang('discussion.strings.order_by') ...</span>
                                </span>
                                <span class="select2-selection__arrow">
                                    <b></b>
                                </span>
                            </span>
                        </span>
                        <span class="dropdown-wrapper"></span>
                    </span>
                    <select  id="discussion_order"
                             placeholder="@lang('discussion.strings.order_by')..."
                             style="width: 100%;display:none">
                        <option value="popular">Popular</option>
                        <option value="datedesc">Newest</option>
                        <option value="dateasc">Oldest</option>
                    </select>
                </div>

                <form class="destination-searchbox custom-row report-custom-row hidden-more-sm" action="" method="get" autocomplete="off">
                    <h3 class="mobile-filter-toggler">Filter</h3>
                    <div class="post-collapse-block">
                        <div class="post-collapse-inner report-collapse-inner" id="travelMateAccordion" role="tablist"
                             aria-multiselectable="true">
                            <div class="travlogSelected rep-selected-block trip-tab-block current-tab" id='travlogSelectAll' ftype="all">
                                <input type='radio' name='order' value='' checked>
                                <label >@lang('other.all')</label>
                            </div>

                            <div class="card rep-selected-block" ftype="reportsCountries">
                                <div class="card-header" role="tab" id="reports_countries">
                                    <div class="collapsed travlogSelected trip-tab-block"  id='travlogSelectCountries'>
                                        <input type='radio' name='order' value=''>
                                        <label >@lang('report.countries')</label>
                                    </div>
                                </div>
                                <div id="reportsCountries" class="report-loc-filter-block">
                                    <div class="card-block">
                                        <div class="report-input-wrap" id="filterForCountry">
                                            <input name="filter_country" id="filter_country" placeholder="Filter by countries..." class="input filter-country-input report-filter-input">
                                            <div class="filtered-country-list">
                                                <ul class="filter-country-block">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card rep-selected-block" ftype="reportsCities">
                                <div class="card-header" role="tab" id="reports_cities">
                                    <div class="collapsed travlogSelected trip-tab-block" id='travlogSelectCities'>
                                        <input type='radio' name='order' value=''  id='travlogInputSelectCities' >
                                        <label >@lang('report.cities')</label>
                                    </div>
                                </div>
                                <div id="reportsCities" class="report-loc-filter-block">
                                    <div class="card-block">
                                        <div class="report-input-wrap">
                                            <input name="filter_city" id="filter_city" placeholder="Filter by cities..." class="input filter-city-input report-filter-input" >
                                            <div class="filtered-city-list">
                                                <ul class="filter-city-block">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="tab-discussion_order grid-layer hidden-more-sm" id="discussion_order_mobile">
                    <span class="text">Show</span>
                    <ul class="tab-list list-style">
                        <li class="tab-list-item">
                            <a href="#" class="tab tab-order active" data-tab-filter="popular">Most Popular</a>
                        </li>
                        <li class="tab-list-item">
                            <a href="#" class="tab tab-order" data-tab-filter="datedesc">Newest</a>
                        </li>
                        <li class="tab-list-item">
                            <a href="#" class="tab tab-order" data-tab-filter="dateasc">Oldest</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="hashed-link" data-dis_id=""></div>
        <div class="disc-alert-block">
            @if(session()->has('success'))
            <div class="alert alert-success" id="postSavedSuccess">
                {{ session()->get('success') }}
            </div>
            @endif
            @if ($errors->has('question'))
            <div class="alert alert-danger" id="postSavedDanger">
                {{ $errors->first('question') }}
            </div>
            @endif
            @if ($errors->has('description'))
            <div class="alert alert-danger" id="postSavedDanger">
                {{ $errors->first('description') }}
            </div>
            @endif
        </div>


        <div class="disc-main-block">
        </div>
        <div id="disc_pagination" style='text-align:center;'>
            <input type="hidden" id="disc_pagenum" value="1">
            <div id="dicussion_loader" class="post-animation-content post-block">
                <div class="post-top-info-layer">
                    <div class="post-top-info-wrap">
                        <div class="post-top-avatar-wrap animation post-animation-avatar"></div>
                        <div class="post-animation-info-txt">
                            <div class="post-top-name animation post-animation-top-name"></div>
                            <div class="post-info animation post-animation-top-info"></div>
                        </div>
                    </div>
                </div>
                <div class="post-animation-conteiner">
                    <div class="post-animation-text animation pw-2"></div>
                    <div class="post-animation-text animation pw-3"></div>
                </div>
                <div class="post-animation-footer">
                    <div class="post-animation-text animation pw-4"></div>
                </div>
            </div>
        </div>

    </div>
    @if(isMobileDevice())
    @if(count($trending_topics) > 0)
        <div class="tab-panel hidden-more-sm" id="discussionsTabPanel_Topics" data-attr="tab-panel">
            <div class="side-topic-box">
                @include('site.discussions.partials._topic-block', ['trending_topics'=>$trending_topics])
            </div>
        </div>
    @endif
    @if(Auth::check())
        <div class="tab-panel hidden-more-sm" id="discussionsTabPanel_Activity" data-attr="tab-panel">
            <div class="post-block post-side-block">
                <div class="post-side-top">
                    <h3 class="side-ttl">
                        @lang('discussion.strings.your_activity')
                    </h3>
                </div>
                <div class="post-side-inner">
                    <div class="activity-wrapper">
                        <div class="activity-top-info">
                            <div class="info-block">
                                <div class="count my-disc-count">{{$answers}}</div>
                                <div class="label">
                                    <a href="javascript:;" class="activity-link disc-my-questions-link">Discussions</a>
                                </div>
                            </div>
                            <div class="info-block">
                                <div class="count">{{$suggestions}}</div>
                                <div class="label">
                                    <a href="javascript:;" class="activity-link disc-my-replies-link">Replies</a>
                                </div>
                            </div>
                        </div>
                        <div class="activity-question-wrap">
                            <h4 class="question-ttl">Recent Discussions</h4>
                            <ul class="question-list w-100">
                                @foreach($my_questions AS $question)
                                    <li class="disc-res-link-{{$question->id}}">
                                        <a href="#" class="question-link disc-question-link  disc-post-modal"
                                           data-dis_id="{{$question->id}}"> {!! str_limit($question->question, 50, ' ...?') !!}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(count($most_helpful) > 0)
        <div class="tab-panel hidden-more-sm" id="discussionsTabPanel_MostHelpful" data-attr="tab-panel">
            <div class="post-block post-side-block most-helpful-block">
                <div class="post-side-top">
                    <h3 class="side-ttl">@lang('discussion.strings.most_helpful') <span class="disc-most-helpful-wrap">(@lang('discussion.strings.this_week'))</span>
                    </h3>
                </div>
                <div class="info-list-wrap" style="overflow:hidden">
                    <div class="helpful-info-list mCustomScrollbar">
                        <div class="disc-helpful-block">
                            @include('site.discussions.partials._helpful-block')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif
    <!-- SIDEBAR -->
    <div class="sidebar-layer disc-sidebar-layer hidden-less-sm" id="sidebarLayer">
        <aside class="sidebar">
            <div class="post-side-top-btn">
                @if(Auth::check())
                    <button type="button" class="btn btn-add-discussion" data-toggle="modal" data-target="#askingPopup">
                @else
                    <button type="button" class="btn btn-add-discussion open-login">
                @endif
                    New Discussion
                </button>
            </div>
            <div class="side-topic-box">
            @if(count($trending_topics) > 0)
                 @include('site.discussions.partials._topic-block', ['trending_topics'=>$trending_topics])
            @endif
            </div>
            @if(Auth::check())
                <div class="post-block post-side-block">
                    <div class="post-side-top">
                        <h3 class="side-ttl">
                            @lang('discussion.strings.your_activity')
                        </h3>
                    </div>
                    <div class="post-side-inner">
                        <div class="activity-wrapper">
                            <div class="activity-top-info">
                                <div class="info-block">
                                    <div class="count my-disc-count">{{$answers}}</div>
                                    <div class="label">
                                        <a href="javascript:;" class="activity-link disc-my-questions-link">Discussions</a>
                                    </div>
                                </div>
                                <div class="info-block">
                                    <div class="count">{{$suggestions}}</div>
                                    <div class="label">
                                        <a href="javascript:;" class="activity-link disc-my-replies-link">Replies</a>
                                    </div>
                                </div>
                            </div>
                            <div class="activity-question-wrap">
                                <h4 class="question-ttl">Recent Discussions</h4>
                                <ul class="question-list w-100">
                                    @if(count($my_questions) > 0)
                                    @foreach($my_questions AS $question)
                                    <li class="disc-res-link-{{$question->id}}">
                                        <a href="#" class="question-link disc-question-link  disc-post-modal" data-dis_id="{{$question->id}}"> {!! str_limit($question->question, 50, ' ...?') !!}</a>
                                    </li>
                                    @endforeach
                                    @else
                                        <li class="">
                                            <span class="disc-question-empty-block">No data found ...</span>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if(count($most_helpful) > 0)
            <div class="post-block post-side-block most-helpful-block">
                <div class="post-side-top">
                    <h3 class="side-ttl">@lang('discussion.strings.most_helpful') <span class="disc-most-helpful-wrap">(@lang('discussion.strings.this_week'))</span></h3>
                </div>
                <div class="info-list-wrap" style="overflow:hidden">
                    <div class="helpful-info-list mCustomScrollbar">
                        <div class="disc-helpful-block">
                           @include('site.discussions.partials._helpful-block')
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </aside>
    </div>
</div>

<div class="modal fade d-shere-info" id="shereInfoPopup" tabindex="-1" data-backdrop="true" aria-hidden="true">
    <div class="modal-dialog modal-custom-style" role="document">
        <div class="disc-shere-notification" style="padding: 10px; text-align: center; background: #32c984; color: #fff; border: 0; box-shadow: 0 4px 10px rgba(0,0,0,0.1); border-radius: 4px; padding: 18px 30px;">
        </div>
    </div>
</div>


@endsection

@section('before_scripts')
@if(Auth::check())
    @include('site.discussions._modals')
    @include('site.home.partials._spam_dialog')
    @include('site.home.new.partials._create-options-modal')
    @include('site.home.new.partials._share-modal')

@endif

<script data-cfasync="false">
    // for spam reporting
    function injectData(id, obj)
    {
        // var button = $(event.relatedTarget);
        var posttype = $(obj).parent().attr("posttype");
        $("#spamReportDlg").find("#dataid").val(id);
        $("#spamReportDlg").find("#posttype").val(posttype);
    }


</script>
@endsection

@section('before_site_script')
<link href="{{asset('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
<script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js" type="text/javascript"></script>
@endsection

@section('after_scripts')
@include('site.discussions._scripts')
@include('site.reports._filter_scripts')
<script data-cfasync="false" src="{{asset('/dist-parcel/assets/Discussion/main.js')}}"></script>
@endsection
