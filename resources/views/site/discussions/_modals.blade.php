<!-- asking popup -->
<div class="modal fade white-style" data-backdrop="false" id="askingPopup" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-750" role="document">
        <div class="modal-custom-block">
            <div class="post-block post-asking-block">
                <div class="post-asking-inner">
                    <form method="post" id="discussion_create_form" action="{{route('discussion.create')}}" style="width: 95%;">
                        <div class="avatar-wrap">
                            <img class="ava" src="{{check_profile_picture(Auth::user()->profile_picture)}}" alt=""
                                 style="width:50px;height:50px;">
                        </div>
                        <div class="ask-content">
                            <div class="ask-row-inner">
                                <div class="ask-row align-items-center">
                                    <div class="ask-label">@lang('discussion.strings.about')</div>
                                    <div class="ask-txt b-bottom-none">
                                        <div class="ask-input-wrap b-bottom-gray disc-destination-searchbox" id="destination_parent">
                                            <input type="search" class="disc-about-destination-input " id="disc_destination_about_input" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="@lang('discussion.strings.place_city_country')">
                                            <input type="hidden" name="destination_id" id="destination_id" value="">
                                            <span class="select-selection__arrow"><b></b></span>
                                            <span class="dest-search-result-block disc-about-results"></span>
                                        </div>
                                        <label id="destination_id-error" class="error" for="destination_id"></label>
                                    </div>
                                </div>
                                <div class="ask-row">
                                    <div class="ask-label">@lang('discussion.strings.title')</div>
                                    <div class="ask-txt">
                                        <div class="ask-input-wrap">
                                            <textarea name="question" id="question" maxlength="150" class="disc-textarea custom-p-holder" oninput="textarea_auto_height(this, '40px')" cols="" rows=""
                                                      placeholder="@lang('discussion.strings.write_your_question')..."
                                                      required maxlength="150"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="ask-row" style="margin-top: -20px">
                                    <div class="ask-label"></div>
                                    <div class="ask-input-wrap" id="question_hint"><span>150</span> characters remaining...
                                        <label id="question-error" class="error" for="question"></label>
                                    </div>
                                </div>
                                <div class="ask-row">
                                    <div class="ask-label">@lang('discussion.strings.description')</div>
                                    <div class="ask-txt">
                                        <div class="ask-input-wrap" style="display:block;">
                                            <textarea name="description" id="description" class="disc-textarea custom-p-holder" maxlength="1000" oninput="textarea_auto_height(this, '75px')" cols="5" rows="5" style="height:75px;"
                                                      placeholder="@lang('discussion.strings.write_more_details_about_your_question')..."
                                                      required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="ask-row" style="margin-top: -20px">
                                    <div class="ask-label"></div>
                                    <div class="ask-input-wrap" id="description_hint"><span>1000</span> characters remaining...
                                        <label id="description-error" class="error" for="description"></label>
                                    </div>

                                </div>
                                <div class="ask-row align-items-center">
                                    <div class="ask-label">Add Media</div>
                                    <div class="ask-inner">
                                        <input type="hidden" data-id="disc_pair" name="disc_pair" value="<?php echo uniqid(); ?>"/>
                                        <div class="ask-medias">
                                            <div class="media-plus-block active">
                                                <div class="ask-media-plus-icon">
                                                    <span class="ask-close"><span class="plus-icon-wrap">+</span></span>
                                                </div>
                                            </div>
                                            <div class="media-plus-block">
                                                <div class="ask-media-plus-icon">
                                                    <span class="ask-close"><span class="plus-icon-wrap">+</span></span>
                                                </div>
                                            </div>
                                            <div class="media-plus-block">
                                                <div class="ask-media-plus-icon">
                                                    <span class="ask-close"><span class="plus-icon-wrap">+</span></span>
                                                </div>
                                            </div>
                                            <div class="media-plus-block">
                                                <div class="ask-media-plus-icon">
                                                    <span class="ask-close"><span class="plus-icon-wrap">+</span></span>
                                                </div>
                                            </div>
                                            <div class="media-plus-block">
                                                <div class="ask-media-plus-icon">
                                                    <span class="ask-close"><span class="plus-icon-wrap">+</span></span>
                                                </div>
                                            </div>

                                            <input type="file" name="file[]" id="disc_file"  style="display:none" multiple>
                                        </div>
                                        <div class="disc-img-error-message" style="color: red;"></div>
                                    </div>
                                </div>
                                <div class="ask-row">
                                    <div class="ask-label">@lang('profile.topic')</div>
                                    <div class="ask-inner">
                                        <div class="ask-txt">
                                            <div class="ask-input-wrap">
                                                <input type="text" class="disc-topic-input custom-p-holder" maxlength="15" id="ask_topic" placeholder="@lang('profile.a_topic_for_your_question')">
                                            </div>
                                        </div>
                                        <label for="ask_topic" class="error ask-topic-error-block" style="color:red">
                                        </label>
                                        <input type="hidden" name="topics" id="topic_list">
                                        <ul class="search-selected-block topic-sel-block">
                                        </ul>
                                    </div>
                                </div>

                                <div class="ask-row ask-experts-block">
                                    <div class="ask-label">Ask Experts</div>
                                    <div class="ask-inner p-rel">
                                        <div class="ask-txt">
                                            <div class="ask-input-wrap disc-ask-expert-block">
                                                <input type="text" class="ask-experts-input custom-p-holder" id="ask_experts" placeholder="Select Experts..." disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="disc-ask-expert-error"></div>
                                        <input type="hidden" name="experts" id="experts_list">
                                        <ul class="search-selected-block experts-sel-block aln-left">
                                        </ul>
                                        <ul class="ask-experts-autocomplate"></ul>
                                    </div>
                                </div>

                                <div class="ask-row" id="tripRow" style="display:none;">
                                    <div class="ask-label">@lang('trip.trip_plan')</div>
                                    <div class="ask-txt">
                                        <div class="ask-input-wrap">
                                            <select name="trip_id" id="trip_id"
                                                    placeholder="@lang('discussion.strings.your_trip_plan')..."
                                                    style="width: 100%;height:50px;border: 5px solid red !important;">
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="ask-foot-btn">
                                <div class="upload-wrap">
                                    <div class="upload-blank">

                                    </div>
                                    <div class="upload-image">


                                    </div>
                                </div>
                                <div class="btn-wrap">
                                    <button class="btn btn-transp btn-clear" type="button" data-dismiss="modal">@lang('buttons.general.cancel')</button>
                                    <button type="submit" class="btn btn-light-primary btn-submit-disc" disabled>@lang('discussion.buttons.post_question')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('site.discussions.partials._ask_scripts')

