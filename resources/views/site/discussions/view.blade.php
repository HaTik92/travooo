<div class="modal white-style" data-backdrop="false" id="discussion_view" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true" data-AEA-385>
    <div class="modal-custom-style modal-850" role="document">
        <button class="modal-close disc-modal-close" type="button" data-dismiss="modal" aria-label="Close">
            <i class="trav-close-icon"></i>
        </button>
        <div class="modal-custom-block">

            <div class="post-block post-happen-question {{!Auth::check()?'mb-100':''}}">
                <div class="dis-loader-bar">
                    <span>
                        <img src="{{asset('assets2/image/preloader.gif')}}" width="100px">
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade disc-video-modal" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-740" role="document">
        <div class="modal-custom-block">
            <div class="modal-body">
                <video controls width="100%">
                    <source src="" type="video/mp4">
                </video>
            </div>
        </div>
    </div>
</div>
