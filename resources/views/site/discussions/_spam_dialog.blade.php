<!-- Spam Report Box Strat -->
<div class="modal fade" tabindex="-1" role="dialog" id="replySpamReportDlg" aria-hidden="true" style="z-index:1052 !important">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Why do you think this content should be reported?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="trav-close-icon" dir="auto"></i>
                </button>
            </div>
            <div class="modal-body" dir="auto">
                <div class="form-group" dir="auto">
                    <div class="form-check" dir="auto">
                        <i class="fa fa-exclamation-circle report-exclamation" aria-hidden="true" dir="auto"></i>
                        <h5 class="d-inline-block" dir="auto">What is the problem with this post?</h5>
                    </div>
                <div class="form-group" dir="auto">
                <div class="row" dir="auto">
                <div class="col-sm-12" dir="auto">
                <div class="container" dir="auto">
                    <div class="radio-tile-group" dir="auto">
                        <div class="input-container" dir="auto">
                            <input class="radio-button form-check-input" type="radio" name="type" id="gridRadios1" checked value="{{\App\Services\Discussions\SpamsRepliesService::REPORT_TYPE_SPAM}}">
                            <div class="radio-tile" dir="auto">
                                <div class="icon fly-icon" dir="auto">
                                    Spam
                                </div>
                            </div>
                        </div>
                        <div class="input-container" dir="auto">
                            <input class="radio-button form-check-input" type="radio" name="type" id="gridRadios2" value="{{\App\Services\Discussions\SpamsRepliesService::REPORT_TYPE_OTHER}}">
                            <div class="radio-tile" dir="auto">
                                <div class="icon fly-icon" dir="auto">
                                    Other
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                </div>
                <div class="form-group" dir="auto">
                    <div class="row" dir="auto">
                        <div class="col-sm-12" dir="auto">
                            <input type="text" class="form-control report-span-input" id="spamText" placeholder="@lang('profile.write_something')" autocomplete="off">
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn send-report-btn" id="replySpamSend">@lang('buttons.general.send')</button>
                <button type="button" class="btn send-report-btn dismiss" id="replySpamCancel" data-dismiss="modal">@lang('buttons.general.cancel')</button>
            </div>
            <input type="hidden" name="dataid" id="dataid">
            <input type="hidden" name="posttype" id="posttype">
        </div>
    </div>
</div>

<!-- Spam Report Box End -->

<script>
    // report spam......

    $("#replySpamReportDlg").on('hidden.bs.modal', function(event){
        var modal = $(this);
        modal.find("#dataid").val('');
        modal.find("#posttype").val('');
        modal.find("input[name=type]")[0].click();
        modal.find("#spamText").val("");
    });
    $("#replySpamReportDlg").find("input[type=radio]").click(function(){
        $(this).val() == '{{\App\Services\Discussions\SpamsRepliesService::REPORT_TYPE_SPAM}}' ?
            $("#replySpamReportDlg").find("#spamText").css("display", "none") :
            $("#replySpamReportDlg").find("#spamText").css("display", "block") ;
    });
    $("#replySpamReportDlg #replySpamSend").click(function(){
        var modal = $("#replySpamReportDlg");
        if(modal.find("input[name=type]:checked").val() == '{{\App\Services\Discussions\SpamsRepliesService::REPORT_TYPE_OTHER}}' && modal.find("#spamText").val() == "")
        {
            alert("Please input some text.");
            return;
        }
        var data = {
            type: modal.find("input[name=type]:checked").val(),
            text: modal.find("#spamText").val()
        };

        $.ajax({
            url: "{{route('discussion.reply.report_spam')}}" + '/' + modal.find("#dataid").val(),
            type: "POST",
            data: data,
            dataType: "json",
            success: function(data, status){
                alert(data);
                modal.find("#replySpamCancel").click();
            },
            error: function(){}
        });
    });
</script>
