<?php
use App\Models\City\Cities;
use App\Models\Country\Countries;
?>
@php
    $title = 'Travooo - flights, hotels';
@endphp

@extends('site.book.template.book')

@section('before-content')
<style>
    .select2-input {
        height:49px !important;
        padding:8px !important;
        border: 1px solid #e6e6e6 !important;
    }  
    .select2-selection__choice {
        background: #f7f7f7 !important;
    padding: 5px 10px !important;
    position: relative !important;
    border-radius: 0 !important;
        color: #4080ff !important;
        margin-top:0px !important;
    }
    .ui-state-highlight {
        height:25px;
        background-color:#c4d9ff;
    }
    em {
        font-style: italic;
    }

</style>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="top-banner-wrap">
        <div class="top-banner-block after-disabled book-top-banner-block"
             style="background-image: url({{url('assets2/image/flight-hotel-bg.jpg')}}">
            <div class="top-banner-flight-hotel">
                <ul class="flight-hotel-tabs" role="tablist">
                    <li>
                        <a href="{{route('book.hotel')}}">
                        <span class="circle-icon">
                          <i class="trav-building-icon"></i>
                        </span>
                            <span>@lang('book.hotels')</span>
                        </a>
                    </li>
                    <li>
                        <a class="current" href="{{route('book.flight')}}">
                    <span class="circle-icon">
                      <i class="trav-angle-plane-icon"></i>
                    </span>
                            <span>@lang('book.flights')</span>
                        </a>
                    </li>
                </ul>
                <div class="top-banner-inner">

                    <h2 class="ban-ttl">Fly around the globe...</h2>
                    <p>The best flight prices for your next destination.</p>
                </div>
                <form method="post" action="{{ route('book.search_flight')}}" class="top-banner-search-form">
                    <div class="flex-row">
                        <div class="flex-item fl-30 with-icon">
                            <span class="icon-wrap">
                              <i class="trav-location"></i>
                            </span>
                            <select class="flex-input" name="city_from" id="city_from"
                                    placeholder="@lang('chat.from')">
                            </select>

                        </div>
                        <div class="flex-item fl-30 with-icon">
                            <span class="icon-wrap">
                              <i class="trav-location"></i>
                            </span>
                            <select class="flex-input" name="city_to" id="city_to"
                                    placeholder="@lang('chat.to')">
                            </select>

                        </div>
                    </div>
                    <div class="flex-row">
                        <div class="flex-item fl-30 with-icon">
                            <span class="icon-wrap">
                              <i class="trav-event-icon"></i>
                            </span>
                            <input autocomplete="off" class="flex-input datepicker" name="date_1" id="date_1"
                                   placeholder="@lang('book.travel_date')" value="" type="text">

                        </div>

                        <div class="flex-item fl-30 with-icon">
                            <span class="icon-wrap">
                              <i class="trav-event-icon"></i>
                            </span>
                            <input autocomplete="off" class="flex-input datepicker" name="date_2" id="date_2"
                                   placeholder="@lang('book.return_date')" value="" type="text">

                        </div>
                        <div class="flex-item fl-10">
                            <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.search')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('content')
<div class="post-block post-flight-style book-tranding-flights">
          <div class="post-side-top">
            <h3 class="side-ttl"><i class="trav-trending-destination-icon"></i> Trending flights
            </h3>
            <div class="side-right-control">
              <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
              <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
            </div>
          </div>
          <div class="post-side-inner">
            <div class="post-slide-wrap">
              <ul id="trendingFlights" class="post-slider">
                <li>
                  <div class="post-flight-inner">
                    <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/184/500386deffcfbe0d054d6ff98dabfc3e03f3acef.jpg" style="width:327px;height:180px" alt="">
                    <div class="post-flight-info">
                      <div class="flight-inner">
                        <div class="fl-left">
                          <p class="city-name">Barcelona</p>
                          <p class="country-name">Spain</p>
                          <p class="going-count">
                            <span class="count">123</span>
                            <span>going today</span>
                          </p>
                        </div>
                        <div class="fl-right">
                          <div class="flag-wrap">
                            <img class="flag-image" src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/es.png" alt="" style="width:32px;height:22px;">
                          </div>
                          <div class="btn-wrap">
                            <button class="btn btn-light-primary btn-bordered">Book Flight</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <img src="https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyBMBJfsF6ISA-KY6BVW3g8q2swqPgG4s5w&size=640x400&format=jpg&scale=1&maptype=terrain&markers=anchor:32,10|icon:https://flugzeit-rechner.de/static/images/marker-departure.png|24.7136,46.67529999999999&markers=anchor:32,10|icon:https://flugzeit-rechner.de/static/images/marker-destination.png|41.3851,2.173400000000015&path=geodesic:true|24.7136,46.67529999999999|41.3851,2.173400000000015" alt="" style="width:327px;height:180px;">
                    <div class="post-direct-info">
                      <div class="direct-block">
                        <p class="dir-label">5h 45m</p>
                        <p class="dir-txt">Direct flight</p>
                      </div>
                      <div class="direct-block">
                        <p class="dir-label">4494.09 km </p>
                        <p class="dir-txt">Total distance</p>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="post-flight-inner">
                    <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/263/4a3c02827e2c4d8ed1f3b7a94d7e9d73c51e123b.jpg" style="width:327px;height:180px" alt="">
                    <div class="post-flight-info">
                      <div class="flight-inner">
                        <div class="fl-left">
                          <p class="city-name">Amalfi</p>
                          <p class="country-name">Italy</p>
                          <p class="going-count">
                            <span class="count">77</span>
                            <span>going today</span>
                          </p>
                        </div>
                        <div class="fl-right">
                          <div class="flag-wrap">
                            <img class="flag-image" src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/it.png" alt="" style="width:32px;height:22px;">
                          </div>
                          <div class="btn-wrap">
                            <button class="btn btn-light-primary btn-bordered">Book Flight</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <img src="https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyBMBJfsF6ISA-KY6BVW3g8q2swqPgG4s5w&size=640x400&format=jpg&scale=1&maptype=terrain&markers=anchor:32,10|icon:https://flugzeit-rechner.de/static/images/marker-departure.png|24.7136,46.67529999999999&markers=anchor:32,10|icon:https://flugzeit-rechner.de/static/images/marker-destination.png|40.634,14.602700000000027&path=geodesic:true|24.7136,46.67529999999999|40.634,14.602700000000027" alt="" style="width:327px;height:180px;">
                    <div class="post-direct-info">
                      <div class="direct-block">
                        <p class="dir-label">4h 31m</p>
                        <p class="dir-txt">Direct flight</p>
                      </div>
                      <div class="direct-block">
                        <p class="dir-label">3463.90 km</p>
                        <p class="dir-txt">Total distance</p>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="post-flight-inner">
                    <img src="https://s3.amazonaws.com/travooo-images2/th1100/cities/109/dfa74694cc57010292be192a9593711c9c8a9b4e.jpg" style="width:327px;height:180px" alt="">
                    <div class="post-flight-info">
                      <div class="flight-inner">
                        <div class="fl-left">
                          <p class="city-name">Tokyo</p>
                          <p class="country-name">Japan</p>
                          <p class="going-count">
                            <span class="count">64</span>
                            <span>going today</span>
                          </p>
                        </div>
                        <div class="fl-right">
                          <div class="flag-wrap">
                            <img class="flag-image" src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/jp.png" alt="" style="width:32px;height:22px;">
                          </div>
                          <div class="btn-wrap">
                            <button class="btn btn-light-primary btn-bordered">Book Flight</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <img src="https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyBMBJfsF6ISA-KY6BVW3g8q2swqPgG4s5w&size=640x400&format=jpg&scale=1&maptype=terrain&markers=anchor:32,10|icon:https://flugzeit-rechner.de/static/images/marker-departure.png|24.7136,46.67529999999999&markers=anchor:32,10|icon:https://flugzeit-rechner.de/static/images/marker-destination.png|35.6895,139.692&path=geodesic:true|24.7136,46.67529999999999|35.6895,139.692" style="width:327px;height:180px" alt="">
                    <div class="post-direct-info">
                      <div class="direct-block">
                        <p class="dir-label">10h 46m</p>
                        <p class="dir-txt">Direct flight</p>
                      </div>
                      <div class="direct-block">
                        <p class="dir-label">8718.94 km</p>
                        <p class="dir-txt">Total distance</p>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
@endsection

@section('before_site_script')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
    <script src="{{asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
@endsection

@section('after_scripts')
    <script>
        $(function () {
            $(".datepicker").datepicker();
        });
        $('#city_from').select2({
            placeholder: 'From',
            debug: true,
            containerCssClass: "select2-input",
            ajax: {
                url: '{{route("book.ajax_search_city")}}',
                dataType: 'json',

                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
        $('#city_to').select2({
            placeholder: 'To',
            debug: true,
            containerCssClass: "select2-input",
            ajax: {
                url: '{{ route("book.ajax_search_city")}}',
                dataType: 'json',

                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    </script>
@endsection