  <div class="modal fade" id="hotelPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
      <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-full" role="document">
      <div class="modal-custom-block">
        <div id="hotel-popup-carousel" class="hotel-popup-carousel owl-carousel">
            @foreach($hotels AS $hotel)
            <?php
            $hotel_info = file_get_contents('https://srv.wego.com/hotels/hotels/'.$hotel->id);
            $hotel_info = json_decode($hotel_info);
            $bd = '';
            if(is_array($hotel->badges)) {
                foreach($hotel->badges AS $badges) {
                    $bd .= @$badges->text . @$badges->subtext . "<br />";
                }
            }
            //dd($hotel_info->reviews);
            //die();
            ?>
            
          <div class="post-block post-modal-place post-modal-thin-inner">
            <div class="top-place-img">
              <img src="{{check_hotel_photo(@$hotel->images[0]->url)}}" alt="place image" style="width:615px;height:250px;">
            </div>
            <div class="place-general-block">
              <div class="place-review-block">
                <div class="place-info">
                  <div class="place-name">{{$hotel->name}}</div>
                  <div class="place-dist">{!!@$bd!!}</div>
                </div>
                <div class="place-review-layer">
                  <ul class="star-list">
                    @for($i=0;$i<$hotel->star;$i++)
                    <li><i class="trav-star-icon"></i></li>
                    @endfor
                  </ul>
                  &nbsp;
                  @if(@$hotel->reviews[0]->count)
                  <span class="review-count">{{@$hotel->reviews[0]->count}}</span>
                  &nbsp;
                        <span>@lang('book.reviews')</span>
                  @endif
                </div>
              </div>
              <div class="place-review-info-main">
                <div class="review-info-block half-block">
                    <div class="info-ttl">@lang('book.short_description')</div>
                  <div class="info-txt">
                    <p>{{@$hotel_info->description}}</p>
                  </div>
                </div>
                <div class="review-info-block half-block">
                    <div class="info-ttl">@lang('profile.address')</div>
                  <div class="info-txt">
                    <address>{{@$hotel_info->address}}</address>
                  </div>
                </div>
                <div class="review-info-block half-block">
                    <div class="info-ttl">@lang('book.check_in') / @lang('book.check_out')</div>
                  <div class="info-txt">
                    <p><b>{{@$hotel_info->checkIn}}</b> / <b>{{@$hotel_info->checkOut}}</b></p>
                  </div>
                </div>
                <div class="review-info-block half-block">
                    <div class="info-ttl">@lang('profile.website')</div>
                  <div class="info-txt">
                    <a href="{{@$hotel_info->website}}" target="blank" class="web-site">{{@$hotel_info->website}}</a>
                  </div>
                </div>
                  <div class="review-info-block half-block">
                      <div class="info-ttl">@lang('profile.phone')</div>
                  <div class="info-txt">
                      <address>{{@$hotel_info->phone1}}<br />{{@$hotel_info->phone2}}</address>
                  </div>
                </div>
                <div class="review-info-block half-block">
                    <div class="info-ttl">@lang('book.fax')</div>
                  <div class="info-txt">
                    <address>{{@$hotel_info->fax}}</address>
                  </div>
                </div>
                <div class="review-info-block">
                  <div class="info-txt">
                    <div class="chart-layer">
                      <img src="https://maps.googleapis.com/maps/api/staticmap?maptype=satellite&center={{@$hotel_info->latitude}},{{@$hotel_info->longitude}}&zoom=10&size=585x290&key={{env('GOOGLE_MAPS_KEY')}}" alt="chart" style="width:585px;height:290px;">
                    </div>
                  </div>
                </div>
                <div class="post-comment-layer">
                  <div class="post-comment-top-info">
                    <ul class="comment-filter">
                        <li>@lang('book.reviews')</li>
                    </ul>
                  </div>
                  <div class="post-comment-wrapper">
                      @if(is_array($hotel_info->reviews))
                      @foreach($hotel_info->reviews AS $review)
                     
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-star-block">
                            <ul class="com-star-list">
                              <li><i class="trav-star-icon"></i></li>
                              <li><i class="trav-star-icon"></i></li>
                              <li><i class="trav-star-icon"></i></li>
                              <li><i class="trav-star-icon"></i></li>
                              <li><i class="trav-star-icon"></i></li>
                            </ul>
                            <span class="count">
                              <b>5</b> / 5
                            </span>
                          </div>
                          <div class="dot">·</div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                      @endforeach
                      @endif

                    <a href="#" class="load-more-link blue">@lang('buttons.general.more') ...</a>
                  </div>
                </div>
              </div>
              <div class="post-foot-booking">
                  <h3 class="book-ttl">@lang('book.booking_options')</h3>
                <table class="booking-table">
                  <tr>
                      <th>@lang('book.booking_site')</th>
                      <th>@lang('book.price_for')</th>
                      <th>@lang('book.total')</th>
                    <th></th>
                  </tr>
                  <tr>
                    <td>
                      <img src="http://placehold.it/23x23" alt="" class="brand-name">
                    </td>
                    <td>
                      <b>@lang('time.count_nights', ['count' => 8])</b>
                    </td>
                    <td>
                      <b>$424</b>
                    </td>
                    <td>
                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <img src="http://placehold.it/84x14" alt="" class="brand-name">
                    </td>
                    <td>
                      <b>@lang('time.count_nights', ['count' => 8])</b>
                    </td>
                    <td>
                      <b>$213</b>
                    </td>
                    <td>
                        <a class="btn btn-light-primary" href="#">@lang('book.booking_book')</a>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
            @endforeach
          
        </div>
      </div>
    </div>
  </div>