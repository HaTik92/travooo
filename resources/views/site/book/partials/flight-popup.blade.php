<div class="modal fade" id="flightPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
      <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-full" role="document">
      <div class="modal-custom-block">
        <div id="flight-popup-carousel" class="flight-popup-carousel owl-carousel flight-style">
          <div class="post-block post-modal-flight-block">
            <div class="flight-general-block">
              <div class="flight-block">
                <div class="flight-ttl-layer">
                  <div class="flight-ttl depart">
                    <i class="fa fa-plane"></i>
                    <!-- <i class="trav-plane-icon"></i> -->
                    <span class="ttl-name">@lang('book.depart')</span>
                  </div>
                  <span class="time">30h 35m</span>
                </div>
                <div class="flight-inner">
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/90x25" alt="">
                    </div>
                   <div class="flight-txt-block">
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                        <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                        <p><span>Casablanca - New York</span></p>
                        <p>Narrow-body jet · Embraer 170</p>
                       </div>
                       <div class="flight-time">
                         <span>@lang('book.economy')</span>
                         <b>8h 05m</b>
                       </div>
                     </div>
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                         <p><span>@lang('book.change_planes_in') New York (JFK) - </span><span
                                   class="red">@lang('book.long_layover')</span></p>
                       </div>
                       <div class="flight-time">
                         <b>21h 09m</b>
                       </div>
                     </div>
                   </div> 
                  </div>
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/80x25" alt="">
                    </div>
                    <div class="flight-txt-block">
                      <div class="txt-inner-row">
                        <div class="flight-txt">
                          <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                          <p><span>New York - Washington</span></p>
                          <p>Air Canada 6 · Wide-body jet</p>
                        </div>
                        <div class="flight-time">
                          <span>@lang('book.economy')y</span>
                          <b>8h 05m</b>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
              <div class="flight-block">
                <div class="flight-ttl-layer">
                  <div class="flight-ttl return">
                    <i class="fa fa-plane"></i>
                    <!-- <i class="trav-plane-icon"></i> -->
                    <span class="ttl-name">@lang('book.return')</span>
                  </div>
                  <span class="time">15h 35m</span>
                </div>
                <div class="flight-inner">
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/90x25" alt="">
                    </div>
                   <div class="flight-txt-block">
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                        <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                        <p><span>Casablanca - New York</span></p>
                        <p>Narrow-body jet · Embraer 170</p>
                       </div>
                       <div class="flight-time">
                         <span>@lang('book.economy')</span>
                         <b>8h 05m</b>
                       </div>
                     </div>
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                         <p><span>@lang('book.change_planes_in') New York (JFK) - </span><span
                                   class="red">@lang('book.long_layover')</span></p>
                       </div>
                       <div class="flight-time">
                         <b>21h 09m</b>
                       </div>
                     </div>
                   </div> 
                  </div>
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/80x25" alt="">
                    </div>
                    <div class="flight-txt-block">
                      <div class="txt-inner-row">
                        <div class="flight-txt">
                          <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                          <p><span>New York - Washington</span></p>
                          <p>Air Canada 6 · Wide-body jet</p>
                        </div>
                        <div class="flight-time">
                          <span>@lang('book.economy')</span>
                          <b>8h 05m</b>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
            <div class="post-foot-booking">
              <h3 class="book-ttl">@lang('book.booking_options')</h3>
              <table class="booking-table">
                <tr>
                  <th>@lang('book.booking_site')</th>
                  <th>@lang('book.class')</th>
                  <th>@lang('book.total')</th>
                  <th></th>
                </tr>
                <tr>
                  <td>
                    <img src="http://placehold.it/65x16" alt="" class="brand-name">
                  </td>
                  <td>
                    <b>@lang('book.economy')</b>
                  </td>
                  <td>
                    <b>$2545</b>
                  </td>
                  <td>
                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                  </td>
                </tr>
                <tr>
                  <td>
                    <img src="http://placehold.it/84x14" alt="" class="brand-name">
                  </td>
                  <td>
                    <b>@lang('book.economy')</b>
                  </td>
                  <td>
                    <b>$2545</b>
                  </td>
                  <td>
                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <div class="post-block post-modal-flight-block">
            <div class="flight-general-block">
              <div class="flight-block">
                <div class="flight-ttl-layer">
                  <div class="flight-ttl depart">
                    <i class="fa fa-plane"></i>
                    <!-- <i class="trav-plane-icon"></i> -->
                    <span class="ttl-name">@lang('book.depart')</span>
                  </div>
                  <span class="time">30h 35m</span>
                </div>
                <div class="flight-inner">
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/90x25" alt="">
                    </div>
                   <div class="flight-txt-block">
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                        <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                        <p><span>Casablanca - New York</span></p>
                        <p>Narrow-body jet · Embraer 170</p>
                       </div>
                       <div class="flight-time">
                         <span>@lang('book.economy')</span>
                         <b>8h 05m</b>
                       </div>
                     </div>
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                         <p><span>@lang('book.change_planes_in') New York (JFK) - </span><span
                                   class="red">@lang('book.long_layover')</span></p>
                       </div>
                       <div class="flight-time">
                         <b>21h 09m</b>
                       </div>
                     </div>
                   </div> 
                  </div>
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/80x25" alt="">
                    </div>
                    <div class="flight-txt-block">
                      <div class="txt-inner-row">
                        <div class="flight-txt">
                          <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                          <p><span>New York - Washington</span></p>
                          <p>Air Canada 6 · Wide-body jet</p>
                        </div>
                        <div class="flight-time">
                          <span>@lang('book.economy')</span>
                          <b>8h 05m</b>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
              <div class="flight-block">
                <div class="flight-ttl-layer">
                  <div class="flight-ttl return">
                    <i class="fa fa-plane"></i>
                    <!-- <i class="trav-plane-icon"></i> -->
                    <span class="ttl-name">@lang('book.return')</span>
                  </div>
                  <span class="time">15h 35m</span>
                </div>
                <div class="flight-inner">
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/90x25" alt="">
                    </div>
                   <div class="flight-txt-block">
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                        <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                        <p><span>Casablanca - New York</span></p>
                        <p>Narrow-body jet · Embraer 170</p>
                       </div>
                       <div class="flight-time">
                         <span>@lang('book.economy')</span>
                         <b>8h 05m</b>
                       </div>
                     </div>
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                         <p><span>@lang('book.change_planes_in') New York (JFK) - </span><span
                                   class="red">@lang('book.long_layover')</span></p>
                       </div>
                       <div class="flight-time">
                         <b>21h 09m</b>
                       </div>
                     </div>
                   </div> 
                  </div>
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/80x25" alt="">
                    </div>
                    <div class="flight-txt-block">
                      <div class="txt-inner-row">
                        <div class="flight-txt">
                          <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                          <p><span>New York - Washington</span></p>
                          <p>Air Canada 6 · Wide-body jet</p>
                        </div>
                        <div class="flight-time">
                          <span>@lang('book.economy')</span>
                          <b>8h 05m</b>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
            <div class="post-foot-booking">
              <h3 class="book-ttl">@lang('book.booking_options')</h3>
              <table class="booking-table">
                <tr>
                  <th>@lang('book.booking_site')</th>
                  <th>@lang('book.class')</th>
                  <th>@lang('book.total')</th>
                  <th></th>
                </tr>
                <tr>
                  <td>
                    <img src="http://placehold.it/65x16" alt="" class="brand-name">
                  </td>
                  <td>
                    <b>@lang('book.economy')</b>
                  </td>
                  <td>
                    <b>$2545</b>
                  </td>
                  <td>
                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                  </td>
                </tr>
                <tr>
                  <td>
                    <img src="http://placehold.it/84x14" alt="" class="brand-name">
                  </td>
                  <td>
                    <b>@lang('book.economy')</b>
                  </td>
                  <td>
                    <b>$2545</b>
                  </td>
                  <td>
                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <div class="post-block post-modal-flight-block">
            <div class="flight-general-block">
              <div class="flight-block">
                <div class="flight-ttl-layer">
                  <div class="flight-ttl depart">
                    <i class="fa fa-plane"></i>
                    <!-- <i class="trav-plane-icon"></i> -->
                    <span class="ttl-name">@lang('book.depart')</span>
                  </div>
                  <span class="time">30h 35m</span>
                </div>
                <div class="flight-inner">
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/90x25" alt="">
                    </div>
                   <div class="flight-txt-block">
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                        <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                        <p><span>Casablanca - New York</span></p>
                        <p>Narrow-body jet · Embraer 170</p>
                       </div>
                       <div class="flight-time">
                         <span>@lang('book.economy')</span>
                         <b>8h 05m</b>
                       </div>
                     </div>
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                         <p><span>@lang('book.change_planes_in') New York (JFK) - </span><span
                                   class="red">@lang('book.long_layover')</span></p>
                       </div>
                       <div class="flight-time">
                         <b>21h 09m</b>
                       </div>
                     </div>
                   </div> 
                  </div>
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/80x25" alt="">
                    </div>
                    <div class="flight-txt-block">
                      <div class="txt-inner-row">
                        <div class="flight-txt">
                          <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                          <p><span>New York - Washington</span></p>
                          <p>Air Canada 6 · Wide-body jet</p>
                        </div>
                        <div class="flight-time">
                          <span>@lang('book.economy')</span>
                          <b>8h 05m</b>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
              <div class="flight-block">
                <div class="flight-ttl-layer">
                  <div class="flight-ttl return">
                    <i class="fa fa-plane"></i>
                    <!-- <i class="trav-plane-icon"></i> -->
                    <span class="ttl-name">@lang('book.return')</span>
                  </div>
                  <span class="time">15h 35m</span>
                </div>
                <div class="flight-inner">
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/90x25" alt="">
                    </div>
                   <div class="flight-txt-block">
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                        <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                        <p><span>Casablanca - New York</span></p>
                        <p>Narrow-body jet · Embraer 170</p>
                       </div>
                       <div class="flight-time">
                         <span>@lang('book.economy')</span>
                         <b>8h 05m</b>
                       </div>
                     </div>
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                         <p><span>@lang('book.change_planes_in') New York (JFK) - </span><span
                                   class="red">@lang('book.long_layover')</span></p>
                       </div>
                       <div class="flight-time">
                         <b>21h 09m</b>
                       </div>
                     </div>
                   </div> 
                  </div>
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/80x25" alt="">
                    </div>
                    <div class="flight-txt-block">
                      <div class="txt-inner-row">
                        <div class="flight-txt">
                          <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                          <p><span>New York - Washington</span></p>
                          <p>Air Canada 6 · Wide-body jet</p>
                        </div>
                        <div class="flight-time">
                          <span>@lang('book.economy')</span>
                          <b>8h 05m</b>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
            <div class="post-foot-booking">
              <h3 class="book-ttl">@lang('book.booking_options')</h3>
              <table class="booking-table">
                <tr>
                  <th>@lang('book.booking_site')</th>
                  <th>@lang('book.class')</th>
                  <th>@lang('book.total')</th>
                  <th></th>
                </tr>
                <tr>
                  <td>
                    <img src="http://placehold.it/65x16" alt="" class="brand-name">
                  </td>
                  <td>
                    <b>@lang('book.economy')</b>
                  </td>
                  <td>
                    <b>$2545</b>
                  </td>
                  <td>
                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                  </td>
                </tr>
                <tr>
                  <td>
                    <img src="http://placehold.it/84x14" alt="" class="brand-name">
                  </td>
                  <td>
                    <b>@lang('book.economy')</b>
                  </td>
                  <td>
                    <b>$2545</b>
                  </td>
                  <td>
                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <div class="post-block post-modal-flight-block">
            <div class="flight-general-block">
              <div class="flight-block">
                <div class="flight-ttl-layer">
                  <div class="flight-ttl depart">
                    <i class="fa fa-plane"></i>
                    <!-- <i class="trav-plane-icon"></i> -->
                    <span class="ttl-name">@lang('book.depart')</span>
                  </div>
                  <span class="time">30h 35m</span>
                </div>
                <div class="flight-inner">
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/90x25" alt="">
                    </div>
                   <div class="flight-txt-block">
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                        <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                        <p><span>Casablanca - New York</span></p>
                        <p>Narrow-body jet · Embraer 170</p>
                       </div>
                       <div class="flight-time">
                         <span>@lang('book.economy')</span>
                         <b>8h 05m</b>
                       </div>
                     </div>
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                        <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span></p>
                       </div>
                       <div class="flight-time">
                         <b>21h 09m</b>
                       </div>
                     </div>
                   </div> 
                  </div>
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/80x25" alt="">
                    </div>
                    <div class="flight-txt-block">
                      <div class="txt-inner-row">
                        <div class="flight-txt">
                          <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                          <p><span>New York - Washington</span></p>
                          <p>Air Canada 6 · Wide-body jet</p>
                        </div>
                        <div class="flight-time">
                          <span>@lang('book.economy')</span>
                          <b>8h 05m</b>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
              <div class="flight-block">
                <div class="flight-ttl-layer">
                  <div class="flight-ttl return">
                    <i class="fa fa-plane"></i>
                    <!-- <i class="trav-plane-icon"></i> -->
                    <span class="ttl-name">@lang('book.return')</span>
                  </div>
                  <span class="time">15h 35m</span>
                </div>
                <div class="flight-inner">
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/90x25" alt="">
                    </div>
                   <div class="flight-txt-block">
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                        <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                        <p><span>Casablanca - New York</span></p>
                        <p>Narrow-body jet · Embraer 170</p>
                       </div>
                       <div class="flight-time">
                         <span>@lang('book.economy')</span>
                         <b>8h 05m</b>
                       </div>
                     </div>
                     <div class="txt-inner-row">
                       <div class="flight-txt">
                        <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span></p>
                       </div>
                       <div class="flight-time">
                         <b>21h 09m</b>
                       </div>
                     </div>
                   </div> 
                  </div>
                  <div class="flight-row">
                    <div class="img-wrap">
                      <img src="http://placehold.it/80x25" alt="">
                    </div>
                    <div class="flight-txt-block">
                      <div class="txt-inner-row">
                        <div class="flight-txt">
                          <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                          <p><span>New York - Washington</span></p>
                          <p>Air Canada 6 · Wide-body jet</p>
                        </div>
                        <div class="flight-time">
                          <span>@lang('book.economy')</span>
                          <b>8h 05m</b>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
            <div class="post-foot-booking">
              <h3 class="book-ttl">@lang('book.booking_options')</h3>
              <table class="booking-table">
                <tr>
                  <th>@lang('book.booking_site')</th>
                  <th>@lang('book.class')</th>
                  <th>@lang('book.total')</th>
                  <th></th>
                </tr>
                <tr>
                  <td>
                    <img src="http://placehold.it/65x16" alt="" class="brand-name">
                  </td>
                  <td>
                    <b>@lang('book.economy')</b>
                  </td>
                  <td>
                    <b>$2545</b>
                  </td>
                  <td>
                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                  </td>
                </tr>
                <tr>
                  <td>
                    <img src="http://placehold.it/84x14" alt="" class="brand-name">
                  </td>
                  <td>
                    <b>@lang('book.economy')</b>
                  </td>
                  <td>
                    <b>$2545</b>
                  </td>
                  <td>
                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>