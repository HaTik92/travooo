<div class="post-block post-flight-style hotels-by-city">
    <div class="post-side-top">
        <h3 class="side-ttl">@lang('book.hotels_by_city')</h3>
    </div>
    <div class="post-hotel-inner">
        <div class="hotel-block hb-50">
            <?php
            $city_1 = \App\Models\City\Cities::find(193);
            ?>
            <div class="hotel-block-inner">
                <img class="city-img" src="https://s3.amazonaws.com/travooo-images2/{{@$city_1->getMedias[0]->url}}" alt="" style="width:500px;height:320px;">
                <div class="hotel-info">
                    <div class="city-name">
                        <span>{{$city_1->transsingle->title}}</span>
                        <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($city_1->country->iso_code)}}.png" alt="" class="flag-img" style="width:28;height:18px;">
                    </div>
                    <div class="hotels-count">
                        {{number_format(count($city_1->hotels))}} hotels
                    </div>
                </div>
                <button class="btn btn-hotel-style">
                    <span class="btn-txt">@lang('book.average_price')</span>
                    <span class="btn-price">
                        <b>usd</b>
                        245
                    </span>
                </button>
            </div>
        </div>
        <div class="hotel-block hb-50">
            <?php
            $city_2 = \App\Models\City\Cities::find(180);
            ?>
            <div class="hotel-block-inner">
                <img class="city-img" src="https://s3.amazonaws.com/travooo-images2/{{@$city_2->getMedias[0]->url}}" alt="" style="width:500px;height:320px;">
                <div class="hotel-info">
                    <div class="city-name">
                        <span>{{$city_2->transsingle->title}}</span>
                        <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($city_2->country->iso_code)}}.png" alt="" class="flag-img" style="width:28;height:18px;">

                    </div>
                    <div class="hotels-count">
                        {{number_format(count($city_2->hotels))}} @lang('book.hotels')
                    </div>
                </div>
                <button class="btn btn-hotel-style">
                    <span class="btn-txt">@lang('book.average_price')</span>
                    <span class="btn-price">
                        <b>usd</b>
                        245
                    </span>
                </button>
            </div>
        </div>
        <div class="hotel-block hb-33">
            <div class="hotel-block-inner">
                <?php
                $city_3 = \App\Models\City\Cities::find(144);
                ?>
                <img class="city-img" src="https://s3.amazonaws.com/travooo-images2/{{@$city_3->getMedias[0]->url}}" alt="" style="width:326px;height:230px;">
                <div class="hotel-info">
                    <div class="city-name">
                        <span>{{$city_3->transsingle->title}}</span>
                        <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($city_3->country->iso_code)}}.png" alt="" class="flag-img" style="width:28px;height:18px;">
                    </div>
                    <div class="hotels-count">
                        {{number_format(count($city_3->hotels))}} @lang('book.hotels')
                    </div>
                </div>
                <button class="btn btn-hotel-style">
                    <span class="btn-txt">@lang('book.average_price')</span>
                    <span class="btn-price">
                        <b>usd</b>
                        245
                    </span>
                </button>
            </div>
        </div>
        <div class="hotel-block hb-33">
            <div class="hotel-block-inner">
                <?php
                $city_4 = \App\Models\City\Cities::find(895);
                ?>
                <img class="city-img" src="https://s3.amazonaws.com/travooo-images2/{{@$city_4->getMedias[0]->url}}" alt="" style="width:326px;height:230px;">
                <div class="hotel-info">
                    <div class="city-name">
                        <span>{{$city_4->transsingle->title}}</span>
                        <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower($city_4->country->iso_code)}}.png" alt="" class="flag-img" style="width:28px;height:18px;">
                    </div>
                    <div class="hotels-count">
                        {{number_format(count($city_4->hotels))}} @lang('book.hotels')
                    </div>
                </div>
                <button class="btn btn-hotel-style">
                    <span class="btn-txt">@lang('book.average_price')</span>
                    <span class="btn-price">
                        <b>usd</b>
                        245
                    </span>
                </button>
            </div>
        </div>
        <div class="hotel-block hb-33">
            <div class="hotel-block-inner">
                <?php
                $city_5 = \App\Models\City\Cities::find(48);
                ?>
                <img class="city-img" src="https://s3.amazonaws.com/travooo-images2/{{@$city_5->getMedias[0]->url}}" alt="" style="width:326px;height:230px;">
                <div class="hotel-info">
                    <div class="city-name">
                        <span>{{@$city_5->transsingle->title}}</span>
                        <img src="https://s3-us-west-2.amazonaws.com/travooo-assets/images/countryflags/medium/{{strtolower(@$city_5->country->iso_code)}}.png" alt="" class="flag-img" style="width:28px;height:18px;">
                    </div>
                    <div class="hotels-count">
                        {{@number_format(count($city_3->hotels))}} @lang('book.hotels')
                    </div>
                </div>
                <button class="btn btn-hotel-style">
                    <span class="btn-txt">@lang('book.average_price')</span>
                    <span class="btn-price">
                        <b>usd</b>
                        245
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>