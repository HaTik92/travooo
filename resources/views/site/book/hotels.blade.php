@php
    $title = 'Travooo - flights, hotels';
@endphp

@extends('site.book.template.book')

@section('before_styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <style>
    .select2-input {
        height:49px !important;
        padding:8px !important;
        border: 1px solid #e6e6e6 !important;
    }  
    .select2-selection__choice {
        background: #f7f7f7 !important;
    padding: 5px 10px !important;
    position: relative !important;
    border-radius: 0 !important;
        color: #4080ff !important;
        margin-top:0px !important;
    }
    .ui-state-highlight {
        height:25px;
        background-color:#c4d9ff;
    }
    em {
        font-style: italic;
    }
</style>


    <style>:host([rtl]){direction:rtl}.singleline{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}[hidden]{display:none!important}wego-ripple{direction:ltr;position:absolute;display:block;color:#ddd}.no-overflow{overflow:hidden}.btn{width:100%;height:inherit;position:relative;background:#fff;margin:0;text-align:left;outline:0;box-sizing:border-box}#btn>*{pointer-events:none}#btn>#popup{pointer-events:all}:host:after{opacity:0;content:"";position:absolute;top:0;left:0;right:0;border-top:3px solid transparent;transition:.3s linear}:host([focused]):after{opacity:1;border-top:3px solid var(--passenger-picker-focus-color)}.uppercase{text-transform:uppercase}#popup{display:none;cursor:default}:host([focused]) #popup{position:absolute;display:block;top:100%;margin-top:2px;background:#fff;border-radius:4px;box-shadow:0 1px 2px 0 rgba(0,0,0,.5);z-index:105}</style>
    <style>wego-sprite{background-repeat:no-repeat;background-size:24px auto;width:24px;height:24px;flex-shrink:0}wego-sprite.passengers{background-position:0 -425px}wego-sprite.cabinclass{background-position:0 -450px}wego-sprite.arrow{background-position:0 -600px}wego-sprite.tick{background-position:0 -575px}wego-sprite.adult{background-position:0 -475px}wego-sprite.child{background-position:0 -500px}wego-sprite.infant{background-position:0 -525px}</style>
    <style>.layout{display:flex}.horizontal{flex-direction:row}.vertical{flex-direction:column}.flex{-ms-flex:1 1 .000000001px;flex:1;flex-basis:.000000001px}.justified{-ms-flex-pack:justify;justify-content:space-between}.layout.center,.layout.center-center{-ms-flex-align:center;align-items:center}.layout.center-center,.layout.center-justified{-ms-flex-pack:center;justify-content:center}.layout.end-justified{-ms-flex-pack:end;justify-content:flex-end}.wrap{flex-wrap:wrap}</style>
    <style include="passenger-picker-styles passenger-picker-sprite-styles flex-styles">:host{display:block;position:relative;outline:0;--passenger-picker-focus-color:#55C901}.text{position:relative;display:block;color:#999;text-align:left;font-size:16px;margin-left:8px;box-sizing:border-box}.margintop{margin-top:2px}.top{font-size:11px;font-weight:500;line-height:normal}.bottom{font-size:14px;font-weight:400;color:#333}:host([rtl]) .text{margin-left:auto;margin-right:8px;text-align:right}.btn{background:0 0;border-radius:inherit}.btn-display{height:inherit;padding:0 14px;cursor:pointer;border-radius:inherit}:host([focused]) #popup{min-width:210px;left:0;padding:0 14px}:host([popup-align-right][focused]) #popup{min-width:210px;right:0;left:auto}.row{position:relative;background:0 0;height:52px}.line{border-bottom:1px solid #e7e7e7}.dropdownitem{display:block;position:relative;height:40px;line-height:40px;padding:0 10px}.dropdownitem[selected]{font-weight:700;background:#fafafa;border-radius:2px}.stepper{position:relative;display:block;color:rgba(0,0,0,.54);background-color:#fafafa;font-size:1.4rem;line-height:32px;min-width:32px;min-height:32px;text-align:center;border-bottom:solid 1px #e9e9e9;cursor:pointer}.dec{border-radius:.3rem 0 0 .3rem;border-right:1px solid #d9d9d9}.inc{border-radius:0 .3rem .3rem 0}.stepper[blocked="1"]{background-color:#f0f0f0;color:#a8a8a8;cursor:auto;pointer-events:none}:host([rtl]) .stepper{-webkit-transform:scaleX(-1);-moz-transform:scaleX(-1);-ms-transform:scaleX(-1);-o-transform:scaleX(-1);transform:scaleX(-1)}#cabingroup{cursor:pointer}#cabingroup .list{transform:translate3d(0,0,0);max-height:0;transition:max-height .35s cubic-bezier(.075,.82,.165,1);overflow:hidden}.expand #cabinmenu{max-height:500px;transition:max-height .35s cubic-bezier(.785,.135,.15,.86)}.arrow{display:block;margin-left:10px;width:0;height:0;border-left:5px solid transparent;border-right:5px solid transparent;border-top:5px solid #777;transition:.35s cubic-bezier(.785,.135,.15,.86)}:host([rtl]) .arrow{margin-left:0;margin-right:10px}.expand .arrow,:host([focused]) .arrow2{-webkit-transform:rotateZ(180deg);-moz-transform:rotateZ(180deg);-ms-transform:rotateZ(180deg);-o-transform:rotateZ(180deg);transform:rotateZ(180deg)}.tick{margin-right:12px;visibility:hidden}.dropdownitem[selected] .tick{visibility:visible}:host([rtl]) .tick{margin-right:auto;margin-left:12px}:host([rtl][focused]) #popup{left:auto;right:0}:host([rtl][popup-align-right][focused]) #popup{right:auto;left:0}:host([rtl]) .dropdownitem{text-align:right}#btn>#popup>.row{cursor:default}:host([dropdown]) .nm{margin:0;color:#fff;text-transform:none;font-size:14px;font-weight:400}:host([dropdown]) #cabinmenu{max-height:unset!important}:host([dropdown]) .passengers{display:none}:host([dropdown]) .b{border:none}:host([dropdown]){height:32px;background:0 0}:host([dropdown]) .btn{border:none;border-radius:2px}:host([dropdown][focused]):after{display:none}:host([dropdown][focused]) #btn{background:#ffffff3d}.arrow2{display:none}:host([dropdown]) .arrow2{display:block;border-top:5px solid #fff}:host([dropdown]) wego-ripple{color:#eee}.horizontal-center{display:flex;flex-direction:row;align-items:center}@media only screen and (max-width:768px){:host([focused]) #popup{width:100%}}@media only screen and (max-width:480px){:host([responsive]) .btn-display{height:60px;background:#fff}:host([responsive][focused]) #popup{width:200%;position:relative;top:unset;box-sizing:border-box}}</style>
@endsection


@section('before-content')
    <div class="top-banner-wrap">
        <div class="top-banner-block after-disabled book-top-banner-block"
             style="background-image: url({{url('assets2/image/flight-hotel-bg.jpg')}}">
            <div class="top-banner-flight-hotel">
                <ul class="flight-hotel-tabs" role="tablist">
                    <li>
                        <a class="current" href="{{route('book.hotel')}}">
                                            <span class="circle-icon">
                                                <i class="trav-building-icon"></i>
                                            </span>
                            <span>@lang('book.hotels')</span>
                        </a>
                    </li>
                    <li>
                        <a class="" href="{{route('book.flight')}}">
                                            <span class="circle-icon">
                                                <i class="trav-angle-plane-icon"></i>
                                            </span>
                            <span>@lang('book.flights')</span>
                        </a>
                    </li>
                </ul>
                <div class="top-banner-inner">

                    <h2 class="ban-ttl">Find a place to lodge in...</h2>
                    <p>The best hotel deals that suit your accommodation needs.</p>
                </div>
                <form method="get" action="{{route('book.search_hotel')}}" class="top-banner-search-form">
                    <div class="flex-row">
                        <div class="flex-item fl-30 with-icon">
                                            <span class="icon-wrap">
                                                <i class="trav-location"></i>
                                            </span>
                            <select class="flex-input" name="city_select" id="city_select"
                                    placeholder="@lang('book.please_enter_your_destination_city')">
                            </select>
                        </div>
                        <div class="flex-item fl-30 with-icon">
                                            <span class="icon-wrap">
                                                <i class="trav-event-icon"></i>
                                            </span>
                            <input class="flex-input" name="daterange" id="daterange"
                                   placeholder="@lang('time.date')"
                                   value="{{date('m/d/Y', time())}} - {{date('m/d/Y', time()+86400)}}" type="text">

                        </div>
{{--                        <passenger-picker id="passengers" class="flex flex-item fl-30" focused="">--}}
{{--                        <div class="btn" id="btn" tabindex="0" style="padding:10px 16px;">--}}
{{--                            <div class="flex btn-display horizontal-center">--}}
{{--                                <wego-ripple></wego-ripple>--}}
{{--                                <wego-sprite class="passengers" height="48" width="48" style="background-image: url('//assets.wego.com/image/sprite/c_pad,f_auto,fl_lossy,q_auto:low,w_48,h_48/v3/search-form-icons');"></wego-sprite>--}}
{{--                                <div class="layout vertical no-overflow">--}}
{{--                                    <span class="text singleline top nm uppercase">Premium Economy</span> --}}
{{--                                    <span class="text bottom nm singleline">9 passengers</span>--}}
{{--                                </div>--}}
{{--                                <div class="arrow arrow2"></div>--}}
{{--                            </div>--}}
{{--                            <div class="layout vertical" id="popup">--}}
{{--                                <div id="cabingroup" class="">--}}
{{--                                    <div class="layout btn center horizontal row">--}}
{{--                                        <wego-ripple></wego-ripple>--}}
{{--                                        <wego-sprite class="cabinclass" height="48" width="48" style="background-image: url('//assets.wego.com/image/sprite/c_pad,f_auto,fl_lossy,q_auto:low,w_48,h_48/v3/search-form-icons');"></wego-sprite>--}}
{{--                                        <div class="text bottom flex">Premium Economy</div>--}}
{{--                                        <div class="arrow" id="arrow"></div>--}}
{{--                                    </div>--}}
{{--                                    <div class="line list" id="cabinmenu">--}}
{{--                                        <div class="bottom singleline dropdownitem horizontal-center">--}}
{{--                                            <wego-ripple></wego-ripple>--}}
{{--                                            <wego-sprite class="tick" height="48" width="48" style="background-image: url('//assets.wego.com/image/sprite/c_pad,f_auto,fl_lossy,q_auto:low,w_48,h_48/v3/search-form-icons');"></wego-sprite>--}}
{{--                                            <span class="flex">Economy</span>--}}
{{--                                        </div>--}}
{{--                                        <div class="bottom singleline dropdownitem horizontal-center" selected="">--}}
{{--                                            <wego-ripple></wego-ripple>--}}
{{--                                            <wego-sprite class="tick" height="48" width="48" style="background-image: url('//assets.wego.com/image/sprite/c_pad,f_auto,fl_lossy,q_auto:low,w_48,h_48/v3/search-form-icons');"></wego-sprite>--}}
{{--                                            <span class="flex">Premium Economy</span>--}}
{{--                                        </div>--}}
{{--                                        <div class="bottom singleline dropdownitem horizontal-center">--}}
{{--                                            <wego-ripple></wego-ripple>--}}
{{--                                            <wego-sprite class="tick" height="48" width="48" style="background-image: url('//assets.wego.com/image/sprite/c_pad,f_auto,fl_lossy,q_auto:low,w_48,h_48/v3/search-form-icons');"></wego-sprite>--}}
{{--                                            <span class="flex">Business Class</span>--}}
{{--                                        </div>--}}
{{--                                        <div class="bottom singleline dropdownitem horizontal-center">--}}
{{--                                            <wego-ripple></wego-ripple>--}}
{{--                                            <wego-sprite class="tick" height="48" width="48" style="background-image: url('//assets.wego.com/image/sprite/c_pad,f_auto,fl_lossy,q_auto:low,w_48,h_48/v3/search-form-icons');"></wego-sprite>--}}
{{--                                            <span class="flex">First Class</span>--}}
{{--                                        </div>--}}
{{--                                        <dom-repeat style="display: none;">--}}
{{--                                            <template is="dom-repeat"></template>--}}
{{--                                        </dom-repeat>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div>--}}
{{--                                    <div class="btn row horizontal-center line">--}}
{{--                                        <wego-sprite class="adult" height="48" width="48" style="background-image: url('//assets.wego.com/image/sprite/c_pad,f_auto,fl_lossy,q_auto:low,w_48,h_48/v3/search-form-icons');"></wego-sprite>--}}
{{--                                        <div class="flex layout vertical">--}}
{{--                                            <span class="text bottom">8 Adults</span>--}}
{{--                                            <span class="text singleline top margintop">>12 years</span>--}}
{{--                                        </div>--}}
{{--                                        <div class="stepper dec" id="a1" blocked="0">--}}
{{--                                            <wego-ripple></wego-ripple>--}}
{{--                                            ---}}
{{--                                        </div>--}}
{{--                                        <div class="stepper inc" id="a2" blocked="1">--}}
{{--                                            <wego-ripple></wego-ripple>--}}
{{--                                            +--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="btn row horizontal-center line">--}}
{{--                                        <wego-sprite class="child" height="48" width="48" style="background-image: url('//assets.wego.com/image/sprite/c_pad,f_auto,fl_lossy,q_auto:low,w_48,h_48/v3/search-form-icons');"></wego-sprite>--}}
{{--                                        <div class="flex layout vertical">--}}
{{--                                            <span class="text bottom">1 Child</span>--}}
{{--                                            <span class="text singleline top margintop">2-12 years</span>--}}
{{--                                        </div>--}}
{{--                                        <div class="stepper dec" id="c1" blocked="0">--}}
{{--                                            <wego-ripple></wego-ripple>--}}
{{--                                            ---}}
{{--                                        </div>--}}
{{--                                        <div class="stepper inc" id="c2" blocked="1">--}}
{{--                                            <wego-ripple></wego-ripple>--}}
{{--                                            +--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="btn row horizontal-center">--}}
{{--                                        <wego-sprite class="infant" height="48" width="48" style="background-image: url('//assets.wego.com/image/sprite/c_pad,f_auto,fl_lossy,q_auto:low,w_48,h_48/v3/search-form-icons');"></wego-sprite>--}}
{{--                                        <div class="flex layout vertical">--}}
{{--                                            <span class="text bottom">0 Infants</span>--}}
{{--                                            <span class="text singleline top margintop"><2 years</span>--}}
{{--                                        </div>--}}
{{--                                        <div class="stepper dec" id="i1" blocked="1">--}}
{{--                                            <wego-ripple></wego-ripple>--}}
{{--                                            ---}}
{{--                                        </div>--}}
{{--                                        <div class="stepper inc" id="i2" blocked="1">--}}
{{--                                            <wego-ripple></wego-ripple>--}}
{{--                                            +--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        </passenger-picker>--}}
                        <div class="flex-item fl-30">
                            <select name="" id="" class="custom-select">
                                <option value="1">1 @lang('book.adult'), @lang('book.economy')</option>
                                <option value="2">@lang('book.item')</option>
                            </select>
                        </div>
                        <div class="flex-item fl-10">
                            <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.search')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--
                        <div class="post-block post-flight-style">
                            <div class="post-side-top">
                                <h3 class="side-ttl"><i class="trav-trending-destination-icon"></i> Trending flights
                                </h3>
                                <div class="side-right-control">
                                    <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                                    <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
                                </div>
                            </div>
                            <div class="post-side-inner">
                                <div class="post-slide-wrap">
                                    <ul id="trendingFlights" class="post-slider">
                                        <li>
                                            <div class="post-flight-inner">
                                                <img src="http://placehold.it/327x180" alt="">
                                                <div class="post-flight-info">
                                                    <div class="flight-inner">
                                                        <div class="fl-left">
                                                            <p class="city-name">Barcelona</p>
                                                            <p class="country-name">Spain</p>
                                                            <p class="going-count">
                                                                <span class="count">52</span>
                                                                <span>going today</span>
                                                            </p>
                                                        </div>
                                                        <div class="fl-right">
                                                            <div class="flag-wrap">
                                                                <img class="flag-image" src="http://placehold.it/32x22?text=mini+map" alt="">
                                                            </div>
                                                            <div class="btn-wrap">
                                                                <button class="btn btn-light-primary btn-bordered">Book Flight</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="http://placehold.it/327x180?text=direction+map" alt="">
                                                <div class="post-direct-info">
                                                    <div class="direct-block">
                                                        <p class="dir-label">3h 45m</p>
                                                        <p class="dir-txt">@lang('book.direct_flight')</p>
                                                    </div>
                                                    <div class="direct-block">
                                                        <p class="dir-label">3120 km</p>
                                                        <p class="dir-txt">@lang('book.total_distance')</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="post-flight-inner">
                                                <img src="http://placehold.it/327x180" alt="">
                                                <div class="post-flight-info">
                                                    <div class="flight-inner">
                                                        <div class="fl-left">
                                                            <p class="city-name">Amalfi</p>
                                                            <p class="country-name">Italy</p>
                                                            <p class="going-count">
                                                                <span class="count">52</span>
                                                                <span>going today</span>
                                                            </p>
                                                        </div>
                                                        <div class="fl-right">
                                                            <div class="flag-wrap">
                                                                <img class="flag-image" src="http://placehold.it/32x22?text=mini+map" alt="">
                                                            </div>
                                                            <div class="btn-wrap">
                                                                <button class="btn btn-light-primary btn-bordered">Book Flight</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="http://placehold.it/327x180?text=direction+map" alt="">
                                                <div class="post-direct-info">
                                                    <div class="direct-block">
                                                        <p class="dir-label">3h 45m</p>
                                                        <p class="dir-txt">@lang('book.direct_flight')</p>
                                                    </div>
                                                    <div class="direct-block">
                                                        <p class="dir-label">3120 km</p>
                                                        <p class="dir-txt">@lang('book.total_distance')</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="post-flight-inner">
                                                <img src="http://placehold.it/327x180" alt="">
                                                <div class="post-flight-info">
                                                    <div class="flight-inner">
                                                        <div class="fl-left">
                                                            <p class="city-name">Tokyo</p>
                                                            <p class="country-name">Japan</p>
                                                            <p class="going-count">
                                                                <span class="count">52</span>
                                                                <span>going today</span>
                                                            </p>
                                                        </div>
                                                        <div class="fl-right">
                                                            <div class="flag-wrap">
                                                                <img class="flag-image" src="http://placehold.it/32x22?text=mini+map" alt="">
                                                            </div>
                                                            <div class="btn-wrap">
                                                                <button class="btn btn-light-primary btn-bordered">Book Flight</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="http://placehold.it/327x180?text=direction+map" alt="">
                                                <div class="post-direct-info">
                                                    <div class="direct-block">
                                                        <p class="dir-label">3h 45m</p>
                                                        <p class="dir-txt">@lang('book.direct_flight')</p>
                                                    </div>
                                                    <div class="direct-block">
                                                        <p class="dir-label">3120 km</p>
                                                        <p class="dir-txt">@lang('book.total_distance')</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
    -->

    @include('site/book/partials/hotels-by-city')
@endsection

@section('before_site_script')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link href="{{asset('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
    <script src="{{asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
@endsection

@section('after_scripts')
    <script>
        $(function () {
            $('#daterange').daterangepicker({
                opens: 'left'
            }, function (start, end, label) {
                console.log("@lang('book.a_new_date_selection_was_made'): " + start.format('YYYY-MM-DD') + ' @lang('chat.to') ' + end.format('YYYY-MM-DD'));
            });
        });
        $('#city_select').select2({
            placeholder: '@lang('book.please_enter_your_destination_city')',
            containerCssClass: "select2-input",
            debug: true,

            ajax: {
                url: '{{route('book.ajax_search_city')}}',
                dataType: 'json',

                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    </script>
@endsection