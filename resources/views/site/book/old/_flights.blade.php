<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - flights, hotels</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">

            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li class="active"><a href="{{url('book/hotel')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('book.nav.home')</span>
                        </a></li>
                </ul>
            </div>


            <div class="top-banner-wrap">
                <div class="top-banner-block after-disabled"
                     style="background-image: url({{url('assets2/image/flight-hotel-bg.jpg')}}">
                    <div class="top-banner-flight-hotel">
                        <ul class="flight-hotel-tabs" role="tablist">
                            <li>
                                <a href="{{url('book/hotel')}}">
                    <span class="circle-icon">
                      <i class="trav-building-icon"></i>
                    </span>
                                    <span>@lang('book.nav.hotels')</span>
                                </a>
                            </li>
                            <li>
                                <a class="current" href="{{url('book/flight')}}">
                    <span class="circle-icon">
                      <i class="trav-angle-plane-icon"></i>
                    </span>
                                    <span>@lang('book.nav.flights')</span>
                                </a>
                            </li>
                        </ul>
                        <div class="top-banner-inner">

                            <h2 class="ban-ttl">@lang('book.titles.flights')</h2>
                            <p>@lang('book.contents.flights')</p>
                        </div>
                        <form method="post" action="{{url('book/search-flight')}}" class="top-banner-search-form">
                            <div class="flex-row">
                                <div class="flex-item fl-30 with-icon">
                    <span class="icon-wrap">
                      <i class="trav-location"></i>
                    </span>
                                    <select class="flex-input" name="city_from" id="city_from"
                                            placeholder="@lang('chat.from')">
                                    </select>
                                </div>
                                <div class="flex-item fl-30 with-icon">
                    <span class="icon-wrap">
                      <i class="trav-location"></i>
                    </span>
                                    <select class="flex-input" name="city_to" id="city_to"
                                            placeholder="@lang('chat.to')">
                                    </select>
                                </div>
                            </div>
                            <div class="flex-row">
                                <div class="flex-item fl-30 with-icon">
                    <span class="icon-wrap">
                      <i class="trav-event-icon"></i>
                    </span>
                                    <input class="flex-input datepicker" name="date_1" id="date_1"
                                           placeholder="@lang('form.placeholders.travel_date')" value="" type="text">
                                </div>

                                <div class="flex-item fl-30 with-icon">
                    <span class="icon-wrap">
                      <i class="trav-event-icon"></i>
                    </span>
                                    <input class="flex-input datepicker" name="date_2" id="date_2"
                                           placeholder="@lang('form.placeholders.return_date')" value="" type="text">
                                </div>
                                <div class="flex-item fl-10">
                                    <button class="btn btn-light-primary btn-bordered">@lang('form.buttons.search')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">


                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">

                        <div class="post-block post-flight-info-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/390x205" alt="">
                            </div>
                            <div class="flight-info-block">
                                <div class="flight-info-content">
                                    <div class="flight-info-row">
                                        <div class="flight-inner">
                                            <p class="label">@lang('place.from')</p>
                                            <h4 class="ttl">Vienna</h4>
                                            <p class="sub-ttl">@lang('book.nag')</p>
                                        </div>
                                        <div class="flight-inner">
                                            <p class="label">@lang('place.to')</p>
                                            <h4 class="ttl">Chiang Mai</h4>
                                            <p class="sub-ttl">@lang('book.fco')</p>
                                        </div>
                                    </div>
                                    <div class="flight-info-row">
                                        <div class="flight-inner">
                                            <p class="label">@lang('profile.traveller')</p>
                                            <h4 class="ttl-sm">@lang('book.count_adult', ['count' => 1])</h4>
                                        </div>
                                        <div class="flight-inner">
                                            <p class="label">@lang('book.class')</p>
                                            <h4 class="ttl-sm">@lang('book.economy')</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-info-foot">
                                    <p>Connecting 2 or more stops</p>
                                    <div class="link-wrap">
                                        <a href="#" class="place-link">Air Vienna,</a>
                                        <a href="#" class="place-link">Emirates,</a>
                                        <a href="#" class="place-link">Chiabg Mai</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="post-block post-flight-info-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/390x205" alt="">
                            </div>
                            <div class="flight-info-block">
                                <div class="flight-info-content">
                                    <div class="flight-info-row">
                                        <div class="flight-inner">
                                            <p class="label">@lang('place.to')</p>
                                            <h4 class="ttl">Chiang Mai</h4>
                                            <p class="sub-ttl">@lang('book.fco')</p>
                                        </div>
                                    </div>
                                    <div class="flight-info-row">
                                        <div class="flight-inner">
                                            <p class="label">@lang('profile.traveller')</p>
                                            <h4 class="ttl-sm">@lang('book.count_adult', ['count' => 1])</h4>
                                        </div>
                                        <div class="flight-inner">
                                            <p class="label">@lang('book.class')</p>
                                            <h4 class="ttl-sm">@lang('book.economy')</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-info-foot">
                                    <p>Connecting 2 or more stops</p>
                                    <div class="link-wrap">
                                        <a href="#" class="place-link">Air Vienna,</a>
                                        <a href="#" class="place-link">Emirates,</a>
                                        <a href="#" class="place-link">Chiabg Mai</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                                <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                                <li><a href="{{url('/')}}">Advertising</a></li>
                                <li><a href="{{url('/')}}">Cookies</a></li>
                                <li><a href="{{url('/')}}">More</a></li>
                            </ul>
                            <p class="copyright">Travooo &copy; 2017</p>
                        </div>
                    </aside>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- modals -->
<!-- hotel popup -->
<div class="modal fade" id="hotelPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-full" role="document">
        <div class="modal-custom-block">
            <div class="hotel-popup-carousel owl-carousel">
                <div class="post-block post-modal-place post-modal-thin-inner">
                    <div class="top-place-img">
                        <img src="http://placehold.it/615x250" alt="place image">
                    </div>
                    <div class="place-general-block">
                        <div class="place-review-block">
                            <div class="place-info">
                                <div class="place-name">@lang('place.name_airport', ['name' => 'Rabat-sale'])</div>
                                <div class="place-dist">@lang('place.airport_in_place', ['place' => 'Sale, Morocco'])</div>
                            </div>
                            <div class="place-review-layer">
                                <ul class="star-list">
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-half-star-icon"></i></li>
                                </ul>
                                &nbsp;
                                <span class="review-count">547</span>
                                &nbsp;
                                <span>@lang('place.reviews')</span>
                            </div>
                        </div>
                        <div class="place-review-info-main">
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, commodi nobis
                                        quaerat nam impedit deleniti?</p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <address>2145 Hamilton Avenue, San Jose CA 95125</address>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.working_times')</div>
                                <div class="info-txt">
                                    <p>All Week, <b>8:30AM</b> – <b>4PM</b></p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.website')</div>
                                <div class="info-txt">
                                    <a href="www.disneyland.com" target="blank" class="web-site">www.disneyland.com</a>
                                </div>
                            </div>
                            <div class="review-info-block">
                                <div class="info-txt">
                                    <div class="chart-layer">
                                        <img src="http://placehold.it/585x290?text=hotel's_location_map" alt="chart">
                                    </div>
                                </div>
                            </div>
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li>@lang('place.reviews')</li>
                                    </ul>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li class="empty"><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>4</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link blue">@lang('buttons.general.more_dots')</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-foot-booking">
                            <h3 class="book-ttl">@lang('book.booking_options')</h3>
                            <table class="booking-table">
                                <tr>
                                    <th>@lang('book.booking_site')</th>
                                    <th>@lang('book.price_for')</th>
                                    <th>@lang('book.total')</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/23x23" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$424</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$213</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="post-block post-modal-place post-modal-thin-inner">
                    <div class="top-place-img">
                        <img src="http://placehold.it/615x250" alt="place image">
                    </div>
                    <div class="place-general-block">
                        <div class="place-review-block">
                            <div class="place-info">
                                <div class="place-name">@lang('place.name_airport', ['name' => 'Rabat-sale'])</div>
                                <div class="place-dist">@lang('place.airport_in_place', ['place' => 'Sale, Morocco'])</div>
                            </div>
                            <div class="place-review-layer">
                                <ul class="star-list">
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-half-star-icon"></i></li>
                                </ul>
                                &nbsp;
                                <span class="review-count">547</span>
                                &nbsp;
                                <span>reviews</span>
                            </div>
                        </div>
                        <div class="place-review-info-main">
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, commodi nobis
                                        quaerat nam impedit deleniti?</p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <address>2145 Hamilton Avenue, San Jose CA 95125</address>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.working_times')</div>
                                <div class="info-txt">
                                    <p>All Week, <b>8:30AM</b> – <b>4PM</b></p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.website')</div>
                                <div class="info-txt">
                                    <a href="www.disneyland.com" target="blank" class="web-site">www.disneyland.com</a>
                                </div>
                            </div>
                            <div class="review-info-block">
                                <div class="info-txt">
                                    <div class="chart-layer">
                                        <img src="http://placehold.it/585x290?text=hotel's_location_map" alt="chart">
                                    </div>
                                </div>
                            </div>
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li>@lang('place.reviews')</li>
                                    </ul>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li class="empty"><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>4</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link blue">@lang('buttons.general.more_dots')</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-foot-booking">
                            <h3 class="book-ttl">@lang('book.booking_options')</h3>
                            <table class="booking-table">
                                <tr>
                                    <th>@lang('book.booking_site')</th>
                                    <th>@lang('book.price_for')</th>
                                    <th>@lang('book.total')</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/23x23" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$424</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$213</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="post-block post-modal-place post-modal-thin-inner">
                    <div class="top-place-img">
                        <img src="http://placehold.it/615x250" alt="place image">
                    </div>
                    <div class="place-general-block">
                        <div class="place-review-block">
                            <div class="place-info">
                                <div class="place-name">@lang('place.name_airport', ['name' => 'Rabat-sale'])</div>
                                <div class="place-dist">@lang('place.airport_in_place', ['place' => 'Sale, Morocco'])</div>
                            </div>
                            <div class="place-review-layer">
                                <ul class="star-list">
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-half-star-icon"></i></li>
                                </ul>
                                &nbsp;
                                <span class="review-count">547</span>
                                &nbsp;
                                <span>@lang('place.reviews')</span>
                            </div>
                        </div>
                        <div class="place-review-info-main">
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, commodi nobis
                                        quaerat nam impedit deleniti?</p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <address>2145 Hamilton Avenue, San Jose CA 95125</address>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.working_times')</div>
                                <div class="info-txt">
                                    <p>All Week, <b>8:30AM</b> – <b>4PM</b></p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.website')</div>
                                <div class="info-txt">
                                    <a href="www.disneyland.com" target="blank" class="web-site">www.disneyland.com</a>
                                </div>
                            </div>
                            <div class="review-info-block">
                                <div class="info-txt">
                                    <div class="chart-layer">
                                        <img src="http://placehold.it/585x290?text=hotel's_location_map" alt="chart">
                                    </div>
                                </div>
                            </div>
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li>@lang('place.reviews')</li>
                                    </ul>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li class="empty"><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>4</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link blue">@lang('buttons.general.more_dots')</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-foot-booking">
                            <h3 class="book-ttl">@lang('book.booking_options')</h3>
                            <table class="booking-table">
                                <tr>
                                    <th>@lang('book.booking_site')</th>
                                    <th>@lang('book.price_for')</th>
                                    <th>@lang('book.total')</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/23x23" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$424</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$213</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="post-block post-modal-place post-modal-thin-inner">
                    <div class="top-place-img">
                        <img src="http://placehold.it/615x250" alt="place image">
                    </div>
                    <div class="place-general-block">
                        <div class="place-review-block">
                            <div class="place-info">
                                <div class="place-name">@lang('place.name_airport', ['name' => 'Rabat-sale'])</div>
                                <div class="place-dist">@lang('place.airport_in_place', ['place' => 'Sale, Morocco'])</div>
                            </div>
                            <div class="place-review-layer">
                                <ul class="star-list">
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-half-star-icon"></i></li>
                                </ul>
                                &nbsp;
                                <span class="review-count">547</span>
                                &nbsp;
                                <span>@lang('place.reviews')</span>
                            </div>
                        </div>
                        <div class="place-review-info-main">
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, commodi nobis
                                        quaerat nam impedit deleniti?</p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <address>2145 Hamilton Avenue, San Jose CA 95125</address>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.working_times')</div>
                                <div class="info-txt">
                                    <p>All Week, <b>8:30AM</b> – <b>4PM</b></p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.website')</div>
                                <div class="info-txt">
                                    <a href="www.disneyland.com" target="blank" class="web-site">www.disneyland.com</a>
                                </div>
                            </div>
                            <div class="review-info-block">
                                <div class="info-txt">
                                    <div class="chart-layer">
                                        <img src="http://placehold.it/585x290?text=hotel's_location_map" alt="chart">
                                    </div>
                                </div>
                            </div>
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li>@lang('place.reviews')</li>
                                    </ul>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li class="empty"><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>4</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link blue">@lang('buttons.general.more_dots')</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-foot-booking">
                            <h3 class="book-ttl">@lang('book.booking_options')</h3>
                            <table class="booking-table">
                                <tr>
                                    <th>@lang('book.booking_site')</th>
                                    <th>@lang('book.price_for')</th>
                                    <th>@lang('book.total')</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/23x23" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$424</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$213</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- hotel popup -->
<div class="modal fade" id="flightPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-full" role="document">
        <div class="modal-custom-block">
            <div class="hotel-popup-carousel owl-carousel flight-style">
                <div class="post-block post-modal-flight-block">
                    <div class="flight-general-block">
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl depart">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.depart')</span>
                                </div>
                                <span class="time">30h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl return">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.return')</span>
                                </div>
                                <span class="time">15h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-foot-booking">
                        <h3 class="book-ttl">@lang('book.booking_options')</h3>
                        <table class="booking-table">
                            <tr>
                                <th>@lang('book.booking_site')</th>
                                <th>@lang('book.class')</th>
                                <th>@lang('book.total')</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/65x16" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="post-block post-modal-flight-block">
                    <div class="flight-general-block">
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl depart">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.depart')</span>
                                </div>
                                <span class="time">30h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl return">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.return')</span>
                                </div>
                                <span class="time">15h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-foot-booking">
                        <h3 class="book-ttl">@lang('book.booking_options')</h3>
                        <table class="booking-table">
                            <tr>
                                <th>@lang('book.booking_site')</th>
                                <th>@lang('book.class')</th>
                                <th>@lang('book.total')</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/65x16" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="post-block post-modal-flight-block">
                    <div class="flight-general-block">
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl depart">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.depart')</span>
                                </div>
                                <span class="time">30h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl return">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.return')</span>
                                </div>
                                <span class="time">15h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-foot-booking">
                        <h3 class="book-ttl">@lang('book.booking_options')</h3>
                        <table class="booking-table">
                            <tr>
                                <th>@lang('book.booking_site')</th>
                                <th>@lang('book.class')</th>
                                <th>@lang('book.total')</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/65x16" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="post-block post-modal-flight-block">
                    <div class="flight-general-block">
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl depart">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.depart')</span>
                                </div>
                                <span class="time">30h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl return">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.return')</span>
                                </div>
                                <span class="time">15h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-foot-booking">
                        <h3 class="book-ttl">@lang('book.booking_options')</h3>
                        <table class="booking-table">
                            <tr>
                                <th>@lang('book.booking_site')</th>
                                <th>@lang('book.class')</th>
                                <th>@lang('book.total')</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/65x16" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="{{url('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
<script src="{{url('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
<script src="{{url('assets2/js/script.js')}}"></script>
<script>
    $(function () {
        $(".datepicker").datepicker();
    });
    $('#city_from').select2({
        placeholder: 'From',
        debug: true,

        ajax: {
            url: '{{url("book/ajaxSearchCity")}}',
            dataType: 'json',

            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
    });
    $('#city_to').select2({
        placeholder: 'To',
        debug: true,

        ajax: {
            url: '{{url("book/ajaxSearchCity")}}',
            dataType: 'json',

            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
    });
</script>
</body>

</html>