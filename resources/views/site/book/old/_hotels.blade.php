<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - flights, hotels</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">

            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li class="active"><a href="{{url('book/hotel')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('navs.general.home')</span>
                        </a></li>
                </ul>
            </div>

            <div class="top-banner-wrap">
                <div class="top-banner-block after-disabled"
                     style="background-image: url({{url('assets2/image/flight-hotel-bg.jpg')}}">
                    <div class="top-banner-flight-hotel">
                        <ul class="flight-hotel-tabs" role="tablist">
                            <li>
                                <a class="current" href="{{url('book/hotel')}}">
                                            <span class="circle-icon">
                                                <i class="trav-building-icon"></i>
                                            </span>
                                    <span>hotels</span>
                                </a>
                            </li>
                            <li>
                                <a class="" href="{{url('book/flight')}}">
                                            <span class="circle-icon">
                                                <i class="trav-angle-plane-icon"></i>
                                            </span>
                                    <span>flights</span>
                                </a>
                            </li>
                        </ul>
                        <div class="top-banner-inner">

                            <h2 class="ban-ttl">Find the Best Hotels Offers</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, distinctio!</p>
                        </div>
                        <form method="post" action="{{url('book/search-hotel')}}" class="top-banner-search-form">
                            <div class="flex-row">
                                <div class="flex-item fl-30 with-icon">
                                            <span class="icon-wrap">
                                                <i class="trav-location"></i>
                                            </span>
                                    <select class="flex-input" name="city_select" id="city_select"
                                            placeholder="Your destination City">
                                    </select>
                                </div>
                                <div class="flex-item fl-30 with-icon">
                                            <span class="icon-wrap">
                                                <i class="trav-event-icon"></i>
                                            </span>
                                    <input class="flex-input" name="daterange" id="daterange"
                                           placeholder="@lang('time.date')"
                                           value="" type="text">

                                </div>
                                <div class="flex-item fl-30">
                                    <select name="" id="" class="custom-select">
                                        <option value="1">1 Adult, Economy</option>
                                        <option value="2">item</option>
                                    </select>
                                </div>
                                <div class="flex-item fl-10">
                                    <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.search')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--
                                <div class="post-block post-flight-style">
                                    <div class="post-side-top">
                                        <h3 class="side-ttl"><i class="trav-trending-destination-icon"></i> Trending flights
                                        </h3>
                                        <div class="side-right-control">
                                            <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                                            <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
                                        </div>
                                    </div>
                                    <div class="post-side-inner">
                                        <div class="post-slide-wrap">
                                            <ul id="trendingFlights" class="post-slider">
                                                <li>
                                                    <div class="post-flight-inner">
                                                        <img src="http://placehold.it/327x180" alt="">
                                                        <div class="post-flight-info">
                                                            <div class="flight-inner">
                                                                <div class="fl-left">
                                                                    <p class="city-name">Barcelona</p>
                                                                    <p class="country-name">Spain</p>
                                                                    <p class="going-count">
                                                                        <span class="count">52</span>
                                                                        <span>going today</span>
                                                                    </p>
                                                                </div>
                                                                <div class="fl-right">
                                                                    <div class="flag-wrap">
                                                                        <img class="flag-image" src="http://placehold.it/32x22?text=mini+map" alt="">
                                                                    </div>
                                                                    <div class="btn-wrap">
                                                                        <button class="btn btn-light-primary btn-bordered">Book Flight</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <img src="http://placehold.it/327x180?text=direction+map" alt="">
                                                        <div class="post-direct-info">
                                                            <div class="direct-block">
                                                                <p class="dir-label">3h 45m</p>
                                                                <p class="dir-txt">@lang('book.direct_flight')</p>
                                                            </div>
                                                            <div class="direct-block">
                                                                <p class="dir-label">3120 km</p>
                                                                <p class="dir-txt">@lang('book.total_distance')</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="post-flight-inner">
                                                        <img src="http://placehold.it/327x180" alt="">
                                                        <div class="post-flight-info">
                                                            <div class="flight-inner">
                                                                <div class="fl-left">
                                                                    <p class="city-name">Amalfi</p>
                                                                    <p class="country-name">Italy</p>
                                                                    <p class="going-count">
                                                                        <span class="count">52</span>
                                                                        <span>going today</span>
                                                                    </p>
                                                                </div>
                                                                <div class="fl-right">
                                                                    <div class="flag-wrap">
                                                                        <img class="flag-image" src="http://placehold.it/32x22?text=mini+map" alt="">
                                                                    </div>
                                                                    <div class="btn-wrap">
                                                                        <button class="btn btn-light-primary btn-bordered">Book Flight</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <img src="http://placehold.it/327x180?text=direction+map" alt="">
                                                        <div class="post-direct-info">
                                                            <div class="direct-block">
                                                                <p class="dir-label">3h 45m</p>
                                                                <p class="dir-txt">@lang('book.direct_flight')</p>
                                                            </div>
                                                            <div class="direct-block">
                                                                <p class="dir-label">3120 km</p>
                                                                <p class="dir-txt">@lang('book.total_distance')</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="post-flight-inner">
                                                        <img src="http://placehold.it/327x180" alt="">
                                                        <div class="post-flight-info">
                                                            <div class="flight-inner">
                                                                <div class="fl-left">
                                                                    <p class="city-name">Tokyo</p>
                                                                    <p class="country-name">Japan</p>
                                                                    <p class="going-count">
                                                                        <span class="count">52</span>
                                                                        <span>going today</span>
                                                                    </p>
                                                                </div>
                                                                <div class="fl-right">
                                                                    <div class="flag-wrap">
                                                                        <img class="flag-image" src="http://placehold.it/32x22?text=mini+map" alt="">
                                                                    </div>
                                                                    <div class="btn-wrap">
                                                                        <button class="btn btn-light-primary btn-bordered">Book Flight</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <img src="http://placehold.it/327x180?text=direction+map" alt="">
                                                        <div class="post-direct-info">
                                                            <div class="direct-block">
                                                                <p class="dir-label">3h 45m</p>
                                                                <p class="dir-txt">@lang('book.direct_flight')</p>
                                                            </div>
                                                            <div class="direct-block">
                                                                <p class="dir-label">3120 km</p>
                                                                <p class="dir-txt">@lang('book.total_distance')</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
            -->

            @include('site/book/partials/hotels-by-city')

        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link href="{{url('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
<script src="{{url('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
<script src="{{url('assets2/js/script.js')}}"></script>

<script>
    $(function () {
        $('#daterange').daterangepicker({
            opens: 'left'
        }, function (start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
    });
    $('#city_select').select2({
        placeholder: 'Please enter your destination city',
        debug: true,

        ajax: {
            url: '{{url("book/ajaxSearchCity")}}',
            dataType: 'json',

            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
    });
</script>

</body>

</html>