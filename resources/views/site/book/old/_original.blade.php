<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - flights, hotels</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">

            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li class="active"><a href="#">
                            <i class="trav-home-icon"></i>
                            <span>@lang('navs.general.home')</span>
                        </a></li>
                </ul>
            </div>

            <div class="left-outside-menu-wrap bottom-menu-wrap" id="leftBottomMenu">
                <ul class="left-outside-menu">
                    <li class=""><a href="#">
              <span class="circle-icon">
                <i class="trav-newsfeed-icon"></i>
              </span>
                            <span>Newsfeed</span>
                        </a></li>
                    <li class=""><a href="#">
              <span class="circle-icon">
                <i class="trav-travel-mates-icon"></i>
              </span>
                            <span>Travel mate</span>
                        </a></li>
                </ul>
            </div>

            <div class="top-banner-wrap">
                <div class="top-banner-block after-disabled"
                     style="background-image: url({{url('assets2/image/flight-hotel-bg.jpg')}}">
                    <div class="top-banner-flight-hotel">
                        <ul class="flight-hotel-tabs" role="tablist">
                            <li>
                                <a class="current" href="#">
                    <span class="circle-icon">
                      <i class="trav-angle-plane-icon"></i>
                    </span>
                                    <span>hotels</span>
                                </a>
                            </li>
                            <li>
                                <a class="" href="#">
                    <span class="circle-icon">
                      <i class="trav-building-icon"></i>
                    </span>
                                    <span>flights</span>
                                </a>
                            </li>
                        </ul>
                        <div class="top-banner-inner">

                            <h2 class="ban-ttl">Find the Best Hotels Offers</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, distinctio!</p>
                        </div>
                        <form action="/" class="top-banner-search-form">
                            <div class="flex-row">
                                <div class="flex-item fl-30 with-icon">
                    <span class="icon-wrap">
                      <i class="trav-location"></i>
                    </span>
                                    <input class="flex-input" id="" placeholder="City" value="New York" type="text">
                                </div>
                                <div class="flex-item fl-30 with-icon">
                    <span class="icon-wrap">
                      <i class="trav-event-icon"></i>
                    </span>
                                <!-- <input class="flex-input" id="" placeholder="Date" value="@lang('time.week.short.thursday') 2/8 - Wed 2/15" type="text"> -->
                                    <select name="" id="" class="custom-select">
                                        <option value="1">@lang('time.week.short.thursday') 2/8 - Wed 2/15</option>
                                        <option value="2">item</option>
                                    </select>
                                </div>
                                <div class="flex-item fl-30">
                                    <select name="" id="" class="custom-select">
                                        <option value="1">1 Adult, Economy</option>
                                        <option value="2">item</option>
                                    </select>
                                </div>
                                <div class="flex-item fl-10">
                                    <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.search')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="post-block post-flight-style">
                <div class="post-side-top">
                    <h3 class="side-ttl"><i class="trav-trending-destination-icon"></i> Trending flights
                    </h3>
                    <div class="side-right-control">
                        <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                        <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
                    </div>
                </div>
                <div class="post-side-inner">
                    <div class="post-slide-wrap">
                        <ul id="trendingFlights" class="post-slider">
                            <li>
                                <div class="post-flight-inner">
                                    <img src="http://placehold.it/327x180" alt="">
                                    <div class="post-flight-info">
                                        <div class="flight-inner">
                                            <div class="fl-left">
                                                <p class="city-name">Barcelona</p>
                                                <p class="country-name">Spain</p>
                                                <p class="going-count">
                                                    <span class="count">52</span>
                                                    <span>going today</span>
                                                </p>
                                            </div>
                                            <div class="fl-right">
                                                <div class="flag-wrap">
                                                    <img class="flag-image"
                                                         src="http://placehold.it/32x22?text=mini+map" alt="">
                                                </div>
                                                <div class="btn-wrap">
                                                    <button class="btn btn-light-primary btn-bordered">Book Flight
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="http://placehold.it/327x180?text=direction+map" alt="">
                                    <div class="post-direct-info">
                                        <div class="direct-block">
                                            <p class="dir-label">3h 45m</p>
                                            <p class="dir-txt">@lang('book.direct_flight')</p>
                                        </div>
                                        <div class="direct-block">
                                            <p class="dir-label">3120 km</p>
                                            <p class="dir-txt">@lang('book.total_distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-flight-inner">
                                    <img src="http://placehold.it/327x180" alt="">
                                    <div class="post-flight-info">
                                        <div class="flight-inner">
                                            <div class="fl-left">
                                                <p class="city-name">Amalfi</p>
                                                <p class="country-name">Italy</p>
                                                <p class="going-count">
                                                    <span class="count">52</span>
                                                    <span>going today</span>
                                                </p>
                                            </div>
                                            <div class="fl-right">
                                                <div class="flag-wrap">
                                                    <img class="flag-image"
                                                         src="http://placehold.it/32x22?text=mini+map" alt="">
                                                </div>
                                                <div class="btn-wrap">
                                                    <button class="btn btn-light-primary btn-bordered">Book Flight
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="http://placehold.it/327x180?text=direction+map" alt="">
                                    <div class="post-direct-info">
                                        <div class="direct-block">
                                            <p class="dir-label">3h 45m</p>
                                            <p class="dir-txt">@lang('book.direct_flight')</p>
                                        </div>
                                        <div class="direct-block">
                                            <p class="dir-label">3120 km</p>
                                            <p class="dir-txt">@lang('book.total_distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="post-flight-inner">
                                    <img src="http://placehold.it/327x180" alt="">
                                    <div class="post-flight-info">
                                        <div class="flight-inner">
                                            <div class="fl-left">
                                                <p class="city-name">Tokyo</p>
                                                <p class="country-name">Japan</p>
                                                <p class="going-count">
                                                    <span class="count">52</span>
                                                    <span>going today</span>
                                                </p>
                                            </div>
                                            <div class="fl-right">
                                                <div class="flag-wrap">
                                                    <img class="flag-image"
                                                         src="http://placehold.it/32x22?text=mini+map" alt="">
                                                </div>
                                                <div class="btn-wrap">
                                                    <button class="btn btn-light-primary btn-bordered">Book Flight
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="http://placehold.it/327x180?text=direction+map" alt="">
                                    <div class="post-direct-info">
                                        <div class="direct-block">
                                            <p class="dir-label">3h 45m</p>
                                            <p class="dir-txt">@lang('book.direct_flight')</p>
                                        </div>
                                        <div class="direct-block">
                                            <p class="dir-label">3120 km</p>
                                            <p class="dir-txt">@lang('book.total_distance')</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="post-block post-flight-style">
                <div class="post-side-top">
                    <h3 class="side-ttl">Hotels by City</h3>
                </div>
                <div class="post-hotel-inner">
                    <div class="hotel-block hb-50">
                        <div class="hotel-block-inner">
                            <img class="city-img" src="http://placehold.it/500x320" alt="">
                            <div class="hotel-info">
                                <div class="city-name">
                                    <span>Paris</span>
                                    <img src="http://placehold.it/28x18" alt="" class="flag-img">
                                </div>
                                <div class="hotels-count">
                                    @choice('book.count_hotels', 2, ['count' => '3,748'])
                                </div>
                            </div>
                            <button class="btn btn-hotel-style">
                                <span class="btn-txt">Average price</span>
                                <span class="btn-price">
                    <b>usd</b>
                    245
                  </span>
                            </button>
                        </div>
                    </div>
                    <div class="hotel-block hb-50">
                        <div class="hotel-block-inner">
                            <img class="city-img" src="http://placehold.it/500x320" alt="">
                            <div class="hotel-info">
                                <div class="city-name">
                                    <span>Marseille</span>
                                    <img src="http://placehold.it/28x18" alt="" class="flag-img">
                                </div>
                                <div class="hotels-count">
                                    @choice('book.count_hotels', 2, ['count' => '3,748'])
                                </div>
                            </div>
                            <button class="btn btn-hotel-style">
                                <span class="btn-txt">Average price</span>
                                <span class="btn-price">
                    <b>usd</b>
                    245
                  </span>
                            </button>
                        </div>
                    </div>
                    <div class="hotel-block hb-33">
                        <div class="hotel-block-inner">
                            <img class="city-img" src="http://placehold.it/326x230" alt="">
                            <div class="hotel-info">
                                <div class="city-name">
                                    <span>Marrakech</span>
                                    <img src="http://placehold.it/28x18" alt="" class="flag-img">
                                </div>
                                <div class="hotels-count">
                                    @choice('book.count_hotels', 2, ['count' => '3,748'])
                                </div>
                            </div>
                            <button class="btn btn-hotel-style">
                                <span class="btn-txt">Average price</span>
                                <span class="btn-price">
                    <b>usd</b>
                    245
                  </span>
                            </button>
                        </div>
                    </div>
                    <div class="hotel-block hb-33">
                        <div class="hotel-block-inner">
                            <img class="city-img" src="http://placehold.it/326x230" alt="">
                            <div class="hotel-info">
                                <div class="city-name">
                                    <span>Casablanca</span>
                                    <img src="http://placehold.it/28x18" alt="" class="flag-img">
                                </div>
                                <div class="hotels-count">
                                    @choice('book.count_hotels', 2, ['count' => '3,748'])
                                </div>
                            </div>
                            <button class="btn btn-hotel-style">
                                <span class="btn-txt">Average price</span>
                                <span class="btn-price">
                    <b>usd</b>
                    245
                  </span>
                            </button>
                        </div>
                    </div>
                    <div class="hotel-block hb-33">
                        <div class="hotel-block-inner">
                            <img class="city-img" src="http://placehold.it/326x230" alt="">
                            <div class="hotel-info">
                                <div class="city-name">
                                    <span>Fes</span>
                                    <img src="http://placehold.it/28x18" alt="" class="flag-img">
                                </div>
                                <div class="hotels-count">
                                    @choice('book.count_hotels', 2, ['count' => '3,748'])
                                </div>
                            </div>
                            <button class="btn btn-hotel-style">
                                <span class="btn-txt">Average price</span>
                                <span class="btn-price">
                    <b>usd</b>
                    245
                  </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="top-banner-wrap">
                <div class="top-banner-block flight-banner">
                    <div class="top-banner-flight-hotel">
                        <a href="#" class="invert-link">
                            <i class="trav-invert-icon"></i>
                        </a>
                        <div class="flight-banner-city-block">
                            <img src="./assets/image/fl-banner-city-1.jpg" alt="">
                            <h2 class="city-ttl">Vienna</h2>
                        </div>
                        <div class="flight-banner-city-block">
                            <img src="./assets/image/fl-banner-city-2.jpg" alt="">
                            <h2 class="city-ttl">Chiang Mai</h2>
                        </div>

                        <ul class="flight-hotel-tabs" role="tablist">
                            <li>
                                <a class="current" href="#">
                                    <i class="trav-angle-left"></i>
                                    <span>@lang("other.back")</span>
                                </a>
                            </li>
                        </ul>

                        <ul class="flight-tag-list">
                            <li>1 Person</li>
                            <li>21 April / 28 April</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="post-block post-flight-tab-block">
                <div class="post-tab-inner">
                    <div class="tab-left-side">
                        <div class="sort-by-select">
                            <label>Sort by</label>
                            <div class="sort-select-wrap">
                                <select class="form-control" id="sortBy">
                                    <option>Duration</option>
                                    <option>Item</option>
                                    <option>Item2</option>
                                </select>
                            </div>
                        </div>
                        <div class="detail-list">
                            <div class="detail-block">
                                <b>Operator</b>
                            </div>
                            <div class="detail-block current">
                                <b>Departure</b>
                            </div>
                            <div class="detail-block">
                                <b>Economy basic</b>
                            </div>
                        </div>
                    </div>
                    <div class="tab-right-side">
                        <div class="post-tab-filter">
                            <div class="dropdown">
                                <a class="dropdown-toggle filter-toggler" role="button" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <i class="trav-filter-icon"></i>
                                    <span>Filter</span>
                                </a>

                                <div class="dropdown-menu filter-dropdown-menu dropdown-menu-right">
                                    <div class="select-block">
                                        <select name="" id="" class="custom-select">
                                            <option value="1">Recommended</option>
                                            <option value="2">item</option>
                                        </select>
                                    </div>
                                    <div class="filter-inner-block">
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="filterRadios" id="nonstop"
                                                   value="nonstop" type="radio">
                                            <label for="nonstop">nonstop</label>
                                        </div>
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="filterRadios" id="1stop"
                                                   value="1stop" type="radio">
                                            <label for="1stop">1 stop</label>
                                        </div>
                                        <div class="custom-check-label">
                                            <input class="custom-check-input" name="filterRadios" id="2plusStop"
                                                   value="2plusStop" type="radio">
                                            <label for="2plusStop">2+ stop</label>
                                        </div>
                                    </div>
                                    <div class="filter-inner-block">
                                        <h3 class="block-ttl">Airlines</h3>
                                        <div class="custom-check-label checkbox-style">
                                            <input class="custom-check-input" type="checkbox" value=""
                                                   id="checkAirline1">
                                            <label class="form-check-label" for="checkAirline1">
                                                <span>Alaska airlines</span>
                                                <span class="count">4</span>
                                            </label>
                                        </div>
                                        <div class="custom-check-label checkbox-style">
                                            <input class="custom-check-input" type="checkbox" value=""
                                                   id="checkAirline2">
                                            <label class="form-check-label" for="checkAirline2">
                                                <span>American airlines</span>
                                                <span class="count">2</span>
                                            </label>
                                        </div>
                                        <div class="custom-check-label checkbox-style">
                                            <input class="custom-check-input" type="checkbox" value=""
                                                   id="checkAirline3">
                                            <label class="form-check-label" for="checkAirline3">
                                                <span>Amtrack</span>
                                                <span class="count">6</span>
                                            </label>
                                        </div>
                                        <div class="custom-check-label checkbox-style">
                                            <input class="custom-check-input" type="checkbox" value=""
                                                   id="checkAirline4">
                                            <label class="form-check-label" for="checkAirline4">
                                                <span>Delta</span>
                                                <span class="count">9</span>
                                            </label>
                                        </div>
                                        <div class="custom-check-label checkbox-style">
                                            <input class="custom-check-input" type="checkbox" value=""
                                                   id="checkAirline5">
                                            <label class="form-check-label" for="checkAirline5">
                                                <span>JetBlue</span>
                                                <span class="count">1</span>
                                            </label>
                                        </div>
                                        <div class="custom-check-label checkbox-style">
                                            <input class="custom-check-input" type="checkbox" value=""
                                                   id="checkAirline6">
                                            <label class="form-check-label" for="checkAirline6">
                                                <span>Southwest</span>
                                                <span class="count">12</span>
                                            </label>
                                        </div>
                                        <div class="custom-check-label checkbox-style">
                                            <input class="custom-check-input" type="checkbox" value=""
                                                   id="checkAirline7">
                                            <label class="form-check-label" for="checkAirline7">
                                                <span>Spirit airline</span>
                                                <span class="count">3</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="post-block post-flight-tab-block">
                <div class="post-tab-inner">
                    <div class="tab-left-side">
                        <div class="detail-list">
                            <div class="detail-block current">
                                <b>Recommendation</b>
                                <i class="fa fa-caret-down"></i>
                            </div>
                            <div class="detail-block">
                                <b>Stars</b>
                                <i class="fa fa-caret-down"></i>
                            </div>
                            <div class="detail-block">
                                <b>Price</b>
                                <i class="fa fa-caret-down"></i>
                            </div>
                        </div>
                    </div>
                    <div class="tab-right-side">
                        <div class="post-tab-filter">
                            <div class="dropdown">
                                <a class="dropdown-toggle filter-toggler" role="button" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <i class="trav-filter-icon"></i>
                                    <span>Filter</span>
                                </a>

                                <div class="dropdown-menu filter-dropdown-menu dropdown-menu-right">
                                    <div class="select-block">
                                        <select name="" id="" class="custom-select">
                                            <option value="1">Recommended</option>
                                            <option value="2">item</option>
                                        </select>
                                    </div>
                                    <div class="filter-inner-block">
                                        <h3 class="block-ttl">Stars</h3>
                                        <ul class="filter-star-list">
                                            <li class="active">
                                                <i class="trav-empty-star-icon"></i>
                                                <span class="count">1</span>
                                            </li>
                                            <li>
                                                <i class="trav-empty-star-icon"></i>
                                                <span class="count">2</span>
                                            </li>
                                            <li>
                                                <i class="trav-empty-star-icon"></i>
                                                <span class="count">3</span>
                                            </li>
                                            <li>
                                                <i class="trav-empty-star-icon"></i>
                                                <span class="count">4</span>
                                            </li>
                                            <li>
                                                <i class="trav-empty-star-icon"></i>
                                                <span class="count">5</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="filter-inner-block">
                                        <h3 class="block-ttl">Review score</h3>
                                        <ul class="filter-score-list">
                                            <li class="active"><span>0+</span></li>
                                            <li class="active"><span>2+</span></li>
                                            <li><span>4+</span></li>
                                            <li><span>6+</span></li>
                                            <li><span>8+</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#hotelPopup">
                        Hotel popup
                    </button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#flightPopup">
                        Flight popup
                    </button>

                    <div class="post-block post-flight-depart">
                        <div class="post-content-inner">
                            <div class="flight-wrap">
                                <div class="flight-row-wrap">
                                    <div class="flight-row">
                                        <div class="img-wrap">
                                            <img src="./assets/image/airlines-brand-1.png" alt="brand">
                                            <h5 class="content-ttl">Alaska Airlines</h5>
                                        </div>
                                        <div class="flight-content">
                                            <div class="flight-inner">
                                                <div class="flight-txt">
                                                    <div class="depart-col">
                                                        <h4>@lang('book.depart')</h4>
                                                        <div class="date">
                                                            <p>Sat, Jan 13</p>
                                                            <p>4:10 pm</p>
                                                        </div>
                                                    </div>
                                                    <div class="depart-col shape-col">
                                                        <div class="time-bordered">
                                                            <span>4h 3m</span>
                                                        </div>
                                                        <div class="time-sub">
                                                            <div class="sub-cell">@lang('book.fco')</div>
                                                            <div class="sub-cell">@lang('book.fco')</div>
                                                        </div>
                                                    </div>
                                                    <div class="depart-col">
                                                        <div class="date">
                                                            <p>Sat, Jan 13</p>
                                                            <p>8:13 pm</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flight-row">
                                        <div class="img-wrap">
                                            <img src="./assets/image/airlines-brand-2.png" alt="brand">
                                            <h5 class="content-ttl">Emirates Airlines</h5>
                                        </div>
                                        <div class="flight-content">
                                            <div class="flight-inner">
                                                <div class="flight-txt">
                                                    <div class="depart-col">
                                                        <h4>@lang('book.depart')</h4>
                                                        <div class="date">
                                                            <p>Sun, 1 Feb</p>
                                                            <p>9:10 pm</p>
                                                        </div>
                                                    </div>
                                                    <div class="depart-col shape-col">
                                                        <div class="time-bordered">
                                                            <span>6h 12m</span>
                                                        </div>
                                                        <div class="time-sub">
                                                            <div class="sub-cell">@lang('book.fco')</div>
                                                            <div class="sub-cell">@lang('book.fco')</div>
                                                        </div>
                                                    </div>
                                                    <div class="depart-col">
                                                        <div class="date">
                                                            <p>Sat, Jan 13</p>
                                                            <p>3:13 pm</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-price-inner">
                                    <div class="flight-price">$850</div>
                                    <button type="button"
                                            class="btn btn-light-bg-grey btn-bordered">@lang('book.view_deal')</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="post-block post-flight-depart">
                        <div class="post-content-inner">
                            <div class="flight-wrap">
                                <div class="flight-row-wrap">
                                    <div class="flight-row">
                                        <div class="img-wrap">
                                            <img src="./assets/image/airlines-brand-1.png" alt="brand">
                                            <h5 class="content-ttl">Alaska Airlines</h5>
                                        </div>
                                        <div class="flight-content">
                                            <div class="flight-inner">
                                                <div class="flight-txt">
                                                    <div class="depart-col">
                                                        <h4>@lang('book.depart')</h4>
                                                        <div class="date">
                                                            <p>Sat, Jan 13</p>
                                                            <p>4:10 pm</p>
                                                        </div>
                                                    </div>
                                                    <div class="depart-col shape-col">
                                                        <div class="time-bordered">
                                                            <span>4h 3m</span>
                                                        </div>
                                                        <div class="time-sub">
                                                            <div class="sub-cell">@lang('book.fco')</div>
                                                            <div class="sub-cell">@lang('book.fco')</div>
                                                        </div>
                                                    </div>
                                                    <div class="depart-col">
                                                        <div class="date">
                                                            <p>Sat, Jan 13</p>
                                                            <p>8:13 pm</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flight-row">
                                        <div class="img-wrap">
                                            <img src="./assets/image/airlines-brand-2.png" alt="brand">
                                            <h5 class="content-ttl">Emirates Airlines</h5>
                                        </div>
                                        <div class="flight-content">
                                            <div class="flight-inner">
                                                <div class="flight-txt">
                                                    <div class="depart-col">
                                                        <h4>@lang('book.depart')</h4>
                                                        <div class="date">
                                                            <p>Sun, 1 Feb</p>
                                                            <p>9:10 pm</p>
                                                        </div>
                                                    </div>
                                                    <div class="depart-col shape-col">
                                                        <div class="time-bordered">
                                                            <span>6h 12m</span>
                                                        </div>
                                                        <div class="time-sub">
                                                            <div class="sub-cell">@lang('book.fco')</div>
                                                            <div class="sub-cell">@lang('book.fco')</div>
                                                        </div>
                                                    </div>
                                                    <div class="depart-col">
                                                        <div class="date">
                                                            <p>Sat, Jan 13</p>
                                                            <p>3:13 pm</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-price-inner">
                                    <div class="flight-price">$850</div>
                                    <button type="button"
                                            class="btn btn-light-bg-grey btn-bordered">@lang('book.view_deal')</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="post-block post-flight-depart">
                        <div class="post-content-inner">
                            <div class="flight-wrap">
                                <div class="flight-row-wrap">
                                    <div class="flight-row">
                                        <div class="img-wrap">
                                            <img src="http://placehold.it/140x140" alt="logo">
                                        </div>
                                        <div class="flight-content">
                                            <h3 class="content-ttl">The melrose georgetown hotel</h3>
                                            <div class="com-star-block">
                                                <ul class="com-star-list">
                                                    <li><i class="trav-star-icon"></i></li>
                                                    <li><i class="trav-star-icon"></i></li>
                                                    <li><i class="trav-star-icon"></i></li>
                                                    <li><i class="trav-star-icon"></i></li>
                                                    <li><i class="trav-star-icon"></i></li>
                                                </ul>
                                            </div>
                                            <div class="flight-content-inner">
                                                <ul class="foot-avatar-list">
                                                    <li><img class="small-ava" src="http://placehold.it/20x20"
                                                             alt="ava"></li><!--
                            -->
                                                    <li><img class="small-ava" src="http://placehold.it/20x20"
                                                             alt="ava"></li><!--
                            -->
                                                    <li><img class="small-ava" src="http://placehold.it/20x20"
                                                             alt="ava"></li>
                                                </ul>
                                                <span>@lang('report.count_others_have_visited_this_place', ['count' => '+21'])</span>
                                            </div>
                                            <div class="flight-inner">
                                                <div class="flight-txt bordered">
                                                    <div class="depart-col">
                                                        <div class="depart-inner">
                                                            <span class="label">8.9</span>
                                                            <p><b>Excellent</b></p>
                                                            <p>@lang('report.count_review', ['count' => '1,882'])</p>
                                                        </div>
                                                    </div>
                                                    <div class="depart-col">
                                                        <div class="depart-inner">
                                                            <p><b>Location</b></p>
                                                            <p>New York City</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-price-inner">
                                    <div class="flight-price">$151</div>
                                    <a href="#" class="hotel-link">
                                        <span>Hotels.com</span>
                                    </a>
                                    <button type="button"
                                            class="btn btn-light-bg-grey btn-bordered">@lang('book.view_deal')</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">

                        <div class="post-block post-flight-info-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/390x205" alt="">
                            </div>
                            <div class="flight-info-block">
                                <div class="flight-info-content">
                                    <div class="flight-info-row">
                                        <div class="flight-inner">
                                            <p class="label">@lang('place.from')</p>
                                            <h4 class="ttl">Vienna</h4>
                                            <p class="sub-ttl">@lang('book.nag')</p>
                                        </div>
                                        <div class="flight-inner">
                                            <p class="label">@lang('place.to')</p>
                                            <h4 class="ttl">Chiang Mai</h4>
                                            <p class="sub-ttl">@lang('book.fco')</p>
                                        </div>
                                    </div>
                                    <div class="flight-info-row">
                                        <div class="flight-inner">
                                            <p class="label">@lang('profile.traveller')</p>
                                            <h4 class="ttl-sm">@lang('book.count_adult', ['count' => 1])</h4>
                                        </div>
                                        <div class="flight-inner">
                                            <p class="label">@lang('book.class')</p>
                                            <h4 class="ttl-sm">@lang('book.economy')</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-info-foot">
                                    <p>Connecting 2 or more stops</p>
                                    <div class="link-wrap">
                                        <a href="#" class="place-link">Air Vienna,</a>
                                        <a href="#" class="place-link">Emirates,</a>
                                        <a href="#" class="place-link">Chiabg Mai</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="post-block post-flight-info-block">
                            <div class="img-wrap">
                                <img src="http://placehold.it/390x205" alt="">
                            </div>
                            <div class="flight-info-block">
                                <div class="flight-info-content">
                                    <div class="flight-info-row">
                                        <div class="flight-inner">
                                            <p class="label">@lang('place.to')</p>
                                            <h4 class="ttl">Chiang Mai</h4>
                                            <p class="sub-ttl">@lang('book.fco')</p>
                                        </div>
                                    </div>
                                    <div class="flight-info-row">
                                        <div class="flight-inner">
                                            <p class="label">@lang('profile.traveller')</p>
                                            <h4 class="ttl-sm">@lang('book.count_adult', ['count' => 1])</h4>
                                        </div>
                                        <div class="flight-inner">
                                            <p class="label">@lang('book.class')</p>
                                            <h4 class="ttl-sm">@lang('book.economy')</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-info-foot">
                                    <p>Connecting 2 or more stops</p>
                                    <div class="link-wrap">
                                        <a href="#" class="place-link">Air Vienna,</a>
                                        <a href="#" class="place-link">Emirates,</a>
                                        <a href="#" class="place-link">Chiabg Mai</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                                <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                                <li><a href="{{url('/')}}">Advertising</a></li>
                                <li><a href="{{url('/')}}">Cookies</a></li>
                                <li><a href="{{url('/')}}">More</a></li>
                            </ul>
                            <p class="copyright">Travooo &copy; 2017</p>
                        </div>
                    </aside>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- modals -->
<!-- hotel popup -->
<div class="modal fade" id="hotelPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-full" role="document">
        <div class="modal-custom-block">
            <div class="hotel-popup-carousel owl-carousel">
                <div class="post-block post-modal-place post-modal-thin-inner">
                    <div class="top-place-img">
                        <img src="http://placehold.it/615x250" alt="place image">
                    </div>
                    <div class="place-general-block">
                        <div class="place-review-block">
                            <div class="place-info">
                                <div class="place-name">@lang('place.name_airport', ['name' => 'Rabat-sale'])</div>
                                <div class="place-dist">@lang('place.airport_in_place', ['place' => 'Sale, Morocco'])</div>
                            </div>
                            <div class="place-review-layer">
                                <ul class="star-list">
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-half-star-icon"></i></li>
                                </ul>
                                &nbsp;
                                <span class="review-count">547</span>
                                &nbsp;
                                <span>reviews</span>
                            </div>
                        </div>
                        <div class="place-review-info-main">
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, commodi nobis
                                        quaerat nam impedit deleniti?</p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <address>2145 Hamilton Avenue, San Jose CA 95125</address>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.working_times')</div>
                                <div class="info-txt">
                                    <p>All Week, <b>8:30AM</b> – <b>4PM</b></p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.website')</div>
                                <div class="info-txt">
                                    <a href="www.disneyland.com" target="blank" class="web-site">www.disneyland.com</a>
                                </div>
                            </div>
                            <div class="review-info-block">
                                <div class="info-txt">
                                    <div class="chart-layer">
                                        <img src="http://placehold.it/585x290?text=hotel's_location_map" alt="chart">
                                    </div>
                                </div>
                            </div>
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li>@lang('place.reviews')</li>
                                    </ul>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li class="empty"><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>4</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link blue">@lang('buttons.general.more_dots')</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-foot-booking">
                            <h3 class="book-ttl">@lang('book.booking_options')</h3>
                            <table class="booking-table">
                                <tr>
                                    <th>@lang('book.booking_site')</th>
                                    <th>@lang('book.price_for')</th>
                                    <th>@lang('book.total')</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/23x23" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$424</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$213</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="post-block post-modal-place post-modal-thin-inner">
                    <div class="top-place-img">
                        <img src="http://placehold.it/615x250" alt="place image">
                    </div>
                    <div class="place-general-block">
                        <div class="place-review-block">
                            <div class="place-info">
                                <div class="place-name">@lang('place.name_airport', ['name' => 'Rabat-sale'])</div>
                                <div class="place-dist">@lang('place.airport_in_place', ['place' => 'Sale, Morocco'])</div>
                            </div>
                            <div class="place-review-layer">
                                <ul class="star-list">
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-half-star-icon"></i></li>
                                </ul>
                                &nbsp;
                                <span class="review-count">547</span>
                                &nbsp;
                                <span>reviews</span>
                            </div>
                        </div>
                        <div class="place-review-info-main">
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, commodi nobis
                                        quaerat nam impedit deleniti?</p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <address>2145 Hamilton Avenue, San Jose CA 95125</address>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.working_times')</div>
                                <div class="info-txt">
                                    <p>All Week, <b>8:30AM</b> – <b>4PM</b></p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.website')</div>
                                <div class="info-txt">
                                    <a href="www.disneyland.com" target="blank" class="web-site">www.disneyland.com</a>
                                </div>
                            </div>
                            <div class="review-info-block">
                                <div class="info-txt">
                                    <div class="chart-layer">
                                        <img src="http://placehold.it/585x290?text=hotel's_location_map" alt="chart">
                                    </div>
                                </div>
                            </div>
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li>@lang('place.reviews')</li>
                                    </ul>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li class="empty"><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>4</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link blue">@lang('buttons.general.more_dots')</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-foot-booking">
                            <h3 class="book-ttl">@lang('book.booking_options')</h3>
                            <table class="booking-table">
                                <tr>
                                    <th>@lang('book.booking_site')</th>
                                    <th>@lang('book.price_for')</th>
                                    <th>@lang('book.total')</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/23x23" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$424</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$213</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="post-block post-modal-place post-modal-thin-inner">
                    <div class="top-place-img">
                        <img src="http://placehold.it/615x250" alt="place image">
                    </div>
                    <div class="place-general-block">
                        <div class="place-review-block">
                            <div class="place-info">
                                <div class="place-name">@lang('place.name_airport', ['name' => 'Rabat-sale'])</div>
                                <div class="place-dist">@lang('place.airport_in_place', ['place' => 'Sale, Morocco'])</div>
                            </div>
                            <div class="place-review-layer">
                                <ul class="star-list">
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-half-star-icon"></i></li>
                                </ul>
                                &nbsp;
                                <span class="review-count">547</span>
                                &nbsp;
                                <span>reviews</span>
                            </div>
                        </div>
                        <div class="place-review-info-main">
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, commodi nobis
                                        quaerat nam impedit deleniti?</p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <address>2145 Hamilton Avenue, San Jose CA 95125</address>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.working_times')</div>
                                <div class="info-txt">
                                    <p>All Week, <b>8:30AM</b> – <b>4PM</b></p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.website')</div>
                                <div class="info-txt">
                                    <a href="www.disneyland.com" target="blank" class="web-site">www.disneyland.com</a>
                                </div>
                            </div>
                            <div class="review-info-block">
                                <div class="info-txt">
                                    <div class="chart-layer">
                                        <img src="http://placehold.it/585x290?text=hotel's_location_map" alt="chart">
                                    </div>
                                </div>
                            </div>
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li>@lang('place.reviews')</li>
                                    </ul>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li class="empty"><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>4</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link blue">@lang('buttons.general.more_dots')</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-foot-booking">
                            <h3 class="book-ttl">@lang('book.booking_options')</h3>
                            <table class="booking-table">
                                <tr>
                                    <th>@lang('book.booking_site')</th>
                                    <th>@lang('book.price_for')</th>
                                    <th>@lang('book.total')</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/23x23" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$424</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$213</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="post-block post-modal-place post-modal-thin-inner">
                    <div class="top-place-img">
                        <img src="http://placehold.it/615x250" alt="place image">
                    </div>
                    <div class="place-general-block">
                        <div class="place-review-block">
                            <div class="place-info">
                                <div class="place-name">@lang('place.name_airport', ['name' => 'Rabat-sale'])</div>
                                <div class="place-dist">@lang('place.airport_in_place', ['place' => 'Sale, Morocco'])</div>
                            </div>
                            <div class="place-review-layer">
                                <ul class="star-list">
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-star-icon"></i></li>
                                    <li><i class="trav-half-star-icon"></i></li>
                                </ul>
                                &nbsp;
                                <span class="review-count">547</span>
                                &nbsp;
                                <span>@lang('place.reviews')</span>
                            </div>
                        </div>
                        <div class="place-review-info-main">
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, commodi nobis
                                        quaerat nam impedit deleniti?</p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('book.short_description')</div>
                                <div class="info-txt">
                                    <address>2145 Hamilton Avenue, San Jose CA 95125</address>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.working_times')</div>
                                <div class="info-txt">
                                    <p>All Week, <b>8:30AM</b> – <b>4PM</b></p>
                                </div>
                            </div>
                            <div class="review-info-block half-block">
                                <div class="info-ttl">@lang('place.website')</div>
                                <div class="info-txt">
                                    <a href="www.disneyland.com" target="blank" class="web-site">www.disneyland.com</a>
                                </div>
                            </div>
                            <div class="review-info-block">
                                <div class="info-txt">
                                    <div class="chart-layer">
                                        <img src="http://placehold.it/585x290?text=hotel's_location_map" alt="chart">
                                    </div>
                                </div>
                            </div>
                            <div class="post-comment-layer">
                                <div class="post-comment-top-info">
                                    <ul class="comment-filter">
                                        <li>@lang('place.reviews')</li>
                                    </ul>
                                </div>
                                <div class="post-comment-wrapper">
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Amine</a>
                                                <a href="#" class="comment-nickname">@ak0117</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex
                                                    doloribus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li class="empty"><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>4</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post-comment-row">
                                        <div class="post-com-avatar-wrap">
                                            <img src="http://placehold.it/45x45" alt="">
                                        </div>
                                        <div class="post-comment-text">
                                            <div class="post-com-name-layer">
                                                <a href="#" class="comment-name">Katherin</a>
                                                <a href="#" class="comment-nickname">@katherin</a>
                                            </div>
                                            <div class="comment-txt">
                                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus
                                                    labore tenetur vel. Neque molestiae repellat culpa qui odit
                                                    delectus.</p>
                                            </div>
                                            <div class="comment-bottom-info">
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                        <li><i class="trav-star-icon"></i></li>
                                                    </ul>
                                                    <span class="count">
                              <b>5</b> / 5
                            </span>
                                                </div>
                                                <div class="dot">·</div>
                                                <div class="com-time">6 hours ago</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="load-more-link blue">@lang('buttons.general.more_dots')</a>
                                </div>
                            </div>
                        </div>
                        <div class="post-foot-booking">
                            <h3 class="book-ttl">@lang('book.booking_options')</h3>
                            <table class="booking-table">
                                <tr>
                                    <th>@lang('book.booking_site')</th>
                                    <th>@lang('book.price_for')</th>
                                    <th>@lang('book.total')</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/23x23" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$424</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                    </td>
                                    <td>
                                        <b>@lang('time.count_nights', ['count' => 8])</b>
                                    </td>
                                    <td>
                                        <b>$213</b>
                                    </td>
                                    <td>
                                        <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- hotel popup -->
<div class="modal fade" id="flightPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <button class="modal-close" type="button" data-dismiss="modal" aria-label="Close">
        <i class="trav-close-icon"></i>
    </button>
    <div class="modal-dialog modal-custom-style modal-full" role="document">
        <div class="modal-custom-block">
            <div class="hotel-popup-carousel owl-carousel flight-style">
                <div class="post-block post-modal-flight-block">
                    <div class="flight-general-block">
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl depart">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.depart')</span>
                                </div>
                                <span class="time">30h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl return">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.return')</span>
                                </div>
                                <span class="time">15h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-foot-booking">
                        <h3 class="book-ttl">@lang('book.booking_options')</h3>
                        <table class="booking-table">
                            <tr>
                                <th>@lang('book.booking_site')</th>
                                <th>@lang('book.class')</th>
                                <th>@lang('book.total')</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/65x16" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="post-block post-modal-flight-block">
                    <div class="flight-general-block">
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl depart">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.depart')</span>
                                </div>
                                <span class="time">30h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl return">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.return')</span>
                                </div>
                                <span class="time">15h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-foot-booking">
                        <h3 class="book-ttl">@lang('book.booking_options')</h3>
                        <table class="booking-table">
                            <tr>
                                <th>@lang('book.booking_site')</th>
                                <th>@lang('book.class')</th>
                                <th>@lang('book.total')</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/65x16" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="post-block post-modal-flight-block">
                    <div class="flight-general-block">
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl depart">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.depart')</span>
                                </div>
                                <span class="time">30h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl return">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.return')</span>
                                </div>
                                <span class="time">15h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-foot-booking">
                        <h3 class="book-ttl">@lang('book.booking_options')</h3>
                        <table class="booking-table">
                            <tr>
                                <th>@lang('book.booking_site')</th>
                                <th>@lang('book.class')</th>
                                <th>@lang('book.total')</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/65x16" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="post-block post-modal-flight-block">
                    <div class="flight-general-block">
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl depart">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.depart')</span>
                                </div>
                                <span class="time">30h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flight-block">
                            <div class="flight-ttl-layer">
                                <div class="flight-ttl return">
                                    <i class="fa fa-plane"></i>
                                    <!-- <i class="trav-plane-icon"></i> -->
                                    <span class="ttl-name">@lang('book.return')</span>
                                </div>
                                <span class="time">15h 35m</span>
                            </div>
                            <div class="flight-inner">
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/90x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 13</b> from <b>9:40 pm</b> to <b>12:45 am</b></p>
                                                <p><span>Casablanca - New York</span></p>
                                                <p>Narrow-body jet · Embraer 170</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><span>Change planes in New York (JFK) - </span><span class="red">Long layover</span>
                                                </p>
                                            </div>
                                            <div class="flight-time">
                                                <b>21h 09m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flight-row">
                                    <div class="img-wrap">
                                        <img src="http://placehold.it/80x25" alt="">
                                    </div>
                                    <div class="flight-txt-block">
                                        <div class="txt-inner-row">
                                            <div class="flight-txt">
                                                <p><b>Sat, Jan 14</b> from <b>9:55 pm</b> to <b>11:12 am</b></p>
                                                <p><span>New York - Washington</span></p>
                                                <p>Air Canada 6 · Wide-body jet</p>
                                            </div>
                                            <div class="flight-time">
                                                <span>@lang('book.economy')</span>
                                                <b>8h 05m</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="post-foot-booking">
                        <h3 class="book-ttl">@lang('book.booking_options')</h3>
                        <table class="booking-table">
                            <tr>
                                <th>@lang('book.booking_site')</th>
                                <th>@lang('book.class')</th>
                                <th>@lang('book.total')</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/65x16" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="http://placehold.it/84x14" alt="" class="brand-name">
                                </td>
                                <td>
                                    <b>@lang('book.economy')</b>
                                </td>
                                <td>
                                    <b>$2545</b>
                                </td>
                                <td>
                                    <a class="btn btn-light-primary" href="#">@lang('book.book')</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script src="{{url('assets2/js/script.js')}}"></script>

</body>

</html>