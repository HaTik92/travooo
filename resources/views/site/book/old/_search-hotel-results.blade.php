<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('assets2/css/style.css')}}">
    <title>Travooo - flights, hotels</title>
</head>

<body>

<div class="main-wrapper">
    @include('site/layouts/header')

    <div class="content-wrap">
        <button class="btn btn-mobile-side left-outside-btn" id="filterToggler">
            <i class="trav-filter"></i>
        </button>

        <div class="container-fluid">

            <!-- left outside menu -->
            <div class="left-outside-menu-wrap" id="leftOutsideMenu">
                <ul class="left-outside-menu">
                    <li class="active"><a href="{{url('book/hotel')}}">
                            <i class="trav-home-icon"></i>
                            <span>@lang('navs.general.home')</span>
                        </a></li>
                </ul>
            </div>


            <br/>


            <div class="post-block post-flight-tab-block">
                <div class="post-tab-inner">
                    <div class="tab-left-side">
                        <div class="detail-list">
                            <div class="detail-block current">
                                <b>Recommendation</b>
                                <i class="fa fa-caret-down"></i>
                            </div>
                            <div class="detail-block">
                                <b>Stars</b>
                                <i class="fa fa-caret-down"></i>
                            </div>
                            <div class="detail-block">
                                <b>Price</b>
                                <i class="fa fa-caret-down"></i>
                            </div>
                        </div>
                    </div>
                    <div class="tab-right-side">
                        <div class="post-tab-filter">
                            <div class="dropdown">
                                <a class="dropdown-toggle filter-toggler" role="button" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <i class="trav-filter-icon"></i>
                                    <span>Filter</span>
                                </a>

                                <div class="dropdown-menu filter-dropdown-menu dropdown-menu-right">
                                    <div class="select-block">
                                        <select name="" id="" class="custom-select">
                                            <option value="1">Recommended</option>
                                            <option value="2">item</option>
                                        </select>
                                    </div>
                                    <div class="filter-inner-block">
                                        <h3 class="block-ttl">Stars</h3>
                                        <ul class="filter-star-list">
                                            <li class="active">
                                                <i class="trav-empty-star-icon"></i>
                                                <span class="count">1</span>
                                            </li>
                                            <li>
                                                <i class="trav-empty-star-icon"></i>
                                                <span class="count">2</span>
                                            </li>
                                            <li>
                                                <i class="trav-empty-star-icon"></i>
                                                <span class="count">3</span>
                                            </li>
                                            <li>
                                                <i class="trav-empty-star-icon"></i>
                                                <span class="count">4</span>
                                            </li>
                                            <li>
                                                <i class="trav-empty-star-icon"></i>
                                                <span class="count">5</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="filter-inner-block">
                                        <h3 class="block-ttl">Review score</h3>
                                        <ul class="filter-score-list">
                                            <li class="active"><span>0+</span></li>
                                            <li class="active"><span>2+</span></li>
                                            <li><span>4+</span></li>
                                            <li><span>6+</span></li>
                                            <li><span>8+</span></li>
                                        </ul>
                                    </div>
                                    <div class="filter-inner-block">
                                        <button>Filter</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="custom-row">
                <!-- MAIN-CONTENT -->
                <div class="main-content-layer">

                    @foreach($hotels AS $hotel)

                        <div class="post-block post-flight-depart">
                            <div class="post-content-inner">
                                <div class="flight-wrap">
                                    <div class="flight-row-wrap">
                                        <div class="flight-row">
                                            <div class="img-wrap">
                                                <img src="{{$hotel->images[0]->url}}" alt="{{$hotel->name}}"
                                                     style="height:140px;width:140px">
                                            </div>
                                            <div class="flight-content">
                                                <h3 class="content-ttl">{{$hotel->name}}</h3>
                                                <div class="com-star-block">
                                                    <ul class="com-star-list">
                                                        @for($i=1;$i<=$hotel->star;$i++)
                                                            <li><i class="trav-star-icon"></i></li>
                                                        @endfor

                                                    </ul>
                                                </div>
                                                <div class="flight-content-inner">
                                            <span>
                                      @foreach($hotel->badges AS $badge)
                                                    {{$badge->text}}
                                                    @if(!$loop->last), @endif
                                                @endforeach
                                              </span>
                                                </div>
                                                <div class="flight-inner">
                                                    <div class="flight-txt bordered">
                                                        <div class="depart-col">
                                                            <div class="depart-inner">
                                                                @foreach($hotel->reviews AS $review)
                                                                    @if($review->reviewerGroup=="ALL")
                                                                        <span class="label">{{$review->score/10}}</span>
                                                                        <p><b>Excellent</b></p>
                                                                        <p>{{number_format($review->count)}} @lang('place.reviews')</p>
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <div class="depart-col">
                                                            <!--
                                                          <div class="depart-inner">
                                                            <p><b>Location</b></p>
                                                            <p>New York City</p>
                                                          </div>
                                                            -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flight-price-inner" id="rate-{{$hotel->id}}">


                                        <div class="flight-price">
                                            ${{@round($rates[$hotel->id][0]->price->totalAmountUsd)}}</div>
                                        <a class="hotel-link">
                                            <span>{{@$rates[$hotel->id][0]->providerCode}}</span>
                                        </a>
                                        <button type="button" class="btn btn-light-bg-grey btn-bordered"
                                                onclick='window.open("{{@$rates[$hotel->id][0]->handoffUrl}}", "_blank")'>
                                            @lang('book.view_deal')
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>

                <!-- SIDEBAR -->
                <div class="sidebar-layer" id="sidebarLayer">
                    <aside class="sidebar">

                        <div class="post-block post-flight-info-block">
                            <div class="img-wrap">
                                <img src="https://s3.amazonaws.com/travooo-images2/{{@$city[0]->getMedias[0]->url}}"
                                     alt="" style="width:383px;height:205px;">
                            </div>
                            <div class="flight-info-block">
                                <div class="flight-info-content">
                                    <div class="flight-info-row">
                                        <div class="flight-inner">
                                            <h4 class="ttl">{{$city_name}}</h4>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="post-block post-flight-info-block">

                            <div class="flight-info-block">
                                <div class="flight-info-content">
                                    <div class="flight-info-row">
                                        <div class="flight-inner">

                                            <div class="city-inner" id="nowInPlace">
                                                <ul class="city-avatar-list">
                                                    <li><a href="#" data-toggle="modal" data-target="#goingPopup"><img
                                                                    class="ava"
                                                                    src="https://www.travooo.com/assets2/image/photos/users/46.jpg"
                                                                    style="width:60px;height:60px;" alt="Malin Åkerman"
                                                                    title="Malin Åkerman"></a></li>
                                                    <li><a href="#" data-toggle="modal" data-target="#goingPopup"><img
                                                                    class="ava"
                                                                    src="https://www.travooo.com/assets2/image/photos/users/44.jpg"
                                                                    style="width:60px;height:60px;" alt="Tyler Joseph"
                                                                    title="Tyler Joseph"></a></li>
                                                    <li><a href="#" data-toggle="modal" data-target="#goingPopup"><img
                                                                    class="ava"
                                                                    src="https://www.travooo.com/assets2/image/photos/users/26.jpeg"
                                                                    style="width:60px;height:60px;" alt="Shazaib Irfan"
                                                                    title="Shazaib Irfan"></a></li>
                                                </ul>
                                                <div class="city-txt"><p><a href="#" class="link" data-toggle="modal"
                                                                            data-target="#goingPopup">Malin Åkerman</a>,
                                                        <a href="#" class="link" data-toggle="modal"
                                                           data-target="#goingPopup">Tyler Joseph</a>, <a href="#"
                                                                                                          class="link"
                                                                                                          data-toggle="modal"
                                                                                                          data-target="#goingPopup">Shazaib
                                                            Irfan</a>, are now in
                                                        <a href="https://www.travooo.com/place/475323">Hotel Anteroom
                                                            Kyoto</a>
                                                    </p></div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="aside-footer">
                            <ul class="aside-foot-menu">
                                <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
                                <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
                                <li><a href="{{url('/')}}">Advertising</a></li>
                                <li><a href="{{url('/')}}">Cookies</a></li>
                                <li><a href="{{url('/')}}">More</a></li>
                            </ul>
                            <p class="copyright">Travooo &copy; 2017</p>
                        </div>
                    </aside>
                </div>

            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.6/js/lightgallery-all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script src="{{url('assets2/js/script.js')}}"></script>

</body>

</html>