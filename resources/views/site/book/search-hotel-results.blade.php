@php
    $title = 'Travooo - flights, hotels';
@endphp

@extends('site.book.template.book')

@section('before-content')
    <div class="post-block post-flight-tab-block">
        <div class="post-tab-inner">
            <div class="tab-left-side">
                <div class="detail-list">
                    <div class="detail-block current">
                        <b>@lang('book.recommendation')</b>
                        <i class="fa fa-caret-down"></i>
                    </div>
                    <div class="detail-block">
                        <b>@lang('book.stars')</b>
                        <i class="fa fa-caret-down"></i>
                    </div>
                    <div class="detail-block">
                        <b>@lang('book.price')</b>
                        <i class="fa fa-caret-down"></i>
                    </div>
                </div>
            </div>
            <div class="tab-right-side">
                <div class="post-tab-filter">
                    <div class="dropdown">
                        <a class="dropdown-toggle filter-toggler" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <i class="trav-filter-icon"></i>
                            <span>@lang('other.filter')</span>
                        </a>

                        <div class="dropdown-menu filter-dropdown-menu dropdown-menu-right">
                            <div class="select-block">
                                <select name="" id="" class="custom-select">
                                    <option value="1">@lang('book.recommended')</option>
                                    <option value="2">@lang('book.item')</option>
                                </select>
                            </div>
                            <div class="filter-inner-block">
                                <h3 class="block-ttl">@lang('book.stars')</h3>
                                <ul class="filter-star-list">
                                    <li class="active">
                                        <i class="trav-empty-star-icon"></i>
                                        <span class="count">1</span>
                                    </li>
                                    <li>
                                        <i class="trav-empty-star-icon"></i>
                                        <span class="count">2</span>
                                    </li>
                                    <li>
                                        <i class="trav-empty-star-icon"></i>
                                        <span class="count">3</span>
                                    </li>
                                    <li>
                                        <i class="trav-empty-star-icon"></i>
                                        <span class="count">4</span>
                                    </li>
                                    <li>
                                        <i class="trav-empty-star-icon"></i>
                                        <span class="count">5</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="filter-inner-block">
                                <h3 class="block-ttl">@lang('book.review_score')</h3>
                                <ul class="filter-score-list">
                                    <li class="active"><span>0+</span></li>
                                    <li class="active"><span>2+</span></li>
                                    <li><span>4+</span></li>
                                    <li><span>6+</span></li>
                                    <li><span>8+</span></li>
                                </ul>
                            </div>
                            <div class="filter-inner-block">
                                <button>@lang('other.filter')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!-- MAIN-CONTENT -->
    <div class="main-content-layer">
        @if(isset($hotels) && $hotels)
        @foreach($hotels AS $hotel)
        @if(isset($rates[$hotel->id]) && !is_null($rates[$hotel->id]))
            <div class="post-block post-flight-depart">
                <div class="post-content-inner">
                    <div class="flight-wrap">
                        <div class="flight-row-wrap">
                            <div class="flight-row">
                                <div class="img-wrap">
                                    <img src="{{$hotel->images[0]->url}}" alt="{{$hotel->name}}"
                                         style="height:140px;width:140px">
                                </div>
                                <div class="flight-content">
                                    <h3 class="content-ttl">{{$hotel->name}}</h3>
                                    <div class="com-star-block">
                                        <ul class="com-star-list">
                                            @for($i=1;$i<=$hotel->star;$i++)
                                                <li><i class="trav-star-icon"></i></li>
                                            @endfor

                                        </ul>
                                    </div>
                                    <div class="flight-content-inner">
                                            <span>
                                      @foreach($hotel->badges AS $badge)
                                                    {{$badge->text}}
                                                    @if(!$loop->last), @endif
                                                @endforeach
                                              </span>
                                    </div>
                                    <div class="flight-inner">
                                        <div class="flight-txt bordered">
                                            <div class="depart-col">
                                                <div class="depart-inner">
                                                    @foreach($hotel->reviews AS $review)
                                                        @if($review->reviewerGroup=="ALL")
                                                            <span class="label">{{$review->score/10}}</span>
                                                            <p><b>Excellent</b></p>
                                                            <p>{{number_format($review->count)}} reviews</p>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="depart-col">
                                                <!--
                                              <div class="depart-inner">
                                                <p><b>Location</b></p>
                                                <p>New York City</p>
                                              </div>
                                                -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flight-price-inner" id="rate-{{$hotel->id}}">

                            <div class="flight-price">
                                
                                ${{@round($rates[$hotel->id][0]->price->totalAmountUsd)}}</div>
                            <a class="hotel-link">
                                <span>{{@$rates[$hotel->id][0]->providerCode}}</span>
                            </a>
                            <button type="button" class="btn btn-light-bg-grey btn-bordered"
                                    onclick='window.open("{{@$rates[$hotel->id][0]->handoffUrl}}", "_blank")'>
                                @lang('book.view_deal')
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @endforeach
        @endif

    </div>

    <!-- SIDEBAR -->
    <div class="sidebar-layer" id="sidebarLayer">
        <aside class="sidebar">

            <div class="post-block post-flight-info-block">
                <div class="img-wrap">
                    <img src="https://s3.amazonaws.com/travooo-images2/{{@$hotel_city[0]->getMedias[0]->url}}"
                         alt="" style="width:383px;height:205px;">
                </div>
                <div class="flight-info-block">
                    <div class="flight-info-content">
                        <div class="flight-info-row">
                            <div class="flight-inner">
                                <h4 class="ttl">@if(isset($city_name)) {{$city_name}} @endif</h4>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="post-block post-flight-info-block">

                <div class="flight-info-block">
                    <div class="flight-info-content">
                        <div class="flight-info-row">
                            <div class="flight-inner">

                                <div class="city-inner" id="nowInPlace">
                                    <ul class="city-avatar-list">
                                        <li><a href="#" data-toggle="modal" data-target="#goingPopup"><img
                                                        class="ava"
                                                        src="https://www.travooo.com/assets2/image/photos/users/46.jpg"
                                                        style="width:60px;height:60px;" alt="Malin Åkerman"
                                                        title="Malin Åkerman"></a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#goingPopup"><img
                                                        class="ava"
                                                        src="https://www.travooo.com/assets2/image/photos/users/44.jpg"
                                                        style="width:60px;height:60px;" alt="Tyler Joseph"
                                                        title="Tyler Joseph"></a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#goingPopup"><img
                                                        class="ava"
                                                        src="https://www.travooo.com/assets2/image/photos/users/26.jpeg"
                                                        style="width:60px;height:60px;" alt="Shazaib Irfan"
                                                        title="Shazaib Irfan"></a></li>
                                    </ul>
                                    <div class="city-txt"><p><a href="#" class="link" data-toggle="modal"
                                                                data-target="#goingPopup">Malin Åkerman</a>,
                                            <a href="#" class="link" data-toggle="modal"
                                               data-target="#goingPopup">Tyler Joseph</a>, <a href="#"
                                                                                              class="link"
                                                                                              data-toggle="modal"
                                                                                              data-target="#goingPopup">Shazaib
                                                Irfan</a>, @lang('book.are_now_in')
                                            <a href="https://www.travooo.com/place/475323">Hotel Anteroom
                                                Kyoto</a>
                                        </p></div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>

            @include('site.layouts._footer-scripts')
        </aside>
    </div>
@endsection
