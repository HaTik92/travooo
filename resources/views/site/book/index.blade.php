@php
    $title = 'Travooo - flights, hotels';
@endphp

@extends('site.book.template.book')

@section('before_styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection


@section('before-content')
    <div class="top-banner-wrap">
        <div class="top-banner-block after-disabled book-top-banner-block"
             style="background-image: url({{url('assets2/image/flight-hotel-bg.jpg')}}">
            <div class="top-banner-flight-hotel">
                <ul class="flight-hotel-tabs" role="tablist">
                    <li>
                        <a class="current" href="{{route('book.hotel')}}">
                                            <span class="circle-icon">
                                                <i class="trav-building-icon"></i>
                                            </span>
                            <span>@lang('book.hotels')</span>
                        </a>
                    </li>
                    <li>
                        <a class="" href="{{route('book.flight')}}">
                                            <span class="circle-icon">
                                                <i class="trav-angle-plane-icon"></i>
                                            </span>
                            <span>@lang('book.flights')</span>
                        </a>
                    </li>
                </ul>
                <div class="top-banner-inner">

                    <h2 class="ban-ttl">@lang('book.find_the_best_hotels_offers')s</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, distinctio!</p>
                </div>
                <form method="post" action="{{route('book.search_hotel')}}" class="top-banner-search-form">
                    <div class="flex-row">
                        <div class="flex-item fl-30 with-icon">
                                            <span class="icon-wrap">
                                                <i class="trav-location"></i>
                                            </span>
                            <select class="flex-input" name="city_select" id="city_select"
                                    placeholder="@lang('book.your_destination_city')">
                            </select>
                        </div>
                        <div class="flex-item fl-30 with-icon">
                                            <span class="icon-wrap">
                                                <i class="trav-event-icon"></i>
                                            </span>
                            <input class="flex-input" name="daterange" id="daterange"
                                   placeholder="@lang('time.date')"
                                   value="" type="text">

                        </div>
                        <div class="flex-item fl-30">
                            <select name="" id="" class="custom-select">
                                <option value="1">1 @lang('book.adult'), @lang('book.economy')</option>
                                <option value="2">@lang('book.item')</option>
                            </select>
                        </div>
                        <div class="flex-item fl-10">
                            <button class="btn btn-light-primary btn-bordered">@lang('buttons.general.search')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--
                        <div class="post-block post-flight-style">
                            <div class="post-side-top">
                                <h3 class="side-ttl"><i class="trav-trending-destination-icon"></i> Trending flights
                                </h3>
                                <div class="side-right-control">
                                    <a href="#" class="slide-link slide-prev"><i class="trav-angle-left"></i></a>
                                    <a href="#" class="slide-link slide-next"><i class="trav-angle-right"></i></a>
                                </div>
                            </div>
                            <div class="post-side-inner">
                                <div class="post-slide-wrap">
                                    <ul id="trendingFlights" class="post-slider">
                                        <li>
                                            <div class="post-flight-inner">
                                                <img src="http://placehold.it/327x180" alt="">
                                                <div class="post-flight-info">
                                                    <div class="flight-inner">
                                                        <div class="fl-left">
                                                            <p class="city-name">Barcelona</p>
                                                            <p class="country-name">Spain</p>
                                                            <p class="going-count">
                                                                <span class="count">52</span>
                                                                <span>going today</span>
                                                            </p>
                                                        </div>
                                                        <div class="fl-right">
                                                            <div class="flag-wrap">
                                                                <img class="flag-image" src="http://placehold.it/32x22?text=mini+map" alt="">
                                                            </div>
                                                            <div class="btn-wrap">
                                                                <button class="btn btn-light-primary btn-bordered">Book Flight</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="http://placehold.it/327x180?text=direction+map" alt="">
                                                <div class="post-direct-info">
                                                    <div class="direct-block">
                                                        <p class="dir-label">3h 45m</p>
                                                        <p class="dir-txt">@lang('book.direct_flight')</p>
                                                    </div>
                                                    <div class="direct-block">
                                                        <p class="dir-label">3120 km</p>
                                                        <p class="dir-txt">@lang('book.total_distance')</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="post-flight-inner">
                                                <img src="http://placehold.it/327x180" alt="">
                                                <div class="post-flight-info">
                                                    <div class="flight-inner">
                                                        <div class="fl-left">
                                                            <p class="city-name">Amalfi</p>
                                                            <p class="country-name">Italy</p>
                                                            <p class="going-count">
                                                                <span class="count">52</span>
                                                                <span>going today</span>
                                                            </p>
                                                        </div>
                                                        <div class="fl-right">
                                                            <div class="flag-wrap">
                                                                <img class="flag-image" src="http://placehold.it/32x22?text=mini+map" alt="">
                                                            </div>
                                                            <div class="btn-wrap">
                                                                <button class="btn btn-light-primary btn-bordered">Book Flight</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="http://placehold.it/327x180?text=direction+map" alt="">
                                                <div class="post-direct-info">
                                                    <div class="direct-block">
                                                        <p class="dir-label">3h 45m</p>
                                                        <p class="dir-txt">@lang('book.direct_flight')</p>
                                                    </div>
                                                    <div class="direct-block">
                                                        <p class="dir-label">3120 km</p>
                                                        <p class="dir-txt">@lang('book.total_distance')</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="post-flight-inner">
                                                <img src="http://placehold.it/327x180" alt="">
                                                <div class="post-flight-info">
                                                    <div class="flight-inner">
                                                        <div class="fl-left">
                                                            <p class="city-name">Tokyo</p>
                                                            <p class="country-name">Japan</p>
                                                            <p class="going-count">
                                                                <span class="count">52</span>
                                                                <span>going today</span>
                                                            </p>
                                                        </div>
                                                        <div class="fl-right">
                                                            <div class="flag-wrap">
                                                                <img class="flag-image" src="http://placehold.it/32x22?text=mini+map" alt="">
                                                            </div>
                                                            <div class="btn-wrap">
                                                                <button class="btn btn-light-primary btn-bordered">Book Flight</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <img src="http://placehold.it/327x180?text=direction+map" alt="">
                                                <div class="post-direct-info">
                                                    <div class="direct-block">
                                                        <p class="dir-label">3h 45m</p>
                                                        <p class="dir-txt">@lang('book.direct_flight')</p>
                                                    </div>
                                                    <div class="direct-block">
                                                        <p class="dir-label">3120 km</p>
                                                        <p class="dir-txt">@lang('book.total_distance')</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
    -->

    @include('site/book/partials/hotels-by-city')
@endsection

@section('before_site_script')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link href="{{url('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
    <script src="{{url('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
@endsection

@section('after_scripts')
    <script>
        $(function () {
            $('#daterange').daterangepicker({
                opens: 'left'
            }, function (start, end, label) {
                console.log("@lang('book.a_new_date_selection_was_made'): " + start.format('YYYY-MM-DD') + ' @lang('chat.to') ' + end.format('YYYY-MM-DD'));
            });
        });
        $('#city_select').select2({
            placeholder: '@lang('book.please_enter_your_destination_city')',
            debug: true,

            ajax: {
                url: '{{route('book.ajax_search_city')}}',
                dataType: 'json',

                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    </script>
@endsection