<div class="left-outside-menu-wrap" id="leftOutsideMenu">
    <ul class="left-outside-menu">
        <li class="active">
            <a href="{{ route('book.hotel')}}">
                <i class="trav-home-icon"></i>
                <span>@lang('navs.general.home')</span>
            </a>
        </li>
    </ul>
</div>