@extends('site.layouts.site')

@section('content-area')
@section('left_menu')
   @include('site/layouts/_left-menu')
@show

    @yield('before-content')

    <div class="custom-row">
        @yield('content')
    </div>

@endsection

@section('before_site_script')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{asset('assets2/js/select2-4.0.5/dist/css/select2.min.css')}}" rel="stylesheet"/>
    <script src="{{asset('assets2/js/select2-4.0.5/dist/js/select2.full.min.js')}}"></script>
@endsection