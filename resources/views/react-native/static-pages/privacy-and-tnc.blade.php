@php
    use Carbon\Carbon;
    use App\Models\CommonPage\CommonPage;
    $title = $page->trans[0]->title;
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <style>
        .peta-header{
            border-bottom: 1px solid #e6e6e6;
            margin-top: 5px;
            margin-bottom: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .peta-header span{
            font-style: italic;
            color: #4d4d4d;
            font-size: 16px;
        }
    </style>
</head>
<body>
    @if ($page->slug == CommonPage::SLUG_PRIVACY_POLICY || $page->slug == CommonPage::SLUG_TERMS_OF_SERVICE)
        <div class="peta-header">
            <span>Last updated {{ Carbon::parse($page->updated_at)->format("F d, Y")}}</span>
        </div>
    @endif
    {!!$page->trans[0]->description!!}
</body>
</html>