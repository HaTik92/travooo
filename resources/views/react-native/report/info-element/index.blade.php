<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
    <style>
        .content.w-632{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .trav-currency-line-block {
            padding: 18px 20px;
            background: #fcfcfc;
            border-radius: 10px;
            border: 1px solid #f3f3f3;
        }
        .trev-info-name {
            line-height: 1.4;
            margin-bottom: 5px;
            font-family: 'Circular Std Book';
            border-bottom: 1px solid #e6e6e6;
            padding-bottom: 5px;
            font-size: 18px;
            color: #1a1a1a;
        }
        .trev-info-desc {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            flex-direction: column;
            line-height: 1.4;
            color: #2f2f2f;
            font-family: 'CircularAirPro Light';
            font-size: 17px;
        }
        .travlog-info-block .subtitle .sub-list, .trev-info-desc .sub-list {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            justify-content: space-between;
            padding: 7px;
            border-radius: 3px;
        }
        .travlog-info-block .subtitle .sub-list:nth-child(2n), 
        .trev-info-desc .sub-list:nth-child(2n) {
            background: #f7f7f7;
        }

        .travlog-info-block .subtitle .sub-list, .trev-info-desc .sub-list {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            justify-content: space-between;
            padding: 7px;
            border-radius: 3px;
        }
    </style>
</head>
<body>
    @include('react-native.report.info-element.partials.item',['item'=> $item])
</body>
</html>