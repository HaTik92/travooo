@if ($item['with_key_value'])
    <div class="content w-632" dir="auto">
        <div class="trav-currency-line-block" dir="auto">
            <div class="block-name trev-info-name" id="infoBlockName" dir="auto">{{$item['title']}}</div>
            <div class="trev-info-desc" id="infoBlockValue" dir="auto">
                @include('react-native.report.info-element.partials.sub-item',[
                    'item' => $item
                ])
            </div>
        </div>
    </div>
@else
    <div class="content w-632" dir="auto">
        <div class="trav-currency-line-block" dir="auto">
            <div class="block-name trev-info-name" id="infoBlockName" dir="auto">{{$item['title']}}</div>
            <div class="trev-info-desc" id="infoBlockValue" dir="auto">
                @include('react-native.report.info-element.partials.sub-item',[
                    'item' => $item
                ])
            </div>
        </div>
    </div> 
@endif