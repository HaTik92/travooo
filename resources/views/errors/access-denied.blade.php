@php
$title = 'Travooo - Access Denied';
@endphp

@extends('site.layouts.site')

@section('base')
    @if(Auth::check())
        @include('site.layouts._header')
    @else
        @include('site.layouts._non-loggedin-user-header')
    @endif
<div class="content-wrap access-deined-content-wrap">
    <div class="container-fluid">
        <div class="access-deined-content">
            <div class="access-deined-title-info">
                <img src="{{asset('assets2/image/access-deinied-icon.png')}}">
                <div class="access-denied-info-wrap">
                    <p class="acc-denied-title">Access denied</p>
                    @if(Auth::check())
                        <a href="{{url('/')}}" class="acc-denied-link"><i class="trav-long-arrow-left" ></i> Return home</a>
                    @endif
                </div>
            </div>
            @if(count($user_lists) > 3)
            <div class="access-denied-slide-content">
                <div class="acc-d-slide-title-wrap">
                    <p class="acc-d-slide-title">Other people you might like...</p>
                    <p class="acc-d-slide-subtitle">In the meantime, you can check out profiles of people that you might want to follow.</p>
                </div>
                <div class="u-list-slide-content">
                    <ul class="user-list-slider" id="user_list_slider">
                        @foreach($user_lists as $user_list)
                        @php
                        $exp_list = [];
                        if($user_list->expert == 1){
                            $exp_cities = $user_list->expertCities;
                            $exp_countries = $user_list->expertCountries;
                            $exp_list =  $exp_cities->toBase()->merge($exp_countries); 
                        }
                        @endphp
                        @if(!user_access_denied($user_list->id))
                        <li class="u-list-item">
                            <a href="{{url('profile', $user_list->id)}}">
                             @if(isset($user_list->cover_photo))
                             <img src="{{$user_list->cover_photo}}" class="cover-img" width="100%" alt="">
                                @else
                                <img src="{{asset('assets2/image/placeholders/pattern.png')}}" class="cover-img" width="100%" alt="">
                                @endif
                            <div class="user-info-block">
                                <img src="{{check_profile_picture($user_list->profile_picture)}}" class="user-image">
                                <div class="acc-user-info-wrap">
                                    <p class="acc-user-info-title">{{$user_list->name}}  {!! get_exp_icon($user_list) !!}</p>
                                    @if(count($exp_list) > 0)
                                    
                                    <p class="acc-user-info-exp">Expert in:
                                     @foreach($exp_list as $k=>$exp)
                                        @if($k<2)
                                            {{ (get_class($exp) === App\Models\ExpertsCities\ExpertsCities::class)?@$exp->cities->transsingle->title:@$exp->countries->transsingle->title}}
                                        @endif
                                        {{($k < 1 && $k+1 < count($exp_list))?',':''}}
                                        {{($k == 2)?'...':''}}
                                    @endforeach

                                    </p>
                                    @endif
                                </div>
                            </div>
                            </a>
                        </li>
                        @endif
                        @endforeach
                    </ul> 
                </div>
            </div>
            @endif
        </div>
        <div class="setting-footer-block pt-20">
            <p class="setting-copyright">Travooo © {{date('Y')}}</p>
        </div>
    </div>

</div>
    @if(!Auth::check())
        @include('errors.partials._footer')
    @endif
@endsection
@section('after_scripts')
<script type="text/javascript">
$(document).ready(function(){
       
    $("#user_list_slider").lightSlider({
        autoWidth:true,
        pager: false,
        enableDrag: true,
        slideMargin: 25,
        prevHtml: '<i class="trav-angle-left"></i>',
        nextHtml: '<i class="trav-angle-right"></i>',
        addClass: 'acc-user-slider-wrap',
        onBeforeStart: function (el) {
            el.removeAttr("style")
        },
        responsive : [
            {
                breakpoint:767,
                settings: {
                    slideMargin:9,
                }
            },
        ],
    })

})
</script>
@endsection


