<!DOCTYPE html>
<html>
<head>
    <title>{{ __('We’ll be back soon...')}}</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('assets2/css/style-15102019-9.css?v='.time())}}">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            background-color: #ffffff;
        }


        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        @media (min-width: 1130px) {
            .content {
                width: 1100px;
            }
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .error-code{
            font-size: 225.5px;
            font-weight: 900;
            font-stretch: normal;
            font-style: normal;
            line-height: 0.37;
            letter-spacing: normal;
            text-align: left;
            color: #1a1a1a;
            font-family: 'Circular Std Book',-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
        }

        .content-block{
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            margin-bottom: 30px;
        }

        .title {
            font-size: 82.5px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.02;
            letter-spacing: normal;
            text-align: left;
            color: #b0b0b0;
            font-family: 'Circular Std Book',-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
            padding-left: 74px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="content-block"><span class="error-code">500</span>  <span class="title">{{ __('We’ll be back soon...')}}</span></div>
    </div>
</div>
</body>
</html>
