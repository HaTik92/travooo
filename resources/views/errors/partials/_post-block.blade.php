@foreach($content_list as $content)
    @php

        if(!$content->author){
            continue;
        }
        $post_object = $content->medias[0]->media;

        $file_url = $post_object->url;
        $file_url_array = explode(".", $file_url);
        $ext = end($file_url_array);
        $allowed_video = array('mp4');

        $url =  @$post_object->url;

    @endphp
    <li class="p-list-item">
        <a href="{{ url('post/'. $content->author->username, $content->id)}}" target="_blank">
            @if(in_array($ext, $allowed_video))
                @php $subtitle = 'Video'; @endphp
                <video width="100%" height="320px" class="fullwidth" id="{{'video_tmp'.$post_object->id}}" data-play={{"play_".$post_object->id}}>
                    <source src="{{$url}}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
                <span  class="p-play-btn play-btn" dir="auto"><i class="fa fa-play" aria-hidden="true" dir="auto"></i></span>
            @else
                @php $subtitle = 'Photo'; @endphp
                <img src="{{$url}}" alt="" class="cover-img">
            @endif
            <div class="p-title-block">
                <span class="p-content-subtitle">{{$subtitle}}</span>
                <span class="p-content-title">{{$content->author->name}}</span>
            </div>
            <div class="plan-info-block">
                <div class="p-interation-block">
                    <span class="plan_button" dir="auto"><i class="trav-heart-fill-icon" dir="auto"></i></span>
                    <b>{{count($content->likes)}}</b>
                </div>
                <div class="p-interation-block">
                    <span class="plan_button" dir="auto"><i class="trav-comment-icon" dir="auto"></i></span>
                    <b>{{count($content->comments)}}</b>
                </div>
                <div class="p-interation-block  ml-auto">
                    <span class="plan_button" dir="auto"><i class="fa fa-share-alt" aria-hidden="true"></i></span>
                    <b>{{count($content->shares)}}</b>
                </div>
            </div>
        </a>
    </li>
@endforeach
