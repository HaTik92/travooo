@foreach($content_list as $report)
    @if(!user_access_denied($report->author->id, true))
        <li class="r-list-item">
            <a href="{{url('reports', $report->id)}}">
                @if(isset($report->cover[0]))
                    <img src="{{$report->cover[0]->val}}" class="cover-img" width="100%" alt="">
                @else
                    <img src="{{asset('assets2/image/placeholders/pattern.png')}}" class="cover-img" width="100%" alt="">
                @endif
                <div class="r-info-wrapper">
                    <div class="r-country-name">
                        @php $num_output = 0; @endphp
                        @foreach(get_report_locations_info($report->id, 'publish') as $locations)
                            {{$locations['title']}} @if(!$loop->last && $num_output != 1), @endif
                            @php $num_output ++;@endphp
                        @endforeach
                    </div>
                    <div class="r-info-title">
                        {{$report->title}}
                    </div>
                    <div class="r-info-block">
                        <img src="{{check_profile_picture($report->author->profile_picture)}}" class="user-image">
                        <div class="acc-user-info-wrap">
                            <p class="acc-user-info-title">{{$report->author->name}}  {!! get_exp_icon($report->author) !!}</p>
                            <p class="acc-user-info-exp">
                                {!! moreTravlogDateFormat($report->created_at)!!}
                            </p>
                        </div>
                    </div>
                </div>
            </a>
        </li>
    @endif
@endforeach
