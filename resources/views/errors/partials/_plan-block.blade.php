@foreach($content_list as $content)
    @if(!user_access_denied($content->author->id, true))
        <li class="p-list-item">
            <a href="{{ url('trip/plan', $content->id)}}" target="_blank">
                <img src="{{get_plan_map($content, 270, 320)}}" alt="map-image"
                     class="cover-img">
                <div class="p-title-block">
                    <span class="p-content-title">{{$content->title}}</span>
                </div>
                <div class="plan-info-block">
                    <div class="p-interation-block">
                        <span class="plan_button" dir="auto"><i class="trav-heart-fill-icon" dir="auto"></i></span>
                        <b>{{count($content->likes)}}</b>
                    </div>
                    <div class="p-interation-block">
                        <span class="plan_button" dir="auto"><i class="trav-comment-icon" dir="auto"></i></span>
                        <b>{{count($content->comments)}}</b>
                    </div>
                    <div class="p-interation-block  ml-auto">
                        <span class="plan_button" dir="auto"><i class="fa fa-share-alt" aria-hidden="true"></i></span>
                        <b>{{count($content->shares)}}</b>
                    </div>
                </div>
            </a>
        </li>
        @endif
@endforeach
