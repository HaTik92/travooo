@php
$title = 'Travooo - Sorry! Content has been deleted.';
@endphp

@extends('site.layouts.site')

@section('base')
    @if(Auth::check())
        @include('site.layouts._header')
    @else
        @include('site.layouts._non-loggedin-user-header')
    @endif
<div class="content-wrap access-deined-content-wrap">
    <div class="container-fluid">
        <div class="access-deined-content">
            <div class="perm-title-info">
                    <p class="perm-denied-title">Sorry! Content has been deleted.</p>
                    <p class="perm-denied-subtitle">The content you’re looking for has been deleted.</p>
            </div>
            @if(count($content_list) >= 3)
            <div class="access-denied-slide-content">
                <div class="u-list-slide-content">
                    <ul class="plan-list-slider" id="plan_report_list_slider">
                        @if($type == 'plan')
                            @include('errors.partials._plan-block', ['content_list' => $content_list])
                        @elseif($type == 'report')
                            @include('errors.partials._report-block', ['content_list' => $content_list])
                        @else
                            @include('errors.partials._post-block', ['content_list' => $content_list])
                        @endif
                    </ul> 
                </div>
            </div>
            @endif
        </div>
        <div class="setting-footer-block pt-20" dir="auto">
            <div class="p-footer-link-block">
                <a href="javascript:history.back()" class="p-footer-link">Go back to the <b>previous page</b></a>
                <span class="p-footer-dot" dir="auto">•</span>
                <a href="{{url('/')}}" class="p-footer-link">Go to <b>News Feed</b></a>
                <span class="p-footer-dot" dir="auto">•</span>
                <a href="{{url('help')}}" class="p-footer-link">Visit our <b>Helper Center</b></a>
            </div>
            <ul class="setting-footer-list pb-2" dir="auto">
                <li dir="auto"><a href="{{url('/')}}" dir="auto">About</a></li>
                <li dir="auto"><a href="{{url('/')}}" dir="auto">Careers</a></li>
                <li dir="auto"><a href="{{url('/')}}" dir="auto">Sitemap</a></li>
                <li dir="auto"><a href="{{route('page.privacy_policy')}}" dir="auto">Privacy</a></li>
                <li dir="auto"><a href="{{route('page.terms_of_service')}}" dir="auto">Terms</a></li>
                <li dir="auto"><a href="{{url('/')}}" dir="auto">Contact</a></li>
                <li dir="auto"><a href="{{route('help.index')}}" dir="auto">Help Center</a></li>
            </ul>
            <p class="setting-copyright" dir="auto">Travooo © 2021</p>
        </div>
    </div>

</div>
    @if(!Auth::check())
        @include('errors.partials._footer')
    @endif
@endsection
@section('after_scripts')
<script type="text/javascript">
$(document).ready(function(){
       
    $("#plan_report_list_slider").lightSlider({
        autoWidth:true,
        pager: false,
        enableDrag: true,
        slideMargin: 35,
        prevHtml: '<i class="trav-angle-left"></i>',
        nextHtml: '<i class="trav-angle-right"></i>',
        addClass: 'acc-user-slider-wrap',
        onBeforeStart: function (el) {
            el.removeAttr("style")
        },
        responsive : [
            {
                breakpoint:767,
                settings: {
                    slideMargin:9,
                }
            },
        ],
    })

})
</script>
@endsection


