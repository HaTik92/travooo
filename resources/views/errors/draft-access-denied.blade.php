@php
$title = 'Travooo - Access Denied';
@endphp

@extends('site.layouts.site')

@section('base')
    @if(Auth::check())
        @include('site.layouts._header')
    @else
        @include('site.layouts._non-loggedin-user-header')
    @endif
<div class="content-wrap access-deined-content-wrap">
    <div class="container-fluid">
        <div class="access-deined-content">
            <div class="access-deined-title-info">
                <img src="{{asset('assets2/image/access-deinied-icon.png')}}">
                <div class="access-denied-info-wrap">
                    <p class="acc-denied-title">Access denied</p>
                    @if(Auth::check())
                        <a href="{{url('/')}}" class="acc-denied-link"><i class="trav-long-arrow-left" ></i> Return home</a>
                    @endif
                </div>
            </div>
            @if($type == 'plan')
                @if(count($content_list) >= 3)
                    <div class="access-denied-slide-content">
                        <div class="acc-d-slide-title-wrap">
                            <p class="acc-d-slide-title">Other content you might like...</p>
                            <p class="acc-d-slide-subtitle">In the meantime, you can check out content that you might be interested in by other people.</p>
                        </div>
                        <div class="u-list-slide-content">
                            <ul class="plan-list-slider" id="plan_list_slider">
                                @include('errors.partials._plan-block', ['content_list' => $content_list])
                            </ul>
                        </div>
                    </div>
                @endif
            @else
                @if(count($content_list) > 3)
                    <div class="access-denied-slide-content">
                        <div class="acc-d-slide-title-wrap">
                            <p class="acc-d-slide-title">Other content you might like...</p>
                            <p class="acc-d-slide-subtitle">In the meantime, you can check out content that you might be interested in by other people.</p>
                        </div>
                        <div class="u-list-slide-content">
                            <ul class="user-list-slider" id="travelog_list_slider">
                                @include('errors.partials._report-block', ['content_list' => $content_list])
                            </ul>
                        </div>
                    </div>
                @endif
            @endif
        </div>
        <div class="setting-footer-block pt-20">
            <p class="setting-copyright">Travooo © {{date('Y')}}</p>
        </div>
    </div>

</div>
    @if(!Auth::check())
        @include('errors.partials._footer')
    @endif
@endsection
@section('after_scripts')
<script type="text/javascript">
$(document).ready(function(){
       
    $("#plan_list_slider, #travelog_list_slider").lightSlider({
        autoWidth:true,
        pager: false,
        enableDrag: true,
        slideMargin: 25,
        prevHtml: '<i class="trav-angle-left"></i>',
        nextHtml: '<i class="trav-angle-right"></i>',
        addClass: 'acc-user-slider-wrap',
        onBeforeStart: function (el) {
            el.removeAttr("style")
        },
        responsive : [
            {
                breakpoint:767,
                settings: {
                    slideMargin:9,
                }
            },
        ],
    })

})
</script>
@endsection


