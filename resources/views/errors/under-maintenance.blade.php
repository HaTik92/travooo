<!DOCTYPE html>
<html>
<head>
    <title>{{ __('Under Maintenance')}}</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('assets2/css/style-15102019-9.css?v='.time())}}">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            background-color: #ffffff;
        }

        header{
            background: white;
            position: relative;
            box-shadow: 0 0 6px rgb(0 0 0 / 10%);
            z-index: 900;
            height: 67px;
            width: 100%;
            position: absolute;
            text-align: center;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        header img{
            width: 190px;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        @media (min-width: 1130px) {
            .content {
                width: 700px;
            }
        }

        .content {
            text-align: center;
            display: inline-block;
            margin: 0 60px;
        }
        .content-block{
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            margin-bottom: 30px;
        }

        .title {
            font-size: 82.5px;
            font-weight: 500;
            font-stretch: normal;
            font-style: normal;
            line-height: 1.02;
            letter-spacing: normal;
            text-align: left;
            color: #b0b0b0;
            font-family: 'Circular Std Book',-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
            padding-left: 74px;
            display: inline-block;
        }

        .subtitle {
            display: inline-block;
            font-size: 20px;
            font-weight: 300;
            line-height: 2.16;
            color: #1a1a1a;
            margin-bottom: 0;
        }
    </style>
</head>
<body>
<header>
    <a href="{{url('/')}}">
        <img src="{{asset('frontend_assets/image/main-logo.png')}}" alt="">
    </a>
</header>
<div class="container">
    <div class="content">
        <div class="content-block">
            <img src="{{asset('assets2/image/maintenance-icon.png')}}">
            <div>
                <span class="title">{{ __('Under Maintenance')}}</span>
                <span class="subtitle">{{ __('We will be back soon.')}}</span>
            </div>
        </div>
    </div>
</div>
</body>
</html>
