<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.experts.applied', 'All', [], ['class' => 'btn btn-primary btn-xs']) }}

    @foreach((clone $badges)->prepend('None badges', 0) as $id => $name)
        {{ link_to_route('admin.experts.applied', $name, ['badge' => $id], ['class' => 'btn btn-primary btn-xs']) }}
    @endforeach

    @foreach($badges as $id => $name)
        {{ link_to_route('admin.experts.applied', $name . ' Next', ['next-badge' => $id], ['class' => 'btn btn-primary btn-xs']) }}
    @endforeach

</div><!--pull right-->

<div class="clearfix"></div>