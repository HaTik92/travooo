<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.interest.index', 'All Interests', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.interest.create', 'Create Interest', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>