<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.invite-links.index', trans('menus.backend.badges-invitations.invite-links.all'), [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.invite-links.create', trans('menus.backend.badges-invitations.invite-links.create'), [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>