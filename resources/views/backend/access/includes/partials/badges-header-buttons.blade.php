<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.badges.index', trans('menus.backend.badges-invitations.badges.title'), [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.badges.create', trans('menus.backend.badges-invitations.badges.create'), [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>