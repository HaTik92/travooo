<a class="btn btn-xs btn-primary refresh-progress" data-id="{{$id}}">
    <i class="fa fa-refresh refresh-progress-icon" aria-hidden="true" data-original-title="Refresh"></i>
    <i class="fa fa-refresh fa-spin refresh-progress-load-icon" aria-hidden="true" data-original-title="Loading" style="display: none"></i>
</a>