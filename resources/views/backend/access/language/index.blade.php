@extends ('backend.layouts.app')

@section ('title', 'Language Manager')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
@endsection

@section('page-header')
   <!--  <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.active') }}</small>
    </h1> -->
    <h1>
    	Language Manager
    	<small>All Languages</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3> -->
            <h3 class="box-title">Languages</h3>

            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.language-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="languages-table" class="table table-condensed table-hover"
                                            data-url="{{ route("admin.access.languages.table") }}"
                                            data-config="{{config('access.language_table')}}">
                    <thead>
                    <tr>
                        <th data-config="{{config('access.language_table')}}.id">id</th>
                        <th data-config="{{config('access.language_table')}}.title">title</th>
                        <th data-config="{{config('access.language_table')}}.code">code</th>
                        <th data-config="{{config('access.language_table')}}.active">Active</th>
                        <th data-config="action">{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->



        <div class="box-header with-border">
            <h3 class="box-title">Translations Progress</h3>

            <div class="box-tools pull-right">
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table id="progress-table" class="table table-condensed table-hover" data-url="{{ route("admin.access.translation.progress") }}">
                    <thead>
                    <tr>
                        <th>Language Title</th>
                        <th>Language Code</th>
                        <th>Event</th>
                        <th>Total Count</th>
                        <th>Done Count</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection