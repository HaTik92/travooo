@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management'))

@section('after-styles')
{{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
{{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.active') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3>

            @include('backend.select-deselect-all-buttons')
            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.user-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header //here-->

        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover"
                                        data-url="{{route('admin.access.user.table')}}"
                                        data-del="{{ route('admin.access.user.delete_ajax') }}"
                                        data-status="{{isset($status) ? $status : 1}}"
                                        data-config="{{config('access.users_table')}}">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ trans('labels.backend.access.users.table.id') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.name') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.email') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.status') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.username') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.confirmed') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.roles') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.created') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.last_updated') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
                <input type="hidden" id="restoreUser"
                       data-title="{{ trans('strings.backend.general.are_you_sure') }}"
                       data-text="{{ trans('strings.backend.access.users.restore_user_confirm') }}"
                       data-confirmButtonText="{{ trans('strings.backend.general.continue') }}"
                       data-cancelButtonText="{{ trans('buttons.general.cancel') }}"
                >
                <input type="hidden" id="deleteUserPerm"
                       data-title="{{ trans('strings.backend.general.are_you_sure') }}"
                       data-text="{{ trans('strings.backend.access.users.delete_user_confirm') }}"
                       data-confirmButtonText="{{ trans('strings.backend.general.continue') }}"
                       data-cancelButtonText="{{ trans('buttons.general.cancel') }}"
                >
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! history()->renderType('User') !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection
