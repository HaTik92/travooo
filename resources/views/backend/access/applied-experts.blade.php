@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.experts.management'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.experts.management') }}
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.experts.applied') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.applied-experts-header-buttons')
            </div><!--box-tools pull-right-->
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover"
                       data-url="{{route('admin.experts.table')}}"
                       data-del="{{ route('admin.access.user.delete_ajax') }}"
                       data-status="1"
                       data-approved="1"
                       data-applied="1"
                       data-config="{{config('access.users_table')}}"
                       data-badges="{{\App\Services\Ranking\RankingService::getAvailableBadgesList()->prepend('None', 0)->toJson()}}"
                       data-filters="{{json_encode($filters)}}"
                >
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ trans('labels.backend.access.users.table.id') }}</th>
                        <th>{{ trans('labels.backend.experts.approved_at') }}</th>
                        <th>{{ trans('labels.backend.experts.badge') }}</th>
                        <th>{{ trans('labels.backend.experts.next_badge') }}</th>
                        <th>{{ trans('labels.backend.experts.points') }}</th>
                        <th>{{ trans('labels.backend.experts.followers') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.name') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.email') }}</th>
                        <th>{{ trans('labels.backend.experts.countries') }}</th>
                        <th>{{ trans('labels.backend.experts.cities') }}</th>
                        <th>{{ trans('labels.backend.experts.places') }}</th>
                        <th>{{ trans('labels.backend.experts.travelstyles') }}</th>
                        <th>{{ trans('labels.backend.experts.interests') }}</th>
                        <th>{{ trans('labels.backend.experts.website') }}</th>
                        <th>{{ trans('labels.backend.experts.twitter') }}</th>
                        <th>{{ trans('labels.backend.experts.facebook') }}</th>
                        <th>{{ trans('labels.backend.experts.instagram') }}</th>
                        <th>{{ trans('labels.backend.experts.youtube') }}</th>
                        <th>{{ trans('labels.backend.experts.pinterest') }}</th>
                        <th>{{ trans('labels.backend.experts.tumblr') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.created') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection
