@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.experts.management'))

@section('after-styles')
{{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
{{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
<style>
    .approve-expert, .disapprove-expert {
        height: 22px;
        width: 23px;
        padding: 0;
        display: inline-flex;
        justify-content: center;
        align-items: center;
        border-radius: 3px;
        border: 0;

    }

    .approve-expert-icon, .disapprove-expert-icon {
        width: 100%;
        height: 100%;
        font-size: 11px;
        padding: 0;
        display: flex;
        justify-content: center;
        align-items: center;
    }
</style>
@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.experts.management') }}
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.access.experts.active') }}</h3>
                @include('backend.access.includes.partials.expert-approve-disapprove-selected-buttons')
            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.experts-header-buttons')
            </div><!--box-tools pull-right-->
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover"
                                        data-url="{{route('admin.experts.table')}}"
                                        data-del="{{ route('admin.access.user.delete_ajax') }}"
                                        data-approve="{{ route('admin.experts.approve_experts') }}"
                                        data-disapprove="{{ route('admin.experts.disapprove_experts') }}"
                                        data-status="1"
                                        data-approved="any"
                                        data-config="{{config('access.users_table')}}">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ trans('labels.backend.access.users.table.id') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.name') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.email') }}</th>
                        <th>{{ trans('labels.backend.experts.website') }}</th>
                        <th>{{ trans('labels.backend.experts.twitter') }}</th>
                        <th>{{ trans('labels.backend.experts.facebook') }}</th>
                        <th>{{ trans('labels.backend.experts.instagram') }}</th>
                        <th>{{ trans('labels.backend.experts.youtube') }}</th>
                        <th>{{ trans('labels.backend.experts.pinterest') }}</th>
                        <th>{{ trans('labels.backend.experts.tumblr') }}</th>
                        <th>{{ trans('labels.backend.experts.points') }}</th>
                        <th>{{ trans('labels.backend.experts.link') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.created') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.last_updated') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection
