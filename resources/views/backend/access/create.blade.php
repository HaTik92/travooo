<?php
use App\Models\Access\User\User;
?>
@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.create'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.create') }}</small>
    </h1>
@endsection

@section('content')
<!-- Language Error Style: Start -->
<style>
    .required_msg{
        padding-left: 20px;
    }
</style>
<!-- Language Error Style: End -->

    {{ Form::open([
            'id'    => 'User_form',
            'route' => 'admin.access.user.store',
            'class' => 'form-horizontal',
            'role' => 'form',
            'method' => 'post',
            'files' => true
        ])
    }}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.access.users.create') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.access.includes.partials.user-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <!-- Required Error : Start -->
            <div class="row error-box">
                <div class="col-md-10">
                    <div class="required_msg">
                    </div>
                </div>
            </div>
            <!-- Required Error : End -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('name', trans('validation.attributes.backend.access.users.name'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.access.users.name')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <!-- Username: Start -->
                <div class="form-group">
                    {{ Form::label('username', trans('validation.attributes.backend.access.users.username'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('username', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.username')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Username: End -->

                <div class="form-group">
                    {{ Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.email')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('password', trans('validation.attributes.backend.access.users.password'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.password')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('password_confirmation', trans('validation.attributes.backend.access.users.password_confirmation'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.password_confirmation')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('date_of_birth', 'Date Of Birth', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-6">
                        <div class="input-group date" id="datetimepicker_date_of_birth" data-target-input="nearest">
                            {{ Form::text('date_of_birth', null, ['class' => 'form-control datetimepicker-input', 'data-target' => '#datetimepicker_date_of_birth']) }}
                            <span class="input-group-addon" data-target="#datetimepicker_date_of_birth" data-toggle="datetimepicker">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <!-- Address: Start -->
                <div class="form-group">
                    {{ Form::label('address', trans('validation.attributes.backend.access.users.address'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('address', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.address')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Address: End -->

                <!-- Gender: Start -->
                <div class="form-group">
                    {{ Form::label('gender', trans('validation.attributes.backend.access.users.gender'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('gender', array_combine(range(1, count(User::getGender())), array_values(User::getGender())), 'all', ['class' => 'form-control select2Class']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Gender: End -->

                <!-- Single: Start -->
                <div class="form-group">
                    {{ Form::label('single', trans('validation.attributes.backend.access.users.single'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('single', User::getRelationStatus(), 'all', ['class' => 'form-control select2Class']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Single: End -->

                <!-- Children: Start -->
                <div class="form-group">
                    {{ Form::label('children', trans('validation.attributes.backend.access.users.children'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('children', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.children')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Children: End -->

                <!-- Mobile: Start -->
                <div class="form-group">
                    {{ Form::label('mobile', trans('validation.attributes.backend.access.users.mobile'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('mobile', null, ['id' => 'customer_phone', 'class' => 'form-control', 'maxlength' => '191', 'data-utils' => asset('js/backend/plugin/intl-tel-input/js/utils.js')]) }}
                        <input type="hiddent" id="server_phone" name = "server_phone" hidden="true">
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Mobile: End -->

                <!-- Nationality: Start -->
                <div class="form-group">
                    {{ Form::label('nationality', trans('validation.attributes.backend.access.users.nationality'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <!-- {{ Form::text('nationality', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.nationality')]) }} -->
                        <?php
                            $countries = array_combine(range(1, count(User::getCountries())), array_values(User::getCountries()));
                            $list = [];
                            foreach ($countries as $key => $country) {
                                $list[$country] = $key;
                            }
                        ?>
                        {{ Form::select('nationality', $list, 'all', ['class' => 'form-control select2Class']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Nationality: End -->

                <div class="form-group">
                    {{ Form::label('type', trans('validation.attributes.backend.access.users.type'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('type', \App\Models\User\User::getUserTypes(), 'all', ['class' => 'form-control select2Class']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <!-- Travel Type: Start -->
                <div class="form-group">
                    {{ Form::label('travel_type', trans('validation.attributes.backend.access.users.travel_type'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('travel_type', User::travelTypes(), 'all', ['class' => 'form-control select2Class']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Travel Type: End -->

                <div class="form-group">
                    {{ Form::label('cities_countries', trans('validation.attributes.backend.access.users.cities_countries'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <select style="width: 200px" multiple name="cities-countries[]" id="" class="cities-countries">
                        </select>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('places', trans('validation.attributes.backend.access.users.places'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <select style="width: 200px" multiple name="places[]" id="" class="places">
                        </select>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('interests', trans('validation.attributes.backend.access.users.interests'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('interests', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.interests')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('website', trans('validation.attributes.backend.access.users.website'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('website', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.website')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('twitter', trans('validation.attributes.backend.access.users.twitter'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('twitter', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.twitter')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('instagram', trans('validation.attributes.backend.access.users.instagram'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('instagram', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.instagram')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('facebook', trans('validation.attributes.backend.access.users.facebook'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('facebook', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.facebook')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('pinterest', trans('validation.attributes.backend.access.users.pinterest'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('pinterest', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.pinterest')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('tumblr', trans('validation.attributes.backend.access.users.tumblr'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('tumblr', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.tumblr')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('youtube', trans('validation.attributes.backend.access.users.youtube'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('youtube', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.youtube')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <!-- Public Profile: Start -->
                <div class="form-group">
                    {{ Form::label('public_profile', trans('validation.attributes.backend.access.users.public_profile'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::checkbox('public_profile', '1', true) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Public Profile: End -->

                <!-- Notifications: Start -->
                <div class="form-group">
                    {{ Form::label('notifications', trans('validation.attributes.backend.access.users.notifications'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::checkbox('notifications', '1', true) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Notifications: End -->

                <!-- Messages: Start -->
                <div class="form-group">
                    {{ Form::label('messages', trans('validation.attributes.backend.access.users.messages'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::checkbox('messages', '1', true) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Messages: End -->

                <!-- SMS: Start -->
                <div class="form-group">
                    {{ Form::label('sms', trans('validation.attributes.backend.access.users.sms'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::checkbox('sms', '1', true) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- SMS: End -->

                <div class="form-group">
                    {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-1">
                        {{ Form::checkbox('status', '1', true) }}
                    </div><!--col-lg-1-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('confirmed', trans('validation.attributes.backend.access.users.confirmed'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-1">
                        {{ Form::checkbox('confirmed', '1', true) }}
                    </div><!--col-lg-1-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.send_confirmation_email') }}<br/>
                        <small>{{ trans('strings.backend.access.users.if_confirmed_off') }}</small>
                    </label>

                    <div class="col-lg-1">
                        {{ Form::checkbox('confirmation_email', '1', true) }}
                    </div><!--col-lg-1-->
                </div><!--form control-->

                <!-- Profile Picture: Start -->
                 <div class="form-group">
                    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.profile_picture') }}
                    </label>

                    <div class="col-lg-1">
                        {!! Form::file('profile_picture', null) !!}
                    </div><!--col-lg-1-->
                </div><!--form control-->
                <!-- Profile Picture: End -->

                <div class="form-group">
                    {{ Form::label('associated_roles', trans('validation.attributes.backend.access.users.associated_roles'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-4">
                        @if (count($roles) > 0)
                            @foreach($roles as $role)
                                <input type="checkbox" value="{{ $role->id }}" name="assignees_roles[{{ $role->id }}]" data-name="{{$role->name}}" id="role-{{ $role->id }}" {{ is_array(old('assignees_roles')) && in_array($role->id, old('assignees_roles')) ? 'checked' : '' }} /> <label for="role-{{ $role->id }}">{{ $role->name }}</label>
                                <a href="#" data-role="role_{{ $role->id }}" class="show-permissions small">
                                    (
                                        <span class="show-text">{{ trans('labels.general.show') }}</span>
                                        <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
                                        {{ trans('labels.backend.access.users.permissions') }}
                                    )
                                </a>
                                <br/>
                                <div class="permission-list hidden" data-role="role_{{ $role->id }}">
                                    @if ($role->all)
                                        {{ trans('labels.backend.access.users.all_permissions') }}<br/><br/>
                                    @else
                                        @if (count($role->permissions) > 0)
                                            <blockquote class="small">{{--
                                        --}}@foreach ($role->permissions as $perm){{--
                                            --}}{{$perm->display_name}}<br/>
                                                @endforeach
                                            </blockquote>
                                        @else
                                            {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
                                        @endif
                                    @endif
                                </div><!--permission list-->
                            @endforeach
                        @else
                            {{ trans('labels.backend.access.users.no_roles') }}
                        @endif
                    </div><!--col-lg-4-->
                </div><!--form control-->
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-info">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.access.user.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs submit_button']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@endsection
@section('after-styles')
    <!-- https://tempusdominus.github.io/bootstrap-3/ -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tempusdominus-bootstrap-3@5.0.0-alpha10/build/css/tempusdominus-bootstrap-3.min.css" />
@endsection

@section('before-scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('after-scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <!-- https://tempusdominus.github.io/bootstrap-3/ -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/tempusdominus-bootstrap-3@5.0.0-alpha10/build/js/tempusdominus-bootstrap-3.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).on('click', 'input[data-name="Super Administrator"]', function(){
            if (this.checked) {
                $('input[data-name="Administrator"]').prop("checked", true);
            }
        })
    </script>
@endsection