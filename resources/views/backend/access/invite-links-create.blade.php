<?php
use App\Models\Access\User\User;
?>
@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.invite-links.management') . ' | ' . trans('labels.backend.invite-links.create'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.invite-links.management') }}
        <small>{{ trans('labels.backend.invite-links.create') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open([
            'id'    => 'User_form',
            'route' => 'admin.invite-links.store',
            'class' => 'form-horizontal',
            'role' => 'form',
            'method' => 'post',
            'files' => true
        ])
    }}

    <div class="required_msg"></div>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.invite-links.create') }}</h3>
            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.invite-links-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {{ Form::label('points', trans('validation.attributes.backend.invite-links.name'), ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.invite-links.name')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
            <div class="form-group">
                {{ Form::label('badge', 'Badge', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::select('badge', \App\Services\Ranking\RankingService::getAvailableBadgesList()->prepend('None', 0), 'all', ['class' => 'form-control select2Class']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-success">
        <div class="box-body">
            <div class="pull-left">
                {{ link_to_route('admin.invite-links.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs submit_button']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->

    {{ Form::close() }}
@endsection
