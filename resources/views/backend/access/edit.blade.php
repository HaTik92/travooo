<?php
use App\Models\Access\User\User;
?>
@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($user, [
            'id' => 'User_form',
            'route' => [
                'admin.access.user.update',
                $user
            ], 
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'PATCH',
            'files'  => true,
        ])
    }}

    <div class="required_msg"></div>
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.access.users.edit') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.access.includes.partials.user-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('name', trans('validation.attributes.backend.access.users.name'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('name', $user->name, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.access.users.name')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <!-- Username: Start -->
                <div class="form-group">
                    {{ Form::label('username', trans('validation.attributes.backend.access.users.username'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('username', $user->username, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.username')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Username: End -->

                <div class="form-group">
                    {{ Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::email('email', $user->email, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.email')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('date_of_birth', 'Date Of Birth', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-6">
                        <div class="input-group date" id="datetimepicker_date_of_birth" data-target-input="nearest">
                            {{ Form::text('date_of_birth', $user->birth_date ? date('Y-m-d', strtotime($user->birth_date)) : null, ['class' => 'form-control datepicker-input', 'data-target' => '#datetimepicker_date_of_birth']) }}
                            <span class="input-group-addon" data-target="#datetimepicker_date_of_birth" data-toggle="datetimepicker">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <!-- Address: Start -->
                <div class="form-group">
                    {{ Form::label('address', trans('validation.attributes.backend.access.users.address'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('address', $user->address, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.address')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Address: End -->

                <!-- Gender: Start -->
                <div class="form-group">
                    {{ Form::label('gender', trans('validation.attributes.backend.access.users.gender'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('gender', array_combine(range(1, count(User::getGender())), array_values(User::getGender())), $user->gender, ['class' => 'form-control select2Class']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Gender: End -->

                <!-- Single: Start -->
                <div class="form-group">
                    {{ Form::label('single', trans('validation.attributes.backend.access.users.single'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('single', User::getRelationStatus(), $user->single, ['class' => 'form-control select2Class']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Single: End -->

                <!-- Children: Start -->
                <div class="form-group">
                    {{ Form::label('children', trans('validation.attributes.backend.access.users.children'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('children', $user->children, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.children')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Children: End -->

                <!-- Mobile: Start -->
                <div class="form-group">
                    {{ Form::label('mobile', trans('validation.attributes.backend.access.users.mobile'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('mobile', null, ['id' => 'customer_phone', 'class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.mobile'), 'data-utils' => asset('js/backend/plugin/intl-tel-input/js/utils.js')]) }}
                        <input type="hiddent" id="server_phone" name = "server_phone" hidden="true">
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Mobile: End -->

                <!-- Nationality: Start -->
                <div class="form-group">
                    {{ Form::label('nationality', trans('validation.attributes.backend.access.users.nationality'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <!-- {{ Form::text('nationality', $user->nationality, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.nationality')]) }} -->
                        <?php
                            $countries = array_combine(range(1, count(User::getCountries())), array_values(User::getCountries()));
                        ?>
                        {{ Form::select('nationality', $countries, [$user->nationality], ['class' => 'form-control select2Class']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Nationality: End -->

                <div class="form-group">
                    {{ Form::label('type', trans('validation.attributes.backend.access.users.type'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('type', \App\Models\User\User::getUserTypes(), $user->type, ['class' => 'form-control select2Class']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <!-- Travel Type: Start -->
                <div class="form-group">
                    {{ Form::label('travel_type', trans('validation.attributes.backend.access.users.travel_type'), ['class' => 'col-lg-2 control-label']) }}

                    @php
                        if($user->type === 2) {
                            $travelStyle = $user->expertTravelStyles->first() ? $user->expertTravelStyles->first()->travel_style_id : null;
                        } else {
                            $travelStyle = $user->travelstyles->first() ? $user->travelstyles->first()->conf_lifestyles_id : null;
                        }
                    @endphp

                    <div class="col-lg-10">
                        {{ Form::select('travel_type', User::travelTypes(), $travelStyle, ['class' => 'form-control select2Class']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Travel Type: End -->

                <div class="form-group">
                    {{ Form::label('cities_countries', trans('validation.attributes.backend.access.users.cities_countries'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <select style="width: 200px" multiple name="cities-countries[]" id="" class="cities-countries">
                        </select>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('places', trans('validation.attributes.backend.access.users.places'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <select style="width: 200px" multiple name="places[]" id="" class="places">
                        </select>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('interests', trans('validation.attributes.backend.access.users.interests'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('interests', $user->interests, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.interests')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('website', trans('validation.attributes.backend.access.users.website'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('website', $user->website, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.website')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('twitter', trans('validation.attributes.backend.access.users.twitter'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('twitter', $user->twitter, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.twitter')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('instagram', trans('validation.attributes.backend.access.users.instagram'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('instagram', $user->instagram, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.instagram')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('facebook', trans('validation.attributes.backend.access.users.facebook'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('facebook', $user->facebook, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.facebook')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('pinterest', trans('validation.attributes.backend.access.users.pinterest'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('pinterest', $user->pinterest, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.pinterest')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('tumblr', trans('validation.attributes.backend.access.users.tumblr'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('tumblr', $user->tumblr, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.tumblr')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('youtube', trans('validation.attributes.backend.access.users.youtube'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('youtube', $user->youtube, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.youtube')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                @if ($user->id != 1)
                    <div class="form-group">
                        {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-1">
                            {{ Form::checkbox('status', $user->status) }}
                        </div><!--col-lg-1-->
                    </div><!--form control-->

                    <div class="form-group">
                        {{ Form::label('confirmed', trans('validation.attributes.backend.access.users.confirmed'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-1">
                            {{ Form::checkbox('confirmed', $user->confirmed) }}
                        </div><!--col-lg-1-->
                    </div><!--form control-->
                     <!-- Public Profile: Start -->
                <div class="form-group">
                    {{ Form::label('public_profile', trans('validation.attributes.backend.access.users.public_profile'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::checkbox('public_profile', $user->public_profile) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Public Profile: End -->

                <!-- Notifications: Start -->
                <div class="form-group">
                    {{ Form::label('notifications', trans('validation.attributes.backend.access.users.notifications'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::checkbox('notifications', $user->notifications) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Notifications: End -->

                <!-- Messages: Start -->
                <div class="form-group">
                    {{ Form::label('messages', trans('validation.attributes.backend.access.users.messages'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::checkbox('messages', $user->messages) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Messages: End -->

                <!-- SMS: Start -->
                <div class="form-group">
                    {{ Form::label('sms', trans('validation.attributes.backend.access.users.sms'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::checkbox('sms', $user->sms) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- SMS: End -->
                <!-- Profile Picture: Start -->
                 <div class="form-group">
                    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.profile_picture') }}
                    </label>

                    <div class="col-lg-1">
                        {!! Form::file('profile_picture', null) !!}
                    </div><!--col-lg-1-->
                </div><!--form control-->
                <div class="form-group">
                    <label class="col-lg-2 control-label">
                    </label>
                    <div class="image-preview col-lg-6" style="max-width: 150px">
                        <img style="width: 100%; height: auto" src="{{ check_profile_picture($user->profile_picture) }}" alt="">
                    </div>
                </div>
                <!-- Profile Picture: End -->

                    <div class="form-group">
                        {{ Form::label('associated_roles', trans('validation.attributes.backend.access.users.associated_roles'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-4">
                            @if (count($roles) > 0)
                                @foreach($roles as $role)
                                    <input type="checkbox" value="{{$role->id}}" name="assignees_roles[{{ $role->id }}]" data-name="{{$role->name}}" {{ is_array(old('assignees_roles')) ? (in_array($role->id, old('assignees_roles')) ? 'checked' : '') : (in_array($role->id, $userRoles) ? 'checked' : '') }} id="role-{{$role->id}}" /> <label for="role-{{$role->id}}">{{ $role->name }}</label>
                                        <a href="#" data-role="role_{{$role->id}}" class="show-permissions small">
                                            (
                                                <span class="show-text">{{ trans('labels.general.show') }}</span>
                                                <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
                                                {{ trans('labels.backend.access.users.permissions') }}
                                            )
                                        </a>
                                    <br/>
                                    <div class="permission-list hidden" data-role="role_{{$role->id}}">
                                        @if ($role->all)
                                            {{ trans('labels.backend.access.users.all_permissions') }}<br/><br/>
                                        @else
                                            @if (count($role->permissions) > 0)
                                                <blockquote class="small">{{--
                                            --}}@foreach ($role->permissions as $perm){{--
                                            --}}{{$perm->display_name}}<br/>
                                                    @endforeach
                                                </blockquote>
                                            @else
                                                {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
                                            @endif
                                        @endif
                                    </div><!--permission list-->
                                @endforeach
                            @else
                                {{ trans('labels.backend.access.users.no_roles') }}
                            @endif
                        </div><!--col-lg-4-->
                    </div><!--form control-->
                @endif
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.access.user.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs submit_button']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

        @if ($user->id == 1)
            {{ Form::hidden('status', 1) }}
            {{ Form::hidden('confirmed', 1) }}
            {{ Form::hidden('assignees_roles[0]', 1) }}
        @endif

    {{ Form::close() }}

    @php
        if($user->type === 2) {
            $places = $user->expertPlaces->map(function($expertPlace) {
                $newPlace = [];

                $newPlace['fav_type'] = 'place';
                $newPlace['fav_id'] = $expertPlace->places_id;

                return $newPlace;
            });



            $cities = $user->expertCities->map(function($expertCity) {
                $newCity = [];

                $newCity['fav_type'] = 'city';
                $newCity['fav_id'] = $expertCity->cities_id;

                return $newCity;
            });

            $countries = $user->expertCountries->map(function($expertCountry) {
                $newCountry = [];

                $newCountry['fav_type'] = 'country';
                $newCountry['fav_id'] = $expertCountry->countries_id;

                return $newCountry;
            });

            $locs = $countries->concat($cities);
        } else {
            $places = $user->favourites->filter(function($loc) { return $loc->fav_type === 'place'; });
            $locs = $user->favourites->filter(function($loc) { return $loc->fav_type === 'city' || $loc->fav_type === 'country'; });
        }
    @endphp

@endsection

@section('after-styles')
    <!-- https://tempusdominus.github.io/bootstrap-3/ -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tempusdominus-bootstrap-3@5.0.0-alpha10/build/css/tempusdominus-bootstrap-3.min.css" />
@endsection


@section('before-scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('after-scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <!-- https://tempusdominus.github.io/bootstrap-3/ -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/tempusdominus-bootstrap-3@5.0.0-alpha10/build/js/tempusdominus-bootstrap-3.min.js"></script>


    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).on('click', 'input[data-name="Super Administrator"]', function(){
            if (this.checked) {
                $('input[data-name="Administrator"]').prop("checked", true);
            }
        })

        // Cities Countries

        $('select.cities-countries').select2({
            ajax: {
                method: 'GET',
                url: '/registration/step-3',
                data: function (params) {
                    var query = {
                        search: params.term,
                    }

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                },
                processResults: function (data) {
                    var newData = $.map(data.data, function(item, index){
                        item.text = item.name + ' (' + item.desc + ')';
                        /* при этом из кода видно, что каждый
                         элемент содержит доп. поле code*/
                        return item;
                    });

                    return {
                        results: newData
                    };
                }
            }
        });

        var citiesCountriesSelect = $('select.cities-countries');

        var preselectedLocs = @json($locs);


        if(typeof preselectedLocs === 'object' && preselectedLocs !== null) {
            preselectedLocs = Object.values(preselectedLocs)
        }

        preselectedLocs = preselectedLocs.map((loc) => {
            return loc.fav_type + '-' + loc.fav_id;
        });

        $.ajax({
            type: 'POST',
            url: '/admin/access/user/cities-countries',
            data: {
                locs: preselectedLocs
            }
        }).then(function (data) {

            data.data.forEach((item) => {
                var option = new Option(item.name + ' (' + item.desc + ')', item.id, true, true);
                citiesCountriesSelect.append(option).trigger('change');
            });

            // manually trigger the `select2:select` event
            citiesCountriesSelect.trigger({
                type: 'select2:select',
                params: {
                    data: data
                }
            });
        });

        // Places

        $('select.places').select2({
            ajax: {
                method: 'GET',
                url: '/registration/step-4',
                data: function (params) {
                    var query = {
                        search: params.term,
                    }

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                },
                processResults: function (data) {
                    var newData = $.map(data.data, function(item, index){
                        item.id = item.type + '-' + item.id; // создаём два требуемых поля
                        item.text = item.name + ' (' + item.desc + ')';
                        /* при этом из кода видно, что каждый
                         элемент содержит доп. поле code*/
                        return item;
                    });
                    return {
                        results: newData
                    };
                }
            }
        });

        var placesSelect = $('select.places');

        var preselectedPlaces = @json($places);

        if(typeof preselectedPlaces === 'object' && preselectedPlaces !== null) {
            preselectedPlaces = Object.values(preselectedPlaces)
        }

        preselectedPlaces = preselectedPlaces.map((loc) => {
            return loc.fav_type + '-' + loc.fav_id;
        });

        $.ajax({
            type: 'POST',
            url: '/admin/access/user/places',
            data: {
                locs: preselectedPlaces
            }
        }).then(function (data) {

            data.data.forEach((item) => {
                var option = new Option(item.name + ' (' + item.desc + ')', item.id, true, true);
                placesSelect.append(option).trigger('change');
            });

            // manually trigger the `select2:select` event
            placesSelect.trigger({
                type: 'select2:select',
                params: {
                    data: data
                }
            });
        });

        // var option = new Option(data.full_name, data.id, true, true);
        // citiesCountries.append(option).trigger('change');
        //
        // // manually trigger the `select2:select` event
        // citiesCountries.trigger({
        //     type: 'select2:select',
        //     params: {
        //         data: data
        //     }
        // });

    </script>
@endsection
