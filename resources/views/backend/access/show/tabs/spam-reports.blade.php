<table class="table table-striped table-hover" style="word-wrap:break-word">
    <h3>{{ trans('labels.backend.access.users.tabs.content.overview.spam') }}</h3>
    <tr>
        <th>ID</th>
        <th>Report Type</th>
        <th>Post Type</th>
        <th>Post ID</th>
        <th>Date</th>
        @if ($user->confirmedReports && count($user->confirmedReports ))
            @foreach ($user->confirmedReports as $confirmedReport)
                @if ($confirmedReport->spamsPost && is_object($confirmedReport->spamsPost))
                    <?php
                        $type = 'other';
                        switch($confirmedReport->spamsPost->report_type ){
                            case 0:
                                $type = 'spam';
                                break;
                            case 2:
                                $type = 'fake_news';
                                break;
                            case 3:
                                $type = 'harassment';
                                break;
                            case 4:
                                $type = 'hate_speech';
                                break;
                            case 5:
                                $type = 'nudity';
                                break;
                            case 6:
                                $type = 'terrorism';
                                break;
                            case 7:
                                $type = 'violence';
                                break;
                            case 8:
                                $type = 'api_reports';
                                break;
                            default:
                                break;
                        }
                    ?>
                    <tr>
                        <td><a href="{{ route('admin.spam_manager.show', ['id' => $confirmedReport->spamsPost->id]) }}" target="_blank">{{ $confirmedReport->spamsPost->id }}</a></td>
                        <td>{{ trans('labels.backend.spam_reports.'.$type) }}</td>
                        <td>{{ $confirmedReport->spamsPost->post_type }}</td>
                        <td>{{ $confirmedReport->spamsPost->posts_id }}</td>
                        <td>{{ $confirmedReport->spamsPost->created_at }}</td>
                    </tr>
                @endif
            @endforeach
        @else
            <tr><td colspan="5" class="text-center">No data available in table</td></tr>
        @endif
    </tr>
</table>