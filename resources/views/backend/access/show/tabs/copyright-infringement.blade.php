<table class="table table-striped table-hover" style="word-wrap:break-word">
    <h3>{{ trans('labels.backend.access.users.tabs.content.overview.copyright') }}</h3>
    <tr>
        <th>ID</th>
        <th>Author Email</th>
        <th>Source Link</th>
        <th>Reported URL</th>
        <th>Infringement Location</th>
        @if($user->copyrightInfringement && count($user->copyrightInfringement) )
            @foreach ($user->copyrightInfringement as $copyrightInfringement)
                <tr>
                    <td><a href="{{route('admin.copyright-infringement.show', $copyrightInfringement->id)}}" target="_blank">{{ $copyrightInfringement->id }}</a></td>
                    <td>{{ $copyrightInfringement->sender_email }}</td>
                    <td>
                        @foreach(json_decode($copyrightInfringement->original_link) as $link)
                            <a href="{{$link}}" target="_blank">{{ $link }}</a><br>
                        @endforeach
                    </td>
                    <td><a href="{{ $copyrightInfringement->reported_url }}" target="_blank">{{ $copyrightInfringement->reported_url }}</a></td>
                    <td>{{ trans('labels.backend.copyright_infringement.' . $copyrightInfringement->infringement_location )}}</td>
                </tr>
            @endforeach
        @else
            <tr><td colspan="5" class="text-center">No data available in table</td></tr>
        @endif
    </tr>
</table>

