@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.view'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.view') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.access.users.view') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.user-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">{{ trans('labels.backend.access.users.tabs.titles.overview') }}</a>
                    </li>

                    <li role="presentation">
                        <a href="#history" aria-controls="history" role="tab" data-toggle="tab">{{ trans('labels.backend.access.users.tabs.titles.history') }}</a>
                    </li>

                    <li role="presentation">
                        <a href="#copyright-infringement" aria-controls="copyright-infringement" role="tab" data-toggle="tab">{{ trans('labels.backend.access.users.tabs.titles.infringement') }}</a>
                    </li>

                    <li role="presentation">
                        <a href="#spam-reports" aria-controls="spam-reports" role="tab" data-toggle="tab">{{ trans('labels.backend.access.users.tabs.titles.spam') }}</a>
                    </li>
                </ul>

                <div class="tab-content">
                    @if ($user)
                        <div role="tabpanel" class="tab-pane mt-30 active" id="overview">
                            @include('backend.access.show.tabs.overview')
                        </div><!--tab overview profile-->

                        <div role="tabpanel" class="tab-pane mt-30" id="history">
                            @include('backend.access.show.tabs.history')
                        </div><!--tab panel history-->

                        <div role="tabpanel" class="tab-pane mt-30" id="copyright-infringement">
                            @include('backend.access.show.tabs.copyright-infringement')
                        </div><!--tab panel infractions-->

                        <div role="tabpanel" class="tab-pane mt-30" id="spam-reports">
                            @include('backend.access.show.tabs.spam-reports')
                        </div><!--tab panel infractions-->
                    @endif
                </div><!--tab content-->

            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
@endsection