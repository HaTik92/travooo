<?php
use App\Models\Access\User\User;
?>
@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.badges.create'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.badges.title') }}
        <small>{{ trans('labels.backend.badges.create') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open([
            'id' => 'User_form',
            'route' => [
                'admin.badges.store'
            ], 
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'POST',
            'files'  => true,
        ])
    }}

    <div class="required_msg"></div>
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.badges.create') }}</h3>

                <div class="box-tools pull-right">
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('name', trans('validation.attributes.backend.badges.name'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.badges.name')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div><!-- /.box-body -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('name', trans('validation.attributes.backend.badges.url'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('url', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.badges.url')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('points', trans('validation.attributes.backend.badges.interactions'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::number('points', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.badges.interactions')]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                </div><!-- /.box-body -->

                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('followers', trans('validation.attributes.backend.badges.followers'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::number('followers', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.badges.followers')]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                </div><!-- /.box-body -->

                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('order', trans('validation.attributes.backend.badges.order'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::number('order', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.badges.order')]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                </div><!-- /.box-body -->

            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.badges.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs submit_button']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}
@endsection
@section('before-scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('after-scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection
