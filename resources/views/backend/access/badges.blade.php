@extends ('backend.layouts.app')

@section ('title', trans('menus.backend.badges-invitations.badges.title'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ trans('menus.backend.badges-invitations.badges.title') }}
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('menus.backend.badges-invitations.badges.title') }}</h3>
            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.badges-header-buttons')
            </div><!--box-tools pull-right-->
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover"
                       data-url="{{route('admin.badges.table')}}"
                       data-status="1"
                       data-config="{{config('access.badges_table')}}">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ trans('labels.backend.badges.table.id') }}</th>
                        <th>{{ trans('labels.backend.badges.table.name') }}</th>
                        <th>{{ trans('labels.backend.badges.table.interactions') }}</th>
                        <th>{{ trans('labels.backend.badges.table.followers') }}</th>
                        <th>{{ trans('labels.backend.badges.table.order') }}</th>
                        <th>{{ trans('labels.backend.badges.table.created') }}</th>
                        <th>{{ trans('labels.backend.badges.table.updated') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection
