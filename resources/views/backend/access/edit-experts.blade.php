
<?php
use App\Models\Access\User\User;
?>
@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.experts.management') . ' | ' . trans('labels.backend.experts.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.experts.management') }}
        <small>{{ trans('labels.backend.experts.edit') }}</small>
    </h1>
@endsection

@section('content')
    <div class="required_msg"></div>
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.experts.email') }}: {{$user->email}}</h3><br>
                <h3 class="box-title">{{ trans('labels.backend.experts.website') }}: <a href="{{$user->website}}" target="_blank">{{$user->website}}</a></h3><br>
                <h3 class="box-title">{{ trans('setting.twitter') }}: <a href="{{$user->twitter}}" target="_blank">{{$user->twitter}}</a></h3><br>
                <h3 class="box-title">{{ trans('setting.facebook') }}: <a href="{{$user->facebook}}" target="_blank">{{$user->facebook}}</a></h3><br>
                <h3 class="box-title">{{ trans('setting.instagram') }}: <a href="{{$user->instagram}}" target="_blank">{{$user->instagram}}</a></h3><br>
                <h3 class="box-title">{{ trans('setting.youtube') }}: <a href="{{$user->youtube}}" target="_blank">{{$user->youtube}}</a></h3><br>
                <h3 class="box-title">{{ trans('labels.backend.experts.pinterest') }}: <a href="{{$user->pinterest}}" target="_blank">{{$user->pinterest}}</a></h3><br>
                <h3 class="box-title">{{ trans('labels.backend.experts.tumblr') }}: <a href="{{$user->tumblr}}" target="_blank">{{$user->tumblr}}</a></h3><br>
                <h3 class="box-title">{{ trans('labels.backend.experts.invite-link') }}: {{@$user->inviteExpertLink->name}} ({{@$user->inviteExpertLink->points}})</h3><br>
                <h3 class="box-title">{{ trans('labels.backend.experts.points') }}: {{@$user->points->points ?? 0}}</h3>

                <div class="box-tools pull-right">
                    @include('backend.access.includes.partials.experts-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                </div><!--form control-->
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@endsection
