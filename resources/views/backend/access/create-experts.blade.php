<?php
use App\Models\Access\User\User;
?>
@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.experts.management'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.experts.management') }}
    </h1>
@endsection

@section('content')
    <!-- Language Error Style: Start -->
    <style>
        .required_msg{
            padding-left: 20px;
        }
    </style>
    <!-- Language Error Style: End -->

    {{ Form::open([
            'id'    => 'User_form',
            'route' => 'admin.experts.store',
            'class' => 'form-horizontal',
            'role' => 'form',
            'method' => 'post',
            'files' => true
        ])
    }}

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.experts.create') }}</h3>

            <div class="box-tools pull-right">
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <!-- Required Error : Start -->
        <div class="row error-box">
            <div class="col-md-10">
                <div class="required_msg">
                </div>
            </div>
        </div>
        <!-- Required Error : End -->

        <div class="box-body">
            <div class="form-group">
                {{ Form::label('name', trans('validation.attributes.backend.access.users.name'), ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.access.users.name')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.email')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <!-- Mobile: Start -->
            <div class="form-group">
                {{ Form::label('mobile', trans('validation.attributes.backend.access.users.mobile'), ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::text('mobile', null, ['id' => 'customer_phone', 'class' => 'form-control', 'maxlength' => '191', 'data-utils' => asset('js/backend/plugin/intl-tel-input/js/utils.js')]) }}
                    <input type="hiddent" id="server_phone" name = "server_phone" hidden="true">
                </div><!--col-lg-10-->
            </div><!--form control-->
            <!-- Mobile: End -->

            <!-- Nationality: Start -->
            <div class="form-group">
                {{ Form::label('nationality', trans('validation.attributes.backend.access.users.nationality'), ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                <!-- {{ Form::text('nationality', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.nationality')]) }} -->
                    <?php
                    $countries = User::getCountries();
                    $list = [];
                    foreach ($countries as $key => $country) {
                        $list[$country] = $country;
                    }
                    ?>
                    {{ Form::select('nationality', $list, 'all', ['class' => 'form-control select2Class']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <!-- Travel Type: Start -->
            <div class="form-group">
                {{ Form::label('travel_type', 'Expertise Travel Type', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::select('travel_type', User::travelTypes(), 'all', ['class' => 'form-control select2Class']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
            <!-- Travel Type: End -->

            <div class="form-group">
                {{ Form::label('cities_countries', 'Expertise cities and countries', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    <select style="width: 200px" multiple name="cities-countries[]" id="" class="cities-countries">
                    </select>
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('interests', trans('validation.attributes.backend.access.users.interests'), ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::text('interests', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.interests')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('website', trans('validation.attributes.backend.access.users.website'), ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::text('website', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.website')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('twitter', trans('validation.attributes.backend.access.users.twitter'), ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::text('twitter', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.twitter')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('instagram', trans('validation.attributes.backend.access.users.instagram'), ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::text('instagram', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.instagram')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('facebook', trans('validation.attributes.backend.access.users.facebook'), ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::text('facebook', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.facebook')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('youtube', trans('validation.attributes.backend.access.users.youtube'), ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::text('youtube', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.backend.access.users.youtube')]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group">
                {{ Form::label('badge', 'Badge', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::select('badge', \App\Services\Ranking\RankingService::getAvailableBadgesList()->prepend('None', 0), 'all', ['class' => 'form-control select2Class']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-info">
        <div class="box-body">
            <div class="pull-left">
                {{ link_to_route('admin.access.user.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs submit_button']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->

    {{ Form::close() }}
@endsection
@section('before-scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('after-scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).on('click', 'input[data-name="Super Administrator"]', function(){
            if (this.checked) {
                $('input[data-name="Administrator"]').prop("checked", true);
            }
        })
    </script>
@endsection