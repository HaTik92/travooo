@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.invite-links.management'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.invite-links.management') }}
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.invite-links.active') }}</h3>
            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.invite-links-header-buttons')
            </div><!--box-tools pull-right-->
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover"
                       data-url="{{route('admin.invite-links.table')}}"
                       data-status="1"
                       data-config="{{config('access.invite_links_table')}}">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ trans('labels.backend.invite-links.table.id') }}</th>
                        <th>{{ trans('labels.backend.invite-links.table.name') }}</th>
                        <th>{{ trans('labels.backend.invite-links.table.code') }}</th>
                        <th>{{ trans('labels.backend.invite-links.table.points') }}</th>
                        <th>{{ trans('labels.backend.invite-links.table.created') }}</th>
                        <th>{{ trans('labels.backend.invite-links.table.updated') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection
