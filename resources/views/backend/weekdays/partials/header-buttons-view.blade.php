<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.weekdays.index', 'All Weekdays', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.weekdays.create', 'Create Weekdays', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>