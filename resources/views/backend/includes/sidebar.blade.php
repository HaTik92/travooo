@php
    use App\Models\CommonPage\CommonPage;
@endphp
<!-- Left side column. contains the logo and sidebar -->
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Image" />
            </div><!--pull-left-->
            <div class="pull-left info">
                <p>{{ access()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('strings.backend.general.status.online') }}</a>
            </div><!--pull-left-->
        </div><!--user-panel-->

        <!-- search form (Optional) -->
        {{ Form::open(['route' => 'admin.search.index', 'method' => 'get', 'class' => 'sidebar-form']) }}
        <div class="input-group">
            {{ Form::text('q', Request::get('q'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('strings.backend.general.search_placeholder')]) }}

            <span class="input-group-btn">
                    <button type='submit' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                  </span><!--input-group-btn-->
        </div><!--input-group-->
    {{ Form::close() }}
    <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>

            <li class="{{ active_class(Active::checkUriPattern('admin/dashboard')) }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>

            <li class="header">{{ trans('menus.backend.sidebar.system') }}</li>

            @role(1)
            <li class="{{ active_class(Active::checkUriPattern('admin/access/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('menus.backend.access.title') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/access/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/access/*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/access/user*')) }}">
                        <a href="{{ route('admin.access.user.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.users.management') }}</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/access/role*')) }}">
                        <a href="{{ route('admin.access.role.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.roles.management') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/access/user/logs*')) }}">
                        <a href="{{ route('admin.access.user.logs') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Admin Logs</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="{{ active_class(Active::checkUriPattern('admin/badges-invitations/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('menus.backend.badges-invitations.title') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/badges-invitations/badges/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/badges-invitations*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/badges-invitations/badges*')) }}">
                        <a href="{{ route('admin.badges.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.badges-invitations.badges.title') }}</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/badges-invitations/invite-links*')) }}">
                        <a href="{{ route('admin.invite-links.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.badges-invitations.invite-links.title') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endauth

        <!-- Copyright Manager Start -->
            <li class="{{ active_class(Active::checkUriPattern('admin/copyright-infringement*') , 'active') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ 'Copyright Infringement' }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <!-- Copyright Infringement -->
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/copyright-infringement*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/hotels/hotels*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/copyright-infringement*')) }}">
                        <a href="{{ route('admin.copyright-infringement.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.copyright_infringement.infringement') }}</span>
                        </a>
                    </li>
                    <!-- Suspended  Accounts -->
                    <li class="{{ active_class(Active::checkUriPattern('admin/copyright-infringement*')) }}">
                        <a href="{{ route('admin.copyright_infringement.users') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.copyright_infringement.suspended_accounts') }}</span>
                        </a>
                    </li>
                    <!-- Infringement History -->
                    <li class="{{ active_class(Active::checkUriPattern('admin/copyright-infringement*')) }}">
                        <a href="{{ route('admin.copyright_infringement.history') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.copyright_infringement.infringement_history') }}</span>
                        </a>
                    </li>
                    <!-- Blacklisted Authors -->
                    <li class="{{ active_class(Active::checkUriPattern('admin/copyright-infringement*')) }}">
                        <a href="{{ route('admin.copyright-infringement.index', [ 'blacklist' => true ]) }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.copyright_infringement.blacklisted_authors') }}</span>
                        </a>
                    </li>
                    <!-- Counter Appeal -->
                    {{--                    <li class="{{ active_class(Active::checkUriPattern('admin/copyright-infringement*')) }}">--}}
                    {{--                        <a href="{{ route('admin.copyright-infringement.counter_appeal') }}">--}}
                    {{--                            <i class="fa fa-circle-o"></i>--}}
                    {{--                            <span>{{ trans('labels.backend.copyright_infringement.counter_appeal') }}</span>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}

                </ul>
            </li>
            <!-- Copyright Manager End -->

            @role(1)
            <!-- Cultures Manager -->
            <li class="{{ active_class(Active::checkUriPattern('admin/cultures*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ trans('labels.backend.cultures.cultures_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/cultures*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/cultures*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/cultures*')) }}">
                        <a href="{{ route('admin.cultures.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.cultures.cultures') }}</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Currencies Manager -->
            <li class="{{ active_class(Active::checkUriPattern('admin/currencies*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ trans('labels.backend.currencies.currencies_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/currencies*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/currencies*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/currencies*')) }}">
                        <a href="{{ route('admin.currencies.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.currencies.currencies') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endauth

            @role(5)
            <!-- Embassies Manager Start -->
            <li class="{{ active_class(Active::checkUriPattern('admin/embassies*') , 'active') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ trans('labels.backend.embassies.embassies_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/embassies*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/embassies/embassies*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/embassies*')) }}">
                        <a href="{{ route('admin.embassies.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.embassies.embassies') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Embassies Manager End -->
            @endauth


            @role(1)
            <!-- Emergency Numbers Manager -->
            <li class="{{ active_class(Active::checkUriPattern('admin/emergencynumbers*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ trans('labels.backend.emergency_numbers.emergency_numbers_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/emergencynumbers*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/emergencynumbers*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/emergencynumbers*')) }}">
                        <a href="{{ route('admin.emergencynumbers.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.emergency_numbers.emergency_numbers') }}</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Events Manager Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/events*') , 'active') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('labels.backend.events.events_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/events*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/events*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/events*')) }}">
                        <a href="{{ route('admin.events.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{trans('labels.general.custom') }} {{ trans('labels.backend.events.events') }}</span>
                        </a>
                        <a href="{{ route('admin.events.api_index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>API {{ trans('labels.backend.events.events') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
        <!-- Events Manager End -->

            <!-- Expert Manager Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/experts/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('menus.backend.experts.title') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/experts/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/experts/*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/experts/pending-approval*')) }}">
                        <a href="{{ route('admin.experts.pending-approval') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.experts.pending-approval') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/experts/approved*')) }}">
                        <a href="{{ route('admin.experts.approved') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.experts.approved') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/experts/not-approved*')) }}">
                        <a href="{{ route('admin.experts.not-approved') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.experts.not-approved') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/experts/create*')) }}">
                        <a href="{{ route('admin.experts.create') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.experts.create') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/experts/invited*')) }}">
                        <a href="{{ route('admin.experts.invited') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.experts.special') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/experts/special-approved*')) }}">
                        <a href="{{ route('admin.experts.special_approved') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Approved Special Experts</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/experts/applied*')) }}">
                        <a href="{{ route('admin.experts.applied') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.experts.applied') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Expert Manager End-->

            <!-- Holidays Manager Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/holidays*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ trans('labels.backend.holidays.holidays_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/holidays*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/holidays*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/holidays*')) }}">
                        <a href="{{ route('admin.holidays.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.holidays.holidays') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Holidays Manager End-->
            @endauth

            @role(6)
            <!-- Hotels Manager Start -->
            <li class="{{ active_class(Active::checkUriPattern('admin/hotels*') , 'active') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ 'Hotels Manager' }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/hotels*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/hotels/hotels*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/hotels*')) }}">
                        <a href="{{ route('admin.hotels.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ 'Hotels' }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Hotels Manager End -->
            @endauth

            @role(1)
            <!-- Interests Manager Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/interest*')) }} treeview" style="display:none;">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ trans('labels.backend.interests.interests_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/interest*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/interest/interest*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/interest*')) }}">
                        <a href="{{ route('admin.interest.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.interests.interests') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Interests Manager End-->

            <!--Languages Manager Start -->
            <li class="{{ active_class(Active::checkUriPattern('admin/access/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                     <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                     <span>{{ trans('labels.backend.languages.languages_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/access/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/access/*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/access/langauge*')) }}">
                        <a href="{{ route('admin.access.languages.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.languages.languages_management') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!--Languages Manager End -->

            <!-- Languages Spoken Manager Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/languagesspoken*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ trans('labels.backend.languages_spoken.languages_spoken_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/languagesspoken*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/languagesspoken/languagesspoken*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/languagesspoken*')) }}">
                        <a href="{{ route('admin.languagesspoken.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.languages_spoken.languages_spoken') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Languages Spoken Manager End-->
            @endauth

            <!-- Locations Manager Start -->
            <li class="{{ active_class(Active::checkUriPattern('admin/location/*')) }} treeview">
                @role(1)
                <a href="#">
                    <i class="fa fa-users"></i>
                    <!-- <span>{{ trans('menus.backend.access.title') }}</span> -->
                    <span>{{ trans('labels.backend.locations.locations_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/location/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/location/*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/location/regions*')) }}">
                        <a href="{{ route('admin.location.regions.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <!-- <span>{{ trans('labels.backend.access.users.management') }}</span> -->
                            <span>{{ trans('labels.backend.locations.regions') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/location/country*')) }}">
                        <a href="{{ route('admin.location.country.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <!-- <span>{{ trans('labels.backend.access.users.management') }}</span> -->
                            <span>{{ trans('labels.backend.locations.countries') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/location/city*')) }}">
                        <a href="{{ route('admin.location.city.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <!-- <span>{{ trans('labels.backend.access.users.management') }}</span> -->
                            <span>{{ trans('labels.backend.locations.cities') }}</span>
                        </a>
                    </li>
                    @endauth

                    @role(4)
                    <li class="{{ active_class(Active::checkUriPattern('admin/location/place/*')) | active_class(Active::checkUriPattern('admin/location/place')) }}">
                        <a href="{{ route('admin.location.place.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <!-- <span>{{ trans('labels.backend.access.users.management') }}</span> -->
                            <span>Places & Activities</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/location/unapproved-items/*')) | active_class(Active::checkUriPattern('admin/location/unapproved-items')) }}">
                        <a href="{{ route('admin.location.unapproved-items.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Unapproved items</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/location/isolated/*')) | active_class(Active::checkUriPattern('admin/location/isolateds')) }}">
                        <a href="{{ route('admin.location.isolated.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <!-- <span>{{ trans('labels.backend.access.users.management') }}</span> -->
                            <span>Isolated Places</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/location/spam-reports/*')) | active_class(Active::checkUriPattern('admin/location/spam-reports')) }}">
                        <a href="{{ route('admin.location.spam-reports.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Spam reports</span>
                        </a>
                    </li>
                    @endauth
                </ul>
            </li>
            <!-- Locations Manager End -->

            @role(1)
            <!-- Log Viewer Start -->
            <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer*')) }} treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>{{ trans('menus.backend.log-viewer.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer')) }}">
                        <a href="{{ route('log-viewer::dashboard') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.dashboard') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer/logs')) }}">
                        <a href="{{ route('log-viewer::logs.list') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.logs') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/error-load-reader')) }}">
                        <a href="{{ url('admin/error-load-reader') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Debug Api Error Manual</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer/rank-cron')) }}">
                        <a href="{{ url('admin/log-viewer/rank-cron') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Rank Cron Logs</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Log Viewer End -->

            <!-- Post Manager Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/posts*')) }} treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>Posts Manager</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/posts*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/posts*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/posts/view')) }}">
                        <a href="{{ route('admin.posts.view') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>View Posts</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Post Manager End -->

            <!-- Media Manager Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/activitymedia*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ 'Media Manager' }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/activitymedia*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/activitymedia/activitymedia**'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/activitymedia*')) }}">
                        <a href="{{ route('admin.activitymedia.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ 'Medias' }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Media Manager End-->

            <!-- Pages Manager Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/pages*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ 'Pages Manager' }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/pages/*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/pages') | Active::checkUriPattern('admin/pages/*')) }}">
                        <a href="{{ route('admin.pages.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ 'Pages' }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/pages_categories*')) }}">
                        <a href="{{ route('admin.pages_categories.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ 'Pages Categories' }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Pages Manager End-->

            <!-- Religions Manager Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/religion*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ trans('labels.backend.religions.religions_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/religion*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/religion/religion*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/religion*')) }}">
                        <a href="{{ route('admin.religion.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.religions.religions') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Religions Manager End-->
            @endauth

            @role(7)
            <!-- Restaurants Manager Start -->
            <li class="{{ active_class(Active::checkUriPattern('admin/restaurants*') , 'active') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ 'Restaurants Manager' }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/restaurants*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/restaurants/restaurants*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/restaurants*')) }}">
                        <a href="{{ route('admin.restaurants.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ 'Restaurants' }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Restaurants Manager End -->
            @endauth

        <!-- Search Form Submissions Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/pages*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>Search Form Submissions</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/submissions/*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/submissions') | Active::checkUriPattern('admin/submissions/*')) }}">
                        <a href="{{ route('admin.submissions.index') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>New submissions</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/submissions') | Active::checkUriPattern('admin/submissions/*')) }}">
                        <a href="{{ route('admin.submissions.approved') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>Approved submissions</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/submissions') | Active::checkUriPattern('admin/submissions/*')) }}">
                        <a href="{{ route('admin.submissions.disapproved') }}">
                            <i class="fa fa-circle-o"></i>
                        <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>Disapproved Submissions</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Search Form Submissions End-->

            @role(1)
            <!-- Destination Suggestions Start -->
            <li class="{{ active_class(Active::checkUriPattern('admin/destinations*') , 'active') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                    <span>{{ 'Sign Up Suggestions' }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/destinations*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/destinations*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/destinations/countries-by-nationality*')) }}">
                        <a href="{{ route('admin.countries-by-nationality.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ 'Top Countries by Nationality' }}</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/destinations/cities-by-nationality*')) }}">
                        <a href="{{ route('admin.cities-by-nationality.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ 'Top Cities by Nationality' }}</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/destinations/generic-top-countries*')) }}">
                        <a href="{{ route('admin.generic-top-countries.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ 'Generic Top Countries' }}</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/destinations/generic-top-cities*')) }}">
                        <a href="{{ route('admin.generic-top-cities.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ 'Generic Top Cities' }}</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/destinations/generic-top-places*')) }}">
                        <a href="{{ route('admin.generic-top-places.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ 'Top Places by Country/City' }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Destination Suggestions End -->
            @endauth

            <!-- Spam Manager Start -->
        @role(5)
            <li class="{{ active_class(Active::checkUriPattern('admin/spam_manager*') , 'active') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('labels.backend.spam_reports.spam_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/spam_manager*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/spam_manager*'), 'display: block;') }}">
                    <li class=" treeview">
                        <a href="#">
                            <i class="fa fa-align-justify"></i>
                            <span>Categories</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>

                        <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/spam_manager*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/spam_manager*'), 'display: block;') }}">
                            <li class="{{ active_class(Active::checkUriPattern('admin/spam_reports*')) }}">
                                <a href="{{ route('admin.spam_manager.index', ['type' => 'spam']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>{{ trans('labels.backend.spam_reports.spam') }} </span>
                                </a>
                                <a href="{{ route('admin.spam_manager.index', ['type' => 'fake_news']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>{{ trans('labels.backend.spam_reports.fake_news') }} </span>
                                </a>
                                <a href="{{ route('admin.spam_manager.index', ['type' => 'harassment']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>{{ trans('labels.backend.spam_reports.harassment') }}  </span>
                                </a>
                                <a href="{{ route('admin.spam_manager.index', ['type' => 'hate_speech']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>{{ trans('labels.backend.spam_reports.hate_speech') }} </span>
                                </a>
                                <a href="{{ route('admin.spam_manager.index', ['type' => 'nudity']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>{{ trans('labels.backend.spam_reports.nudity') }} </span>
                                </a>
                                <a href="{{ route('admin.spam_manager.index', ['type' => 'terrorism']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>{{ trans('labels.backend.spam_reports.terrorism') }} </span>
                                </a>
                                <a href="{{ route('admin.spam_manager.index', ['type' => 'violence']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>{{ trans('labels.backend.spam_reports.violence') }} </span>
                                </a>
                                <a href="{{ route('admin.spam_manager.index', ['type' => 'other']) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>{{ trans('labels.backend.spam_reports.other') }} </span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/spam_reports*')) }}">
                        <a href="{{ route('admin.spam_manager.index', ['type' => 'api_reports']) }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.spam_reports.api_reports_for_review') }} </span>
                        </a>
                        <a href="{{ route('admin.spam_manager.api_reports') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.spam_reports.api_reports') }} </span>
                        </a>
                        <a href="{{ route('admin.spam_manager.history') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.spam_reports.history') }} </span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Spam Manager End -->
        @endauth

        @role(1)
        <!-- Top Places Start -->
            <li class="{{ active_class(Active::checkUriPattern('admin/top-places/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('Top Places') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/top-places/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/top-places/*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/top-places/places*')) }}">
                        <a href="{{ route('admin.places.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('Top Places') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/top-places/hotels*')) }}">
                        <a href="{{ route('admin.top_hotels.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('Top Hotels') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/top-places/restaurants*')) }}">
                        <a href="{{ route('admin.restaurants.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('Top Restaurants') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Top Places End -->
        @endauth

            @role(1)
            <!-- Life Styles Manager Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/lifestyle*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                     <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                     <span>Travel Styles</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/lifestyle*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/lifestyle/lifestyle*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/lifestyle*')) }}">
                        <a href="{{ route('admin.lifestyle.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>Travel Styles</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Life Styles Manager End-->

            <!-- Weekdays Manager Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/weekdays*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                     <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                     <span>{{ trans('labels.backend.weekdays.weekdays_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/weekdays*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/weekdays/weekdays*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/weekdays*')) }}">
                        <a href="{{ route('admin.weekdays.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.weekdays.weekdays') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Weekdays Manager End-->

            <!-- Help Center Start-->
            <li class="{{ active_class(Active::checkUriPattern('admin/weekdays*')) }} treeview">
                <a href="#">
                    <i class="fa fa-question-circle"></i>
                    <span>Help Center</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/commonpages*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/commonpages/*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/weekdays*')) }} treeview">
                        <a href="#">
                            <i class="fa fa-gavel"></i>
                            <span>{{ trans('labels.backend.legal.legal_pages') }}</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
        
                        <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/commonpages*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/commonpages/*'), 'display: block;') }}">
                            <li class="{{ active_class(Active::checkUriPattern('admin/commonpages/'.CommonPage::SLUG_PRIVACY_POLICY)) }}">
                                <a href="{{ route('admin.commonpages.show',CommonPage::SLUG_PRIVACY_POLICY) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>{{ trans('labels.backend.legal.privacy_policy') }}</span>
                                </a>
                            </li>
                            <li class="{{ active_class(Active::checkUriPattern('admin/commonpages/'.CommonPage::SLUG_TERMS_OF_SERVICE)) }}">
                                <a href="{{ route('admin.commonpages.show',CommonPage::SLUG_TERMS_OF_SERVICE) }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>{{ trans('labels.backend.legal.terms_of_service') }}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/commonpages/')) }}">
                        <a href="{{ route('admin.help-center.menues') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Menues</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/commonpages/')) }}">
                        <a href="{{ route('admin.help-center.subMenues') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Sub Menues</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Help Center End-->

            <!-- Accommodations Manager -->
            <li class="{{ active_class(Active::checkUriPattern('admin/accommodations*')) }} treeview" style="display:none;">
                <a href="#">
                    <i class="fa fa-users"></i>
                     <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                     <span>{{ trans('labels.backend.accommodations.accommodations_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/accommodations*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/accommodations/accommodations*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/accommodations*')) }}">
                        <a href="{{ route('admin.accommodations.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.accommodations.accommodations') }}</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Age Ranges Manager -->
            <li class="{{ active_class(Active::checkUriPattern('admin/ageranges*')) }} treeview" style="display:none;">
                <a href="#">
                    <i class="fa fa-users"></i>
                     <!-- <span>{{ trans('menus.backend.language.title') }}</span> -->
                     <span>{{ trans('labels.backend.age_ranges.age_ranges_manager') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/ageranges*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/ageranges/ageranges*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/ageranges*')) }}">
                        <a href="{{ route('admin.ageranges.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <!-- <span>{{ trans('labels.backend.access.langauge.management') }}</span> -->
                            <span>{{ trans('labels.backend.age_ranges.age_ranges') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endauth


        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>
