<?php

use App\Models\Access\language\Languages;

$languages = DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
?>
@extends ('backend.layouts.app')

@section ('title', 'Countries Manager' . ' | ' . 'Create Country')

@section('page-header')
    <!-- <h1>
        {{ trans('labels.backend.access.users.management') }}
            <small>{{ trans('labels.backend.access.users.create') }}</small>
    </h1> -->
    <h1>
        Country Manager
        <small>Create Country</small>
    </h1>
@endsection

@section('after-styles')
    <style>
        .add_icon {
            width: 40px;
            font-size: 19px;
        }
        #map {
            height: 300px;
        }
        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 300px;
        }
        #pac-input:focus {
            border-color: #4d90fe;
        }
        .pac-container {
            font-family: Roboto;
        }
        .alert-warning {
            display: none;
        }
    </style>

    <!-- Language Error Style: Start -->
    <style>
        .required_msg {
            padding-left: 20px;
        }
    </style>
    <!-- Language Error Style: End -->

    <link  href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection

@section('content')
    {{ Form::open([
            'id'     => 'country_form',
            'route'  => 'admin.location.country.store',
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'post',
            'files'  => true
        ])
    }}
    <div class="box box-success">
        <div class="box-header with-border">
        <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.create') }}</h3> -->
            <h3 class="box-title">Create Country</h3>

        </div><!-- /.box-header -->
        <!-- Language Error : Start -->
        <div class="row error-box">
            <div class="col-md-10">
                <div class="required_msg">
                </div>
            </div>
        </div>
        <!-- Language Error : End -->
        <div class="box-body">
            @if(!empty($languages))
                <ul class="nav nav-tabs">
                    @foreach($languages as $language)
                        <li class="{{ ($languages[0]->id == $language->id)? 'active':'' }}">
                            <a data-toggle="tab" href="#{{$language->code}}">{{ $language->title }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages as $language)
                        <div id="{{ $language->code }}"
                             class="tab-pane fade in {{ ($language->code == 'en')? 'active':'' }}">
                            <br/>
                            {{ Form::hidden('translations['.$language->code.'][languages_id]', $language->id, []) }}

                        <!-- Start Title -->
                            <div class="form-group">
                                {{ Form::label('translations['.$language->code.'][title]', 'Title', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$language->code.'][title]', null, ['class' => 'form-control required', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => 'Title']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Title -->

                            <!-- Start: Description -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$language->code.'][description]', 'Description', ['class' => 'col-lg-2 control-label description_input']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$language->code.'][description]', null, ['class' => 'form-control description_input description', 'placeholder' => 'Description']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Description -->

                            <!-- Start: Nationality -->
                            <div class="form-group">
                                {{ Form::label('translations['.$language->code.'][nationality]', 'Nationality', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$language->code.'][nationality]', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => 'Nationality']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Nationality -->

                            <!-- Start: population -->
                            <div class="form-group">
                                {{ Form::label('translations['.$language->code.'][population]', 'Population', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$language->code.'][population]', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => 'Population']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Population -->

                            <!-- Start: cost_of_living -->
                            <div class="form-group">
                                {{ Form::label('translations['.$language->code.'][cost_of_living]', 'Cost of Living Index', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$language->code.'][cost_of_living]', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => 'Cost of Living Index']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: cost_of_living -->

                            <!-- Start: geo_stats -->
                            <div class="form-group">
                                {{ Form::label('translations['.$language->code.'][geo_stats]', 'Crime Rate Index', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$language->code.'][geo_stats]', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => 'Crime Rate Index']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: geo_stats -->

                            <!-- Start: demographics -->
                            <div class="form-group">
                                {{ Form::label('translations['.$language->code.'][demographics]', 'Quality of Life Index', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$language->code.'][demographics]', null, ['class' => 'form-control', 'maxlength' => '191','placeholder' => 'Quality of Life Index']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: demographics -->

                            <!-- Start: economy -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('abouts[type][]', 'Restrictions', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-sm-10">
                                    <table class="table table-bordered">
                                        <colgroup>
                                            <col span="1" style="width: 26%;">
                                            <col span="1" style="width: 70%;">
                                            <col span="1" style="width: 4%;">
                                        </colgroup>
                                        <tr>
                                            <th>Title</th>
                                            <th>Body</th>
                                            <th class="set_last_col">Action</th>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                <div class="col-lg-12">
                                                    {{ Form::text('abouts[title][]', '', ['id' => 'restrictions_title','class' => 'form-control', 'maxlength' => 500, 'placeholder' => '']) }}
                                                </div><!--col-lg-12-->
                                            </td>
                                            <td>
                                                <div class="col-lg-12">
                                                    {{-- {{ Form::textarea('translations['.$language->code.'][economy]', null, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Restrictions']) }} --}}
                                                    {{ Form::hidden('abouts[type][]', 'restrictions' , ['id' => 'restrictions_type']) }}
                                                    {{ Form::text('abouts[body][]', '', ['id' => 'restrictions', 'class' => 'form-control', 'maxlength' => 5000, 'placeholder' => '']) }}
                                                </div><!--col-lg-8-->
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" id="add_icon_restrictions" class="btn btn-success btn-xs add_icon">+</a>
                                            </td>
                                        </tr>
                                        <tbody id="addMoreRowsRestrictions"></tbody>
                                    </table>
                                </div>
                            </div><!--form control-->
                            <!-- End: economy -->

                            <!-- Start: suitable_for -->
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-12 textarea-msg"></div>--}}
                                {{--{{ Form::label('translations['.$language->code.'][suitable_for]', 'Hijab Allowed?', ['class' => 'col-lg-2 control-label']) }}--}}

                                {{--<div class="col-lg-10">--}}
                                    {{--{{ Form::textarea('translations['.$language->code.'][suitable_for]', null, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Hijab Allowed?']) }}--}}
                                {{--</div><!--col-lg-10-->--}}
                            {{--</div><!--form control-->--}}
                            <!-- End: suitable_for -->

                            <!-- Languages Tabs: Start -->
                        </div>
                    @endforeach
                </div>

                <!-- Start: code -->
                <div class="form-group">
                    {{ Form::label('code', 'Country Code', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('code', null, ['class' => 'form-control', 'maxlength' => '5', 'required' => 'required', 'placeholder' => 'Country Code']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- End: code -->

                <!-- Start: code -->
                <div class="form-group">
                    {{ Form::label('iso_code', 'ISO Code', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('iso_code', null, ['class' => 'form-control', 'maxlength' => '2', 'required' => 'required', 'placeholder' => '2 letters ISO Code']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- End: code -->
                <!-- Active: Start -->
                <div class="form-group">
                    {{ Form::label('title', trans('validation.attributes.backend.access.languages.active'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::checkbox('active', '1', true) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Active: End -->
                <!-- Region: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Continent', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('region_id', $regions , null,['class' => 'select2Class form-control']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Region: End -->

                <div class="form-group">
                    {{ Form::label('title', 'Select Location', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        <input id="pac-input" class="form-control" type="text" placeholder="Search Box">
                        <div id="map" data-lat="-33.8688" data-lng="151.2195"></div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('title', 'Lat,Lng', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">

                        {{ Form::hidden('lat_lng', null, ['class' => 'form-control disabled', 'id' => 'lat-lng-input', 'placeholder' => 'Lat,Lng']) }}

                        {{ Form::text('lat_lng_show', null, ['class' => 'form-control disabled', 'id' => 'lat-lng-input_show', 'placeholder' => 'Lat,Lng' , 'disabled' => 'disabled']) }}
                    </div>
                </div>

                <!-- Currencies: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Currencies', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::select('currencies_id[]', $currencies, null,['id' => 'currenciesSelect', 'class' => 'select2Class form-control multi-select2', 'multiple' => 'multiple', 'data-placeholder' => 'Choose Currencies...', 'data-url' => url('admin/currencies')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Currencies: End -->

                <!-- Capitals: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Capital', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('cities_id[]', $cities, null,['id' => 'citiesSelect', 'class' => 'select2Class form-control multi-select2', 'multiple' => 'multiple', 'data-placeholder' => 'Choose Capital...', 'data-url' => url('admin/location/city')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Capitals: End -->

                <!-- EmergencyNumbers: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Emergency Numbers', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('emergency_numbers_id[]', $emergency_numbers, null,['id' => 'emergencyNumbersSelect', 'class' => 'select2Class form-control multi-select2', 'multiple' => 'multiple', 'data-placeholder' => 'Choose Emergency Numbers...', 'data-url' => url('admin/emergencynumbers')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- EmergencyNumbers: End -->


                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="col-xs-12 alert alert-warning alert-dismissible fade in">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Warning!</strong> <span class="set_error_msg"></span>
                        </div>
                    </div>
                    {{ Form::label('title', 'Holidays', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <table class="table table-bordered">
                            <tr>
                                <th>Holidays list</th>
                                <th>Date</th>
                                <th class="set_last_col">Action</th>
                            </tr>
                            <tr>
                                <td width="70%">
                                    <div class="holiday_form">
                                        {{ Form::select('holidays_id[]', $holidays, null,['id' => 'holidaysSelect', 'placeholder' => 'Select a Holiday', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%', 'data-placeholder' => 'Choose Holidays...', 'data-url' => url('admin/holidays')]) }}
                                        {{ Form::select('', $holidays, null,['id' => 'holidaysSelectHidden', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'Choose Holidays...']) }}
                                    </div><!--col-lg-10-->
                                </td>
                                <td>
                                    {{ Form::text('holidays_date[]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}
                                    {{ Form::text('', null, ['id' => 'holidaysDateHidden', 'style' => 'display: none;', 'class' => 'form-control', 'autofocus' => 'autofocus']) }}
                                </td>

                                <td>
                                    <a href="javascript:void(0);" id="add_icon_holiday" class="btn btn-success btn-xs add_icon">+</a>
                                </td>
                            </tr>
                            <tbody id="addMoreRowsHoliday"></tbody>
                        </table>
                    </div><!--col-lg-10-->
                </div><!--form control-->


                <!-- Official Languages: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Official Languages', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('languages_spoken_id[]', $languages_spoken, null,['id' => 'languagesSpokenSelect', 'class' => 'select2Class form-control multi-select2', 'multiple' => 'multiple', 'data-placeholder' => 'Choose Official Languages...', 'data-url' => url('admin/languagesspoken')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Official Languages: End -->

                <!-- Additional Languages: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Additional Languages', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('additional_languages_spoken_id[]', $languages_spoken , null,['id' => 'additionalLanguagesSpokenSelect', 'class' => 'select2Class form-control multi-select2', 'multiple' => 'multiple', 'data-placeholder' => 'Choose Additional Languages...', 'data-url' => url('admin/languagesspoken')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Additional Languages: End -->

                <!-- Lifestyles: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Travel Styles', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <table class="table table-bordered">
                            <tr>
                                <th>Lifestyle</th>
                                <th>Rating</th>
                                <th class="set_last_col">Action</th>
                            </tr>
                            <tr>
                                <td width="70%">
                                    {{ Form::select('lifestyles_id[]', $lifestyles, null ,['id' => 'lifestyleSelect', 'placeholder' => 'Select a Lifestyle', 'class' => 'select2Class form-control multi-select2' , 'style' => 'width: 100%', 'data-placeholder' => 'Choose Lifestyle...']) }}
                                    {{ Form::select('', $lifestyles, null, ['id' => 'lifestylesSelectHidden', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'Choose Lifestyle...']) }}
                                </td>
                                <td>
                                    {{ Form::number('lifestyles_rating[]', null, ['class' => 'form-control','min' => '0', 'max' => '10', 'step' => '1', 'autofocus' => 'autofocus']) }}
                                    {{ Form::number('', null, ['id' => 'lifestyleRatingHidden', 'style' => 'display: none;', 'class' => 'form-control', 'min' => '0', 'max' => '10', 'step' => '1','autofocus' => 'autofocus']) }}
                                </td>
                                <td>
                                    <a href="javascript:void(0);" id="add_icon_lifestyle" class="btn btn-success btn-xs add_icon">+</a>
                                </td>
                            </tr>
                            <tbody id="addMoreRowsLifestyle"></tbody>
                        </table>
                    </div>
                </div>
                <!-- Lifestyles: End -->

                <!-- Religions: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Religions', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('religions_id[]', $religions , null,['class' => 'select2Class form-control multi-select2' , 'multiple' => 'multiple']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Religions: End -->
                <!-- Images: Start -->
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="col-xs-12 alert alert-warning alert-dismissible fade in">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Warning!</strong> <span class="set_error_msg"></span>
                        </div>
                    </div>
                    {{ Form::label('title', 'Upload Images', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <table class="table table-bordered">
                            <tr>
                                <th>Select File</th>
                                <th>Title</th>
                                <th>Author Name</th>
                                <th>Author Link</th>
                                <th>Source Link</th>
                                <th>License Name</th>
                                <th>License Link</th>
                                <th class="set_last_col">Action</th>
                            </tr>
                            <tr>
                                <td class="set_img_col">{{ Form::file('',[ 'name' => 'license_images[0][image]']) }}</td>
                                <td>{{ Form::text('license_images[0][title]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                <td>{{ Form::text('license_images[0][author_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                <td>{{ Form::text('license_images[0][author_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                <td>{{ Form::text('license_images[0][source_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                <td>{{ Form::text('license_images[0][license_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                <td>{{ Form::text('license_images[0][license_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                <td>
                                    <a href="javascript:void(0);" id="add_icon" class="btn btn-success btn-xs add_icon">+</a>
                                </td>
                            </tr>
                            <tbody id="addMoreRows"></tbody>
                        </table>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Images: End -->
        @endif
        <!-- Languages Tabs: End -->
        </div>
    </div>
    <div class="box box-info">
        <div class="box-body">
            <div class="pull-left">
                {{ link_to_route('admin.location.country.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs submit_button']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
    {{ Form::close() }}
@endsection