@extends ('backend.layouts.app')

@section ('title', 'Countries Management' . ' | ' . 'View Country')

@section('page-header')
    <h1>
        Countries Management
        <small>View Country</small>
    </h1>
@endsection

@section('after-styles')
    <style type="text/css">
        td.description {
            word-break: break-all;
        }
        #map {
            height: 300px;
        }
    </style>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">View Country</h3>

            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.country-header-buttons-view')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel" style="overflow: hidden;">
                
                <table class="table table-striped table-hover">
                    @foreach($countrytrans as $key => $country_translation)
                        <tr> <th> <h3 style="color:#0A8F27">{{ $country_translation->translanguage->title }}</h3> </th><td></td> </tr>
                        <tr>
                            <th>Title <small>({{ $country_translation->translanguage->title }})</small></th>
                            <td>{{ $country_translation->title }}</td>
                        </tr>
                        <tr>
                            <th>Description <small>({{ $country_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$country_translation->description?></p></td>
                        </tr>
                        <tr>
                            <th>Nationality <small>({{ $country_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$country_translation->nationality?></p></td>
                        </tr>
                        <tr>
                            <th>Population <small>({{ $country_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$country_translation->population?></p></td>
                        </tr>
                        <tr>
                            <th>Cost of Living Index <small>({{ $country_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$country_translation->cost_of_living?></p></td>
                        </tr>
                        <tr>
                            <th>Crime Rate Index <small>({{ $country_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$country_translation->geo_stats?></p></td>
                        </tr>
                        <tr>
                            <th>Quality of Life Index <small>({{ $country_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$country_translation->demographics?></p></td>
                        </tr>
                        @endforeach

                        <?php
                            $month = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                        ?>
                        <tr>
                            <th>Best Time to Travel</th>
                            <td class="description">
                            @foreach ($countryabouts as $key => $country_about)
                                @if ($country_about->type == 'best_time')
                                    <?php
                                        $start_end = explode(",", $country_about->body);
                                        $title = str_replace('_', ' ', ucfirst($country_about->title));
                                    ?>
                                    @if (count($start_end) > 1)
                                        @foreach ($start_end as $item)
                                            <?php
                                                $item = explode("-", $item);
                                            ?>
                                        <p><b>{{$title}}:</b> {{$month[$item[0]]}}-{{$month[$item[1]]}}</p>
                                        @endforeach                                
                                    @else
                                        <?php
                                            $start_end = explode("-", $country_about->body);
                                        ?>
                                        <p><b>{{$title}}:</b> {{$month[$start_end[0]]}}-{{$month[$start_end[1]]}}</p>
                                    @endif
                                @endif
                            @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Restrictions </th>
                            <td class="description">
                            @foreach ($countryabouts as $country_about)
                                @if ($country_about['type'] == 'restrictions')
                                <p><b>{{ $country_about['title'] }}: </b>{{ $country_about['body'] }}</p>
                                @endif
                            @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Packing Tips </th>
                            <td class="description">
                            @foreach ($countryabouts as $country_about)
                                @if ($country_about['type'] == 'planning_tips')
                                <p><b>{{ $country_about['title'] }}: </b>{{ $country_about['body'] }}</p>
                                @endif
                            @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Health Notes </th>
                            <td class="description">
                            @foreach ($countryabouts as $country_about)
                                @if ($country_about['type'] == 'health_notes')
                                <p><b>{{ $country_about['title'] }}: </b>{{ $country_about['body'] }}</p>
                                @endif
                            @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Potential Dangers </th>
                            <td class="description">
                            @foreach ($countryabouts as $country_about)
                                @if ($country_about['type'] == 'potential_dangers')
                                <p><b>{{ $country_about['title'] }}: </b>{{ $country_about['body'] }}</p>
                                @endif
                            @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Speed Limit </th>
                            <td class="description">
                            @foreach ($countryabouts as $country_about)
                                @if ($country_about['type'] == 'speed_limit')
                                <p><b>{{ $country_about['title'] }}: </b>{{ $country_about['body'] }}</p>
                                @endif
                            @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Etiquette </th>
                            <td class="description">
                            @foreach ($countryabouts as $country_about)
                                @if ($country_about['type'] == 'etiquette')
                                <p><b>{{ $country_about['title'] }}: </b>{{ $country_about['body'] }}</p>
                                @endif
                            @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th> <h3 style="color:#0A8F27">Common Fields</h3></th><td></td>   
                        </tr>
                        <tr>
                            <th>Country Code </th>
                            @if(isset($country->code))
                                <td> <p><?=$country->code?></p> </td>
                            @else
                                <td> <p>No Country Code Added.</p> </td>
                            @endif
                        </tr>
                        <tr>
                            <th>Continent </th>
                            @if(isset($region->title))
                                <td> <p><?=$region->title?></p> </td>
                            @else
                                <td> <p>No Continent Added.</p> </td>
                            @endif
                        </tr>
                    <tr>
                        <th>Active </th>
                        <td>
                            @if($country->active == 1) 
                              <p><label class="label label-success">Active</label></p> 
                            @else
                              <p><label class="label label-danger">Deactive</label></p>
                            @endif
                        </td>
                    </tr>

                    <!-- Start: Airports -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Airports</h3></th><td></td>   
                    </tr>
                    @if(empty($airports))
                      <tr>
                          <th> <p>No Airports Added.</p> </th><td></td>
                      </tr>
                    @endif
                    @foreach($airports as $key => $airport)
                      <tr>
                          <th> <p><?=$airport?></p> </th><td></td>
                      </tr>
                    @endforeach
                    <!-- End: Airports -->

                    <!-- Start: Currencies -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Currencies</h3></th><td></td>   
                    </tr>
                    @if(empty($currencies))
                      <tr>
                          <th> <p>No Currencies Added.</p> </th><td></td>
                      </tr>
                    @endif
                    @foreach($currencies as $key => $currency)
                      <tr>
                          <th> <p><?=$currency?></p> </th><td></td>
                      </tr>
                    @endforeach
                    <!-- End: Currencies -->

                    <!-- Start: Capitals -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Capital Cities</h3></th><td></td>   
                    </tr>
                    @if(empty($capitals))
                      <tr>
                          <th> <p>No Capitals Added.</p> </th><td></td>
                      </tr>
                    @endif
                    @foreach($capitals as $key => $capital)
                      <tr>
                          <th> <p><?=$capital?></p> </th><td></td>
                      </tr>
                    @endforeach
                    <!-- End: Capitals -->

                    <!-- Start: EmergencyNumbers -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Emergency Numbers</h3></th><td></td>   
                    </tr>
                    @if(empty($emergencyNumbers))
                      <tr>
                          <th> <p>No Emergency Numbers Added.</p> </th><td></td>
                      </tr>
                    @endif
                    @foreach($emergencyNumbers as $key => $number)
                      <tr>
                          <th> <p><?=$number?></p> </th><td></td>
                      </tr>
                    @endforeach
                    <!-- End: EmergencyNumbers -->

                    <!-- Start: Holidays -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Holidays and date</h3></th><td></td>
                    </tr>
                    @if(empty($holidays))
                      <tr>
                          <th> <p>No Holidays Added.</p> </th><td></td>
                      </tr>
                    @endif
                    @foreach($holidays as $key => $holiday)
                      <tr>
                          <th> <p><?=$holiday?></p> </th>
                          <td>{{$holidaysDate[$key]['date']}}</td>
                      </tr>
                    @endforeach
                    <!-- End: Holidays -->

                    <!-- Start: Official Languages -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Official Languages</h3></th><td></td>   
                    </tr>
                    @if(empty($languagesSpoken))
                      <tr>
                          <th> <p>No Official Languages Added.</p> </th><td></td>
                      </tr>
                    @endif
                    @foreach($languagesSpoken as $key => $language_spoken)
                      <tr>
                          <th> <p><?=$language_spoken?></p> </th><td></td>
                      </tr>
                    @endforeach
                    <!-- End: Official Languages -->

                    <!-- Start: Additional Languages Spoken -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Additional Languages Spoken</h3></th><td></td>   
                    </tr>
                    @if(empty($additionalLanguagesSpoken))
                      <tr>
                          <th> <p>No Additional Languages Spoken Added.</p> </th><td></td>
                      </tr>
                    @endif
                    @foreach($additionalLanguagesSpoken as $key => $language_spoken)
                      <tr>
                          <th> <p><?=$language_spoken?></p> </th><td></td>
                      </tr>
                    @endforeach
                    <!-- End: Additional Languages Spoken -->

                    <!-- Start: Suitable Travel Styles -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Travel Styles</h3></th><td></td>
                    </tr>
                    @if(empty($lifestyles))
                      <tr>
                          <th> <p>No suitable travel styles added.</p> </th><td></td>
                      </tr>
                    @endif
                    @foreach($lifestyles as $key => $lifestyle)
                      <tr>
                          <th> <span><?=$lifestyle['title']?></span>&nbsp<span><?=$lifestyle['rating']?></span></th>
                      </tr>
                    @endforeach
                    <!-- End: Suitable Travel Styles -->

                    <!-- Start: Religions -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Religions</h3></th><td></td>   
                    </tr>
                    @if(empty($religions))
                      <tr>
                          <th> <p>No Religions Added.</p> </th><td></td>
                      </tr>
                    @endif
                    @foreach($religions as $key => $religion)
                      <tr>
                          <th> <p><?=$religion?></p> </th><td></td>
                      </tr>
                    @endforeach
                    <!-- End: Religions -->

                    <!-- Start: Medias -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Medias</h3></th><td></td>   
                    </tr>
                    @if(empty($medias))
                      <tr>
                          <th> <p>No Medias Added.</p> </th><td></td>
                      </tr>
                    @endif
                    @foreach($medias as $key => $media)
                      <tr>
                          <th> <p><?=$media?></p> </th><td></td>
                      </tr>
                    @endforeach
                    <!-- End: Medias -->

                    <!-- Start: Images -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Images</h3></th><td></td>   
                    </tr>
                </table>
                <div class="row">
                    @if(empty($medias_image))
                        <div style="padding-left: 20px;"><p>No Images Added.</p></div>
                    @else
                        @foreach($medias_image as $rowmedia)
                            @if( !empty($rowmedia->url) )
                                <div class="col-md-2">
                                    <a href="https://s3.amazonaws.com/travooo-images2/{{$rowmedia->url}}" target='_blank'>
                                        <img class="" src="https://s3.amazonaws.com/travooo-images2/{{$rowmedia->url}}"
                                             style="width:165px; height:150px;padding-top: 13px;"/>
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
                <!-- End: Medias -->
                <!-- Cover Image Section - Start -->
                <div class="row">
                    <h3 style="color:#0A8F27;padding-left: 26px;">Cover Image</h3>
                    @if(empty($cover))
                        <div style="padding-left: 20px;">No Cover Image Added.</div>
                    @else
                        <div style="padding-top: 10px;padding-left: 20px;"><img src="{{$cover['url']}}" style="width: 170px;height:150px;" /></div>
                    @endif
                </div>
                <!-- Cover Image Section - End -->
                <table class="table table-striped table-hover">
                    <tr>
                         <th> <h3 style="color:#0A8F27">Location</h3></th><td></td>   
                    </tr>
                    <tr>
                        <th>Latitude , Longitude</th>
                        <td> <p><?= $country->lat ?> , <?= $country->lng ?></p> </td>
                    </tr>
                </table>
                <!-- Map will be created in the "map" div -->
                <div id="map" data-lat="{{ $country->lat }}" data-lng="{{ $country->lng }}"></div>
                <!-- Map div end -->

            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
@endsection