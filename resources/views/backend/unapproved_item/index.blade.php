@extends ('backend.layouts.app')

@section('title', 'Unapproved Items Manager')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page-header')
<!--  <h1>
     {{ trans('labels.backend.access.users.management') }}
     <small>{{ trans('labels.backend.access.users.active') }}</small>
 </h1> -->
<h1>

    Unapproved Items Manager
    <small>Active Unapproved Items</small>
</h1>
@endsection

@section('content')
<div class="row deleted-box">
    <div class="col-md-12">
        <div class="deleted_msg"></div>
    </div>
</div>
<div class="box box-success">
    <div class="box-header with-border">
        <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3> -->
        <h3 class="box-title">Unapproved Items</h3>

        @include('backend.select-deselect-all-buttons')

        <ul class="list-inline">
            <li class="list-inline-item">
                <select id="moveToSelectedCountry" class="custom-filters form-control">
                    <option value="">Search Country</option>
                </select>
            </li>
            <li class="list-inline-item">
                <select id="moveToSelectedCity" class="custom-filters form-control">
                    <option value="">Search Country</option>
                </select>
            </li>
            <li class="list-inline-item">
                <button id="moveToCity" class="btn btn-warning" type="submit">
                    Move all selected places to a new city
                </button>
                <a href="{{route('admin.location.place.mass', ['action' => 'unapproved'])}}">Go to the mass operations page</a>
            </li>
            <li class="list-inline-item pull-right">
                <button id="massApprove" class="btn btn-xs btn-success"  type="button">
                    Mass Approve
                </button>
            </li>
        </ul>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div id="deleteLoadingUnapprovedItem" style="display: none">
            <i class="fa fa-spinner fa-spin" style="position:absolute;top:50%;left:50%;width:200px;margin-left:-100px;text-align:center;font-size: 50px;"></i>
        </div>
        <div class="table-responsive">
            <table
                    id="unapproved-item-table"
                    class="table table-condensed table-hover"
                    data-url="{{ route("admin.location.unapproved_item.table") }}"
                    data-del="{{ route("admin.location.place.delete_ajax") }}"
                    data-config="{{ config('locations.place_table') }}"
            >
                <thead>
                    <tr>
                        <th></th>
                        <th>id</th>
                        <th>Title</th>
                        <th>Address</th>
                        <th>Pluscode</th>
                        <th>City</th>
                        <th>Unapproved Item Type</th>
                        <th>Featured</th>
                        <th>Images</th>
                        <th>Active</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <input id="searchCity" type="hidden" value="{{ route("admin.location.place.cities") }}">
            <input id="UnapprovedItemType" type="hidden" value="{{ route("admin.location.place.types") }}">
            <input id="MassApproveItem" type="hidden" value="{{ route("admin.location.unapproved_item.mass_approve") }}">
        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->
@endsection

@section('after-scripts')
<style>
    table.dataTable tbody td.select-checkbox, table.dataTable tbody th.select-checkbox {
        width:20px !important;
    }
</style>
@endsection
