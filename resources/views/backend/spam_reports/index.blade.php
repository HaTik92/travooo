@extends ('backend.layouts.app')

@section('title', 'Spam reports')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page-header')
    <h1>
        Spam reports
        <small>Spam reports</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Spam reports</h3>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table
                        id="spamReportsTable"
                        class="table table-condensed table-hover"
                        data-url="{{ route("admin.location.spam_reports.table") }}">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Report text</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
