<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.religion.index', 'All Religions', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.religion.create', 'Create Religion', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>