@extends ('backend.layouts.app')

@section('title', 'Top Restaurants Manager')

@section('after-styles')
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page-header')
<!--  <h1>
     {{ trans('labels.backend.access.users.management') }}
     <small>{{ trans('labels.backend.access.users.active') }}</small>
 </h1> -->
<h1>
    Top Restaurants Manager
    <small>Active Restaurants</small>
</h1>
@endsection

@section('before-styles')
   <style>
        .city-order-column{
            width: 100%;
            display: inline-block;
            cursor: pointer;
        }

        .city-order-block{
            display: none;
        }
    </style>
@endsection

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3> -->
        <h3 class="box-title">Top Restaurants</h3>
            @include('backend.select-deselect-all-buttons')

        <div class="box-tools pull-right">
            @include('backend.top_places.partials.restaurants-header.header-buttons')
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div id="deleteLoadingPlace" style="display: none">
            <i class="fa fa-spinner fa-spin" style="position:absolute;top:50%;left:50%;width:200px;margin-left:-100px;text-align:center;font-size: 50px;"></i>
        </div>
        <div class="table-responsive">
            <table id="top_restaurant_table" class="table table-condensed table-hover"
                                    data-url="{{ route("admin.top_restaurants.table") }}"
                                    data-del="{{ route("admin.top_restaurants.delete_ajax") }}"
                                    data-config="places_top">
                <thead>
                    <tr>
                        <th></th>
                        <th>id</th>
                        <th>Place Id</th>
                        <th>Title</th>
                        <th>Address</th>
                        <th>Country</th>
                        <th>City</th>
                        <th>Place Type</th>
                        <th>Review Number</th>
                        <th>Rating</th>
                        <th>Order Number</th>
                        <th>Images</th>
                        <th>Active</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="top-place-body">
                </tbody>
            </table>
            <input id="placeCountry" type="hidden" value="{{ route("admin.top_restaurants.countries") }}">
            <input id="searchCity" type="hidden" value="{{ route("admin.top_restaurants.cities") }}">
            <input id="UpdateSorting" type="hidden" value="{{ route("admin.top_restaurants.update_sort") }}">

        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->
@endsection

@section('after-scripts')
<div id="sortModal" class="modal" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close btn_close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Update Order Number</h4>
        </div>
        <div class="modal-body">
            <input type="hidden" id="tp_id">

            <input type="number" id="sort_id" placeholder="1,2,3...." class="custom-filters form-control" style="    width: 230px;
            margin-bottom: 5px;
            margin-left: 100px;">
        </div>
        <div class="modal-footer">
            <button class="btn btn-success btn-xs update_sort"value="Update">Update</button>
            <button type="button" class="btn btn-xs btn_close" data-dismiss="modal">Close</button>
        </div>
      </div>
  
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<style>
    table.dataTable tbody td.select-checkbox, table.dataTable tbody th.select-checkbox {
        width:20px !important;
    }
</style>
@endsection
