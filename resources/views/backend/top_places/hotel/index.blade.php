@extends ('backend.layouts.app')

@section('title', 'Top Hotels Manager')

@section('after-styles')
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page-header')
<!--  <h1>
     {{ trans('labels.backend.access.users.management') }}
     <small>{{ trans('labels.backend.access.users.active') }}</small>
 </h1> -->
<h1>
    Top Hotels Manager
    <small>Active Hotels</small>
</h1>
@endsection

@section('before-styles')
   <style>
        .city-order-column{
            width: 100%;
            display: inline-block;
            cursor: pointer;
        }

        .city-order-block{
            display: none;
        }
    </style>
@endsection

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3> -->
        <h3 class="box-title">Top Hotels</h3>
            @include('backend.select-deselect-all-buttons')

        <div class="box-tools pull-right">
            @include('backend.top_places.partials.hotels-header.header-buttons')
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div id="deleteLoadingPlace" style="display: none">
            <i class="fa fa-spinner fa-spin" style="position:absolute;top:50%;left:50%;width:200px;margin-left:-100px;text-align:center;font-size: 50px;"></i>
        </div>
        <div class="table-responsive">
            <table id="top_hotel_table" class="table table-condensed table-hover"
                                    data-url="{{ route("admin.top_hotels.table") }}"
                                    data-del="{{ route("admin.top_hotels.delete_ajax") }}"
                                    data-config="places_top">
                <thead>
                    <tr>
                        <th></th>
                        <th>id</th>
                        <th>Place Id</th>
                        <th>Title</th>
                        <th>Address</th>
                        <th>Country</th>
                        <th>City</th>
                        <th>Place Type</th>
                        <th>Review Number</th>
                        <th>Rating</th>
                        <th>Order Number</th>
                        <th>Images</th>
                        <th>Active</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="top-place-body">
                </tbody>
            </table>
            <input id="hotelCountry" type="hidden" value="{{ route("admin.top_hotels.countries") }}">
            <input id="searchCity" type="hidden" value="{{ route("admin.top_hotels.cities") }}">
            <input id="UpdateSorting" type="hidden" value="{{ route("admin.top_hotels.update_sort") }}">

        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->
@endsection
