<?php

?>
@extends ('backend.layouts.app')

@section ('title', 'Top Hotels Manager' . ' | ' . 'Create Top Hotel')

@section('page-header')
    <!-- <h1>
        {{ trans('labels.backend.access.users.management') }}
            <small>{{ trans('labels.backend.access.users.create') }}</small>
    </h1> -->
    <h1>
        Top Hotels Manager
        <small>Create Top Hotel</small>
    </h1>
@endsection

@section('after-styles')
    <style>
        #map {
            height: 300px;
        }
        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 300px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        .pac-container {
            font-family: Roboto;
        }
        .set_img_col{display: block; width:200px;}
        .set_last_col{display: block; width: 54px;padding-left: 7px;}
        .add_icon{width: 40px; font-size: 19px;}
        #addMoreRows{border-top:0px;}
        #set_error_msg{color: red; font-weight: bold;}

        .alert-warning {
            display: none;
        }
    </style>

    <!-- Language Error Style: Start -->
    <style>
        .required_msg{
            padding-left: 20px;
        }
    </style>
    <!-- Language Error Style: End -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection

@section('content')
    {{ Form::open([
            'id'     => 'top_hotel_form',
            'route'  => 'admin.top_hotels.store',
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'post',
            'files'  => true
        ])
    }}
    <div class="box box-success">
        <div class="box-header with-border">
        <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.create') }}</h3> -->
            <h3 class="box-title">Create Top Hotel</h3>

        </div>
        <!-- /.box-header -->
        <!-- Language Error : Start -->
        <div class="row error-box">
            <div class="col-md-10">
                <div class="required_msg">
                </div>
            </div>
        </div>
        <div class="box-body">
                <!-- Cities: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Cities', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('cities_id', [] , null, ['id' => 'hotelCitiesSelect', 'class' => 'form-control', 'data-placeholder' => 'Choose City...', 'data-url' => url('admin/location/city'), 'required' => 'required']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Cities: End -->
            <!-- Cities: Start -->
            <div class="form-group">
                {{ Form::label('title', 'Hotel', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::select('place_id', [] , null, ['id' => 'topHotelSelect', 'class' => 'form-control', 'data-placeholder' => 'Choose Hotel...' , 'data-url' => url('home/searchAllPOIs'), 'required' => 'required']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
            <!-- Cities: End -->
        </div>
    </div>
    <div class="box box-info">
        <div class="box-body">
            <div class="pull-left">
                {{ link_to_route('admin.top_hotels.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs submit_button']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
    {{ Form::close() }}
@endsection
