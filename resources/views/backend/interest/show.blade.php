@extends ('backend.layouts.app')

@section ('title', 'Interest Management' . ' | ' . 'View Interest')

@section('page-header')
    <h1>
        Activity Interest
        <small>View Interest</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">View Activity Type</h3>

            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.interest-header-buttons-view')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div role="tabpanel">
                <table class="table table-striped table-hover">
                    @foreach($interesttrans as $key => $interest_translation)
                        <tr> <th> <h3 style="color:#0A8F27">{{ $interest_translation->translanguage->title }}</h3> </th><td></td> </tr>
                        <tr>
                            <th>Title <small>({{ $interest_translation->translanguage->title }})</small></th>
                            <td>{{ $interest_translation->title }}</td>
                        </tr>
                    @endforeach                    
                </table>
            </div><!--tab panel-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection