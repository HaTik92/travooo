<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.currencies.index', 'All Currencies', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.currencies.create', 'Create Currencies', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>