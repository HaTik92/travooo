@extends ('backend.layouts.app')

@section ('title', 'Currencies Management' . ' | ' . 'View Currencies')

@section('page-header')
    <h1>
        Currencies Management
        <small>View Currencies</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">View Currencies</h3>

            <div class="box-tools pull-right">
                @include('backend.currencies.partials.header-buttons-view')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">
                
                <table class="table table-striped table-hover">
                    @foreach($currenciestrans as $key => $currencies_translation)
                        <tr> <th> <h3 style="color:#0A8F27">{{ $currencies_translation->translanguage->title }}</h3> </th><td></td> </tr>
                        <tr>
                            <th>Title <small>({{ $currencies_translation->translanguage->title }})</small></th>
                            <td>{{ $currencies_translation->title }}</td>
                        </tr>
                    @endforeach
                    <tr> 
                        <th> <h3 style="color:#0A8F27"> Common Fields </h3> </th> 
                    </tr>
                    <tr>
                        <th>Active</th>
                        <td>
                            @if($currencies->active == 1)
                                <label class="label label-success">Active</label>
                            @else
                                <label class="label label-danger">Deactive</label>
                            @endif
                        </td>
                    </tr>                              
                </table>
            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
@endsection