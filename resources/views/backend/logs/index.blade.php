@extends ('backend.layouts.app')

@section('title', 'Rank Cron Log Manager')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
@endsection

@section('page-header')
    <h1>
    	Rank Cron Log Manager
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Rank Cron Logs</h3>

            <div class="box-tools pull-right">
                @include('backend.logs.partials.header-buttons')
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="rank-cron-table"
                       class="table table-condensed table-hover"
                       data-url="{{ route("admin.logs.rank-cron-table") }}"
                       data-truncate-url="{{ route('admin.logs.rank-cron-table-truncate') }}">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Cron Id</th>
                            <th>Post Type</th>
                            <th>Action</th>
                            <th>Error Message</th>
                            <th>Post Id</th>
                            <th>Loop Count</th>
                            <th>Rank Type</th>
                            <th>Start Time</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
