@extends ('backend.layouts.app')

@section('title', 'Places Manager')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page-header')
<!--  <h1>
     {{ trans('labels.backend.access.users.management') }}
     <small>{{ trans('labels.backend.access.users.active') }}</small>
 </h1> -->
<h1>
    Isolated Places
    <small>Deffered Places</small>
</h1>
@endsection

@section('content')
<div class="box box-success">
   

    <div class="box-body">
        <div id="deleteLoadingPlace" style="display: none">
            <i class="fa fa-spinner fa-spin" style="position:absolute;top:50%;left:50%;width:200px;margin-left:-100px;text-align:center;font-size: 50px;"></i>
        </div>
        <div class="table-responsive">
            <table id="isolated-table" class="table table-condensed table-hover"
                                    data-url="{{ route("admin.location.isolated.table") }}"
                                    data-del="{{ route("admin.location.place.delete_ajax") }}"
                                    data-config="{{ config('locations.place_table') }}">
                <thead>
                    <tr>
                        <th></th>
                        <th>id</th>
                        <th>Title</th>
                        <th>Address</th>
                        <th>Section</th>
                        <th>Pluscode</th>
                        <th>Country</th>
                        <th>City</th>
                        <th>Place Type</th>
                        <th>Featured</th>
                        <th>Images</th>
                        <th>Active</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <input id="placeCountry" type="hidden" value="{{ route("admin.location.place.countries") }}">
            <input id="searchCity" type="hidden" value="{{ route("admin.location.place.cities") }}">
            <input id="searchSection" type="hidden" value="{{ route("admin.location.place.sections") }}">
            <input id="PlaceType" type="hidden" value="{{ route("admin.location.place.types") }}">

        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->
@endsection

@section('after-scripts')
<style>
    table.dataTable tbody td.select-checkbox, table.dataTable tbody th.select-checkbox {
        width:20px !important;
    }
</style>
@endsection
