<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.hotels.index', 'All Hotels', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.hotels.create', 'Create Hotels', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>