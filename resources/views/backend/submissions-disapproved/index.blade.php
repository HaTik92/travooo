@extends ('backend.layouts.app')

@section('title', 'New Submissions')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
@endsection

@section('page-header')
   <!--  <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.active') }}</small>
    </h1> -->
    <h1>
    	Search Submissions
    	<small>Disapproved Submissions</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3> -->
            <h3 class="box-title">Submissions</h3>

            <div class="box-tools pull-right">
                {{-- @include('backend.pages.partials.header-buttons') --}}
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="submissions-disapproved-table" class="table table-condensed table-hover"
                                        data-url="{{ route("admin.submissions.disapproved.table") }}"
                                        data-status="1"
                                        data-config="{{config('submissions.submissions_table')}}">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>User ID</th>
                        <th>Place Name</th>
                        <th>Category</th>
                        <th>Hyperlink</th>
                        <th>City</th>
                        <th>Address</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        {{-- <th>Actions</th> --}}
                        {{-- <th>{{ trans('labels.general.actions') }}</th> --}}
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection
