@extends ('backend.layouts.app')

@section('title', 'Restaurants Manager')

@section('after-styles')
{{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
{{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
@endsection

@section('page-header')
   <!--  <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.active') }}</small>
    </h1> -->
    <h1>
    	{{ 'Restaurants Manager' }}
    	<small>{{ 'Restaurants' }}</small>
    </h1>
@endsection

@section('content')
    <div class="row deleted-box">
        <div class="col-md-12">
            <div class="deleted_msg"></div>
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header with-border">
            <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3> -->
            <h3 class="box-title">Restaurants</h3>
            @include('backend.select-deselect-all-buttons')
            <div class="box-tools pull-right">
                @include('backend.restaurants.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="restaurants-table" class="table table-condensed table-hover"
                                              data-url="{{ route("admin.restaurants.table") }}"
                                              data-del="{{ route("admin.restaurants.delete_ajax") }}"
                                              data-config="{{config('restaurants.restaurants_table')}}">
                    <thead>
                    <tr>
                        <th></th>
                        <th>id</th>
                        <th>Title</th>
                        <th>Address</th>
                        <th>Pluscode</th>
                        <th>Country</th>
                        <th>City</th>
                        <th>Place Type</th>
                        <th>Active</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
                <input id="placeCountry" type="hidden" value="{{ route("admin.restaurants.countries") }}">
                <input id="searchCity" type="hidden" value="{{ url('admin/location/city') }}">
                <input id="PlaceType" type="hidden" value="{{ route("admin.restaurants.types") }}">
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@section('after-scripts')
<style>
    #restaurants-table thead tr th:nth-child(10){
        display:none !important;
    }
    #restaurants-table tbody tr td:nth-child(10){
        display:none !important;
    }
    
    table.dataTable tbody td.select-checkbox, table.dataTable tbody th.select-checkbox {
        width:20px !important;
    }
</style>
@endsection
