<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.restaurants.index', 'All Restaurants', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.restaurants.create', 'Create Restaurants', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>