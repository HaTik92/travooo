@extends ('backend.layouts.app')

@section ('title', 'Restaurants Manager' . ' | ' . 'Import Restaurants')

@section('page-header')
<h1>
    <!-- {{ trans('labels.backend.access.users.management') }} -->
    Restaurants Management
    <small>Import Restaurants</small>
</h1>
@endsection

@section('after-styles')
<style>
    #gmap {
        height: 600px;
    }
    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-container {
        font-family: Roboto;
    }
</style>

<!-- Language Error Style: Start -->
<style>
    .required_msg{
        padding-left: 20px;
    }
</style>
<!-- Language Error Style: End -->

<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection

@section('content')
{{ Form::open([
            'id'    => 'User_form',
            'route' => 'admin.restaurants.search',
            'class' => 'form-horizontal',
            'role' => 'form',
            'method' => 'post',
            'files' => true
        ])
}}

<div class="box box-success">
    <div class="box-header with-border">
        <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.create') }}</h3> -->
        <h3 class="box-title">Import Restaurants</h3>

        <div class="box-tools pull-right">

        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <!-- Countries: Start -->
        <div class="form-group">
            {{ Form::label('title', 'Country', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::select('countries_id', array('0' => 'Please select ...') + $countries , null,['class' => 'select2Class form-control', 'id' => 'country_id', 'data-url' => url('admin/location/country/jsoncities')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
        <!-- Countries: End -->

        <!-- Cities: Start -->
        <div class="form-group">
            {{ Form::label('title', 'Cities', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::select('cities_id', array('0' => 'Please select ...') + $cities , null,['class' => 'select2Class form-control', 'id' => 'city_id']) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
        <!-- Cities: End -->
        <!-- Title: Start -->
        <div class="form-group">
            {{ Form::label('address', 'Address', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::text('address', null, ['class' => 'form-control', 'maxlength' => '255', 'autofocus' => 'autofocus', 'placeholder' => 'Enter city or location to search']) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
        <!-- Title: End -->

        <!-- Code: Start -->
        <div class="form-group">
            {{ Form::label('query', 'Query', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::textarea('query', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Enter comma delimited queries to search']) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
        <div class="form-group">
            {{ Form::label('latlng', 'Latitude, Longitude', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::text('latlng', null, ['class' => 'form-control', 'id' => 'latlng', 'maxlength' => '255', 'autofocus' => 'autofocus', 'placeholder' => 'Enter Latitude and Lonitude']) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('map', 'Map', ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">

                <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                <div id="gmap" data-search="{{ route('admin.restaurants.searchhistory') }}"></div>


            </div><!--col-lg-10-->
        </div><!--form control-->
        <!-- Code: End -->
    </div><!-- /.box-body -->
</div><!--box-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.restaurants.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit('Search', ['class' => 'btn btn-success btn-xs submit_button']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->

{{ Form::close() }}
@endsection