<?php
use App\Models\Access\language\Languages;
// $languages = DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
?>

@extends ('backend.layouts.app')

@section ('title', 'Restaurants Manager' . ' | ' . 'Edit Restaurant')

@section('page-header')
    <h1>
        <!-- {{ trans('labels.backend.access.users.management') }} -->
        Restaurants Management
        <small>Edit Restaurant</small>
    </h1>
@endsection

@section('after-styles')
    <style>
        #map {
            height: 300px;
        }
        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 300px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        .pac-container {
            font-family: Roboto;
        }

        .add_icon {
            width: 40px;
            font-size: 19px;
        }

        .alert-warning {
            display: none;
        }
    </style>
    <!-- Language Error Style: Start -->
    <style>
        .required_msg{
            padding-left: 20px;
        }
    </style>
    <!-- Language Error Style: End -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection

@section('content')
    {{ Form::model($restaurant, [
            'id'     => 'restaurants_update_form',
            'route'  => ['admin.restaurants.update', $restaurantid],
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'PATCH',
            'files'  => 'true'
        ]) 
    }}

        <div class="box box-success">
            <div class="box-header with-border">
                <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.create') }}</h3> -->
                <h3 class="box-title">Edit Restaurants</h3>

            </div><!-- /.box-header -->

            <!-- Language Error : Start -->
            <div class="row error-box">
                <div class="col-md-10">
                    <div class="required_msg">
                    </div>
                </div>
            </div>
            <!-- Language Error : End -->

            <div class="box-body">
                @if(!empty($languages))
                    <ul class="nav nav-tabs">
                    @foreach($languages as $language)
                        <li class="{{ ($language->code == 'en')? 'active':'' }}">
                            <a data-toggle="tab" href="#{{$language->code}}">{{ $language->title }}</a>
                        </li>
                    @endforeach
                    </ul>
                    <div class="tab-content">
                    @foreach($data['translations'] as $translation)
                        @if($translation->translanguage->active == 1)
                        <div id="{{$translation->translanguage->code }}" class="tab-pane fade in {{ ($translation->translanguage->code == 'en')? 'active':'' }}">
                            <br />
                            {{ Form::hidden('translations['.$translation->translanguage->code.'][id]', $translation->id , []) }}
                            {{ Form::hidden('translations['.$translation->translanguage->code.'][languages_id]', $translation->translanguage->id , []) }}
                            <!-- Start Title -->
                            <div class="form-group">
                                {{ Form::label('translations['.$translation->translanguage->code.'][title]', 'Title', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$translation->translanguage->code.'][title]', $translation->title, ['class' => 'form-control required', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => 'Title']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Title -->
                            <!-- Start: Description -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][description]', 'Description', ['class' => 'col-lg-2 control-label description_input']) }}
                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][description]', $translation->description, ['class' => 'form-control description_input description', 'placeholder' => 'Description']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Description -->
                            <!-- Start: working_days -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][working_days]', 'Working Days', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][working_days]', $translation->working_days, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Working Days']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: working_days -->
                            <!-- Start: working_times -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][working_times]', 'Working Times', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][working_times]', $translation->working_times, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Working Times']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: working_times -->
                            <!-- Start: how_to_go -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][how_to_go]', 'How To Go', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][how_to_go]', $translation->how_to_go, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'How To Go']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: how_to_go -->
                            <!-- Start: when_to_go -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][when_to_go]', 'When To Go', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][when_to_go]', $translation->when_to_go, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'When To Go']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: when_to_go -->
                            <!-- Start: Price Level -->
                            <div class="form-group">
                                {{ Form::label('translations['.$translation->translanguage->code.'][price_level]', 'Price Level', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-lg-10">
                                    {{ Form::input('number','translations['.$translation->translanguage->code.'][price_level]', $translation->price_level, ['class' => 'form-control', 'placeholder' => 'Price Level']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Price Level -->
                            <!-- Start: num_activities -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][num_activities]', 'Num Activities', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][num_activities]', $translation->num_activities, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Num Activities']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: num_activities -->
                            <!-- Start: popularity -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][popularity]', 'Popularity', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][popularity]', $translation->popularity, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Popularity']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: popularity -->
                            <!-- Start: conditions -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][conditions]', 'Conditions', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][conditions]', $translation->conditions, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Conditions']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: conditions -->
                            <!-- Languages Tabs: Start -->
                        </div>
                        @endif
                    @endforeach
                    </div>

                    <!-- Active: Start -->
                    <div class="form-group">
                        {{ Form::label('title', trans('validation.attributes.backend.access.languages.active'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            @if($data['active'] == 1)
                                {{ Form::checkbox('active', $data['active'] , true) }}
                            @else
                                {{ Form::checkbox('active', $data['active'] , false) }}
                            @endif
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- Active: End -->
                    
                    <!-- Country: Start -->
                    <div class="form-group">
                        {{ Form::label('title', 'Country', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::select('countries_id', isset($countries) ? $countries : [], isset($data['countries_id']) ? $data['countries_id'] : [],['class' => 'select2Class form-control country-input', 'id' => 'country_id', 'data-url' => url('admin/location/country/jsoncities')]) }}
                            <input id="countryInfo" class="form-control" type="hidden" value="{{ json_encode($countries_location) }}">
                            <input id="restaurantInfo" class="form-control" type="hidden" value="{{ json_encode($restaurant) }}">
                            <input type="hidden" id="getCities" value="{{ url('admin/location/country/jsoncities')}}">
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- Country: End -->

                    <!-- City: Start -->
                    <div class="form-group">
                        {{ Form::label('title', 'City', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            <h5 style="color: orange;">Please note that searched city is depend on selected country</h5>
                            {{ Form::select('cities_id', isset($cities) ? $cities : [], isset($data['cities_id']) ? $data['cities_id'] : [], ['class' => 'select2Class form-control', 'id' => 'city_id', 'required' => 'required', 'data-url' => url('admin/location/city')]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- City: End -->

                    <!-- Place: Start -->
                    <div class="form-group">
                        {{ Form::label('title', 'Place', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::select('places_id', isset($data['place']) ? $data['place'] : [], isset($data['places_id']) ? $data['places_id'] : [],['class' => 'select2Class form-control', 'id' => 'place_search', 'required' => 'required', 'data-url' => url('admin/location/place')]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- Place: End -->

                    <!-- Medias: Start -->
                    {{--<div class="form-group">--}}
                        {{--{{ Form::label('title', 'Medias', ['class' => 'col-lg-2 control-label']) }}--}}

                        {{--<div class="col-lg-10">--}}
{{--                            {{ Form::select('medias_id[]', $medias , $data['selected_medias'] ,['class' => 'select2Class form-control' , 'multiple' => 'multiple']) }}--}}
                            {{--{{ Form::select('medias_id[]', $data['selected_medias'] , $data['selected_medias_ids'], ['class' => 'form-control' , 'id' => 'selected_medias', 'multiple' => 'multiple']) }}--}}
                        {{--</div><!--col-lg-10-->--}}
                    {{--</div><!--form control-->--}}
                    {{--<!-- Medias: End -->--}}

                    <div class="form-group">
                    {{ Form::label('title', 'Select Location', ['class' => 'col-lg-2 control-label']) }}
                        <div class="col-lg-10">
                            <input id="pac-input" class="form-control" type="text" placeholder="Search Box" >
                            <div id="map"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('title', 'Lat,Lng', ['class' => 'col-lg-2 control-label']) }}
                        <div class="col-lg-10">
                            
                            {{ Form::hidden('lat_lng', $data['lat_lng'], ['class' => 'form-control disabled', 'id' => 'lat-lng-input', 'required' => 'required', 'placeholder' => 'Lat,Lng']) }}

                            {{ Form::text('lat_lng_show', $data['lat_lng'], ['class' => 'form-control disabled', 'id' => 'lat-lng-input_show', 'required' => 'required', 'placeholder' => 'Lat,Lng' , 'disabled' => 'disabled']) }}
                        </div>
                    </div>
                    <!-- Images: Start -->
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="col-xs-12 alert alert-warning alert-dismissible fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Warning!</strong> <span class="set_error_msg"></span>
                            </div>
                        </div>
                        {{ Form::label('title', 'Upload Images', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Select File</th>
                                    <th>Title</th>
                                    <th>Author Name</th>
                                    <th>Author Link</th>
                                    <th>Source Link</th>
                                    <th>License Name</th>
                                    <th>License Link</th>
                                    <th>Delete</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td class="set_img_col">{{ Form::file('upload_img',[ 'name' => 'license_images[-1][image]']) }}</td>
                                    <td>{{ Form::text('license_images[-1][title]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[-1][author_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[-1][author_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[-1][source_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[-1][license_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[-1][license_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>

                                    </td>
                                    <td><a href="#javascript" id="add_icon" class="btn btn-success btn-xs add_icon">+</a>
                                        <input type="hidden" id="cnt" value="-1" />
                                    </td>
                                </tr>
                                @if( !empty($media_results) )
                                    @foreach( $media_results as $rowmedia)

                                        <tr>
                                            <td class="set_img_col">
                                                @if( !empty($rowmedia->url) )
                                                    <div class="col-md-2" id="set_delete{{$rowmedia->id}}">
                                                        <a href="https://s3.amazonaws.com/travooo-images2/{{$rowmedia->url}}"
                                                           target='_blank'>
                                                            <img class=""
                                                                 src="https://s3.amazonaws.com/travooo-images2/{{$rowmedia->url}}"
                                                                 width="100" height="100"/>
                                                        </a>
                                                    </div>
                                                @endif
                                            </td>
                                            <td>{{ Form::text("license_images[".$rowmedia['id']."][title]", $rowmedia['title'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                            <td>{{ Form::text("license_images[".$rowmedia['id']."][author_name]", $rowmedia['author_name'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                            <td>{{ Form::text("license_images[".$rowmedia['id']."][author_url]", $rowmedia['author_url'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                            <td>{{ Form::text("license_images[".$rowmedia['id']."][source_url]", $rowmedia['source_url'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                            <td>{{ Form::text("license_images[".$rowmedia['id']."][license_name]", $rowmedia['license_name'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                            <td>{{ Form::text("license_images[".$rowmedia['id']."][license_url]", $rowmedia['license_url'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                            <td><input type="checkbox" name="del[]" value="{{$rowmedia['id']}}" /></td>
                                            <td><a href="#javascript" id="add_icon" class="btn btn-success btn-xs add_icon">+</a>
                                            </td>
                                        </tr>

                                    @endforeach
                                @endif
                                <tbody id="addMoreRows"></tbody>
                            </table>
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- Images: End -->
                @endif
                <!-- Languages Tabs: End -->
            </div>
        </div>

        <div class="box box-info">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.restaurants.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs submit_button']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@endsection