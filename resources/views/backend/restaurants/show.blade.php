@extends ('backend.layouts.app')

@section ('title', 'Restaurants Management' . ' | ' . 'View Restaurant')

@section('page-header')
    <h1>
        Restaurants Management
        <small>View Restaurant</small>
    </h1>
@endsection

@section('after-styles')
    <style type="text/css">
        td.description {
            word-break: break-all;
        }
        #map {
            height: 300px;
        }
    </style>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">View Restaurant</h3>

            <div class="box-tools pull-right">
                @include('backend.restaurants.partials.header-buttons-view')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">

                <table class="table table-striped table-hover">
                    @foreach($restaurant_trans as $key => $restaurants_translation)
                        <tr> <th> <h3 style="color:#0A8F27">{{ $restaurants_translation->translanguage->title }}</h3> </th><td></td> </tr>
                        <tr>
                            <th>Title <small>({{ $restaurants_translation->translanguage->title }})</small></th>
                            <td>{{ $restaurants_translation->title }}</td>
                        </tr>
                        <tr>
                            <th>Description <small>({{ $restaurants_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$restaurants_translation->description?></p></td>
                        </tr>
                        <tr>
                            <th>Working Days <small>({{ $restaurants_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$restaurants_translation->working_days?></p></td>
                        </tr>
                        <tr>
                            <th>Working Times <small>({{ $restaurants_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$restaurants_translation->working_times?></p></td>
                        </tr>
                        <tr>
                            <th>How To Go  <small>({{ $restaurants_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$restaurants_translation->how_to_go?></p></td>
                        </tr>
                        <tr>
                            <th>When To Go <small>({{ $restaurants_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$restaurants_translation->when_to_go ?></p></td>
                        </tr>
                        <tr>
                            <th>Price Level <small>({{ $restaurants_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$restaurants_translation->price_level ?></p></td>
                        </tr>
                        <tr>
                            <th>Number of Activities <small>({{ $restaurants_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$restaurants_translation->num_activities ?></p></td>
                        </tr>
                        <tr>
                            <th>Popularity <small>({{ $restaurants_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$restaurants_translation->popularity ?></p></td>
                        </tr>
                        <tr>
                            <th>Conditions <small>({{ $restaurants_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$restaurants_translation->conditions ?></p></td>
                        </tr>
                    @endforeach
                    <tr>
                         <th> <h3 style="color:#0A8F27">Common Fields</h3></th><td></td>   
                    </tr>
                    <tr>
                        <th>Country </th>
                        <td> <p><?=$country->title?></p> </td>
                    </tr>
                    <tr>
                        <th>City </th>
                        <td> <p><?=$city->title?></p> </td>
                    </tr>
                    <tr>
                        <th>Place </th>
                        @if(!empty($place->title))
                            <td><p><?=$place->title?></p></td>
                        @else
                            <td><p>-</p></td>
                        @endif
                    </tr>
                    <tr>
                        <th>Active </th>
                        <td>
                            @if($restaurant->active == 1)
                              <p><label class="label label-success">Active</label></p> 
                            @else
                              <p><label class="label label-danger">Deactive</label></p>
                            @endif
                        </td>
                    </tr>
                    
                    <!-- Start: Medias -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Medias</h3></th><td></td>   
                    </tr>
                    @if(empty($medias))
                      <tr>
                          <th> <p>No Medias Added.</p> </th>
                      </tr>
                    @endif
                    @foreach($medias as $key => $media)
                      <tr>
                          <th> <p><?=$media?></p> </th>
                      </tr>
                    @endforeach
                    <!-- End: Medias -->
                    
                    <tr>
                         <th> <h3 style="color:#0A8F27">Restaurant Location</h3></th><td></td>   
                    </tr>
                    <tr>
                        <th>Latitude , Longitude</th>
                        <td> <p><?=$restaurant->lat?> , <?=$restaurant->lng?></p> </td>
                    </tr>
                </table>
                <!-- Map will be created in the "map" div -->
                <div id="map"></div>
                <input id="restaurantInfo" class="form-control" type="hidden" value="{{ json_encode($restaurant) }}">
                <!-- Map div end -->
            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
@endsection