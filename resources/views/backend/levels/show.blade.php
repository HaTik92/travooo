@extends ('backend.layouts.app')

@section ('title', 'Levels Management' . ' | ' . 'View Level')

@section('page-header')
    <h1>
        Levels Management
        <small>View Level</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">View Level</h3>

            <div class="box-tools pull-right">
                @include('backend.levels.partials.header-buttons-view')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div role="tabpanel">
                <table class="table table-striped table-hover">
                    @foreach($levelstrans as $key => $levels_translation)
                        <tr> <th> <h3 style="color:#0A8F27">{{ $levels_translation->translanguage->title }}</h3> </th><td></td> </tr>
                        <tr>
                            <th>Title <small>({{ $levels_translation->translanguage->title }})</small></th>
                            <td>{{ $levels_translation->title }}</td>
                        </tr>
                    @endforeach                    
                </table>
            </div><!--tab panel-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection