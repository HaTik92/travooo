@extends ('backend.layouts.app')

@section ('title', 'Region Manager')

@section('after-styles')
{{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
{{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
@endsection

@section('page-header')
    <h1>
    	Region Manager
    	<small>All Regions</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3> -->
            <h3 class="box-title">Regions</h3>
            @include('backend.select-deselect-all-buttons')
            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.regions-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div id="deleteLoadingRegions" style="display: none">
                <i class="fa fa-spinner fa-spin" style="position:absolute;top:50%;left:50%;width:200px;margin-left:-100px;text-align:center;font-size: 50px;"></i>
            </div>
            <div class="table-responsive">
                <table id="regions-table" class="table table-condensed table-hover"
                                          data-url="{{ route("admin.location.regions.table") }}"
                                          data-del="{{ route("admin.location.regions.delete_ajax") }}"
                                          data-config="{{config('locations.regions_table')}}">
                    <thead>
                    <tr>
                        <th></th>
                        <th>id</th>
                        <th>Title</th>
                        <th>Active</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection
