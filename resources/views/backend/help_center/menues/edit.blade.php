
@extends ('backend.layouts.app')

@section ('title', $page_title)

@section('page-header')
    <!-- <h1>
        {{-- {{ trans('labels.backend.access.users.management') }} --}}
    </h1> -->
    <h1>
        {{$page_title}}
    </h1>
@endsection

@section('after-styles')
    <!-- Language Error Style: Start -->
    <style>
        .required_msg{
            padding-left: 20px;
        }
    </style>
    <!-- Language Error Style: End -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection

@section('content')
    {{ Form::model($menu,[
            'id'     => 'pages_form',
            'route'  => ['admin.help-center.menu.update', $menu->id],
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'POST',
        ]) 
    }}
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Menu</h3>

            </div><!-- /.box-header -->
            <!-- Language Error : Start -->
            <div class="row error-box">
                <div class="col-md-10">
                    <div class="required_msg">
                    </div>
                </div>
            </div>
            <!-- Language Error : End -->
            <div class="box-body">
                @if(!empty($languages))
                    <ul class="nav nav-tabs">
                    @foreach($languages as $language)
                        <li class="{{ ($languages[0]->id == $language->id)? 'active':'' }}">
                            <a data-toggle="tab" href="#{{$language->code}}">{{ $language->title }}</a>
                        </li>
                    @endforeach
                    </ul>

                    <div class="tab-content">
                    @foreach($languages as $language)
                        <div id="{{ $language->code }}" class="tab-pane fade in {{ ($languages[0]->id == $language->id)? 'active':'' }}">
                            <br />
                            <!-- Start Title -->
                            <div class="form-group">
                                {{ Form::label('title_'.$language->id, 'Title', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('title', $menu->name, ['class' => 'form-control required', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => 'Title']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                        </div>
                    @endforeach
                    </div>

                    <!-- Active: Start -->
                    {{-- <div class="form-group"> --}}
                        {{-- {{ Form::label('title', trans('validation.attributes.backend.access.languages.active'), ['class' => 'col-lg-2 control-label']) }} --}}

                        {{-- <div class="col-lg-10">
                            @if($menu['active'] == CommonPage::ACTIVE)
                                {{ Form::checkbox('active', '1', true) }}
                            @else
                                {{ Form::checkbox('active', '2', false) }}
                            @endif
                        </div><!--col-lg-10--> --}}
                    {{-- </div><!--form control--> --}}
                    <!-- Active: End -->
                @endif
                <!-- Languages Tabs: End -->
            </div>
        </div>
        <div class="box box-info">
            <div class="box-body">
                <div class="pull-left">
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs submit_button']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}

    
@endsection

@section('after-scripts')
    <script>
        $(document).ready(function() {
            $('.description').summernote({
                placeholder: 'Page description...',
                tabsize: 4,
                height: 500,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear','strikethrough', 'superscript', 'subscript']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link']],
                    ['view', ['fullscreen', 'codeview', 'help']],
                ]
            });
        });
    </script>
@endsection