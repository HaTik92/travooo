
@extends ('backend.layouts.app')

@section ('title', $page_title)

@section('page-header')
    <!-- <h1>
        {{-- {{ trans('labels.backend.access.users.management') }} --}}
    </h1> -->
    <h1>
        {{$page_title}}
    </h1>
@endsection

@section('after-styles')
    <!-- Language Error Style: Start -->
    <style>
        .required_msg{
            padding-left: 20px;
        }
        .float-right {
            float: right;
        }
        .w-25 {
            width: 20%;
        }
        .w-100 {
            width: 100%;
        }
        .deletecontent {
            
        }
        .content-box {
            
        }
    </style>
    <!-- Language Error Style: End -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection

@section('content')
    {{ Form::model($subMenu,[
            'id'     => 'pages_form',
            'route'  => ['admin.help-center.subMenu.update', $subMenu->id],
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'POST',
        ]) 
    }}
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Sub Menu Content</h3>

            </div><!-- /.box-header -->
            <!-- Language Error : Start -->
            <div class="row error-box">
                <div class="col-md-10">
                    <div class="required_msg">
                    </div>
                </div>
            </div>
            <!-- Language Error : End -->
            <div class="box-body">
                @if(!empty($languages))
                    <ul class="nav nav-tabs">
                    @foreach($languages as $language)
                        <li class="{{ ($languages[0]->id == $language->id)? 'active':'' }}">
                            <a data-toggle="tab" href="#{{$language->code}}">{{ $language->title }}</a>
                        </li>
                    @endforeach
                    </ul>

                    <div class="tab-content">
                    @foreach($languages as $language)
                        <div id="{{ $language->code }}" class="tab-pane fade in {{ ($languages[0]->id == $language->id)? 'active':'' }}">
                            <br />
                            
                            <!-- Start Title -->
                            <div class="form-group content_forms">
                                {{ Form::label('title_'.$language->id, 'Title', ['class' => 'col-lg-2 control-label']) }}
                                
                                <div class="col-lg-10 mb-5">
                                    {{ Form::text('title', $subMenu->title, ['class' => 'form-control required', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => '']) }}
                                </div><!--col-lg-10-->
                                
                                {{ Form::label('title_'.$language->id, 'Categories', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-lg-10 mb-5">
                                    @php
                                        $items = $categories->pluck('name', 'id');
                                        @endphp
                                    {{ Form::select('category', $items, $subMenu->category_id, ['class' => 'col-lg-2 control-label']) }}
                                </div><!--col-lg-10-->
                                
                                @foreach ($subMenu->content as $content)
                                    <div class="content-box">
                                        <div class="d-flex text-right mr-5 pr-5">
                                            <button class="deletecontent btn btn-light mr-5" data-id="{{$content->id}}">
                                                <img src="{{url('assets2/image/icons/delete-content.svg')}}" width="30" alt="">
                                            </button>
                                        </div>
                                        {{ Form::label('title_'.$language->id, 'Position', ['class' => 'col-lg-2 control-label']) }}
                                        <div class="col-lg-10 mb-5">
                                            {{ Form::number('position_'.$content->id, $content->position, ['class' => 'w-25 form-control required', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => '']) }}
                                        </div><!--col-lg-10-->

                                        @if ($content->type == "content")
                                            {{ Form::label('title_'.$language->id, 'Content', ['class' => 'col-lg-2 control-label']) }}

                                            <div class="col-lg-10">
                                                {{ Form::textarea('content_'.$content->id, $content->content, ['class' => 'form-control required description', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => '']) }}
                                            </div><!--col-lg-10-->                                        
                                        @elseif($content->type == "device_faq")
                                            {{ Form::label('title_'.$language->id, 'Desktop FAQ', ['class' => 'col-lg-2 control-label']) }}

                                            <div class="col-lg-10">
                                                {{ Form::textarea('desktop_faq_'.$content->id, $content->desktop_faq, ['class' => 'form-control required description', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => '']) }}
                                            </div><!--col-lg-10-->


                                            {{ Form::label('title_'.$language->id, 'Mobile FAQ', ['class' => 'col-lg-2 control-label']) }}

                                            <div class="col-lg-10">
                                                {{ Form::textarea('mobile_faq_'.$content->id, $content->mobile_faq, ['class' => 'form-control required description', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => '']) }}
                                            </div><!--col-lg-10-->
                                        @elseif($content->type == "media") 
                                            {{ Form::label('title_'.$language->id, 'Media', ['class' => 'col-lg-2 control-label']) }}
                                            {{ Form::file('upload_img',[ 'name' => 'image', 'class' => 'upload_media', 'data-id' => $content->id]) }}
                                            @if ($content->media_url)
                                                <div class="col-lg-10" id="media_{{$content->id}}">
                                                    <img class="w-100" src="{{$content->media_url}}" alt="">
                                                </div>
                                            @endif
                                        @endif
                                        
                                    </div>                                    
                                @endforeach
                                    
                                
                            </div><!--form control-->
                            <div class="float-right">
                                <button class="btn btn-success add_content">Add Content</button>
                                <button class="btn btn-success add_device">Add Device FAQ</button>
                                <button class="btn btn-success add_media">Add Media</button>
                            </div>
                        </div>
                    @endforeach
                    </div>

                    <!-- Active: Start -->
                    {{-- <div class="form-group"> --}}
                        {{-- {{ Form::label('title', trans('validation.attributes.backend.access.languages.active'), ['class' => 'col-lg-2 control-label']) }} --}}

                        {{-- <div class="col-lg-10">
                            @if($subMenu['active'] == CommonPage::ACTIVE)
                                {{ Form::checkbox('active', '1', true) }}
                            @else
                                {{ Form::checkbox('active', '2', false) }}
                            @endif
                        </div><!--col-lg-10--> --}}
                    {{-- </div><!--form control--> --}}
                    <!-- Active: End -->
                @endif
                <!-- Languages Tabs: End -->
            </div>
        </div>
        <div class="box box-info">
            <div class="box-body">
                <div class="pull-left">
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs submit_button']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}

    
@endsection

@section('after-scripts')
    <script>
        $(document).ready(function() {
            $('.description').summernote({
                placeholder: 'Page description...',
                tabsize: 4,
                height: 500,
                toolbar: [
                    ['style', ['style']],
                    ['fontsize', ['fontsize']],
                    ['font', ['bold', 'underline', 'clear','strikethrough', 'superscript', 'subscript']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link']],
                    ['view', ['fullscreen', 'codeview', 'help']],
                ]
            });


            $(".add_content").click(function (e) { 
                e.preventDefault()
                $.ajax({
                    type: "post",
                    url: "{{route('admin.help-center.subMenu.new.content', 'content')}}",
                    data: {
                        submenu_id: "{{$subMenu->id}}"
                    },
                    success: function (content) {
                        console.log(content)
                        $(".content_forms").append(`
                            <div class="content-box">
                                <div class="d-flex text-right mr-5 pr-5">
                                    <button class="deletecontent btn btn-light mr-5" data-id="${content.id}">
                                        <img src="{{url('assets2/image/icons/delete-content.svg')}}" width="30" alt="">
                                    </button>
                                </div>
                                <label  class="col-lg-2 control-label">Position</label>
                                <div class="col-lg-10 mb-5">
                                    <input class="w-25 form-control required" type="number" required="required" autofocus="autofocus" placeholder="" name="position_${content.id}"" value="${content.position}">
                                </div>
                                <label  class="col-lg-2 control-label">Content</label>
                                <div class="col-lg-10">
                                    <textarea class="form-control required description" maxlength="191" required="required" autofocus="autofocus" placeholder="" name="content_${content.id}" cols="50" rows="10">${content.content}</textarea>
                                </div>
                            </div>
                        `);
                        $('.description').summernote({
                            placeholder: 'Page description...',
                            tabsize: 4,
                            height: 500,
                            toolbar: [
                                ['style', ['style']],
                                ['fontsize', ['fontsize']],
                                ['font', ['bold', 'underline', 'clear','strikethrough', 'superscript', 'subscript']],
                                ['fontname', ['fontname']],
                                ['color', ['color']],
                                ['para', ['ul', 'ol', 'paragraph']],
                                ['table', ['table']],
                                ['insert', ['link']],
                                ['view', ['fullscreen', 'codeview', 'help']],
                            ]
                        });
                    }
                });
            });


            $(".add_device").click(function (e) { 
                e.preventDefault()
                $.ajax({
                    type: "post",
                    url: "{{route('admin.help-center.subMenu.new.content', 'device_faq')}}",
                    data: {
                        submenu_id: "{{$subMenu->id}}"
                    },
                })
                .done(function(content) {
                    $(".content_forms").append(`
                        <div class="content-box">
                            <div class="d-flex text-right mr-5 pr-5">
                                <button class="deletecontent btn btn-light mr-5" data-id="${content.id}">
                                    <img src="{{url('assets2/image/icons/delete-content.svg')}}" width="30" alt="">
                                </button>
                            </div>

                            <label class="col-lg-2 control-label">Position</label>
                            <div class="col-lg-10 mb-5">
                                <input class="w-25 form-control required" type="number" required="required" autofocus="autofocus" placeholder="" name="position_${content.id}"" value="${content.position}">
                            </div>

                            <label class="col-lg-2 control-label">Desktop FAQ</label>
                            <div class="col-lg-10">
                                <textarea class="form-control required description" maxlength="10000" required="required" autofocus="autofocus" placeholder="" name="desktop_faq_${content.id}" cols="50" rows="10">${content.desktop_faq}</textarea>
                            </div>

                            <label class="col-lg-2 control-label">Mobile FAQ</label>
                            <div class="col-lg-10">
                                <textarea class="form-control required description" maxlength="10000" required="required" autofocus="autofocus" placeholder="" name="mobile_faq_${content.id}" cols="50" rows="10">${content.mobile_faq}</textarea>
                            </div>
                        </div>
                    `);
                    $('.description').summernote({
                        placeholder: 'Page description...',
                        tabsize: 4,
                        height: 500,
                        toolbar: [
                            ['style', ['style']],
                            ['fontsize', ['fontsize']],
                            ['font', ['bold', 'underline', 'clear','strikethrough', 'superscript', 'subscript']],
                            ['fontname', ['fontname']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['table', ['table']],
                            ['insert', ['link']],
                            ['view', ['fullscreen', 'codeview', 'help']],
                        ]
                    });
                })
                .fail(function(response) {
                    alert(response.responseJSON.message)
                });
            });


            $(".add_media").click(function (e) { 
                e.preventDefault()
                $.ajax({
                    type: "post",
                    url: "{{route('admin.help-center.subMenu.new.content', 'media')}}",
                    data: {
                        submenu_id: "{{$subMenu->id}}"
                    },
                })
                .done(function(content) {
                    $(".content_forms").append(`
                        <div class="content-box">
                            <div class="d-flex text-right mr-5 pr-5">
                                <button class="deletecontent btn btn-light mr-5" data-id="${content.id}">
                                    <img src="{{url('assets2/image/icons/delete-content.svg')}}" width="30" alt="">
                                </button>
                            </div>
                            
                            <label class="col-lg-2 control-label">Position</label>
                            <div class="col-lg-10 mb-5">
                                <input class="w-25 form-control required" type="number" required="required" autofocus="autofocus" placeholder="" name="position_${content.id}"" value="${content.position}">
                            </div>

                            <label for="image" class="col-lg-2 control-label">Media</label>
                                <input name="image" class="upload_media" data-id="${content.id}" type="file">
                                <div class="col-lg-10" id="media_${content.id}">
                                    <img class="w-100" src="" alt="">
                                </div>
                        </div>
                    `);
                    $('.description').summernote({
                        placeholder: 'Page description...',
                        tabsize: 4,
                        height: 500,
                        toolbar: [
                            ['style', ['style']],
                            ['fontsize', ['fontsize']],
                            ['font', ['bold', 'underline', 'clear','strikethrough', 'superscript', 'subscript']],
                            ['fontname', ['fontname']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['table', ['table']],
                            ['insert', ['link']],
                            ['view', ['fullscreen', 'codeview', 'help']],
                        ]
                    });
                })
                .fail(function(response) {
                    alert(response.responseJSON.message)
                });
            });

            $(document).on('change', '.upload_media', function(e) {
                let file = $(this)[0].files[0];
                let id = $(this).attr("data-id")
                let URL = window.location.protocol + "//" + window.location.host + "/admin";
                let formData = new FormData();
                formData.append('media', file);
                $.ajax({
                    type: "post",
                    url: URL + `/help-center/sub-menu/content/${id}/media_upload`,
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function (response) {
                        $(`#media_${id} > img`).attr("src", response.media_url)
                    },
                    error: function(response) {
                        console.log(response)
                        alert(response.responseJSON.message)
                    }
                });
            });

            $(document).on("click", ".deletecontent", function (e) {
                e.preventDefault()
                let answer = window.confirm("Are you want delete")
                let URL = window.location.protocol + "//" + window.location.host;
                let id = $(this).attr("data-id")
                let currentElement = $(this)
                // console.log(URL + `/help-center/sub-menu/content/${id}/delete`)
                if(answer) {
                    $.ajax({
                        type: "delete",
                        url: URL + `/admin/help-center/sub-menu/content/${id}/delete`,
                    }).done(function (response) {
                        currentElement.closest(".content-box")[0].remove()
                    }).fail(function() {
                        alert("Error")
                    });
                }
            });
        });
    </script>
@endsection