<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.emergencynumbers.index', 'All Emergency Numbers', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.emergencynumbers.create', 'Create Emergency Number', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>