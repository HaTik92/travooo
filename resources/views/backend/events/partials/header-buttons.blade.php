<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.events.create', 'Create Event', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>