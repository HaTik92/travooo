<?php
use App\Models\Access\language\Languages;
$languages = DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
?>
@extends ('backend.layouts.app')

@section ('title', 'Events Manager' . ' | ' . 'Create Event')

@section('page-header')
    <h1>
        Events Manager
        <small>Create Event</small>
    </h1>
@endsection

@section('content')
    {{ Form::open([
            'id'     => 'events_form',
            'route'  => 'admin.events.store',
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'post',
            'novalidate' => env('APP_DEBUG'),
            'files'  => true
        ])
    }}
    <form>
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Create Event</h3>

            </div><!-- /.box-header -->
            <!-- Language Error : Start -->
            <div class="row error-box">
                <div class="col-md-10">
                    <div class="required_msg">
                    </div>
                </div>
            </div>
            <!-- Language Error : End -->
            <div class="box-body">
                @if( ! empty($languages))
                    <ul class="nav nav-tabs">
                        @foreach($languages as $language)
                            <li class="{{ ($languages[0]->id == $language->id)? 'active':'' }}">
                                <a data-toggle="tab" href="#{{$language->code}}">{{ $language->title }}</a>
                            </li>
                        @endforeach
                    </ul>

                    <div class="tab-content">
                        @foreach($languages as $language)
                            <div id="{{ $language->code }}" class="tab-pane fade in {{ ($languages[0]->id == $language->id)? 'active':'' }}">
                                <br />
                                {{ Form::hidden('translations['.$language->code.'][languages_id]', $language->id, []) }}
                                <!-- Start Name -->
                                <div class="form-group">
                                    {{ Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) }}

                                    <div class="col-lg-10">
                                        {{ Form::text('name', null, ['class' => 'form-control required', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => 'Name']) }}
                                    </div><!--col-lg-10-->
                                </div><!--form control-->
                                <!-- End: Name -->

                                <!-- Start: Category -->
                                <div class="form-group">
                                    {{ Form::label('category', 'Category', ['class' => 'col-lg-2 control-label']) }}

                                    <div class="col-lg-10">
                                        {{ Form::text('category', null, ['class' => 'form-control', 'placeholder' => 'Category']) }}
                                    </div><!--col-lg-10-->
                                </div>
                                <!-- End: Category -->

                                <!-- Start: Description -->
                                <div class="form-group">
                                    <div class="col-sm-12 textarea-msg"></div>
                                    {{ Form::label('description', 'Description', ['class' => 'col-lg-2 control-label description_input']) }}

                                    <div class="col-lg-10">
                                        {{ Form::textarea('description', null, ['class' => 'form-control description_input description', 'placeholder' => 'Description']) }}
                                    </div><!--col-lg-10-->
                                </div><!--form control-->
                                <!-- End: Description -->

                                <!-- Start: Date start -->
                                <div class="form-group">
                                    {{ Form::label('start', 'Date Start', ['class' => 'col-lg-2 control-label']) }}
                                    <div class="col-lg-6">
                                        <div class="input-group date" id="datetimepicker_start" data-target-input="nearest">
                                            {{ Form::text('start', null, ['class' => 'form-control datetimepicker-input', 'data-target' => '#datetimepicker_start']) }}
                                            <span class="input-group-addon" data-target="#datetimepicker_start" data-toggle="datetimepicker">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div><!--col-lg-10-->
                                </div><!--form control-->
                                <!-- End: Date start -->

                                <!-- Start: Date end -->
                                <div class="form-group">
                                    {{ Form::label('end', 'Date End', ['class' => 'col-lg-2 control-label']) }}
                                    <div class="col-lg-6">
                                        <div class="input-group date" id="datetimepicker_end" data-target-input="nearest">
                                            {{ Form::text('end', null, ['class' => 'form-control datetimepicker-input', 'data-target' => '#datetimepicker_end']) }}
                                            <span class="input-group-addon" data-target="#datetimepicker_end" data-toggle="datetimepicker">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div><!--col-lg-10-->
                                </div><!--form control-->
                                <!-- End: Date end -->

                                <!-- Countries: Start -->
                                <div class="form-group">
                                    {{ Form::label('country_id', 'Country', ['class' => 'col-lg-2 control-label']) }}

                                    <div class="col-lg-10">
                                        {{ Form::select('location_country_id', $countries, null,['class' => 'select2Class form-control country-input', 'id' => 'country_id', 'data-url' => url('admin/location/country/jsoncities')]) }}
                                    </div><!--col-lg-10-->
                                    <input id="countryInfo" class="form-control" type="hidden" value="{{ json_encode($countries_location) }}">

                                </div><!--form control-->
                                <!-- Countries: End -->

                                <!-- Cities: Start -->
                                <div class="form-group">
                                    {{ Form::label('city_id', 'Cities', ['class' => 'col-lg-2 control-label']) }}

                                    <div class="col-lg-10">
                                        {{ Form::select('cities_id', array(null => 'Please select ...') + $cities, null, ['required', 'class' => 'select2Class form-control', 'id' => 'city_id']) }}
                                    </div><!--col-lg-10-->
                                </div><!--form control-->
                                <!-- Cities: End -->

                                <div class="form-group">
                                    {{ Form::label('search_box', 'Select Location', ['class' => 'col-lg-2 control-label']) }}
                                    <div class="col-lg-10">
                                        <input id="pac-input" class="form-control" type="text" placeholder="Search Box" required>
                                        <div id="map"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('lat_lng', 'Lat,Lng', ['class' => 'col-lg-2 control-label']) }}
                                    <div class="col-lg-10">

                                        {{ Form::hidden('lat_lng', null, ['class' => 'form-control disabled', 'id' => 'lat-lng-input', 'required' => 'required', 'placeholder' => 'Lat,Lng']) }}

                                        {{ Form::text('lat_lng_show', null, ['class' => 'form-control disabled', 'id' => 'lat-lng-input_show', 'required' => 'required', 'placeholder' => 'Lat,Lng' , 'disabled' => 'disabled']) }}
                                    </div>
                                </div>

                                <!-- Images: Start -->
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 alert alert-warning alert-dismissible fade in">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <strong>Warning!</strong> <span class="set_error_msg"></span>
                                        </div>
                                    </div>
                                    {{ Form::label('title', 'Upload Images', ['class' => 'col-lg-2 control-label']) }}

                                    <div class="col-lg-10">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>Select File</th>
                                                <th>Title</th>
                                                <th>Author Name</th>
                                                <th>Author Link</th>
                                                <th>Source Link</th>
                                                <th>License Name</th>
                                                <th>License Link</th>
                                                <th class="set_last_col">Action</th>
                                            </tr>
                                            <tr>
                                                <td class="set_img_col">{{ Form::file('',[ 'name' => 'license_images[0][image]']) }}</td>
                                                <td>{{ Form::text('license_images[0][title]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                                <td>{{ Form::text('license_images[0][author_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                                <td>{{ Form::text('license_images[0][author_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                                <td>{{ Form::text('license_images[0][source_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                                <td>{{ Form::text('license_images[0][license_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                                <td>{{ Form::text('license_images[0][license_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                                <td>
                                                    <a href="javascript:void(0);" id="add_row" class="btn btn-success btn-xs add_icon">+</a>
                                                </td>
                                            </tr>
                                            <tbody id="addMoreRows"></tbody>
                                        </table>
                                    </div><!--col-lg-10-->
                                </div><!--form control-->
                                <!-- Images: End -->
                            </div>
                        @endforeach
                    </div>
                @endif
            </div><!-- /.box-body -->
        </div>

        <div class="box box-info">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.events.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs submit_button']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}
@endsection

@section('after-styles')
    <!-- Language Error Style: Start -->
    <style>
        .add_icon {
            width: 40px;
            font-size: 19px;
        }
        #map {
            height: 300px;
        }
        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 300px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }
        .required_msg{
            padding-left: 20px;
        }
        .alert-warning {
            display: none;
        }
    </style>
    <!-- Language Error Style: End -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">

    <!-- https://tempusdominus.github.io/bootstrap-3/ -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tempusdominus-bootstrap-3@5.0.0-alpha10/build/css/tempusdominus-bootstrap-3.min.css" />
@endsection

@section('after-scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <!-- https://tempusdominus.github.io/bootstrap-3/ -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/tempusdominus-bootstrap-3@5.0.0-alpha10/build/js/tempusdominus-bootstrap-3.min.js"></script>
@endsection