@extends ('backend.layouts.app')

@section('title', 'Events Manager')

@section('page-header')
    <h1>
        Events Manager
        <small>Active Events</small>
    </h1>
@endsection

@section('content')
    <div class="row deleted-box">
        <div class="col-md-12">
            <div class="deleted_msg"></div>
        </div>
    </div>
    <div class="box box-success">
        <?php $url = isset($eventUrlType) ? route("admin.events.api_table") : route("admin.events.table");?>
        <div class="box-header with-border">
            <h3 class="box-title">Events</h3>
            @include('backend.select-deselect-all-buttons')

            <div class="box-tools pull-right">
                @if(!isset($event_url_type))
                    @include('backend.events.partials.header-buttons')
                @endif
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div id="deleteLoadingEvents" style="display: none">
                <i class="fa fa-spinner fa-spin" style="position:absolute;top:50%;left:50%;width:200px;margin-left:-100px;text-align:center;font-size: 50px;"></i>
            </div>
            <div class="table-responsive">
                <table id="events-table" class="table table-condensed table-hover"
                       data-url="{{ $url }}"
                       data-del="{{ route("admin.events.delete_ajax") }}"
                       data-config="{{config('events_model.events_table')}}">
                    <thead>
                    <tr>
                        <th></th>
                        <th>id</th>
                        <th>Title</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->


@endsection

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
@endsection

@section('after-scripts')
    <style>
        table.dataTable tbody td.select-checkbox, table.dataTable tbody th.select-checkbox {
            width:20px !important;
        }
    </style>
@endsection