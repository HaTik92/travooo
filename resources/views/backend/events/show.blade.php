@extends ('backend.layouts.app')

@section ('title', 'Events Management' . ' | ' . 'View Event')

@section('page-header')
    <h1>
        Event Management
        <small>View Event</small>
    </h1>
@endsection

@section('after-styles')
    <style type="text/css">
        td.description {
            word-break: break-all;
        }
    </style>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">View Event</h3>

            <div class="box-tools pull-right">
                @if(!$event->provider_id)
                    @include('backend.events.partials.header-buttons-view')
                @endif
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div role="tabpanel">
                <table class="table table-striped table-hover">
                    <tr> <th> <h3 style="color:#0A8F27">Event {{ $event->title }}</h3> </th><td></td> </tr>
                    <tr>
                        <th>ID</th>
                        <td>{{ $event->id }}</td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <td>{{ $event->title }}</td>
                    </tr>
                    <tr>
                        <th>Category</th>
                        <td>{{ $event->category }}</td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td>{!!  $event->description !!} </td>
                    </tr>
                    <tr>
                        <th>City</th>
                        <td class="description"><?=$event->city?></td>
                    </tr>
                    <tr>
                        <th>Start Date</th>
                        <td class="description"><p><?=$event->start?></p></td>
                    </tr>
                    <tr>
                        <th>End Date</th>
                        <td class="description"><p><?=$event->end?></p></td>
                    </tr>
                    @if($event->provider_id)
                        <tr>
                            <th>Provider ID</th>
                            <td>{{ $event->provider_id }}</td>
                        </tr>
                    @endif
                    @if($event->labels)
                        <tr>
                            <th>Labels</th>
                            <td>{{ $event->labels }}</td>
                        </tr>
                    @endif
                </table>
                <table class="table table-hover">
                    <tr>
                        <th> <h3 style="color:#0A8F27">Location</h3></th><td></td>
                    </tr>
                    <tr>
                        <th>Latitude , Longitude</th>
                        <td> <p><?= $event->lat ?> , <?= $event->lng ?></p> </td>
                    </tr>
                </table>
                <!-- Map will be created in the "map" div -->
                <div id="map" data-lat="{{ $event->lat }}" data-lng="{{ $event->lng }}"></div>
                <tr></tr>
                <table class="table table-hover">
                    <tr>
                        <th> <h3 style="color:#0A8F27">Medias</h3></th><td></td>
                    </tr>
                    @if(empty($medias))
                        <tr>
                            <th> <p>No Medias Added.</p> </th><td></td>
                        </tr>
                    @endif
                    @foreach($medias as $key => $media)
                        <tr>
                            <th><p><?=$media?></p> </th><td></td>
                        </tr>
                    @endforeach

                    <!-- End: Medias -->
                    <tr>
                        <th> <h3 style="color:#0A8F27">Images</h3></th><td></td>
                    </tr>
                </table>
                <!-- Start: Images -->
                <div class="row">
                    @if(empty($images))
                        <div style="padding-left: 20px;"><p>No Images Added.</p></div>
                    @else
                        @foreach($images as $image)
                            @if( !empty($image) )
                                <div class="col-md-2">
                                    <a href="https://s3.amazonaws.com/travooo-images2/{{$image}}" target='_blank'>
                                        <img class="" src="https://s3.amazonaws.com/travooo-images2/{{$image}}"
                                             style="width:165px; height:150px;padding-top: 13px;"/>
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div><!--tab panel-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection