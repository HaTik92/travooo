@extends ('backend.layouts.app')

@section ('title', 'Languages Spoken Manager')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
@endsection

@section('page-header')
    <h1>
    	Languages Spoken Manager
    	<small>All Languages Spoken</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3> -->
            <h3 class="box-title">Languages Spoken</h3>

            <div class="box-tools pull-right">
                @include('backend.languages_spoken.partials.header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="languages-spoken-table" class="table table-condensed table-hover"
                                                   data-url="{{ route("admin.languagesspoken.table") }}"
                                                   data-config="{{config('language_spoken.languages_spoken_table')}}">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Title</th>
                        <th>Active</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection