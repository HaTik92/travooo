@extends ('backend.layouts.app')

@section ('title', 'Activities Management' . ' | ' . 'View Activity')

@section('page-header')
    <h1>
        Activities Management
        <small>View Activity</small>
    </h1>
@endsection

@section('after-styles')
    <style type="text/css">
        #map {
            height: 300px;
        }
    </style>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">View Activity</h3>

            <div class="box-tools pull-right">
                @include('backend.activity.partials.header-buttons-view')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">
                
                <table class="table table-striped table-hover">
                    @foreach($activitytrans as $key => $activity_translation)
                        <tr> <th> <h3 style="color:#0A8F27">{{ $activity_translation->translanguage->title }}</h3> </th><td></td> </tr>
                        <tr>
                            <th>Title <small>({{ $activity_translation->translanguage->title }})</small></th>
                            <td>{{ $activity_translation->title }}</td>
                        </tr>
                        <tr>
                            <th>Description <small>({{ $activity_translation->translanguage->title }})</small></th>
                            <td>{{ $activity_translation->description }}</td>
                        </tr>
                        <tr>
                            <th>Working Days <small>({{ $activity_translation->translanguage->title }})</small></th>
                            <td>{{ $activity_translation->working_days }}</td>
                        </tr>
                        <tr>
                            <th>Working Times<small>({{ $activity_translation->translanguage->title }})</small></th>
                            <td>{{ $activity_translation->working_times }}</td>
                        </tr>
                        <tr>
                            <th>How To Go <small>({{ $activity_translation->translanguage->title }})</small></th>
                            <td>{{ $activity_translation->how_to_go }}</td>
                        </tr>
                        <tr>
                            <th>When To Go <small>({{ $activity_translation->translanguage->title }})</small></th>
                            <td>{{ $activity_translation->when_to_go }}</td>
                        </tr>
                        <tr>
                            <th>Popularity <small>({{ $activity_translation->translanguage->title }})</small></th>
                            <td>{{ $activity_translation->popularity }}</td>
                        </tr>
                        <tr>
                            <th>Conditions <small>({{ $activity_translation->translanguage->title }})</small></th>
                            <td>{{ $activity_translation->conditions }}</td>
                        </tr>
                    @endforeach
                        <tr> <th> <h3 style="color:#0A8F27"> Common Fields </h3> </th> </tr>
                        <tr>
                            <th>Active</th>
                            <td>
                                @if($activity->active == 1)
                                    <label class="label label-success">Active</label>
                                @else
                                    <label class="label label-danger">Deactive</label>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Country</th>
                            <td> {{ $country->title }} </td>
                        </tr>
                        <tr>
                            <th>City</th>
                            <td> {{ $city->title }} </td>
                        </tr>
                        <tr>
                            <th>Place</th>
                            <td> {{ $place->title }} </td>
                        </tr>
                        <tr>
                            <th>Activity Type</th>
                            <td> {{ $activity_type->title }} </td>
                        </tr>
                        <tr>
                            <th>Safety Degree</th>
                            <td> {{ $safety_degree->title }} </td>
                        </tr>                    
                </table>
                <!-- Map will be created in the "map" div -->
                <div id="map" data-lat="{{ $activity->lat }}" data-lng="{{ $activity->lng }}"></div>
                <!-- Map div end -->

            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
@endsection