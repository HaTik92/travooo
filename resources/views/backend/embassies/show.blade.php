@extends ('backend.layouts.app')

@section ('title', 'Embassies Management' . ' | ' . 'View Embassies')

@section('page-header')
    <h1>
        Embassies Management
        <small>View Embassies</small>
    </h1>
@endsection

@section('after-styles')
    <style type="text/css">
        td.description {
            word-break: break-all;
        }
        #map {
            height: 300px;
        }
    </style>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">View Embassies</h3>

            <div class="box-tools pull-right">
                @include('backend.embassies.partials.header-buttons-view')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">
                
                <table class="table table-striped table-hover">
                    @foreach($embassiestrans as $key => $embassies_translation)
                        <tr> <th> <h3 style="color:#0A8F27">{{ $embassies_translation->translanguage->title }}</h3> </th><td></td> </tr>
                        <tr>
                            <th>Title <small>({{ $embassies_translation->translanguage->title }})</small></th>
                            <td>{{ $embassies_translation->title }}</td>
                        </tr>
                        <tr>
                            <th>Quick Facts <small>({{ $embassies_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$embassies_translation->description?></p></td>
                        </tr>
                        <!-- -->
                        <tr>
                            <th>Address <small>({{ $embassies_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$embassies_translation->address ?></p></td>
                        </tr>
                        <tr>
                            <th>Phone <small>({{ $embassies_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$embassies_translation->phone?></p></td>
                        </tr>
                        <tr>
                            <th>Working Days <small>({{ $embassies_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?= $embassies_translation->working_days?></p></td>
                        </tr>
                        <tr>
                            <th>Best Time to Visit <small>({{ $embassies_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?= $embassies_translation->when_to_go?></p></td>
                        </tr>
                        <tr>
                            <th>Price Level <small>({{ $embassies_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?= $embassies_translation->price_level ?></p></td>
                        </tr>
                        <tr>
                            <th>Website <small>({{ $embassies_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?= $embassies_translation->history ?></p></td>
                        </tr>
                    @endforeach
                    <tr>
                         <th> <h3 style="color:#0A8F27">Common Fields</h3></th><td></td>   
                    </tr>
                    <tr>
                        <th>Location Country</th>
                        <td> <p><?=$country->title?></p> </td>
                    </tr>
                    @if($originCountry && $originCountry->title)
                        <tr>
                            <th>Origin Country </th>
                            <td> <p>{{$originCountry->title}}</p> </td>
                        </tr>
                    @endif
                    <tr>
                        <th>Cities</th>
                        <td>
                            @if($cities)
                                <p>{{$cities}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Active </th>
                        <td>
                            @if($embassies->active == 1) 
                              <p><label class="label label-success">Active</label></p> 
                            @else
                              <p><label class="label label-danger">Deactive</label></p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                         <th> <h3 style="color:#0A8F27">Location</h3></th><td></td>   
                    </tr>
                    <tr>
                        <th>Latitude , Longitude</th>
                        <td> <p><?=$embassies->lat?> , <?=$embassies->lng?></p> </td>
                    </tr>
                    
                </table>
                <!-- Map will be created in the "map" div -->
                <div id="map"></div>
                <input id="embassyInfo" class="form-control" type="hidden" value="{{ json_encode($embassies) }}">
                <!-- Map div end -->

            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
@endsection