<?php
use App\Models\Access\language\Languages;
// $languages = DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
?>

@extends ('backend.layouts.app')

@section ('title', 'Embassies Manager' . ' | ' . 'Edit Embassies')

@section('page-header')
    <h1>
        <!-- {{ trans('labels.backend.access.users.management') }} -->
        Embassies Management
        <small>Edit Embassies</small>
    </h1>
@endsection

@section('after-styles')
    <style>
        #map {
            height: 300px;
        }
        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 300px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        .pac-container {
            font-family: Roboto;
        }

        .add_icon {
            width: 40px;
            font-size: 19px;
        }

        .alert-warning {
            display: none;
        }
    </style>
    <!-- Language Error Style: Start -->
    <style>
        .required_msg{
            padding-left: 20px;
        }
    </style>
    <!-- Language Error Style: End -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection

@section('content')
    {{ Form::model($embassies, [
            'id'     => 'embassies_update_form',
            'route'  => ['admin.embassies.update', $embassiesid],
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'PATCH',
            'files'  => true
        ]) 
    }}

        <div class="box box-success">
            <div class="box-header with-border">
                <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.create') }}</h3> -->
                <h3 class="box-title">Create Embassies</h3>

            </div><!-- /.box-header -->

            <!-- Language Error : Start -->
            <div class="row error-box">
                <div class="col-md-10">
                    <div class="required_msg">
                    </div>
                </div>
            </div>
            <!-- Language Error : End -->
            
            <div class="box-body">
                @if(!empty($languages))
                    <ul class="nav nav-tabs">
                    @foreach($languages as $language)
                        <li class="{{ ($languages[0]->id == $language->id)? 'active':'' }}">
                            <a data-toggle="tab" href="#{{$language->code}}">{{ $language->title }}</a>
                        </li>
                    @endforeach
                    </ul>
                    <div class="tab-content">
                    @foreach($data['translations'] as $translation)
                        @if($translation->translanguage->active)
                        <div id="{{ $translation->translanguage->code }}" class="tab-pane fade in {{ ($translation->translanguage->code == 'en')? 'active':'' }}">
                            <br />
                            {{ Form::hidden('translations['.$translation->translanguage->code.'][id]', $translation->id , []) }}
                            {{ Form::hidden('translations['.$translation->translanguage->code.'][languages_id]', $translation->translanguage->id , []) }}
                            <!-- Start Title -->
                            <div class="form-group">
                                {{ Form::label('translations['.$translation->translanguage->code.'][title]', 'Title', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$translation->translanguage->code.'][title]', $translation->title , ['class' => 'form-control required', 'maxlength' => '1000', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => 'Title']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Title -->

                            <!-- Start: Description -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][description]', 'Quick Facts', ['class' => 'col-lg-2 control-label description_input']) }}
                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][description]', $translation->description, ['class' => 'form-control description_input description', 'placeholder' => 'Quick Facts']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Description -->

                            <!-- Start: Address -->
                            <div class="form-group">
                                {{ Form::label('translations['.$translation->translanguage->code.'][address]', 'Address', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$translation->translanguage->code.'][address]', $translation->address, ['class' => 'form-control', 'placeholder' => 'Address', 'maxlength' => 255]) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Address -->

                            <!-- Start: Phone -->
                            <div class="form-group">
                                {{ Form::label('translations['.$translation->translanguage->code.'][phone]', 'Phone', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$translation->translanguage->code.'][phone]', $translation->phone, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => 'Phone']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Phone -->

                            <!-- Start: Working Hours -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][working_days]', 'Working Hours', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][working_days]', $translation->working_days, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Working Hours']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Working Hours -->

                            <!-- Start: When To Go -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][when_to_go]', 'Best Time to Visit', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][when_to_go]', $translation->when_to_go, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Best Time to Visit']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: When To Go -->

                            <!-- Start: Price Level -->
                            <div class="form-group">
                                {{ Form::label('translations['.$translation->translanguage->code.'][price_level]', 'Price Level', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::input('number','translations['.$translation->translanguage->code.'][price_level]', $translation->price_level, ['class' => 'form-control', 'placeholder' => 'Price Level']) }}

                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Price Level -->

                            <!-- Start: Website -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][history]', 'Website', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][history]', $translation->history, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Website']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: History -->
                        <!-- Languages Tabs: Start -->
                        </div>
                        @endif
                    @endforeach
                    </div>
                
                    <!-- Active: Start -->
                    <div class="form-group">
                        {{ Form::label('title', trans('validation.attributes.backend.access.languages.active'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            @if($data['active'] == 1)
                                {{ Form::checkbox('active', $data['active'] , true) }}
                            @else
                                {{ Form::checkbox('active', $data['active'] , false) }}
                            @endif
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- Active: End -->
                    <!-- Location Countries: Start -->
                    <div class="form-group">
                        {{ Form::label('title', 'Location Country', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::select('location_country_id', $countries, null,['class' => 'select2Class form-control country-input', 'id' => 'country_id', 'data-url' => url('admin/location/country/jsoncities')]) }}
                            <input id="countryInfo" class="form-control" type="hidden" value="{{ json_encode($countriesLocation) }}">
                            <input id="embassyInfo" class="form-control" type="hidden" value="{{ json_encode($embassies) }}">
                            <input type="hidden" id="getCities" value="{{ url('admin/location/country/jsoncities')}}">
                        </div>

                    </div><!--form control-->
                    <!-- Countries: End -->

                    <!-- Origin Countries: Start -->
                    <div class="form-group">
                        {{ Form::label('title', 'Origin Country', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::select('origin_country_id', $countries, $data['origin_country_id'],['class' => 'select2Class form-control']) }}
                            <input id="countryInfo" class="form-control" type="hidden" value="{{ json_encode($countriesLocation) }}">
                            <input id="embassyInfo" class="form-control" type="hidden" value="{{ json_encode($embassies) }}">
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- Countries: End -->
                     <!-- Cities: Start -->
                    <div class="form-group">
                        {{ Form::label('title', 'Cities', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            <h5 style="color: orange;">Please note that searched city is depend on selected country</h5>
                            {{ Form::select('cities_id', $data['selected_city_arr '] , $data['city_id'],['class' => 'select2Class form-control', 'id' => 'city_id']) }}

                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- Cities: End -->

                    <div class="form-group">
                    {{ Form::label('title', 'Select Location', ['class' => 'col-lg-2 control-label']) }}
                        <div class="col-lg-10">
                            <input id="pac-input" class="form-control" type="text" placeholder="Search Box">
                            <div id="map"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('title', 'Lat,Lng', ['class' => 'col-lg-2 control-label']) }}
                        <div class="col-lg-10">
                            
                            {{ Form::hidden('lat_lng', $data['lat_lng'], ['class' => 'form-control disabled', 'id' => 'lat-lng-input', 'required' => 'required', 'placeholder' => 'Lat,Lng']) }}

                            {{ Form::text('lat_lng_show', $data['lat_lng'], ['class' => 'form-control disabled', 'id' => 'lat-lng-input_show', 'required' => 'required', 'placeholder' => 'Lat,Lng' , 'disabled' => 'disabled']) }}
                        </div>
                    </div>
                    <!-- Images: Start -->
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="col-xs-12 alert alert-warning alert-dismissible fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Warning!</strong> <span class="set_error_msg"></span>
                            </div>
                        </div>
                        {{ Form::label('title', 'Upload Images', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Select File</th>
                                    <th>Title</th>
                                    <th>Author Name</th>
                                    <th>Author Link</th>
                                    <th>Source Link</th>
                                    <th>License Name</th>
                                    <th>License Link</th>
                                    <th>Delete</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td class="set_img_col">{{ Form::file('upload_img',[ 'name' => 'license_images[-1][image]']) }}</td>
                                    <td>{{ Form::text('license_images[-1][title]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[-1][author_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[-1][author_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[-1][source_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[-1][license_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[-1][license_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>

                                    </td>
                                    <td><a href="#javascript" id="add_icon" class="btn btn-success btn-xs add_icon">+</a>
                                        <input type="hidden" id="cnt" value="-1" />
                                    </td>
                                </tr>
                                @if( !empty($media_results) )
                                    @foreach( $media_results as $rowmedia)

                                        <tr>
                                            <td class="set_img_col">
                                                @if( !empty($rowmedia->url) )
                                                    <div class="col-md-2" id="set_delete{{$rowmedia->id}}">
                                                        <!-- Cover delete Icon -->
                                                        <i id="delete_covers"
                                                           onclick="delete_additional_img('{{$rowmedia->url}}', '{{$rowmedia->id}}')"
                                                           class="setDeleteIc zoom-effect-icon fa fa-times image-hide"
                                                           data-toggle="tooltip" rel="tooltip" title="" data-id="" aria-hidden="true"
                                                           state="2" start-check="1" data-original-title="Remove Image"></i>
                                                        <a href="https://s3.amazonaws.com/travooo-images2/{{$rowmedia->url}}"
                                                           target='_blank'>
                                                            <img class=""
                                                                 src="https://s3.amazonaws.com/travooo-images2/{{$rowmedia->url}}"
                                                                 width="100" height="100"/>
                                                        </a>
                                                    </div>
                                                @endif
                                            </td>
                                            <td>{{ Form::text("license_images[".$rowmedia['id']."][title]", $rowmedia['title'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                            <td>{{ Form::text("license_images[".$rowmedia['id']."][author_name]", $rowmedia['author_name'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                            <td>{{ Form::text("license_images[".$rowmedia['id']."][author_url]", $rowmedia['author_url'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                            <td>{{ Form::text("license_images[".$rowmedia['id']."][source_url]", $rowmedia['source_url'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                            <td>{{ Form::text("license_images[".$rowmedia['id']."][license_name]", $rowmedia['license_name'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                            <td>{{ Form::text("license_images[".$rowmedia['id']."][license_url]", $rowmedia['license_url'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                            <td><input type="checkbox" name="del[]" value="{{$rowmedia['id']}}" /></td>
                                            <td><a href="#javascript" id="add_icon" class="btn btn-success btn-xs add_icon">+</a>
                                            </td>
                                        </tr>

                                    @endforeach
                                @endif
                                <tbody id="addMoreRows"></tbody>
                            </table>
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- Images: End -->
                @endif
                <!-- Languages Tabs: End -->
            </div>
        </div>

        <div class="box box-info">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.embassies.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs submit_button']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@endsection

@section('after-styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection
@section('after-scripts')
    <style>
        .zoom-effect-icon:hover {
            font-size: 17px;
        }
        .image-hide{
            display: none;
        }
    </style>
@endsection
