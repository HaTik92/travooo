<div class="pull-right mb-10 hidden-sm hidden-xs">
    <!-- {{ link_to_route('admin.access.user.create', trans('menus.backend.access.users.create'), [], ['class' => 'btn btn-success btn-xs']) }} -->
    {{ link_to_route('admin.embassies.import', 'Import Embassies', [], ['class' => 'btn btn-success btn-xs']) }}
    {{ link_to_route('admin.embassies.create', 'Create Embassies', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>