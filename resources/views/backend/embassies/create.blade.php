<?php  
use App\Models\Access\language\Languages;
$languages = DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
?>
@extends ('backend.layouts.app')

@section ('title', 'Embassies Manager' . ' | ' . 'Create Embassies')

@section('page-header')
    <!-- <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.create') }}</small>
    </h1> -->
    <h1>
        Embassies Manager
        <small>Create Embassies</small>
    </h1>
@endsection

@section('after-styles')
    <style>
        .add_icon {
            width: 40px;
            font-size: 19px;
        }
        #map {
            height: 300px;
        }
        #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      .alert-warning {
          display: none;
      }
    </style>

    <!-- Language Error Style: Start -->
    <style>
        .required_msg{
            padding-left: 20px;
        }
    </style>
    <!-- Language Error Style: End -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection

@section('content')
    {{ Form::open([
            'id'     => 'embassies_form',
            'route'  => 'admin.embassies.store',
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'post',
            'files'  => true
        ]) 
    }}
        <div class="box box-success">
            <div class="box-header with-border">
                <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.create') }}</h3> -->
                <h3 class="box-title">Create Embassies</h3>

            </div><!-- /.box-header -->
            <!-- Language Error : Start -->
            <div class="row error-box">
                <div class="col-md-10">
                    <div class="required_msg">
                    </div>
                </div>
            </div>
            <!-- Language Error : End -->
            <div class="box-body">
                @if(!empty($languages))
                    <ul class="nav nav-tabs">
                    @foreach($languages as $language)
                        <li class="{{ ($languages[0]->id == $language->id)? 'active':'' }}">
                            <a data-toggle="tab" href="#{{$language->code}}">{{ $language->title }}</a>
                        </li>
                    @endforeach
                    </ul>
                    <div class="tab-content">
                    @foreach($languages as $language)
                        <div id="{{ $language->code }}" class="tab-pane fade in {{ ($language->code == 'en')? 'active':'' }}">
                            <br />
                            {{ Form::hidden('translations['.$language->code.'][languages_id]', $language->id, []) }}

                        <!-- Start Title -->
                            <div class="form-group">
                                {{ Form::label('translations['.$language->code.'][title]', 'Title', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$language->code.'][title]', null, ['class' => 'form-control required', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => 'Title']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Title -->

                            <!-- Start: Quick Facts -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$language->code.'][description]', 'Quick Facts', ['class' => 'col-lg-2 control-label description_input']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$language->code.'][description]', null, ['class' => 'form-control description_input description', 'placeholder' => 'Quick Facts']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Quick Facts -->

                            <!-- Start: Address -->
                            <div class="form-group">
                                {{ Form::label('translations['.$language->code.'][address]', 'Address', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$language->code.'][address]', null, ['class' => 'form-control', 'placeholder' => 'Address', 'maxlength' => 255]) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Address -->

                            <!-- Start: Phone -->
                            <div class="form-group">
                                {{ Form::label('translations['.$language->code.'][phone]', 'Phone', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$language->code.'][phone]', null, ['class' => 'form-control', 'maxlength' => 191, 'placeholder' => 'Phone']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Phone -->

                            <!-- Start: Working Hours -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$language->code.'][working_days]', 'Working Hours', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$language->code.'][working_days]', null, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Working Hours']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Working Hours -->

                            <!-- Start: Best Time to Visit -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$language->code.'][when_to_go]', 'Best Time to Visit', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$language->code.'][when_to_go]', null, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Best Time to Visit']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Best Time to Visit -->

                            <!-- Start: Price Level -->
                            <div class="form-group">
                                {{ Form::label('translations['.$language->code.'][price_level]', 'Price Level', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                     {{ Form::input('number','translations['.$language->code.'][price_level]', null , ['class' => 'form-control', 'placeholder' => 'Price Level']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Price Level -->

                            <!-- Start: Website -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$language->code.'][history]', 'Website', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$language->code.'][history]', null, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Website']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-
                            <!-- Languages Tabs: Start -->
                        </div>
                    @endforeach
                    </div>

                    <!-- Active: Start -->
                    <div class="form-group">
                        {{ Form::label('title', trans('validation.attributes.backend.access.languages.active'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::checkbox('active', '1', true) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- Active: End -->

                    <!-- Countries: Start -->
                    <div class="form-group">
                        {{ Form::label('title', 'Country', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::select('location_country_id', $countries, null,['class' => 'select2Class form-control country-input', 'id' => 'country_id', 'data-url' => url('admin/location/country/jsoncities')]) }}
                        </div><!--col-lg-10-->
                        <input type="hidden" id="getCities" value="{{ url('admin/location/country/jsoncities')}}">
                        <input id="countryInfo" class="form-control" type="hidden" value="{{ json_encode($countries_location) }}">

                    </div><!--form control-->
                    <!-- Countries: End -->

                     <!-- Cities: Start -->
                    <div class="form-group">
                        {{ Form::label('title', 'Cities', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::select('cities_id', array(null => 'Please select ...') + $cities, null, ['required', 'class' => 'select2Class form-control', 'id' => 'city_id']) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- Cities: End -->
        
                    <div class="form-group">
                    {{ Form::label('title', 'Select Location', ['class' => 'col-lg-2 control-label']) }}
                        <div class="col-lg-10">
                            <input id="pac-input" class="form-control" type="text" placeholder="Search Box" required>
                            <div id="map"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('title', 'Lat,Lng', ['class' => 'col-lg-2 control-label']) }}
                        <div class="col-lg-10">
                            
                            {{ Form::hidden('lat_lng', null, ['class' => 'form-control disabled', 'id' => 'lat-lng-input', 'required' => 'required', 'placeholder' => 'Lat,Lng']) }}

                            {{ Form::text('lat_lng_show', null, ['class' => 'form-control disabled', 'id' => 'lat-lng-input_show', 'required' => 'required', 'placeholder' => 'Lat,Lng' , 'disabled' => 'disabled']) }}
                        </div>
                    </div>
                    <!-- Images: Start -->
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="col-xs-12 alert alert-warning alert-dismissible fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Warning!</strong> <span class="set_error_msg"></span>
                            </div>
                        </div>
                        {{ Form::label('title', 'Upload Images', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Select File</th>
                                    <th>Title</th>
                                    <th>Author Name</th>
                                    <th>Author Link</th>
                                    <th>Source Link</th>
                                    <th>License Name</th>
                                    <th>License Link</th>
                                    <th class="set_last_col">Action</th>
                                </tr>
                                <tr>
                                    <td class="set_img_col">{{ Form::file('',[ 'name' => 'license_images[0][image]']) }}</td>
                                    <td>{{ Form::text('license_images[0][title]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[0][author_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[0][author_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[0][source_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[0][license_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>{{ Form::text('license_images[0][license_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    <td>
                                        <a href="javascript:void(0);" id="add_icon" class="btn btn-success btn-xs add_icon">+</a>
                                    </td>
                                </tr>
                                <tbody id="addMoreRows"></tbody>
                            </table>
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- Images: End -->
                @endif
                <!-- Languages Tabs: End -->
            </div>
        </div>
        <div class="box box-info">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.embassies.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs submit_button']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}
@endsection

@section('after-scripts')
    <style>
        .zoom-effect-icon:hover {
            font-size: 17px;
        }
        .image-hide{
            display: none;
        }
    </style>
@endsection
