@extends ('backend.layouts.app')

@section('title', 'Suspended Accounts')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page-header')
    <h1>
        Suspended Accounts Manager
        <small>Reports</small>
    </h1>
@endsection

@section('content')
    <div class="row deleted-box">
        <div class="col-md-12">
            <div class="deleted_msg"></div>
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Suspended Accounts</h3>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table
                        id="infringementsSuspendedAccounts"
                        class="table table-condensed table-hover"
                        data-url="{{ route('admin.copyright_infringement.users_table') }}">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th style="min-width: 120px">{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection