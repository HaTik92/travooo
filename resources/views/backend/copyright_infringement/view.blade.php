@extends ('backend.layouts.app')

@section ('title', 'Copyright Infringement' . ' | ' . 'View Copyright Infringement')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page-header')
    <h1>
        Copyright Infringement Management
        <small>View Copyright Infringement</small>
    </h1>
@endsection

@section('content')
    <div class="row deleted-box">
        <div class="col-md-12">
            <div class="deleted_msg"></div>
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">View Copyright Infringement</h3>

            <div class="box-tools pull-right">
                <div class="mb-10 hidden-sm hidden-xs">
                    <button  id="confirm_btn" data-id="{{$copyright->id}}" class="btn btn-success btn-xs">Confirm</button>
                    <button  id="block-user-btn" data-id="{{$copyright->owner_id}}" class="btn btn-warning btn-xs">Block Author</button>
                    <button  id="delete_btn"  data-id="{{$copyright->id}}" class="btn btn-danger btn-xs">Delete</button>
                </div><!--pull right-->

                <div class="clearfix"></div>
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">

                <table class="table table-striped table-hover">
                    <tr>
                        <th>Id </th>
                        <td>{{ $copyright->id }}</td>
                    </tr>
                    <tr>
                        <th>Sender Type </th>
                        <td>{{ trans('labels.backend.copyright_infringement.' . $copyright->sender_type ) }}</td>
                    </tr>
                    <tr>
                        <th>Owner Name</th>
                        <td>{{ $copyright->owner_name }}</td>
                    </tr>
                    <tr>
                        <th>Sender Name</th>
                        <td>{{ $copyright->sender_name }}</td>
                    </tr>
                    <tr>
                        <th>Company</th>
                        <td>{{ $copyright->company }}</td>
                    </tr>
                    <tr>
                        <th>Job Title</th>
                        <td>{{ $copyright->job_title }}</td>
                    </tr>
                    <tr>
                        <th>Author Email</th>
                        <td>{{ $copyright->sender_email }}</td>
                    </tr>

                    <tr>
                        <th>Street</th>
                        <td>{{ $copyright->street }}</td>
                    </tr>
                    </tr>
                    <tr>
                        <th>City</th>
                        <td>{{ $copyright->city }}</td>
                    </tr>
                    <tr>
                        <th>State</th>
                        <td>{{ $copyright->state }}</td>
                    </tr>
                    <tr>
                        <th>Postal Code</th>
                        <td>{{ $copyright->postal_code }}</td>
                    </tr>
                    <tr>
                        <th>Country</th>
                        <td>{{ $copyright->country }}</td>
                    </tr>
                    <tr>
                        <th>Phone</th>
                        <td>{{ $copyright->phone }}</td>
                    </tr>

                    <tr>
                        <th>Fax</th>
                        <td>{{ $copyright->fax }}</td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        @foreach(json_decode($copyright->description) AS $description)
                            <td>{{ $description }}</td>
                        @endforeach
                    </tr>
                    <tr>
                        <th>Source Link</th>
                        <td>
                            @foreach(json_decode($copyright->original_link) AS $original_link)
                                <a href="{{ $original_link }}" target="_blank">{{ $original_link }}</a></td>
                            @endforeach
                    </tr>
                    <tr>
                        <th>Infringement Location</th>
                        <td>
                            {{ trans('labels.backend.copyright_infringement.' . $copyright->infringement_location ) }}<br>
                        </td>
                    </tr>
                    <tr>
                        <th>Reported URL</th>
                        <td>
                            <a href="{{ $copyright->reported_url }}" target="_blank">{{  substr($copyright->reported_url, 0, 25)}}</a><br>
                        </td>
                    </tr>
                    <tr>
                        <th>Violator URL</th>
                        <td>
                            <a href="{{url("/profile/".$copyright->owner_id)}}" target="_blank">{{ $copyright->owner_name }}</a><small>({{$copyright->approvedInfractions}})</small><br>
                        </td>
                    </tr>
                    <tr>
                        <th>Infringement Description</th>
                        <td>
                            {{ $copyright->infringement_description }}
                        </td>
                    </tr>
                    <tr>
                        <th>Authority Act</th>
                        <td>{{ $copyright->authority_act }}</td>
                    </tr>
                    <tr>
                        <th>Signature</th>
                        <td>{{ $copyright->signature }}</td>
                    </tr>
                </table>
            </div><!--tab panel-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection
<style>
    td {
        word-break: break-all;
    }
</style>