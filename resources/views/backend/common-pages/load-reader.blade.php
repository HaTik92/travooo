@php
    use App\Models\CommonPage\CommonPage;
@endphp
@extends ('backend.layouts.app')

@section ('title', $page_title)

@section('page-header')
    <!-- <h1>
        {{ trans('labels.backend.access.users.management') }}
    </h1> -->
    <h1>
        {{$page_title}}
    </h1>
@endsection

@section('after-styles')
    <style>
        .required_msg{
            padding-left: 20px;
        }
    </style>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Recent History</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div>
        <div class="box-body">
            <ul class="timeline">
                @foreach ($arr as $item)
                    <li>
                        <i class="fa fa-plus"></i>
                        <div class="timeline-item">
                            <span class="time"> 
                                <a href="{{ url('admin/log/download')."/".$item['file']}}" class="btn btn-xs btn-success">
                                    <i class="fa fa-download"></i>
                                </a>
                            </span>
                            <h3 class="timeline-header no-border"><strong>{{$item['file']}}</strong> </h3>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection

@section('after-scripts')
   
@endsection