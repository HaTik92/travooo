@extends ('backend.layouts.app')

@section('title', 'Spam Manager')

@section('after-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="{{asset('assets2/js/jquery-confirm/jquery-confirm.min.js')}}"></script>
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page-header')
    <h1>
        Spam Manager
        <small>Reports History</small>
    </h1>
@endsection

@section('content')
    <div class="row deleted-box">
        <div class="col-md-12">
            <div class="deleted_msg"></div>
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Spam Manager - Reports History</h3>
            <div class="box-tools pull-right">
                {{ Form::select('filter', ['' => 'All', 'mark_as_safe' => 'Safe Posts', 'remove_post' => 'Removed posts', 'remove_post_and_block_user' => 'Removed posts w/user deletion'] , [] ,['aria-controls' => 'spamManagerHistoryTable','class' => 'custom-filters form-control', 'id' => 'filterReportHistoryType']) }}
                <input type="hidden" name="submit" value="filter"/>
                {{ Form::close() }}
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table
                        id="spamManagerHistoryTable"
                        class="table table-condensed table-hover"
                        data-url="{{ route('admin.spam_manager.history_table')}}">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Admin Name / ID</th>
                        <th>Post ID</th>
                        <th>Post Type</th>
                        <th>Spam Type</th>
                        <th>Date</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
