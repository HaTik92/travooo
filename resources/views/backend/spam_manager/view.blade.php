@extends ('backend.layouts.app')

@section ('title', 'Spam Posts' . ' | ' . 'View Spam Posts')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page-header')
    <h1>
        Spam Posts Management
        <small>View Spam Posts</small>
    </h1>
@endsection

@section('content')
    <div class="row deleted-box">
        <div class="col-md-12">
            <div class="deleted_msg"></div>
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">View Spam Posts</h3>

            <div class="box-tools pull-right">
                <div class="clearfix"></div>
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">

                <table class="table table-striped table-hover">
                    <tr>
                        <th>Id </th>
                        <td>{{ $spamPosts->id }}</td>
                    </tr>
                    <tr>
                        <th>Report Type</th>
                        <td>{{ trans('labels.backend.spam_reports.'.$spamPosts->report_type) }}</td>
                    </tr>
                    <tr>
                        <th>Post Type</th>
                        <td>{{ $spamPosts->post_type }}</td>
                    </tr>
                    <tr>
                        <th>Report Text</th>
                        <td>{{ $spamPosts->report_text }}</td>
                    </tr>
                    <tr>
                        <th>Post ID</th>
                        <td>{{ $spamPosts->posts_id }}</td>
                    </tr>
                    <tr>
                        <th>Owner</th>
                        <td>
                            @if(isset($spamPosts->user) && $spamPosts->user)
                                @if($spamPosts->user->expert)
                                    <span class="exp-icon">EXP</span>
                                @endif
                                <span class="post-foot-block post-reaction"><i>#{{$spamPosts->user->id}}</i> - <a href="{{url('profile/'.$spamPosts->user->id)}}">{{$spamPosts->user->name}}</a></span>
                            @else
                                <label class="label label-default">Unknown</label>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Users Count</th>
                        <td>
                            @if($spamPosts->report_users && is_object($spamPosts->report_users))
                            {{ count($spamPosts->report_users) }}
                            @else
                            <span>0</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>User who got reported</th>
                        <td>
                            @if($spamPosts->author)
                                <a href="{{url('profile-posts/'.$spamPosts->author->id)}}">{{ $spamPosts->author->name }}</a>
                            @else
                                <span>-</span>
                            @endif
                        </td>
                    </tr>
                </table>
            </div><!--tab panel-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection
<style>
    td {
        word-break: break-all;
    }
</style>