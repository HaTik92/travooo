@extends ('backend.layouts.app')

@section('title', 'Spam Manager')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" href="{{asset('assets2/css/travooo.css?0.6.8'.time())}}">
@endsection
@section('after-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
@endsection
@section('page-header')
    <h1>
        Spam Manager
        <small>Reports</small>
    </h1>
@endsection

@section('content')
    <div class="row deleted-box">
        <div class="col-md-12">
            <div class="deleted_msg"></div>
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Spam Manager - Reports</h3>
            <div class="box-tools pull-right">
                {{ Form::select('filter', ['' => 'All', 'experts' => 'Experts Reports', 'flagged_authors' => 'Flagged Authors'], [] ,['aria-controls' => 'spamManagerTable', 'class' => 'custom-filters form-control', 'id' => 'filterExpertReports']) }}
                {{ Form::close() }}
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table
                        id="spamManagerTable"
                        class="table table-condensed table-hover"
                        data-url="{{ route('admin.spam_manager.table', ['type' => $type]) }}">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Report Type</th>
                        <th>Post Type</th>
                        <th>Owner</th>
                        <th>Users Count</th>
                        <th>Report text</th>
                        <th>User who got reported</th>
                        <th>Post ID</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <style>
        .custom-scroll {
            max-height: 78px;
            word-break: break-all;
            overflow-y: scroll

        }
        .custom-scroll::-webkit-scrollbar-track
        {
            /*-webkit-box-shadow: inset 0 0 3px rgba(0,0,0,0.3);*/
            border-radius: 5px;
            /*background-color: #F5F5F5;*/
            /*background-color: #FFF;*/
        }

        .custom-scroll::-webkit-scrollbar
        {
            width: 6px;
            /*background-color: #ccc;*/
            /*background-color: #fff;*/
        }

        .custom-scroll::-webkit-scrollbar-thumb
        {
            border-radius: 5px;
            -webkit-box-shadow: inset 0 0 3px rgba(0,0,0,.3);
            background-color: #ccc;
        }
    </style>
@endsection
