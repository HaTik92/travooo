<?php
use App\Models\Access\language\Languages;
// $languages = DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
?>

@extends ('backend.layouts.app')

@section ('title', 'Travel Styles Manager' . ' | ' . 'Edit Travel Style')

@section('page-header')
    <h1>
        <!-- {{ trans('labels.backend.access.users.management') }} -->
        Travel Styles Management
        <small>Create Travel Style</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($lifestyle, [
            'id'     => 'lifestyle_update_form',
            'route'  => ['admin.lifestyle.update', $lifestyleid],
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'PATCH',
            'files'  => true
        ])
    }}

        <!-- CUSTOM HIDDEN INPUT FIELDS - START -->

        <input type="hidden" name="media-cover-image" id="media-cover-image">
        <input type="hidden" name="remove-cover-image" id="remove-cover-image" value="0">
        <!-- CUSTOM HIDDEN INPUT FIELDS - END -->

        <div class="box box-success">
            <div class="box-header with-border">
                <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.create') }}</h3> -->
                <h3 class="box-title">Update Travel Style</h3>

                <div class="box-tools pull-right">

                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <!-- Language Error : Start -->
            <div class="row error-box">
                <div class="col-md-10">
                    <div class="required_msg">
                    </div>
                </div>
            </div>
            <!-- Language Error : End -->

            <div class="box-body">
                @if(!empty($languages))
                    <ul class="nav nav-tabs">
                    @foreach($languages as $language)
                        <li class="{{ ($languages[0]->id == $language->id)? 'active':'' }}">
                            <a data-toggle="tab" href="#{{$language->code}}">{{ $language->title }}</a>
                        </li>
                    @endforeach
                    </ul>

                    <div class="tab-content">
                    @foreach($languages as $language)
                        <div id="{{ $language->code }}" class="tab-pane fade in {{ ($languages[0]->id == $language->id)? 'active':'' }}">
                            <br />
                            <!-- Start Title -->
                            <div class="form-group">
                                {{ Form::label('title_'.$language->id, 'Title', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('title_'.$language->id, isset($data['title_'.$language->id]) ? $data['title_'.$language->id] : null, ['class' => 'form-control required', 'maxlength' => '255', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => 'Title']) }}

                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Title -->
                        </div>
                    @endforeach
                    </div>

                    <div class="col-md-2">

                    </div>
                    <div class="col-md-10">
                        <table class="table table-bordered">
                            <tr>
                                <td class="set_img_col">{{ Form::file('upload_img',[ 'name' => 'license_images[-1][image]']) }}</td>
                            </tr>
                            <tr>
                                <th>Select File</th>
                                <th>Delete</th>
                            </tr>

                            @if( !empty($media_results) )
                                @foreach( $media_results as $rowmedia)

                                    <tr>
                                        <td class="set_img_col">
                                            @if( !empty($rowmedia->url) )
                                                <div class="col-md-10" id="set_delete{{$rowmedia->id}}">
                                                    <!-- Cover delete Icon -->
                                                    <i id="delete_covers"
                                                       onclick="delete_additional_img('{{$rowmedia->url}}', '{{$rowmedia->id}}')"
                                                       class="setDeleteIc zoom-effect-icon fa fa-times image-hide"
                                                       data-toggle="tooltip" rel="tooltip" title="" data-id="" aria-hidden="true"
                                                       state="2" start-check="1" data-original-title="Remove Image"></i>
                                                    <a href="https://s3.amazonaws.com/travooo-images2/{{$rowmedia->url}}"
                                                       target='_blank'>
                                                        <img class=""
                                                             src="https://s3.amazonaws.com/travooo-images2/{{$rowmedia->url}}"
                                                             width="100" height="100"/>
                                                    </a>
                                                </div>
                                            @endif
                                        </td>
                                        <td><input type="checkbox" name="del[]" value="{{$rowmedia['id']}}" /></td>
                                        </td>
                                    </tr>
                                    <input type="hidden" name="delete-images[]" id="delete-images" value="{{$rowmedia['id']}}">
                                @endforeach
                            @endif
                            <tbody id="addMoreRows"></tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div><!--box-->

        <div class="box box-info">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.lifestyle.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs submit_button']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@endsection

@section('after-styles')
    <!-- Language Error Style: Start -->
    <style>
        .required_msg{
            padding-left: 20px;
        }
    </style>
    <!-- Language Error Style: End -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection
@section('after-scripts')
    <style>
        .zoom-effect-icon:hover {
            font-size: 17px;
        }

        .image-hide {
            display: none;
        }
    </style>
@endsection
