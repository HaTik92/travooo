@extends ('backend.layouts.app')

@section('title', 'Places Manager')

@section('after-styles')

@endsection

@section('page-header')

<h1>
    Places Manager
    <small>Mass tasks</small>
</h1>
@endsection

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Places</h3>

    </div><!-- /.box-header -->

    <div id="places-mass-vue-app" class="box-body">


        <h5 class="h3 box-title">Filters</h5>
        <hr>



        <div class="row">

            <div class="col-sm-3">
                <div :class="{'has-error': _.has(filterFormErrors, 'country_id')}" class="form-group">
                    <select id="countrySelectFilter" class="custom-filters form-control">
                        <option value="">Search Country</option>
                    </select>
                    <span class="help-block"
                          style="display:none"
                          :style="{display: _.has(filterFormErrors, 'country_id') ? 'block' : 'none'}"
                          v-if="_.has(filterFormErrors, 'country_id')"
                          v-for="error in filterFormErrors.country_id"
                          >
                        @{{ error }}
                    </span>
                </div><!-- /.form-group -->
            </div>

            <div class="col-sm-3">
                <div :class="{'has-error': _.has(filterFormErrors, 'city_id')}" class="form-group">
                    <select id="citySelectFilter" class="custom-filters form-control">
                        <option value="">Search City</option>
                    </select>
                    <span class="help-block"
                          style="display:none"
                          :style="{display: _.has(filterFormErrors, 'city_id') ? 'block' : 'none'}"
                          v-if="_.has(filterFormErrors, 'city_id')"
                          v-for="error in filterFormErrors.city_id"
                    >
                        @{{ error }}
                    </span>
                </div><!-- /.form-group -->
            </div>

            <div class="col-sm-3">
                <div :class="{'has-error': _.has(filterFormErrors, 'address')}" class="form-group">
                    <input type="text" v-model="filters.address" class="form-control" id="addressInputFilter" placeholder="Address">
                    <span class="help-block"
                          style="display:none"
                          :style="{display: _.has(filterFormErrors, 'address') ? 'block' : 'none'}"
                          v-if="_.has(filterFormErrors, 'address')"
                          v-for="error in filterFormErrors.address"
                    >
                        @{{ error }}
                    </span>
                </div><!-- /.form-group -->
            </div>

            <div class="col-sm-3">
                <div :class="{'has-error': _.has(filterFormErrors, 'pluscode')}" class="form-group">
                    <input type="text" v-model="filters.pluscode" class="form-control" id="pluscodeInputFilter" placeholder="Pluscode">
                    <span class="help-block"
                          style="display:none"
                          :style="{display: _.has(filterFormErrors, 'pluscode') ? 'block' : 'none'}"
                          v-if="_.has(filterFormErrors, 'pluscode')"
                          v-for="error in filterFormErrors.pluscode"
                    >
                        @{{ error }}
                    </span>
                </div><!-- /.form-group -->
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-3"></div>
            <div class="col-sm-3">
                <div :class="{'has-error': _.has(filterFormErrors, 'address_not_match')}" class="form-group">
                    <input type="text" v-model="filters.address_not_match" class="form-control" id="addressNotMatchInputFilter" placeholder="Address not match">
                    <span class="help-block"
                          style="display:none"
                          :style="{display: _.has(filterFormErrors, 'address_not_match') ? 'block' : 'none'}"
                          v-if="_.has(filterFormErrors, 'address_not_match')"
                          v-for="error in filterFormErrors.address_not_match"
                    >
                        @{{ error }}
                    </span>
                </div><!-- /.form-group -->
            </div>
            <div class="col-sm-3">
                <div :class="{'has-error': _.has(filterFormErrors, 'pluscode_not_match')}" class="form-group">
                    <input type="text" v-model="filters.pluscode_not_match" class="form-control" id="pluscodeNotMatchInputFilter" placeholder="Pluscode not match">
                    <span class="help-block"
                          style="display:none"
                          :style="{display: _.has(filterFormErrors, 'pluscode_not_match') ? 'block' : 'none'}"
                          v-if="_.has(filterFormErrors, 'pluscode_not_match')"
                          v-for="error in filterFormErrors.pluscode_not_match"
                    >
                        @{{ error }}
                    </span>
                </div><!-- /.form-group -->
            </div>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-3"></div>
            <div class="col-sm-3"></div>
            <div class="col-sm-3">
                <loader-btn :loading="loadings.filters"
                            :disabled="loadings.filters"
                            @click="clickApplyFilters('{!! $action !!}')" class="btn btn-success btn-block">Apply filters</loader-btn>
                <span
                        :class="{'text-danger': filterFormStatus.status =='error', 'text-success': filterFormStatus.status =='success'}"
                        style="display:none"
                        :style="{display: _.has(filterFormStatus, 'text') ? 'block' : 'none'}"
                        v-if="_.has(filterFormStatus, 'text')"
                >
                        @{{ filterFormStatus.text }}
                    </span>
            </div>
        </div><!-- /.row -->

        <div class="row" style="margin-top:50px; min-height: 80px;">
            <div class="col-sm-12">
                <div :class="{'alert-success': founded_ids.length > 0, 'alert-warning': founded_ids.length <= 0}"
                     style="display:none"
                     :style="{display: 'block'}"
                     class="alert" role="alert"><strong v-text="founded_ids.length"></strong> places found</div>
            </div>
        </div><!-- /.row -->



        <h5 class="h3 box-title">Actions</h5>
        <hr>



        <div class="row">
            <div class="col-sm-4">
                <div :class="{'has-error': _.has(actionMoveToErrors, 'country_id')}" class="form-group">
                    <select id="moveToCountrySelect" class="custom-filters form-control">
                        <option value="">Search Country</option>
                    </select>
                    <span class="help-block"
                          style="display:none"
                          :style="{display: _.has(actionMoveToErrors, 'country_id') ? 'block' : 'none'}"
                          v-if="_.has(actionMoveToErrors, 'country_id')"
                          v-for="error in actionMoveToErrors.country_id"
                    >
                        @{{ error }}
                    </span>
                </div><!-- /.form-group -->
            </div>
            <div class="col-sm-4">
                <div :class="{'has-error': _.has(actionMoveToErrors, 'city_id')}" class="form-group">
                    <select id="moveToCitySelect" class="custom-filters form-control">
                        <option value="">Search City</option>
                    </select>
                    <span class="help-block"
                          style="display:none"
                          :style="{display: _.has(actionMoveToErrors, 'city_id') ? 'block' : 'none'}"
                          v-if="_.has(actionMoveToErrors, 'city_id')"
                          v-for="error in actionMoveToErrors.city_id"
                    >
                        @{{ error }}
                    </span>
                </div><!-- /.form-group -->
            </div>
            <div class="col-sm-4">
                <loader-btn :loading="loadings.moveToAction"
                            :disabled="loadings.moveToAction || founded_ids.length <= 0"
                            @click="clickApplyAction" class="btn btn-danger btn-block">Move places to selected city</loader-btn>
                <span v-for="status in actionMoveToStatus"
                        :class="{'text-danger': status.status =='error', 'text-success': status.status =='success'}"
                        style="display:none"
                        :style="{display: _.has(status, 'text') ? 'block' : 'none'}"
                        v-if="_.has(status, 'text')"
                >
                        @{{ status.text }}
                    </span>
                <span class="text-danger"
                      style="display:none"
                      :style="{display: _.has(actionMoveToErrors, 'ids') ? 'block' : 'none'}"
                      v-if="_.has(actionMoveToErrors, 'ids')"
                      v-for="error in actionMoveToErrors.ids"
                      v-text="error"
                ></span>
            </div>
        </div><!-- /.row -->






    </div><!-- /.box-body -->
</div><!--box-->
@endsection

@section('after-scripts')

@endsection
