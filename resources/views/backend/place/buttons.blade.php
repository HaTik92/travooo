<a href="{{ route('admin.location.place.show', $place) }}" class="btn btn-xs btn-info">
    <i class="fa fa-search" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{{ trans('buttons.general.crud.view') }}"></i>
</a>
<a href="{{ route('admin.location.place.edit', $place) }}" class="btn btn-xs btn-primary">
    <i class="fa fa-pencil" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{{ trans('buttons.general.crud.edit') }}"></i>
</a>
@if($place->active == 2)
    <a href="{{ route('admin.location.place.mark', [$place, 1]) }}" class="btn btn-xs btn-success">
        <i class="fa fa-play" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{{ trans('buttons.backend.access.users.activate') }}"></i>
    </a>
@elseif($place->active == 1)
    <a href="{{ route('admin.location.place.mark', [$place, 2]) }}" class="btn btn-xs btn-warning">
        <i class="fa fa-pause" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{{ trans('buttons.backend.access.users.deactivate') }}"></i>
    </a>
@endif
<a href="{{  route('admin.location.place.destroy', $place) }}"
   data-method="delete"
   data-trans-button-cancel="{{ trans('buttons.general.cancel') }}"
   data-trans-button-confirm="{{ trans('buttons.general.crud.delete') }}"
   data-trans-title="{{ trans('strings.backend.general.are_you_sure') }}"
   class="btn btn-xs btn-danger"
>
    <i class="fa fa-trash" rel="tooltip" data-toggle="tooltip" data-placement="top" title="{{ trans('buttons.general.crud.delete') }}"></i>
</a>