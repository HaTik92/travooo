@extends ('backend.layouts.app')

@section ('title', 'Places Management' . ' | ' . 'View Place')

@section('page-header')
    <h1>
        Places Management
        <small>View Place</small>
    </h1>
@endsection

@section('after-styles')
    <style type="text/css">
        td.description {
            word-break: break-all;
        }
        #map {
            height: 300px;
        }
    </style>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">View Place</h3>

            <div class="box-tools pull-right">
                @include('backend.place.partials.header-buttons-view')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">

                <table class="table table-striped table-hover">

                    @foreach($placetrans as $key => $place_translation)
                        <tr> <th> <h3 style="color:#0A8F27">{{ $place_translation->translanguage->title }}</h3> </th><td></td> </tr>
                        <tr>
                            <th>Title <small>({{ $place_translation->translanguage->title }})</small></th>
                            <td>{{ $place_translation->title }}</td>
                        </tr>
                        <tr>
                            <th>Quick Facts <small>({{ $place_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$place_translation->description?></p></td>
                        </tr>
                        <tr>
                            <th>Address <small>({{ $place_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$place_translation->address ?></p></td>
                        </tr>
                        <tr>
                            <th>Phone <small>({{ $place_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$place_translation->phone?></p></td>
                        </tr>
                        <tr>
                            <th>Working Days <small>({{ $place_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$place_translation->working_days?></p></td>
                        </tr>
                        <tr>
                            <th>Best Time to Visit <small>({{ $place_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$place_translation->when_to_go?></p></td>
                        </tr>
                        <tr>
                            <th>Price Level <small>({{ $place_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$place_translation->price_level ?></p></td>
                        </tr>
                        <tr>
                            <th>Website <small>({{ $place_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$place_translation->history ?></p></td>
                        </tr>
                    @endforeach
                    <tr>
                         <th> <h3 style="color:#0A8F27">Common Fields</h3></th><td></td>
                    </tr>
                    <tr>
                        <th>Country </th>
                        <td> <p><?=$country->title?></p> </td>
                    </tr>
                    <tr>
                        <th>Cities </th>
                        <td> <p><?=$city->title?></p> </td>
                    </tr>
                    <tr>
                        <th>Active </th>
                        <td>
                            @if($place->active == 1)
                              <p><label class="label label-success">Active</label></p>
                            @else
                              <p><label class="label label-danger">Deactive</label></p>
                            @endif
                        </td>
                    </tr>

                    <!-- Start: Medias -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Medias</h3></th><td></td>
                    </tr>
                    @if(empty($medias))
                      <tr>
                          <th> <p>No Medias Added.</p> </th><td></td>
                      </tr>
                    @endif
                    @foreach($medias as $key => $media)
                      <tr>
                          <th> <p><?=$media?></p> </th><td></td>
                      </tr>
                    @endforeach
                    <!-- End: Medias -->
                    <!-- Start: Images -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Images</h3></th><td></td>   
                    </tr>
                </table>
                <div class="row">
                    @if(empty($images))
                        <div style="padding-left: 20px;"><p>No Images Added.</p></div>
                    @endif
                    @foreach($images as $key => $image)
                        <div class="col-md-2" style="margin-right: 20px;margin-top:10px;">  
                            <a href="<?=$image?>"><img src="<?=$image?>" style="width:170px;height:150px;"/>
                            </a>
                        </div>
                    @endforeach
                </div>
                <!-- End: Images -->
                <!-- Cover Image Section - Start -->
                <table class="table table-striped table-hover" style="margin-top: 20px;">
                    <tr>
                        <th><h3 style="color:#0A8F27;">Cover Image</h3></th><td></td>
                    </tr>
                </table>
                <div class="row" style="margin-bottom: 10px;margin-bottom: 20px;">
                    @if(empty($cover))
                        <div style="padding-left: 20px;">No Cover Image Added.</div>
                    @else
                        <div style="padding-top: 10px;padding-left: 20px;"><img src="{{$cover['url']}}" style="width: 170px;height:150px;" /></div>
                    @endif
                </div>
                <!-- Cover Image Section - End -->
                <table class="table table-striped table-hover">
                    <tr>
                         <th> <h3 style="color:#0A8F27">Location</h3></th><td></td>
                    </tr>
                    <tr>
                        <th>Latitude , Longitude</th>
                        <td> <p><?=$place->lat?> , <?=$place->lng?></p> </td>
                    </tr>

                </table>
                <!-- Map will be created in the "map" div -->
                <div id="map"></div>
                <input id="placeInfo" class="form-control" type="hidden" value="{{ json_encode($place) }}">
                <!-- Map div end -->

            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
@endsection