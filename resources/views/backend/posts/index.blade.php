@extends ('backend.layouts.app')

@section('title', 'Posts Manager')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
@endsection

@section('page-header')
    <h1>
        Post Manager
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Post Viewer</h3>
        </div>
        <div class="box-body">
            <div class="col-md-12 search-section" data-url="{{ route('admin.posts.search') }}">
                <div class="col-md-2">
                    <input type="text" class="form-control post-id" name="post_id">
                </div>
                <div class="col-md-2">
                    <select class="form-control post-type" name="post_type">
                        @foreach($types as $type)
                            <option value="{{ $type }}">{{ ucwords($type) }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-success search">Search</button>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4">
                    <table class="table table-condensed table-hover res-table hidden">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>User Id</th>
                                <th>Rank</th>
                                <th>Rank 7</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="tb-id"></td>
                                <td class="tb-user-id"></td>
                                <td class="tb-rank"></td>
                                <td class="tb-rank-7"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
