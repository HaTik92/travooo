<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.ageranges.index', 'All Age Ranges', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.ageranges.create', 'Create Age Ranges', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>