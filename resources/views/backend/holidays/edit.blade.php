<?php
use App\Models\Access\language\Languages;
// $languages = DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
?>

@extends ('backend.layouts.app')

@section ('title', 'Holidays Manager' . ' | ' . 'Edit Holidays')

@section('page-header')
    <h1>
        <!-- {{ trans('labels.backend.access.users.management') }} -->
        Holidays Management
        <small>Edit Holidays</small>
    </h1>
@endsection

@section('after-styles')
    <!-- Language Error Style: Start -->
    <style>
        .required_msg{
            padding-left: 20px;
        }
        .image-hide {
            display: none;
        }
    </style>
    <!-- Language Error Style: End -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection

@section('content')
    {{ Form::model($holidays, [
            'id'     => 'holidays_update_form',
            'route'  => ['admin.holidays.update', $holidaysid],
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'PATCH',
            'files'  => true

        ]) 
    }}

        <div class="box box-success">
            <div class="box-header with-border">
                <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.create') }}</h3> -->
                <h3 class="box-title">Edit Holidays</h3>
            </div><!-- /.box-header -->

            <!-- Language Error : Start -->
            <div class="row error-box">
                <div class="col-md-10">
                    <div class="required_msg">
                    </div>
                </div>
            </div>
            <!-- Language Error : End -->
            
            <div class="box-body">
                @if(!empty($languages))
                    <ul class="nav nav-tabs">
                    @foreach($languages as $language)
                        <li class="{{ ($languages[0]->id == $language->id)? 'active':'' }}">
                            <a data-toggle="tab" href="#{{$language->code}}">{{ $language->title }}</a>
                        </li>
                    @endforeach
                    </ul>

                    <div class="tab-content">
                    @foreach($languages as $language)
                        <div id="{{ $language->code }}" class="tab-pane fade in {{ ($languages[0]->id == $language->id)? 'active':'' }}">
                            <br />
                            <!-- Start Title -->
                            <div class="form-group">
                                {{ Form::label('title_'.$language->id, 'Title', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('title_'.$language->id, isset($data['title_'.$language->id]) ? $data['title_'.$language->id] : null, ['class' => 'form-control required', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => 'Title']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Title -->
                            <!-- Start Slug -->
                            <div class="form-group">
                                {{ Form::label('slug_'.$language->id, 'Slug', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('slug_'.$language->id, isset($data['slug_'.$language->id]) ? $data['slug_'.$language->id] : null, ['class' => 'form-control', 'maxlength' => '191', 'autofocus' => 'autofocus', 'placeholder' => 'Slug']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Slug -->
                            <!-- Start Description -->
                            <div class="form-group">
                                {{ Form::label('description_'.$language->id, 'Description', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('description_'.$language->id, isset($data['description_'.$language->id]) ? $data['description_'.$language->id] : null , ['class' => 'form-control description', 'autofocus' => 'autofocus', 'placeholder' => 'Description']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Description -->
                            <!-- Languages Tabs: Start -->
                        </div>
                    @endforeach
                    </div>

                    <!-- Images: Start -->
                    <div class="form-group">
                        {{ Form::label('title', 'Change Image', ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            <table class="table table-bordered">
                                @if(!empty($rowmedia))
                                    <tr class="row_old_image">
                                        <th>Selected File</th>
                                        <th>Title</th>
                                        <th>Author Name</th>
                                        <th>Author Link</th>
                                        <th>Source Link</th>
                                        <th>License Name</th>
                                        <th>License Link</th>
                                        <th>Delete</th>
                                    </tr>
                                    <tr class="row_old_image">
                                        <td class="set_img_col">
                                            @if( !empty($rowmedia->url) )
                                                <div class="col-md-2" id="set_delete{{$rowmedia->id}}">
                                                    <!-- Cover delete Icon -->
                                                    <i id="delete_covers"
                                                       onclick="delete_additional_img('{{$rowmedia->url}}', '{{$rowmedia->id}}')"
                                                       class="setDeleteIc zoom-effect-icon fa fa-times image-hide"
                                                       data-toggle="tooltip" rel="tooltip" title="" data-id="" aria-hidden="true"
                                                       state="2" start-check="1" data-original-title="Remove Image"></i>
                                                    <a href="https://s3.amazonaws.com/travooo-images2/{{$rowmedia->url}}"
                                                       target='_blank'>
                                                        <img class=""
                                                             src="https://s3.amazonaws.com/travooo-images2/{{$rowmedia->url}}"
                                                             width="100" height="100"/>
                                                    </a>
                                                </div>
                                            @endif
                                        </td>
                                        <td>{{ Form::text("license_images[".$rowmedia['id']."][title]", $rowmedia['title'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text("license_images[".$rowmedia['id']."][author_name]", $rowmedia['author_name'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text("license_images[".$rowmedia['id']."][author_url]", $rowmedia['author_url'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text("license_images[".$rowmedia['id']."][source_url]", $rowmedia['source_url'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text("license_images[".$rowmedia['id']."][license_name]", $rowmedia['license_name'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text("license_images[".$rowmedia['id']."][license_url]", $rowmedia['license_url'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td><input type="checkbox" name="del[]" class="delete" value="{{$rowmedia['id']}}" /></td>
                                    </tr>
                                @endif
                                    <tr class="upload_img">
                                        <th>Select File</th>
                                        <th>Title</th>
                                        <th>Author Name</th>
                                        <th>Author Link</th>
                                        <th>Source Link</th>
                                        <th>License Name</th>
                                        <th>License Link</th>
                                    </tr>
                                    <tr class="upload_img">
                                        <td class="set_img_col">{{ Form::file('upload_img',[ 'name' => 'license_images[0][image]']) }}</td>
                                        <td>{{ Form::text('license_images[0][title]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text('license_images[0][author_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text('license_images[0][author_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text('license_images[0][source_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text('license_images[0][license_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text('license_images[0][license_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                    </tr>
                            </table>
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <!-- Images: End -->

                @endif
                <!-- Languages Tabs: End -->
            </div>
        </div>

        <div class="box box-info">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.holidays.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs submit_button']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@endsection

@section('after-styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection
