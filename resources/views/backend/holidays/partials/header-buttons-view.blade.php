<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.holidays.index', 'All Holidays', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.holidays.create', 'Create Holidays', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>