<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.cultures.index', 'All Cultures', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.cultures.create', 'Create Cultures', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>