@extends ('backend.layouts.app')

@section('title', 'Cities Manager')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page-header')
   <!--  <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.active') }}</small>
    </h1> -->
    <h1>
    	{{ trans('labels.backend.cities.cities_manager') }}
    	<small>{{ trans('labels.backend.cities.active_cities') }}</small>
    </h1>
@endsection

@section('content')
    <div class="row deleted-box">
        <div class="col-md-12">
            <div class="deleted_msg"></div>
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header with-border">
            <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3> -->
            <h3 class="box-title">Cities</h3>
            @include('backend.select-deselect-all-buttons')
            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.city-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div id="deleteLoadingCities" style="display: none">
                <i class="fa fa-spinner fa-spin" style="position:absolute;top:50%;left:50%;width:200px;margin-left:-100px;text-align:center;font-size: 50px;"></i>
            </div>

            <div class="table-responsive">
                <table id="cities-table" class="table table-condensed table-hover"
                                         data-url="{{ route("admin.location.city.table") }}"
                                         data-del="{{ route("admin.location.city.delete_ajax") }}"
                                         data-country="{{ route("admin.location.city.countries") }}"
                                         data-config="{{config('locations.city_table')}}">
                    <thead>
                    <tr>
                        <th></th>
                        <th>id</th>
                        <th>Title</th>
                        <th>Code</th>
                        <th>Country</th>
                        <th>lat</th>
                        <th>lng</th>
                        <th>Active</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection