<tr>
    <td>Low Season</td>
    <td>
        <table class="table table-bordered">
            <colgroup>
                <col span="1" style="width: 48%;">
                <col span="1" style="width: 48%;">
                <col span="1" style="width: 4%;">
            </colgroup>
            <tr>
                <th>Start</th>
                <th>End</th>
                <th>End</th>
            </tr>
            <tr>
                <td>
                    {{ Form::selectMonth('best_time[low_season][low_start][]', (isset($low_se)) ? $low_se[0] : null, ['id' => 'low_start', 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                    {{ Form::selectMonth('', (isset($low_se)) ? $low_se[0] : null,['id' => 'monthSelectHidden-low_start', 'placeholder' => 'select month', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    {{ Form::selectMonth('best_time[low_season][low_end][]', (isset($low_se)) ? $low_se[1] : null, ['id' => 'low_end', 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                    {{ Form::selectMonth('', (isset($low_se)) ? $low_se[1] : null,['id' => 'monthSelectHidden-low_end', 'placeholder' => 'select month', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    <a href="javascript:void(0);" id="add_icon_low_season" class="btn btn-success btn-xs add_icon">+</a>
                </td>
            </tr>
            @if (count($low_start_end) > 1)
                @foreach ($low_start_end as $key => $item)
                <?php
                    if ($key == 0) continue;
                    $low_se = explode('-', $item);
                    // dd($low_se);
                ?>
                <tr>
                    <td>
                        {{ Form::selectMonth('best_time[low_season][low_start][]', (isset($low_se)) ? $low_se[0] : null, ['id' => $key.'low_start', 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                        {{ Form::selectMonth('', (isset($low_se)) ? $low_se[0] : null,['id' => 'monthSelectHidden-'.$key.'low_start', 'placeholder' => 'select month', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                    </td>
                    <td>
                        {{ Form::selectMonth('best_time[low_season][low_end][]', (isset($low_se)) ? $low_se[1] : null, ['id' => $key.'low_end', 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                        {{ Form::selectMonth('', (isset($low_se)) ? $low_se[1] : null,['id' => 'monthSelectHidden-'.$key.'low_end', 'placeholder' => 'select month', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                    </td>
                    <td>
                        <a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row" style="width:100%">‐</a>
                    </td>
                </tr>
                @endforeach
            @endif
            <tbody id="addMoreRowsLowSeason"></tbody>
        </table>
    </td>
</tr>
<tr>
    <td>Shoulder</td>
    <td>
        <table class="table table-bordered">
            <colgroup>
                <col span="1" style="width: 48%;">
                <col span="1" style="width: 48%;">
                <col span="1" style="width: 4%;">
            </colgroup>
            <tr>
                <th>Start</th>
                <th>End</th>
                <th>End</th>
            </tr>
            <tr>
                <td>
                    {{ Form::selectMonth('best_time[shoulder][shoulder_start][]', (isset($shoulder_se)) ? $shoulder_se[0] : null, ['id' => 'shoulder_start', 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                    {{ Form::selectMonth('', (isset($shoulder_se)) ? $shoulder_se[0] : null, ['id' => 'monthSelectHidden-shoulder_start', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    {{ Form::selectMonth('best_time[shoulder][shoulder_end][]', (isset($shoulder_se)) ? $shoulder_se[1] : null, ['id' => 'shoulder_end', 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                    {{ Form::selectMonth('', (isset($shoulder_se)) ? $shoulder_se[1] : null, ['id' => 'monthSelectHidden-shoulder_end', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    <a href="javascript:void(0);" id="add_icon_shoulder" class="btn btn-success btn-xs add_icon">+</a>
                </td>
            </tr>
            @if (count($shoulder_start_end) > 1)
                @foreach ($shoulder_start_end as $key => $item)
                <?php
                    if ($key == 0) continue;
                    $shoulder_se = explode('-', $item);
                ?>
                <tr>
                    <td>
                        {{ Form::selectMonth('best_time[shoulder][shoulder_start][]', (isset($shoulder_se)) ? $shoulder_se[0] : null, ['id' => $key.'shoulder_start', 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                        {{ Form::selectMonth('', (isset($shoulder_se)) ? $shoulder_se[0] : null, ['id' => 'monthSelectHidden-'.$key.'shoulder_start', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                    </td>
                    <td>
                        {{ Form::selectMonth('best_time[shoulder][shoulder_end][]', (isset($shoulder_se)) ? $shoulder_se[1] : null, ['id' => $key.'shoulder_end', 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%', (isset($shoulder_se)) ? "" : "disabled"]) }}
                        {{ Form::selectMonth('', (isset($shoulder_se)) ? $shoulder_se[1] : null, ['id' => 'monthSelectHidden-'.$key.'shoulder_end', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                    </td>
                    <td>
                        <a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row" style="width:100%">‐</a>
                    </td>
                </tr>
                @endforeach
            @endif
            <tbody id="addMoreRowsShoulder"></tbody>
        </table>
    </td>
</tr>
<tr>
    <td>High Season</td>
    <td>
        <table class="table table-bordered">
            <colgroup>
                <col span="1" style="width: 48%;">
                <col span="1" style="width: 48%;">
                <col span="1" style="width: 4%;">
            </colgroup>
            <tr>
                <th>Start</th>
                <th>End</th>
                <th>End</th>
            </tr>
            <tr>
                <td>
                    {{ Form::selectMonth('best_time[high_season][high_start][]', (isset($high_se)) ? $high_se[0] : null, ['id' => 'high_start', 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                    {{ Form::selectMonth('', (isset($high_se)) ? $high_se[0] : null, ['id' => 'monthSelectHidden-high_start', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    {{ Form::selectMonth('best_time[high_season][high_end][]', (isset($high_se)) ? $high_se[1] : null, ['id' => 'high_end', 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%', (isset($high_se)) ? "" : "disabled"]) }}
                    {{ Form::selectMonth('', (isset($high_se)) ? $high_se[1] : null, ['id' => 'monthSelectHidden-high_end', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    <a href="javascript:void(0);" id="add_icon_high_season" class="btn btn-success btn-xs add_icon">+</a>
                </td>
            </tr>
            @if (count($shoulder_start_end) > 1)
                @foreach ($shoulder_start_end as $key => $item)
                <?php
                    if ($key == 0) continue;
                    $shoulder_se = explode('-', $item);
                ?>
                <td>
                    {{ Form::selectMonth('best_time[high_season][high_start][]', (isset($high_se)) ? $high_se[0] : null, ['id' => $key.'high_start', 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                    {{ Form::selectMonth('', (isset($high_se)) ? $high_se[0] : null, ['id' => 'monthSelectHidden-'.$key.'high_start', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    {{ Form::selectMonth('best_time[high_season][high_end][]', (isset($high_se)) ? $high_se[1] : null, ['id' => $key.'high_end', 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%', (isset($high_se)) ? "" : "disabled"]) }}
                    {{ Form::selectMonth('', (isset($high_se)) ? $high_se[1] : null, ['id' => 'monthSelectHidden-'.$key.'high_end', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    <a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row" style="width:100%">‐</a>
                </td>
                @endforeach
            @endif
            <tbody id="addMoreRowsHighSeason"></tbody>
        </table>
    </td>
</tr>











<tr>
    <td>Low Season</td>
    <td>
        <table class="table table-bordered">
            <colgroup>
                <col span="1" style="width: 48%;">
                <col span="1" style="width: 48%;">
                <col span="1" style="width: 4%;">
            </colgroup>
            <tr>
                <th>Start</th>
                <th>End</th>
                <th>End</th>
            </tr>
            <tr>
                <td>
                    {{ Form::selectMonth('best_time[low_season][low_start][]', (isset($low_se)) ? $low_se[0] : null, ['id' => 'low_start', 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                    {{ Form::selectMonth('', (isset($low_se)) ? $low_se[0] : null,['id' => 'monthSelectHidden-low_start', 'placeholder' => 'select month', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    {{ Form::selectMonth('best_time[low_season][low_end][]', (isset($low_se)) ? $low_se[1] : null, ['id' => 'low_end', 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                    {{ Form::selectMonth('', (isset($low_se)) ? $low_se[1] : null,['id' => 'monthSelectHidden-low_end', 'placeholder' => 'select month', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    <a href="javascript:void(0);" id="add_icon_low_season" class="btn btn-success btn-xs add_icon">+</a>
                </td>
            </tr>
            @if (count($low_start_end) > 1)
                @foreach ($low_start_end as $key => $item)
                <?php
                    if ($key == 0) continue;
                    $low_se = explode('-', $item);
                    // dd($low_se);
                ?>
                <tr>
                    <td>
                        {{ Form::selectMonth('best_time[low_season][low_start][]', (isset($low_se)) ? $low_se[0] : null, ['id' => $key.'low_start', 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                        {{ Form::selectMonth('', (isset($low_se)) ? $low_se[0] : null,['id' => 'monthSelectHidden-'.$key.'low_start', 'placeholder' => 'select month', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                    </td>
                    <td>
                        {{ Form::selectMonth('best_time[low_season][low_end][]', (isset($low_se)) ? $low_se[1] : null, ['id' => $key.'low_end', 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                        {{ Form::selectMonth('', (isset($low_se)) ? $low_se[1] : null,['id' => 'monthSelectHidden-'.$key.'low_end', 'placeholder' => 'select month', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                    </td>
                    <td>
                        <a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row" style="width:100%">‐</a>
                    </td>
                </tr>
                @endforeach
            @endif
            <tbody id="addMoreRowsLowSeason"></tbody>
        </table>
    </td>
</tr>
<tr>
    <td>Shoulder</td>
    <td>
        <table class="table table-bordered">
            <colgroup>
                <col span="1" style="width: 48%;">
                <col span="1" style="width: 48%;">
                <col span="1" style="width: 4%;">
            </colgroup>
            <tr>
                <th>Start</th>
                <th>End</th>
                <th>End</th>
            </tr>
            <tr>
                <td>
                    {{ Form::selectMonth('best_time[shoulder][shoulder_start][]', (isset($shoulder_se)) ? $shoulder_se[0] : null, ['id' => 'shoulder_start', 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                    {{ Form::selectMonth('', (isset($shoulder_se)) ? $shoulder_se[0] : null, ['id' => 'monthSelectHidden-shoulder_start', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    {{ Form::selectMonth('best_time[shoulder][shoulder_end][]', (isset($shoulder_se)) ? $shoulder_se[1] : null, ['id' => 'shoulder_end', 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                    {{ Form::selectMonth('', (isset($shoulder_se)) ? $shoulder_se[1] : null, ['id' => 'monthSelectHidden-shoulder_end', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    <a href="javascript:void(0);" id="add_icon_shoulder" class="btn btn-success btn-xs add_icon">+</a>
                </td>
            </tr>
            @if (count($shoulder_start_end) > 1)
                @foreach ($shoulder_start_end as $key => $item)
                <?php
                    if ($key == 0) continue;
                    $shoulder_se = explode('-', $item);
                ?>
                <tr>
                    <td>
                        {{ Form::selectMonth('best_time[shoulder][shoulder_start][]', (isset($shoulder_se)) ? $shoulder_se[0] : null, ['id' => $key.'shoulder_start', 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                        {{ Form::selectMonth('', (isset($shoulder_se)) ? $shoulder_se[0] : null, ['id' => 'monthSelectHidden-'.$key.'shoulder_start', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                    </td>
                    <td>
                        {{ Form::selectMonth('best_time[shoulder][shoulder_end][]', (isset($shoulder_se)) ? $shoulder_se[1] : null, ['id' => $key.'shoulder_end', 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%', (isset($shoulder_se)) ? "" : "disabled"]) }}
                        {{ Form::selectMonth('', (isset($shoulder_se)) ? $shoulder_se[1] : null, ['id' => 'monthSelectHidden-'.$key.'shoulder_end', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                    </td>
                    <td>
                        <a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row" style="width:100%">‐</a>
                    </td>
                </tr>
                @endforeach
            @endif
            <tbody id="addMoreRowsShoulder"></tbody>
        </table>
    </td>
</tr>
<tr>
    <td>High Season</td>
    <td>
        <table class="table table-bordered">
            <colgroup>
                <col span="1" style="width: 48%;">
                <col span="1" style="width: 48%;">
                <col span="1" style="width: 4%;">
            </colgroup>
            <tr>
                <th>Start</th>
                <th>End</th>
                <th>End</th>
            </tr>
            <tr>
                <td>
                    {{ Form::selectMonth('best_time[high_season][high_start][]', (isset($high_se)) ? $high_se[0] : null, ['id' => 'high_start', 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                    {{ Form::selectMonth('', (isset($high_se)) ? $high_se[0] : null, ['id' => 'monthSelectHidden-high_start', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    {{ Form::selectMonth('best_time[high_season][high_end][]', (isset($high_se)) ? $high_se[1] : null, ['id' => 'high_end', 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%', (isset($high_se)) ? "" : "disabled"]) }}
                    {{ Form::selectMonth('', (isset($high_se)) ? $high_se[1] : null, ['id' => 'monthSelectHidden-high_end', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    <a href="javascript:void(0);" id="add_icon_high_season" class="btn btn-success btn-xs add_icon">+</a>
                </td>
            </tr>
            @if (count($shoulder_start_end) > 1)
                @foreach ($shoulder_start_end as $key => $item)
                <?php
                    if ($key == 0) continue;
                    $shoulder_se = explode('-', $item);
                ?>
                <td>
                    {{ Form::selectMonth('best_time[high_season][high_start][]', (isset($high_se)) ? $high_se[0] : null, ['id' => $key.'high_start', 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                    {{ Form::selectMonth('', (isset($high_se)) ? $high_se[0] : null, ['id' => 'monthSelectHidden-'.$key.'high_start', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    {{ Form::selectMonth('best_time[high_season][high_end][]', (isset($high_se)) ? $high_se[1] : null, ['id' => $key.'high_end', 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%', (isset($high_se)) ? "" : "disabled"]) }}
                    {{ Form::selectMonth('', (isset($high_se)) ? $high_se[1] : null, ['id' => 'monthSelectHidden-'.$key.'high_end', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                </td>
                <td>
                    <a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row" style="width:100%">‐</a>
                </td>
                @endforeach
            @endif
            <tbody id="addMoreRowsHighSeason"></tbody>
        </table>
    </td>
</tr>