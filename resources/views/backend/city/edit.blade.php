<?php
use App\Models\Access\language\Languages;
// $languages = DB::table('conf_languages')->where('active', Languages::LANG_ACTIVE)->get();
?>

@extends ('backend.layouts.app')

@section ('title', 'Cities Manager' . ' | ' . 'Edit City')

@section('page-header')
    <h1>
    <!-- {{ trans('labels.backend.access.users.management') }} -->
        Cities Management
        <small>Edit City</small>
    </h1>
@endsection

@section('after-styles')
    <style>

        .add_icon {
            width: 40px;
            font-size: 19px;
        }
        .about_name {
            text-align: right;
        }

        #map {
            height: 300px;
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 300px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        .pac-container {
            font-family: Roboto;
        }

        .alert-warning {
            display: none;
        }
    </style>
    <!-- Language Error Style: Start -->
    <style>
        .required_msg {
            padding-left: 20px;
        }
    </style>
    <!-- Language Error Style: End -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
@endsection

@section('content')
    {{ Form::model($city, [
            'id'     => 'city_update_form',
            'route'  => ['admin.location.city.update', $cityid],
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'PATCH',
            'files'  => true
        ])
    }}

    <!-- CUSTOM HIDDEN INPUT FIELDS - START -->
    <input type="hidden" name="delete-images" id="delete-images">
    <input type="hidden" name="media-cover-image" id="media-cover-image">
    <input type="hidden" name="remove-cover-image" id="remove-cover-image" value="0">
    <!-- CUSTOM HIDDEN INPUT FIELDS - END -->

    <div class="box box-success">
        <div class="box-header with-border">
        <!-- <h3 class="box-title">{{ trans('labels.backend.access.users.create') }}</h3> -->
            <h3 class="box-title">Edit City</h3>

        </div><!-- /.box-header -->

        <!-- Language Error : Start -->
        <div class="row error-box">
            <div class="col-md-10">
                <div class="required_msg">
                </div>
            </div>
        </div>
        <!-- Language Error : End -->

        <div class="box-body">
            @if(!empty($languages))
                <ul class="nav nav-tabs">
                    @foreach($languages as $language)
                        <li class="{{ ($languages[0]->id == $language->id)? 'active':'' }}">
                            <a data-toggle="tab" href="#{{$language->code}}">{{ $language->title }}</a>
                        </li>
                    @endforeach
                </ul>

                <div class="tab-content">
                    @foreach($data['translations'] as $translation)
                        @if($translation->translanguage->active)
                        <div id="{{ $translation->translanguage->code }}"
                             class="tab-pane fade in {{ ($translation->translanguage->code == 'en')? 'active':'' }}">
                            <br/>
                            {{ Form::hidden('translations['.$translation->translanguage->code.'][id]', $translation->id , []) }}
                            {{ Form::hidden('translations['.$translation->translanguage->code.'][languages_id]', $translation->translanguage->id , []) }}

                        <!-- Start Title -->
                            <div class="form-group">
                                {{ Form::label('translations['.$translation->translanguage->code.'][title]', 'Title', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$translation->translanguage->code.'][title]', $translation->title , ['class' => 'form-control required', 'maxlength' => '1000', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => 'Title']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Title -->

                            <!-- Start: Quick Facts -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][description]', 'Description', ['class' => 'col-lg-2 control-label description_input']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][description]', $translation->description, ['class' => 'form-control description_input description', 'placeholder' => 'Description']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Quick Facts -->

                            <!-- Start: best_time -->
                            <?php
                                // dd($abouts);
                                foreach ($abouts as $about) {
                                    if ($about['title'] == 'low_season') {
                                        $low_start_end = explode(",", $about['body']);
                                        $low_se = explode('-', $low_start_end[0]);
                                        // dd($low_se);
                                    } elseif ($about['title'] == 'shoulder') {
                                        $shoulder_start_end = explode(",", $about['body']);
                                        $shoulder_se = explode('-', $shoulder_start_end[0]);
                                    } elseif ($about['title'] == 'high_season') {
                                        $high_start_end = explode(",", $about['body']);
                                        $high_se = explode('-', $high_start_end[0]);
                                    }
                                }
                                $seasones = [
                                    'low_season' => [
                                        'title'     => "Low season",
                                        'start'     => "low_start",
                                        'end'       => "low_end",
                                        'start_end' => @$low_start_end
                                    ],
                                    'shoulder' => [
                                        'title'     => "Shoulder",
                                        'start'     => "shoulder_start",
                                        'end'       => "shoulder_end",
                                        'start_end' => @$shoulder_start_end
                                    ],
                                    'high_season' => [
                                        'title'     => "High season",
                                        'start'     => "high_start",
                                        'end'       => "high_end",
                                        'start_end' => @$high_start_end
                                    ]
                                ];
                            ?>
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('BestTime', 'Best time To Go', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-sm-10">
                                    <table class="table table-bordered">
                                        <colgroup>
                                            <col span="1" style="width: 15%;">
                                            <col span="1" style="width: 85%;">
                                        </colgroup>
                                       @foreach ($seasones as $key => $season)
                                        <tr>
                                            
                                            <td>{{$season['title']}}</td>
                                            <td>
                                                <table class="table table-bordered">
                                                    <colgroup>
                                                        <col span="1" style="width: 48%;">
                                                        <col span="1" style="width: 48%;">
                                                        <col span="1" style="width: 4%;">
                                                    </colgroup>
                                                    <tr>
                                                        <th>Start</th>
                                                        <th>End</th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            {{ Form::selectMonth('best_time['.$key.']['.$season['start'].'][]', null, ['id' => $season['start'], 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                                                            {{ Form::selectMonth('', null,['id' => 'monthSelectHidden-'.$season['start'], 'placeholder' => 'select month', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                                                        </td>
                                                        <td>
                                                            {{ Form::selectMonth('best_time['.$key.']['.$season['end'].'][]', null, ['id' => $season['end'], 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                                                            {{ Form::selectMonth('', null,['id' => 'monthSelectHidden-'.$season['end'], 'placeholder' => 'select month', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0);" id="add_icon_{{$key}}" class="btn btn-success btn-xs add_icon">+</a>
                                                        </td>
                                                    </tr>
                                                    @if (isset($season['start_end']))
                                                        @foreach ($season['start_end'] as $index => $item)
                                                        <?php
                                                            // if ($key == 0) continue;
                                                            $start_end = explode('-', $item);
                                                            // dd($start_end);
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                {{ Form::selectMonth('best_time['.$key.']['.$season['start'].'][]', $start_end[0], ['id' => $index.$season['start'], 'placeholder' => 'select month', 'class' => 'best_season_start select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                                                                {{ Form::selectMonth('', $start_end[0], ['id' => 'monthSelectHidden-'.$index.$season['start'], 'placeholder' => 'select month', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                                                            </td>
                                                            <td>
                                                                {{ Form::selectMonth('best_time['.$key.']['.$season['end'].'][]', $start_end[1], ['id' => $index.$season['end'], 'placeholder' => 'select month', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%']) }}
                                                                {{ Form::selectMonth('', $start_end[1], ['id' => 'monthSelectHidden-'.$index.$season['end'], 'placeholder' => 'select month', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'select month']) }}
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row" style="width:100%">‐</a>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    @endif
                                                    <tbody id="addMoreRows{{$key}}"></tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        @endforeach
                                        
                                    </table>
                                </div>
                            </div><!--form control-->

                            <!-- End: best_time -->
                            <!-- Start: Daily Costs -->
                            <?php
                                // dd($abouts);
                                foreach ($abouts as $about) {
                                    if ($about['title'] == 'budget') {
                                        $budget = $about['body'];
                                    } elseif ($about['title'] == 'mid_range') {
                                        $mid_range = $about['body'];
                                    } elseif ($about['title'] == 'high_range') {
                                        $high_range = $about['body'];
                                    }
                                    
                                    
                                }
                            ?>
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('DailyCosts', 'Daily Costs', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-sm-10">
                                    <table class="table table-bordered">
                                        <colgroup>
                                            <col span="1" style="width: 30%;">
                                            <col span="1" style="width: 70%;">
                                        </colgroup>
                                        <tr>
                                            <th>Status</th>
                                            <th>Amount of money</th>
                                        </tr>
                                        <tr>
                                            <td>Budget</td>
                                            <td>
                                                {{ Form::input('text' ,'daily_costs[budget]', isset($budget) ? $budget : "", ['class' => 'form-control', 'placeholder' => 'Enter the amount and currency in that country']) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Mid-Range</td>
                                            <td>
                                                {{ Form::input('text' ,'daily_costs[mid_range]', isset($mid_range) ? $mid_range : "", ['class' => 'form-control', 'placeholder' => 'Enter the amount and currency in that country']) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>High-Range</td>
                                            <td>
                                                {{ Form::input('text' ,'daily_costs[high_range]', isset($high_range) ? $high_range : "", ['class' => 'form-control', 'placeholder' => 'Enter the amount and currency in that country']) }}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div><!--form control-->
                            <!-- End: Daily Costs -->
                            <!-- Start: Transportation -->
                            <?php
                            // dd($abouts);
                                foreach ($abouts as $about) {
                                    if ($about['type'] == 'transportation') {
                                        $transports = explode(",", $about['body']);
                                    }
                                }
                            ?>
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('transportation[transportation]', 'Transportation', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-sm-10">
                                    <table class="table table-bordered">
                                        <colgroup>
                                            <col span="1" style="width: 75%;">
                                            <col span="1" style="width: 25%;">
                                        </colgroup>
                                        <tr>
                                            <th>description of transport</th>
                                            <th>add or remove transport</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div>
                                                    {{ Form::input('text' ,'transportation[transports][]', '', ['class' => 'form-control', 'id' => 'transport', 'placeholder' => 'Enter transportation in that city']) }}
                                                </div>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" id="add_icon_transportation" class="btn btn-success btn-xs add_icon" style="width:100%">+</a>
                                            </td>
                                        </tr>
                                        @if (isset($transports))
                                            @foreach ($transports as $transport)
                                                @if ($transport != '')
                                                <tr>
                                                    <td>
                                                        <div>
                                                            {{ Form::input('text' ,'transportation[transports][]', $transport, ['class' => 'form-control', 'id' => 'transport_2', 'placeholder' => 'Enter transportation in that city']) }}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row" style="width:100%">&dash;</a>
                                                    </td>
                                                </tr>   
                                                @endif
                                            @endforeach
                                        @endif
                                        <tbody id="addMoreRowsTransportation"></tbody>
                                    </table>
                                </div>
                            </div><!--form control-->

                            <!-- End: Transportation -->
                            <!-- Start: Sockets & Plugs -->
                            <?php
                            // dd($abouts);
                                $typeSockets = [];
                                foreach ($abouts as $about) {
                                    if ($about['type'] == 'sockets_plugs') {
                                        $typeSockets = explode(",", $about['body']);
                                    }
                                }
                                $types = [
                                    'Type A/100 – 127 V',
                                    'Type B/100 – 127 V',
                                    'Type C/220 – 240 V',
                                    'Type D/220 – 240 V',
                                    'Type E/220 – 240 V',
                                    'Type F/220 – 240 V',
                                    'Type G/220 – 240 V',
                                    'Type I/220 – 240 V',
                                    'Type J/220 – 240 V',
                                    'Type K/220 – 240 V',
                                    'Type L/220 – 240 V',
                                    'Type M/220 – 240 V',
                                    'Type N/100 – 240 V',
                                    'Type O/220 – 240 V'
                                ];
                            ?>
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('Sockets', 'Sockets & Plugs', ['class' => 'col-lg-2 control-label']) }}
                                <div class="col-sm-10">
                                    <table class="table table-bordered">
                                        @foreach ($types as $item)
                                        <tr>
                                            <td>{{ Form::label('sockets_plugs[]', $item, ['class' => 'col-lg-12 control-label']) }}</td>
                                            <td>{{ Form::checkbox('sockets_plugs[]', $item, in_array($item, @$typeSockets) ? true : false) }}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <!-- End: Sockets & Plugs -->

                            <!-- Start: cost_of_living -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][cost_of_living]', 'Cost of Living Index', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][cost_of_living]', $translation->cost_of_living, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Cost of Living Index']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: cost_of_living -->

                            <!-- Start: geo_stats -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][geo_stats]', 'Crime Rate Index', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][geo_stats]', $translation->geo_stats, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Crime Rate Index']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: geo_stats -->

                            <!-- Start: demographics -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][demographics]', 'Quality of Life Index', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][demographics]', $translation->demographics, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => 'Quality of Life Index']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: demographics -->

                            <!-- Start: Metrics -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][metrics]', 'System of Measurement', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][metrics]', $translation->metrics, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => '']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Metrics -->


                            {{--<!-- Start: Accommodation -->--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-12 textarea-msg"></div>--}}
                                {{--{{ Form::label('translations['.$translation->translanguage->code.'][accommodation]', 'Accommodation', ['class' => 'col-lg-2 control-label']) }}--}}

                                {{--<div class="col-lg-10">--}}
                                    {{--{{ Form::textarea('translations['.$translation->translanguage->code.'][accommodation]', $translation->accommodation, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => '']) }}--}}
                                {{--</div><!--col-lg-10-->--}}
                            {{--</div><!--form control-->--}}
                            {{--<!-- End: Accommodation -->--}}
                            {{--<!-- Start: Local Poisons -->--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-12 textarea-msg"></div>--}}
                                {{--{{ Form::label('translations['.$translation->translanguage->code.'][local_poisons]', 'Local Poisons', ['class' => 'col-lg-2 control-label']) }}--}}

                                {{--<div class="col-lg-10">--}}
                                    {{--{{ Form::textarea('translations['.$translation->translanguage->code.'][local_poisons]', $translation->local_poisons, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => '']) }}--}}
                                {{--</div><!--col-lg-10-->--}}
                            {{--</div><!--form control-->--}}
                            {{--<!-- End: Local Poisons -->--}}
                            {{-- <!-- Start: Sockets & Plugs --> --}}
                            {{-- <div class="form-group"> --}}
                                {{-- <div class="col-sm-12 textarea-msg"></div> --}}
                                {{-- {{ Form::label('translations['.$translation->translanguage->code.'][sockets]', 'Sockets & Plugs', ['class' => 'col-lg-2 control-label']) }} --}}

                                {{-- <div class="col-lg-10"> --}}
                                    {{-- {{ Form::textarea('translations['.$translation->translanguage->code.'][sockets]', $translation->sockets, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => '']) }} --}}
                                {{-- </div><!--col-lg-10--> --}}
                            {{-- </div><!--form control--> --}}
                            {{-- <!-- End: Sockets & Plugs --> --}}
                            <!-- Start: Working days -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][working_days]', 'Working Days', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][working_days]', $translation->working_days, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => '']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Working days -->
                            <!-- Start: Internet Accessibility -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][internet]', 'Internet Accessibility', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][internet]', $translation->internet, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => '']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Internet Accessibility -->

                            <!-- Start: Pollution Index -->
                            <div class="form-group">
                                <div class="col-sm-12 textarea-msg"></div>
                                {{ Form::label('translations['.$translation->translanguage->code.'][pollution_index]', 'Pollution Index', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::textarea('translations['.$translation->translanguage->code.'][pollution_index]', $translation->pollution_index, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => '']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Pollution Index -->
                            {{-- <!-- Start: Transportation --> --}}
                            {{-- <div class="form-group"> --}}
                                {{-- <div class="col-sm-12 textarea-msg"></div> --}}
                                {{-- {{ Form::label('translations['.$translation->translanguage->code.'][transportation]', 'Transportation', ['class' => 'col-lg-2 control-label']) }} --}}

                                {{-- <div class="col-lg-10"> --}}
                                    {{-- {{ Form::textarea('translations['.$translation->translanguage->code.'][transportation]', $translation->transportation, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => '']) }} --}}
                                {{-- </div><!--col-lg-10--> --}}
                            {{-- </div><!--form control--> --}}
                            <!-- End: Transportation -->
                            {{--<!-- Start: Country Highlights -->--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-12 textarea-msg"></div>--}}
                                {{--{{ Form::label('translations['.$translation->translanguage->code.'][highlights]', 'Country Highlights', ['class' => 'col-lg-2 control-label']) }}--}}

                                {{--<div class="col-lg-10">--}}
                                    {{--{{ Form::textarea('translations['.$translation->translanguage->code.'][highlights]', $translation->highlights, ['class' => 'form-control', 'maxlength' => 5000, 'placeholder' => '']) }}--}}
                                {{--</div><!--col-lg-10-->--}}
                            {{--</div><!--form control-->--}}
                            {{--<!-- End: Country Highlights -->--}}
                            <div class="form-group">
                                {{ Form::label('translations['.$translation->translanguage->code.'][population]', 'Population', ['class' => 'col-lg-2 control-label']) }}

                                <div class="col-lg-10">
                                    {{ Form::text('translations['.$translation->translanguage->code.'][population]', $translation->population, ['class' => 'form-control', 'maxlength' => 1000, 'placeholder' => 'Population']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <!-- End: Population -->
                        </div>
                        @endif
                    @endforeach
                    <!-- Start: Abouts -->
                    <?php
                        $citiesAbouts = [
                            'Restrictions',
                            'Planning Tips',
                            'Health Notes',
                            'Potential Dangers',
                            'Speed Limit',
                            'Etiquette'
                        ];
                    ?>
                    @foreach ($citiesAbouts as $item)
                        <?php
                        $appendId = str_replace(' ', '', $item);
                        $item_name = strtolower($item);
                        $item_name = str_replace(' ', '_', $item_name);
                        ?>
                    <div class="form-group">
                        <div class="col-sm-12 textarea-msg"></div>
                        {{ Form::label('abouts[type][]', $item, ['class' => 'col-lg-2 control-label']) }}
                        <div class="col-sm-10">
                            <table class="table table-bordered">
                                <colgroup>
                                    <col span="1" style="width: 26%;">
                                    <col span="1" style="width: 70%;">
                                    <col span="1" style="width: 4%;">
                                </colgroup>
                                <tr>
                                    <th>Title</th>
                                    <th class="set_last_col">Body</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-lg-12">
                                            {{ Form::text('abouts[title][]', '', ['id' => $item_name.'_title','class' => 'form-control', 'maxlength' => 500, 'placeholder' => '']) }}
                                        </div><!--col-lg-12-->
                                    </td>
                                    <td>
                                        <div class="col-lg-12">
                                            {{ Form::hidden('abouts[type][]', $item_name , ['id' => $item_name.'_type']) }}
                                            {{ Form::text('abouts[body][]', '', ['id' => $item_name, 'class' => 'form-control', 'maxlength' => 5000, 'placeholder' => '']) }}
                                        </div><!--col-lg-12-->
                                    </td>
                                    <td>
                                        <a href="javascript:void(0);" id="add_icon_{{$item_name}}" class="btn btn-success btn-xs add_icon">+</a>
                                    </td>
                                </tr>
                                @if ( !empty($abouts) )
                                    @foreach ($abouts as $about)
                                        @if ($about['type'] == $item_name)
                                        <tr>
                                            <td>
                                                <div class="col-lg-12">
                                                    {{ Form::text('abouts[title][]', $about['title'], ['id' => $item_name.'_title','class' => 'form-control', 'maxlength' => 500, 'placeholder' => '']) }}
                                                </div><!--col-lg-12-->
                                            </td>
                                            <td>
                                                <div class="col-lg-12">
                                                    {{ Form::hidden('abouts[type][]', $item_name , ['id' => $item_name.'_type']) }}
                                                    {{ Form::text('abouts[body][]', $about['body'], ['id' => $item_name, 'class' => 'form-control', 'maxlength' => 5000, 'placeholder' => '']) }}
                                                </div><!--col-lg-12-->
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                @endif
                                <tbody id="addMoreRows{{$appendId}}"></tbody>
                            </table>
                        </div>
                    </div>
                    @endforeach
                    <!-- End: Abouts -->
                </div>

                <!-- Start: code -->
                <div class="form-group">
                    {{ Form::label('code', 'City Code', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::input('number' ,'code', $data['code'], ['class' => 'form-control', 'maxlength' => '3', 'placeholder' => 'City Code']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- End: code -->
                <!-- Active: Start -->
                <div class="form-group">
                    {{ Form::label('title', trans('validation.attributes.backend.access.languages.active'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        @if($data['active'] == 1)
                            {{ Form::checkbox('active', $data['active'] , true) }}
                        @else
                            {{ Form::checkbox('active', $data['active'] , false) }}
                        @endif
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Active: End -->
                <!-- Active: Is Capital -->
                <div class="form-group">
                    {{ Form::label('title', 'Is Capital?', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        @if($data['is_capital'] == 1)
                            {{ Form::checkbox('is_capital', $data['is_capital'] , true) }}
                        @else
                            {{ Form::checkbox('is_capital', $data['is_capital'] , false) }}
                        @endif
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Is Capital: End -->

                <!-- Country: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Country', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('countries_id', $countries , $data['countries_id'],['class' => 'select2Class form-control country-input']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Country: End -->

                <div class="form-group">
                    {{ Form::label('title', 'Select Location', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        <input id="pac-input" class="form-control" type="text" placeholder="Search Box">
                        <input id="countryInfo" class="form-control" type="hidden" value="{{ json_encode($countriesLocation) }}">
                        <input id="cityInfo" class="form-control" type="hidden" value="{{ json_encode($city) }}">
                        <div id="map"></div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('title', 'Lat,Lng', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::hidden('lat_lng', $data['lat_lng'], ['class' => 'form-control disabled', 'id' => 'lat-lng-input', 'placeholder' => 'Lat,Lng']) }}
                        {{ Form::text('lat_lng_show', $data['lat_lng'], ['class' => 'form-control disabled', 'id' => 'lat-lng-input_show', 'placeholder' => 'Lat,Lng' , 'disabled' => 'disabled']) }}
                    </div>
                </div>

                <!-- Currencies: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Currencies', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::select('currencies_id[]', isset($currencies) ? $currencies : [] , isset($selected_currencies) ? $selected_currencies : [],['class' => 'select2Class form-control', 'multiple' => 'multiple', 'data-placeholder' => 'Choose Currencies...']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Currencies: End -->

                <!-- Religions: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Religions', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::select('religions_id[]', isset($religions) ? $religions : [] , isset($selected_religions) ? $selected_religions : [],['class' => 'select2Class form-control' , 'multiple' => 'multiple']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Religions: End -->

                <!-- Lifestyles: Start -->
                <div class="form-group">
                    {{ Form::label('title', ' Travel Styles', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        <table class="table table-bordered">
                            <tr>
                                <th>Lifestyle</th>
                                <th>Rating</th>
                                <th class="set_last_col">Action</th>
                            </tr>
                            <tr>
                                <td width="70%">
                                    {{ Form::select('lifestyle_id[]', $data['selected_lifestyles']['all_lifestyles'] , null ,['id' => 'lifestyleSelect', 'placeholder' => 'Select a Lifestyle', 'class' => 'select2Class form-control multi-select2' , 'style' => 'width: 100%', 'data-placeholder' => 'Choose Lifestyle...']) }}
                                    {{ Form::select('', $data['selected_lifestyles']['all_lifestyles'] , null, ['id' => 'lifestylesSelectHidden', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'Choose Lifestyle...']) }}
                                </td>
                                <td>
                                    {{ Form::number('lifestyle_rating[]', null, ['class' => 'form-control','min' => '0', 'max' => '10', 'step' => '1', 'autofocus' => 'autofocus']) }}
                                    {{ Form::number('', null, ['id' => 'lifestyleRatingHidden', 'style' => 'display: none;', 'class' => 'form-control', 'min' => '0', 'max' => '10', 'step' => '1','autofocus' => 'autofocus']) }}
                                </td>
                                <td>
                                    <a href="javascript:void(0);" id="add_icon_lifestyle" class="btn btn-success btn-xs add_icon">+</a>
                                </td>
                            </tr>
                            @if(isset($data['selected_lifestyles']['ids']))
                                @foreach($data['selected_lifestyles']['rating'] as $lifestyle_id => $lifestyle_rating)
                                    <tr>
                                        <td width="70%">
                                            {{ Form::select('lifestyles_id[]', $data['selected_lifestyles']['all_lifestyles'], $lifestyle_id ?: [] , ['id' => 'lifestylesSelect'.$lifestyle_id, 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%', 'data-placeholder' => 'Choose Lifestyles...']) }}
                                            {{ Form::select('lifestyles_id[]', $data['selected_lifestyles']['all_lifestyles'], $lifestyle_id ?: [], ['id' => 'lifestylesSelectHidden', 'name' => 'lifestyles['.$lifestyle_id.']', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'Choose Lifestyles...']) }}
                                        </td>
                                        <td>
                                            {{ Form::number('lifestyles_rating[]', $lifestyle_rating,  ['class' => 'form-control','min' => '0', 'max' => '10', 'step' => '1', 'required' => 'required', 'autofocus' => 'autofocus']) }}
                                            {{ Form::number('', $lifestyle_rating, ['id' => 'lifestyleRatingHidden', 'style' => 'display: none;', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                            <tbody id="addMoreRowsLifestyle"></tbody>
                        </table>
                    </div>
                </div>
                <!-- Lifestyles: End -->

                <!-- Official Languages: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Official Languages', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('languages_spoken_id[]', isset($spoken_languages) ? $spoken_languages : [], isset($selected_languages_spoken) ? $selected_languages_spoken : [],['class' => 'select2Class form-control', 'multiple' => 'multiple', 'data-placeholder' => 'Choose Official Languages...']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Official Languages: End -->

                <!-- Additional Languages: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Additional Languages', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::select('additional_languages_spoken_id[]', isset($spoken_languages) ? $spoken_languages : [], isset($additional_selected_languages_spoken) ? $additional_selected_languages_spoken : [],['class' => 'select2Class form-control', 'multiple' => 'multiple', 'data-placeholder' => 'Choose Additional Languages...']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Additional Languages: End -->
            <!--Holidays start-->
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="col-xs-12 alert alert-warning alert-dismissible fade in">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Warning!</strong> <span class="set_error_msg"></span>
                        </div>
                    </div>
                    {{ Form::label('title', 'Holidays', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <table class="table table-bordered">
                            <tr>
                                <th>Holidays list</th>
                                <th>Date</th>
                                <th class="set_last_col">Action</th>
                            </tr>
                            <tr>
                                <td width="70%">
                                    <div class="holiday_form">
                                        {{ Form::select('holidays_id[]', $data['selected_holidays']['all_holidays'], null,['id' => 'holidaysSelect', 'placeholder' => 'Select a Holiday', 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%', 'data-placeholder' => 'Choose Holidays...', 'data-url' => url('admin/holidays')]) }}
                                        {{ Form::select('', $data['selected_holidays']['all_holidays'], null,['id' => 'holidaysSelectHidden', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'Choose Holidays...']) }}
                                    </div><!--col-lg-10-->
                                </td>
                                <td>
                                    {{ Form::text('holidays_date[]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}
                                    {{ Form::text('', null, ['id' => 'holidaysDateHidden', 'style' => 'display: none;', 'class' => 'form-control', 'autofocus' => 'autofocus']) }}
                                </td>

                                <td>
                                    <a href="javascript:void(0);" id="add_icon_holiday" class="btn btn-success btn-xs add_icon">+</a>
                                </td>
                            </tr>
                            @if(isset($data['selected_holidays']['ids']))
                                @foreach($data['selected_holidays']['holiday_date'] as $holiday_id => $holiday_date)
                                    <tr>
                                        <td width="70%">
                                            <div class="holiday_form">
                                                {{ Form::select('holidays_id[]', $data['selected_holidays']['all_holidays'], $holiday_id ? $holiday_id : [], ['id' => 'holidaysSelect'.$holiday_id, 'class' => 'select2Class form-control multi-select2', 'style' => 'width: 100%', 'data-placeholder' => 'Choose Holidays...', 'data-url' => url('admin/holidays')]) }}
                                                {{ Form::select('holidays_id[]', $data['selected_holidays']['all_holidays'], $holiday_id ? $holiday_id : [], ['id' => 'holidaysSelectHidden', 'name' => 'holidays[]', 'style' => 'display: none; width: 100%', 'data-placeholder' => 'Choose Holidays...']) }}
                                            </div><!--col-lg-10-->
                                        </td>
                                        <td>
                                            {{ Form::text('holidays_date[]', $holiday_date, ['class' => 'form-control required', 'autofocus' => 'autofocus', 'required' => 'required']) }}
                                            {{ Form::text('', null, ['id' => 'holidaysDateHidden', 'style' => 'display: none;', 'class' => 'form-control', 'autofocus' => 'autofocus']) }}
                                        </td>

                                        <td>
                                            <a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                            <tbody id="addMoreRowsHoliday"></tbody>
                        </table>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!--Holidays end-->
                <!-- EmergencyNumbers: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Emergency Numbers', ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::select('emergency_numbers_id[]', isset($numbers) ? $numbers : [], isset($emergency_numbers) ? $emergency_numbers : [],['class' => 'select2Class form-control', 'multiple' => 'multiple', 'data-placeholder' => 'Choose Emergency Numbers...']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- EmergencyNumbers: End -->

                <!-- Images: Start -->
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="col-xs-12 alert alert-warning alert-dismissible fade in">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Warning!</strong> <span class="set_error_msg"></span>
                        </div>
                    </div>
                    {{ Form::label('title', 'Upload Images', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        <table class="table table-bordered">
                            <tr>
                                <th>Select File</th>
                                <th>Title</th>
                                <th>Author Name</th>
                                <th>Author Link</th>
                                <th>Source Link</th>
                                <th>License Name</th>
                                <th>License Link</th>
                                <th>Delete</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td class="set_img_col">{{ Form::file('upload_img',[ 'name' => 'license_images[-1][image]']) }}</td>
                                <td>{{ Form::text('license_images[-1][title]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                <td>{{ Form::text('license_images[-1][author_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                <td>{{ Form::text('license_images[-1][author_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                <td>{{ Form::text('license_images[-1][source_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                <td>{{ Form::text('license_images[-1][license_name]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                <td>{{ Form::text('license_images[-1][license_url]', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                <td>
                                    
                                </td>
                                <td><a href="#javascript" id="add_icon" class="btn btn-success btn-xs add_icon">+</a>
                                    <input type="hidden" id="cnt" value="-1" />
                                </td>
                            </tr>
                            @if( !empty($media_results) )
                                @foreach( $media_results as $rowmedia)
                                    <tr>
                                        <td class="set_img_col">
                                            @if( !empty($rowmedia->url) )
                                                <div class="col-md-2" id="set_delete{{$rowmedia->id}}">
                                                    <!-- Cover delete Icon -->
                                                    <i id="delete_covers"
                                                       onclick="delete_additional_img('{{$rowmedia->url}}', '{{$rowmedia->id}}')"
                                                       class="setDeleteIc zoom-effect-icon fa fa-times image-hide"
                                                       data-toggle="tooltip" rel="tooltip" title="" data-id="" aria-hidden="true"
                                                       state="2" start-check="1" data-original-title="Remove Image"></i>
                                                    <a href="https://s3.amazonaws.com/travooo-images2/{{$rowmedia->url}}"
                                                       target='_blank'>
                                                        <img class=""
                                                             src="https://s3.amazonaws.com/travooo-images2/{{$rowmedia->url}}"
                                                             width="100" height="100"/>
                                                    </a>
                                                </div>
                                            @endif
                                        </td>
                                        <td>{{ Form::text("license_images[".$rowmedia['id']."][title]", $rowmedia['title'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text("license_images[".$rowmedia['id']."][author_name]", $rowmedia['author_name'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text("license_images[".$rowmedia['id']."][author_url]", $rowmedia['author_url'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text("license_images[".$rowmedia['id']."][source_url]", $rowmedia['source_url'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text("license_images[".$rowmedia['id']."][license_name]", $rowmedia['license_name'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td>{{ Form::text("license_images[".$rowmedia['id']."][license_url]", $rowmedia['license_url'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}</td>
                                        <td><input type="checkbox" name="del[]" value="{{$rowmedia['id']}}" /></td>
                                        <td><a href="#javascript" id="add_icon" class="btn btn-success btn-xs add_icon">+</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            <tbody id="addMoreRows"></tbody>
                        </table>
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Images: End -->
        @endif
        <!-- Languages Tabs: End -->
        </div>
    </div>
    <div class="box box-info">
        <div class="box-body">
            <div class="pull-left">
                {{ link_to_route('admin.location.city.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs submit_button']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->

    {{ Form::close() }}
@endsection

@section('after-scripts')
    <style>
        .zoom-effect-icon:hover {
            font-size: 17px;
        }

        .image-hide {
            display: none;
        }
    </style>
@endsection
