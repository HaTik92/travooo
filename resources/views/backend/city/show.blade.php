@extends ('backend.layouts.app')

@section ('title', 'Cities Management' . ' | ' . 'View City')

@section('page-header')
    <h1>
        Cities Management
        <small>View City</small>
    </h1>
@endsection

@section('after-styles')
    <style type="text/css">
        td.description {
            word-break: break-all;
        }
        #map {
            height: 300px;
        }
    </style>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">View City</h3>

            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.city-header-buttons-view')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">
                
                <table class="table table-striped table-hover">
                    @foreach($citytrans as $key => $city_translation)
                        <tr> <th> <h3 style="color:#0A8F27">{{ $city_translation->translanguage->title }}</h3> </th><td></td> </tr>
                        <tr>
                            <th>Title <small>({{ $city_translation->translanguage->title }})</small></th>
                            <td>{{ $city_translation->title }}</td>
                        </tr>
                        <tr>
                            <th>Description <small>({{ $city_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$city_translation->description?></p></td>
                        </tr>
                        <tr>
                            <th>Cost of Living Index <small>({{ $city_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$city_translation->cost_of_living?></p></td>
                        </tr>
                        <tr>
                            <th>Crime Rate Index <small>({{ $city_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$city_translation->geo_stats?></p></td>
                        </tr>
                        <tr>
                            <th>Quality of Life Index <small>({{ $city_translation->translanguage->title }})</small></th>
                            <td class="description"><p><?=$city_translation->demographics?></p></td>
                        </tr>
                    @endforeach
                    <?php
                        $month = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                    ?>
                    <tr>
                        <th>Best Time to Travel</th>
                        <td class="description">
                        @foreach ($cityabouts as $key => $city_about)
                            @if ($city_about->type == 'best_time')
                                <?php
                                    $start_end = explode(",", $city_about->body);
                                    $title = str_replace('_', ' ', ucfirst($city_about->title));
                                ?>
                                @if (count($start_end) > 1)
                                    @foreach ($start_end as $item)
                                        <?php
                                            $item = explode("-", $item);
                                        ?>
                                    <p><b>{{$title}}:</b> {{$month[$item[0]]}}-{{$month[$item[1]]}}</p>
                                    @endforeach                                
                                @else
                                    <?php
                                        $start_end = explode("-", $city_about->body);
                                    ?>
                                    <p><b>{{$title}}:</b> {{$month[$start_end[0]]}}-{{$month[$start_end[1]]}}</p>
                                @endif
                            @endif
                        @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Restrictions </th>
                        <td class="description">
                        @foreach ($cityabouts as $city_about)
                            @if ($city_about['type'] == 'restrictions')
                            <p><b>{{ $city_about['title'] }}: </b>{{ $city_about['body'] }}</p>
                            @endif
                        @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Packing Tips </th>
                        <td class="description">
                        @foreach ($cityabouts as $city_about)
                            @if ($city_about['type'] == 'planning_tips')
                            <p><b>{{ $city_about['title'] }}: </b>{{ $city_about['body'] }}</p>
                            @endif
                        @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Health Notes </th>
                        <td class="description">
                        @foreach ($cityabouts as $city_about)
                            @if ($city_about['type'] == 'health_notes')
                            <p><b>{{ $city_about['title'] }}: </b>{{ $city_about['body'] }}</p>
                            @endif
                        @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Potential Dangers </th>
                        <td class="description">
                        @foreach ($cityabouts as $city_about)
                            @if ($city_about['type'] == 'potential_dangers')
                            <p><b>{{ $city_about['title'] }}: </b>{{ $city_about['body'] }}</p>
                            @endif
                        @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Speed Limit </th>
                        <td class="description">
                        @foreach ($cityabouts as $city_about)
                            @if ($city_about['type'] == 'speed_limit')
                            <p><b>{{ $city_about['title'] }}: </b>{{ $city_about['body'] }}</p>
                            @endif
                        @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>Etiquette </th>
                        <td class="description">
                        @foreach ($cityabouts as $city_about)
                            @if ($city_about['type'] == 'etiquette')
                            <p><b>{{ $city_about['title'] }}: </b>{{ $city_about['body'] }}</p>
                            @endif
                        @endforeach
                        </td>
                    </tr>
                    <tr>
                         <th> <h3 style="color:#0A8F27">Common Fields</h3></th><td></td>   
                    </tr>
                    <tr>
                        <th>City Code </th>
                        <td> <p><?=$city->code?></p> </td>
                    </tr>
                    <tr>
                        <th>Country </th>
                        <td> <p><?=$country->title?></p> </td>
                    </tr>
                    <tr>
                        <th>Active </th>
                        <td>
                            @if($city->active == 1) 
                              <p><label class="label label-success">Active</label></p> 
                            @else
                              <p><label class="label label-danger">Deactive</label></p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Is Capital? </th>
                        <td>
                            @if($city->is_capital == 1) 
                              <p><label class="label label-success">Yes</label></p> 
                            @else
                              <p><label class="label label-danger">No</label></p>
                            @endif
                        </td>
                    </tr>

                    <!-- Start: Currencies -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Currencies</h3></th><td></td>   
                    </tr>
                    @if(empty($currencies))
                      <tr>
                          <th> <p>No Currencies Added.</p> </th>
                      </tr>
                    @endif
                    @foreach($currencies as $key => $currency)
                      <tr>
                          <th> <p><?=$currency?></p> </th>
                      </tr>
                    @endforeach
                    <!-- End: Currencies -->

                    <!-- Start: Official Languages -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Official Languages</h3></th><td></td>   
                    </tr>
                    @if(empty($languagesSpoken))
                      <tr>
                          <th> <p>No Official Languages Added.</p> </th>
                      </tr>
                    @endif
                    @foreach($languagesSpoken as $key => $language_spoken)
                      <tr>
                          <th> <p><?=$language_spoken?></p> </th>
                      </tr>
                    @endforeach
                    <!-- End: Official Languages -->

                    <!-- Start: Additional Languages -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Additional Languages</h3></th><td></td>   
                    </tr>
                    @if(empty($additionalLanguagesSpoken))
                      <tr>
                          <th> <p>No Additional Languages Added.</p> </th>
                      </tr>
                    @endif
                    @foreach($additionalLanguagesSpoken as $key => $language_spoken)
                      <tr>
                          <th> <p><?=$language_spoken?></p> </th>
                      </tr>
                    @endforeach
                    <!-- End: Additional Languages -->

                        <tr>
                            <th> <h3 style="color:#0A8F27">Holidays and date</h3></th><td></td>
                        </tr>
                        @if(empty($holidays))
                            <tr>
                                <th> <p>No Holidays Added.</p> </th><td></td>
                            </tr>
                        @endif
                        @foreach($holidays as $key => $holiday)
                            <tr>
                                <th> <p><?=$holiday?></p> </th>
                                <td>{{$holidaysDate[$key]['date']}}</td>
                            </tr>
                        @endforeach
                    <!-- End: Holidays -->

                    <!-- Start: Medias -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Medias</h3></th><td></td>   
                    </tr>
                    @if(empty($medias))
                      <tr>
                          <th> <p>No Medias Added.</p> </th>
                      </tr>
                    @endif
                    @foreach($medias as $key => $media)
                      <tr>
                          <th> <p><?=$media?></p> </th>
                      </tr>
                    @endforeach
                    <!-- End: Medias -->

                    <!-- Start: Religions -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Religions</h3></th><td></td>   
                    </tr>
                    @if(empty($religions))
                      <tr>
                          <th> <p>No Religions Added.</p> </th>
                      </tr>
                    @endif
                    @foreach($religions as $key => $religion)
                      <tr>
                          <th> <p><?=$religion?></p> </th>
                      </tr>
                    @endforeach
                    <!-- End: Religions -->

                    <!-- Start: Suitable Travel Styles -->
                    <tr>
                         <th> <h3 style="color:#0A8F27"> Travel Styles</h3></th><td></td>
                    </tr>
                    @if(empty($lifestyles))
                      <tr>
                          <th> <p>No suitable travel styles added.</p> </th>
                      </tr>
                    @endif
                    @foreach($lifestyles as $key => $lifestyle)
                      <tr>
                          <th> <span><?=$lifestyle['title']?></span>&nbsp<span><?=$lifestyle['rating']?></span></th>
                      </tr>
                    @endforeach
                    <!-- End: Suitable Travel Styles -->

                    <!-- Start: Emergency Numbers -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Emergency Numbers</h3></th><td></td>   
                    </tr>
                    @if(empty($emergencynumbers))
                      <tr>
                          <th> <p>No Emergency Numbers Added.</p> </th>
                      </tr>
                    @endif
                    @foreach($emergencynumbers as $key => $number)
                      <tr>
                          <th> <p><?=$number?></p> </th>
                      </tr>
                    @endforeach
                    <!-- End: Emergency Numbers -->

                    <!-- Airports -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Airports Locations</h3></th><td></td>   
                    </tr>
                    @if(empty($airports))
                      <tr>
                          <th> <p>No Airports Locations Added.</p> </th>
                      </tr>
                    @endif
                    @foreach($airports as $key => $airport)
                      <tr>
                          <th> <p><?=$airport?></p> </th>
                      </tr>
                    @endforeach
                    <!-- End: Airports -->
                    
                    <!-- Start: Images -->
                    <tr>
                         <th> <h3 style="color:#0A8F27">Images</h3></th><td></td>   
                    </tr>
                </table>
                <div class="row">
                    @if(empty($images))
                        <div style="padding-left: 20px;"><p>No Images Added.</p></div>
                    @endif
                    @foreach($images as $key => $image)
                        <div class="col-md-2" style="margin-right: 20px;margin-top:10px;">
                            <a href="https://s3.amazonaws.com/travooo-images2/{{$image}}" style="width:170px;height:150px;"
                               target='_blank'>
                                <img class=""
                                     src="https://s3.amazonaws.com/travooo-images2/{{$image}}"
                                     width="100" height="100"/>
                            </a>
                        </div>
                    @endforeach
                </div>
                <!-- End: Images -->
                <!-- Cover Image Section - Start -->
                <table class="table table-striped table-hover" style="margin-top: 20px;">
                    <tr>
                        <th><h3 style="color:#0A8F27;">Cover Image</h3></th><td></td>
                    </tr>
                </table>
                <div class="row" style="margin-bottom: 10px;margin-bottom: 20px;">
                    @if(empty($cover))
                        <div style="padding-left: 20px;">No Cover Image Added.</div>
                    @else
                        <div style="padding-top: 10px;padding-left: 20px;"><img src="{{$cover['url']}}" style="width: 170px;height:150px;" /></div>
                    @endif
                </div>
                <!-- Cover Image Section - End -->
                <table class="table table-striped table-hover">
                    <tr>
                         <th> <h3 style="color:#0A8F27">City Location</h3></th><td></td>   
                    </tr>
                    <tr>
                        <th>Latitude , Longitude</th>
                        <td> <p><?=$city->lat?> , <?=$city->lng?></p> </td>
                    </tr>
                </table>
                <!-- Map will be created in the "map" div -->
                <div id="map"></div>
                <input id="cityInfo" class="form-control" type="hidden" value="{{ json_encode($city) }}">
                <!-- Map div end -->
            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
@endsection