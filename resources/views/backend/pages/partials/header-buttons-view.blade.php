<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.pages.index', 'All Pages', [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.pages.create', 'Create Pages', [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>