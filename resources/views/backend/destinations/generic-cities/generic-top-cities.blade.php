@extends ('backend.layouts.app')

@section('title', 'Generic Top Cities')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css") }}
    {{ Html::style("https://cdn.datatables.net/select/1.2.3/css/select.dataTables.min.css") }}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.destinations.generic_top_cities') }}
    </h1>
@endsection

@section('content')
    <div class="row deleted-box">
        <div class="col-md-12">
            <div class="deleted_msg"></div>
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.destinations.generic_top_cities') }}</h3>
            <div class="box-tools pull-right">
                @include('backend.destinations.partials.generic-top-cities-header')
            </div><!--box-tools pull-right-->
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table
                        id="GenericTopCitiesTable"
                        class="table table-condensed table-hover"
                        data-url="{{ route('admin.destinations.generic_top_cities_table')}}">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ trans('labels.backend.destinations.countries') }}</th>
                        <th>{{ trans('labels.backend.destinations.cities') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
