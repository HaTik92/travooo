<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.generic-top-cities.create', trans('labels.backend.destinations.create'), [], ['class' => 'btn btn-success btn-xs']) }}
</div><!--pull right-->

<div class="clearfix"></div>
