@extends ('backend.layouts.app')

@section ('title', 'Destination Manager' . ' | ' . 'Generic Top Places')

@section('page-header')
    <h1>
        Destination Manager
        <small>{{ trans('labels.backend.destinations.create_generic_top_places') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open([
            'id'     => 'generic_places_form',
            'route'  => 'admin.generic-top-places.store',
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'post'
        ])
    }}
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.destinations.create_generic_top_places') }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <!-- Countries: Start -->
            <div class="form-group">
                {{ Form::label('title', 'Country', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::select('country_id', $countries, null,['id' => 'countryForPlacesSelect', 'class' => 'select2Class form-control multi-select2',  'data-placeholder' => 'Choose Country...']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
            <!-- Countries: End -->

                <!-- Cities: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'City', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('city_id', $cities, null,['id' => 'cityForPlacesSelect', 'class' => 'select2Class form-control multi-select2',  'data-placeholder' => 'Choose City...']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Cities: End -->

            <!-- Places: Start -->
            <div class="form-group">
                {{ Form::label('title', 'Place', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::select('place_id', $places, null,['id' => 'genericPlacesSelect', 'class' => 'select2Class form-control multi-select2',  'data-placeholder' => 'Choose Place...']) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
            <!-- Places: End -->
        </div>
    </div>
    <div class="box box-info">
        <div class="box-body">
            <div class="pull-left">
                {{ link_to_route('admin.generic-top-places.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs submit_button']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
    {{ Form::close() }}
@endsection
