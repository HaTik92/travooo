@extends ('backend.layouts.app')

@section ('title', 'Destination Manager' . ' | ' . 'Generic Top Countries')

@section('page-header')
    <!-- <h1>
        {{ trans('labels.backend.access.users.management') }}
            <small>{{ trans('labels.backend.access.users.create') }}</small>
    </h1> -->
    <h1>
        Destination Manager
        <small>{{ trans('labels.backend.destinations.create_generic_top_countries') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open([
            'id'     => 'generic_countries_form',
            'route'  => 'admin.generic-top-countries.store',
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'post'
        ])
    }}
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.destinations.create_generic_top_countries') }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
                <!-- Countries: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Country', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('country_id', $countries, null,['id' => 'genericCountriesSelect', 'class' => 'select2Class form-control multi-select2',  'data-placeholder' => 'Choose Country...', 'data-url' => url('admin/location/city')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Countries: End -->
        </div>
    </div>
    <div class="box box-info">
        <div class="box-body">
            <div class="pull-left">
                {{ link_to_route('admin.generic-top-countries.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs submit_button']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
    {{ Form::close() }}
@endsection
