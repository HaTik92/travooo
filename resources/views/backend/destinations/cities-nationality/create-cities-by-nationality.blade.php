@extends ('backend.layouts.app')

@section ('title', 'Destination Manager' . ' | ' . 'Create Cities by Nationality')

@section('page-header')
    <!-- <h1>
        {{ trans('labels.backend.access.users.management') }}
            <small>{{ trans('labels.backend.access.users.create') }}</small>
    </h1> -->
    <h1>
        Destination Manager
        <small>{{ trans('labels.backend.destinations.create_cities_by_nationality') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open([
            'id'     => 'city_nationality_form',
            'route'  => 'admin.cities-by-nationality.store',
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'post'
        ])
    }}
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.destinations.create_cities_by_nationality') }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
                <!-- Nationality: Start -->
                <div class="form-group">
                    {{ Form::label('title', 'Nationality', ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::select('nationality_id', $countries, null,['id' => 'countryForCitiesSelect', 'class' => 'select2Class form-control multi-select2',  'data-placeholder' => 'Choose Nationality...', 'data-url' => url('admin/location/city')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
                <!-- Nationality: End -->

            <!-- Cities: Start -->
            <div class="form-group">
                {{ Form::label('title', 'Cities', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::select('cities_id[]', $cities, null,['id' => 'genericCitiesSelect', 'class' => 'select2Class form-control multi-select2', 'multiple' => 'multiple', 'data-placeholder' => 'Choose Cities...', 'data-url' => url('admin/location/city'), 'data-maximum-selection-length' => "10"]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
            <!-- Cities: End -->
        </div>
    </div>
    <div class="box box-info">
        <div class="box-body">
            <div class="pull-left">
                {{ link_to_route('admin.countries-by-nationality.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs submit_button']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
    {{ Form::close() }}
@endsection
