@extends ('backend.layouts.app')

@section ('title', 'Countries Manager' . ' | ' . 'Edit Country')

@section('page-header')
    <h1>
        Destination Manager
        <small>{{ trans('labels.backend.destinations.edit_country') }}</small>
    </h1>
@endsection

@section('after-styles')
    <style>
        .selecte-nationality{
            margin-bottom: 0;
            padding-top: 5px;
            font-size: 16px;
            font-weight: 500;
        }
    </style>
@endsection

@section('content')
    {{ Form::model($countries, [
            'id'     => 'country_update_form',
            'route'  => ['admin.countries-by-nationality.update', $nationalityid],
            'class'  => 'form-horizontal',
            'role'   => 'form',
            'method' => 'PATCH',
        ])
    }}

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.destinations.edit_countries_by_nationality') }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <!-- Nationality: Start -->
            <div class="form-group">
                {{ Form::label('title', 'Nationality', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    <p class="selecte-nationality">{{$selected_nationality}}</p>
                </div><!--col-lg-10-->
            </div><!--form control-->
            <!-- Nationality: End -->

            <!-- Countries: Start -->
            <div class="form-group">
                {{ Form::label('title', 'Countries', ['class' => 'col-lg-2 control-label']) }}

                <div class="col-lg-10">
                    {{ Form::select('countries_id[]',  $all_countries, $selected_countries, ['id' => 'countriesSelect', 'class' => 'select2Class form-control multi-select2', 'multiple' => 'multiple', 'data-placeholder' => 'Choose Countries...', 'data-url' => url('admin/location/city'), 'data-maximum-selection-length' => "10"]) }}
                </div><!--col-lg-10-->
            </div><!--form control-->
            <!-- Countries: End -->
        </div>
    </div>

    <div class="box box-info">
        <div class="box-body">
            <div class="pull-left">
                {{ link_to_route('admin.countries-by-nationality.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs submit_button']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
    {{ Form::close() }}
@endsection
