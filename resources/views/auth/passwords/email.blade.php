<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="/frontend_assets/css/style.css">
    <title>Travooo - forget pass</title>
</head>

<body>

<div class="forget-wrapper">
    <a href="{{url('home')}}" class="main-logo">
        <img src="{{asset('frontend_assets/image/main-logo.png')}}" alt="logo">
    </a>
    <div class="forget-main-block">
        <div class="forget-content {{ $errors->has('email') ? ' error-event' : '' }}">
            @if (session('status'))
                <h3 class="ttl">Email sent!</h3>
                <p>We sent a message to <a href="mailto:{{session('email')}}">{{session('email')}}</a> so you can pick your new password.</p>
                <p>Didn't get the email? <a href="{{url('help')}}" class="try-link">Try these tips from our Help Center</a></p>
                <p>Not your email address? <a href="{{url('password/email')}}" class="try-link">Try again</a></p>
            @else
            @if ($errors->has('email'))
                <div class="error-alert">
                    {{ $errors->first('email') }}
                </div>
            @endif
            <h3 class="ttl">Reset Password</h3>
            <p>Enter the username and the email address you used when you joined and we'll send you instructions to reset your password.</p>
            <form action="{{url('password/email')}}" method="POST" class="insert-form send-email-form" id="send_email_form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="field-row">
                    <div class="input-wrap {{ $errors->has('email') ? ' has-danger' : '' }}">
                        <input type="text" id="email" placeholder="Email Address or Username" name="email" value="{{ old('email') }}">
                    </div>
                    <button type="submit" class="btn btn-light-primary btn-bordered">Send</button>
                </div>
                <label id="email-error" class="error" for="email"></label>
            </form>
            @endif
        </div>
        <div class="forget-foot">
            <a href="{{ url('/login') }}" class="sign-in-link">Back to Sign In</a>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="foot-top">
        <ul class="aside-foot-menu">
            <li><a href="{{url('/')}}">About</a></li>
            <li><a href="{{url('/')}}">Careers</a></li>
            <li><a href="{{url('/')}}">Sitemap</a></li>
            <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
            <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
            <li><a href="{{url('/')}}">Contact</a></li>
            <li><a href="{{route('help.index')}}">Help Center</a></li>
        </ul>
    </div>
    <div class="foot-bottom">
        <p class="copyright">Travooo © {{date('Y')}}</p>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.js"></script>
<script src="/frontend_assets/js/script.js"></script>

</body>

</html>
