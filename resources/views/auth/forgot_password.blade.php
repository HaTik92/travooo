<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="/frontend_assets/css/style.css">
    <title>Travooo - forget pass</title>
</head>

<body>

<div class="forget-wrapper">
    <a href="#" class="main-logo">
        <img src="/frontend_assets/image/large-logo.png" alt="logo">
    </a>
    <div class="forget-main-block">
        <div class="forget-content">
            <h3 class="ttl">Reset Password</h3>
            <p>Enter the email address you used when you joined and we'll send you instructions to reset your password.</p>
            <form action="/" class="insert-form">
                <div class="field-row">
                    <div class="input-wrap">
                        <input type="text" placeholder="Email Address">
                    </div>
                    <button class="btn btn-light-primary btn-bordered">Send</button>
                </div>
            </form>
        </div>
        <div class="forget-foot">
            <a href="#" class="sign-in-link">Back to Sign In</a>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="foot-top">
        <ul class="aside-foot-menu">
            <li><a href="#">About</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Sitemap</a></li>
            <li><a href="{{route('page.privacy_policy')}}">Privacy</a></li>
            <li><a href="{{route('page.terms_of_service')}}">Terms</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
    </div>
    <div class="foot-bottom">
        <p class="copyright">Travooo © 2017</p>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="/frontend_assets/js/script.js"></script>

</body>

</html>
