import BaseComponent from "../core/baseComponent";

export default class HolidaysComponent extends BaseComponent {

    _initProperty () {
        this.holidaysTable = $('#holidays-table');
        if($('.row_old_image').length == 2){
            $(".upload_img input").prop('disabled', true);
        } else {
            $(".upload_img input").prop('disabled', false);
        }
    }

    get url() {
        return this.holidaysTable.data('url');
    }

    get config() {
        return this.holidaysTable.data('config');
    }

    _initBind () {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
        $(document).on('change', '.delete', this.disableEnableBrowse.bind(this));
    }
    _initPlugin () {
        super._initPlugin();
        this.holidaysTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {
                    data: 'transsingle',
                    name: 'transsingle.slug',
                    render: function (data) {
                        return data && data.slug ? data.slug : '-';
                    }
                },
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }

    disableEnableBrowse(){
        if($('.delete').is( ":checked" )) {
            $(".upload_img input").prop('disabled', false);
            $(".row_old_image input.form-control").prop('disabled', true);
        } else {
            $(".upload_img input").prop('disabled', true);
            $(".row_old_image input.form-control").prop('disabled', false);
        }
    }
}