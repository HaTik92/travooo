import BaseComponent from "../core/baseComponent";
import Map from '../helpers/map';

export default class DestinationComponent extends BaseComponent {

    _initProperty () {
        this.countriesByNationalityTable  = $('#CountriesByNationalityTable');
        this.citiesByNationalityTable  = $('#CitiesByNationalityTable');
        this.genericCountriesTable  = $('#GenericTopCountriesTable');
        this.genericCitiesTable  = $('#GenericTopCitiesTable');
        this.genericPlacesTable  = $('#GenericTopPlacesTable');

        this.countryForCitiesSelect = $('#countryForCitiesSelect');
        this.countryForPlacesSelect = $('#countryForPlacesSelect');
        this.cityForPlacesSelect = $('#cityForPlacesSelect');
    }

    get url() {
        return this.countriesByNationalityTable.data('url');
    }

    get citiesUrl() {
        return this.citiesByNationalityTable.data('url');
    }

    get genericCountriesUrl() {
        return this.genericCountriesTable.data('url');
    }

    get genericCitiesUrl() {
        return this.genericCitiesTable.data('url');
    }

    get genericPlacesUrl() {
        return this.genericPlacesTable.data('url');
    }


    _initPlugin () {
        //Countries by Nationality DataTable
        this.countriesByNationalityTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: null, name: null, defaultContent: '', searchable: false, sortable: false},
                {data: 'title', name: 'transsingle.title'},
                {data: 'countries', name: 'countries', searchable: false},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });

        //Cities by Nationality DataTable
        this.citiesByNationalityTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.citiesUrl,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: null, name: null, defaultContent: '', searchable: false, sortable: false},
                {data: 'title', name: 'transsingle.title'},
                {data: 'cities', name: 'cities',searchable: false},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });


        //Generic Top Countries DataTable
        this.genericCountriesTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.genericCountriesUrl,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: null, name: null, defaultContent: '', searchable: false, sortable: false},
                {data: 'title', name: 'transsingle.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });

        //Generic Top Cities DataTable
        this.genericCitiesTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.genericCitiesUrl,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: null, name: null, defaultContent: '', searchable: false, sortable: false},
                {data: 'title', name: 'transsingle.title'},
                {data: 'cities', name: 'cities_trans.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });

        //Generic Top Places DataTable
        this.genericPlacesTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.genericPlacesUrl,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: null, name: null, defaultContent: '', searchable: false, sortable: false},
                {data: 'title', name: 'transsingle.title'},
                {data: 'cities', name: 'cities_trans.title'},
                {data: 'places', name: 'places_trans.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });


        //Country For Cities Select2
        this.countryForCitiesSelect.on('select2:select', function (e) {
            var data = e.params.data;

            $.ajax({
                url: '/admin/destinations/generic-top-cities-by-country',
                type: 'post',
                data: {
                    countryId: data.id
                },
                success: function(res){
                    $('#genericCitiesSelect').empty();
                    $('#genericCitiesSelect').select2({
                        data: res
                    });
                }
            });
        });

        //Country For Places Select2
        this.countryForPlacesSelect.on('select2:select', function (e) {
            var data = e.params.data;

            $.ajax({
                url: '/admin/destinations/generic-top-places-by-country',
                type: 'post',
                data: {
                    countryId: data.id
                },
                success: function(res){

                    $('#cityForPlacesSelect').empty();
                    $('#cityForPlacesSelect').select2({
                        data: res.cities
                    });

                    $('#cityForPlacesSelect').attr('countryid', data.id);

                    $('#genericPlacesSelect').empty();
                    $('#genericPlacesSelect').select2({
                        data: res.places
                    });
                }
            });
        });

        //City For Places Select2
        this.cityForPlacesSelect.on('select2:select', function (e) {
            var data = e.params.data;

            $.ajax({
                url: '/admin/destinations/generic-top-places-by-city',
                type: 'post',
                data: {
                    cityId: data.id
                },
                success: function(res){
                    $('#genericPlacesSelect').empty();
                    $('#genericPlacesSelect').select2({
                        data: res
                    });
                }
            });
        });

        $('[data-toggle="tooltip"]').tooltip();
        $('.select2Class').select2();
    }
}
