import BaseComponent from "../core/baseComponent";

export default class HelpCentersComponent extends BaseComponent {

    _initProperty () {
        this.sub_menuesTable    = $('#menues-table');
        // this.pagesCatTable = $('#page-categories-table');
    }

    get url() {
        return this.sub_menuesTable.data('url');
    }

    get config() {
        return this.sub_menuesTable.data('config');
    }

    _initBind () {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin () {
        super._initPlugin();
        this.sub_menuesTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'name', name: this.config + '.name'},
                {data: 'created_at', name: this.config + '.created_at'},
                {data: 'updated_at', name: this.config + '.updated_at'},
                {data: 'actions', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }
}