import BaseComponent from "../core/baseComponent";

export default class CopyrightInfringementUsersComponent extends BaseComponent {


    _initProperty() {
        this.table;
        this.tableEl = $('#copyrightInfringementHistoryTable');
    }

    _initBind () {
        let self = this;
        $(document).on('click','.restore_post_btn', self.restorePost.bind(self));
    }

    get url() {
        return this.tableEl.data('url');
    }

    _initPlugin() {
        this.table = this.tableEl.DataTable({
            lengthMenu: [10, 25, 50, 100, 1000, 10000],
            deferRender: true,
            columnDefs: [
                {
                    orderable: true,
                },
            ],
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: function (request) {
                }
            },
            columns: [
                {data: 'id', name: 'id', visible: true},
                {data: 'infringement_location',name: 'infringement_location',visible: true},
                {data: 'created_at', name: 'created_at', visible: true},
                {
                    data: null,
                    searchable: false,
                    sortable: false,
                    visible: true,
                    render: function (data) {
                        if (data) {
                            return '' +
                                '<button data-id="' + data.id + '" class="btn btn-xs btn-success restore_post_btn" data-toggle="tooltip" data-placement="top" title="Restore Post"><i class="fa fa-arrow-up" style="pointer-events: none;" ></i></button>&nbsp;' +
                                '';
                        } else {
                            return '';
                        }
                    }
                },
            ],
            order: [[0, "desc"]],
            searchDelay: 500,
        });

    }

    restorePost(e) {
        var id = parseInt($(e.target).data('id'));
        $.ajax({
            url: 'restore-post',
            type: 'post',
            data: {
                id: id
            },
            success: function(data){
                var result = data.data;
                if(result.success === true){
                    document.location.reload();
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Post restored successfully</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
                        $("#deleted-alert").slideUp(500);
                    });
                }
            }
        });
    }
}