import Vue from 'vue/dist/vue.js'
import LoaderBtn from './LoaderBtn.js'
import BaseComponent from "../core/baseComponent";
import _ from "lodash";

export default class PlacesMassComponent extends BaseComponent {
    _initProperty () {
        this.VUE_APP = null;
    }

    _initPlugin() {
        let that = this;

        this.VUE_APP = new Vue({
            el: '#places-mass-vue-app',
            components: {
                'loader-btn': LoaderBtn,
            },
            data: {
                founded_ids: [],

                filters: {
                    country_id: null,
                    city_id: null,
                    address: '',
                    address_not_match: '',
                    pluscode: '',
                    pluscode_not_match: '',
                },

                actions: {
                    moveTo: {
                        country_id: null,
                        city_id: null,
                    }
                },

                actionMoveToErrors: {},
                actionMoveToStatus: [],

                filterFormErrors: {},
                filterFormStatus: {},

                loadings: {
                    moveToAction: false,
                    filters: false
                }
            },
            methods:{
                clickApplyAction() {
                    let that = this;

                    this.clearActionMoveToErrors();
                    this.clearActionMoveToStatus();

                    this.loadings.moveToAction = true;

                    let data = _.assign(
                        {},
                        this.actions.moveTo,
                        {ids: that.founded_ids}
                    );

                    axios.post('/admin/location/place/action/moveto', data)
                        .then(function (response) {
                            // handle success
                            $('#moveToCountrySelect').val(null).trigger('change');

                            that.actions.moveTo.country_id = null;
                            that.actions.moveTo.city_id = null;

                            that.actionMoveToStatus = [{
                                status: 'success',
                                text: 'Success'
                            }];
                        })
                        .catch(function (error) {
                            // handle error
                            if (error.response) {
                                // The request was made and the server responded with a status code
                                // that falls out of the range of 2xx
                                // console.log(error.response.data);
                                // console.log(error.response.status);
                                // console.log(error.response.headers);
                                switch (error.response.status) {
                                    case 422:
                                        that.actionMoveToErrors = error.response.data;
                                        break;

                                    default:
                                        that.actionMoveToStatus = [{
                                            status: 'error',
                                            text: error.response.statusText
                                        }];

                                        break;
                                }
                            } else if (error.request) {
                                // The request was made but no response was received
                                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                                // http.ClientRequest in node.js
                                console.log(error.request);
                            } else {
                                // Something happened in setting up the request that triggered an Error
                                console.log('Error', error.message);
                            }
                            // console.log(error.config);
                        })
                        .then(function () {
                            // always executed
                            that.loadings.moveToAction = false;
                        });
                },

                clickApplyFilters(action){
                    let that = this;

                    this.clearFoundedIds();
                    this.clearFilterFormErrors();
                    this.clearFilterFormStatus();

                    this.loadings.filters = true;
                    this.filters.action = action;
                    axios.post('/admin/location/place/mass/filter', this.filters)
                        .then(function (response) {
                            // handle success
                            that.founded_ids = response.data;
                        })
                        .catch(function (error) {
                            // handle error
                            if (error.response) {
                                // The request was made and the server responded with a status code
                                // that falls out of the range of 2xx
                                // console.log(error.response.data);
                                // console.log(error.response.status);
                                // console.log(error.response.headers);
                                switch (error.response.status) {
                                    case 422:
                                        that.filterFormErrors = error.response.data;
                                        break;

                                    default:
                                        that.filterFormStatus = {
                                            status: 'error',
                                            text: error.response.statusText
                                        };

                                        break;
                                }
                            } else if (error.request) {
                                // The request was made but no response was received
                                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                                // http.ClientRequest in node.js
                                console.log(error.request);
                            } else {
                                // Something happened in setting up the request that triggered an Error
                                console.log('Error', error.message);
                            }
                            // console.log(error.config);
                        })
                        .then(function () {
                            // always executed
                            that.loadings.filters = false;
                        });
                },

                clearActionMoveToErrors(){
                    this.actionMoveToErrors = {};
                },

                clearFilterFormErrors(){
                    this.filterFormErrors = {};
                },

                clearFoundedIds(){
                    this.founded_ids = [];
                },

                clearFilterFormStatus(){
                    this.filterFormStatus = {};
                },

                clearActionMoveToStatus(){
                    this.actionMoveToStatus = [];
                }


            },
            computed: {
                _() {
                    return _;
                }
            }
        });

        $('#moveToCountrySelect').select2({
            width: "100%",
            placeholder: "Please select Country",

            ajax: {
                url: "/admin/location/place/get-all-countries",
                dataType: "json",
                delay: 250,
                processResults: function (countries) {
                    return {
                        results: countries,
                    };
                },
                cache: true
            }
        }).on("select2:select", function (e) {
            let country_id = parseInt($(e.currentTarget).val());

            that.VUE_APP.actions.moveTo.country_id = country_id;
        }).on("select2:clear", function (e) {
            that.VUE_APP.actions.moveTo.country_id = null;
        }).on('change', function (e) {
            $('#moveToCitySelect').val(null).trigger('change');

            that.VUE_APP.actions.moveTo.city_id = null;
        });

        $('#moveToCitySelect').select2({
            width: "100%",
            placeholder: "Please select City",
            allowClear: true,

            ajax: {
                url: "/admin/location/place/get-all-cities",
                dataType: "json",
                data: function (params) {
                    let countryId = that.VUE_APP.actions.moveTo.country_id;

                    var queryParameters = {
                        q: params.term,
                        country_id: countryId
                    };

                    return queryParameters;
                },
                delay: 250,
                processResults: function (cities) {
                    return {
                        results: cities,
                    };
                },
                cache: true
            }
        }).on("select2:select", function (e) {
            let city_id = parseInt($(e.currentTarget).val());

            that.VUE_APP.actions.moveTo.city_id = city_id;
        }).on("select2:clear", function (e) {
            that.VUE_APP.actions.moveTo.city_id = null;
        });

        $('#countrySelectFilter').select2({
            width: "100%",
            placeholder: "Please select Country",
            allowClear: true,
            ajax: {
                url: "/admin/location/place/get-all-countries",
                dataType: "json",
                delay: 250,
                processResults: function (countries) {
                    return {
                        results: countries,
                    };
                },
                cache: true
            }
        }).on("select2:select", function (e) {
            let country_id = parseInt($(e.currentTarget).val());

            that.VUE_APP.filters.country_id = country_id;
        }).on("select2:clear", function (e) {
            that.VUE_APP.filters.country_id = null;
        });

        $('#citySelectFilter').select2({
            width: "100%",
            placeholder: "Please select City",
            allowClear: true,

            ajax: {
                url: "/admin/location/place/get-all-cities",
                dataType: "json",
                data: function (params) {
                    let countryId = that.VUE_APP.filters.country_id;

                    var queryParameters = {
                        q: params.term,
                        country_id: countryId
                    };

                    return queryParameters;
                },
                delay: 250,
                processResults: function (cities) {
                    return {
                        results: cities,
                    };
                },
                cache: true
            }
        }).on("select2:select", function (e) {
            let city_id = parseInt($(e.currentTarget).val());

            that.VUE_APP.filters.city_id = city_id;
        }).on("select2:clear", function (e) {
            that.VUE_APP.filters.city_id = null;
        });

        $('#countrySelectFilter').on('change', function (e) {
            $('#citySelectFilter').val(null).trigger('change');

            that.VUE_APP.filters.city_id = null;
        });
    }
}