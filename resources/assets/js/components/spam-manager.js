import BaseComponent from "../core/baseComponent";

export default class SpamManagerComponent extends BaseComponent {


    _initProperty() {
        this.table;
        this.historyTable;
        this.apiReportsTable;
        this.tableEl = $('#spamManagerTable');
        this.historyTableEl = $('#spamManagerHistoryTable');
        this.apiReportsTableEl = $('#apiReportsTable');
    }

    _initBind () {
        let self = this;
        $(document).on('click', '.mark_as_safe_btn', this.markAsSafe.bind(this));
        $(document).on('click', '.restore_post_btn', this.restorePost.bind(this));
        $(document).on('click', '#restore_post', this.restoreApiReportPost.bind(this));
        $(document).on('click', '.unblock_btn', self.unBlockUser.bind(self));
        $('#filterReportHistoryType').change( function () {
            self.historyTable.columns().search($('#filterReportHistoryType').val()).draw();
        });
        $('#filterExpertReports').change( function () {
            self.table.columns().search($('#spamManagerTableType').val()).draw();
        });
        $('#apiReportsTable').on( 'keyup', function () {
            self.apiReportsTable.columns().search($('#apiReportsTable').val()).draw();
        } );
        // $('#spamManagerTable').change(function () {
        //     self.tableEl.columns().search($('#spamManagerTable').val()).draw();
        // });
        // $('#spamManagerHistoryTable').change(function () {
        //     self.historyTableEl.search($('#spamManagerHistoryTable').val()).draw();
        // });

        $('#spamManagerTable').on( 'page.dt', function () {
            $('html, body').animate({
                scrollTop: $('#spamManagerTable').offset().top
            }, 200);
        });

        $(document).on('click', '.block_and_delete_btn', this.blockAndDelete.bind(this));
        // $(document).on('click', '.warning_btn', this.warnUser.bind(this));
        $(document).on('click', '.delete_btn', this.deletePost.bind(this));
        $(document).on('mouseover', '.info-report', function(self) {
            $(self.target).closest('.info-dropdown').addClass( ' open' );
        }).on('mouseout', '.info-report', function(self) {
            $(self.target).closest('.info-dropdown').removeClass( 'open' );
        });
    }

    get url() {
        return this.tableEl.data('url');
    }
    get historyUrl() {
        return this.historyTableEl.data('url');
    }

    get apiReportsUrl() {
        return this.apiReportsTableEl.data('url');
    }

    _initPlugin() {
        let self = this;
        self.historyTable = self.historyTableEl.DataTable({
            lengthMenu: [10, 25, 50, 100, 1000, 10000],
            deferRender: true,
            columnDefs: [
                {
                    orderable: true,
                },
            ],
            processing: true,
            serverSide: true,
            ajax: {
                url: this.historyUrl,
                type: 'post',
                data: function (request) {
                    request.filter = $('#filterReportHistoryType').val();
                }
            },
            columns: [
                {data: 'id', name: 'id', visible: true},
                {
                    data: 'user',
                    name: 'user.name',
                    visible: true,
                    sortable: true,
                    searchable: true,
                    render: function (data) {
                        return '<div class="post-foot-block post-reaction"><i>#'+data.id+'</i> - '+data.name+'</div>';
                    }
                },
                {data: 'posts_id', name: 'posts_id', visible: true, sortable: true, searchable: true},
                {data: 'post_type', name: 'post_type', visible: true},
                {data: 'report_type', name: 'report_type', visible: true},
                {data: 'date', name: 'created_at', visible: true},
                {
                    data: null,
                    visible: true,
                    render: function ( data) {
                        if (data.post && data.post.deleted_at) {
                            return '' +
                                '<button data-id="' + data.spam_post_id + '" class="btn btn-xs btn-success restore_post_btn" data-toggle="tooltip" data-placement="top" title="Restore Post"><i class="fa fa-arrow-up" style="pointer-events: none;" ></i></button>&nbsp;' +
                                '';
                        } else {
                            return '';
                        }
                    }
                },
            ],
            order: [[0, "desc"]],
                searchDelay: 500,
        });
        self.table = this.tableEl.DataTable({
            lengthMenu: [10, 25, 50, 100, 1000, 10000],
            deferRender: true,
            columnDefs: [
                {
                    orderable: true,
                },
                { 'width': '18%', 'targets' : 8 },
                {
                    targets: [ 4 ],
                    orderData: [ 2,1 ]
                }
            ],
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: function (request) {
                    request.filter = $('#filterExpertReports').val();
                }
            },
            columns: [
                {data: 'id', name: 'id', visible: true},
                {data: 'report_type',
                    name: 'report_type',
                    visible: true,
                    render: function(data) {
                        switch(data){
                            case '1':
                                return '<label class="label label-success">Other</label>';
                            case '2':
                                return '<label class="label label-danger">Fake News</label>';
                            case '3':
                                return '<label class="label label-danger">Harassment</label>';
                            case '4':
                                return '<label class="label label-danger">Hate Speech</label>';
                            case '5':
                                return '<label class="label label-danger">Nudity</label>';
                            case '6':
                                return '<label class="label label-danger">Terrorism</label>';
                            case '7':
                                return '<label class="label label-danger">Violence</label>';
                            case '8':
                                return '<label class="label label-danger">API Reports</label>';
                            default:
                                return '<label class="label label-default">Spam</label>';
                        }
                    }},
                {data: 'post_type', name: 'post_type', visible: true},
                {
                    data: 'user',
                    name: 'users_id',
                    visible: true,
                    render: function(data) {
                        let content = '';
                        if(!data || !Object.keys(data).length){
                            return '<label class="label label-default">Unknown</label>';
                        }
                        if (data && data.expert) {
                            content += '<span class="exp-icon">EXP</span>' + ' ';
                        }
                        return '<div class="post-foot-block post-reaction"><i>#'+data.id+'</i> - '+content+data.name+'</div>';
                    }
                },
                {
                    data: 'report_users',
                    name: 'report_users',
                    visible: true,
                    render: function(data) {
                        var text = '';
                        data.forEach(function(item, user) {
                            text += '<tr>'+
                                '<td>'+item.id+'</td>'+
                                '<td>'+item.name+'</td>'+
                                '<td>'+item.date+'</td>'+
                                '<td>'+item.text+'</td>'+
                                '</tr>';
                        });

                        var userData = '<div class="dropdown dropright">' +
                            '<a type="button" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false" class="btn">'+
                            data.length+
                            '</a>'+
                            '<div class="dropdown-menu" posttype="Post">'+
                            '<table>'+
                            '<tr>'+
                            '<th>User ID</th>'+
                            '<th>Name</th>'+
                            '<th>Date of submission</th>'+
                            '<th>Text</th>'+
                            '</tr>'+
                            text +
                            '</table>'+
                            '</div>'+
                            '</div>';
                        return userData ;
                    }
                },
                {
                    data: 'report_text',
                    name: 'report_text',
                    sortable: true,
                    searchable: true,
                    visible: true,
                    render: function(data) {
                        if(!data){
                            return '-';
                        }
                        return '<div class="custom-scroll">' + data + '</div>';
                    }
                },
                {
                    data: 'author',
                    name: 'author',
                    visible: true,
                    render: function(data) {
                        if (data.name) {
                            let reports = data.history;
                            let markAsSafe = 0;
                            let blockIcon = '&nbsp';
                            let removePostAndBlockUser = 0;
                            let removePost = 0;
                            if (reports) {
                                reports.forEach(function (item, index) {
                                    if (item.type === 'remove_post_and_block_user') {
                                        removePostAndBlockUser++;
                                    }
                                    if (item.type === 'mark_as_safe') {
                                        markAsSafe++;
                                    }
                                    if (item.type === 'remove_post') {
                                        removePost++;
                                    }
                                });
                            }
                            if (data.blocked == true) {
                                blockIcon += '<i class="fa fa-lock"></i>';
                            }

                            if (data.flagged.length > 0) {
                                blockIcon += '<i class="fa fa-flag"></i>';
                            }
                            return '<div class="dropdown dropright info-dropdown"><a target="_blank" class="info-report" href="/profile-posts/' + data.id + '#' + data.postsId + '">' +
                                '<img class="expert_type" src="public/assets2/image/sign_up/expert_type.png" alt="">' + data.name + blockIcon + '</a>' +
                                '<div class="dropdown-menu" style="text-align: center;min-width: 125px;">' +
                                '<label class="label label-xs label-success" title="Mark as Safe"><i class="fa fa-arrow-up" style="pointer-events: none;" >' + markAsSafe + '</i></label>&nbsp;' +
                                '<label class="label label-xs label-warning" title="Remove post + Warning user"><i style="pointer-events: none;" class="fa fa-exclamation-triangle" ></i>&nbsp;<i style="pointer-events: none;" class="fa fa-times" aria-hidden="true">' + removePost + '</i></label>&nbsp;&nbsp;' +
                                '<label class="label label-xs label-danger" title="Block user + Delete"><i class="fa fa-ban" style="pointer-events: none;"></i>&nbsp;<i style="pointer-events: none;" class="fa fa-times" aria-hidden="true">' + removePostAndBlockUser + '</i></.button>' +
                                '</div>' +
                                '</div>';
                        } else {
                            return '<label class="label label-default">-</label>';
                        }
                    }
                },
                {data: 'posts_id', name: 'posts_id', visible: true},
                {
                    data: null,
                    visible: true,
                    render: function (data) {
                        var buttons = '';
                        if (data.author && data.author.blocked === true ) {
                            buttons += '<button data-id="'+data.author.id+'" class="btn btn-xs btn-success unblock_btn" data-toggle="tooltip" data-placement="top" title="Unblock user"><i style="pointer-events: none;" class="fa fa-unlock" ></i></button>&nbsp;';
                        }
                        return buttons +
                            '<button data-id="'+data.id+'" class="btn btn-xs btn-success mark_as_safe_btn" data-toggle="tooltip" data-placement="top" title="Mark as Safe"><i class="fa fa-arrow-up" style="pointer-events: none;" ></i></button>&nbsp;' +
                            '<a href="spam_manager/' + data.id + '" class="btn btn-xs btn-info view_btn mx-1" data-toggle="tooltip" data-placement="top" title="View Report"><i class="fa fa-search" style="pointer-events: none;" ></i></a>&nbsp' +
                            '<button data-id="'+data.id+'" class="btn btn-xs btn-warning delete_btn" data-toggle="tooltip" data-placement="top" title="Remove post"><i style="pointer-events: none;" class="fa fa-exclamation-triangle" ></i>&nbsp;<i style="pointer-events: none;" class="fa fa-times" aria-hidden="true"></i></button>&nbsp;' +
                            '<button data-id="'+data.id+'" class="btn btn-xs btn-danger block_and_delete_btn" data-toggle="tooltip" data-placement="top" title="Block user + Delete"><i class="fa fa-ban" style="pointer-events: none;"></i>&nbsp;<i style="pointer-events: none;" class="fa fa-times" aria-hidden="true"></i></button>' +
                            '';
                    }
                },
            ],
            order: [[0, "desc"]],
            searchDelay: 500,
        });

        self.apiReportsTable = self.apiReportsTableEl.DataTable({
            lengthMenu: [10, 25, 50, 100, 1000, 10000],
            deferRender: true,
            columnDefs: [
                {
                    orderable: true,
                },
            ],
            processing: true,
            serverSide: true,
            ajax: {
                url: this.apiReportsUrl,
                type: 'post',
                data: function (request) {

                }
            },
            columns: [
                {data: 'id', name: 'id', visible: true},
                {data: 'post_id', name: 'post_id', visible: true},
                {
                    data: 'user',
                    name: 'user_id',
                    visible: true,
                    render: function (data) {
                        return '<div class="post-foot-block post-reaction"><i>#'+data.id+'</i> - '+data.name+'</div>';
                    }
                },
                {
                    data: 'media_url',
                    name: 'media_url',
                    visible: true,
                    render: function (data) {
                        return '<a href="' + data + '">' + data + '</a>';
                    }},
                {data: 'text', name: 'text', visible: true},
                {data: 'type', name: 'type', visible: true},
                {data: 'created_at', name: 'created_at', visible: true},
                {
                    data: null,
                    sortable: false,
                    searchable: false,
                    visible: true,
                    render: function ( data) {
                        if ( data.post_id) {
                            return '' +
                                '<button data-id="' + data.post_id + '"  data-type="' + data.type + '" id="restore_post" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="Restore Post"><i class="fa fa-arrow-up" style="pointer-events: none;" ></i></button>&nbsp;' +
                                '';
                        } else {
                            return '';
                        }
                    }
                },
            ],
            order: [[0, "desc"]],
            searchDelay: 500,
        });
    }

    markAsSafe(e) {
        var id = parseInt($(e.target).data('id'));
        $.ajax({
            url: 'spam_manager/mark-as-safe',
            type: 'post',
            data: {
                id: id
            },
            success: function(data){
                var result = data.data;
                if(result.success === true){
                    document.location.reload();
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Successfully</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
                        $("#deleted-alert").slideUp(500);
                    });
                }
            }
        });
    }
    // warnUser(e) {
    //     var id = parseInt($(e.target).data('id'));
    //     $.ajax({
    //         url: 'spam_manager/warn',
    //         type: 'post',
    //         data: {
    //             id: id
    //         },
    //         success: function(data){
    //             var result = data.data;
    //             if(result.success === true){
    //                 document.location.reload();
    //                 var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>User warn successfully.</div>';
    //                 $('.deleted_msg').html(msg);
    //                 $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
    //                     $("#deleted-alert").slideUp(500);
    //                 });
    //             }
    //         }
    //     });
    // }

    deletePost(e) {
        var id = parseInt($(e.target).data('id'));
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure you want to delete this report? <div class="mb-3"></div>',
            columnClass: 'col-md-5 col-md-offset-5',
            // closeIcon: true,
            offsetTop: 100,
            offsetBottom: 500,
            scrollToPreviousElement:false,
            scrollToPreviousElementAnimate:false,
            buttons: {
                cancel: function () {
                },
                confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'shift'],
                    action: function () {
                        $.ajax({
                            url: 'spam_manager/remove-post',
                            type: 'post',
                            data: {
                                id: id
                            },
                            success: function(data){
                                var result = data.data;
                                if(result.success === true){
                                    document.location.reload();
                                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Post successfully deleted</div>';
                                    $('.deleted_msg').html(msg);
                                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
                                        $("#deleted-alert").slideUp(500);
                                    });
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    blockAndDelete(e) {
        var id = parseInt($(e.target).data('id'));
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure you want to delete this report and block user? <div class="mb-3"></div>',
            columnClass: 'col-md-5 col-md-offset-5',
            closeIcon: true,
            offsetTop: 0,
            offsetBottom: 500,
            scrollToPreviousElement:false,
            scrollToPreviousElementAnimate:false,
            buttons: {
                cancel: function () {
                },
                confirm: {
                    text: 'Confirm',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'shift'],
                    action: function () {
                        $.ajax({
                            url: 'spam_manager/remove-post-and-block-user',
                            type: 'post',
                            data: {
                                id: id
                            },
                            success: function(data){
                                var result = data.data;
                                if(result.success === true){
                                    document.location.reload();
                                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Post deleted and user blocked successfully</div>';
                                    $('.deleted_msg').html(msg);
                                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
                                        $("#deleted-alert").slideUp(500);
                                    });
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    restorePost(e) {
        var id = parseInt($(e.target).data('id'));
        $.ajax({
            url: 'restore-removed-post',
            type: 'post',
            data: {
                id: id
            },
            success: function(data){
                var result = data.data;
                if(result.success === true){
                    document.location.reload();
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Post restored successfully</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
                        $("#deleted-alert").slideUp(500);
                    });
                }
            }
        });
    }

    restoreApiReportPost(e) {
        var id = parseInt($(e.target).data('id'));
        var type = $(e.target).data('type') ? $(e.target).data('type') : 'Post';
        $.ajax({
            url: 'restore-api-report-post',
            type: 'post',
            data: {
                id: id,
                type: type
            },
            success: function(data){
                var result = data.data;
                if(result.success === true){
                    document.location.reload();
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Post restored successfully</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
                        $("#deleted-alert").slideUp(500);
                    });
                }
            }
        });
    }

    unBlockUser(e) {
        var id = parseInt($(e.target).data('id'));
        $.ajax({
            url: '/admin/access/user/unblock',
            type: 'post',
            data: {
                id: id
            },
            success: function(data){
                var result = data.data;
                if(result.success === true){
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>User successfully unblocked</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
                        $("#deleted-alert").slideUp(500);
                    });
                    document.location.reload();
                }
            }
        });
    }
}