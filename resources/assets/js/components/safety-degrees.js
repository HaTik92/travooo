import BaseComponent from "../core/baseComponent";

export default class SafetyComponent extends BaseComponent {
    _initProperty() {
        this.safetyTable = $('#safety-degrees-table');
    }

    get url() {
        return this.safetyTable.data('url');
    }

    get config() {
        return this.safetyTable.data('config');
    }

    _initBind() {
        $(document).on('click','.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin() {
        super._initPlugin();
        this.safetyTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }
}