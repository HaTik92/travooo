import BaseComponent from "../core/baseComponent";

export default class PagesComponent extends BaseComponent {

    _initProperty () {
        this.pagesTable    = $('#pages-table');
        this.pagesCatTable = $('#page-categories-table');
    }

    get url() {
        return this.pagesTable.data('url');
    }

    get catUrl() {
        return this.pagesCatTable.data('url');
    }

    get config() {
        return this.pagesTable.data('config');
    }

    get catConfig() {
        return this.pagesCatTable.data('config');
    }

    _initBind () {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin () {
        super._initPlugin();
        this.pagesTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {
                    name: this.config + '.active',
                    data: 'active',
                    sortable: false,
                    searchable: false,
                    render: function(data) {
                        if (data == 1) {
                            return '<label class="label label-success">Active</label>';
                        } else {
                            return '<label class="label label-danger">Deactive</label>';
                        }
                    }
                },
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });

        this.pagesCatTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.catUrl,
                type: 'post',
                data: {status: 1}
            },
            columns: [
                {data: 'id', name: this.catConfig + '.id'},
                {data: 'title', name: this.catConfig + '.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });

        $('[medias_id]').select2({
            width:'100%',
            minimumInputLength: 2,
            quietMillis: 10,
            ajax: {
                url: $('#getMedias').val(),
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                        page: params.page || 1
                    };
                },
                processResults: function (data) {
                    return {
                        results: data.data,
                        pagination: {
                            more: data.paginate
                        }
                    };
                },
                cache: true
            }
        });
    }
}