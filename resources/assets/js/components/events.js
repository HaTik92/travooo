import BaseComponent from "../core/baseComponent";
import DatatableHelper from '../helpers/datatable';
import Map from '../helpers/map';

export default class EventsComponent extends BaseComponent {
    get url() {
        return this.eventsTable.data('url');
    }

    get config() {
        return this.eventsTable.data('config');
    }

    get eventDelRoute() {
        return this.eventsTable.data('del');
    }

    _initProperty () {
        this.table;
        this.eventsTable = $('#events-table');
        this.mediaIndex      = 1;

        global.initAutocomplete = window.initAutocomplete = this.initAutocomplete;
        if (location.pathname.substring(1) !== 'admin/events') {
            Map.bindApiKey();
        }
    }

    _initBind () {
        //add icon
        $(document).on('click','#add_row', this.addRow.bind(this));
        //remove icon row
        $(document).on('click','.delete_row', this.deleteRow.bind(this));

        $(document).on('change', '#country_id', this.getJsonCities.bind(this));
        $(document).on('change','.country-input', this.countryInput.bind(this));
        $(document).on('click','#select-all', this.selectDeselectAll.bind(this));
        $(document).on('click','#delete-all-selected', this.deleteAllSelected.bind(this));
    }
    _initPlugin () {
        const self = this;

        this.table = this.eventsTable.DataTable({
            lengthMenu: [ 10, 25, 50, 100, 1000,10000 ],
            deferRender: true,
            columnDefs: [
                {
                    orderable: false,
                    className: 'select-checkbox',
                    targets:   0,
                },
                // {
                //     "targets": [9, 10, 11],
                //     "orderable": false
                // },
                // { "width": "20%", "targets" : 5 },
                // { "width": "20%", "targets" : 6 }
            ],
            checkboxes: {
                selectRow: true,
            },
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: function (request) {
                    var titleIndex;
                    request.status = 1;
                    request.trashed = false;
                    request.columns.forEach(function (item, index) {
                        if (item.data == "address") {
                            request.columns[index].search.value1 = {matchValue: $('#address-filter').val()};
                            request.columns[index].search.value2 = {notMatchValue: $('#not-address-filter').val()};
                        }
                        if (item.data == "pluscode") {
                            request.columns[index].search.value1 = {matchValue2: $('#pluscode-filter').val()};
                            request.columns[index].search.value2 = {notMatchValue2: $('#not-pluscode-filter').val()};
                        }
                        if (item.data == "title") {
                            titleIndex = index;
                        }
                    });
                    if (request.search.value != "") {
                        request.columns[titleIndex].search.value = request.search.value;
                        request.columns[titleIndex].search.regex = "false";
                        // request.search.value = null;
                    }
                }
            },
            columns: [
                {data: null, name: null, defaultContent: ''},
                {data: 'id', name: this.config + '.id'},
                {data: 'title', name: 'title'},
                // {data: 'address', name: 'transsingle.address'},
                // {data: 'pluscode', name: 'pluscode', visible: true},
                // {data: 'country_title', name: 'countries_trans.title'},
                // {data: 'city_title', name: 'cities_trans.title'},
                // {data: 'place_type', name: 'embassies.place_type'},
                // {
                //     name: this.config + '.active',
                //     data: 'active',
                //     sortable: false,
                //     searchable: false,
                //     render: function(data) {
                //         if (data == 1) {
                //             return '<label class="label label-success">Active</label>';
                //         } else {
                //             return '<label class="label label-danger">Deactive</label>';
                //         }
                //     }
                // },
                {data: 'action', name: 'action',  visible: true, searchable: false, sortable: false, ordering: false},
                // {data: 'cities_id', name: this.config + '.cities_id'},
                // {data: 'location_country_id', name: this.config + '.location_country_id'},
            ],
            order: [[1, "asc"]],
            searchDelay: 500,
            initComplete: function () {

                console.log('initComplete');

                // DatatableHelper.hideDtColumn(self.embassiessTable, 11);
                // DatatableHelper.hideDtColumn(self.embassiessTable, 12);
                // this.api().columns().every(function () {
                //     var column = this;
                //     var select = $('<select><option value=""></option></select>')
                //         .appendTo($(column.footer()).empty())
                //         .on('change', function () {
                //             var val = $.fn.dataTable.util.escapeRegex($(this).val());
                //             column.search(val ? '^' + val + '$' : '', true, false).draw();
                //         });
                //     column.data().unique().sort().each(function (d, j) {
                //         select.append('<option value="' + d + '">' + d + '</option>')
                //     });
                // });
                // var cities = [];
                // var place_types = [];
                // $('#embassies-table tbody tr').each(function(){
                //     var temp_text = $(this).find('td:nth-child(5)').html();
                //     cities[temp_text] = temp_text;
                //     temp_text = $(this).find('td:nth-child(6)').html();
                //     place_types[temp_text] = temp_text;
                // });
                // $('#embassies-table thead').append('<tr><td></td><td></td><td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td></tr>');
                // var count = 0;
                // $('#embassies-table thead tr:nth-child(2) td').each(function () {
                //
                //     if (count == 3){
                //         $(this).html(
                //             '<input type="text" id="address-filter" placeholder="match" class="custom-filters form-control" style="width:150px; margin-bottom: 5px" />' +
                //             '<input type="text" id="not-address-filter" placeholder="not match" class="custom-filters form-control" style="width:150px" />'
                //         );
                //     }
                //
                //     if (count == 4){
                //         $(this).html(
                //             '<input type="text" id="pluscode-filter" placeholder="match" class="custom-filters form-control" style="width:80px; margin-bottom: 5px" />' +
                //             '<input type="text" id="not-pluscode-filter" placeholder="not match" class="custom-filters form-control" style="width:80px" />'
                //         );
                //     }
                //
                //     if (count == 5){
                //         $(this).html('<select id="country-filter" class="custom-filters form-control"><option value="">Search Country</option></select>');
                //     }
                //
                //     if (count == 6){
                //         $(this).html('<select id="city-filter" class="custom-filters form-control"><option value="">Search City</option></select>');
                //     }
                //
                //     if (count == 7){
                //         $(this).html('<select id="place-type-filter" class="custom-filters"><option value="">Search Place Type</option></select>');
                //     }
                //     count++;
                // });

                // $('#country-filter').select2({
                //     width:'100%',
                //     placeholder: 'Search Country',
                //     ajax: {
                //         url: $('#placeCountry').val(),
                //         dataType: 'json',
                //         delay: 250,
                //         processResults: function (data) {
                //             return {
                //                 results: data
                //             };
                //         },
                //         cache: true
                //     }
                // });
                // $('#city-filter').select2({
                //     width:'100%',
                //     placeholder: 'Search City',
                //     minimumInputLength: 2,
                //     quietMillis: 10,
                //     ajax: {
                //         url: $('#searchCity').val(),
                //         dataType: 'json',
                //         delay: 250,
                //         data: function (params) {
                //             return {
                //                 q: $.trim(params.term),
                //                 page: params.page || 1
                //             };
                //         },
                //         processResults: function (data) {
                //             return {
                //                 results: data.data,
                //                 pagination: {
                //                     more: data.paginate
                //                 }
                //             };
                //         },
                //         cache: true
                //     }
                // });
                //
                // $('#place-type-filter').select2({
                //     width:'100%',
                //     placeholder: 'Search Place Types',
                //     ajax: {
                //         url: $('#PlaceType').val(),
                //         dataType: 'json',
                //         delay: 250,
                //         processResults: function (data) {
                //             return {
                //                 results: data
                //             };
                //         },
                //         cache: true
                //     }
                // });
            },
            drawCallback: function () {

                console.log('drawCallback');

                // DatatableHelper.hideDtColumn(self.embassiessTable, 11);
                // DatatableHelper.hideDtColumn(self.embassiessTable, 12);
            }
        });


        if (/admin\/events\/\d+$/.test(location.pathname.substring(1)) == false) {
            this.registerSummernote('.description', 5000, function (max) {
                $('.textarea-notification').text('maxlength' + max);
                if (max == 0) {
                    var msg = '<div id="language-alert" class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong>Description should not be greater than 5000 characters.</div>';
                    var textareaMsg = $('.description').parent().siblings('.textarea-msg')
                    textareaMsg.html(msg)
                    $(".textarea-msg").fadeTo(5000, 500).slideUp(500, function () {
                        $(".textarea-msg").slideUp(500);
                    });
                }
            })
        }

        if(location.pathname.substring(1) == 'admin/events/create'){
            $('#datetimepicker_start').datetimepicker({
                format:'YYYY-MM-DD HH:mm:ss',
                allowInputToggle: true,
                widgetPositioning:{
                    horizontal: 'auto',
                    vertical: 'auto'
                }
            });
            $('#datetimepicker_end').datetimepicker({
                format:'YYYY-MM-DD HH:mm:ss',
                allowInputToggle: true,
                widgetPositioning:{
                    horizontal: 'auto',
                    vertical: 'auto'
                }
            });
        }

        $('.select2Class').select2();
    }

    addRow(e) {
        var newRow = $("<tr></tr>"),
            tbCols = [];

        tbCols.push('<td class="set_img_col"><input type="file" name="license_images['+this.mediaIndex+'][image]"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][title]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][author_name]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][author_url]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][source_url]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][license_name]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][license_url]" class="form-control"></td>');
        tbCols.push('<td><a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a></td>');
        this.mediaIndex++;
        newRow.append(tbCols.join("\r\n"));

        if ($('.delete_row').length <= 8) {
            $("#addMoreRows").append(newRow);
        } else {
            $(".set_error_msg").html('Only 10 Images Rows can Added');
            $(".alert-warning").slideDown();
            setTimeout(function() {
                $(".alert-warning").slideUp();
            }, 2000);
            return false;
        }
    }

    deleteRow(e) {
        $(e.currentTarget).closest("tr").remove();
    }

    countryInput() {
        let map = window.map;
        let searchBox = window.searchBox;
        let countriesLocation = JSON.parse($('#countryInfo').val())[$('.country-input').val()];
        map.setCenter({
            lat: Number(countriesLocation.lat),
            lng: Number(countriesLocation.lng)
        });
        searchBox.componentRestrictions.country = countriesLocation.iso_code;
    };

    initAutocomplete() {
        var map;
        var searchBox;

        var markers = [];

        if (location.pathname.substring(1) == 'admin/events/import') {
            map = window.map = new google.maps.Map(document.getElementById('gmap'), {
                center: {lat: -33.8688, lng: 151.2195},
                zoom: 13
            });
        }

        if ($('#countryInfo').val() != null) {
            var countriesLocation = JSON.parse($('#countryInfo').val())[$('.country-input').val()];
            map = window.map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: Number(countriesLocation.lat), lng: Number(countriesLocation.lng)},
                zoom: 7,
                mapTypeId: 'roadmap'
            });
        }


        // Create the search box and link it to the UI element.
        if (document.getElementById('pac-input')) {
            var input = document.getElementById('pac-input');
            if (location.pathname.substring(1) !== 'admin/events/import') {
                var options = {
                    componentRestrictions: {country: countriesLocation.iso_code}
                };
                searchBox = window.searchBox = new google.maps.places.Autocomplete(input, options);

                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                searchBox.bindTo('bounds', map);
                searchBox.addListener('place_changed', function () {
                    var place = searchBox.getPlace();

                    if (!place) {
                        return;
                    }
                    var marker = new google.maps.Marker({
                        map: map,
                        icon: {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        },
                        title: place.name,
                        draggable: true,
                        position: place.geometry.location
                    });

                    var latitude = place.geometry.location.lat();
                    var longitude = place.geometry.location.lng();
                    $('#lat-lng-input').val(latitude + "," + longitude);
                    $('#lat-lng-input_show').val(latitude + "," + longitude);

                    google.maps.event.addListener(marker, 'dragend', function (evt) {
                        $('#lat-lng-input').val(evt.latLng.lat().toFixed(3) + "," + evt.latLng.lng().toFixed(3));
                    });

                    var bounds = new google.maps.LatLngBounds();

                    if (place.geometry.viewport) {
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });
            } else {
        //         searchBox = window.searchBox = new google.maps.places.SearchBox(input);
        //         map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        //
        //         // Bias the SearchBox results towards current map's viewport.
        //         map.addListener('bounds_changed', function () {
        //             searchBox.setBounds(map.getBounds());
        //         });
        //
        //         var markers = [];
        //         // Listen for the event fired when the user selects a prediction and retrieve
        //         // more details for that place.
        //         searchBox.addListener('places_changed', function () {
        //             var places = searchBox.getPlaces();
        //
        //             if (places.length == 0) {
        //                 return;
        //             }
        //
        //             // Clear out the old markers.
        //             markers.forEach(function (marker) {
        //                 marker.setMap(null);
        //             });
        //             markers = [];
        //
        //             // For each place, get the icon, name and location.
        //             var bounds = new google.maps.LatLngBounds();
        //             places.forEach(function (place) {
        //                 if (!place.geometry) {
        //                     console.log("Returned place contains no geometry");
        //                     return;
        //                 }
        //                 var icon = {
        //                     url: place.icon,
        //                     size: new google.maps.Size(71, 71),
        //                     origin: new google.maps.Point(0, 0),
        //                     anchor: new google.maps.Point(17, 34),
        //                     scaledSize: new google.maps.Size(25, 25)
        //                 };
        //
        //                 // Create a marker for each place.
        //                 markers.push(new google.maps.Marker({
        //                     map: map,
        //                     icon: icon,
        //                     title: place.name,
        //                     position: place.geometry.location
        //                 }));
        //
        //                 if (place.geometry.viewport) {
        //                     // Only geocodes have viewport.
        //                     bounds.union(place.geometry.viewport);
        //                 } else {
        //                     bounds.extend(place.geometry.location);
        //                 }
        //             });
        //             map.fitBounds(bounds);
        //         });
        //         map.addListener('click', function (event) {
        //             var loc = event.latLng.lat() + "," + event.latLng.lng();
        //             document.getElementById('latlng').value = loc;
        //             // Position = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
        //
        //         });
        //         map.addListener('idle', function (event) {
        //             draw_markers(map);
        //         });
            }
        }
    }

    getJsonCities(e) {
        $.getJSON(
            $(e.currentTarget).data('url'),
            {
                countryID: parseInt($(e.currentTarget).val())
            },
            function (data) {
                var model = $('select#city_id');
                model.empty();
                model.append("<option value=''>Select City </option>");
                $.each(data, function (index, element) {
                    model.append("<option value='" + element.id + "'>" + element.title + "</option>");
                });
            }
        );
    }

    registerSummernote(element, max, callbackMax) {
        $(element).summernote({
            height: 200,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    if (t.trim().length >= max) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    if (typeof callbackMax == 'function') {
                        callbackMax(max - t.trim().length);
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    document.execCommand('insertText', false, all.trim().substring(0, max));
                    if (typeof callbackMax == 'function') {
                        callbackMax(max - t.length);
                    }
                }
            }
        });
    }
    selectDeselectAll(){
        $(".select-all").trigger( "click" );
    }

    deleteAllSelected(){
        if(this.eventsTable.find('tbody tr.selected').length == 0){

            alert('Please select some rows first.');
            return false;
        }
        if(!confirm('Are you sure you want to delete the selected rows?')){
            return false;
        }
        $('#deleteLoadingEvents').show();
        $('#deleteLoadingEvents .fa-spin').css("z-index","5");
        $('.table-responsive').css("opacity","0.2");
        $('.select-checkbox').click(function(e) {
            e.stopPropagation();
        });
        var ids = '';
        var i = 0;
        this.eventsTable.find('tbody tr.selected').each(function(){
            if(i != 0){
                ids += ',';
            }
            ids += $(this).find('td:nth-child(2)').html();
            i++;
        });
        $.ajax({
            url: this.eventDelRoute,
            type: 'post',
            data: {
                ids: ids
            },
            success: function(data){
                var result = JSON.parse(data);
                if(result.result == true){
                    document.location.reload();
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Events Deleted Successfully</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
                        $("#deleted-alert").slideUp(500);
                    });
                }
            }
        });
    }
}