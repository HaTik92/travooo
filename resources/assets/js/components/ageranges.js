import BaseComponent from '../core/baseComponent';

export default class AgeRangesComponent extends BaseComponent {
    _initProperty () {
        this.agerangesTable = $('#ageranges-table');
    }

    get url () {
        return this.agerangesTable.data('url');
    }

    get config () {
        return this.agerangesTable.data('config');
    }

    _initBind () {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin () {
        super._initPlugin();
        this.agerangesTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {
                    name: this.config + '.active',
                    data: 'active',
                    sortable: false,
                    searchable: false,
                    render: function (data) {
                        if (data == 1) {
                            return '<label class="label label-success">Active</label>';
                        } else {
                            return '<label class="label label-danger">Deactive</label>';
                        }
                    }
                },
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }
}