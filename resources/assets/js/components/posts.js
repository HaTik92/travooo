import BaseComponent from "../core/baseComponent";

export default class PostsComponent extends BaseComponent {
    _initProperty () {
        this.searchSection = $('.search-section');
    }

    get url() {
        return this.searchSection.data('url');
    }

    get id() {
        return this.searchSection.find('.post-id').val();
    }

    get type() {
        return this.searchSection.find('.post-type').val();
    }

    _initPlugin () {
        super._initPlugin();

        var _this = this;
        $(document).on('click', '.search', function () {
            if (_this.id && _this.type) {
                $.ajax({
                    type: "POST",
                    url: _this.url,
                    data: {
                        id: _this.id,
                        type: _this.type
                    },
                    success: function (result) {
                        var table = _this.searchSection.closest('.box-body').find('.res-table');
                        if (result.data) {
                            table.removeClass('hidden');
                            table.find('.tb-id').html(result.data.id);
                            table.find('.tb-user-id').html(result.data.users_id);
                            table.find('.tb-rank').html(result.data.rank);
                            table.find('.tb-rank-7').html(result.data.rank_7);
                        } else {
                            table.addClass('hidden');
                            alert("We Couldn't Find Any Post With That Credentials.")
                        }
                    }
                });
            } else {
                alert("Please Fill Values.")
            }
        })
    }
}
