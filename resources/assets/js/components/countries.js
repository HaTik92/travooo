import BaseComponent from "../core/baseComponent";
import Map from '../helpers/map';

export default class CountriesComponent extends BaseComponent {

    _initProperty () {
        $('body').on('DOMNodeInserted', 'select', function () {
            $(this).select2();
        });
        this.countriesTable  = $('#countries-table');
        this.mediaIndex      = 1;
        this.count      = 1;
        this.lifestyleCount = 1;
        this.restrictionsCount = 1;
        this.planning_tipsCount = 1;
        this.health_notesCount = 1;
        this.potential_dangersCount = 1;
        this.speed_limitCount = 1;
        this.etiquetteCount = 1;
        this.transportationCount = 1;
        this.low_Count = 13;
        this.shoulder_Count = 13;
        this.high_Count = 13;
        this.selectElements = [
            $('#currenciesSelect'),
            $('#citiesSelect'),
            $('#emergencyNumbersSelect'),
            $('#holidaysSelect'),
            $('#lifestyleSelect'),
            $('#languagesSpokenSelect'),
            $('#additionalLanguagesSpokenSelect')
        ];
        global.initAutocomplete = window.initAutocomplete = this.initAutocomplete;
        if (location.pathname.substring(1) !== 'admin/location/country') {
            Map.bindApiKey();
        }
    }

    get url() {
        return this.countriesTable.data('url');
    }

    get config() {
        return this.countriesTable.data('config');
    }

    get countryDelRoute() {
        return this.countriesTable.data('del');
    }

    _initBind () {
        $(document).on('click','#select-all', this.selectDeselectAll.bind(this));
        $(document).on('click','#delete-all-selected', this.deleteAllSelected.bind(this));
        $('textarea[maxlength]').keypress(this.textareaType.bind(this));
        $(document).on('click','.submit_button', this.submitValidation.bind(this));
        //add icon
        $(document).on('click','#add_icon', this.addRow.bind(this));
        //add icon
        $(document).on('click','#add_icon_holiday', this.addRowHoliday.bind(this));
        $(document).on('click','#add_icon_lifestyle', this.addRowLifestyle.bind(this));
        $(document).on('click','#add_icon_restrictions', this.addRowRestrictions.bind(this));
        $(document).on('click','#add_icon_planning_tips', this.addRowPlanningTips.bind(this));
        $(document).on('click','#add_icon_health_notes', this.addRowHealthNotes.bind(this));
        $(document).on('click','#add_icon_potential_dangers', this.addRowPotentialDangers.bind(this));
        $(document).on('click','#add_icon_speed_limit', this.addRowSpeedLimit.bind(this));
        $(document).on('click','#add_icon_etiquette', this.addRowEtiquette.bind(this));
        $(document).on('click','#add_icon_transportation', this.addRowTransportation.bind(this));
        $(document).on('click','#add_icon_low_season', this.addRowLowSeason.bind(this));
        $(document).on('click','#add_icon_shoulder', this.addRowShoulder.bind(this));
        $(document).on('click','#add_icon_high_season', this.addRowHighSeason.bind(this));
        //remove icon row
        $(document).on('click','.delete_row', this.deleteRow.bind(this));
        $(document).on('change','.best_season_start', this.besttime.bind(this));
    }

    _initPlugin () {
        this.countriesTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: null, name: null, defaultContent: ''},
                {data: 'id', name: this.config + '.id'},
                {data: 'title', name: 'transsingle.title'},
                {data: 'code', name: this.config + '.code'},
                {data: 'lat', name: this.config + '.lat'},
                {data: 'lng', name: this.config + '.lng'},
                {
                    name: this.config + '.active',
                    data: 'active',
                    sortable: false,
                    searchable: false,
                    render: function(data) {
                        if (data == 1) {
                            return '<label class="label label-success">Active</label>';
                        } else {
                            return '<label class="label label-danger">Deactive</label>';
                        }
                    }
                },
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });

        $.each(this.selectElements, function( index, value ) {
            value.select2({
                placeholder: value.data('placeholder'),
                minimumInputLength: 2,
                quietMillis: 10,
                ajax: {
                    url: value.data('url'),
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term),
                            page: params.page || 1
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.data,
                            pagination: {
                                more: data.paginate
                            }
                        };
                    },
                    cache: true
                }
            });
        });

        if (/admin\/location\/country\/\d+$/.test(location.pathname.substring(1)) == false) {
            this.registerSummernote('.description', 5000, function(max) {
                $('.textarea-notification').text('maxlength'+max)
                if (max == 0) {
                    var msg = '<div id="language-alert" class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong>Description should not be greater than 5000 characters.</div>';
                    var textareaMsg = $('.description').parent().siblings('.textarea-msg')
                    textareaMsg.html(msg)
                    $(".textarea-msg").fadeTo(5000, 500).slideUp(500, function () {
                        $(".textarea-msg").slideUp(500);
                    });
                }
            });
        }

        $('[data-toggle="tooltip"]').tooltip();
        $('.select2Class').select2();
    }

    initAutocomplete() {
        var placeLat = Number($('#map').data('lat'));
        var placeLng = Number($('#map').data('lng'));

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: placeLat, lng: placeLng},
            zoom: 9,
            mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        if (document.getElementById('pac-input')) {
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });
        }

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        if (location.pathname.substring(1) !== 'admin/location/country/create') {
            var myLatLng = {lat: placeLat, lng: placeLng};

            markers.push(new google.maps.Marker({
                map: map,
                position: myLatLng,
                draggable : true,
            }));

            google.maps.event.addListener(markers[0], 'dragend', function (evt) {
                var lat =  evt.latLng.lat().toFixed(3);
                var longit = evt.latLng.lng().toFixed(3);
                if (document.getElementById('lat-lng-input') && document.getElementById('lat-lng-input_show')) {
                    document.getElementById('lat-lng-input').setAttribute("value", lat + "," + longit );
                    document.getElementById('lat-lng-input_show').setAttribute("value", lat + "," + lat );
                }
            });
        }

        if (searchBox) {
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        draggable : true,
                        position: place.geometry.location
                    }));

                    var latitude = place.geometry.location.lat();
                    var longitude = place.geometry.location.lng();
                    document.getElementById('lat-lng-input').setAttribute("value", latitude + "," + longitude );
                    document.getElementById('lat-lng-input_show').setAttribute("value", latitude + "," + longitude );

                    google.maps.event.addListener(markers[0], 'dragend', function (evt) {
                        var lat =  evt.latLng.lat().toFixed(3);
                        var longit = evt.latLng.lng().toFixed(3);
                        document.getElementById('lat-lng-input').setAttribute("value", lat + "," + longit );
                        document.getElementById('lat-lng-input_show').setAttribute("value", lat + "," + longit );
                    });

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }
    }

    besttime(e) {
        let months = [
            'select month',
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ]
        let selectId = e.target.id.split("_")[0]
        const Start = $(`#${selectId}_start`).val()

        if (+Start == 12) {
            $(`#${selectId}_end`).val(1)
            $(`#select2-${selectId}_end-container`).html(months[1])
        } else if(Start == "") {
            $(`#${selectId}_end`).val("")
            $(`#select2-${selectId}_end-container`).html(months[0])
        } else {
            $(`#${selectId}_end`).val(+Start+1)
            $(`#select2-${selectId}_end-container`).html(months[+Start+1])
        }
    };

    selectDeselectAll(){
            $(".select-all").trigger( "click" );
    }

    deleteAllSelected(){
        if(this.countriesTable.find('tbody tr.selected').length == 0){
            alert('Please select some rows first.');
            return false;
        }

        if(!confirm('Are you sure you want to delete the selected rows?')){
            return false;
        }
        $('#deleteLoadingCountries').show();
        $('#deleteLoadingCountries .fa-spin').css("z-index","5");
        $('.table-responsive').css("opacity","0.2");
        $('.select-checkbox').click(function(e) {
            e.stopPropagation();
        });
        var ids = '';
        var i = 0;
        this.countriesTable.find('tbody tr.selected').each(function(){
            if(i != 0){
                ids += ',';
            }
            ids += $(this).find('td:nth-child(2)').html();
            i++;
        });

        $.ajax({
            url: this.countryDelRoute,
            type: 'post',
            data: {
                ids: ids
            },
            success: function(data){
                var result = JSON.parse(data);
                if(result.result == true){
                    document.location.reload();
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Countries Deleted Successfully</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
                        $("#deleted-alert").slideUp(500);
                    });
                }
            }
        });
    }

    registerSummernote(element, max, callbackMax) {
        $(element).summernote({
            height: 200,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    if (t.trim().length >= max) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    if (typeof callbackMax == 'function') {
                        callbackMax(max - t.trim().length);
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    document.execCommand('insertText', false, all.trim().substring(0, max));
                    if (typeof callbackMax == 'function') {
                        callbackMax(max - t.length);
                    }
                }
            }
        });
    }

    textareaType(event) {
        var key = event.which;
        var msg = '<div id="language-alert" class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong>Text should not be greater than 5000 characters.</div>';
        if(key != 8) {
            var maxLength = $(event.currentTarget).attr('maxlength');
            var length = event.currentTarget.value.length;
            if(length >= maxLength) {
                var textareaMsg = $(event.target).closest('.form-group').find('.textarea-msg')
                textareaMsg.html(msg)
                textareaMsg.fadeTo(5000, 500).slideUp(500, function () {
                    textareaMsg.slideUp(500);
                });
            }
        }
    }

    addRow(e) {
        var newRow = $("<tr></tr>"),
        tbCols = [];

        tbCols.push('<td class="set_img_col"><input type="file" name="license_images['+this.mediaIndex+'][image]"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][title]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][author_name]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][author_url]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][source_url]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][license_name]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][license_url]" class="form-control"></td>');

        tbCols.push('<td><a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a></td>');
        this.mediaIndex++;
        newRow.append(tbCols.join("\r\n"));

        if ($('.delete_row').length <= 9) {
            $("#addMoreRows").append(newRow);
        } else {
            $(".set_error_msg").html('Only 10 Images Rows can Added');
            $(".alert-warning").slideDown();
            setTimeout(function() {
                $(".alert-warning").slideUp();
            }, 2000);
            return false;
        }
    }


    addRowHoliday() {
        this.count++;

        $('#addMoreRowsHoliday').append('' +
            '<tr>'+
            '<td width="70%"><div width="100%" class="sel_'+this.count+'"></div></td>'+
            '<td class="date_'+this.count+'"></td>'+
            '<td>'+'<a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a>'+'</td>'+
            '</tr>');

        let myClone = $('#holidaysSelectHidden').clone();
        myClone.attr('id', 'holidaysSelect_'+this.count);
        myClone.attr('name','holidays_id[]');
        $(".sel_"+this.count).append(myClone);

        let myClone2 = $('#holidaysDateHidden').clone();
        myClone2.css('display','block');
        myClone2.attr('name','holidays_date[]');
        myClone2.attr('required', 'required');
        $(".date_"+this.count).append(myClone2);
    }
    addRowLifestyle() {
        this.lifestyleCount++;
        $('#addMoreRowsLifestyle').append('' +
            '<tr>'+
            '<td width="70%"><div width="100%" class="sel_'+this.lifestyleCount+'"></div></td>'+
            '<td class="rating_'+this.lifestyleCount+'"></td>'+
            '<td>'+'<a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a>'+'</td>'+
            '</tr>');

        let myClone = $('#lifestylesSelectHidden').clone();
        myClone.attr('id', 'lifestyleSelect_'+this.lifestyleCount);
        myClone.attr('name','lifestyles_id[]');
        $(".sel_"+this.lifestyleCount).append(myClone);

        let myClone2 = $('#lifestyleRatingHidden').clone();
        myClone2.css('display','block');
        myClone2.attr('name','lifestyles_rating[]');
        $(".rating_"+this.lifestyleCount).append(myClone2);
    }

    addRowRestrictions() {
        this.restrictionsCount++;
        $('#addMoreRowsRestrictions').append('' +
            '<tr>' +
                '<td>' +
                    '<div class="col-lg-12 restrictions_title_'+this.restrictionsCount+'">' +
                    '</div><!--col-lg-12-->' +
                '</td>' +
                '<td>' +
                    '<div class="col-lg-12 restrictions_'+this.restrictionsCount+'">' +
                    '</div><!--col-lg-12-->' +
                '</td>' +
                '<td>' +
                    '<a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a>' +
                '</td>' +
            '</tr>' 
        );

        let myClone = $('#restrictions_title').clone();
        myClone.attr('id', 'restrictions_title_'+this.restrictionsCount);
        myClone.attr('name','abouts[title][]');
        $(".restrictions_title_"+this.restrictionsCount).append(myClone);

        let myClone1 = $('#restrictions_type').clone();
        myClone1.attr('id', 'restrictions_type_'+this.restrictionsCount);
        myClone1.attr('name','abouts[type][]');
        $(".restrictions_title_"+this.restrictionsCount).append(myClone1);
        
        let myClone2 = $('#restrictions').clone();
        myClone2.attr('id', 'restrictions_  '+this.restrictionsCount);
        myClone2.attr('name','abouts[body][]');
        $(".restrictions_"+this.restrictionsCount).append(myClone2);
    }

    addRowPlanningTips() {
        this.planning_tipsCount++;
        $('#addMoreRowsPlanningTips').append('' +
            '<tr>' +
                '<td>' +
                    '<div class="col-lg-12 planning_tips_title_'+this.planning_tipsCount+'">' +
                    '</div><!--col-lg-12-->' +
                '</td>' +
                '<td>' +
                    '<div class="col-lg-12 planning_tips_'+this.planning_tipsCount+'">' +
                    '</div><!--col-lg-12-->' +
                '</td>' +
                '<td>' +
                    '<a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a>' +
                '</td>' +
            '</tr>' 
        );

        let myClone = $('#planning_tips_title').clone();
        myClone.attr('id', 'planning_tips_title_'+this.planning_tipsCount);
        myClone.attr('name','abouts[title][]');
        $(".planning_tips_title_"+this.planning_tipsCount).append(myClone);

        let myClone1 = $('#planning_tips_type').clone();
        myClone1.attr('id', 'planning_tips_type_'+this.planning_tipsCount);
        myClone1.attr('name','abouts[type][]');
        $(".planning_tips_title_"+this.planning_tipsCount).append(myClone1);
        
        let myClone2 = $('#planning_tips').clone();
        myClone2.attr('id', 'planning_tips_'+this.planning_tipsCount);
        myClone2.attr('name','abouts[body][]');
        $(".planning_tips_"+this.planning_tipsCount).append(myClone2);
    }

    addRowHealthNotes() {
        this.health_notesCount++;
        $('#addMoreRowsHealthNotes').append('' +
            '<tr>' +
                '<td>' +
                    '<div class="col-lg-12 health_notes_title_'+this.health_notesCount+'">' +
                    '</div><!--col-lg-12-->' +
                '</td>' +
                '<td>' +
                    '<div class="col-lg-12 health_notes_'+this.health_notesCount+'">' +
                    '</div><!--col-lg-12-->' +
                '</td>' +
                '<td>' +
                    '<a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a>' +
                '</td>' +
            '</tr>' 
        );

        let myClone = $('#health_notes_title').clone();
        myClone.attr('id', 'health_notes_title_'+this.health_notesCount);
        myClone.attr('name','abouts[title][]');
        $(".health_notes_title_"+this.health_notesCount).append(myClone);

        let myClone1 = $('#health_notes_type').clone();
        myClone1.attr('id', 'health_notes_type_'+this.health_notesCount);
        myClone1.attr('name','abouts[type][]');
        $(".health_notes_title_"+this.health_notesCount).append(myClone1);
        
        let myClone2 = $('#health_notes').clone();
        myClone2.attr('id', 'health_notes_'+this.health_notesCount);
        myClone2.attr('name','abouts[body][]');
        $(".health_notes_"+this.health_notesCount).append(myClone2);
    }

    addRowPotentialDangers() {
        this.potential_dangersCount++;
        $('#addMoreRowsPotentialDangers').append('' +
            '<tr>' +
                '<td>' +
                    '<div class="col-lg-12 potential_dangers_title_'+this.potential_dangersCount+'">' +
                    '</div><!--col-lg-12-->' +
                '</td>' +
                '<td>' +
                    '<div class="col-lg-12 potential_dangers_'+this.potential_dangersCount+'">' +
                    '</div><!--col-lg-12-->' +
                '</td>' +
                '<td>' +
                    '<a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a>' +
                '</td>' +
            '</tr>' 
        );

        let myClone = $('#potential_dangers_title').clone();
        myClone.attr('id', 'potential_dangers_title_'+this.potential_dangersCount);
        myClone.attr('name','abouts[title][]');
        $(".potential_dangers_title_"+this.potential_dangersCount).append(myClone);

        let myClone1 = $('#potential_dangers_type').clone();
        myClone1.attr('id', 'potential_dangers_type_'+this.potential_dangersCount);
        myClone1.attr('name','abouts[type][]');
        $(".potential_dangers_title_"+this.potential_dangersCount).append(myClone1);
        
        let myClone2 = $('#potential_dangers').clone();
        myClone2.attr('id', 'potential_dangers_'+this.potential_dangersCount);
        myClone2.attr('name','abouts[body][]');
        $(".potential_dangers_"+this.potential_dangersCount).append(myClone2);
    }

    addRowSpeedLimit() {
        this.speed_limitCount++;
        $('#addMoreRowsSpeedLimit').append('' +
            '<tr>' +
                '<td>' +
                    '<div class="col-lg-12 speed_limit_title_'+this.speed_limitCount+'">' +
                    '</div><!--col-lg-12-->' +
                '</td>' +
                '<td>' +
                    '<div class="col-lg-12 speed_limit_'+this.speed_limitCount+'">' +
                    '</div><!--col-lg-12-->' +
                '</td>' +
                '<td>' +
                    '<a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a>' +
                '</td>' +
            '</tr>' 
        );

        let myClone = $('#speed_limit_title').clone();
        myClone.attr('id', 'speed_limit_title_'+this.speed_limitCount);
        myClone.attr('name','abouts[title][]');
        $(".speed_limit_title_"+this.speed_limitCount).append(myClone);

        let myClone1 = $('#speed_limit_type').clone();
        myClone1.attr('id', 'speed_limit_type_'+this.speed_limitCount);
        myClone1.attr('name','abouts[type][]');
        $(".speed_limit_title_"+this.speed_limitCount).append(myClone1);
        
        let myClone2 = $('#speed_limit').clone();
        myClone2.attr('id', 'speed_limit_'+this.speed_limitCount);
        myClone2.attr('name','abouts[body][]');
        $(".speed_limit_"+this.speed_limitCount).append(myClone2);
    }

    addRowEtiquette() {
        this.etiquetteCount++;
        $('#addMoreRowsEtiquette').append('' +
            '<tr>' +
                '<td>' +
                    '<div class="col-lg-12 etiquette_title_'+this.etiquetteCount+'">' +
                    '</div><!--col-lg-12-->' +
                '</td>' +
                '<td>' +
                    '<div class="col-lg-12 etiquette_'+this.etiquetteCount+'">' +
                    '</div><!--col-lg-12-->' +
                '</td>' +
                '<td>' +
                    '<a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a>' +
                '</td>' +
            '</tr>' 
        );

        let myClone = $('#etiquette_title').clone();
        myClone.attr('id', 'etiquette_title_'+this.etiquetteCount);
        myClone.attr('name','abouts[title][]');
        $(".etiquette_title_"+this.etiquetteCount).append(myClone);

        let myClone1 = $('#etiquette_type').clone();
        myClone1.attr('id', 'etiquette_type_'+this.etiquetteCount);
        myClone1.attr('name','abouts[type][]');
        $(".etiquette_title_"+this.etiquetteCount).append(myClone1);
        
        let myClone2 = $('#etiquette').clone();
        myClone2.attr('id', 'etiquette_'+this.etiquetteCount);
        myClone2.attr('name','abouts[body][]');
        $(".etiquette_"+this.etiquetteCount).append(myClone2);
    }

    addRowTransportation() {
        this.transportationCount++;
        $('#addMoreRowsTransportation').append('' +
            '<tr>'+
                '<td>'+
                    '<div class="transport_'+this.transportationCount+'">'+
                    '</div>'+    
                '</td>'+
                '<td>'+
                    '<a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row" style="width:100%">‐</a>'+
                '</td>'+
            '</tr>'
        );

        let myClone = $('#transport').clone();
        myClone.attr('id', 'transport_'+this.etiquetteCount);
        $(".transport_"+this.transportationCount).append(myClone);

    }

    addRowLowSeason() {
        this.low_Count++;
        $('#addMoreRowslow_season').append('' +
            '<tr>' +
                '<td class="low_start_'+this.low_Count+'">' +
                '</td>' +
                '<td class="low_end_'+this.low_Count+'">' +
                '</td>' +
                '<td>' +
                    '<a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row" style="width:100%">‐</a>' +
                '</td>' +
            '</tr>' 
        );

        let myClone = $('#monthSelectHidden-low_start').clone();
        myClone.attr({'id': this.low_Count+'low_start', 'class': 'best_season_start', 'name': 'best_time[low_season][low_start][]'}).val('').select2();
        $(".low_start_"+this.low_Count).append(myClone);

        let myClone2 = $('#monthSelectHidden-low_end').clone();
        myClone2.attr({'id': this.low_Count+'low_end', 'name': 'best_time[low_season][low_end][]'}).val('').select2();
        $(".low_end_"+this.low_Count).append(myClone2);
    }
    
    addRowShoulder() {
        this.shoulder_Count++;
        $('#addMoreRowsshoulder').append('' +
            '<tr>' +
                '<td class="shoulder_start_'+this.shoulder_Count+'">' +
                '</td>' +
                '<td class="shoulder_end_'+this.shoulder_Count+'">' +
                '</td>' +
                '<td>' +
                    '<a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row" style="width:100%">‐</a>' +
                '</td>' +
            '</tr>' 
        );

        let myClone = $('#monthSelectHidden-shoulder_start').clone();
        myClone.attr({'id': this.shoulder_Count+'shoulder_start', 'class': 'best_season_start', 'name': 'best_time[shoulder][shoulder_start][]'}).val('').select2();
        $(".shoulder_start_"+this.shoulder_Count).append(myClone);

        let myClone2 = $('#monthSelectHidden-shoulder_end').clone();
        myClone2.attr({'id': this.shoulder_Count+'shoulder_end', 'name': 'best_time[shoulder][shoulder_end][]'}).val('').select2();
        $(".shoulder_end_"+this.shoulder_Count).append(myClone2);
    }

    addRowHighSeason() {
        this.high_Count++;
        $('#addMoreRowshigh_season').append('' +
            '<tr>' +
                '<td class="high_start_'+this.high_Count+'">' +
                '</td>' +
                '<td class="high_end_'+this.high_Count+'">' +
                '</td>' +
                '<td>' +
                    '<a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row" style="width:100%">‐</a>' +
                '</td>' +
            '</tr>' 
        );

        let myClone = $('#monthSelectHidden-high_start').clone();
        myClone.attr({'id': this.high_Count+'high_start', 'class': 'best_season_start', 'name': 'best_time[high_season][high_start][]'}).val('').select2();
        $(".high_start_"+this.high_Count).append(myClone);

        let myClone2 = $('#monthSelectHidden-high_end').clone();
        myClone2.attr({'id': this.high_Count+'high_end', 'name': 'best_time[high_season][high_end][]'}).val('').select2();
        $(".high_end_"+this.high_Count).append(myClone2);
    }
    deleteRow(e) {
        $(e.currentTarget).closest("tr").remove();
    }
}