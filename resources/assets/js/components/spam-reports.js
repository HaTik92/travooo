import BaseComponent from "../core/baseComponent";
import Map from '../helpers/map';

export default class UnapprovedItemsComponent extends BaseComponent {

    _initProperty() {
        this.table;
        this.spamReportsTable = $('#spamReportsTable');
    }

    get url() {
        return this.spamReportsTable.data('url');
    }

    _initPlugin() {
        this.table = this.spamReportsTable.DataTable({
            lengthMenu: [10, 25, 50, 100, 1000, 10000],
            deferRender: true,
            columnDefs: [
                {
                    orderable: true,
                },
            ],
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: function () {

                }
            },
            columns: [
                {data: 'id', name: 'id', visible: true},
                {data: 'report_text', name: 'report_text', visible: true},
                {data: 'action', name: 'action', searchable: false, sortable: false, visible: true},
            ],
            order: [[1, "asc"]],
            searchDelay: 500,
        });
    }
}