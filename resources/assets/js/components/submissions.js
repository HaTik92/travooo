import BaseComponent from "../core/baseComponent";

export default class SubmissionsComponent extends BaseComponent {

    _initProperty () {
        this.submissionsTable    = $('#submissions-table');
        // this.pagesCatTable = $('#page-categories-table');
    }

    get url() {
        return this.submissionsTable.data('url');
    }

    get config() {
        return this.submissionsTable.data('config');
    }

    _initBind () {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin () {
        super._initPlugin();
        this.submissionsTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'user_name', name: this.config + '.user_name'},
                {data: 'user_email', name: this.config + '.user_email'},
                {data: 'user_id', name: this.config + '.user_id'},
                {data: 'place_name', name: this.config + '.place_name'},
                {data: 'place_category', name: this.config + '.place_category'},
                {data: 'place_hyperlink', name: this.config + '.place_hyperlink', render: function ( data, type, row ) {
                    return '<a href=' + data + ' target="_blank">Show place</a>'
                }},
                {data: 'place_city', name: this.config + '.place_city'},
                {data: 'place_address', name: this.config + '.place_address'},
                {data: 'created_at', name: this.config + '.created_at'},
                {data: 'updated_at', name: this.config + '.updated_at'},
                {data: 'actions', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }
}