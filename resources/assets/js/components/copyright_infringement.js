import BaseComponent from "../core/baseComponent";

export default class CopyrightInfringementComponent extends BaseComponent {


    _initProperty() {
        // CopyrightInfringementComponent
        this.table;
        this.tableEl = $('#copyrightInfringementsTable');

        this.usersTable;
        this.usersTableEl = $('#copyrightInfringementsTable');
    }

    _initBind() {
        let self = this;
        $(document).on('click', '.reply_btn', self.replyRequest.bind(self));
        $(document).on('click', '.delete_btn', self.deleteRequest.bind(self));
        $(document).on('click', '#confirm_btn', self.confirmRequest.bind(self));
        $(document).on('click', '#block-user-btn', self.blockUserBtn.bind(self));
        $(document).on('click', '#toggle-button', self.showMoreForms.bind(self));
        $(document).on('click', '#delete_btn', self.deleteRequest.bind(self));
        $('#copyrightInfringementsTable').change(function () {
            self.tableEl.columns().search($('#copyrightInfringementsTable').val()).draw();
        });
        $('#inputSearch').on('keyup', function () {
            this.table.search(this.value).draw();
        });
        $('#copyrightInfringementsTable').change(function () {
            self.tableEl.search($('#copyrightInfringementsTable').val()).draw();
        });
        $('#copyrightInfringementsTable').on( 'page.dt', function () {
            $('html, body').animate({
                scrollTop: $('#copyrightInfringementsTable').offset().top
            }, 200);
        });
    }

    get url() {
        return this.tableEl.data('url');
    }

    get usersUrl() {
        return this.tableEl.data('url');
    }

    _initPlugin() {
        console.log(this.url);
        this.table = this.tableEl.DataTable({
            lengthMenu: [10, 25, 50, 100, 1000, 10000],
            deferRender: true,
            columnDefs: [
                {
                    orderable: true,
                },
            ],
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: function (request) {
                    request.blacklist = $('#copyrightInfringementsTable').data('blacklist');
                }
            },
            columns: [
                {data: 'id', name: 'id', visible: true},
                {data: 'sender_email', name: 'sender_email', visible: true},
                {
                    data: 'original_link',
                    name: 'original_link',
                    visible: true,
                    sortable: false,
                    searchable: false,
                    render: function (data) {
                        if (data) {
                            var links = JSON.parse(data);
                            var lastLink = '-';
                            var text = '';
                            let linksCount = links.length > 1 ? ' (' + links.length + ' more)' : ''
                            links.forEach(function (item, key) {
                                if (item) {
                                    text += '<tr>' +
                                        '<td>Link#' + (key + 1) + '</td>' +
                                        '<td><a href="' + item + '" target="_blank">' + item + '</a></td>' +
                                        '</tr>';
                                    lastLink = item.length < 25 ? item : item.substr(0, 25) + '...';
                                }
                            });
                            var sourceLink = '<div class="dropdown dropright">' +
                                '<a href="' + lastLink + ' " target="_blank">' + lastLink + '</a><a type="button" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false">' +
                                linksCount +
                                '</a>' +
                                '<div class="dropdown-menu" posttype="Source Link">' +
                                '<table>' +
                                '<tr>' +
                                '<th>N#</th>' +
                                '<th>Link</th>' +
                                '</tr>' +
                                text +
                                '</table>' +
                                '</div>' +
                                '</div>';
                            return sourceLink;
                        } else {
                            return '-';
                        }
                    }
                },
                {
                    data: 'forms',
                    name: 'forms',
                    visible: true,
                    render: function (data) {
                        let links = '';
                        if (data) {
                            links += '<div>';
                            data.forEach(function (id, key) {
                                let name = window.location.hostname + '/admin/copyright-infringement/' + id;
                                if (key < 1) {
                                    links += '<a target="_blank" href="copyright-infringement/' + id + '">' + name + '</a><br>';
                                } else {
                                    links += '<div class ="less" style="display: none"><a target="_blank" href="copyright-infringement/' + id + '">' + name + '</a><br></div>';
                                }
                            });
                            if (links.length > 25) {
                                links += '<a id="toggle-button" href="javascript:void(0);">See More</a>';
                            }
                            links += '</div>';
                        }
                        return links ? links : '-';
                    }
                },
                {
                    data: 'reported_url',
                    name: 'reported_url',
                    visible: true,
                    sortable: false,
                    searchable: false,
                    render: function (data) {
                        var link = data.length < 25 ? data : data.substr(0, 25) + '...';
                        return '<a href="' + data + '" data-toggle="tooltip" title="' + data + '" target="_blank">' + link + '</a>';
                    }
                },
                {
                    data: null,
                    visible: true,
                    sortable: false,
                    searchable: false,
                    render: function (data) {
                        if (data.user) {
                            return '<a target="_blank" href="/profile/' + data.owner_id + '">' + data.user.name + ' </a><small>(' + data.approvedInfractions + ')</small>';
                        } else {
                            return '-';
                        }
                    }
                },
                {data: 'infringement_location', name: 'infringement_location', visible: true},
                {
                    data: null,
                    searchable: false,
                    sortable: false,
                    visible: true,
                    render: function (data) {
                        return '' +
                            '<button id="confirm_btn" data-id="' + data.id + '"  class="btn btn-xs btn-success view_btn mx-1" title="Approve requests">' +
                            '    <i class="fa fa-check" style="pointer-events: none;" ></i>' +
                            '</button>&nbsp' +
                            '<a href="copyright-infringement/' + data.id + '" class="btn btn-xs btn-success view_btn mx-1" title="View requests">' +
                            '    <i class="fa fa-arrow-up" style="pointer-events: none;" ></i>' +
                            '</a>&nbsp' +
                            '<button type="button" data-id="' + data.owner_id + '" class="btn btn-xs btn-warning  mx-1" id="block-user-btn" title="Block User">' +
                            ' <i style="pointer-events: none;" class="fa fa-lock"></i>' +
                            '</button>&nbsp' +
                            '<button type="button" class="btn btn-xs btn-warning  mx-1" title="Reply requests" data-toggle="modal" data-target="#replyModal">' +
                            ' <i style="pointer-events: none;" class="fa fa-reply" ></i>' +
                            '</button>&nbsp' +
                            '<button  data-id="' + data.id + '" class="btn btn-xs btn-danger delete_btn  mx-1" data-toggle="tooltip" data-placement="top" title="Delete requests"><i style="pointer-events: none;" class="fa fa-times" aria-hidden="true"></i></button>&nbsp;' +
                            '<div class="modal fade" id="replyModal" tabindex="-1" role="dialog" aria-labelledby="replyModalLabel" aria-hidden="true">' +
                            '  <div class="modal-dialog" role="document">' +
                            '    <div class="modal-content">' +
                            '      <div class="modal-header">' +
                            '        <span class="modal-title" id="replyModalLabel">Reply copyright infringement</span>' +
                            '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                            '          <span aria-hidden="true">&times;</span>' +
                            '        </button>' +
                            '      </div>' +
                            '      <div class="modal-body">' +
                            '          <div class="form-group">\n' +
                            '                     <div class="form-group">\n' +
                            '                        <div class="row">\n' +
                            '                            <div class="col-sm-12">\n' +
                            '                                <textarea style="resize: none;" name="message" id="reply-message" class="form-control"  placeholder="Reply Text" autocomplete="off"></textarea>' +
                            '                            </div>\n' +
                            '                        </div>\n' +
                            '                    </div>' +
                            '          </div>' +
                            '      </div>' +
                            '      <div class="modal-footer">' +
                            '        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>' +
                            '        <button type="button" data-id="' + data.id + '"  class="btn btn-primary reply_btn">Reply</button>' +
                            '      </div>' +
                            '    </div>' +
                            '  </div>' +
                            '</div>' +
                            '';
                    }
                },
            ],
            order: [[0, "desc"]],
            searchDelay: 500,
        });

    }

    replyRequest(e) {
        var id = parseInt($(e.target).data('id'));
        $.ajax({
            url: 'copyright-infringement/reply',
            type: 'post',
            data: {
                id: id,
                message: $('#reply-message').val()
            },
            success: function (data) {
                var result = data.data;
                if (result.success === true) {
                    $("#replyModal").modal('toggle');
                    $("#replyModal").fadeOut();
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + result.message + '</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function () {
                        $("#deleted-alert").slideUp(500);
                    });
                }
            }
        });
    }

    deleteRequest(e) {
        var id = parseInt($(e.target).data('id'));
        if (!confirm('Are you sure you want to delete the post?')) {
            return false;
        }
        $.ajax({
            url: '/admin/copyright-infringement/delete',
            type: 'post',
            data: {
                id: id
            },
            success: function (data) {
                var result = data.data;
                if (result.success === true) {
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + result.message + '</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function () {
                        $("#deleted-alert").slideUp(500);
                    });

                }
                window.location.href = "/admin/copyright-infringement";
            }
        });
    }

    confirmRequest(e) {
        var id = parseInt($(e.target).data('id'));
        if (!confirm('Are you sure you want to confirm  the report?')) {
            return false;
        }
        $.ajax({
            url: '/admin/copyright-infringement/confirm',
            type: 'post',
            data: {
                id: id
            },
            success: function (data) {
                var result = data.data;
                if (result.success === true) {
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Post successfully deleted</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function () {
                        $("#deleted-alert").slideUp(500);
                    });
                }
                window.location.href = "/admin/copyright-infringement";
            }
        });
    }

    blockUserBtn(e) {
        var id = parseInt($(e.target).data('id'));
        console.log(id);
        $.ajax({
            url: '/admin/access/user/block',
            type: 'post',
            data: {
                id: id
            },
            success: function (data) {
                var result = data.data;
                if (result.success === true) {
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>User successfully blocked</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function () {
                        $("#deleted-alert").slideUp(500);
                    });
                }
                window.location.href = "/admin/copyright-infringement";
            }
        });
    }

    showMoreForms(e) {
        var moretext = 'See More';
        var lesstext = 'See Less';
        $(e.target).closest('div').find('.less').toggle();
        if ($(e.target).text() === moretext ) {
            $(e.target).text(lesstext);
        } else if ($(e.target).text() === lesstext) {
            $(e.target).text(moretext);
        }
    }
}
