import BaseComponent from "../core/baseComponent";

export default class RankCronComponent extends BaseComponent {
    _initProperty () {
        this.rankCronTable = $('#rank-cron-table');
    }

    get url() {
        return this.rankCronTable.data('url');
    }

    get truncateUrl() {
        return this.rankCronTable.data('truncate-url');
    }

    _initPlugin () {
        super._initPlugin();
        var dataTable = this.rankCronTable.DataTable({
            pageLength: 25,
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'POST',
                data: {status: 1}
            },
            columns: [
                {data: 'id',         name: 'id'},
                {data: 'cron_id',    name: 'cron_id'},
                {data: 'post_type',  name: 'post_type'},
                {data: 'action',     name: 'action'},
                {data: 'message',    name: 'message'},
                {data: 'post_id',    name: 'post_id'},
                {data: 'loop_count', name: 'loop_count'},
                {data: 'r_type',     name: 'r_type'},
                {data: 'created_at', name: 'created_at'},
            ],
            order: [[0, "desc"]],
            searchDelay: 500
        });

        var _this = this;
        $(document).on('click', '.truncate-rank-logs', function () {
            if (confirm('Are You Really Want To Delete All Data?')) {
                $.ajax({
                    type: "POST",
                    url: _this.truncateUrl,
                    success: function (result) {
                        if (result.success === true) {
                            dataTable.ajax.reload();
                        } else {
                            alert('Something Went Wrong!')
                        }
                    }
                });
            }
        })
    }
}