import Component from '../core/component';
import '../helpers/roleScript';
export default class UserRolesComponent extends Component {

    _initProperty () {
        this.roleTable = $('#roles-table');
    }

    get url() {
        return this.roleTable.data('url');
    }

    get config() {
        return this.roleTable.data('config');
    }

    _initPlugin () {
        this.roleTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post'
            },
            columns: [
                {data: 'name', name: this.config + '.name'},
                {data: 'permissions', name: 'permissions.display_name', sortable: false},
                {data: 'users', name: 'users', searchable: false, sortable: false},
                {data: 'sort', name: this.config + '.sort', sortable: false},
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ],
            order: [[3, "asc"]],
            searchDelay: 500
        });
    }
}