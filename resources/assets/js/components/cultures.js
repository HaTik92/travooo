import BaseComponent from "../core/baseComponent";

export default class CulturesComponent extends BaseComponent {

    _initProperty () {
        this.culturesTable = $('#cultures-table');
    }

    get url() {
        return this.culturesTable.data('url');
    }

    get config() {
        return this.culturesTable.data('config');
    }

    _initBind () {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }
    _initPlugin () {
        super._initPlugin();
        this.culturesTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }
}