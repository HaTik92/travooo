import BaseComponent from "../core/baseComponent";
import Map from '../helpers/map';

export default class TopHotelsComponent extends BaseComponent {

    _initProperty() {
        this.table;
        this.hotelsTable = $('#top_hotel_table');
        this.selectElements = $('#hotelCitiesSelect');
        this.selectHotelElements = $('#topHotelSelect');
        this.mediaIndex = 1;
        global.selected_country = '';
        global.selected_city = '';
        global.initAutocomplete = window.initAutocomplete = this.initAutocomplete;
    }

    get url() {
        return this.hotelsTable.data('url');
    }

    get config() {
        return this.hotelsTable.data('config');
    }

    get placeDelRoute() {
        return this.hotelsTable.data('del');
    }

    _initBind() {
        $(document).on('click', '#select-all', this.selectDeselectAll.bind(this));
        $(document).on('click', '#delete-all-selected', this.deleteAllSelected.bind(this));
        $(document).on('click', '.city-order-column', this.openSelectedInput.bind(this));
        $(document).on('keyup', '.city-order-input', this.updateSorting.bind(this));
        $(document).on('blur', '.city-order-input', this.sortFilter.bind(this));
        $(document).on('change', '#not-address-filter', this.notAddressFilter.bind(this));
        $(document).on('change', '#address-filter', this.addressFilter.bind(this));
        $(document).on('change', '#country-filter', this.countryFilter.bind(this));
        $(document).on('change', '#city-filter', this.cityFilter.bind(this));
        $('textarea[maxlength]').keypress(this.textareaType.bind(this));
    }

    _initPlugin() {
        this.table = this.hotelsTable.DataTable({
            lengthMenu: [10, 25, 50, 100, 1000, 10000],
            deferRender: true,
            columnDefs: [{
                    orderable: true,
                    className: 'select-checkbox',
                    targets: 0,
                },
                { className: 'text-center', "targets": 5 },
                { "width": "20%", "targets": [6, 7] },
                { "searchable": false, "targets": 0 }
            ],
            select: {
                style: 'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: function(request) {
                    var titleIndex;
                    request.status = 1;
                    request.trashed = false;
                    request.columns.forEach(function(item, index) {
                        if (item.data == "address") {
                            request.columns[index].search.value1 = { matchValue: $('#address-filter').val() };
                            request.columns[index].search.value2 = { notMatchValue: $('#not-address-filter').val() };
                        }
                        if (item.data == "title") {
                            titleIndex = index;
                        }
                    });
                    if (request.search.value != "") {
                        request.columns[titleIndex].search.value = request.search.value;
                        request.columns[titleIndex].search.regex = "false";
                        // request.search.value = null;
                    }
                }
            },
            columns: [
                { data: null, name: null, defaultContent: '', orderable: false },
                { data: 'id', name: 'id', visible: false },
                {
                    name: 'places_id',
                    data: 'places_id',
                    sortable: true,
                    searchable: false,
                    render: function(data, type, row) {
                        return '<a href="/place/'+ data +'" target="_blank">'+ data +'</a>';
                    },
                    visible: true
                },
                { data: 'title', name: 'title', visible: true },
                { data: 'address', name: 'address', visible: true },
                { data: 'country_title', name: 'country_title', visible: true },
                { data: 'city_title', name: 'city_title', visible: true },
                { data: 'place_id_title', name: 'place_type', visible: true },
                { data: 'reviews_num', name: 'reviews_num', visible: true },
                { data: 'rating', name: 'rating', visible: true },
                {
                    name: 'order_num',
                    data: 'order_num',
                    sortable: true,
                    searchable: false,
                    render: function(data, type, row) {
                        return '<span class="city-order-column">'+ data +'</span>' +
                            '<div class="city-order-block"><input type="number" id="'+ data +'" data-placeId="' + row.id + '"  class="city-order-input"  value="'+ data +'"></div>';
                    },
                    visible: true
                },
                { data: 'media_count', name: 'media_count', visible: true },
                {
                    name: this.config + '.active',
                    data: 'active',
                    sortable: false,
                    searchable: false,
                    render: function(data) {
                        if (data == 1) {
                            return '<label class="label label-success">Active</label>';
                        } else {
                            return '<label class="label label-danger">Deactive</label>';
                        }
                    },
                    visible: true
                },
                { data: 'action', name: 'action', searchable: false, sortable: false, visible: true },
            ],
            order: [
                [10, "asc"]
            ],
            searchDelay: 500,
            initComplete: function() {
                $('#top_hotel_table thead').append('<tr><td></td><td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td></tr>');
                var count = 0;
                $('#top_hotel_table thead tr:nth-child(2) td').each(function() {

                    if (count == 3) {
                        $(this).html(
                            '<input type="text" id="address-filter" placeholder="match" class="custom-filters form-control" style="width:125px; margin-bottom: 5px" />' +
                            '<input type="text" id="not-address-filter" placeholder="not match" class="custom-filters form-control" style="width:125px" />'
                        );
                    }

                    if (count == 4) {
                        $(this).html('<select id="country-filter" class="custom-filters form-control"><option value="">Search Country</option></select>');
                    }

                    if (count == 5) {
                        $(this).html('<select id="city-filter" class="custom-filters form-control"><option value="">Search City</option></select>');
                    }
                    count++;
                });

                $('#country-filter').select2({
                    width: '100%',
                    placeholder: 'Search Country',
                    allowClear: true,
                    ajax: {
                        url: $('#hotelCountry').val(),
                        dataType: 'json',
                        delay: 250,
                        processResults: function(data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });
                $('#city-filter').select2({
                    width: '100%',
                    placeholder: 'Search City',
                    minimumInputLength: 2,
                    allowClear: true,
                    ajax: {
                        url: $('#searchCity').val(),
                        dataType: 'json',
                        delay: 250,
                        data: function(params) {
                            return {
                                q: $.trim(params.term),
                                page: params.page || 1,
                                country: global.selected_country
                            };
                        },
                        processResults: function(data) {
                            return {
                                results: data.data,
                                pagination: {
                                    more: data.paginate
                                }
                            };
                        },
                        cache: true
                    }
                });
            }
        });


            this.selectElements.select2({
                placeholder: this.selectElements.data('placeholder'),
                minimumInputLength: 2,
                quietMillis: 10,
                ajax: {
                    url: this.selectElements.data('url'),
                    dataType: 'json',
                    data: function(params) {
                        return {
                            q: $.trim(params.term),
                            page: params.page || 1,
                            countryId: $('.country-input').val()
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: data.data,
                            pagination: {
                                more: data.paginate
                            }
                        };
                    },
                    cache: true
                }
            });

        this.selectElements.on('select2:select', function (e) {
            var data = e.params.data;
            global.selected_city = data.id;
        });


        this.selectHotelElements.select2({
            placeholder: this.selectHotelElements.data('placeholder'),
            minimumInputLength: 2,
            quietMillis: 10,
            ajax: {
                url: this.selectHotelElements.data('url'),
                dataType: 'json',
                data: function(params) {
                    return {
                        q: $.trim(params.term),
                        lat: window.locationLat,
                        lng: window.locationLng,
                        city_id: global.selected_city,
                        without_users:true
                    };
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(obj) {
                            if (obj.place_type == 'lodging') {
                                return { id: obj.id, text: obj.title};
                            }
                        })
                    };
                },
                cache: true
            }
        });


        $('[data-toggle="tooltip"]').tooltip();
        $('.select2Class').select2();
    }

    selectDeselectAll() {
        $(".select-all").trigger("click");
    }


    openSelectedInput(e){
        var _this = $(e.currentTarget);
        $('.city-order-block').hide();
        var input = _this.closest('td').find('.city-order-input');
        if(input.val() == 0){
            input.val('');
        }

        _this.closest('td').find('.city-order-block').show();
        $('.city-order-column').show();
        _this.hide();
    }


    updateSorting(e){

    }



    deleteAllSelected() {
        if (this.hotelsTable.find('tbody tr.selected').length == 0) {
            alert('Please select some rows first.');
            return false;
        }

        if (!confirm('Are you sure you want to delete the selected rows?')) {
            return false;
        }
        $('#deleteLoadingPlace').show();
        $('#deleteLoadingPlace .fa-spin').css("z-index", "5");
        $('.table-responsive').css("opacity", "0.2");
        $('.select-checkbox').click(function(e) {
            e.stopPropagation();
        });
        var ids = '';
        var i = 0;
        this.hotelsTable.find('tbody tr.selected').each(function() {
            if (i != 0) {
                ids += ',';
            }
            ids += $(this).find('td:nth-child(2)').html();
            i++;
        });

        $.ajax({
            url: this.placeDelRoute,
            type: 'post',
            data: {
                ids: ids
            },
            success: function(data) {
                var result = JSON.parse(data);
                if (result.result == true) {
                    document.location.reload();
                }
            }
        });
    }

    notAddressFilter() {
        this.table.draw();
    }

    addressFilter() {
        this.table.draw();
    }

    sortFilter(e) {
        var _this = $(e.currentTarget);
        var url = $('#UpdateSorting').val();
        var place_id = _this.attr('data-placeId');
        var prev_sort_id = _this.attr('data-placeId');
        var sort_num = _this.val();
        var _current =  this.table;

        $.ajax({
            url: url,
            type: 'post',
            data: {
                id: place_id,
                val: sort_num
            },
            success: function(data) {
                _current.draw();
            }
        });

    }





    countryFilter(e) {
        var val = $(e.currentTarget).val();
        var dataTable = this.table;
        global.selected_country = val;
        if (val != '') {
            dataTable.settings().init().columns.forEach(function(item, index) {
                if (item.data == "country_title") {
                    if (dataTable.columns(index).search() !== val) {
                        dataTable.columns(index).search(val, false).draw();
                    }
                }
            });
        } else {
            dataTable.settings().init().columns.forEach(function(item, index) {
                if (item.data == "country_title") {
                    dataTable.columns(index).search('').draw();
                }
            });
        }
    }


    cityFilter(e) {
        var val = $(e.currentTarget).val();
        var dataTable = this.table;
        if (val != '') {
            dataTable.settings().init().columns.forEach(function(item, index) {
                if (item.data == "city_title") {
                    if (dataTable.columns(index).search() !== val) {
                        dataTable.columns(index).search(val, false).draw();
                    }
                }
            });
        } else {
            dataTable.settings().init().columns.forEach(function(item, index) {
                if (item.data == "city_title") {
                    dataTable.columns(index).search('').draw();
                }
            });
        }
    }



    textareaType(event) {
        var key = event.which;
        var msg = '<div id="language-alert" class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong>Text should not be greater than 5000 characters.</div>';
        if (key != 8) {
            var maxLength = $(event.currentTarget).attr('maxlength');
            var length = event.currentTarget.value.length;
            if (length >= maxLength) {
                var textareaMsg = $(event.target).closest('.form-group').find('.textarea-msg')
                textareaMsg.html(msg)
                textareaMsg.fadeTo(5000, 500).slideUp(500, function() {
                    textareaMsg.slideUp(500);
                });
            }
        }
    }


    initAutocomplete() {

    }

}
