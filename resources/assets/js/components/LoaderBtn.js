import Vue from 'vue/dist/vue.js'

export default Vue.component('LoaderBtn', {
    name: 'LoaderBtn',

    template: '<button :disabled="loading || disabled" class="position-relative" type="button" v-on="$listeners">' +
        '<svg style="position: absolute; width: 0; height: 0; overflow: hidden" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">' +
        '    <defs>' +
        '        <symbol id="icon-spinner" viewBox="0 0 32 32">' +
        '            <path d="M12 4c0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.209-1.791 4-4 4s-4-1.791-4-4zM20.485 7.515c0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.209-1.791 4-4 4s-4-1.791-4-4zM26 16c0-1.105 0.895-2 2-2s2 0.895 2 2c0 1.105-0.895 2-2 2s-2-0.895-2-2zM22.485 24.485c0-1.105 0.895-2 2-2s2 0.895 2 2c0 1.105-0.895 2-2 2s-2-0.895-2-2zM14 28c0 0 0 0 0 0 0-1.105 0.895-2 2-2s2 0.895 2 2c0 0 0 0 0 0 0 1.105-0.895 2-2 2s-2-0.895-2-2zM5.515 24.485c0 0 0 0 0 0 0-1.105 0.895-2 2-2s2 0.895 2 2c0 0 0 0 0 0 0 1.105-0.895 2-2 2s-2-0.895-2-2zM4.515 7.515c0 0 0 0 0 0 0-1.657 1.343-3 3-3s3 1.343 3 3c0 0 0 0 0 0 0 1.657-1.343 3-3 3s-3-1.343-3-3zM1.75 16c0-1.243 1.007-2.25 2.25-2.25s2.25 1.007 2.25 2.25c0 1.243-1.007 2.25-2.25 2.25s-2.25-1.007-2.25-2.25z"></path>' +
        '        </symbol>' +
        '    </defs>' +
        '</svg>' +
        '        <div :class="{transparent: ! loading}" class="spinner">' +
        '           <svg v-show="loading" class="icon icon-spinner fa-spin"><use xlink:href="#icon-spinner"></use></svg>' +
        '        </div>' +
        '' +
        '        <span :class="{transparent:loading}">' +
        '            <slot>Send</slot>' +
        '        </span>' +
        '</button>',

    props: {
        loading:{
            type: Boolean,
            default: false
        },
        disabled:{
            type: Boolean,
            default: false
        },
    },
    mounted() {

    }
});

