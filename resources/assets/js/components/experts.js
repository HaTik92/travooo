import Component from '../../../../public/assets2/js/core/component';
import '../../../../public/assets2/js/helpers/userPermissions';
import Swal from 'sweetalert2';

export default class ExpertComponent extends Component {
    _initProperty () {
        this.userTable      = $('#users-table');
        this.status         = this.userTable.data('status') ? this.userTable.data('status') : false;
        this.trashed        = this.userTable.data('trashed') ? this.userTable.data('trashed') : false;
        this.utils          = $('#customer_phone').data('utils') ? $('#customer_phone').data('utils') : false;
        this.restore        = $('#restoreUser');
        this.deletePerm     = $('#deleteUserPerm');
        this.approved       = this.userTable.data('approved');
        this.applied        = this.userTable.data('applied');
        this.invited        = this.userTable.data('invited');
        this.badges         = this.userTable.data('badges');
        this.filters        = this.userTable.data('filters');
    }

    get url() {
        return this.userTable.data('url');
    }

    get config() {
        return this.userTable.data('config');
    }

    get userDelRoute() {
        return this.userTable.data('del');
    }

    get userApprExpertsRoute() {
        return this.userTable.data('approve');
    }

    get userDisapprExpertsRoute() {
        return this.userTable.data('disapprove');
    }

    _initBind () {
        $(document).on('click','#select-all', this.selectAll.bind(this));
        $(document).on('click','#disapprove-selected', this.disapproveSelected.bind(this));
        $(document).on('click','#approve-selected', this.approveSelected.bind(this));
        $(document).on('click','#deselect-all', this.deselectAll.bind(this));
        $(document).on('click','#delete-all-selected', this.deleteAllSelected.bind(this));
        $(document).on('click','.submit_button', this.getNumber.bind(this));
        $(document).on('click',"a[name='delete_user_perm']", this.deleteUserPerm.bind(this));
        $(document).on('click',"a[name='restore_user']", this.restoreUser.bind(this));
        $(document).on('click',"a.approve", this.approveExpert.bind(this));
        $(document).on('click',"a.disapprove", this.disapproveExpert.bind(this));
        $(document).on('click',"a.show-disapprove-reason", this.showDisapproveMessage.bind(this));
        $(document).on('click',"span.change-badge", this.changeBadge.bind(this));
        $(document).on('click',"a.apply", this.applyExpert.bind(this));
        $(document).on('click',"a.disapply", this.disapplyExpert.bind(this));
    }

    _initPlugin () {
        var columns = [
            {data: null, name: null, defaultContent: ''},
            {data: 'id', name: this.config + '.id'},
            {data: 'name', name: this.config + '.name'},
            {data: 'email', name: this.config + '.email'},
            {data: 'countries', name: this.config + '.expert_countries', defaultContent: '—', sortable: false, searchable: false,
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (oData.expert_countries && oData.expert_countries.length) {
                        $(nTd).html('');
                        oData.expert_countries.forEach(function(val, i) {
                            if (i > 0) {
                                $(nTd).append(', ');
                            }
                            $(nTd).append(val.countries.trans[0].title);
                        });
                    }
                }},
            {data: 'cities', name: this.config + '.expert_cities', defaultContent: '—', sortable: false, searchable: false,
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (oData.expert_cities && oData.expert_cities.length) {
                        $(nTd).html('');
                        oData.expert_cities.forEach(function(val, i) {
                            if (i > 0) {
                                $(nTd).append(', ');
                            }
                            $(nTd).append(val.cities.trans[0].title);
                        });
                    }
                }},
            {data: 'places', name: this.config + '.expert_places', defaultContent: '—', sortable: false, searchable: false,
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (oData.expert_places && oData.expert_places.length) {
                        $(nTd).html('');
                        oData.expert_places.forEach(function(val, i) {
                            if (i > 0) {
                                $(nTd).append(', ');
                            }
                            $(nTd).append(val.places.trans[0].title);
                        });
                    }
                }},
            {data: 'expert_travel_styles', name: this.config + '.expert_travel_styles', defaultContent: '—', sortable: false, searchable: false,
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (oData.expert_travel_styles && oData.expert_travel_styles.length) {
                        $(nTd).html('');
                        oData.expert_travel_styles.forEach(function(val, i) {
                            if (!val.travel_styles || !val.travel_styles.trans[0]) {
                                return;
                            }
                            if (i > 0) {
                                $(nTd).append(', ');
                            }
                            $(nTd).append(val.travel_styles.trans[0].title);
                        });
                    }
                }},
            {data: 'interests', name: this.config + '.interests', defaultContent: '—', sortable: false, searchable: false},
            {data: 'website', name: this.config + '.website', defaultContent: '—',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (sData !== '—' && sData !== null) {
                        $(nTd).html('<a target="_blank" href="' + sData + '"></a>');

                        $(nTd).find('a').text($(nTd).find('a').attr('href'));
                    }
                }},
            {data: 'twitter', name: this.config + '.twitter', defaultContent: '—',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (sData !== '—' && sData !== null) {
                        $(nTd).html('<a target="_blank" href="' + sData + '"></a>');

                        $(nTd).find('a').text($(nTd).find('a').attr('href'));
                    }
                }},
            {data: 'facebook', name: this.config + '.facebook', defaultContent: '—',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (sData !== '—' && sData !== null) {
                        $(nTd).html('<a target="_blank" href="' + sData + '"></a>');

                        $(nTd).find('a').text($(nTd).find('a').attr('href'));
                    }
                }},
            {data: 'instagram', name: this.config + '.instagram', defaultContent: '—',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (sData !== '—' && sData !== null) {
                        $(nTd).html('<a target="_blank" href="' + sData + '"></a>');

                        $(nTd).find('a').text($(nTd).find('a').attr('href'));
                    }
                }},
            {data: 'youtube', name: this.config + '.youtube', defaultContent: '—',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (sData !== '—' && sData !== null) {
                        $(nTd).html('<a target="_blank" href="' + sData + '"></a>');

                        $(nTd).find('a').text($(nTd).find('a').attr('href'));
                    }
                }},
            {data: 'pinterest', name: this.config + '.pinterest', defaultContent: '—',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (sData !== '—' && sData !== null) {
                        $(nTd).html('<a target="_blank" href="' + sData + '"></a>');

                        $(nTd).find('a').text($(nTd).find('a').attr('href'));
                    }
                }},
            {data: 'tumblr', name: this.config + '.tumblr', defaultContent: '—',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (sData !== '—' && sData !== null) {
                        $(nTd).html('<a target="_blank" href="' + sData + '"></a>');

                        $(nTd).find('a').text($(nTd).find('a').attr('href'));
                    }
                }},
            // {data: 'points.points', name: this.config + '.points.points', defaultContent: '0', searchable: false},
            // {data: 'invite_expert_link.code', name: this.config + '.invite_expert_link.code', defaultContent: '—', searchable: false,
            //     fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
            //         if (sData !== '—') {
            //             $(nTd).html('<a target="_blank" href="/login?invited=' + sData + '"></a>');
            //
            //             $(nTd).find('a').text($(nTd).find('a').attr('href'));
            //         }
            //     }},
            {data: 'created_at', name: this.config + '.created_at'},
            // {data: 'updated_at', name: this.config + '.updated_at'},
            {data: 'actions', name: 'actions', searchable: false, sortable: false}
        ];

        if (this.approved === 1 || this.approved === 'special') {
            columns.splice( 2, 0,  {data: 'approved_at', name: this.config + '.approved_at', defaultContent: '—'});
            columns.splice( 3, 0,  {data: 'badge.badge.name', name: this.config + '.badge.badge.name', defaultContent: 'None',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).html('<span class="change-badge" data-id="' + oData.id + '" style="border-bottom: 1px dashed #000; cursor: pointer;">' + sData + '</span>');
                }});
            columns.splice( 4, 0,  {data: 'points.points', name: 'points.points', defaultContent: '0', searchable: false});
            columns.splice( 5, 0,  {data: 'get_followers.0.count', name: 'get_followers.count', defaultContent: '0',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (!oData.get_followers.length) {
                        $(nTd).html(0);
                    }
                }});
        }

        if (this.invited === 1) {
            columns.splice( 2, 0,  {data: 'status', name: this.config + '.status', defaultContent: '—',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (sData == 1) {
                        $(nTd).html('Joined');
                    } else {
                        $(nTd).html('Pending');
                    }
                }});
        }

        if (this.applied === 1) {
            columns.splice( 4, 0,  {data: 'next_badge', name: this.config + '.next_badge', defaultContent: '—',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if (oData.next_badge) {
                        var badge = JSON.parse(oData.next_badge);
                        $(nTd).html(badge.name);
                    }
                }});
        }

        this.userTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets: [0]
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: this.status, trashed: this.trashed, approved: this.approved, invited: this.invited, filters: this.filters}
            },
            columns: columns,
            order: [[1, "asc"]],
            searchDelay: 500
        });

        $('.select2Class').select2();

        $("#customer_phone").intlTelInput({
            autoHideDialCode: false,
            formatOnDisplay: true,
            separateDialCode: true,
            utilsScript: this.utils
        });

        $('.datepicker').datepicker($.extend({
                inline: true,
                changeYear: true,
                changeMonth: true,
            },
            $.datepicker.regional['en']
        ));
    }

    selectAll(){
        if(this.userTable.find('tbody tr').length == 1){
            if(this.userTable.find('tbody tr td').hasClass('dataTables_empty')){
                return false;
            }
        }
        this.userTable.find('tbody tr').addClass('selected');
    }

    deselectAll(){
        this.userTable.find('tbody tr').removeClass('selected');
    }

    deleteAllSelected(){
        if(this.userTable.find('tbody tr.selected').length == 0){
            alert('Please select some rows first.');
            return false;
        }

        if(!confirm('Are you sure you want to delete the selected rows?')){
            return false;
        }
        var ids = '';
        var i = 0;
        this.userTable.find('tbody tr.selected').each(function(){
            if(i != 0){
                ids += ',';
            }
            ids += $(this).find('td:nth-child(2)').html();
            i++;
        });

        $.ajax({
            url: this.userDelRoute,
            type: 'post',
            data: {
                ids: ids
            },
            success: function(data){
                var result = JSON.parse(data);
                if(result.result == true){
                    document.location.reload();
                }
            }
        });
    }

    getNumber(e) {
        e.preventDefault();
        var countryData = $('#customer_phone').intlTelInput('getSelectedCountryData');
        var tel = '+'+countryData['dialCode']+' '+$('#customer_phone').val();
        $('#server_phone').val(tel);

        // *************** validate phone and fax before submit
        if($('#customer_phone').intlTelInput('isValidNumber')) {
            $('#User_form').submit();
        }else{
            var text = '';
            if($('#customer_phone').val() == ''){
                text = 'Phone number must not be empty.';
            }else{
                text = 'Phone number should be according to the given format';
            }
            var msg = '<div id="required-alert" class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> ' + text + '.</div>';

            $('.required_msg').html(msg);
            $("#required-alert").fadeTo(5000, 500).slideUp(500, function(){
                $("#required-alert").slideUp(500);
            });
        }
    }

    changeBadge(e){
        e.preventDefault();

        var id = $(e.target).attr('data-id');
        var self = this;

        Swal.fire({
            title: 'Choose a badge',
            text: 'Choose a badge:',
            input: 'select',
            inputOptions: this.badges,
            showCancelButton: true,
            closeOnConfirm: false,
            preConfirm: function(inputValue) {
                $.ajax({
                    type: "POST",
                    url: "/admin/experts/change-badge/" + id,
                    data: {badge: inputValue}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        swal("Changed!", "success");
                        self.userTable.DataTable().ajax.reload();
                    }
                }).fail(function( result ) {
                    swal.showInputError("Error");
                    return false;
                });
            }
        });
    }

    showDisapproveMessage(e) {
        e.preventDefault();

        var elem = e.target.parentElement;
        var text = $(elem).data('text');

        Swal.fire({
            title: 'Disapprove reason',
            text: text,
            showCancelButton: true,
            closeOnConfirm: false,
        });
    }

    approveExpert(e) {
        e.preventDefault();

        var elem = e.target.parentElement;
        var id = $(elem).data('id');

        var self = this;

        Swal.fire({
            title: "Approve",
            text: "Choose a badge:",
            input: "select",
            inputOptions: this.badges,
            closeOnConfirm: false,
            preConfirm: function(inputValue) {
                $.ajax({
                    type: "POST",
                    url: "/admin/experts/approve/" + id,
                    data: {badge: inputValue}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        swal("Approved!", "success");
                        self.userTable.DataTable().ajax.reload();
                    }
                }).fail(function( result ) {
                    swal.showInputError("Error");
                    return false;
                });
            }
        });
    }

    disapproveExpert(e) {
        e.preventDefault();
        var elem = e.target.parentElement;
        var id = $(elem).data('id');

        var self = this;

        swal({
            title: "Disapprove",
            text: "Set disapprove reason:",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Enter disapprove reason",
        }, function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to write something!");
                return false;
            }
            if (inputValue.length > 200) {
                swal.showInputError("You have exceeded 200 characters!");
                return false;
            }

            $.ajax({
                type: "POST",
                url: "/admin/experts/disapprove/" + id,
                data: {message: inputValue}
            }).done(function( result ) {
                if (result.status === 'success') {
                    swal("Disapproved!", "You send: " + inputValue, "success");
                    self.userTable.DataTable().ajax.reload();
                }
            }).fail(function( result ) {
                swal.showInputError("Error");
                return false;
            });
        });
    }

    applyExpert(e) {
        e.preventDefault();

        var elem = e.target.parentElement;
        var id = $(elem).data('id');

        var self = this;

        Swal.fire({
            title: "Apply?",
            closeOnConfirm: false,
            preConfirm: function(inputValue) {
                $.ajax({
                    type: "POST",
                    url: "/admin/experts/apply/" + id,
                    data: {badge: inputValue}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        swal("Applied!", "success");
                        self.userTable.DataTable().ajax.reload();
                    }
                }).fail(function( result ) {
                    swal.showInputError("Error");
                    return false;
                });
            }
        });
    }

    disapplyExpert(e) {
        e.preventDefault();

        var elem = e.target.parentElement;
        var id = $(elem).data('id');

        var self = this;

        Swal.fire({
            title: "Disapply?",
            closeOnConfirm: false,
            preConfirm: function(inputValue) {
                $.ajax({
                    type: "POST",
                    url: "/admin/experts/disapply/" + id,
                    data: {badge: inputValue}
                }).done(function( result ) {
                    if (result.status === 'success') {
                        swal("Disapplied!", "success");
                        self.userTable.DataTable().ajax.reload();
                    }
                }).fail(function( result ) {
                    swal.showInputError("Error");
                    return false;
                });
            }
        });
    }

    deleteUserPerm(e) {
        e.preventDefault();
        var elem = e.target.parentElement;
        var linkURL = elem.getAttribute('href');

        swal({
            title: this.restore.data('title'),
            text: this.restore.data('text'),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: this.restore.data('confirmButtonText'),
            cancelButtonText: this.restore.data('cancelButtonText'),
            closeOnConfirm: false
        }, function(isConfirmed){
            if (isConfirmed){
                window.location.href = linkURL;
            }
        });
    };

    restoreUser(e) {
        e.preventDefault();
        var elem = e.target.parentElement;
        var linkURL = elem.getAttribute('href');

        console.log(elem)
        console.log(linkURL)
        swal({
            title: this.deletePerm.data('title'),
            text: this.deletePerm.data('text'),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: this.deletePerm.data('confirmButtonText'),
            cancelButtonText: this.deletePerm.data('cancelButtonText'),
            closeOnConfirm: false
        }, function(isConfirmed){
            if (isConfirmed){
                window.location.href = linkURL;
            }
        });
    };

    approveSelected(e) {
        if(this.userTable.find('tbody tr.selected').length == 0){
            alert('Please select some rows first.');
            return false;
        }
        var ids = '';
        var i = 0;
        this.userTable.find('tbody tr.selected').each(function(){
            if(i != 0){
                ids += ',';
            }
            ids += $(this).find('td:nth-child(2)').html();
            i++;
        });

        var self = this;

        swal({
            title: "Approve Selected Experts",
            text: "Add points to expert:",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Enter a number of points",
        }, function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to write something!");
                return false;
            }
            if (isNaN(inputValue)) {
                swal.showInputError('You need to write numeric value!');
                return false;
            }

            $.ajax({
                type: "POST",
                url: self.userApprExpertsRoute,
                data: {
                    ids: ids,
                    points: inputValue
                },
            }).done(function( result ) {
                if (result.status === 'success') {
                    swal("Approved!", "You add: " + inputValue + " Points", "success");
                    self.userTable.DataTable().ajax.reload();
                }
            }).fail(function( result ) {
                swal.showInputError("Error");
                return false;
            });
        });
    };

    disapproveSelected(e) {
        if(this.userTable.find('tbody tr.selected').length == 0){
            alert('Please select some rows first.');
            return false;
        }

        var ids = '';
        var i = 0;
        this.userTable.find('tbody tr.selected').each(function(){
            if(i != 0){
                ids += ',';
            }
            ids += $(this).find('td:nth-child(2)').html();
            i++;
        });
        var self = this;

        swal({
            title: "Disapprove Selected Experts",
            text: "Send message:",
            type: "input",
            inputAttributes: {
                maxlength: 200
            },
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Enter message to user",
        }, function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to write something!");
                return false;
            }
            if (inputValue.length > 200) {
                swal.showInputError("You have exceeded 200 characters!");
                return false;
            }

            $.ajax({
                type: "POST",
                url: self.userDisapprExpertsRoute,
                data: {
                    ids: ids,
                    message: inputValue
                },
            }).done(function( result ) {
                if (result.status === 'success') {
                    swal("Disapproved!", "You send: " + inputValue, "success");
                    self.userTable.DataTable().ajax.reload();
                }
            }).fail(function( result ) {
                swal.showInputError("Error");
                return false;
            });
        });
    };
}
