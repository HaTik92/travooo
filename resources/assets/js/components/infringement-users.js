import BaseComponent from "../core/baseComponent";

export default class CopyrightInfringementUsersComponent extends BaseComponent {


    _initProperty() {
        this.table;
        this.tableEl = $('#infringementsSuspendedAccounts');
    }

    _initBind () {
        let self = this;
        $(document).on('click','.unblock_user', self.unBlockUser.bind(self));
        $(document).on('click','.block_user', self.blockUser.bind(self));
    }

    get url() {
        return this.tableEl.data('url');
    }

    _initPlugin() {
        this.table = this.tableEl.DataTable({
            lengthMenu: [10, 25, 50, 100, 1000, 10000],
            deferRender: true,
            columnDefs: [
                {
                    orderable: true,
                },
            ],
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: function (request) {
                }
            },
            columns: [
                {data: 'id', name: 'id', visible: true},
                {data: 'name', name: 'name', visible: true},
                {data: 'email', name: 'email', visible: true},

                // {
                //     data: null,
                //     name: null,
                //     visible: true,
                //     sortable: false,
                //     searchable: false,
                //     render: function(data) {
                //         var copyrightInfringement = data.copyright_infringement;
                //         var sourceLink = '-';
                //         var lastLink = '-';
                //         var text = '';
                //         if (Object.keys(copyrightInfringement).length) {
                //             copyrightInfringement.forEach(function (item, key) {
                //                 if (item.reported_url) {
                //                     var link = item.reported_url;
                //                     if (link) {
                //                         text += '<tr>' +
                //                             '<td>Link#' + (key + 1) + '</td>' +
                //                             '<td><a href="' + link + '" target="_blank">' + link + '</a></td>' +
                //                             '</tr>';
                //                         lastLink = link.length < 25 ? link : link.substr(1, 25) + '...';
                //                     }
                //                 }
                //             });
                //             sourceLink = '<div class="dropdown dropright">' +
                //                 '<a type="button" data-toggle="dropdown" aria-haspopup="true"  aria-expanded="false">' +
                //                 lastLink +
                //                 '</a>' +
                //                 '<div class="dropdown-menu" posttype="Source Link">' +
                //                 '<table>' +
                //                 '<tr>' +
                //                 '<th>N#</th>' +
                //                 '<th>Link</th>' +
                //                 '</tr>' +
                //                 text +
                //                 '</table>' +
                //                 '</div>' +
                //                 '</div>';
                //             return sourceLink;
                //         }
                //     }
                // },
                {
                    data: null,
                    searchable: false,
                    sortable: false,
                    visible: true,
                    render: function (data) {
                        if (Object.keys(data.user_blocks).length) {
                            return '' +
                                '<button data-id="' + data.id + '" class="btn btn-xs btn-success unblock_user" data-toggle="tooltip" data-placement="top" title="Unlock User"><i class="fa fa-unlock-alt" style="pointer-events: none;"></i> </button>&nbsp;' +
                                '';
                        } else {
                            return '' +
                                '<button type="button" data-id="' + data.id + '" class="btn btn-xs btn-warning  mx-1 block_user" title="Block User">' +
                                ' <i style="pointer-events: none;" class="fa fa-lock"></i>' +
                                '</button>&nbsp' +
                                '';
                        }
                    }
                },
            ],
            order: [[0, "desc"]],
            searchDelay: 500,
        });

    }

    unBlockUser(e) {
        var id = parseInt($(e.target).data('id'));
        $.ajax({
            url: '/admin/access/user/unblock',
            type: 'post',
            data: {
                id: id
            },
            success: function(data){
                var result = data.data;
                if(result.success === true){
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>User successfully blocked</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
                        $("#deleted-alert").slideUp(500);
                    });
                }
                window.location.href="/admin/copyright-infringement/users";
            }
        });
    }

    blockUser(e) {
        var id = parseInt($(e.target).data('id'));
        $.ajax({
            url: '/admin/access/user/block',
            type: 'post',
            data: {
                id: id
            },
            success: function(data){
                var result = data.data;
                if(result.success === true){
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>User successfully blocked</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
                        $("#deleted-alert").slideUp(500);
                    });
                }
                window.location.href="/admin/copyright-infringement/users";
            }
        });
    }
}