import Component from '../core/component';
import BaseComponent from "../core/baseComponent";

export default class ActivityTypesComponent extends BaseComponent {
    _initProperty () {
        this.activityTypesTable = $('#activity-types-table');
    }

    get url() {
        return this.activityTypesTable.data('url');
    }

    get config() {
        return this.activityTypesTable.data('config');
    }

    _initBind () {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin () {
        super._initPlugin();
        this.activityTypesTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }
}