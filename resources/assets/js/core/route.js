import $ from 'jquery';

export class Router {
    match (url) {
        switch (true) {

            // Dashboard
            case 'admin/dashboard' === url:
                require(['../components/dashboard.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Access Management
            case 'admin/access/user' === url:
            case 'admin/access/user/create' === url:
            case 'admin/access/user/deactivated' === url:
            case 'admin/access/user/suspended' === url:
            case 'admin/access/user/non-confirmed' === url:
            case 'admin/access/user/deleted' === url:
            case 'admin/access/user/logs' === url:
            case 'admin/access/user/block' === url:
            case /admin\/access\/user\/\d+\/edit/.test(url):
            case 'admin/experts/create' === url:
                require(['../components/user.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Experts Management
            case 'admin/experts' === url:
            case 'admin/experts/not-approved' === url:
            case 'admin/experts/approved' === url:
            case 'admin/experts/pending-approval' === url:
            case 'admin/experts/approve-experts' === url:
            case 'admin/experts/special-approved' === url:
            case 'admin/experts/disapprove-experts' === url:
            case 'admin/experts/invited' === url:
            case 'admin/experts/applied' === url:
                require(['../components/experts.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Invite Links Management
            case 'admin/badges-invitations/invite-links' === url:
                require(['../components/invite-links.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Badges
            case 'admin/badges-invitations/badges' === url:
                require(['../components/badges.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Accommodations Management
            case 'admin/accommodations' === url:
            case 'admin/accommodations/create' === url:
            case /admin\/accommodations\/\d+\/edit/.test(url):
                require(['../components/accommodations.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Access Management -> Role Management
            case 'admin/access/role' === url:
            case 'admin/access/role/create' === url:
            case /admin\/access\/role\/\d+\/edit/.test(url):
                require(['../components/userRoles.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // AgeRanges Management
            case 'admin/ageranges' === url:
            case 'admin/ageranges/create' === url:
            case /admin\/ageranges\/\d+\/edit/.test(url):
                require(['../components/ageranges.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Activities Management
            case 'admin/activities/activity' === url:
            case 'admin/activities/activity/create' === url:
            case /admin\/activities\/activity\/\d+/.test(url):
            case /admin\/activities\/activity\/\d+\/edit/.test(url):
                require(['../components/activity.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Activity Types Management
            case 'admin/activities/activitytypes' === url:
            case 'admin/activities/activitytypes/create' === url:
            case /admin\/activities\/activitytypes\/\d+\/edit/.test(url):
                require(['../components/activitytypes.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Activity Media Management
            case 'admin/activitymedia' === url:
            case 'admin/activitymedia/create' === url:
            case /admin\/activitymedia\/\d+\/edit/.test(url):
                require(['../components/activitymedia.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Languages Manager
            case 'admin/access/languages' === url:
            case 'admin/access/languages/create' === url:
                require(['../components/languages.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Locations Manager -> Regions
            case 'admin/location/regions' === url:
            case 'admin/location/regions/create' === url:
            case /admin\/location\/regions\/\d+\/edit/.test(url):
                require(['../components/regions.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Locations Manager -> Countries
            case 'admin/location/country' === url:
            case 'admin/location/country/create' === url:
            case /admin\/location\/country\/\d+/.test(url):
            case /admin\/location\/country\/\d+\/edit/.test(url):
                require(['../components/countries.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Locations Manager -> Cities
            case 'admin/location/city' === url:
            case 'admin/location/city/create' === url:
            case /admin\/location\/city\/\d+/.test(url):
            case /admin\/location\/city\/\d+\/edit/.test(url):
                require(['../components/cities.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Locations Manager -> Places & Activities
            case 'admin/location/place' === url:
            case 'admin/location/place/create' === url:
            case /admin\/location\/place\/\d+/.test(url):
            case /admin\/location\/place\/\d+\/edit/.test(url):
            case 'admin/location/place/import' === url:
            case 'admin/location/place/search' === url:
                require(['../components/places.js'], (module) => {
                    this._initComponent(module);
                });
                break;
                 // Locations Manager -> Isolated places
            case 'admin/location/isolated' === url:
            case 'admin/location/isolated/create' === url:
            case /admin\/location\/isolated\/\d+/.test(url):
            case /admin\/location\/isolated\/\d+\/edit/.test(url):
            case 'admin/location/isolated/import' === url:
            case 'admin/location/isolated/search' === url:
                require(['../components/isolated.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            case 'admin/location/place/mass' === url:
                require(['../components/placesMass.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Top Places -> Places
            case 'admin/top-places/places' === url:
            case 'admin/top-places/places/create' === url:
            case /admin\/top-places\/places\/\d+/.test(url):
            case /admin\/top-places\/places\/\d+\/edit/.test(url):
            case 'admin/top-places/places/import' === url:
            case 'admin/top-places/places/search' === url:
                require(['../components/top_places.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Top Places -> Hotels
            case 'admin/top-places/hotels' === url:
            case 'admin/top-places/hotels/create' === url:
            case /admin\/top-places\/hotels\/\d+/.test(url):
            case /admin\/top-places\/hotels\/\d+\/edit/.test(url):
            case 'admin/top-places/hotels/import' === url:
            case 'admin/top-places/hotels/search' === url:
                require(['../components/top_hotels.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Top Places -> Restaurants
            case 'admin/top-places/restaurants' === url:
            case 'admin/top-places/restaurants/create' === url:
            case /admin\/top-places\/restaurants\/\d+/.test(url):
            case /admin\/top-places\/restaurants\/\d+\/edit/.test(url):
            case 'admin/top-places/restaurants/import' === url:
            case 'admin/top-places/restaurants/search' === url:
                require(['../components/top_restaurants.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Locations Manager -> Places & Activities
            case 'admin/location/placetypes' === url:
            case 'admin/location/placetypes/create' === url:
            case /admin\/location\/placetypes\/\d+/.test(url):
            case /admin\/location\/placetypes\/\d+\/edit/.test(url):
                require(['../components/placetypes.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Locations Manager -> Unapproved items
            case 'admin/location/unapproved-items' === url:
            case 'admin/location/unapproved-items/create' === url:
            case /admin\/location\/unapproved-items\/\d+/.test(url):
            case /admin\/location\/unapproved-items\/\d+\/edit/.test(url):
            case 'admin/location/unapproved-items/import' === url:
            case 'admin/location/unapproved-items/mass-approve' === url:
            case 'admin/location/unapproved-items/search' === url:
                require(['../components/unapproved-item.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Locations Manager -> Spam reports
            case 'admin/location/spam-reports' === url:
            // case 'admin/location/unapproved-items/create' === url:
            // case /admin\/location\/unapproved-items\/\d+/.test(url):
            // case /admin\/location\/unapproved-items\/\d+\/edit/.test(url):
            // case 'admin/location/unapproved-items/import' === url:
            // case 'admin/location/unapproved-items/search' === url:
                require(['../components/spam-reports.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Spam Manager
            case 'admin/spam_manager' === url:
            case 'admin/spam_manager/history-table' === url:
            case 'admin/spam_manager/history' === url:
            case 'admin/spam_manager/api-reports' === url:
            case 'admin/spam_manager/api-reports-table' === url:
                require(['../components/spam-manager.js'], (module) => {
                    this._initComponent(module);
                });
                break;


            // Destinations Manager -> Countries by Nationality
            case 'admin/destinations/countries-by-nationality' === url:
            case 'admin/destinations/countries-by-nationality/create' === url:
            case /admin\/destinations\/countries-by-nationality\/\d+/.test(url):
            case /admin\/destinations\/countries-by-nationality\/\d+\/edit/.test(url):
                require(['../components/destinations.js'], (module) => {
                    this._initComponent(module);
                });
                break;


            // Destinations Manager -> Cities by Nationality
            case 'admin/destinations/cities-by-nationality' === url:
            case 'admin/destinations/cities-by-nationality/create' === url:
            case /admin\/destinations\/cities-by-nationality\/\d+/.test(url):
            case /admin\/destinations\/cities-by-nationality\/\d+\/edit/.test(url):
                require(['../components/destinations.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Destinations Manager -> Generic Top Countries
            case 'admin/destinations/generic-top-countries' === url:
            case 'admin/destinations/generic-top-countries/create' === url:
            case /admin\/destinations\/generic-top-countries\/\d+/.test(url):
            case /admin\/destinations\/generic-top-countries\/\d+\/edit/.test(url):
                require(['../components/destinations.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Destinations Manager -> Generic Top Cities
            case 'admin/destinations/generic-top-cities' === url:
            case 'admin/destinations/generic-top-cities/create' === url:
            case /admin\/destinations\/generic-top-cities\/\d+/.test(url):
            case /admin\/destinations\/generic-top-cities\/\d+\/edit/.test(url):
                require(['../components/destinations.js'], (module) => {
                    this._initComponent(module);
                });
                break;


            // Destinations Manager -> Generic Top Places
            case 'admin/destinations/generic-top-places' === url:
            case 'admin/destinations/generic-top-places/create' === url:
            case /admin\/destinations\/generic-top-places\/\d+/.test(url):
            case /admin\/destinations\/generic-top-places\/\d+\/edit/.test(url):
                require(['../components/destinations.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Embassies Manager -> Embassies
            case 'admin/embassies' === url:
            case 'admin/embassies/create' === url:
            case /admin\/embassies\/\d+/.test(url):
            case /admin\/embassies\/\d+\/edit/.test(url):
            case 'admin/embassies/import' === url:
            case 'admin/embassies/search' === url:
                require(['../components/embassies.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Events Manager
            case 'admin/events' === url:
            case 'admin/events/api-table' === url:
            case 'admin/events/api-index' === url:
            case 'admin/event' === url:
            case /admin\/events\/\d+/.test(url):
            case /admin\/events\/\d+\/edit/.test(url):
            case 'admin/events/create' === url:
                require(['../components/events.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Hobbies Manager -> Hobbies
            case 'admin/hobbies' === url:
            case 'admin/hobbies/create' === url:
            case /admin\/hobbies\/\d+/.test(url):
            case /admin\/hobbies\/\d+\/edit/.test(url):
                require(['../components/hobbies.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Hotels Manager -> Hotels
            case 'admin/hotels' === url:
            case 'admin/hotels/create' === url:
            case /admin\/hotels\/\d+/.test(url):
            case /admin\/hotels\/\d+\/edit/.test(url):
            case 'admin/hotels/import' === url:
            case 'admin/hotels/search' === url:
                require(['../components/hotels.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Interest Manager -> Interest
            case 'admin/interest' === url:
            case 'admin/interest/create' === url:
            case /admin\/interest\/\d+/.test(url):
            case /admin\/interest\/\d+\/edit/.test(url):
                require(['../components/interest.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // SafetyDegrees Manager -> SafetyDegrees
            case 'admin/safety-degrees/safety' === url:
            case 'admin/safety-degrees/safety/create' === url:
            case /admin\/safety-degrees\/safety\/\d+/.test(url):
            case /admin\/safety-degrees\/safety\/\d+\/edit/.test(url):
                require(['../components/safety-degrees.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Levels Manager -> Levels
            case 'admin/levels' === url:
            case 'admin/levels/create' === url:
            case /admin\/levels\/\d+/.test(url):
            case /admin\/levels\/\d+\/edit/.test(url):
                require(['../components/levels.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Restaurants Manager -> Restaurants
            case 'admin/restaurants' === url:
            case 'admin/restaurants/create' === url:
            case /admin\/restaurants\/\d+/.test(url):
            case /admin\/restaurants\/\d+\/edit/.test(url):
            case 'admin/restaurants/import' === url:
            case 'admin/restaurants/search' === url:
                require(['../components/restaurants.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Religions Manager -> Religions
            case 'admin/religion' === url:
            case 'admin/religion/create' === url:
            case /admin\/religion\/\d+\/edit/.test(url):
                require(['../components/religions.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Travel Styles -> Travel Styles
            case 'admin/lifestyle' === url:
            case 'admin/lifestyle/create' === url:
            case /admin\/lifestyle\/\d+\/edit/.test(url):
                require(['../components/lifestyles.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Languages Spoken Manager -> Languages Spoken
            case 'admin/languagesspoken' === url:
            case 'admin/languagesspoken/create' === url:
            case /admin\/languagesspoken\/\d+\/edit/.test(url):
                require(['../components/languagesSpoken.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Weekdays Manager -> Weekdays
            case 'admin/weekdays' === url:
            case 'admin/weekdays/create' === url:
            case /admin\/weekdays\/\d+\/edit/.test(url):
                require(['../components/weekdays.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Holidays Manager Manager -> Holidays
            case 'admin/holidays' === url:
            case 'admin/holidays/create' === url:
            case /admin\/holidays\/\d+\/edit/.test(url):
                require(['../components/holidays.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Emergency Numbers Manager -> Emergency Numbers
            case 'admin/emergencynumbers' === url:
            case 'admin/emergencynumbers/create' === url:
            case /admin\/emergencynumbers\/\d+\/edit/.test(url):
                require(['../components/emergencynumbers.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Currencies Manager -> Currencies
            case 'admin/currencies' === url:
            case 'admin/currencies/create' === url:
            case /admin\/currencies\/\d+\/edit/.test(url):
                require(['../components/currencies.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Timings Manager -> Timings
            case 'admin/timings' === url:
            case 'admin/timings/create' === url:
            case /admin\/timings\/\d+\/edit/.test(url):
                require(['../components/timings.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Cultures Manager -> Cultures
            case 'admin/cultures' === url:
            case 'admin/cultures/create' === url:
            case /admin\/cultures\/\d+\/edit/.test(url):
                require(['../components/cultures.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            //Pages Manager -> Pages
            case 'admin/pages' === url:
            case 'admin/pages/create' === url:
            case /admin\/pages\/\d+\/edit/.test(url):
            case 'admin/pages_categories' === url:
            case 'admin/pages_categories/create' === url:
            case /admin\/pages_categories\/\d+\/edit/.test(url):
                require(['../components/pages.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            case /admin\/copyright-infringement\/\d+/.test(url):
            case 'admin/copyright-infringement' === url:
            case 'admin/copyright-infringement/view' === url:
            case 'admin/copyright-infringement/reply' === url:
            case 'admin/copyright-infringement/delete' === url:
            case 'admin/copyright-infringement/confirm' === url:
                require(['../components/copyright_infringement.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            case 'admin/copyright-infringement/users' === url:
            case 'admin/copyright-infringement/users-table' === url:
                require(['../components/infringement-users.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            case 'admin/copyright-infringement/history' === url:
            case 'admin/copyright-infringement/history-table' === url:
            case 'admin/copyright-infringement/restore-post' === url:
                require(['../components/infringement-history.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            case 'admin/submissions' === url:
                require(['../components/submissions.js'], (module) => {
                    this._initComponent(module);
                });
                break;
            case 'admin/submissions/approved' === url:
                require(['../components/submissions-approved.js'], (module) => {
                    this._initComponent(module);
                });
                break;
            case 'admin/submissions/disapproved' === url:
                require(['../components/submissions-disapproved.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            case 'admin/help-center/menues' === url:
                require(['../components/help-center-menues.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            case 'admin/help-center/sub-menues' === url:
                require(['../components/help-center-submenues.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            case 'admin/log-viewer/rank-cron' === url:
                require(['../components/rank-cron.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            case 'admin/posts/view' === url:
                require(['../components/posts.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            default:
                break;
        }
    }

    /**
     *
     * @param componentClass
     * @private
     */
    _initComponent (module) {
        $(document).ready(() => {
            let component = new module.default();
            component.init();
        });
    }
}
