import Component from '../core/component';

export default class BaseComponent extends Component {
    submitValidation () {
        var msg = '<div id="language-alert" class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Please Fill Information In All Languages Tabs.</div>';

        $('.required').each(function(index, data){
            var flag = false;

            if($(this).val() == ''){
                flag = true;
            }

            if(flag){
                $('.required_msg').html(msg);
                $("#language-alert").fadeTo(5000, 500).slideUp(500, function(){
                    $("#language-alert").slideUp(500);
                });
            }
        });
    }
}