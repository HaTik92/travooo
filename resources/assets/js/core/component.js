export default class Component {
    init () {
        this._initProperty();
        this._initBind();
        this._initPlugin();
    }

    _initProperty () {

    }

    _initBind () {

    }

    _initPlugin () {
        $('[data-toggle="tooltip"]').tooltip();
        $('.select2Class').select2();
        $('.description').summernote({
            height: 200
        });
    }
}