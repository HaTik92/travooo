import Sortable, { AutoScroll } from 'sortablejs/modular/sortable.core.esm.js';

Sortable.mount(new AutoScroll());

window.reloadSortable = function reloadSortable() {
    let sortable = new Sortable($('#sortable').get(0), {
        ghostClass: "ui-state-highlight",
        dragClass: "row-drag",
        onEnd: function (evt) {
            var sortedIDs = sortable.toArray();
            $("#createReportStep2").submit();
            $('#step2_sort').val(sortedIDs);
        },
        onStart: function (evt) {
        },
        //handle: ".handle",
        animation: 50,
        dataIdAttr: 'id',
    });

    let sortedIDs = sortable.toArray();
    $('#step2_sort').val(sortedIDs);
}

reloadSortable();
