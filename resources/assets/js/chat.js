import Echo from 'laravel-echo'

window.io = require('socket.io-client');

if (typeof io !== 'undefined') {
    window.Echo = new Echo({
        broadcaster: 'socket.io',
        host: window.location.hostname
    });
}

let url = window.location.href;
var arr = url.split("/");
let host = arr[0] + "//" + arr[2];

//If we are on chat related page
if (url.indexOf(host + "/chat") >= 0) {
    //Used to receive new messages
    window.Echo.private('chat-channel.' + user_id)
        .listen('ChatSendPrivateMessageEvent', (e) => {

            console.log('chat-channel.{user_id} => ', 'ChatSendPrivateMessageEvent => ', e);

            let sender_id = 0;

            sender_id = e.data.from_id;

            //Append message to container if it belongs to selected container
            let selectedContainerId = document.getElementById('chat_conversation_id').value;

            if (parseInt(selectedContainerId) === parseInt(e.data.chat_conversation_id)) {
                appendMessage(e.data, true);
            }
            else {
                if (e.select_conversation_id !== null) {

                    if (parseInt(sender_id) === parseInt(user_id)) {
                        setChatConversationId('conversation-container-' + e.select_conversation_id);
                    }

                }
            }

            //Update conversation title
            let convContainer = document.getElementById('conversation-title-' + e.data.chat_conversation_id);
            convContainer.innerHTML = e.data.conv_title;

            //Increase unread count
            if (parseInt(sender_id) !== parseInt(user_id)) {

                //Mark conversation as unread
                convContainer.style.fontWeight = "bold";

                incrementConversationUnread(e.data.chat_conversation_id);

                let countContainerSmall = document.getElementById("chat-unread-counter-small");
                let unreadCount = parseInt(countContainerSmall.innerHTML);
                unreadCount++;

                if (unreadCount < 0) {
                    unreadCount = 0;
                }

                countContainerSmall.innerHTML = unreadCount;
            }

            //Move conv to top
            moveConversationToTop(e.data.chat_conversation_id);

        });

    //Triggered when new conversation is added
    window.Echo.private('chat-channel.' + user_id)
        .listen('ChatNewConversationEvent', (e) => {
            console.log('chat-channel.{user_id} => ', 'ChatNewConversationEvent => ', e);
            appendNewConversation(e.data);

        });

}
else {
    window.Echo.private('chat-channel.' + user_id)
        .listen('ChatSendPrivateMessageEvent', (e) => {

            console.log('chat-channel.{user_id} => ', 'ChatSendPrivateMessageEvent => ', e);

            let sender_id = 0;

            sender_id = e.data.from_id;

            //Increase unread count
            if (parseInt(sender_id) !== parseInt(user_id)) {
                let countContainer = document.getElementById("chat-unread-counter");

                let unreadConv = 0;

                unreadConv = e.data.unread_chat_conversations_count;

                countContainer.innerHTML = unreadConv;

                //if (unreadConv > 0) {
                //    countContainer.style.display = "block";
                //}
                //else {
                //    countContainer.style.display = "none";
                //}
            }
        });
}
