<?php

return [

    /*
    |--------------------------------------------------------------------------
    | HTTP Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the views/errors files.
    |
    */

    '404' => [
        'title'       => "Página no encontrada",
        'description' => "Lo sentimos, pero la página a la que intenta acceder no existe.",
    ],

    '503' => [
        'title'       => "Volvemos en un momento.",
        'description' => "Volvemos en un momento.",
    ],
];
