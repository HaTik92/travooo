<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'        => "Estas credenciales no coinciden con nuestros registros.",
    'general_error' => "No tiene acceso para hacer eso.",
    'socialite'     => [
        'unacceptable' => ":provider no es un inicio de sesión aceptable.",
    ],
    'throttle' => "Demasiados intentos de inicio de sesión. Por favor, inténtelo de nuevo en :seconds segundos.",
    'unknown'  => "Se ha producido un error inesperado",
];
