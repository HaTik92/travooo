<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => "Las contraseñas deben tener seis caracteres como mínimo y coincidir con la validación.",
    'reset'    => "¡Se ha restablecido su contraseña!",
    'sent'     => "¡Hemos enviado un correo electrónico con el enlace para restablecer su contraseña!",
    'token'    => "Este autentificado para restaurar la contraseña no es válido.",
    'user'     => "La dirección de correo electrónico no corresponde a ningún usuario.",
    'required' => "The username or email address is required.",
    'invalid'  => "Invalid username or email address.",
];
