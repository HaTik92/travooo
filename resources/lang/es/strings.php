<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Strings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in strings throughout the system.
    | Regardless where it is placed, a string can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'delete_user_confirm'  => "¿Está seguro de que desea eliminar este usuario permanentemente? Es probable que se produzca un error en los lugares de la aplicación que hagan referencia al identificador de este usuario. Actúe bajo su propia responsabilidad. Esta acción no se podrá deshacer.",
                'if_confirmed_off'     => "(Se apagará al confirmar)",
                'restore_user_confirm' => "¿Restaurar este usuario a su estado original?",
            ],
        ],

        'dashboard' => [
            'title'   => "Panel de administración",
            'welcome' => "Bienvenido",
        ],

        'general' => [
            'all_rights_reserved' => "Todos los derechos están reservados.",
            'are_you_sure'        => "¿Está seguro de que desea hacer esto?",
            'boilerplate_link'    => "Laravel 5 Boilerplate",
            'continue'            => "Continuar",
            'member_since'        => "Miembro desde",
            'minutes'             => " minutos",
            'search_placeholder'  => "Buscar...",
            'timeout'             => "Su sesión se desconectó automáticamente porque no hubo actividad en ",

            'see_all' => [
                'messages'      => "Ver todos los mensajes",
                'notifications' => "Ver todo",
                'tasks'         => "Ver todas las tareas",
            ],

            'status' => [
                'online'  => "En línea",
                'offline' => "Desconectado",
            ],

            'you_have' => [
                'messages'      => "{0} No tiene mensajes|{1} Tiene 1 mensaje|[2,Inf] Tiene :number mensajes",
                'notifications' => "{0} No tiene notificaciones|{1} Tiene 1 notificación|[2,Inf] Tiene :number notificaciones",
                'tasks'         => "{0} No tiene tareas pendientes|{1} Tiene 1 tarea|[2,Inf] Tiene :number tareas",
            ],
        ],

        'search' => [
            'empty'      => "Introduzca un criterio de búsqueda.",
            'incomplete' => "Para este sistema usted debe escribir su propio parámetro de búsqueda.",
            'title'      => "Resultados de la búsqueda",
            'results'    => "Resultados de la búsqueda para :query",
        ],

        'welcome' => "<p>Este es el tema AdminLTE de <a href=\"https://almsaeedstudio.com/\" target=\"_blank\">https://almsaeedstudio.com/</a>. Esta es la versión básica, contiene únicamente los estilos y comandos imprescindibles para funcionar. Descargue la versión completa para empezar a añadir componentes a su panel.</p>\n<p>Se muestran todas las funciones a excepción de <strong>Gestión de acceso</strong> a la izquierda. Este boilerplate contiene una biblioteca de control de acceso totalmente funcional para administrar usuarios/roles/permisos.</p>\n<p>Tenga en cuenta que esta aplicación está en desarrollo, puede haber errores y otros problemas no identificados. Haré todo lo posible para arreglarlos a medida que los reciba.</p>\n<p>Espero que disfrute de todo el esfuerzo que le he dedicado. Por favor, visite la página <a href=\"https://github.com/rappasoft/laravel-5-boilerplate\" target=\"_blank\">GitHub</a> para más información e informar de cualquier problema, aquí <a href=\"https://github.com/rappasoft/Laravel-5-Boilerplate/issues\" target=\"_blank\"> </a>.</p>\n<p><strong>Cuesta mucho mantener este proyecto debido a la velocidad a la que cambia la rama principal de Laravel, así que se agradece cualquier ayuda.</strong></p>\n<p>- Anthony Rappa</p>",
    ],

    'emails' => [
        'auth' => [
            'error'                   => "¡Huy!",
            'greeting'                => "¡Hola!",
            'regards'                 => "Saludos,",
            'trouble_clicking_button' => "Si experimenta algún problema pulse el botón \":action_text\", copie y peque la siguiente URL en su navegador:",
            'thank_you_for_using_app' => "¡Gracias por usar nuestra aplicación!",

            'password_reset_subject'    => "Restablecer contraseña",
            'password_cause_of_email'   => "Usted ha recibido este mensaje porque nos ha llegado una solicitud para restablecer la contraseña de su cuenta.",
            'password_if_not_requested' => "Si usted no ha solicitado restablecer la contraseña no es necesario que haga nada más.",
            'reset_password'            => "Pulse aquí para restablecer su contraseña",

            'click_to_confirm' => "Pulse aquí para confirmar su cuenta:",
        ],
    ],

    'frontend' => [
        'test' => "Prueba",

        'tests' => [
            'based_on' => [
                'permission' => "Sobre permisos - ",
                'role'       => "Sobre roles - ",
            ],

            'js_injected_from_controller' => "Javascript impulsado desde un controlador",

            'using_blade_extensions' => "Usar extensiones de corte",

            'using_access_helper' => [
                'array_permissions'     => "Usar el Asistente de acceso con un conjunto de Nombres de permiso o identificadores de los que el usuario tiene plena posesión.",
                'array_permissions_not' => "Usar el Asistente de acceso con un conjunto de Nombres de permiso o identificadores de los que el usuario no tiene plena posesión.",
                'array_roles'           => "Usar el Asistente de acceso con un conjunto de Nombres de rol o identificadores de los que el usuario tiene plena posesión.",
                'array_roles_not'       => "Usar el Asistente de acceso con un conjunto de Nombres de rol o identificadores de los que el usuario no tiene plena posesión.",
                'permission_id'         => "Usar el Asistente de acceso con identificador de permiso",
                'permission_name'       => "Usar el Asistente de acceso con Nombre de permiso",
                'role_id'               => "Usar el Asistente de acceso con identificador de rol",
                'role_name'             => "Usar el Asistente de acceso con Nombre de rol",
            ],

            'view_console_it_works'          => "Vista de consola, usted debería ver «¡Funciona!» procedente de FrontendController@index",
            'you_can_see_because'            => "¡Puede ver esto porque tiene el rol «:role»!",
            'you_can_see_because_permission' => "¡Puede ver esto porque tiene el permiso «:permission»!",
        ],

        'user' => [
            'change_email_notice'  => "Si quiere cambiar de e-mail, permanecerá desconectado hasta que se confirme su nueva dirección electrónica.",
            'email_changed_notice' => "Usted deberá confirmar su nueva dirección electrónica antes de volver a iniciar sesión.",
            'profile_updated'      => "El perfil se ha actualizado con éxito.",
            'password_updated'     => "La contraseña se ha actualizado con éxito.",
        ],

        'welcome_to' => "Bienvenido a :place",

        'copy' => "Travooo &copy; 2017",

        'share_this_page' => "Compartir esta página",

        'loading' => "Cargando",

    ],
];
