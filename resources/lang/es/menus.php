<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => "Gestión de acceso",

            'roles' => [
                'all'        => "Todos los roles",
                'create'     => "Crear rol",
                'edit'       => "Editar rol",
                'management' => "Gestión de roles",
                'main'       => "Roles",
            ],

            'users' => [
                'all'             => "Todos los usuarios",
                'change-password' => "Cambiar contraseña",
                'create'          => "Crear usuario",
                'deactivated'     => "Desactivar usuarios",
                'deleted'         => "Eliminar usuarios",
                'edit'            => "Editar usuario",
                'main'            => "Usuarios",
                'view'            => "Ver usuario",
            ],
        ],
        'langauge' => [
            'title' => "Gestión de idioma",
        ],
    
        'countries' => [
            'title' => "Gestión de países",            
        ],

        'log-viewer' => [
            'main'      => "Visualizar registros",
            'dashboard' => "Panel de control",
            'logs'      => "Registros",
        ],

        'sidebar' => [
            'dashboard' => "Panel de control",
            'general'   => "General",
            'system'    => "Sistema",
        ],
    ],

    'language-picker' => [
        'language' => "Idioma",
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'ar'    => "Árabe",
            'zh'    => "Chino simplificado",
            'zh-TW' => "Chino tradicional",
            'da'    => "Danés",
            'de'    => "Alemán",
            'el'    => "Griego",
            'en'    => "Inglés",
            'es'    => "Español",
            'fr'    => "Francés",
            'id'    => "Indonesio",
            'it'    => "Italiano",
            'ja'    => "Japonés",
            'nl'    => "Holandés",
            'pt_BR' => "Brasileño",
            'ru'    => "Ruso",
            'sv'    => "Sueco",
            'th'    => "Tailandés",
        ],
    ],
];
