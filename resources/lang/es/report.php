<?php

return [
    'central_part'                                         => "Parte central",
    'trip_plans'                                           => "Planificación del viaje",
    'travel_mates'                                         => "Acompañantes de viaje",
    'most_popular'                                         => "Lo más popular",
    'newest'                                               => "Lo más nuevo",
    'oldest'                                               => "Lo más antiguo",
    'all'                                                  => "Todo",
    'my_reports'                                           => "Mis crónicas",
    'country_name'                                         => "Nombre del país...",
    'countries'                                            => "Países",
    'cities'                                               => "Ciudades",
    'map_popup'                                            => "Ventana de mapa",
    'comments_popup'                                       => "Ventana de comentarios",
    'travlog_popup'                                        => "Ventana de cuaderno de bitácora",
    'create_travlog_popup'                                 => "Ventana para crear un cuaderno de bitácora",
    'share_on_travooo_popup'                               => "Ventana para compartir en Travooo",
    'trip_plan_popup'                                      => "Ventana de plan de viaje",
    'embed_popup'                                          => "Incrustar ventana",
    'reactions'                                            => "Reacciones",
    'count_comments'                                       => ":count comentarios",
    'share'                                                => "Compartir",
    'share_online'                                         => "Compartir en línea",
    'spread_the_word'                                      => "¡Qué corra la voz!",
    'embed'                                                => "Incrustar",
    'save_it_for_later'                                    => "Guardar para más tarde",
    'share_on_travooo'                                     => "Compartir en Travooo",
    'help_your_friends'                                    => "Ayude a sus personas amigas",
    'expert_in'                                            => "Competente en",
    'country_capital'                                      => "Capital del país",
    'population'                                           => "Población",
    'currency'                                             => "Moneda",
    'trip_plan_by'                                         => "Plan de viaje por",
    'view_plan'                                            => "Ver plan",
    'count_others_have_visited_this_place'                 => ":count más han visitado este sitio",
    'excellent'                                            => "Excelente:",
    'to_count_destinations_region_on_date'                 => "a :count destinos en :date",
    'count_review'                                         => ":count opiniones",
    'user_wright'                                          => ":name Wright",
    'more_travlogs'                                        => "Más cuadernos de bitácora",
    'travlog_details'                                      => "Detalles de bitácora",
    'report_title'                                         => "Título de la crónica",
    'where_to_go_this_summer'                              => "¿EEUU, Canadá o Japón? ¿Dónde ir este verano?",
    'max_characters_count'                                 => "Caracteres MÁXIMOS: :count",
    'description'                                          => "Descripción",
    'type_to_search_for_a_country'                         => "Escriba para buscar un país...",
    'selected_countries'                                   => "Países seleccionados:",
    'creating_a_travlog'                                   => "Creación de un cuaderno de bitácora",
    'title'                                                => "Título",
    'type_a_title'                                         => "Escriba un título",
    'count_tips_to_experience_region_to_the_fullest'       => ":count consejos para vivir :region al máximo",
    'upload_image'                                         => "Subir imagen",
    'select_from_library'                                  => "Seleccionar de la biblioteca",
    'video'                                                => "Video",
    'past_a_video_link_from_youtube_vimeo_and_press_enter' => "Copie el enlace de un video de Youtube o Vimeo y pulse Enter",
    'text'                                                 => "Texto",
    'start_writing_your_text_here'                         => "Empiece a escribir su texto aquí...",
    'move'                                                 => "Recorrido",
    'map'                                                  => "Mapa",
    'people'                                               => "Gente",
    'type_a_name'                                          => "Escriba un nombre...",
    'travooo_info'                                         => "Información de Travooo",
    'add_another_travooo_info'                             => "Añada más información de Travooo...",
    'my_trip_plan'                                         => "Mi plan de viaje",
    'add_photo_to_travlog'                                 => "Añada una foto a su cuaderno de bitácora",
    'search'                                               => "Buscar...",
    'add_travooo_info'                                     => "Añada información de Travooo",
    'general_facts'                                        => "Datos generales",
    'emergency_numbers'                                    => "Números de emergencia",
    'indexes'                                              => "Índice",
    'daily_costs'                                          => "Precio diario",
    'nationality'                                          => "Nacionalidad",
    'languages_spoken'                                     => "Idiomas que se hablan",
    'currencies'                                           => "Monedas",
    'timings'                                              => "Horarios",
    'religions'                                            => "Religiones",
    'units_of_measurement'                                 => "Unidades de medida",
    'working_days'                                         => "Días laborables",
    'transportation_methods'                               => "Métodos de transporte",
    'speed_limit'                                          => "Límite de velocidad",
    'say_something'                                        => "Diga algo...",
    'report'                                               => "Crónica",
    'trip_overview'                                        => "Resumen del viaje",
    'short_description_optional'                           => "Descripción corta (opcional)",
    'drag_or_click_to_add_cover_photo_for_your_travlog' => "Arrastre o pulse para añadir una foto de portada a su cuaderno de bitácora",
    'city_country_or_place'                             => "Ciudad, país o lugar...",
    'type_a_place'                                      => "Escriba un lugar...",
    'distance'                                          => "Distancia",
    'destination'                                       => "Destino",
    'count_destinations'                                => ":count destinos",
    'copy_and_past_this_code_into'                      => "Copie y pegue este código en el lugar de su página donde desee que aparezca la extensión:",
    'create_a_report'                                   => "Crear una crónica"
];