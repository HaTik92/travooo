<?php

return [
    'add_attachment'          => "Añadir un adjunto",
    'messages'                => "Mensajes",
    'to'                      => "A",
    'from'                    => "De",
    'by'                      => "por",
    'type_a_message_here'     => "Escriba un mensaje aquí",
    'write_a_message'         => "Escriba un mensaje...",
    'add_content_to_the_chat' => "Agregar un comentario al chat",
    'emoticons'               => [
        'like'  => "Me gusta",
        'haha'  => "Jajaja",
        'wow'   => "¡Ohhh!",
        'sad'   => "Triste",
        'angry' => "Furioso"
    ],


    'to_dd' => "Para:"
];