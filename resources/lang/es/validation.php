<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ":attribute tiene que aceptarse .",
    'active_url'           => ":attribute no es una URL válida.",
    'after'                => ":attribute debe ser de una fecha posterior a :date.",
    'after_or_equal'       => ":attribute debe ser de :date o posterior.",
    'alpha'                => ":attribute unicamente puede contener letras.",
    'alpha_dash'           => ":attribute unicamente puede contener letras, números y guiones.",
    'alpha_num'            => ":attribute unicamente puede contener letras y números.",
    'array'                => ":attribute debe ser un conjunto.",
    'before'               => ":attribute debe ser de una fecha anterior a :date.",
    'before_or_equal'      => ":attribute debe ser de :date o anterior.",
    'between'              => [
        'numeric' => ":attribute debe estar entre :min y :max.",
        'file'    => ":attribute debe estar entre :min y :max kilobytes.",
        'string'  => ":attribute debe estar entre :min y :max caracteres.",
        'array'   => ":attribute debe estar entre :min y :max elementos.",
    ],
    'boolean'              => "El campo de :attribute debe ser verdadero o falso.",
    'confirmed'            => "La validación de :attribute no coincide.",
    'date'                 => ":attribute no es una fecha válida.",
    'date_format'          => ":attribute no coincide con el formato :format.",
    'different'            => ":attribute y :other deben ser diferentes.",
    'digits'               => ":attribute debe ser de :digits dígitos.",
    'digits_between'       => ":attribute debe estar entre :min y :max dígitos.",
    'dimensions'           => "Las dimensiones de la imagen de :attribute no son válidas.",
    'distinct'             => "El campo de :attribute tiene un valor duplicado.",
    'email'                => ":attribute debe ser una dirección electrónica válida.",
    'exists'               => "La selección de :attribute no es válida.",
    'file'                 => ":attribute debe ser un archivo.",
    'filled'               => "El campo de :attribute debe tener un valor.",
    'image'                => ":attribute debe ser una imagen.",
    'in'                   => "La selección de :attribute no es válida.",
    'in_array'             => "El campo de :attribute no existe en :other.",
    'integer'              => ":attribute debe ser un número entero.",
    'ip'                   => ":attribute debe ser una dirección IP válida.",
    'json'                 => ":attribute debe ser una cadena JSON válida.",
    'max'                  => [
        'numeric' => ":attribute no puede superar :max.",
        'file'    => ":attribute no puede superar los :max kilobytes.",
        'string'  => ":attribute no puede superar los :max caracteres.",
        'array'   => ":attribute no puede superar los :max elementos.",
    ],
    'mimes'                => ":attribute debe ser un archivo :values.",
    'mimetypes'            => ":attribute debe ser un archivo :values.",
    'min'                  => [
        'numeric' => ":attribute no puede ser inferior a :min.",
        'file'    => ":attribute no puede ser inferior a :min kilobytes.",
        'string'  => ":attribute no puede ser inferior a :min caracteres.",
        'array'   => ":attribute no puede ser inferior a :min elementos.",
    ],
    'not_in'               => "La selección de :attribute no es válida.",
    'numeric'              => ":attribute debe ser un número.",
    'present'              => "El campo :attribute debe aparecer.",
    'regex'                => "El formato de :attribute no es válido.",
    'required'             => "El campo :attribute es obligatorio.",
    'required_if'          => "El campo :attribute es obligatorio cuando :other es :value.",
    'required_unless'      => "El campo :attribute es obligatorio a no ser que :other esté en :values.",
    'required_with'        => "El campo :attribute es obligatorio cuando hay :values.",
    'required_with_all'    => "El campo :attribute es obligatorio cuando hay :values.",
    'required_without'     => "El campo :attribute es obligatorio cuando no hay :values.",
    'required_without_all' => "El campo :attribute es obligatorio cuando no hay ningún :values.",
    'same'                 => ":attribute y :other deben coincidir.",
    'size'                 => [
        'numeric' => ":attribute debe ser :size.",
        'file'    => ":attribute debe ser de :size kilobytes.",
        'string'  => ":attribute debe ser de :size caracteres.",
        'array'   => ":attribute debe contener :size elementos.",
    ],
    'string'               => ":attribute debe ser una cadena.",
    'timezone'             => ":attribute debe ser una zona válida.",
    'unique'               => ":attribute ya está siendo utilizado.",
    'uploaded'             => "Fallo al subir :attribute.",
    'url'                  => "El formato de :attribute no es válido.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => "mensaje personalizado",
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [

        'backend' => [
            'access' => [
                'permissions' => [
                    'associated_roles' => "Roles asociados",
                    'dependencies'     => "Dependencias",
                    'display_name'     => "Nombre que se va a mostrar",
                    'group'            => "Grupo",
                    'group_sort'       => "Ordenar por grupo",

                    'groups' => [
                        'name' => "Nombre del grupo",
                    ],

                    'name'   => "Nombre",
                    'system' => "¿Sistema?",
                ],

                'roles' => [
                    'associated_permissions' => "Permisos asociados",
                    'name'                   => "Nombre",
                    'sort'                   => "Ordenar",
                ],

                'users' => [
                    'active'                  => "Activar",
                    'associated_roles'        => "Roles asociados",
                    'confirmed'               => "Confirmado",
                    'email'                   => "Dirección electrónica",
                    'name'                    => "Nombre",
                    'other_permissions'       => "Otros permisos",
                    'password'                => "Contraseña",
                    'password_confirmation'   => "Validar contraseña",
                    'send_confirmation_email' => "Enviar correo de validación",
                    'age'                     => "Edad",
                    'address'                 => "Dirección",
                    'gender'                  => "Sexo",
                    'single'                  => "Estado de relación",
                    'children'                => "Hijos",
                    'mobile'                  => "Nº de móvil",
                    'nationality'             => "Nacionalidad",
                    'public_profile'          => "Estado del perfil",
                    'notifications'           => "Notificaciones",
                    'messages'                => "Mensajes",
                    'username'                => "Nombre de usuario",
                    'travel_type'             => "Tipo de viaje",
                    'profile_picture'         => "Foto de perfil",
                    'sms'                     => "Notificación SMS",
                ],
                'languages' => [
                    'title' => "Nombre del idioma",
                    'code'  => "Código del idioma",
                    'active' => "Activar",
                ],
            ],
        ],

        'frontend' => [
            'email'                     => "Dirección electrónica",
            'name'                      => "Nombre",
            'password'                  => "Contraseña",
            'password_confirmation'     => "Validar contraseña",
            'old_password'              => "Contraseña antigua",
            'new_password'              => "Contraseña nueva",
            'new_password_confirmation' => "Validar la nueva contraseña",
        ],
    ],

];
