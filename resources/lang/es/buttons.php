<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'activate'           => "Activar",
                'change_password'    => "Cambiar contraseña",
                'clear_session'      => "Limpiar sesión",
                'deactivate'         => "Desactivar",
                'delete_permanently' => "Eliminar permanentemente",
                'login_as'           => "Conectarse como :user",
                'resend_email'       => "Volver a enviar mensaje de validación",
                'restore_user'       => "Restaurar usuario/a",
            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => "Validar cuenta",
            'reset_password'  => "Restablecer contraseña",
        ],
    ],

    'general' => [
        'cancel'   => "Cancelar",
        'continue' => "Continuar",

        'crud' => [
            'create' => "Crear",
            'delete' => "Eliminar",
            'edit'   => "Editar",
            'update' => "Actualizar",
            'view'   => "Ver",
        ],

        'save'           => "Guardar",
        'view'           => "Ver",
        'search'         => "Buscar",
        'search_dots'    => "Buscar...",
        'post'           => "PUBLICAR",
        'more'           => "Más",
        'load_more'      => "Subir más",
        'load_more_dots' => "Subir más...",
        'send'           => "Enviar",
        'see_more'       => "Ver más",
        'follow'         => "Seguir",
        'copy'           => "Copiar",
        'next'           => "Siguiente",
        'publish'        => "Publicar",
        'request'        => "Solicitar",
        'delete_dots'    => "Eliminar...",
        'approve'        => "Aprobar",
        'disapprove'     => "Rechazar",
        'invite'         => "Invitar",
        'see_all'        => "Ver todo ",

        'more_dots' => "Más...",
        'unfollow'  => "Dejar de seguir"
    ],
];
