<?php

return [
    'placeholders' => [
        'from'                => "De",
        'to'                  => "A",
        'travel_date'         => "Fecha de viaje",
        'return_date'         => "Fecha de regreso",
        'type_a_message_here' => "Escriba un mensaje aquí"
    ],
    'buttons'      => [
        'search' => "Buscar",
        'send'   => "Enviar"
    ]
];