<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'roles' => [
                'already_exists'    => "Ese rol ya existe. Por favor, elija otro nombre.",
                'cant_delete_admin' => "No se puede eliminar el administrador de roles.",
                'create_error'      => "Ha surgido un problema al crear este rol. Por favor, inténtelo de nuevo.",
                'delete_error'      => "Ha surgido un problema al borrar este rol. Por favor, inténtelo de nuevo.",
                'has_users'         => "No se puede eliminar un rol que tiene usuarios asociados.",
                'needs_permission'  => "Debe seleccionar, como mínimo, un permiso para este rol.",
                'not_found'         => "Ese rol no existe.",
                'update_error'      => "Ha surgido un problema al actualizar este rol. Por favor, inténtelo de nuevo.",
            ],

            'users' => [
                'cant_deactivate_self'  => "No puede hacerse ese a sí mismo.",
                'cant_delete_admin'  => "No se puede eliminar el super administrador.",
                'cant_delete_self'      => "No puede eliminarse a sí mismo.",
                'cant_delete_own_session' => "No puede eliminar su propia sesión.",
                'cant_restore'          => "Este usuario no se ha eliminado, por lo que no se puede restaurar.",
                'create_error'          => "Ha surgido un problema al crear este usuario. Por favor, inténtelo de nuevo.",
                'delete_error'          => "Ha surgido un problema al borrar este usuario. Por favor, inténtelo de nuevo.",
                'delete_first'          => "Se debe primero borrar este usuario antes de eliminarlo permanentemente.",
                'email_error'           => "Esta dirección electrónica pertenece a otro usuario.",
                'mark_error'            => "Ha surgido un problema al actualizar este usuario. Por favor, inténtelo de nuevo.",
                'not_found'             => "Ese usuario no existe.",
                'restore_error'         => "Ha surgido un problema al restaurar este usuario. Por favor, inténtelo de nuevo.",
                'role_needed_create'    => "Debe elegir por lo menos un rol.",
                'role_needed'           => "Debe elegir como mínimo un rol.",
                'session_wrong_driver'  => "Debe configurar el controlador de la sesión en la base de datos para utilizar esta función.",
                'update_error'          => "Ha surgido un problema al actualizar este usuario. Por favor, inténtelo de nuevo.",
                'update_password_error' => "Ha surgido un problema al cambiar la contraseña de este usuario. Por favor, inténtelo de nuevo.",
            ],

            'language' => [
                'code_error'    => "Este código de idioma ya existe en la base de datos",
                'update_error'  => "Ha surgido un error inesperado, inténtelo de nuevo",
            ],
        ],
    ],

    'frontend' => [
        'auth' => [
            'confirmation' => [
                'already_confirmed' => "Su cuenta ya está validada.",
                'confirm'           => "¡Valide su cuenta!",
                'created_confirm'   => "Su cuenta se ha creado con éxito. Le hemos enviado un correo electrónico para validar su cuenta.",
                'mismatch'          => "El código de validación no coincide.",
                'not_found'         => "El código de validación no existe.",
                'resend'            => "Su cuenta no está validada. Por favor, pulse el enlace de validación de su correo electrónico o <a href=\"".route('frontend.auth.account.confirm.resend', ':user_id').'">click here</a> to resend the confirmation e-mail.',
                'success'           => "¡Su cuenta se ha validado con éxito!",
                'resent'            => "Se ha enviado un nuevo mensaje de validación a la dirección electrónica que figura en su fichero.",
            ],

            'deactivated' => "Su cuenta se ha desactivado.",
            'email_taken' => "La dirección electrónica ya está siendo utilizada.",

            'password' => [
                'change_mismatch' => "Esta no es su contraseña antigua.",
            ],

            'registration_disabled' => "El registro está actualmente fuera de servicio.",
        ],
    ],
];
