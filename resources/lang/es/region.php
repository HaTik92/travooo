<?php

return [
    'city'                                              => "Ciudad",
    'city_in_name'                                      => "Ciudad en :name",
    'country_in_name'                                   => "País en :name",
    'country'                                           => "País",
    'potential_dangers'                                 => "Posibles peligros",
    'indexes'                                           => "Índice",
    'map_index'                                         => "Índice del mapa",
    'pollution'                                         => "Contaminación",
    'cost_of_living'                                    => "Coste de la vida",
    'crime_rate'                                        => "Tasa de criminalidad",
    'quality_of_life'                                   => "Calidad de vida",
    'suggest'                                           => "Sugerir",
    'have_something_to_say_about_caution_in_new_york'   => "¿Tiene alguna advertencia sobre Nueva York?",
    'have_something_to_say_about_etiquette_in_new_york' => "¿Tiene algo que decir sobre el protocolo en Nueva York?",
    'have_something_to_say_about_health_in_new_york'    => "¿Tiene algo que decir sobre la salud en Nueva York?",
    'country_in'                                        => "País en",
    'etiquette'                                         => "Protocolo",
    'restrictions'                                      => "Restricciones",
    'risk_of_disease'                                   => "Riesgo de enfermedad",
    'daily_costs'                                       => "Precio diario",
    'packing_tips'                                      => "Consejos sobre el equipaje",
    'visa'                                              => "Visa",
    'required_for_moroccan_passport_holders'                 => "Obligatorio para los titulares de pasaportes marroquíes",
    'id'                                                     => "Identificador",
    'proin_cursus_erat_at_lorem_placerat_bibendum_tincidunt' => "Proin cursus erat at lorem placerat, bibendum tincidunt.",
    'photos'                                                 => "Fotos",
    'mauris_laoreet_nibh_nec_odio_porta_consectetur'    => "Mauris laoreet nibh nec odio porta consectetur.",
    'good_behavior_certificate'                         => "Certificado de buena conducta",
    'aenean_viverra_mi_at_varius_venenatis'             => "Aenean viverra mi at varius venenatis.",
    'embassy_location'                                  => "Ubicación de la embajada",
    'address'                                           => "122 Boulevard d'Anfa, 5th Floor, Casablanca 20000, Marruecos.",
    'opening_time'                                      => "Horario de apertura",
    'opening_time_dd'                                   => "Horario de apertura:",
    'phone_number'                                      => "Número de teléfono",
    'phone_number_dd'                                   => "Número de teléfono:",
    'website_url'                                       => "URL del sitio web",
    'website_url_dd'                                    => "URL del sitio web:",
    'no_in_morocco'                                     => "¿No hay en Marruecos?",
    'not_in_morocco'                                    => "¿No se encuentra en Marruecos?",
    'france'                                            => "Francia",
    'faroe_islands'                                     => "Islas Feroe",
    'fiji'                                              => "Fiji",
    'new_york_today'                                    => "Nueva York hoy",
    'add_photo'                                         => "Añadir foto",
    'best_time_to_go'                                        => "El mejor momento para ir",
    'flight_search'                                          => "Buscar vuelo",
    'low_season'                                             => "Temporada baja",
    'shoulder'                                               => "Temporada media",
    'high_season'                                            => "Temporada alta",
    'flights_from_morocco'                                   => "Vuelos desde Marruecos",
    'new_york_away_from_rabat_in_direct_flight_time'         => "Nueva York está a 7 h 48 min de Rabat de vuelo directo, sin contar cualquier posible escala.",
    '7h_48min'                                               => "7 h 48 min",
    'main_airports'                                          => "Aeropuertos principales",
    'see_all'                                                => "Ver todo ",
    'about_the_city'                                         => "Sobre la ciudad",
    'nationality'                                            => "Nacionalidad",
    'languages_spoken'                                       => "Idiomas que se hablan",
    'currencies'                                             => "Monedas",
    'timings'                                                => "Horarios",
    'religions'                                              => "Religiones",
    'phone_code'                                             => "Prefijo",
    'units_of_measurement'                                   => "Unidades de medida",
    'working_days'                                           => "Días laborables",
    'transportation_methods'                                 => "Métodos de transporte",
    'speed_limit'                                            => "Límite de velocidad",
    'emergency_number'                                       => "Números de emergencia",
    'top_places'                                             => "Lugares destacados",
    'accessibility'                                          => "Accesibilidad",
    'atms'                                                   => "Centrales bancarias automáticas",
    'internet'                                               => "Internet",
    'sockets'                                                => "Enchufes",
    'hotels'                                                 => "Hoteles",
    'atm_machines'                                           => "Cajeros automáticos",
    'stay_at'                                                => "Alojarse en",
    'interested_in_staying_at'                               => "Interesado en alojarse en",
    'book_a_hotel_now'                                       => "Reserve un hotel ¡AHORA!",
    'city_in'                                                => "Ciudad en",
    'suggest_an_advice'                                      => "Haga una recomendación",
    'less_safe'                                              => "Menos seguro",
    'safeer'                                                 => "Más seguro",
    'national_holidays'                                      => "Vacaciones nacionales",
    'trip_plans'                                             => "Planificación del viaje",
    'best_places'                                            => "Mejores lugares",
    'nearby_events'                                          => "Próximos eventos",
    'videos'                                            => "Videos",
    'your_friends'                                      => "Sus personas amigas",
    'experts_users'                                     => "Especialistas y usuarios",
    'amy_green'                                         => "Amy Green",
    'checked_in'                                        => "Facturado",
    'stories_popup'                                     => "Ventana de historias",
    'trip_plans_popup'                                  => "Ventana de planificación del viaje",
    'about_the_country'                                 => "Sobre el país",
    'discussed'                                         => "Comentado",
    'by'                                                => "por",
    'and_count_more'                                    => "y :count más",
    'population'                                        => "Población",
    'cities'                                            => "Ciudades",
    'capital_of'                                        => "Capital de",
    'followers'                                         => "Personas que le siguen",
    'talking_about_it'                                  => "Hablar sobre ello",
    'write_an_advice_about_caution_in'                  => "Escriba una advertencia sobre",
    'country_capital'                                   => "Capital del país",
    'currency'                                          => "Moneda ",
    'symbol'                                            => "Símbolo",
    'code'                                              => "Código",
    'country_name_dots'                                 => "Nombre del país...",
    'count_city'                                        => "{0} :count ciudad|[2, *] :count ciudades",
    'capital_of_place'                                  => "Capital de :place",
    'nav'                                               => [
        'about'        => "Sobre",
        'packing_tips' => "Consejos sobre el equipaje",
        'weather'      => "Clima",
        'etiquette'    => "Protocolo",
        'health '      => "Salud",
        'visa'         => "Visa",
        'when_to_go'   => "¿Cuándo ir?",
        'caution'      => "Advertencia",
    ],
    'buttons'                                           => [
        'follow'          => "Seguir",
        'let_us_know'     => "Háganoslo saber",
        'suggest_one'     => "Haga una sugerencia",
        'add_your_advice' => "Añada una recomendación",
        'add_a_tip'       => "Añada un consejo",
        'plan_a_trip'     => "Planifique un viaje",
        'message'         => "Mensaje"
    ],

    'write_an_advice_about_etiquette_in_place'    => "Escriba una recomendación sobre protocolo en :place ...",
    'write_an_advice_about_health_in_place'       => "Escriba una recomendación sobre salud en :place ...",
    'write_an_advice_about_packing_tips_in_place' => "Escriba una recomendación sobre equipaje en :place ...",
    'write_an_advice_about_caution_in_place'      => "Escriba una advertencia sobre :place ...",
    'check_the_full_trip_plan_dd'                 => "Compruebe todo el plan de viaje:",
    'experts_global'                               => "Global",
    'experts_local'                               => "Local",
];