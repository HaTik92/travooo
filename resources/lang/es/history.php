<?php

return [

    /*
    |--------------------------------------------------------------------------
    | History Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain strings associated to the
    | system adding lines to the history table.
    |
    */

    'backend' => [
        'none'            => "No hay ninguna historia reciente.",
        'none_for_type'   => "No hay ninguna historia de este tipo.",
        'none_for_entity' => "No hay ninguna historia de este :entity.",
        'recent_history'  => "Historia reciente",

        'roles' => [
            'created' => "rol creado",
            'deleted' => "rol borrado",
            'updated' => "rol actualizado",
        ],
        'users' => [
            'changed_password'    => "cambiar la contraseña al usuario",
            'created'             => "usuario creado",
            'deactivated'         => "usuario desactivado",
            'deleted'             => "usuario borrado",
            'permanently_deleted' => "usuario borrado permanentemente",
            'updated'             => "usuario actualizado",
            'reactivated'         => "usuario reactivado",
            'restored'            => "usuario restaurado",
        ],
    ],
];
