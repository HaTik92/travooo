<?php

return [
    'home' => "Inicio",
    'login' => "Conectarse a su cuenta",
    'email' => "Dirección electrónica",
    'password' => "Contraseña",
    'forgot_password' => "¿Ha olvidado su contraseña?",
    'do_login' => "Iniciar sesión",
    'fb_login' => "Seguir con Facebook",
    'tw_login' => "Seguir con Twitter",
    'not_a_member' => "¿Aún no tiene cuenta?",
    'signup' => "Registrarse ",
    'create_account' => "Crear una cuenta gratuita",
    'username' => "Nombre de usuario",
    'password' => "Contraseña",
    'password_confirm' => "Validar contraseña",
    'continue' => "Continuar",
    'or' => "O",
    'agree_terms' => "Al crear una cuenta usted está de acuerdo con",
    'travooos' => "de Travooo",
    'terms_of_service' => "Términos del servicio",
    'privacy_policy' => "Política de privacidad",
    'already_member' => "¿Ya se ha registrado?",
    'full_name' => "Nombre completo",
    'age' => "Edad",
    'male' => "Hombre",
    'female' => "Mujer",
    'back' => "Volver",
    'Please select your Travel Style' => "Por favor, selecciones su estilo de viaje",
];
