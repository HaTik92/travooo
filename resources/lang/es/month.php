<?php

return [
    'january'   => "enero",
    'february'  => "febrero",
    'march'     => "marzo",
    'april'     => "abril",
    'may'       => "mayo",
    'june'      => "junio",
    'july'      => "julio",
    'august'    => "agosto",
    'september' => "septiembre",
    'october'   => "octubre",
    'november'  => "noviembre",
    'december'  => "diciembre",

    'short' => [
        'january'   => "ene",
        'february'  => "feb",
        'march'     => "mar",
        'april'     => "abr",
        'may'       => "may",
        'june'      => "jun",
        'july'      => "jul",
        'august'    => "ago",
        'september' => "sep",
        'october'   => "oct",
        'november'  => "nov",
        'december'  => "dic",
    ]
];