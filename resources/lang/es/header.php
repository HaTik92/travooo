<?php

return [
    'places'      => "lugares",
    'restaurants' => "restaurantes",
    'hotels'      => "hoteles",
    'countries'   => "países",
    'cities'      => "ciudades",
    'people'      => "gente",

];