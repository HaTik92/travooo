<?php

return [
    'nav'                                     => [
        'travel_history' => "Historial de viaje",
        'my_map'         => "Mi mapa",
        'trip_plans'     => "Planificación del viaje ",
        'photos'         => "Fotos",
        'videos'         => "Videos",
        'reviews'        => "Reseñas",
        'badges'         => "Distintivos"

    ],
    'profile_photos_popup'                    => "Ventana de fotos de perfil",
    'links'                                   => "Enlaces",
    'website'                                 => "Sitio web",
    'from_where'                              => "¿Desde dónde?",
    'age'                                     => "Edad",
    'profession'                              => "Profesión",
    'interest'                                => "Intereses",
    'expert_in'                               => "Competente en",
    'member_since'                            => "Miembro desde",
    'visited'                                 => "Visitado",
    'posts'                                   => "Publicaciones",
    'followers'                               => "Personas que le siguen",
    'following'                               => "Siguiendo",
    'about'                                   => "Sobre",
    'follow'                                  => "Seguir",
    'unfollow'                                => "Dejar de seguir",
    'level_up'                                => "Subir de nivel",
    'your_next_badge'                         => "Siguiente distintivo",
    'badge_name'                              => "Nombre del distintivo",
    'users_like_you_have_this_badge'          => "Personas como usted tienen este distintivo",
    'all_your_badges'                         => "Todos sus distintivos",
    'more'                                    => "más",
    'points_to_the_next_badge'                => "puntos para el siguiente distintivo",
    'left'                                    => "faltan",
    'likes'                                   => "Me gusta",
    'trips'                                   => "Viajes",
    'reviews'                                 => "Reseñas",
    'videos'                                  => "Videos",
    'photos'                                  => "Fotos",
    'all_favorites'                           => "Todos los favoritos",
    'sort_by'                                 => "Ordenar por",
    'date'                                    => "Fecha",
    'city_in'                                 => "Ciudad en",
    'saved_from'                              => "Guardado de",
    'photo'                                   => "Foto",
    'country_in'                              => "País en",
    'video'                                   => "Video",
    'national_holiday_in'                     => "Vacaciones nacionales en",
    'trip_plan'                               => "Plan de viaje",
    'create_by'                               => "Creado por",
    'view_plan'                               => "Ver plan",
    'tip_about'                               => "consejo sobre",
    'saved_from_a'                            => "Guardado de un/a",
    'by'                                      => "por",
    'type'                                    => "Clase",
    'places'                                  => "Lugares",
    'events'                                  => "Eventos",
    'asking'                                  => "Solicitar",
    'for_recommendations'                     => "Recomendaciones",
    'for_tips'                                => "Consejos",
    'about_a_trip_plan'                       => "Sobre un plan de viaje",
    'a_general_question'                      => "Una pregunta general",
    'topic'                                   => "Tema",
    'a_topic_for_your_question'               => "Por ejemplo, un tema para su pregunta podría ser «Fotografía, Nueva York»",
    'place_city_country'                      => "Lugar, ciudad, país...",
    'destination'                             => "Destino",
    'tips_about'                              => "Consejos sobre",
    'select_one_of_your_trip_plans'           => "Seleccione una de sus planificaciones",
    'select'                                  => "Seleccionar",
    'destinations'                            => "Destinos",
    'question'                                => "Preguntas",
    'write_your_question'                     => "Escriba una pregunta",
    'description'                             => "Descripción",
    'write_more_details_about_your_question'  => "Escriba más detalles a cerca de su pregunta...",
    'attach_pictures_count_max'               => "Adjunte fotos (:count máx.)",
    'select_one_of_your_trip_plan'            => "Seleccione uno de sus planes de viaje",
    'search_in_your_trip_plans'               => "Busque en sus planificaciones",
    'asking_popup'                            => "Ventana de consulta",
    'travel_mates'                            => "Acompañantes de viaje",
    'friends'                                 => "Personas amigas",
    'operator'                                => "Operador",
    'photographer'                            => "Fotógrafo/a",
    'traveller'                               => "Viajero/a",
    'friend'                                  => "Persona amiga",
    'travel_mate'                             => "Acompañante de viaje",
    'adventurer'                              => "Aventurero/a",
    'explorer'                                => "Explorador/a",
    'surfer'                                  => "Surfista",
    'unfriend'                                => "Poco amistoso",
    'badges'                                  => "Distintivos",
    'leaderboard'                             => "Clasificación",
    'points'                                  => "Puntos",
    'unlocked'                                => "Desbloqueado",
    'count_have_this_badge'                   => ":count tienen este distintivo",
    'reviewer'                                => "Comentarista",
    'you_uploaded_a_photo'                    => "Su foto se ha subido",
    'you_just_uploaded_a'                     => "Acaba de subir un",
    'related_to'                              => "en relación a",
    'you_commented_on_a_post'                 => "Ha comentado una publicación",
    'you_uploaded_count_photo'                => "Ha subido :count fotos",
    'loading'                                 => "Cargando...",
    'all_reviews'                             => "Todos los comentarios",
    'score'                                   => "puntuación",
    'from_count_reviews'                      => "de :count comentarios",
    'more_photos'                             => "Más fotos",
    'countries'                               => "Países",
    'privacy'                                 => "Privacidad",
    'show_all'                                => "mostrar todo",
    'cities'                                  => "Ciudades",
    'add_to_trip_plan'                        => "Añadir al plan de viaje",
    'write_a_review'                          => "Escribir una reseña",
    'hide_from_section'                       => "Ocultar de la sección",
    'park_in'                                 => "Parque en",
    'mountain_in'                             => "Montaña en",
    'hotel'                                   => "Hotel",
    'hotel_in'                                => "Hotel en",
    'find_more_events'                        => "Encuentre más eventos",
    'event_in'                                => "Evento en",
    'all_videos'                              => "Todos los videos",
    'reactions'                               => "reacciones",
    'comments'                                => "comentarios",
    'all_photos'                              => "Todas las fotos",
    'albums'                                  => "Álbumes",
    'create_album'                            => "Crear un álbum",
    'all_visited'                             => "Todo lo visitado",
    'your_photos'                             => "Sus fotos",
    'full_screen'                             => "Pantalla completa",
    'duration'                                => "Duración",
    'distance'                                => "Distancia",
    'added_a'                                 => "Se añadió un/a",
    'share'                                   => "Compartir",
    'spread_the_word'                         => "¡Qué corra la voz!",
    'add_to_favorites'                        => "Añadir a favoritos",
    'save_it_for_later'                       => "Guardar para más tarde",
    'report'                                  => "Crónica",
    'help_us_understand'                      => "Ayúdenos a comprender",
    'view_profile'                            => "Ver perfil",
    'profile'                                 => "Perfil",
    'shared_a'                                => "Compartir un/a",
    'started_following'                       => "Ahora le sigue",
    'count_talking_about_this'                => ":count están hablando de esto",
    'timeline'                                => "Biografía",
    'write_something'                         => "escriba_algo...",
    'in'                                      => "en",
    'delete'                                  => "Eliminar...",
    'location'                                => "Ubicación",
    'from'                                    => "De:",
    'all_trip_plans'                          => "Todos los planes de viaje",
    'edit'                                    => "Editar",
    'solo'                                    => "solo",
    'urban'                                   => "de la ciudad",
    'count_users_like_you_have_this_badge'    => ":count personas como usted tienen este distintivo",
    'count_points_to_the_next_badge'          => ":count puntos para el siguiente distintivo",
    'search_your_visited_places'              => "Buscar los lugares que ha visitado...",
    'trip_plans'                              => "Planificación del viaje ",
    'new_trip_plan'                           => "Nuevo plan de viaje",
    'upcoming'                                => "Próximamente",
    'creation_date'                           => "Fecha de creación",
    'title'                                   => "Título",
    'to'                                      => "para",
    'invited'                                 => "Enviada invitación",
    'are_you_sure'                            => "¿Está usted seguro?",
    'if_you_canceled_this_trip_you_will_lose' => "Si cancela este viaje usted perderá ¡todo lo que ha avanzado y su configuración!",
    'unknown_error_while_deleting_trip_plan'  => "¡Error desconocido al eliminar el plan de viaje!",
    'post'                                    => "Publicación",
    'phone'                                   => "Teléfono",
    'fax'                                     => "Fax",
    'address'                                 => "Dirección",
    'count_followers'                         => ":count personas que le siguen",
    'count_follower'                          => "{0}:count persona que le sigue|[2,*]:count personas que le siguen",
    'male'                                    => "Hombre",
    'female'                                  => "Mujer",
    'count_years'                             => "{0}:count año|[2,*]:count años",
    'country_of_origin'                       => "País de procedencia",
    'interests'                               => "Aficiones",
    'search_for_an_interest'                  => "Buscar una afición...",
    'gender'                                  => "Sexo",
    'have_something_to_tell_name'             => "¿Tiene algo que decirle a :name?",
    'discussed_by'                            => "Comentado por",
    'count_users'                             => ":count usuarios",
    'invite_people'                           => "Invitar a gente",
    'suggested_by'                            => "Sugerido por",
    'check_in'                => "Facturación",
    'checked_in_here'         => "facturado aquí",
    'contact_him_now'         => "ponerse en contacto ahora",
    'public'                  => "Público",
    'everyone_can_see_this'   => "Cualquiera puede ver esto",
    'only_visible_to_friends' => "Solo las personas amigas pueden verlo",
    'visible_only_to_me'      => "Solo yo puedo verlo",
    'private'                 => "Privado",
    'count_left'              => ":count faltan",
    'count_points'            => ":count puntos"
];