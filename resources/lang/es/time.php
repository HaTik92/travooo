<?php

return [
    'h'                => "h",
    'min'              => "min",
    'am'               => "AM",
    'pm'               => "PM",
    'hours'            => "Horas",
    'count_hours'      => "{0}:count horas|[2,*]:count horas",
    'hours_ago'        => "hace horas",
    'nights'           => "noches",
    'all_week'         => "Toda la semana",
    'day'              => "Día",
    'day_value'        => "Día :value",
    'count_day'        => "{0} :count días|[2, *] :count días",
    'count_day_in'     => "{0} :count días en|[2, *]:count días en",
    'count_days'       => ":count días",
    'date'             => "Fecha",
    'mutual_dates'     => "Fechas comunes",
    'mutual_dates_dd'  => "Fechas comunes:",
    'trip_start_date'  => "Fecha de inicio del viaje",
    'trip_finish_date' => "Fecha de finalización del viaje",
    'time'             => "Programar",
    'minutes'          => "Minutos",
    'count_min'        => ":count min",
    'week'             => [
        'monday'    => "Lunes",
        'tuesday'   => "Martes",
        'wednesday' => "Miércoles",
        'thursday'  => "Jueves",
        'friday'    => "Viernes",
        'saturday'  => "Sábado",
        'sunday'    => "Domingo",
        'short'     => [
            'monday'    => "lun",
            'tuesday'   => "mar",
            'wednesday' => "mié",
            'thursday'  => "jue",
            'friday'    => "vie",
            'saturday'  => "sáb",
            'sunday'    => "dom"
        ]
    ],

    'count_nights' => ":count noches",
    'from_to'      => "De: :from a :to"
];