<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'   => "Inicio",
        'logout' => "Desconectarse",
    ],

    'frontend' => [
        'dashboard' => "Panel de control",
        'login'     => "Conectarse",
        'macros'    => "Macros",
        'register'  => "Registrarse",

        'user' => [
            'account'         => "Mi cuenta",
            'administration'  => "Administrar",
            'change_password' => "Cambiar contraseña",
            'my_information'  => "Mi información",
            'profile'         => "Perfil",
        ],

        'privacy'     => "Privacidad",
        'about'       => "Sobre",
        'terms'       => "Términos",
        'advertising' => "Publicidad",
        'copyright  ' => "Derechos de autor",
        'cookies'     => "Galletas",
        'more'        => "Más",
        'forum'       => "Foro",
        'newest'      => "Lo más nuevo",
        'trending'    => "Tendencias",
        'ask'         => "Preguntar",
        'map'         => "Mapa",
        'flights'     => "Vuelos",
        'hotels'      => "Hoteles",
        'reports'     => "Crónicas"

    ],
];
