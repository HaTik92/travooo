<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all'     => "Todo",
        'yes'     => "Sí",
        'no'      => "No",
        'custom'  => "Personalizar",
        'actions' => "Acciones",
        'active'  => "Activar",
        'buttons' => [
            'save'   => "Guardar",
            'update' => "Actualizar",
        ],
        'hide'              => "Ocultar",
        'inactive'          => "Inactivo",
        'none'              => "Ninguno",
        'show'              => "Mostrar",
        'toggle_navigation' => "Cambiar de navegación",
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create'     => "Crear rol",
                'edit'       => "Editar rol",
                'management' => "Gestión de roles",

                'table' => [
                    'number_of_users' => "Número de usuarios",
                    'permissions'     => "Permisos",
                    'role'            => "Rol",
                    'sort'            => "Ordenar",
                    'total'           => "total de roles|total de roles",
                ],
            ],
            'language' => [
                'management' => "Gestión de idioma"
            ],
            'users' => [
                'active'              => "Usuarios activos",
                'all_permissions'     => "Todos los permisos",
                'change_password'     => "Cambiar contraseña",
                'change_password_for' => "Cambiar contraseña a :user",
                'create'              => "Crear usuario",
                'deactivated'         => "Desactivar usuarios",
                'deleted'             => "Eliminar usuarios",
                'edit'                => "Editar usuario",
                'management'          => "Gestión de usuarios",
                'no_permissions'      => "Sin permisos",
                'no_roles'            => "Sin roles para configurar.",
                'permissions'         => "Permisos",
                'age'                 => "Edad",
                'table' => [
                    'confirmed'      => "Confirmado",
                    'created'        => "Creado",
                    'email'          => "e-mail",
                    'id'             => "Identificador",
                    'last_updated'   => "Última actualización",
                    'name'           => "Nombre",
                    'no_deactivated' => "Usuarios no desactivados",
                    'no_deleted'     => "Usuarios no borrados",
                    'roles'          => "Roles",
                    'total'          => "Total de usuarios|total de usuarios",
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => "Resumen",
                        'history'  => "Historia",
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => "Avatar",
                            'confirmed'    => "Confirmado",
                            'created_at'   => "Creado en ",
                            'deleted_at'   => "Eliminado en",
                            'email'        => "e-mail",
                            'last_updated' => "Última actualización",
                            'name'         => "Nombre",
                            'status'       => "Estado",
                        ],
                    ],
                ],

                'view' => "Ver usuario",
            ],
        ],
        'languages' => [
            'languages_manager' => "Gestor de idiomas",
            'languages_management'=> "Gestión de idiomas"
        ],
        'levels' => [
            'levels' => "Niveles",
            'levels_manager' => "Gestor de niveles",
        ],
        'locations' => [
            'locations_manager' => "Gestor de ubicaciones",
            'regions' => "Regiones",
            'countries' => "Países",
            'cities' => "Ciudades",
            'place_types' => "Tipos de lugar",
            'places' => "Lugares"
        ],
        'safety_degrees' => [
            'safety_degrees_manager' => "Gestor de nivel de seguridad",
            'safety_degrees' => "Nivel de seguridad",
        ],
        'activities' => [
            'activities_manager' => "Gestor de actividades",
            'activity_types' => "Tipo de actividades",
            'activities' => "Actividades",
            'activity_media' => "Actividades en las redes",
        ],
        'interests' => [
            'interests_manager' => "Gestor de aficiones",
            'interests' => "Aficiones"
        ],
        'religions' => [
            'religions_manager' => "Gestor de religiones",
            'religions' => "Religiones"
        ],
        'lifestyles' => [
            'lifestyles_manager' => "Gestor de estilos de vida",
            'lifestyles' => "Estilos de vida"
        ],
        'languages_spoken' => [
            'languages_spoken_manager' => "Gestor de los idiomas que se hablan",
            'languages_spoken' => "Idiomas que se hablan "
        ],
        'timings' => [
            'timings_manager' => "Gestor de horarios",
            'timings' => "Horarios"
        ],
        'weekdays' => [
            'weekdays_manager' => "Gestor de días de la semana",
            'weekdays' => "Días de la semana"
        ],
        'holidays' => [
            'holidays_manager' => "Gestor de vacaciones",
            'holidays' => "Vacaciones"
        ],
        'hobbies' => [
            'hobbies_manager' => "Gestor de pasatiempos",
            'hobbies' => "Pasatiempos"
        ],
        'emergency_numbers' => [
            'emergency_numbers_manager' => "Gestor de números de emergencia",
            'emergency_numbers' => "Números de emergencia"
        ],
        'currencies' => [
            'currencies_manager' => "Gestor de monedas",
            'currencies' => "Monedas"
        ],
        'cultures' => [
            'cultures_manager' => "Gestor de culturas",
            'cultures' => "Culturas"
        ],
        'accommodations' => [
            'accommodations_manager' => "Gestor de alojamientos",
            'accommodations' => "Alojamientos"
        ],
        'age_ranges' => [
            'age_ranges_manager' => "Gestor de franjas de edad",
            'age_ranges' => "Franjas de edad"
        ],
        'cities' => [
            'cities_manager' => "Gestor de ciudades",
            'active_cities' => "Ciudades activas",
        ],
        'embassies' => [
            'embassies_manager' => "Gestor de embajadas",
            'active_embassies' => "Embajadas activas",
            'embassies' => "Embajadas"
        ],
    ],

    'frontend' => [

        'auth' => [
            'login_box_title'    => "Conectarse",
            'login_button'       => "Conectarse",
            'login_with'         => "Conectarse con :social_media",
            'register_box_title' => "Registrarse",
            'register_button'    => "Registrarse",
            'remember_me'        => "Recordar",
        ],

        'passwords' => [
            'forgot_password'                 => "¿Ha olvidado su contraseña?",
            'reset_password_box_title'        => "Restablecer contraseña",
            'reset_password_button'           => "Restablecer contraseña",
            'send_password_reset_link_button' => "Enviar enlace para restablecer contraseña",
        ],

        'macros' => [
            'country' => [
                'alpha'   => "Código alfa del país",
                'alpha2'  => "Código alfa-2 del país",
                'alpha3'  => "Código alfa-3 del país",
                'numeric' => "Código numérico del país",
            ],

            'macro_examples' => "Ejemplos de macro",

            'state' => [
                'mexico' => "Lista de los estado de México",
                'us'     => [
                    'us'       => "Estados de EEUU",
                    'outlying' => "Territorios periféricos de EEUU",
                    'armed'    => "Fuerzas armadas de EEUU",
                ],
            ],

            'territories' => [
                'canada' => "Lista de provincias y territorios de Canadá",
            ],

            'timezone' => "Zona horaria",
        ],

        'user' => [
            'passwords' => [
                'change' => "Cambiar contraseña",
            ],

            'profile' => [
                'avatar'             => "Avatar",
                'created_at'         => "Creado en ",
                'edit_information'   => "Editar información",
                'email'              => "e-mail",
                'last_updated'       => "Última actualización",
                'name'               => "Nombre",
                'update_information' => "Actualizar información",
            ],
        ],

    ],
];
