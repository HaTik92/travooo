<?php

return [
    'sort_by'        => "Ordenar por",
    'filter'         => "Filtro",
    'website'        => "Sitio web",
    'back'           => "Volver",
    'all'            => "Todo",
    'reactions'      => "Reacciones",
    'reaction'       => "{0} Reacciones|[2,*] Reacciones",
    'and_count_more' => "y :count más",
    'views'          => "Visitas",
    'by'             => "Por",
    'top'            => "Destacado",
    'new'            => "Nuevo",
    'in'             => "en",
    'from'           => "De",
    'approved'       => "Aprobado",
    'report'        => "Crónica",
    'declined'      => "Rechazado",
    'central_part'  => "Parte central",
    'at'            => "en",
    'until'         => "Hasta",
    'add'           => "Añadir",
    'please_select' => "Por favor, seleccione...",
    'no_preference' => "Sin preferencias",
    'error_unknown' => "error_desconocido",
    'are_you_sure'  => "¿Está usted seguro?",
    'enter_amount'  => "Introduzca una cantidad",
    'count_more'    => ":count más",
    'start'         => "Empezar",
    'for_advice'    => "solicitar asesoramiento",
    'next_step'     => "Siguiente paso",
    'at_date'       => "el :date",
    'loading_dots'  => "Cargando..."

];