<?php

return [
    'comments'        => "Comentarios",
    'count_comment'   => "{0}:count comentario|[2,*] comentarios",
    'top'             => "Destacado",
    'new'             => "Nuevo",
    'write_a_comment' => "Escribir un comentario"
];