<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => "El rol se ha creado correctamente.",
            'deleted' => "El rol se ha eliminado correctamente.",
            'updated' => "El rol se ha actualizado correctamente.",
        ],

        'users' => [
            'confirmation_email'  => "Se ha enviado un nuevo mensaje de validación a la dirección electrónica que figura en su fichero.",
            'created'             => "El usuario se ha creado correctamente.",
            'deleted'             => "El usuario se ha eliminado correctamente.",
            'deleted_permanently' => "El usuario se ha eliminado de manera permanente.",
            'restored'            => "El usuario se ha restaurado correctamente.",
            'session_cleared'      => "La sesión del usuario se ha limpiado correctamente.",
            'updated'             => "El usuario se ha actualizado correctamente.",
            'updated_password'    => "La contraseña del usuario se ha actualizado correctamente.",
        ],

        'language' => [
            'created' => "Se ha creado un nuevo idioma",
            'deleted' => "Idioma borrado correctamente",
            'updated' => "Idioma actualizado correctamente",
        ],
    ],
];
