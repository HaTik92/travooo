<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'        => "معلومات تسجيل الدخول غير متوفرة في سجلاتنا.",
    'general_error' => "ليس لديك إذن للوصول.",
    'socialite'     => [
        'unacceptable' => ":المزود هو نوع تسجيل دخول غير صالح.",
    ],
    'throttle' => "محاولات كثيرة لتسجيل الدخول. يرجى المحاولة مجددًا خلال:",
    'unknown'  => "حدث خطأ غير متوقع",
];
