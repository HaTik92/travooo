<?php

return [

    /*
    |--------------------------------------------------------------------------
    | HTTP Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the views/errors files.
    |
    */

    '404' => [
        'title'       => "لم يتم العثور على الصفحة",
        'description' => "عذرًا، الصفحة التي تبحث عنها لم تعد موجودة",
    ],

    '503' => [
        'title'       => "سنعود قريبًا. ",
        'description' => "سنعود قريبًا. ",
    ],
];
