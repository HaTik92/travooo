<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => "يجب أن تحتوي كلمة المرور على ستة رموز على الأقل وأن توافق التأكيد.",
    'reset'    => "تم تغيير كلمة مرورك!",
    'sent'     => "أرسلنا إليك رسالة رابط تغيير كلمة السر",
    'token'    => "التوكن الخاص بتغيير كلمة المرور غير صالح.",
    'user'     => "لا يوجد حساب بهذا العنوان.",
    'required' => "The username or email address is required.",
    'invalid'  => "Invalid username or email address.",
];
