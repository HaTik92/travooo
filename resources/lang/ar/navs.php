<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'   => "الصفحة الرئيسية",
        'logout' => "تسجيل الخروج",
    ],

    'frontend' => [
        'dashboard' => "لوحة المعلومات",
        'login'     => "تسجيل الدخول",
        'macros'    => "ماكروز",
        'register'  => "إنشاء حساب",

        'user' => [
            'account'         => "حسابي",
            'administration'  => "الإدارة",
            'change_password' => "تغيير كلمة المرور",
            'my_information'  => "بياناتي",
            'profile'         => "صفحتي ",
        ],

        'privacy'     => "الخصوصية",
        'about'       => "نبذة",
        'terms'       => "الشروط",
        'advertising' => "الدعاية",
        'copyright'   => "حقوق النشر",
        'cookies'     => "ملفات تعريف الارتباط",
        'more'        => "المزيد",
        'forum'       => "المنصة",
        'newest'      => "الأحدث",
        'trending'    => "الأكثر تداولاً",
        'ask'         => "اسأل",
        'map'         => "الخريطة",
        'flights'     => "رحلات ",
        'hotels'      => "فنادق",
        'reports'     => "تقارير"

    ],
];
