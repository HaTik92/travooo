<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => "إدارة الوصول",

            'roles' => [
                'all'        => "جميع الأدوار",
                'create'     => "إنشاء دور",
                'edit'       => "تعديل الدور",
                'management' => "إدارة الدور",
                'main'       => "الأدوار",
            ],

            'users' => [
                'all'             => "جميع حسابات المستخدمين",
                'change-password' => "تغيير كلمة المرور",
                'create'          => "إنشاء حساب",
                'deactivated'     => "الحسابات المعطّلة",
                'deleted'         => "الحسابات المحذوفة",
                'edit'            => "تعديل الحسابات ",
                'main'            => "الحسابات ",
                'view'            => "عرض حساب المستخدم ",
            ],
        ],
        'langauge' => [
            'title' => "إدارة اللغة",
        ],
    
        'countries' => [
            'title' => "إدارة الدول",            
        ],

        'log-viewer' => [
            'main'      => "مستعرض السجل",
            'dashboard' => "لوحة المعلومات",
            'logs'      => "السجلات",
        ],

        'sidebar' => [
            'dashboard' => "لوحة المعلومات",
            'general'   => "عام",
            'system'    => "النظام",
        ],
    ],

    'language-picker' => [
        'language' => "اللغة",
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'ar'    => "العربية",
            'zh'    => "الصينية المبسطة",
            'zh-TW' => "الصينية التقليدية",
            'da'    => "الدنماركية",
            'de'    => "الألمانية",
            'el'    => "اليونانية",
            'en'    => "الإنجليزية",
            'es'    => "الإسبانية",
            'fr'    => "الفرنسية",
            'id'    => "الإندونيسية",
            'it'    => "الإيطالية",
            'ja'    => "اليابانية",
            'nl'    => "الهولندية",
            'pt_BR' => "البرتغالية البرازيلية",
            'ru'    => "الروسية",
            'sv'    => "السويدية",
            'th'    => "التايلندية",
        ],
    ],
];
