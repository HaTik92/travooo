<?php

return [
    'comments'        => "التعليقات",
    'count_comment'   => "{0}:عدد تعليق |[2,*] تعليقات",
    'top'             => "الأكثر تفاعلاً",
    'new'             => "حديث",
    'write_a_comment' => "اترك تعليقاً"
];