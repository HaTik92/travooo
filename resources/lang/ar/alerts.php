<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => "تم إنشاء الدور بنجاح.",
            'deleted' => "تم حذف الدور بنجاح.",
            'updated' => "تم تحديث الدور بنجاح.",
        ],

        'users' => [
            'confirmation_email'  => "تم إرسال رسالة تأكيد جديدة إلى البريد الإلكتروني في الملف.",
            'created'             => "تم إنشاء حساب المستخدم بنجاح.",
            'deleted'             => "تم حذف حساب المستخدم بنجاح.",
            'deleted_permanently' => "تم حذف حساب المستخدم بشكل مؤقت.",
            'restored'            => "تمت استعادة حساب المستخدم بنجاح.",
            'session_cleared'      => "تم مسح جلسة المستخدم بنجاح.",
            'updated'             => "تم تحديث حساب المستخدم بنجاح.",
            'updated_password'    => "تم تحديث كلمة مرور حساب المستخدم بنجاح.",
        ],

        'language' => [
            'created' => "تم إنشاء لغة جديدة.",
            'deleted' => "تم حذف اللغة بنجاح.",
            'updated' => "تم تحديث اللغة بنجاح.",
        ],
    ],
];
