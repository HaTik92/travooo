<?php

return [
    'january'   => "يناير",
    'february'  => "فبراير",
    'march'     => "مارس",
    'april'     => "أبريل",
    'may'       => "مايو",
    'june'      => "يونيو",
    'july'      => "يوليو",
    'august'    => "أغسطس",
    'september' => "سبتمبر",
    'october'   => "أكتوبر",
    'november'  => "نوفمبر",
    'december'  => "ديسمبر",

    'short' => [
        'january'   => "يناير",
        'february'  => "فبراير",
        'march'     => "مارس",
        'april'     => "أبريل",
        'may'       => "مايو",
        'june'      => "يونيو",
        'july'      => "يوليو",
        'august'    => "أغسطس",
        'september' => "سبتمبر",
        'october'   => "أكتوبر",
        'november'  => "نوفمبر",
        'december'  => "ديسمبر",
    ]
];