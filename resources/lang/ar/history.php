<?php

return [

    /*
    |--------------------------------------------------------------------------
    | History Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain strings associated to the
    | system adding lines to the history table.
    |
    */

    'backend' => [
        'none'            => "لا يوجد سجل سابق.",
        'none_for_type'   => "لا يوجد سجل لهذا النوع.",
        'none_for_entity' => "لا يوجد سجل لهذه :فئة",
        'recent_history'  => "السجل السابق",

        'roles' => [
            'created' => "أنشأ دور",
            'deleted' => "حذف الدور",
            'updated' => "حدّث الدور",
        ],
        'users' => [
            'changed_password'    => "غير كلمة مرور ",
            'created'             => "أنشأ حساب",
            'deactivated'         => "عطل الحساب",
            'deleted'             => "حذف الحساب",
            'permanently_deleted' => "حذف الحساب بشكل دائم",
            'updated'             => "حدث الحساب",
            'reactivated'         => "أعاد تفعيل الحساب",
            'restored'            => "استعاد الحساب",
        ],
    ],
];
