<?php

return [
    'places'      => "أماكن",
    'restaurants' => "مطاعم",
    'hotels'      => "فنادق",
    'countries'   => "دول",
    'cities'      => "مدن",
    'people'      => "أشخاص",

];