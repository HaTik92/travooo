<?php

return [
    'write_something'                => "اكتب شيئًا",
    'global'                         => "حول العالم",
    'friends'                        => "أصدقاء",
    'friends_tab'                    => "نافذة الأصدقاء",
    'top_places'                     => "أفضل الأماكن",
    'following_this_place'           => "متابعو هذا المكان",
    'follow'                         => "متابعة",
    'stories'                        => "قصص",
    'checked-in_at'                  => "سجل وصوله إلى",
    'share'                          => "مشاركة",
    'spread_the_word'                => "شارك في نشر الخبر",
    'add_to_favorites'               => "أضف إلى المفضلة",
    'save_it_for_later'              => "حفظ لوقت لاحق",
    'report'                         => "إبلاغ",
    'help_us_understand'             => "ساعدنا كي نفهم",
    'following_this'                 => "يتابع هذا المنشور",
    'started_following'              => "بدأ بمتابعة",
    'discover_new_destinations'      => "اكتشف وجهات جديدة",
    'discover_new_people'            => "اكتشف أشخاص جدد",
    'followers'                      => "المتابعون ",
    'discover_new_travelers'         => "اكتشف مسافرين جدد",
    'talking_about_this'             => "يتحدث عن هذا",
    'reactions'                      => "التفاعلات",
    'comments'                       => "التعليقات",
    'gave_a_rating_of'               => "أعطى تقييم لـ",
    'to'                             => "إلى",
    'added_a_comment_about_a'        => "أضاف تعليق على",
    'photo'                          => "صورة",
    'added_a'                        => "أضاف",
    'remove_this_post'               => "أزل هذا المنشور",
    'likes'                          => "الإعجابات",
    'places_you_might_like'          => "أماكن قد تعجبك",
    'when_is_the_best_time_to_visit' => "ما هو أفضل وقت للزيارة؟",
    'in'                             => "في",
    'join_the_discussion'            => "انضم إلى المحادثة",
    'updated_his'                    => "حدّث ",
    'status'                         => "الحالة",
    'view_profile'                   => "استعراض الصحفة",
    'trending_destinations'          => "الوجهات الأكثر رواجاً",
    'created_a'                      => "أنشأ",
    'trip_plan'                      => "خطة رحلة",
    'view_plan'                      => "عرض الخطة",
    'checking_on'                    => "يتفقّد",
    'restaurant'               => "مطعم",
    'shared_a'                 => "شارك",
    'destinations'             => "وجهات",
    'on'                       => "على",
    'and_will_stay'            => "وسيبقى",
    'event'                    => "حدث",
    'posted_by'                => "نشره",
    'independence_day'         => "يوم الاستقلال",
    'national_Holiday'         => "عطلة وطنية",
    'country_in'               => "دولة في",
    'videos_you_might_like'    => "فيديوهات قد تعجبك",
    'count_following_this'     => ":عدد يتابعون هذا ",
    'count_talking_about_this' => ":عدد يتحدثون عن هذا",
    'downvote'                       => 'Downvote',
    'upvote'                         => 'Upvote',
    'delete'                         => 'Delete'
];