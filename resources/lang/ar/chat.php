<?php

return [
    'add_attachment'          => "أضف مرفق",
    'messages'                => "الرسائل",
    'to'                      => "إلى",
    'from'                    => "من",
    'by'                      => "بواسطة",
    'type_a_message_here'     => "اكتب رسالة هنا",
    'write_a_message'         => "اكتب رسالة...",
    'add_content_to_the_chat' => "أضف محتوى إلى المحادثة",
    'emoticons'               => [
        'like'  => "أعجبني",
        'haha'  => "Haha",
        'wow'   => "Wow",
        'sad'   => "Sad",
        'angry' => "Angry"
    ],


    'to_dd' => "إلى:"
];