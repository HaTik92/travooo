<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Strings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in strings throughout the system.
    | Regardless where it is placed, a string can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'delete_user_confirm'  => "هل أنت متأكد أنك تريد حذف هذا المستخدم بشكل دائم؟ أي مكان في الطلب يشير إلى هوية المستخدم هذه سينتج عنه خطأ غالبًا. استمر على مسؤوليتك. لا يمكنك استرجاع هذا عند حذفه. ",
                'if_confirmed_off'     => "(إذا كان زر التأكيد مطفأ)",
                'restore_user_confirm' => "إعادة هذا المستخدم إلى حالته الأصلية؟",
            ],
        ],

        'dashboard' => [
            'title'   => "لوحة المعلومات الإدارية",
            'welcome' => "مرحباً بك",
        ],

        'general' => [
            'all_rights_reserved' => "جميع الحقوق محفوظة.",
            'are_you_sure'        => "هل أنت متأكد أنّك تريد القيام بذلك؟",
            'boilerplate_link'    => "Laravel 5 Boilerplate",
            'continue'            => "استمرار",
            'member_since'        => "عضو منذ",
            'minutes'             => " دقائق ",
            'search_placeholder'  => "بحث...",
            'timeout'             => "تم تسجيل خروج بشكل تلقائي لأسباب أمنية نتيجة عدم نشاطك خلال ",

            'see_all' => [
                'messages'      => "رؤية كل الرسائل",
                'notifications' => "عرض الكل",
                'tasks'         => "عرض كل المهام",
            ],

            'status' => [
                'online'  => "متصل",
                'offline' => "منفصل",
            ],

            'you_have' => [
                'messages'      => "{0} لا يوجد رسائل|{1} لديك 1 رسائل|[2,Inf] لديك :عدد رسائل",
                'notifications' => "{0} لا يوجد تنبيهات |{1} لديك 1 تنبيهات|[2,Inf] لديك :عدد تنبيهات",
                'tasks'         => "{0} لا يوجد مهام|{1} لديك 1 مهام|[2,Inf] لديك :عدد مهام",
            ],
        ],

        'search' => [
            'empty'      => "يرجى إدخال كلمة بحث",
            'incomplete' => "عليك كتابة منطق البحث الخاص بك لهذا النظام",
            'title'      => "نتائج البحث",
            'results'    => "نتائج بحث لـ :الاستفسار",
        ],

        'welcome' => "<p>This is the AdminLTE theme by <a href=\"https://almsaeedstudio.com/\" target=\"_blank\">https://almsaeedstudio.com/</a>. هذه نسخة أساسية تشمل الأنماط والنصوص اللازمة لتفعيل النظام. نزّل النسخة الكاملة لتبدأ بإضافة المكونات إلى لوحتك. </p>\n</p>جميع الوظائف هي للعرض فقط باستثناء<strong> إدارة الوصول<strong> إلى اليسار.  يتوفر هذا النص المعياري (boilerplate) مع تحكم وصول فعال تام لإدارة المستخدمين/الأدوار/الأذونات. </p>\n</p>يرجى الانتباه أنه لا يزال قيد التجربة، لذلك، قد يحتوي على خلل أو عيوب أخرى لم أتعرض لها بعد.  سأفعل ما بوسعي لإصلاحها بمجرد تلقيها.</p>\n</p>أتمنى أن تستمتعوا بالجهود التي بذلتها هنا. يرجى زيارة صفحة <a href=\"https://github.com/rappasoft/laravel-5-boilerplate\" target=\"_blank\">GitHub</a> لمزيد المعلومات، ويرجى الإبلاغ عن أي خلل على<a href=\"https://github.com/rappasoft/Laravel-5-Boilerplate/issues\" target=\"_blank\">issues here</a>.</p>\n<p><strong>متطلبات هذا المشروع كثيرة ويصعب استيعابها نظراً لسرعة تغير فرع master في برنامج Laravel، لذلك، أقدر أي مساعدة أحصل عليها. </strong></p>\n<p>- أنثوني رابا<p>",
    ],

    'emails' => [
        'auth' => [
            'error'                   => "Whoops!",
            'greeting'                => "Hello!",
            'regards'                 => "أفضل التحيات",
            'trouble_clicking_button' => "إذا واجهت مشكلة في الضغط على زر \":action_text\"، انسخ وألصق الـ URL أسفله في متصفح الأنترنت الذي تستخدمه",
            'thank_you_for_using_app' => "شكرًا لاستخدامك تطبيقنا!",

            'password_reset_subject'    => "تغيير كلمة المرور",
            'password_cause_of_email'   => "وصلتك هذه الرسالة لأننا تلقينا طلب منك لتغيير كلمة مرور حسابك.",
            'password_if_not_requested' => "إذا لم تطلب تغيير كلمة المرور، يرجى تجاهل هذه الرسالة.",
            'reset_password'            => "اضغط هنا لتغيير كملة المرور.",

            'click_to_confirm' => "اضغط هنا لتأكيد حسابك:",
        ],
    ],

    'frontend' => [
        'test' => "اختبار",

        'tests' => [
            'based_on' => [
                'permission' => "مبنى على الإذن - ",
                'role'       => "مبنى على الدور - ",
            ],

            'js_injected_from_controller' => "حقن شيفرة Javascript من متحكم",

            'using_blade_extensions' => "باستخدام امتدادات شفرة",

            'using_access_helper' => [
                'array_permissions'     => "باستخدام Acess Helper مع مجموعة من أسماء الأذونات أو الهويات، حيث يجب على المستخدم أن يملك كل شيء.",
                'array_permissions_not' => "باستخدام Acess Helper مع مجموعة من أسماء أو هويات الأذونات، حيث يجب على المستخدم أن يملك كل شيء.",
                'array_roles'           => "باستخدام Acess Helper مع مجموعة من أسماء أو هويات الأدوار، حيث يجب على المستخدم أن يملك كل شيء.",
                'array_roles_not'       => "باستخدام Acess Helper مع مجموعة من أسماء أو هويات الأدوار، حيث لا يجب على المستخدم أن يملك كل شيء.",
                'permission_id'         => "باستخدام Acess Helper مع هوية الإذن.",
                'permission_name'       => "باستخدام Acess Helper مه اسم الإذن.",
                'role_id'               => "باستخدام Acess Helper مع هوية الدور.",
                'role_name'             => "باستخدام Acess Helper مع اسم الدور.",
            ],

            'view_console_it_works'          => "عرض المنصة، يجب أن تظهر عبارة 'تم الأمر بنجاح' مصدرها FrontendController@index",
            'you_can_see_because'            => "يمكنك رؤية ذلك لأن لديك دور ':الدور'!",
            'you_can_see_because_permission' => "يمكنك رؤية ذلك لأن لديك إذن ':الإذن'!",
        ],

        'user' => [
            'change_email_notice'  => "إذا غيرت بريدك الإلكتروني سيتم تسجيل خروجك حتى تؤكد عنوان بريدك الإلكتروني الجديد.",
            'email_changed_notice' => "عليك تأكيد عنوان بريدك الإلكتروني الجديد قبل أن تتمكن من تسجيل دخولك مجددًا.",
            'profile_updated'      => "تم تحديث الصفحة بنجاح.",
            'password_updated'     => "تم تحديث كلمة المرور بنجاح.",
        ],

        'welcome_to' => "مرحبًا بك في :مكان",

        'copy' => "Travooo &copy; 2017",

        'share_this_page' => "شارك هذه الصفحة",

        'loading' => "جار التحميل...",

    ],
];
