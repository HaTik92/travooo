<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'roles' => [
                'already_exists'    => "هذا الدور مستعمل. يرجى اختيار اسم آخر.",
                'cant_delete_admin' => "لا يمكنك حذف دور الأدمن.",
                'create_error'      => "وقعت مشكلة أثناء إنشاء هذا الدور. يرجى المحاولة مجددًا.",
                'delete_error'      => "وقعت مشكلة أثناء حذف هذا الدور. يرجى المحاولة مجددًا.",
                'has_users'         => "لا يمكنك حذف دور يستعلمه مستخدمون آخرون.",
                'needs_permission'  => "عليك اختيار إذن واحد على الأقل لهذا الدور.",
                'not_found'         => "هذا الدور غير موجود.",
                'update_error'      => "وقعت مشكلة أثناء تحديث هذا الدور. يرجى المحاولة مجددًا.",
            ],

            'users' => [
                'cant_deactivate_self'  => "لا يمكنك القيام بذلك بنفسك.",
                'cant_delete_admin'  => "لا يمكنك حذف دور الأدمن الأعلى.",
                'cant_delete_self'      => "لا يمكنك حذف نفسك.",
                'cant_delete_own_session' => "لا يمكنك حذف جلستك.",
                'cant_restore'          => "هذا الحساب غير محذوف لذلك لا يمكن استعادته.",
                'create_error'          => "وقعت مشكلة أثناء إنشاء هذا الحساب. يرجى المحاولة مجددًا.",
                'delete_error'          => "وقعت مشكلة أثناء حذف هذا الحساب. يرجى المحاولة مجددًا.",
                'delete_first'          => "يجب حذف هذا الحساب أوّلًا قبل أن يتم التخلص منه بشكل دائم.",
                'email_error'           => "عنوان البريد الإلكتروني ينتمي لمستخدم آخر.",
                'mark_error'            => "وقعت مشكلة في تحديث هذا الحساب. يرجى المحاولة مجددًا.",
                'not_found'             => "هذا الحساب غير موجود.",
                'restore_error'         => "وقعت مشكلة أثناء استعادة هذا الحساب. يرجى المحاولة مجددًا.",
                'role_needed_create'    => "عليك اختيار دور واحد على الأقل.",
                'role_needed'           => "عليك اختيار دور واحد على الأقل.",
                'session_wrong_driver'  => "يجب ضبط مشغل جلستك على قاعدة البيانات لاستخدام هذه الميزة.",
                'update_error'          => "وقعت مشكلة أثناء تحديث هذا الحساب. يرجى المحاولة مجددًا.",
                'update_password_error' => "وقعت مشكلة في تغيير كلمة مرور هذا الحساب. يرجى المحاولة مجددًا.",
            ],

            'language' => [
                'code_error'    => "رمز اللغة موجود في قاعدة البيانات.",
                'update_error'  => "وقع خطأ غير متوقع، حاول مجددًا.",
            ],
        ],
    ],

    'frontend' => [
        'auth' => [
            'confirmation' => [
                'already_confirmed' => "سبق أن تم تأكيد حسابك.",
                'confirm'           => "أكد حسابك!",
                'created_confirm'   => "تم إنشاء حسابك بنجاح. أرسلنا لك رسالة تأكيد الحساب.",
                'mismatch'          => "رمز التأكيد الخاص بك غير صحيح.",
                'not_found'         => "رمز التأكيد الخاص بك غير موجود.",
                'resend'            => "لم يتم تأكيد حسابك. يرجى الضغط على رابط التأكيد في بريدك الإلكتروني، أو <a href=\"".route('frontend.auth.account.confirm.resend', ':user_id').'">click here</a> to resend the confirmation e-mail.',
                'success'           => "تم تأكيد حسابك بنجاح!",
                'resent'            => "تم إرسال رسالة تأكيد جديدة إلى العنوان الإلكتروني في الملف.",
            ],

            'deactivated' => "تم تعطيل حسابك.",
            'email_taken' => "هذا البريد الإلكتروني مستعمل.",

            'password' => [
                'change_mismatch' => "هذه ليست كلمة مرورك القديمة.",
            ],

            'registration_disabled' => "التسجيل مغلق حالياً.",
        ],
    ],
];
