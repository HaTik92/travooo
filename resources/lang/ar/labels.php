<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all'     => "الكل",
        'yes'     => "نعم",
        'no'      => "لا",
        'custom'  => "تخصيصأوامر",
        'actions' => "",
        'active'  => "فعال",
        'buttons' => [
            'save'   => "حفظ",
            'update' => "تحديث",
        ],
        'hide'              => "إخفاء",
        'inactive'          => "غير فعال",
        'none'              => "غير محدد",
        'show'              => "إظهار",
        'toggle_navigation' => "تبديل طريقة التصفح",
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create'     => "إنشاء دور",
                'edit'       => "تعديل الدور",
                'management' => "إدارة الدور",

                'table' => [
                    'number_of_users' => "عدد المستخدمين",
                    'permissions'     => "الأذونات",
                    'role'            => "الدور",
                    'sort'            => "تصنيف",
                    'total'           => "مجموع الدور|مجموع الأدوار",
                ],
            ],
            'language' => [
                'management' => "إدارة اللغة"
            ],
            'users' => [
                'active'              => "المستخدمون الفعالون",
                'all_permissions'     => "جميع الأذونات",
                'change_password'     => "تغيير كلمة المرور",
                'change_password_for' => "تغيير كلمة مرور :المستخدم",
                'create'              => "إنشاء مستخدم",
                'deactivated'         => "المُستخدمون المعطّلون",
                'deleted'             => "المُستخدمون المحذوفون",
                'edit'                => "تعديل المستخدم",
                'management'          => "إدارة المستخدم",
                'no_permissions'      => "لا يوجد أذونات",
                'no_roles'            => "لا يوجد أدوار لأعدادها",
                'permissions'         => "الأذونات",
                'age'                 => "العمر",
                'table' => [
                    'confirmed'      => "تم التأكيد",
                    'created'        => "تم الإنشاء",
                    'email'          => "البريد الإلكتروني",
                    'id'             => "الهوية",
                    'last_updated'   => "آخر تحديث",
                    'name'           => "الاسم",
                    'no_deactivated' => "لا يوجد مُستخدمين معطّلين",
                    'no_deleted'     => "لا يوجد مُستخدمين محذوفين",
                    'roles'          => "الأدوار",
                    'total'          => "إجمالي الحساب|إجمالي الحسابات",
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => "لمحة",
                        'history'  => "السجل",
                        'infringement' => 'انتهاك حقوق الملكية',
                        'spam'         => 'تقارير البريد العشوائي',
                        ],

                    'content' => [
                        'overview' => [
                            'avatar'       => "الأفاتار",
                            'confirmed'    => "تم التأكيد",
                            'copyright'    => "حقوق النشر",
                            'spam'         => "تقارير البريد العشوائي",
                            'created_at'   => "تم الإنشاء في",
                            'deleted_at'   => "نم الحذف في ",
                            'email'        => "البريد الإلكتروني",
                            'last_updated' => "آخر تحديث",
                            'name'         => "الاسم",
                            'status'       => "الحالة",
                        ],
                    ],
                ],

                'view' => "استعراض المستخدم",
            ],
        ],
        'languages' => [
            'languages_manager' => "إدارة اللغات",
            'languages_management'=> "إدارة اللغات"
        ],
        'levels' => [
            'levels' => "المستويات",
            'levels_manager' => "إدارة المستويات",
        ],
        'locations' => [
            'locations_manager' => "إدارة المواقع",
            'regions' => "المناطق",
            'countries' => "الدول",
            'cities' => "المدن",
            'place_types' => "أنواع المكان",
            'places' => "أماكن"
        ],
        'safety_degrees' => [
            'safety_degrees_manager' => "إدارة درجات الأمان",
            'safety_degrees' => "درجات الأمان",
        ],
        'activities' => [
            'activities_manager' => "إدارة النشاطات",
            'activity_types' => "أنواع النشاطات",
            'activities' => "النشاطات",
            'activity_media' => "وسائل إعلام النشاط",
        ],
        'interests' => [
            'interests_manager' => "إدارة الاهتمامات",
            'interests' => "الاهتمامات"
        ],
        'religions' => [
            'religions_manager' => "إدارة الأديان",
            'religions' => "الأديان"
        ],
        'lifestyles' => [
            'lifestyles_manager' => "إدارة نمط الحياة",
            'lifestyles' => "أنماط الحياة"
        ],
        'languages_spoken' => [
            'languages_spoken_manager' => "إدارة اللغات المستخدمة للتواصل",
            'languages_spoken' => "اللغات المستخدمة للتواصل"
        ],
        'timings' => [
            'timings_manager' => "إدارة الأوقات",
            'timings' => "الأوقات"
        ],
        'weekdays' => [
            'weekdays_manager' => "إدارة نهايات الأسبوع",
            'weekdays' => "نهايات الأسبوع"
        ],
        'holidays' => [
            'holidays_manager' => "إدارة العطل",
            'holidays' => "العطل"
        ],
        'hobbies' => [
            'hobbies_manager' => "إدارة الهوايات",
            'hobbies' => "الهوايات"
        ],
        'emergency_numbers' => [
            'emergency_numbers_manager' => "إدارة أرقام الطوارئ",
            'emergency_numbers' => "أرقام الطوارئ"
        ],
        'currencies' => [
            'currencies_manager' => "إدارة العملات",
            'currencies' => "العملات"
        ],
        'cultures' => [
            'cultures_manager' => "إدارة الثقافات",
            'cultures' => "الثقافات"
        ],
        'accommodations' => [
            'accommodations_manager' => "إدارة أماكن الإقامة",
            'accommodations' => "أماكن الإقامة"
        ],
        'age_ranges' => [
            'age_ranges_manager' => "إدارة مدى الأعمار",
            'age_ranges' => "مدى الأعمار"
        ],
        'cities' => [
            'cities_manager' => "إدارة المدن",
            'active_cities' => "المدن الفعالة",
        ],
        'embassies' => [
            'embassies_manager' => "إدارة السفارات",
            'active_embassies' => "السفارات الفعالة",
            'embassies' => "السفارات"
        ],
    ],

    'frontend' => [

        'auth' => [
            'login_box_title'    => "تسجيل الدخول",
            'login_button'       => "تسجيل الدخول",
            'login_with'         => "تسجيل الدخول عبر :موقع_التواصل",
            'register_box_title' => "أنشئ حساب",
            'register_button'    => "أنشئ حساب",
            'remember_me'        => "تذكرني",
        ],

        'passwords' => [
            'forgot_password'                 => "هل نسيت كلمة المرور؟",
            'reset_password_box_title'        => "تغيير كلمة المرور",
            'reset_password_button'           => "تغيير كلمة المرور",
            'send_password_reset_link_button' => "أرسل رابط تغيير كلمة المرور",
        ],

        'macros' => [
            'country' => [
                'alpha'   => "Country Alpha Codes",
                'alpha2'  => "Country Alpha 2 Codes",
                'alpha3'  => "Country Alpha 3 Codes",
                'numeric' => "الرموز العددية للدول",
            ],

            'macro_examples' => "أمثلة ماكرو",

            'state' => [
                'mexico' => "لائحة دولة المكسيك",
                'us'     => [
                    'us'       => "الولايات المتحدة الأمريكية",
                    'outlying' => "المناطق خارج الولايات المتحدة والتابعة لها",
                    'armed'    => "القوات المسلحة الأمريكية",
                ],
            ],

            'territories' => [
                'canada' => "لائحة مقاطعات ومناطق كندا",
            ],

            'timezone' => "المنطقة الزمنية",
        ],

        'user' => [
            'passwords' => [
                'change' => "تغيير كلمة المرور",
            ],

            'profile' => [
                'avatar'             => "الأفاتار",
                'created_at'         => "تم الإنشاء في",
                'edit_information'   => "تعديل البيانات",
                'email'              => "البريد الإلكتروني",
                'last_updated'       => "آخر تحديث",
                'name'               => "الاسم",
                'update_information' => "تحديث البيانات",
            ],
        ],

    ],
];
