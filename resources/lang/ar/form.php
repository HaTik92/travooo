<?php

return [
    'placeholders' => [
        'from'                => "من",
        'to'                  => "إلى",
        'travel_date'         => "تاريخ المغادرة",
        'return_date'         => "تاريخ العودة",
        'type_a_message_here' => "اكتب رسالة هنا"
    ],
    'buttons'      => [
        'search' => "بحث",
        'send'   => "إرسال"
    ]
];