<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'activate'           => "تفعيل",
                'change_password'    => "تغيير كلمة المرور",
                'clear_session'      => "مسح الجلسة ",
                'deactivate'         => "تعطيل",
                'delete_permanently' => "حذف بشكل دائم",
                'login_as'           => "تسجيل الدخول كـ :المستخدم",
                'resend_email'       => "إعادة إرسال رسالة التأكيد",
                'restore_user'       => "استعادة حساب المستخدم",
            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => "تأكيد الحساب",
            'reset_password'  => "تغيير كلمة المرور",
        ],
    ],

    'general' => [
        'cancel'   => "إلغاء",
        'continue' => "استمرار",

        'crud' => [
            'create' => "إنشاء",
            'delete' => "حذف",
            'edit'   => "تعديل",
            'update' => "تحديث",
            'view'   => "عرض",
        ],

        'save'           => "حفظ",
        'view'           => "عرض",
        'search'         => "بحث",
        'search_dots'    => "بحث...",
        'post'           => "نشر ",
        'more'           => "المزيد",
        'load_more'      => "حمل المزيد",
        'load_more_dots' => "جمل المزيد...",
        'send'           => "إرسال",
        'see_more'       => "رؤية المزيد",
        'follow'         => "متابعة",
        'copy'           => "نسخ",
        'next'           => "التالي",
        'publish'        => "نشر",
        'request'        => "طلب",
        'delete_dots'    => "حذف",
        'approve'        => "موافقة",
        'disapprove'     => "رفض",
        'invite'         => "دعوة",
        'see_all'        => "رؤية الكل",

        'more_dots' => "المزيد...",
        'unfollow'  => "إلغاء المتابعة"
    ],
];
