<?php

return [
    'h'                => "ساعة",
    'min'              => "دقيقة",
    'am'               => "صباحاً",
    'pm'               => "مساءً",
    'hours'            => "ساعات",
    'count_hours'      => "{0}:عدد ساعة|[2,*]:عدد ساعات",
    'hours_ago'        => "قبل () ساعات",
    'nights'           => "عدد الليالي",
    'all_week'         => "الأسبوع كاملاً",
    'day'              => "يوم",
    'day_value'        => "يوم :قيمة",
    'count_day'        => "{0} :عدد يوم|[2, *] :عدد أيام",
    'count_day_in'     => "{0}:عدد أيام في |[2,*]:عدد أيام في",
    'count_days'       => ":عدد أيام",
    'date'             => "التاريخ",
    'mutual_dates'     => "تواريخ مشتركة",
    'mutual_dates_dd'  => "تواريخ مشتركة:",
    'trip_start_date'  => "تاريخ بداية الرحلة",
    'trip_finish_date' => "تاريخ نهاية الرحلة",
    'time'             => "الوقت",
    'minutes'          => "دقائق ",
    'count_min'        => ":عدد دقائق",
    'week'             => [
        'monday'    => "الإثنين",
        'tuesday'   => "الثلاثاء",
        'wednesday' => "الأربعاء",
        'thursday'  => "الخميس",
        'friday'    => "الجمعة",
        'saturday'  => "السبت",
        'sunday'    => "الأحد",
        'short'     => [
            'monday'    => "الإثنين",
            'tuesday'   => "الثلاثاء",
            'wednesday' => "الأربعاء",
            'thursday'  => "الخميس",
            'friday'    => "الجمعة",
            'saturday'  => "السبت",
            'sunday'    => "الأحد"
        ]
    ],

    'count_nights' => ":عدد ليالي",
    'from_to'      => "من :من إلى :إلى"
];