<?php
return [
    'buttons' => [
        'new_question'  => "سؤال جديد",
        'post_question' => "انشر السؤال"
    ],
    'strings' => [
        'more'                                   => "المزيد",
        'asked_for'                              => "سأل عن",
        's_in'                                   => "s in",
        'at'                                     => "في",
        'action'                                 => "أمر",
        'top_recommendations_by_users'           => "أفضل الاقتراحات من المستخدمين",
        'top_tips_by_users'                      => "أفضل النصائح من المستخدمين",
        'top_planning_tips_by_users'             => "أفضل نصائح التخطيط من المستخدمين ",
        'top_answers_by_users'                   => "أفضل الإجابات من المستخدمين",
        'said'                                   => "قال",
        'no_discussions'                         => "لا يوجد نقاشات",
        'what_people_are_talking_about'           => "ما يسأل عنه الناس",
        'talking_about_this'                      => "يسألون عن هذا",
        'your_activity'                          => "نشاطك",
        'answers'                                => "إجابات",
        'suggestions'                            => "اقتراحات",
        'tips_given'                             => "النصائح المعطاة",
        'your_questions'                         => "سؤالك",
        'more_questions'                         => "أسئلة أخرى",
        'most_helpful'                           => "الأكثر مساعدة",
        'this_week'                              => "هذا الأسبوع",
        'another_action'                         => "أمر آخر",
        'something_else_here'                    => "شيء آخر هنا",
        'replies'                                => "ردود",
        'in'                                     => "في",
        'views'                                  => "مشاهدات",
        'follow'                                 => "متابعة",
        'write_a_tip'                            => "اكتب نصيحة",
        'recommendations_by_users'               => "اقتراحات من المستخدمين",
        'tips_by_users'                          => "نصائح من المستخدمين",
        'planning_tips_by_users'                 => "نصائح التخطيط من المستخدمين ",
        'answers_by_users'                       => "إجابات من المستخدمين",
        'top_first'                              => "أفضل الأول",
        'no_tips_for_now'                        => "لا يوجد نصائح الآن",
        'asking'                                 => "يبحثون عن",
        'for_recommendations'                    => "اقتراحات",
        'for_tips'                               => "نصائح",
        'about_a_trip_plan'                      => "خطة رحلة",
        'a_general_question'                     => "سؤال عام",
        'about'                                  => "عن",
        'place_city_country'                     => "مكان، مدينة، دولة",
        'trip_plan'                              => "خطة رحلة",
        'your_trip_plan'                         => "خطة رحلتك",
        'question'                               => "سؤال ",
        'write_your_question'                    => "اكتب سؤالك",
        'description'                            => "وصف",
        'write_more_details_about_your_question' => "اكتب مزيدًا من التفاصيل حول سؤالك",
    ],

    'action_number'        => "فعل :عدد",
    'no_discussions_dots'  => "لا يوجد نقاشات...",
    'count_more_questions' => ":عدد أسئلة أخرى",
    'view_all_answers'     => "أظهر جميع الإجابات",
    'write_a_tip_dots'     => "اكتب نصيحة...",
    'no_tips_for_now_dots' => "لا يوجد نصائح الآن..."
];