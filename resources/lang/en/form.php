<?php

return [
    'placeholders' => [
        'from'                => 'From',
        'to'                  => 'To',
        'travel_date'         => 'Travel date',
        'return_date'         => 'Return date',
        'type_a_message_here' => 'Type a message here'
    ],
    'buttons'      => [
        'search' => 'Search',
        'send'   => 'Send'
    ]
];