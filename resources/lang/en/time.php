<?php

return [
    'h'                => 'h',
    'min'              => 'min',
    'am'               => 'AM',
    'pm'               => 'PM',
    'hours'            => 'Hours',
    'count_hours'      => '{0}:count Hour|[2,*]:count Hours',
    'hours_ago'        => 'hours ago',
    'nights'           => 'nights',
    'all_week'         => 'All Week',
    'day'              => 'Day',
    'day_value'        => 'Day :value',
    'count_day'        => '{0} :count Day|[2, *] :count Days',
    'count_day_in'     => '{0}:count Day in|[2,*]:count Days in',
    'count_days'       => ':count Days',
    'date'             => 'Date',
    'mutual_dates'     => 'Mutual dates',
    'mutual_dates_dd'  => 'Mutual Dates:',
    'trip_start_date'  => 'Trip start date',
    'trip_finish_date' => 'Trip finish date',
    'time'             => 'Time',
    'minutes'          => 'Minutes',
    'count_min'        => ':count min',
    'week'             => [
        'monday'    => 'Monday',
        'tuesday'   => 'Tuesday',
        'wednesday' => 'Wednesday',
        'thursday'  => 'Thursday',
        'friday'    => 'Friday',
        'saturday'  => 'Saturday',
        'sunday'    => 'Sunday',
        'short'     => [
            'monday'    => 'Mon',
            'tuesday'   => 'Tue',
            'wednesday' => 'Wed',
            'thursday'  => 'Thu',
            'friday'    => 'Fri',
            'saturday'  => 'Sat',
            'sunday'    => 'Sun'
        ]
    ],

    'count_nights' => ':count nights',
    'from_to'      => 'From: :from to :to'
];