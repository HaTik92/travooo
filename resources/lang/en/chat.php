<?php

return [
    'add_attachment'          => 'Add attachment',
    'messages'                => 'Messages',
    'to'                      => 'To',
    'from'                    => 'From',
    'by'                      => 'by',
    'type_a_message_here'     => 'Type a message here',
    'write_a_message'         => 'Write a message..',
    'add_content_to_the_chat' => 'Add content to the chat',
    'emoticons'               => [
        'like'  => 'Like',
        'haha'  => 'Haha',
        'wow'   => 'Wow',
        'sad'   => 'Sad',
        'angry' => 'Angry'
    ],


    'to_dd' => 'To:'
];