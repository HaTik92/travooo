<?php

return [
    'trip_plan'                                               => '{0}Trip Plan|[0,*] Trip Plans',
    'trip_plans'                                              => 'Trip Plans',
    'trip_plan_by'                                            => 'Trip Plan by',
    'trip'                                                    => 'Trip',
    'trending_trip_plan'                                      => 'Trending Trip Plan',
    'across_the_country_road_trip'                            => 'Across the :country Road Trip',
    'talking_about_it'                                        => 'Talking about it',
    'duration'                                                => 'Duration',
    'destination'                                             => '{0}Destination|[0,*] Destinations',
    'destination_dd'                                          => '{0}Destination:|[0,*]Destinations:',
    'count_destination'                                       => '{0}:count Destination|[0,*]:count Destinations',
    'starting_dd'                                             => 'Starting:',
    'budget_dd'                                               => 'Budget:',
    'budget'                     => 'Budget',
    'duration_dd'                => 'Duration:',
    'distance'                   => 'Distance',
    'place'                      => '{0}Place|[2,*]Places',
    'count_place'                => '{0}:count Place|[2,*]:count Places',
    'my_trip_plan'               => 'My trip plan',
    'my_trip_plans'              => 'My trip plans',
    'trip_overview'              => 'Trip overview',
    'plan_trip'                  => 'Plan trip',
    'nature'                     => 'Nature',
    'food'                       => 'Food',
    'starting_date'              => 'Starting date',
    'count_are_going'            => ':count are going',
    'leave_trip_plan'            => 'Leave Trip Plan',
    'results_without_trip_plans' => 'Results without Trip Plans',
    'select_a_trip_plan'         => 'Select a Trip Plan',
    'no_destinations_added_yet'                               => 'No destinations added yet',
    'i_do_not_have_a_trip_plan_ready'                         => 'I don\'t have a Trip Plan ready',
    'trip_end_date'                                           => 'Trip end date',
    'trip_planner'                                            => 'Trip Planner',
    'send_your_version_back_to_name'                          => 'Send your version back to :name',
    'save_plan'                                               => 'Save plan',
    'will_spend'                                              => 'Will spend',
    'planning_to_stay'                                        => 'Planning to stay',
    'when_you_will_check_in'                                  => 'When you will check-in?',
    'how_much_you_are_expecting_to_spend'                     => 'How much you are expecting to spend',
    'spending_so_far'                                         => 'Spending so far',
    'how_you_will_arrive_here'                                => 'How you will arrive here?',
    'add_a_place_in_name'                                     => 'Add a Place in :name',
    'move_to_a_new_country_or_city'                           => 'Move to a new Country or City',
    'unknown_error_while_deleting_trip_plan'                  => 'Unknown error while deleting Trip Plan!',
    'you_need_to_add_at_least_one_place_to_publish_your_trip' => 'You need to add at least one Place to publish your Trip!',
    'if_you_canceled_this_trip_you_will'                      => 'If you canceled this Trip, you will lose all your progress and any setting you added!',
    'spend'                                                   => 'Spend',
    'stayed'                                                  => 'Stayed',
    'spent'                                                   => 'Spent',
    'tips'                                                    => 'Tips',
    'overview'                                                => 'Overview',
    'suggested_places_count'                                  => 'Suggested Places :count',
    'trip_plan_messages'                                      => 'Trip plan messages',
    'use_plan'                                                => 'use plan',
    'used_this_trip_plan'                                     => 'Used this trip plan',
    'see_all_tips_count'                                      => 'See all tips(:count)',
    'flying_to'                                               => 'Flying to',
    'tips_about_place'                                        => 'Tips about :place',
    'more_stories'                                            => 'More stories',
    'previous_destination'                                    => 'Previous destination',
    'book_a_tour'                                             => 'book a tour',
    'search_places_hotels_cities'                             => 'Search places, hotels, cities...',
    'suggestions'                                             => 'Suggestions',
    'what_do_you_want_to_call_your_trip'                      => 'What do you want to call your trip?',
    'please_enter_a_descriptive_name_for_your_trip'           => 'Please enter a descriptive name for your trip',
    'when_do_you_intend_to_go_on_this_trip'                   => 'When do you intend to go on this trip?',
    'privacy_options'                                         => 'Privacy options',
    'you_decide_who_can_see_your_trip_plan'                   => 'You decide who can see your Trip Plan, you can change this setting at any time.',
    'invite_people_to_organize_this_trip_plan_together'       => 'Invite people to organize this trip plan together',
    'respond_to_suggestions_by_name'                          => 'Respond to suggestions by: :name',
    'accept_suggestions'                                      => 'Accept suggestions: this will result in using this version for the Trip Plan, instead of your version.',
    'reject_suggestions'                                      => 'Reject suggestions: this will keep your Trip Plan untouched, you can still invite people to participate in it.',
    'which_city_would_you_like_to_visit'                      => 'Which city would you like to visit?',
    'what_is_your_next_destination_in'                        => 'What is your next destination in'

];