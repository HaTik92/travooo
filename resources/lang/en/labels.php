<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all'     => 'All',
        'yes'     => 'Yes',
        'no'      => 'No',
        'custom'  => 'Custom',
        'actions' => 'Actions',
        'active'  => 'Active',
        'buttons' => [
            'save'   => 'Save',
            'update' => 'Update',
        ],
        'hide'              => 'Hide',
        'inactive'          => 'Inactive',
        'none'              => 'None',
        'show'              => 'Show',
        'toggle_navigation' => 'Toggle Navigation',
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions'     => 'Permissions',
                    'role'            => 'Role',
                    'sort'            => 'Sort',
                    'total'           => 'role total|roles total',
                ],
            ],
            'language' => [
                'management' => 'Language Management'
            ],
            'users' => [
                'active'              => 'Active Users',
                'all_permissions'     => 'All Permissions',
                'change_password'     => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'create'              => 'Create User',
                'deactivated'         => 'Deactivated Users',
                'deleted'             => 'Deleted Users',
                'edit'                => 'Edit User',
                'management'          => 'User Management',
                'no_permissions'      => 'No Permissions',
                'no_roles'            => 'No Roles to set.',
                'permissions'         => 'Permissions',
                'age'                 => 'Age',
                'table' => [
                    'confirmed'      => 'Confirmed',
                    'created'        => 'Created',
                    'email'          => 'E-mail',
                    'id'             => 'ID',
                    'last_updated'   => 'Last Updated',
                    'name'           => 'Name',
                    'username'       => 'Username',
                    'status'         => 'Status',
                    'no_deactivated' => 'No Deactivated Users',
                    'no_deleted'     => 'No Deleted Users',
                    'roles'          => 'Roles',
                    'total'          => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview'     => 'Overview',
                        'history'      => 'History',
                        'infringement' => 'Copyright Infringement',
                        'spam'         => 'Spam Reports',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => 'Avatar',
                            'confirmed'    => 'Confirmed',
                            'copyright'    => 'Copyright',
                            'created_at'   => 'Created At',
                            'deleted_at'   => 'Deleted At',
                            'email'        => 'E-mail',
                            'last_updated' => 'Last Updated',
                            'name'         => 'Name',
                            'status'       => 'Status',
                            'spam'         => 'Spam Reports',
                            'confirmation_code' => 'Confirmation Code'
                        ],
                    ],
                ],

                'view' => 'View User',
            ],
        ],
        'experts' => [
            'management' => 'Experts Management',
            'edit' => 'Expert approve',
            'create' => 'Add An Expert',
            'email' => 'Email',
            'status' => 'Status',
            'approved_at' => 'Approved Date',
            'badge' => 'Badge',
            'followers' => 'Followers',
            'website' => 'Website',
            'countries' => 'Countries',
            'cities' => 'Cities',
            'places' => 'Places',
            'travelstyles' => 'Travel Styles',
            'interests' => 'Interests',
            'twitter' => 'Twitter',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'youtube' => 'Youtube',
            'link' => 'Invite Link',
            'points' => 'Points',
            'pinterest' => 'Pinterest',
            'active' => 'Active Experts',
            'approved' => 'Approved Experts',
            'not-approved' => 'Disapproved Experts',
            'pending-approval' => 'Pending Approval Experts',
            'invite-link' => 'Invite Link Type',
            'disapproved_reason' => 'Disapproved Reason',
            'invited' => 'Invited',
            'special' => 'Special Case Experts',
            'tumblr' => 'Tumblr',
            'applied' => 'Applied To Badge Experts',
            'next_badge' => 'Next Badge',
        ],
        'invite-links' => [
            'create' => 'Create Invite Link',
            'management' => 'Invite Links Management',
            'active' => 'All Invite Links',
            'table' => [
                'id' => 'Id',
                'name' => 'Name',
                'code' => 'Code',
                'points' => 'Amount of Points',
                'created' => 'Created At',
                'updated' => 'Updated At',
            ]
        ],
        'badges' => [
            'title' => 'Badges',
            'create' => 'Create badge',
            'edit' => 'Edit badge',
            'management' => 'Invite Links Management',
            'active' => 'All Invite Links',
            'table' => [
                'id' => 'Id',
                'name' => 'Name',
                'interactions' => 'Interactions',
                'followers' => 'Followers',
                'order' => 'Order',
                'created' => 'Created At',
                'updated' => 'Updated At',
            ]
        ],
        'languages' => [
            'languages_manager' => 'Languages Manager',
            'languages_management' => 'Languages Management'
        ],
        'levels' => [
            'levels' => 'Leves',
            'levels_manager' => 'Levels Manager',
        ],
        'locations' => [
            'locations_manager' => 'Locations Manager',
            'regions' => 'Regions',
            'countries' => 'Countries',
            'cities' => 'Cities',
            'place_types' => 'Place Types',
            'places' => 'Places'
        ],
        'safety_degrees' => [
            'safety_degrees_manager' => 'Safety Degrees manager',
            'safety_degrees' => 'Safety Degrees',
        ],
        'activities' => [
            'activities_manager' => 'Activities Manager',
            'activity_types' => 'Activity Types',
            'activities' => 'Activities',
            'activity_media' => 'Activity Media',
        ],
        'interests' => [
            'interests_manager' => 'Interests Manager',
            'interests' => 'Interests'
        ],
        'religions' => [
            'religions_manager' => 'Religions Manager',
            'religions' => 'Religions'
        ],
        'lifestyles' => [
            'lifestyles_manager' => 'Life Styles Manager',
            'lifestyles' => 'Life Styles'
        ],
        'languages_spoken' => [
            'languages_spoken_manager' => 'Languages Spoken Manager',
            'languages_spoken' => 'Languages Spoken'
        ],
        'timings' => [
            'timings_manager' => 'Timings Manager',
            'timings' => 'Timings'
        ],
        'weekdays' => [
            'weekdays_manager' => 'Weekdays Manager',
            'weekdays' => 'Weekdays'
        ],
        'legal' => [
            'legal_pages' => 'Legal Pages',
            'privacy_policy' => 'Privacy Policy',
            'terms_of_service' => 'Terms Of Service',
        ],
        'holidays' => [
            'holidays_manager' => 'Holidays Manager',
            'holidays' => 'Holidays'
        ],
        'hobbies' => [
            'hobbies_manager' => 'Hobbies Manager',
            'hobbies' => 'Hobbies'
        ],
        'emergency_numbers' => [
            'emergency_numbers_manager' => 'Emergency Numbers Manager',
            'emergency_numbers' => 'Emergency Numbers'
        ],
        'currencies' => [
            'currencies_manager' => 'Currencies Manager',
            'currencies' => 'Currencies'
        ],
        'cultures' => [
            'cultures_manager' => 'Cultures Manager',
            'cultures' => 'Cultures'
        ],
        'accommodations' => [
            'accommodations_manager' => 'Accommodations Manager',
            'accommodations' => 'Accommodations'
        ],
        'age_ranges' => [
            'age_ranges_manager' => 'Age Ranges Manager',
            'age_ranges' => 'Age Ranges'
        ],
        'cities' => [
            'cities_manager' => 'Cities Manager',
            'active_cities' => 'Active Cities',
        ],
        'embassies' => [
            'embassies_manager' => 'Embassies Manager',
            'active_embassies' => 'Active Embassies',
            'embassies' => 'Embassies'
        ],
        'events' => [
            'events' => 'Events',
            'events_manager' => 'Events Manager',
        ],
        'spam_reports' => [
            'spam_manager' => 'Spam Manager',
            'reports' => 'Reports',
            'fake_news' => 'Fake News',
            'harassment' => 'Harassment',
            'hate_speech' => 'Hate Speech',
            'nudity' => 'Nudity',
            'spam' => 'Spam',
            'terrorism' => 'Terrorism',
            'violence' => 'Violence',
            'other' => 'Other',
            'history' => 'History',
            'api_reports' => 'API Reports',
            'api_reports_for_review' => 'Reports For Review',
        ],
        'copyright_infringement' => [
            'post' => 'Post',
            'profile_image' => 'Profile Image',
            'travelog' => 'Travelog',
            'event' => 'Event',
            'trip_plan' => 'Trip Plan',
            'other' => 'Other',
            'copyright_owner' => 'Copyright Owner',
            'authorized_by_copyright_owner' => 'Authorized by copyright owner',
            'non_above' => 'Non above',
            'infringement' => 'Copyright Infringement',
            'suspended_accounts' => 'Suspended  Accounts',
            'infringement_history' => 'Infringement History',
            'blacklisted_authors' => 'Blacklisted Authors',
            'counter_appeal' => 'Counter Appeal',
            'post_discussion' => 'Post Discussion'
        ],
        'destinations' => [
            'create' => 'Create',
            'cities_by_nationality' => 'Cities by Nationality',
            'countries_by_nationality' => 'Countries by Nationality',
            'top_cities_by_nationality' => 'Top Cities by Nationality',
            'top_countries_by_nationality' => 'Top Countries by Nationality',
            'create_countries_by_nationality' => 'Create Countries by Nationality',
            'edit_country' => 'Edit Country',
            'edit_city' => 'Edit City',
            'nationality' => 'Nationality',
            'edit_countries_by_nationality' => 'Edit Countries by Nationality',
            'edit_cities_by_nationality' => 'Edit Cities by Nationality',
            'create_cities_by_nationality' => 'Create Cities by Nationality',
            'cities' => 'Cities',
            'countries' => 'Countries',
            'generic_top_countries' => 'Generic Top Countries',
            'generic_top_cities' => 'Generic Top Cities',
            'create_generic_top_countries' => 'Create Generic Top Countries',
            'create_generic_top_cities' => 'Create Generic Top Cities',
            'edit_generic_top_countries' => 'Edit Generic Top Countries',
            'edit_generic_top_cities' => 'Edit Generic Top Cities',
            'generic_top_places' => 'Generic Top Places',
            'places' => 'Places',
            'edit_place' => 'Edit Place',
            'create_generic_top_places' => 'Create Generic Top Places',
            'edit_generic_top_places' => 'Edit Generic Top Places',
        ],
    ],

    'frontend' => [

        'auth' => [
            'login_box_title'    => 'Login',
            'login_button'       => 'Login',
            'login_with'         => 'Login with :social_media',
            'register_box_title' => 'Register',
            'register_button'    => 'Register',
            'remember_me'        => 'Remember Me',
        ],

        'passwords' => [
            'forgot_password'                 => 'Forgot Your Password?',
            'reset_password_box_title'        => 'Reset Password',
            'reset_password_button'           => 'Reset Password',
            'send_password_reset_link_button' => 'Send Password Reset Link',
        ],

        'macros' => [
            'country' => [
                'alpha'   => 'Country Alpha Codes',
                'alpha2'  => 'Country Alpha 2 Codes',
                'alpha3'  => 'Country Alpha 3 Codes',
                'numeric' => 'Country Numeric Codes',
            ],

            'macro_examples' => 'Macro Examples',

            'state' => [
                'mexico' => 'Mexico State List',
                'us'     => [
                    'us'       => 'US States',
                    'outlying' => 'US Outlying Territories',
                    'armed'    => 'US Armed Forces',
                ],
            ],

            'territories' => [
                'canada' => 'Canada Province & Territories List',
            ],

            'timezone' => 'Timezone',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Change Password',
            ],

            'profile' => [
                'avatar'             => 'Avatar',
                'created_at'         => 'Created At',
                'edit_information'   => 'Edit Information',
                'email'              => 'E-mail',
                'last_updated'       => 'Last Updated',
                'name'               => 'Name',
                'update_information' => 'Update Information',
            ],
        ],

    ],
];
