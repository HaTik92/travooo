<?php

return [
    'places'      => 'places',
    'restaurants' => 'restaurants',
    'hotels'      => 'hotels',
    'countries'   => 'countries',
    'cities'      => 'cities',
    'people'      => 'people',

];