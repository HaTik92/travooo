<?php

return [
    'comments'        => 'Comments',
    'count_comment'   => '{0}:count Comment|[2,*] Comments',
    'top'             => 'Top',
    'new'             => 'New',
    'write_a_comment' => 'Write a comment',
    'write_a_reply'   => 'Write a reply'
];