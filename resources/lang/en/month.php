<?php

return [
    'january'   => 'January',
    'february'  => 'February',
    'march'     => 'March',
    'april'     => 'April',
    'may'       => 'May',
    'june'      => 'June',
    'july'      => 'July',
    'august'    => 'August',
    'september' => 'September',
    'october'   => 'October',
    'november'  => 'November',
    'december'  => 'December',

    'short' => [
        'january'   => 'Jan',
        'february'  => 'Feb',
        'march'     => 'Mar',
        'april'     => 'Apr',
        'may'       => 'May',
        'june'      => 'Jun',
        'july'      => 'Jul',
        'august'    => 'Aug',
        'september' => 'Sep',
        'october'   => 'Oct',
        'november'  => 'Nov',
        'december'  => 'Dec',
    ]
];