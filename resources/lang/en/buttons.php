<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'activate'           => 'Activate',
                'change_password'    => 'Change Password',
                'clear_session'      => 'Clear Session',
                'deactivate'         => 'Deactivate',
                'delete_permanently' => 'Delete Permanently',
                'login_as'           => 'Login As :user',
                'resend_email'       => 'Resend Confirmation E-mail',
                'restore_user'       => 'Restore User',
            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => 'Confirm Account',
            'reset_password'  => 'Reset Password',
        ],
    ],

    'general' => [
        'cancel'   => 'Cancel',
        'crop'     => 'Crop',
        'continue' => 'Continue',

        'crud' => [
            'create' => 'Create',
            'delete' => 'Delete',
            'edit'   => 'Edit',
            'update' => 'Update',
            'view'   => 'View',
            'approve' => 'Approve',
            'disapprove' => 'Disapprove',
            'apply' => 'Apply',
            'disapply' => 'Disapply',
            'show_disapprove_reason' => 'Show disapprove reason',
        ],

        'save'           => 'Save',
        'view'           => 'View',
        'search'         => 'Search',
        'search_dots'    => 'Search...',
        'post'           => 'POST',
        'more'           => 'More',
        'load_more'      => 'Load more',
        'load_more_dots' => 'Load more...',
        'send'           => 'Send',
        'see_more'       => 'See More',
        'follow'         => 'Follow',
        'copy'           => 'Copy',
        'next'           => 'Next',
        'publish'        => 'Publish',
        'request'        => 'Request',
        'delete_dots'    => 'Delete...',
        'approve'        => 'Approve',
        'disapprove'     => 'Disapprove',
        'invite'         => 'Invite',
        'see_all'        => 'See All',

        'more_dots' => 'More...',
        'unfollow'  => 'UnFollow'
    ],
];
