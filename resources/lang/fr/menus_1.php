<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => "Gestion de l'accès",

            'roles' => [
                'all'        => "Tous les rôles",
                'create'     => "Créer un rôle",
                'edit'       => "Modifier le rôle",
                'management' => "Gestion des rôles",
                'main'       => "Rôles",
            ],

            'users' => [
                'all'             => "Tous les utilisateurs",
                'change-password' => "Changer le mot de passe",
                'create'          => "Créer un utilisateur",
                'deactivated'     => "Utilisateurs désactivés",
                'deleted'         => "Utilisateurs supprimés",
                'edit'            => "Modifier l'utilisateur",
                'main'            => "Utilisateur",
                'view'            => "Voir l'utilisateur",
            ],
        ],
        'langauge' => [
            'title' => "Gestion des langues",
        ],
    
        'countries' => [
            'title' => "Gestion des pays",            
        ],

        'log-viewer' => [
            'main'      => "Visualiseur de fichiers",
            'dashboard' => "Tableau de bord",
            'logs'      => "Fichiers",
        ],

        'sidebar' => [
            'dashboard' => "Tableau de bord",
            'general'   => "Généralités",
            'system'    => "Système",
        ],
    ],

    'language-picker' => [
        'language' => "Langue",
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'ar'    => "Arabe",
            'zh'    => "Chinois simplifié",
            'zh-TW' => "Chinois Traditionnel",
            'da'    => "Danois",
            'de'    => "Allemand",
            'el'    => "Grec",
            'en'    => "Anglais",
            'es'    => "Espagnol",
            'fr'    => "Français",
            'id'    => "Indonésien",
            'it'    => "Italien",
            'ja'    => "Japonnais",
            'nl'    => "Néerlandais",
            'pt_BR' => "Portugais du Brésil",
            'ru'    => "Russe",
            'sv'    => "Suédois",
            'th'    => "Thaïlandais",
        ],
    ],
];
