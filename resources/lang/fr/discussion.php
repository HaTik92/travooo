<?php
return [
    'buttons' => [
        'new_question'  => "Nouvelle question",
        'post_question' => "Poster une question"
    ],
    'strings' => [
        'more'                                   => "plus",
        'asked_for'                              => "Demandé pour",
        's_in'                                   => "dans",
        'at'                                     => "à",
        'action'                                 => "Action",
        'top_recommendations_by_users'           => "Principales recommandations des utilisateurs",
        'top_tips_by_users'                      => "Les meilleurs conseils des utilisateurs",
        'top_planning_tips_by_users'             => "Conseils de planification par les utilisateurs",
        'top_answers_by_users'                   => "Principales réponses des utilisateurs",
        'said'                                   => "mentionné",
        'no_discussions'                         => "Aucune discussion",
        'what_people_are_talking_about'           => "Ce que les gens demandent à propos de",
        'talking_about_this'                      => "En posant des questions à ce sujet",
        'your_activity'                          => "Votre activité",
        'answers'                                => "Réponses",
        'suggestions'                            => "Suggestions",
        'tips_given'                             => "Trucs et astuces donnés",
        'your_questions'                         => "Vos questions",
        'more_questions'                         => "questions supplémentaires",
        'most_helpful'                           => "Très utile",
        'this_week'                              => "Cette semaine",
        'another_action'                         => "Autre action",
        'something_else_here'                    => "Il y a autre chose ici",
        'replies'                                => "Réponses",
        'in'                                     => "à",
        'views'                                  => "Vues",
        'follow'                                 => "Suivre",
        'write_a_tip'                            => "Ecrire une astuce",
        'recommendations_by_users'               => "Recommandations des utilisateurs",
        'tips_by_users'                          => "Astuces des utilisateurs",
        'planning_tips_by_users'                 => "Conseils de planification par les utilisateurs",
        'answers_by_users'                       => "Réponses des utilisateurs",
        'top_first'                              => "Le top d'abord",
        'no_tips_for_now'                        => "Pas de conseils pour l'instant",
        'asking'                                 => "Demande",
        'for_recommendations'                    => "Pour les recommandations",
        'for_tips'                               => "Pour des conseils",
        'about_a_trip_plan'                      => "À propos d'un plan de voyage",
        'a_general_question'                     => "Une question d'ordre général",
        'about'                                  => "À propos de",
        'place_city_country'                     => "Lieu, Ville, Pays",
        'trip_plan'                              => "Plan de voyage",
        'your_trip_plan'                         => "Votre plan de voyage",
        'question'                               => "Question",
        'write_your_question'                    => "Posez votre question par écrit",
        'description'                            => "Description",
        'write_more_details_about_your_question' => "Ecrivez plus de détails sur votre question",
    ],

    'action_number'        => "Action :numéro",
    'no_discussions_dots'  => "Aucune discussion...",
    'count_more_questions' => ":nombre questions supplémentaires",
    'view_all_answers'     => "Voir toutes les réponses",
    'write_a_tip_dots'     => "Ecrire une astuce...",
    'no_tips_for_now_dots' => "Pas de conseils pour l'instant..."
];