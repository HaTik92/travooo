<?php

return [
    'places'      => "lieux",
    'restaurants' => "restaurants",
    'hotels'      => "hôtels",
    'countries'   => "pays",
    'cities'      => "villes",
    'people'      => "personnes",

];