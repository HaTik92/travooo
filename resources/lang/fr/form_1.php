<?php

return [
    'placeholders' => [
        'from'                => "De",
        'to'                  => "À",
        'travel_date'         => "Date du voyage",
        'return_date'         => "Date de retour",
        'type_a_message_here' => "Saisissez un message ici"
    ],
    'buttons'      => [
        'search' => "Rechercher",
        'send'   => "Envoyer"
    ]
];