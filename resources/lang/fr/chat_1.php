<?php

return [
    'add_attachment'          => "Ajouter une pièce jointe",
    'messages'                => "Messages",
    'to'                      => "À",
    'from'                    => "De",
    'by'                      => "par",
    'type_a_message_here'     => "Saisissez un message ici",
    'write_a_message'         => "Ecrire un message...",
    'add_content_to_the_chat' => "Ajouter du contenu à la discussion",
    'emoticons'               => [
        'like'  => "Aime",
        'haha'  => "Haha",
        'wow'   => "Waouh",
        'sad'   => "Triste",
        'angry' => "En colère"
    ],


    'to_dd' => "À :"
];