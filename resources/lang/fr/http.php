<?php

return [

    /*
    |--------------------------------------------------------------------------
    | HTTP Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the views/errors files.
    |
    */

    '404' => [
        'title'       => "Page introuvable",
        'description' => "Désolé, mais la page que vous essayiez de voir n'existe pas.",
    ],

    '503' => [
        'title'       => "Je reviens tout de suite.",
        'description' => "Je reviens tout de suite.",
    ],
];
