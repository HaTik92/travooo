<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => "L'attribut :doit être accepté.",
    'active_url'           => "L'attribut :n'est pas une URL valide.",
    'after'                => "L'attribut :doit être une date postérieure à la date :date.",
    'after_or_equal'       => "L'attribut :doit être une date postérieure ou égale à la date :date.",
    'alpha'                => "L'attribut :ne peut contenir que des lettres.",
    'alpha_dash'           => "L'attribut :ne peut contenir que des lettres, des chiffres et des tirets.",
    'alpha_num'            => "L'attribut :ne peut contenir que des lettres et des chiffres.",
    'array'                => "L'attribut :doit être un tableau.",
    'before'               => "L'attribut :doit être une date antérieure à la date :date.",
    'before_or_equal'      => "L'attribut :doit être une date antérieure ou égale à la date :date.",
    'between'              => [
        'numeric' => "L'attribut :doit être compris entre :min et :max.",
        'file'    => "L'attribut :doit être compris entre :min et :max kilo-octets.",
        'string'  => "L'attribut :doit être compris entre :min et :max caractères.",
        'array'   => "L'attribut :doit avoir entre :min et :max éléments.",
    ],
    'boolean'              => "Le champ :attribut doit être vrai ou faux.",
    'confirmed'            => "La confirmation de l'attribut :ne correspond pas.",
    'date'                 => "L'attribut :n'est pas une date valide.",
    'date_format'          => "L'attribut :ne correspond pas au format :format.",
    'different'            => "L'attribut :et :autre doivent être différents.",
    'digits'               => "L'attribut :doit être un :chiffre chiffres.",
    'digits_between'       => "L'attribut :doit être compris entre :min et :max chiffres.",
    'dimensions'           => "L'attribut: a des dimensions d'image non valides.",
    'distinct'             => "Le champ :attribut a une valeur en double.",
    'email'                => "L'attribut :doit être une adresse email valide.",
    'exists'               => "L'attribut :sélectionné n'est pas valide.",
    'file'                 => "L'attribut :doit être un fichier.",
    'filled'               => "Le champ :attribut doit avoir une valeur.",
    'image'                => "L'attribut :doit être une image.",
    'in'                   => "L'attribut :sélectionné n'est pas valide.",
    'in_array'             => "Le champ :attribut n'existe pas dans :autre.",
    'integer'              => "L'attribut :doit être un entier.",
    'ip'                   => "L'attribut :doit être une adresse IP valide.",
    'json'                 => "L'attribut :doit être une chaîne JSON valide.",
    'max'                  => [
        'numeric' => "L'attribut :ne doit pas être supérieur à :max.",
        'file'    => "L'attribut :ne doit pas être supérieur à :max kilo-octets.",
        'string'  => "L'attribut :ne doit pas être supérieur à :max caractères.",
        'array'   => "L'attribut :ne peut pas avoir plus de :max éléments.",
    ],
    'mimes'                => "L'attribut :doit être un fichier de type: :valeurs.",
    'mimetypes'            => "L'attribut :doit être un fichier de type: :valeurs.",
    'min'                  => [
        'numeric' => "L'attribut :doit être au moins :min.",
        'file'    => "L'attribut :doit être d'au moins :min kilo-octets.",
        'string'  => "L'attribut :doit être au moins :min caractères.",
        'array'   => "L'attribut :doit avoir au moins :min éléments.",
    ],
    'not_in'               => "L'attribut :sélectionné n'est pas valide.",
    'numeric'              => "L'attribut :doit être un nombre.",
    'present'              => "Le champ :attribut doit être présent.",
    'regex'                => "Le format :de l’attribut n'est pas valide.",
    'required'             => "Le champ de l'attribut :est obligatoire.",
    'required_if'          => "Le champ de l'attribut :est requis lorsque :autre est :valeur.",
    'required_unless'      => "Le champ :attribut est obligatoire sauf si :autre est compris dans :valeurs.",
    'required_with'        => "Le champ de l'attribut :est obligatoire lorsque valeurs :existe.",
    'required_with_all'    => "Le champ de l'attribut :est obligatoire lorsque valeurs :existe.",
    'required_without'     => "Le champ d'attribut :est requis lorsqu'aucune des valeurs suivantes :n'est présente.",
    'required_without_all' => "Le champ d'attribut :est requis lorsqu'aucune des valeurs de :n'est présente.",
    'same'                 => "Les attributs :et :autre doivent correspondre.",
    'size'                 => [
        'numeric' => "L'attribut :doit être :taille.",
        'file'    => "L'attribut :doit être :taille kilo-octets.",
        'string'  => "L'attribut :doit être des caractères de:taille.",
        'array'   => "L'attribut :doit contenir :les éléments de taille.",
    ],
    'string'               => "L'attribut :doit être une chaîne.",
    'timezone'             => "L'attribut :doit être une zone valide.",
    'unique'               => "L'attribut :a déjà été saisi.",
    'uploaded'             => "L'attribut :n'a pas pu être téléchargé.",
    'url'                  => "Le format :de l’attribut n'est pas valide.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => "message personnalisé",
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [

        'backend' => [
            'access' => [
                'permissions' => [
                    'associated_roles' => "Rôles connexes",
                    'dependencies'     => "Dépendances",
                    'display_name'     => "Nom d'affichage",
                    'group'            => "Groupe",
                    'group_sort'       => "Trier par groupe",

                    'groups' => [
                        'name' => "Nom du groupe",
                    ],

                    'name'   => "Nom",
                    'system' => "Système ?",
                ],

                'roles' => [
                    'associated_permissions' => "Permissions connexes",
                    'name'                   => "Nom",
                    'sort'                   => "Trier",
                ],

                'users' => [
                    'active'                  => "Actif",
                    'associated_roles'        => "Rôles connexes",
                    'confirmed'               => "Confirmé",
                    'email'                   => "Adresse e-mail",
                    'name'                    => "Nom",
                    'other_permissions'       => "Autres autorisations",
                    'password'                => "Mot de passe",
                    'password_confirmation'   => "Confirmation du mot de passe",
                    'send_confirmation_email' => "Envoyer un e-mail de confirmation",
                    'age'                     => "Âge",
                    'address'                 => "Adresse",
                    'gender'                  => "Sexe",
                    'single'                  => "Situation de la relation",
                    'children'                => "Enfants",
                    'mobile'                  => "N° de téléphone mobile",
                    'nationality'             => "Nationalité",
                    'public_profile'          => "Statut du profil",
                    'notifications'           => "Notifications",
                    'messages'                => "Messages",
                    'username'                => "Nom d'utilisateur",
                    'travel_type'             => "Type de voyage",
                    'profile_picture'         => "Photo de profil",
                    'sms'                     => "Notification par SMS",
                ],
                'languages' => [
                    'title' => "Nom de la langue",
                    'code'  => "Code de la langue",
                    'active' => "Actif",
                ],
            ],
        ],

        'frontend' => [
            'email'                     => "Adresse e-mail",
            'name'                      => "Nom",
            'password'                  => "Mot de passe",
            'password_confirmation'     => "Confirmation du mot de passe",
            'old_password'              => "Ancien mot de passe",
            'new_password'              => "Nouveau mot de passe",
            'new_password_confirmation' => "Confirmation du nouveau mot de passe",
        ],
    ],

];
