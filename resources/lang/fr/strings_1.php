<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Strings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in strings throughout the system.
    | Regardless where it is placed, a string can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'delete_user_confirm'  => "Êtes-vous sûr de vouloir supprimer définitivement cet utilisateur ? Toute erreur dans l’application faisant référence à l’identifiant de cet utilisateur sera la plus probable. Procédez à vos risques et périls. Ça ne peut pas être annulé.",
                'if_confirmed_off'     => "( Si confirmé est désactivé )",
                'restore_user_confirm' => "Restaurer cet utilisateur à son état d'origine ?",
            ],
        ],

        'dashboard' => [
            'title'   => "Tableau de bord administratif",
            'welcome' => "Bienvenue",
        ],

        'general' => [
            'all_rights_reserved' => "Tous droits réservés.",
            'are_you_sure'        => "Etes-vous sûr de vouloir faire ça ?",
            'boilerplate_link'    => "système standard Laravel 5",
            'continue'            => "Continuer",
            'member_since'        => "Membre depuis",
            'minutes'             => " minutes",
            'search_placeholder'  => "Rechercher...",
            'timeout'             => "Vous avez été automatiquement déconnecté pour des raisons de sécurité car vous n'aviez aucune activité dans ",

            'see_all' => [
                'messages'      => "Voir tous les messages",
                'notifications' => "Voir tout",
                'tasks'         => "Voir toutes les tâches",
            ],

            'status' => [
                'online'  => "Online",
                'offline' => "Hors connexion",
            ],

            'you_have' => [
                'messages'      => "{0} Vous n'avez pas de messages|\\{\\1\\} Vous avez 1 nouveau message|[2,Inf] Vous avez :nombre de messages",
                'notifications' => "{0} Vous n'avez pas de notifications|\\{\\1\\} Vous avez 1 notification|[2,Inf] Vous avez :nombre de notifications",
                'tasks'         => "{0} Vous n'avez pas de tâches|\\{\\1\\} Vous avez 1 tâche|[2,Inf] Vous avez :nombre de tâches",
            ],
        ],

        'search' => [
            'empty'      => "Veuillez entrer un terme de recherche.",
            'incomplete' => "Vous devez écrire votre propre logique de recherche pour ce système.",
            'title'      => "Résultats de la recherche",
            'results'    => "Résultats de la recherche pour :requête",
        ],

        'welcome' => "<p>Il s'agit du thème AdminLTE de <a href=\"https://almsaeedstudio.com/\" target=\"_blank\">https://almsaeedstudio.com/</a>. Il s’agit d’une version simplifiée ne contenant que les styles et les scripts nécessaires à son exécution. Téléchargez la version complète pour commencer à ajouter des composants à votre tableau de bord.</p>\n<p>Toutes les fonctionnalités sont destinées à l'affichage, à l'exception du <strong>Gestion des accès </strong> situé à gauche. Ce système standard est livré avec une bibliothèque de contrôle d’accès entièrement fonctionnelle permettant de gérer les utilisateurs/rôles/autorisations.</p>\n<p>N'oubliez pas qu'il s'agit d'un travail en cours et qu'il peut y avoir des bugs ou d'autres problèmes que je n'ai pas rencontrés. Je ferai de mon mieux pour les réparer au fur et à mesure que je les recevrai.</p>\n<p>J'espère que vous apprécierez tout le travail que j'y ai mis. Pour plus d'informations, veuillez consulter la page <a href=\"https://github.com/rappasoft/laravel-5-boilerplate\" target=\"_blank\">GitHub</a> et signaler tout problème <a href=\"https://github.com/rappasoft/Laravel-5-Boilerplate/issues\" target=\"_blank\"> ici</a>.</p>\n<p><strong>Ce projet est très difficile à suivre compte tenu du rythme auquel la branche principale de Laravel change, toute aide est donc appréciée.</strong></p>\n<p>- Anthony Rappa</p>",
    ],

    'emails' => [
        'auth' => [
            'error'                   => "Oups !",
            'greeting'                => "Bonjour !",
            'regards'                 => "Salutations,",
            'trouble_clicking_button' => "Si vous ne parvenez pas à cliquer sur le bouton \": action_text\", copiez et collez l’URL ci-dessous dans votre navigateur Web :",
            'thank_you_for_using_app' => "Merci d'utiliser notre application !",

            'password_reset_subject'    => "Réinitialiser le mot de passe",
            'password_cause_of_email'   => "Vous recevez cet email car nous avons reçu une demande de réinitialisation du mot de passe pour votre compte.",
            'password_if_not_requested' => "Si vous n'avez pas demandé de réinitialisation de mot de passe, aucune autre action n'est requise.",
            'reset_password'            => "Cliquez ici pour réinitialiser votre mot de passe",

            'click_to_confirm' => "Cliquez ici pour confirmer votre compte :",
        ],
    ],

    'frontend' => [
        'test' => "Test",

        'tests' => [
            'based_on' => [
                'permission' => "Basé sur la permission - ",
                'role'       => "Basé sur les rôles - ",
            ],

            'js_injected_from_controller' => "Javascript injecté à partir d'un contrôleur",

            'using_blade_extensions' => "Utilisation des extensions de lame",

            'using_access_helper' => [
                'array_permissions'     => "Utilisation d'Access Helper avec un tableau de noms de permission ou d'identifiants où l'utilisateur doit tout posséder.",
                'array_permissions_not' => "Utilisation d'Access Helper avec un tableau de noms de permission ou d'identifiants pour lesquels l'utilisateur ne doit pas nécessairement tout posséder.",
                'array_roles'           => "Utilisation d'Access Helper avec un tableau de noms de rôle ou d'identifiants où l'utilisateur doit tout posséder.",
                'array_roles_not'       => "Utilisation d'Access Helper avec tableau de noms de rôles ou d'ID où l'utilisateur n'a pas besoin de tout posséder.",
                'permission_id'         => "Utilisation d'Access Helper avec un ID de permission",
                'permission_name'       => "Utilisation d’Access Helper avec nom d'autorisation",
                'role_id'               => "Utilisation d'Access Helper avec l'ID de rôle",
                'role_name'             => "Utilisation de Access Helper avec nom de rôle",
            ],

            'view_console_it_works'          => "Afficher la console, vous devriez voir 'ça marche !' qui vient de FrontendController@index",
            'you_can_see_because'            => "Vous pouvez voir cela parce que vous avez le rôle de ':rôle' !",
            'you_can_see_because_permission' => "Vous pouvez voir cela parce que vous avez la permission de ':permission' !",
        ],

        'user' => [
            'change_email_notice'  => "Si vous modifiez votre adresse e-mail, vous serez déconnecté jusqu'à ce que vous confirmiez votre nouvelle adresse e-mail.",
            'email_changed_notice' => "Vous devez confirmer votre nouvelle adresse e-mail avant de pouvoir vous reconnecter.",
            'profile_updated'      => "Profil mis à jour avec succès.",
            'password_updated'     => "Mot de passe mis à jour avec succès.",
        ],

        'welcome_to' => "Bienvenue à :lieu",

        'copy' => "Travooo & copy ; 2017",

        'share_this_page' => "Partager cette page",

        'loading' => "Chargement",

    ],
];
