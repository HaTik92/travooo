<?php

return [
    'h'                => "h",
    'min'              => "min",
    'am'               => "ante meridiem",
    'pm'               => "post meridiem",
    'hours'            => "Heures",
    'count_hours'      => "{0}:nombre d'heures|[2,*]:nombre d'heures",
    'hours_ago'        => "il y a quelques heures",
    'nights'           => "nuits",
    'all_week'         => "Toute la semaine",
    'day'              => "Jour",
    'day_value'        => "Jour :valeur",
    'count_day'        => "{0} :nombre de jour|[2,*] :nombre de jours",
    'count_day_in'     => "{0}:nombre de jour en|[2,*]:nombre de jours en",
    'count_days'       => ":nombre de jours",
    'date'             => "Date",
    'mutual_dates'     => "Dates mutuelles",
    'mutual_dates_dd'  => "Dates mutuelles:",
    'trip_start_date'  => "Date de début du voyage",
    'trip_finish_date' => "Date de fin du voyage",
    'time'             => "Heure",
    'minutes'          => "Minutes",
    'count_min'        => ":nombre min",
    'week'             => [
        'monday'    => "Lundi",
        'tuesday'   => "Mardi",
        'wednesday' => "Mercredi",
        'thursday'  => "Jeudi",
        'friday'    => "Vendredi",
        'saturday'  => "Samedi",
        'sunday'    => "Dimanche",
        'short'     => [
            'monday'    => "Lun",
            'tuesday'   => "Mar",
            'wednesday' => "Mer",
            'thursday'  => "Jeu",
            'friday'    => "Ven",
            'saturday'  => "Sam",
            'sunday'    => "Dim"
        ]
    ],

    'count_nights' => ":nombre nuits",
    'from_to'      => "De: :e à :à"
];