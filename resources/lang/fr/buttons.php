<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'activate'           => "Activer",
                'change_password'    => "Changer le mot de passe",
                'clear_session'      => "Désactiver la session",
                'deactivate'         => "Désactiver",
                'delete_permanently' => "Supprimer de façon permanente",
                'login_as'           => "Se connecter en tant que :utilisateur",
                'resend_email'       => "Renvoyer l'e-mail de confirmation",
                'restore_user'       => "Restaurer l'utilisateur",
            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => "Confirmer le compte",
            'reset_password'  => "Réinitialiser le mot de passe",
        ],
    ],

    'general' => [
        'cancel'   => "Annuler",
        'continue' => "Continuer",

        'crud' => [
            'create' => "Créer",
            'delete' => "Supprimer",
            'edit'   => "Modifier",
            'update' => "Mise à jour",
            'view'   => "Afficher",
        ],

        'save'           => "Enregistrer",
        'view'           => "Afficher",
        'search'         => "Rechercher",
        'search_dots'    => "Rechercher...",
        'post'           => "Publication",
        'more'           => "Plus",
        'load_more'      => "Charger plus",
        'load_more_dots' => "Charger plus...",
        'send'           => "Envoyer",
        'see_more'       => "Voir plus",
        'follow'         => "Suivre",
        'copy'           => "Copier",
        'next'           => "Suivant",
        'publish'        => "Publier",
        'request'        => "Demande",
        'delete_dots'    => "Supprimer...",
        'approve'        => "Approuver",
        'disapprove'     => "Ne pas approuver",
        'invite'         => "Inviter",
        'see_all'        => "Tout voir",

        'more_dots' => "Plus...",
        'unfollow'  => "Se désabonner"
    ],
];
