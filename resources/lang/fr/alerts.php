<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => "Le rôle a été créé avec succès.",
            'deleted' => "Le rôle a été supprimé avec succès.",
            'updated' => "Le rôle a été mis à jour avec succès.",
        ],

        'users' => [
            'confirmation_email'  => "Un nouvel e-mail de confirmation a été envoyé à l'adresse enregistrée.",
            'created'             => "Le compte de l’utilisateur a été créé avec succès.",
            'deleted'             => "Le compte de l’utilisateur a été supprimé avec succès.",
            'deleted_permanently' => "L'utilisateur a été supprimé définitivement.",
            'restored'            => "Le compte de l’utilisateur a été restauré avec succès.",
            'session_cleared'      => "La session de l'utilisateur a été effacée avec succès.",
            'updated'             => "Le compte de l’utilisateur a été mis à jour avec succès.",
            'updated_password'    => "Le mot de passe de l’utilisateur a été mis à jour avec succès.",
        ],

        'language' => [
            'created' => "Nouvelle langue créée",
            'deleted' => "Langue supprimée avec succès",
            'updated' => "Langue mise à jour avec succès",
        ],
    ],
];
