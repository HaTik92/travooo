<?php

return [
    'january'   => "Janvier",
    'february'  => "Février",
    'march'     => "Mars",
    'april'     => "Avril",
    'may'       => "Mai",
    'june'      => "Juin",
    'july'      => "Juillet",
    'august'    => "Août",
    'september' => "Septembre",
    'october'   => "Octobre",
    'november'  => "Novembre",
    'december'  => "Décembre",

    'short' => [
        'january'   => "Jan",
        'february'  => "Fév",
        'march'     => "Mar",
        'april'     => "Avr",
        'may'       => "Mai",
        'june'      => "Juin",
        'july'      => "Juil",
        'august'    => "Août",
        'september' => "Sept",
        'october'   => "Oct",
        'november'  => "Nov",
        'december'  => "Déc",
    ]
];