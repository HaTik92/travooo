<?php

return [
    'comments'        => "Commentaires",
    'count_comment'   => "{0}:nombre Commentaire|[2,*] Commentaires",
    'top'             => "Haut",
    'new'             => "Nouveau",
    'write_a_comment' => "Ecrire un commentaire"
];