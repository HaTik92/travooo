<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'   => "Accueil",
        'logout' => "Se déconnecter",
    ],

    'frontend' => [
        'dashboard' => "Tableau de bord",
        'login'     => "Se connecter",
        'macros'    => "Macros",
        'register'  => "S'inscrire",

        'user' => [
            'account'         => "Mon compte",
            'administration'  => "Administration",
            'change_password' => "Changer le mot de passe",
            'my_information'  => "Mes données personnelles",
            'profile'         => "Profil",
        ],

        'privacy'     => "Confidentialité",
        'about'       => "À propos de",
        'terms'       => "Termes et conditions",
        'advertising' => "Publicité",
        'cookies'     => "Cookies",
        'more'        => "Plus",
        'forum'       => "Forum",
        'newest'      => "Le plus récent",
        'trending'    => "Tendances",
        'ask'         => "Demander",
        'map'         => "Carte",
        'flights'     => "Vols",
        'hotels'      => "Hotels",
        'reports'     => "Rapports"

    ],
];
