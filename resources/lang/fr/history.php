<?php

return [

    /*
    |--------------------------------------------------------------------------
    | History Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain strings associated to the
    | system adding lines to the history table.
    |
    */

    'backend' => [
        'none'            => "Il n'y a pas d'historique récente.",
        'none_for_type'   => "Il n'y a pas d'historique pour ce type.",
        'none_for_entity' => "Il n'y a pas d'historique pour cette :entité.",
        'recent_history'  => "Historique récent",

        'roles' => [
            'created' => "rôle créé",
            'deleted' => "rôle supprimé",
            'updated' => "rôle actualisé",
        ],
        'users' => [
            'changed_password'    => "mot de passe changé pour l'utilisateur",
            'created'             => "utilisateur créé",
            'deactivated'         => "utilisateurs désactivés",
            'deleted'             => "utilisateurs supprimés",
            'permanently_deleted' => "utilisateur supprimé définitivement",
            'updated'             => "utilisateur actualisé",
            'reactivated'         => "utilisateur réactivé",
            'restored'            => "utilisateur restauré",
        ],
    ],
];
