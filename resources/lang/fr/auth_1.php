<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'        => "Ces informations d'identification ne correspondent pas à nos enregistrements.",
    'general_error' => "Vous n'y avez pas accès.",
    'socialite'     => [
        'unacceptable' => ":fournisseur n'est pas un type de connexion acceptable.",
    ],
    'throttle' => "Trop de tentatives de connexion. Veuillez réessayer dans :secondes secondes.",
    'unknown'  => "Une erreur inconnue s'est produite",
];
