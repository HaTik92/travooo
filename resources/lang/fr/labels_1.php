<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all'     => "Tout",
        'yes'     => "Oui",
        'no'      => "Non",
        'custom'  => "Personnalisé",
        'actions' => "Actions",
        'active'  => "Actif",
        'buttons' => [
            'save'   => "Enregistrer",
            'update' => "Mise à jour",
        ],
        'hide'              => "Masquer",
        'inactive'          => "Inactif",
        'none'              => "Aucun",
        'show'              => "Montrer",
        'toggle_navigation' => "Basculer la navigation",
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create'     => "Créer un rôle",
                'edit'       => "Modifier le rôle",
                'management' => "Gestion des rôles",

                'table' => [
                    'number_of_users' => "Nombre d'utilisateurs",
                    'permissions'     => "Autorisations",
                    'role'            => "Rôle",
                    'sort'            => "Trier",
                    'total'           => "total de rôles|total de rôles",
                ],
            ],
            'language' => [
                'management' => "Gestion des langues"
            ],
            'users' => [
                'active'              => "Utilisateurs actifs",
                'all_permissions'     => "Toutes les autorisations",
                'change_password'     => "Changer le mot de passe",
                'change_password_for' => "Changer le mot de passe pour :utilisateur",
                'create'              => "Créer un utilisateur",
                'deactivated'         => "Utilisateurs désactivés",
                'deleted'             => "Utilisateurs supprimés",
                'edit'                => "Modifier l'utilisateur",
                'management'          => "Gestion des utilisateurs",
                'no_permissions'      => "Aucune autorisation",
                'no_roles'            => "Aucun rôle à définir.",
                'permissions'         => "Autorisations",
                'age'                 => "Âge",
                'table' => [
                    'confirmed'      => "Confirmé",
                    'created'        => "Créé",
                    'email'          => "E-mail",
                    'id'             => "ID",
                    'last_updated'   => "Dernière mise à jour",
                    'name'           => "Nom",
                    'no_deactivated' => "Aucun utilisateur désactivé",
                    'no_deleted'     => "Aucun utilisateur supprimé",
                    'roles'          => "Rôles",
                    'total'          => "nombre total d'utilisateurs|nombre total d'utilisateurs",
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => "Aperçu",
                        'history'  => "Historique",
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => "Avatar",
                            'confirmed'    => "Confirmé",
                            'created_at'   => "Créé à",
                            'deleted_at'   => "Supprimé à",
                            'email'        => "E-mail",
                            'last_updated' => "Dernière mise à jour",
                            'name'         => "Nom",
                            'status'       => "État",
                        ],
                    ],
                ],

                'view' => "Voir l'utilisateur",
            ],
        ],
        'languages' => [
            'languages_manager' => "Gestionnaire des langue",
            'languages_management'=> "Gestion des langues"
        ],
        'levels' => [
            'levels' => "Niveaux",
            'levels_manager' => "Gestionnaire de niveaux",
        ],
        'locations' => [
            'locations_manager' => "Gestionnaire des sites",
            'regions' => "Régions",
            'countries' => "Pays",
            'cities' => "Villes",
            'place_types' => "Types de lieux",
            'places' => "Places"
        ],
        'safety_degrees' => [
            'safety_degrees_manager' => "Gestionnaire des degrés de sécurité",
            'safety_degrees' => "Degrés de sécurité",
        ],
        'activities' => [
            'activities_manager' => "Gestionnaire des activités",
            'activity_types' => "Types d'activités",
            'activities' => "Activités",
            'activity_media' => "Activité Médias",
        ],
        'interests' => [
            'interests_manager' => "Gestionnaire des intérêts",
            'interests' => "Centres d'intérêt"
        ],
        'religions' => [
            'religions_manager' => "Gestionnaire des religions",
            'religions' => "Religions"
        ],
        'lifestyles' => [
            'lifestyles_manager' => "Gestionnaire de styles de vie",
            'lifestyles' => "Styles de vie"
        ],
        'languages_spoken' => [
            'languages_spoken_manager' => "Gestionnaire des langues parlées",
            'languages_spoken' => "Langues parlées"
        ],
        'timings' => [
            'timings_manager' => "Gestionnaire de calendrier",
            'timings' => "Calendrier"
        ],
        'weekdays' => [
            'weekdays_manager' => "Gestionnaire des jours de la semaine",
            'weekdays' => "Jours de la semaine"
        ],
        'holidays' => [
            'holidays_manager' => "Gestionnaire des jours fériés",
            'holidays' => "Jours fériés"
        ],
        'hobbies' => [
            'hobbies_manager' => "Gestionnaire des loisirs",
            'hobbies' => "Loisirs"
        ],
        'emergency_numbers' => [
            'emergency_numbers_manager' => "Gestionnaire des numéros d'urgence",
            'emergency_numbers' => "Numéros d'urgence"
        ],
        'currencies' => [
            'currencies_manager' => "Gestionnaire des devises",
            'currencies' => "Monnaies"
        ],
        'cultures' => [
            'cultures_manager' => "Gestionnaire des cultures",
            'cultures' => "Cultures"
        ],
        'accommodations' => [
            'accommodations_manager' => "Gestionnaire de l'hébergement",
            'accommodations' => "Hébergement"
        ],
        'age_ranges' => [
            'age_ranges_manager' => "Gestionnaire de tranches d'âge",
            'age_ranges' => "Tranches d'âge"
        ],
        'cities' => [
            'cities_manager' => "Gestionnaire des villes",
            'active_cities' => "Villes actives",
        ],
        'embassies' => [
            'embassies_manager' => "Gestionnaire des ambassades",
            'active_embassies' => "Ambassades actives",
            'embassies' => "Ambassades"
        ],
    ],

    'frontend' => [

        'auth' => [
            'login_box_title'    => "Se connecter",
            'login_button'       => "Se connecter",
            'login_with'         => "Se connecter avec :reseau_social",
            'register_box_title' => "S'inscrire",
            'register_button'    => "S'inscrire",
            'remember_me'        => "Se souvenir de moi",
        ],

        'passwords' => [
            'forgot_password'                 => "Avez Vous Oublié Votre Mot de Passe ?",
            'reset_password_box_title'        => "Réinitialiser le mot de passe",
            'reset_password_button'           => "Réinitialiser le mot de passe",
            'send_password_reset_link_button' => "Envoyer le lien de réinitialisation du mot de passe",
        ],

        'macros' => [
            'country' => [
                'alpha'   => "Codes alphanumériques de pays",
                'alpha2'  => "Codes alphanumériques 2 de pays",
                'alpha3'  => "Codes alphanumériques 3 de pays",
                'numeric' => "Codes numériques des pays",
            ],

            'macro_examples' => "Exemples de macro",

            'state' => [
                'mexico' => "Liste des États du Mexique",
                'us'     => [
                    'us'       => "États d'Amérique",
                    'outlying' => "Territoires éloignés des États-Unis",
                    'armed'    => "Forces armées des États-Unis",
                ],
            ],

            'territories' => [
                'canada' => "Liste des provinces et territoires du Canada",
            ],

            'timezone' => "Fuseau horaire",
        ],

        'user' => [
            'passwords' => [
                'change' => "Changer le mot de passe",
            ],

            'profile' => [
                'avatar'             => "Avatar",
                'created_at'         => "Créé à",
                'edit_information'   => "Modifier les informations",
                'email'              => "E-mail",
                'last_updated'       => "Dernière mise à jour",
                'name'               => "Nom",
                'update_information' => "Mettre à jour les informations",
            ],
        ],

    ],
];
