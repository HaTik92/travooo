<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'roles' => [
                'already_exists'    => "Ce rôle existe déjà. Veuillez choisir un autre nom.",
                'cant_delete_admin' => "Vous ne pouvez pas supprimer le rôle d'administrateur.",
                'create_error'      => "Il y a eu un problème pour créer ce rôle. Veuillez réessayer.",
                'delete_error'      => "Il y a eu un problème lors de la suppression de ce rôle. Veuillez réessayer.",
                'has_users'         => "Vous ne pouvez pas supprimer un rôle avec les utilisateurs associés.",
                'needs_permission'  => "Vous devez sélectionner au moins une autorisation pour ce rôle.",
                'not_found'         => "Ce rôle n'existe pas.",
                'update_error'      => "Il y a eu un problème lors de la mise à jour de ce rôle. Veuillez réessayer.",
            ],

            'users' => [
                'cant_deactivate_self'  => "Vous ne pouvez pas vous faire ça.",
                'cant_delete_admin'  => "Vous ne pouvez pas supprimer le super administrateur.",
                'cant_delete_self'      => "Vous ne pouvez pas vous supprimer.",
                'cant_delete_own_session' => "Vous ne pouvez pas supprimer votre propre session.",
                'cant_restore'          => "Cet utilisateur n'est pas supprimé et ne peut donc pas être restauré.",
                'create_error'          => "Il y a eu un problème lors de la création du compte de cet utilisateur. Veuillez réessayer.",
                'delete_error'          => "Il y a eu un problème lors de la suppression de cet utilisateur. Veuillez réessayer.",
                'delete_first'          => "Cet utilisateur doit d'abord être supprimé avant de pouvoir être détruit définitivement.",
                'email_error'           => "Cette adresse email appartient à un autre utilisateur.",
                'mark_error'            => "Il y a eu un problème lors de la mise à jour de cet utilisateur. Veuillez réessayer.",
                'not_found'             => "Cet utilisateur n'existe pas.",
                'restore_error'         => "Il y a eu un problème lors de la restauration de cet utilisateur. Veuillez réessayer.",
                'role_needed_create'    => "Vous devez choisir au moins un rôle.",
                'role_needed'           => "Vous devez choisir au moins un rôle.",
                'session_wrong_driver'  => "Votre pilote de session doit être défini sur la base de données pour utiliser cette fonctionnalité.",
                'update_error'          => "Il y a eu un problème lors de la mise à jour de cet utilisateur. Veuillez réessayer.",
                'update_password_error' => "Il y a eu un problème lors de la modification du mot de passe de cet utilisateur. Veuillez réessayer.",
            ],

            'language' => [
                'code_error'    => "Ce code de langue existe déjà dans la base de données",
                'update_error'  => "Une erreur inattendue s'est produite, réessayez",
            ],
        ],
    ],

    'frontend' => [
        'auth' => [
            'confirmation' => [
                'already_confirmed' => "Votre compte est déjà confirmé.",
                'confirm'           => "Confirmez votre compte !",
                'created_confirm'   => "Votre compte a été créé avec succès. Nous vous avons envoyé un e-mail pour confirmer votre compte.",
                'mismatch'          => "Votre code de confirmation ne correspond pas.",
                'not_found'         => "Ce code de confirmation n'existe pas.",
                'resend'            => "Votre compte n'est pas confirmé. Veuillez cliquer sur le lien de confirmation contenu dans l'e-mail que vous avez reçu, ou <a href=\"".route('frontend.auth.account.confirm.resend', ':user_id').'">click here</a> to resend the confirmation e-mail.',
                'success'           => "Votre compte a été confirmé avec succès !",
                'resent'            => "Un nouvel e-mail de confirmation a été envoyé à l'adresse enregistrée.",
            ],

            'deactivated' => "Votre compte a été désactivé.",
            'email_taken' => "Cette adresse e-mail est déjà enregistrée.",

            'password' => [
                'change_mismatch' => "Ce n'est pas votre ancien mot de passe.",
            ],

            'registration_disabled' => "Les inscriptions sont actuellement fermées.",
        ],
    ],
];
