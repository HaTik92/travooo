import Tabs from "./Tabs";

export class TabsFilter extends Tabs {
    constructor(tablist, props = {}) {
        super();
        this.tablist = tablist;
        this.props = props;
    }
}