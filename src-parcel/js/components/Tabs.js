export default class Tabs {
    constructor(tablist, props = {}) {
        // Get relevant elements and collections
        this.tablist = tablist || document.querySelector('[data-attr="tablist"]');
        if (this.tablist === null)
            return;
        this.props = Object.assign({}, props, {
            tabs: this.tablist.querySelectorAll('[data-attr="tab"]'),
            panels: document.querySelectorAll('[data-attr="tab-panel"]'),
            callback: (e) => {
            },
        });
    }

    init() {
        this.tablist.setAttribute('role', 'tablist');
        this.handleTabs();
        Array.prototype.forEach.call(this.props.panels, this.handlePanel.bind(this));
    }

    // Add semantics are remove user focusability for each tab
    handleTabs() {
        Array.prototype.forEach.call(this.props.tabs, (tab, i) => {
            tab.setAttribute('role', 'tab');
            tab.setAttribute('id', 'tab' + (i + 1));
            tab.setAttribute('tabindex', '-1');
            tab.parentNode.setAttribute('role', 'presentation');

            if (i === 0) {
                tab.removeAttribute('tabindex');
                tab.setAttribute('aria-selected', 'true');
                tab.classList.add('active');
            }

            // Handle clicking of tabs for mouse users
            tab.addEventListener('click', this.handleClick.bind(this));
        });
    }

    // Add tab panel semantics and hide them all
    handlePanel(panel, i) {
        panel.setAttribute('role', 'tabpanel');
        panel.setAttribute('tabindex', '-1');
        let id = panel.getAttribute('id');
        panel.setAttribute('aria-labelledby', this.props.tabs[i].id);
        panel.hidden = i !== 0;
    }

    handleClick(e) {
        e.preventDefault();
        let currentTab = this.tablist.querySelector('[aria-selected]');
        if (e.currentTarget !== currentTab) {
            this.switchTab(currentTab, e.currentTarget);
            this.props.callback(e);
        }
    }

    // The tab switching function
    switchTab(oldTab, newTab) {
        newTab.focus();
        // Make the active tab focusable by the user (Tab key)
        newTab.removeAttribute('tabindex');
        // Set the selected state
        newTab.setAttribute('aria-selected', 'true');
        oldTab.removeAttribute('aria-selected');
        oldTab.setAttribute('tabindex', '-1');
        // Get the indices of the new and old tabs to find the correct
        // tab panels to show and hide
        let newPanel = newTab.getAttribute('href').replace('#', '').trim() ? document.querySelector(`${newTab.getAttribute('href')}`) : null;

        this.props.panels?.forEach(item => {
            item.hidden = true;
        });
        newPanel ? newPanel.hidden = false : null;
        this.toggleClass(oldTab, newTab);
    }

    toggleClass(oldTab, newTab) {
        oldTab ? oldTab.classList.remove("active") : null;
        newTab.classList.add("active");
    }
}