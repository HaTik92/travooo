export default class Filter {
    constructor(elem, props = {}) {
        this.elem = elem;
        this.repots_filters = props.reportFilters?.filters || repots_filters;
        this.order = props.reportFilters?.order || order;
        this.type = props.reportFilters?.type || type;
        this.callback = props.callback || function () {
        };
    }

    init() {
        this.setFilters();
        this.callback();
    }

    setFilters() {
        repots_filters = this.repots_filters;
        order = this.order;
        type = this.type;
    }
}