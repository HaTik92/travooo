import '../scss/modules/discussion/discussions.scss';
import Tabs from "./components/Tabs";
import {TabsFilter} from "./components/TabsFilter";

const tabs = new Tabs();
const $orderFilterTabs = document.querySelector('.tab-discussion_order .tab-list');
const tabsFilter = new TabsFilter($orderFilterTabs, loadSearchContent);

tabs.init();
new TabsFilter($orderFilterTabs, {
    tabs: $orderFilterTabs.querySelectorAll('[data-tab-filter]'),
    callback: function (e) {
        const newTab = e.currentTarget;

        order = newTab.dataset.tabFilter;
        loadSearchContent();
    }
}).handleTabs();