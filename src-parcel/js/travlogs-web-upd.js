import '../scss/modules/reports/travlogs-web-upd.scss';

// Disable to lock the scrolling on select
const selectEl = [
    '[class*=report_user]',
    '[class*=report_map-]',
    '[class*=report-place]'
]
const popup = $('#createTravlogNextPopup');

popup.on('scroll', function () {
    selectEl.forEach((el)=>{
        $(`${el}`).select2('close');
    });
});