import "../../../scss/modules/global-trip/main.scss";
import {TabsFilter} from "../../components/TabsFilter";
const $orderTabsFilter = document.querySelector('#global_trip_order_mobile .tab-list');

// new TabsFilter($orderTabsFilter, getPlansFilter).handleTabs();
new TabsFilter($orderTabsFilter, {
    tabs: $orderTabsFilter.querySelectorAll('[data-tab-filter]'),
    callback: function (e) {
        const newTab = e.currentTarget;

        order = newTab.dataset.tabFilter;
        getPlansFilter();
    }
}).handleTabs();