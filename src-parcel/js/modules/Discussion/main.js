import '../../../scss/modules/discussion/discussions.scss';
import Tabs from "../../components/Tabs";
import {TabsFilter} from "../../components/TabsFilter";
import {setTabFilterContentOrder} from "../../utilities/funcs";

const tabs = new Tabs();
const $orderFilterTabs = document.querySelector('.tab-discussion_order .tab-list');

tabs.init();
new TabsFilter($orderFilterTabs, {
    tabs: $orderFilterTabs.querySelectorAll('[data-tab-filter]'),
    callback: function (e) {
        setTabFilterContentOrder(e);
        loadSearchContent();
    }
}).handleTabs();