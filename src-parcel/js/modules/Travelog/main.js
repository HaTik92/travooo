import "../../../scss/modules/reports/main.scss";
import {getLocalStorageItem} from "../../utilities/funcs";

const reportFilters = getLocalStorageItem('report_filters');
const $travelogOrder = $('#travelog_order');
const $travelogOrderTab = $('#travelog_orderTab');

repots_filters = reportFilters?.filters || repots_filters;
order = reportFilters?.order || order;
type = reportFilters?.type || type;

const $reptype = $(`[reptype=${type}]`);

handleRepType($reptype);
$travelogOrder.val(order);
$travelogOrder.trigger('change');
$travelogOrderTab.find('[data-attr="tab"]').removeClass('active');
$travelogOrderTab.find(`[href="#${order}"]`).addClass('active');

/**
 * Create Travelog
 * Remove Warning messages after another type list item is chosen
 */

const $createTravlogNextPopup = $('#createTravlogNextPopup'),
    $actionListItem = $createTravlogNextPopup.find('.rep-action-list li a'),
    $errorMsg = $createTravlogNextPopup.find('#message2');
const handleErrorMsg = () => {
    $errorMsg.html('');
};

$('body').on('click', $actionListItem, handleErrorMsg);

// Trip plans
const handleTripHasPrivate = (evt) => {
    const $tripHasPrivate = $('.trip-has-private');

    $tripHasPrivate.removeClass('hover');
    if ($(evt.target).hasClass('trip-has-private')) {
        $(evt.target).addClass('hover');
    }
};

$('body').on('click', '.trip-has-private', handleTripHasPrivate);
$('body').on('change', '.insert-plan-input', handleTripHasPrivate);