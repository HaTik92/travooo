<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEventsSetColumnsToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {

            if (collect(DB::select("SHOW INDEXES FROM events"))->pluck('Key_name')->contains('provider_id')) {
                $table->dropIndex('provider_id');
            }

        });

        Schema::table('events', function (Blueprint $table) {
            $table->integer('places_id')->nullable()->change();
            $table->text('provider_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->integer('places_id')->nullable(false)->change();
            $table->text('provider_id')->nullable(false)->change();
        });
    }
}
