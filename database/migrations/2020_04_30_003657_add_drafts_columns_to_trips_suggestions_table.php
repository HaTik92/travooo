<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDraftsColumnsToTripsSuggestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips_suggestions', function (Blueprint $table) {
            $table->boolean('is_for_draft')->default(false)->after('suggested_trips_places_id');
            $table->boolean('is_saved')->default(false)->after('is_for_draft');
            $table->json('meta')->nullable()->after('is_saved');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips_suggestions', function (Blueprint $table) {
            $table->dropColumn(['is_for_draft', 'is_saved', 'meta']);
        });
    }
}
