<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFoftDeletesToReportsLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_locations', function (Blueprint $table) {
            $table->softDeletes();
            $table->index('location_type');
            $table->index('location_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_locations', function (Blueprint $table) {
            $table->dropSoftDeletes();
            $table->dropIndex(['location_type']);
            $table->dropIndex(['location_id']);
        });
    }
}
