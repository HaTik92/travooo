<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableRankingBadgesAddOrderColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ranking_badges', function (Blueprint $table) {
            $table->unsignedInteger('followers')->default(0)->after('points');
            $table->unsignedInteger('order')->default(0)->after('followers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ranking_badges', function (Blueprint $table) {
            $table->dropColumn('followers');
            $table->dropColumn('order');
        });
    }
}
