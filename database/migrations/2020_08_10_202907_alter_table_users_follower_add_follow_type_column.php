<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersFollowerAddFollowTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_followers', function (Blueprint $table) {
            $table->integer('follow_type')->default(1)->after('followers_id')->comment('1 - follow, 2 - unfollow');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_followers', function (Blueprint $table) {
            $table->dropColumn(['follow_type']);
        });
    }
}
