<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('api_reports')) {
            Schema::create('api_reports', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('post_id');
                $table->integer('user_id');
                $table->string('media_url')->nullable();
                $table->string('text');
                $table->string('type');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_reports');
    }
}
