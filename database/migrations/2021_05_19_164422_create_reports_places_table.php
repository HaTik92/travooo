<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('reports_places')) {
            Schema::create('reports_places', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('reports_id')->unsigned();
                $table->integer('places_id')->unsigned();
                $table->engine = 'InnoDB';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('reports_places')) {
            Schema::dropIfExists('reports_places');
        }
    }
}
