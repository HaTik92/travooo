<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries_abouts', function (Blueprint $table) {
            $table->integer('id', true);
			$table->integer('countries_id')->index('countries_id');
			$table->string('type');
			$table->string('title');
			$table->text('body', 65535)->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries_abouts');
    }
}
