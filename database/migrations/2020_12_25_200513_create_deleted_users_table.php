<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeletedUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('deleted_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reason_id')->nullable();
            $table->string('name')->nullable();
            $table->string('username')->nullable();
            $table->string('email');
            $table->string('contact_email')->nullable();
            $table->integer('gender')->nullable()->comment('1 - Male, 2 - Female, 3 - Unspecified');
            $table->date('birth_date')->nullable();
            $table->string('profile_picture')->nullable();
            $table->string('mobile')->nullable();
            $table->integer('expert')->nullable()->comment('0 - Regular, 1 - Expert');
            $table->integer('nationality')->nullable();
            $table->string('interests')->nullable();
            $table->text('about')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('deleted_users');
    }

}
