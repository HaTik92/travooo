<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SearchSubmissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_submissions', function(Blueprint $table)
		{
            $table->integer('id', true);
			$table->integer('user_id')->index('user_id');
            $table->string('place_name', 255)->nullable();
            $table->string('place_city', 255)->nullable();
            $table->string('place_country', 255)->nullable();
            $table->string('place_address', 255)->nullable();
            $table->string('place_state', 255)->nullable();
            $table->string('place_category', 255)->nullable();
            $table->string('iso_code', 10);
            // $table->string('place_hyperlink', 255);
            $table->decimal('lat', 10, 8);
            $table->decimal('lng', 11, 8);
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->timestamp('deleted_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
