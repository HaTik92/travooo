<?php

use App\Models\Logs\RankCronLog;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankCronLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rank_cron_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cron_id', 25)->index();
            $table->string('post_type')->index();
            $table->string('action')->index();
            $table->text('message')->nullable();
            $table->integer('post_id')->nullable();
            $table->integer('loop_count')->nullable();
            $table->tinyInteger('rank_type')->default(RankCronLog::RANK_TYPE_7)->index();
            $table->timestamps();

            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rank_cron_logs');
    }
}
