<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingUsersBlockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_users_block', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('users_id')->unsigned()->index('users_id');
             $table->integer('blocked_users_id');
            $table->timestamps();
            
             $table->foreign('users_id', 'setting_users_block_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_users_block');
    }
}
