<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRankingUserTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ranking_user_transactions', function (Blueprint $table) {
            $table->dropColumn('action');
            $table->dropColumn('variable');
            $table->dropColumn('badges_id');
            $table->dropColumn('author_id');
            $table->unsignedInteger('countries_id')->after('points');
            $table->unsignedInteger('entity_id')->after('countries_id');
            $table->string('entity_type')->after('entity_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ranking_user_transactions', function (Blueprint $table) {
            $table->integer('author_id')->default(null)->after('id');
            $table->integer('badges_id')->after('users_id');
            $table->string('action')->after('points');
            $table->integer('variable')->after('action');
        });
    }
}
