<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableRankingUserBadgesChangeColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ranking_user_badges', function (Blueprint $table) {
            $table->dropColumn('level');
            $table->dropColumn('level_points');
            $table->boolean('is_preset')->default(false)->after('badges_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ranking_user_badges', function (Blueprint $table) {
            $table->dropColumn('is_preset');
            $table->integer('level');
            $table->integer('level_points');
        });
    }
}
