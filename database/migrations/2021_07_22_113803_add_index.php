<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_followers', function (Blueprint $table) {
            $table->index(['followers_id', 'follow_type']);
		});

        Schema::table('events', function (Blueprint $table) {
            $table->index('cities_id');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->index('deleted_at');
        });

        Schema::table('posts_comments', function (Blueprint $table) {
            $table->index([DB::raw('type(3)'), 'created_at']);
        });

        Schema::table('posts_shares', function (Blueprint $table) {
            $table->index([DB::raw('type(3)'), 'created_at']);
        });

        Schema::table('posts_likes', function (Blueprint $table) {
            $table->index('created_at');
        });

        Schema::table('reports', function (Blueprint $table) {
            $table->index(['language_id', 'flag']);
        });

        Schema::table('notifications', function (Blueprint $table) {
            $table->index('users_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_followers', function (Blueprint $table) {
            $table->dropIndex(['followers_id', 'follow_type']);
        });

        Schema::table('events', function (Blueprint $table) {
            $table->dropIndex('cities_id');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex('deleted_at');
        });

        Schema::table('posts_comments', function (Blueprint $table) {
            $table->dropIndex([DB::raw('type(3)'), 'created_at']);
        });

        Schema::table('posts_shares', function (Blueprint $table) {
            $table->dropIndex([DB::raw('type(3)'), 'created_at']);
        });

        Schema::table('posts_likes', function (Blueprint $table) {
            $table->dropIndex('created_at');
        });

        Schema::table('reports', function (Blueprint $table) {
            $table->dropIndex(['language_id', 'flag']);
        });
        Schema::table('notifications', function (Blueprint $table) {
            $table->dropIndex('users_id');
        });
    }
}
