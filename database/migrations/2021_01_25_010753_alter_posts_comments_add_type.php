<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPostsCommentsAddType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts_comments', function (Blueprint $table) {
            $table->string('type', 20)->default('text')->after('text');
            $table->unsignedInteger('post_type_id')->default(0)->after('type');
            $table->index('post_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts_comments', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropIndex(['post_type_id']);
            $table->dropColumn('post_type_id');
        });
    }
}
