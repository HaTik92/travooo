<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfirmedReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirmed_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('spam_post_id')->unsigned();
            $table->foreign('spam_post_id')->references('id')->on('spams_posts')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->string('type');
            $table->integer('admin_id')->unsigned();
            $table->foreign('admin_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confirmed_reports');
    }
}
