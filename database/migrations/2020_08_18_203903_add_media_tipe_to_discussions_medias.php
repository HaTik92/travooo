<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMediaTipeToDiscussionsMedias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('discussions_medias')) {
            Schema::table('discussions_medias', function (Blueprint $table) {
                $table->string('media_type')->after('discussion_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discussions_medias', function (Blueprint $table) {
                $table->dropColumn(['media_type']);
        });
    }
}
