<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpertsTravelStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experts_travel_styles', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('users_id')->unsigned()->index('users_id');
            $table->integer('travel_style_id')->index('travel_style_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experts_travel_styles');
    }
}
