<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMediasAddUsersIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    if (!Schema::hasColumn('medias', 'users_id'))
        {
        Schema::table('medias', function (Blueprint $table) {
            $table->unsignedInteger('users_id')->nullable()->after('url');
            $table->foreign('users_id')->references('id')->on('users');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medias', function (Blueprint $table) {
            $table->dropColumn(['users_id']);
        });
    }
}
