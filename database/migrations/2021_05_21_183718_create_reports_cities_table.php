<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reports_id')->unsigned();
            $table->integer('cities_id')->unsigned();
            $table->integer('status')->default(0)->comment('0 - draft, 1 - publish, 2 - deleted');
            $table->foreign('reports_id')->references('id')->on('reports')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports_cities');
    }
}
