<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFromWhereColumnInUsersFollowers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users_followers')) {
            Schema::table('users_followers', function (Blueprint $table) {
                $table->string('from_where', 50)->after('follow_type')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_followers', function (Blueprint $table) {
                $table->dropColumn(['from_where']);
        });
    }
}
