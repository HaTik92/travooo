<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderByCityColumnToPlacesTop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('places_top')) {
            Schema::table('places_top', function (Blueprint $table) {
                $table->integer('order_by_city')->default(0)->after('destination_type');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('places_top', function (Blueprint $table) {
            $table->dropColumn(['order_by_city']);
        });
    }
}
