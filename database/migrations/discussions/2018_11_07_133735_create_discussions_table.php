<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscussionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id');
            $table->integer('type');
            $table->string('destination_type')->nullable();
            $table->integer('destination_id')->nullable();
            $table->string('question')->nullable();
            $table->text('description')->nullable();
            $table->integer('last_reply_by')->nullable();
            $table->timestamp('last_reply_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussions');
    }
}
