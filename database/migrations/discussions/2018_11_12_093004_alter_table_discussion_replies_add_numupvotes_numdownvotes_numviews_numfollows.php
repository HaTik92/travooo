<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDiscussionRepliesAddNumupvotesNumdownvotesNumviewsNumfollows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discussion_replies', function (Blueprint $table) {
            if (!Schema::hasColumn('discussion_replies', 'num_upvotes')) {
                $table->integer('num_upvotes');
            }
            if (!Schema::hasColumn('discussion_replies', 'num_downvotes')) {
                $table->integer('num_downvotes');
            }
            if (!Schema::hasColumn('discussion_replies', 'num_views')) {
                $table->integer('num_views');
            }
            if (!Schema::hasColumn('discussion_replies', 'num_follows')) {
                $table->integer('num_follows');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
