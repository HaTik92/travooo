<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateToCountriesHolidays extends Migration
{
    public function up()
    {
        Schema::table('countries_holidays', function (Blueprint $table) {
                $table->string('date')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries_holidays', function (Blueprint $table) {
                $table->dropColumn('date');
        });
    }
}
