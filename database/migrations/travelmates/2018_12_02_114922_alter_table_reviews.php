<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReviews extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('reviews', function(Blueprint $table) {
            if (Schema::hasColumn('reviews', 'cities_id')) {
                $table->dropForeign('reviews_ibfk_1');
                $table->dropColumn('cities_id');
            }
            if (Schema::hasColumn('reviews', 'countries_id')) {
                $table->dropForeign('reviews_ibfk_2');
                $table->dropColumn('countries_id');
            }
            if (Schema::hasColumn('reviews', 'users_id')) {
                $table->dropForeign('reviews_ibfk_7');
                $table->dropColumn('users_id');
            }
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
