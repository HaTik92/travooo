<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips', function(Blueprint $table) {
            $table->dropColumn('duration');
            $table->dropColumn('distance');
            $table->dropColumn('num_places');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->integer('active_version')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
