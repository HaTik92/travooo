<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTravelmatesRequestsUsers2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('travelmate_request_users')) {
            Schema::table('travelmate_request_users', function($table) {
                if (!Schema::hasColumn('travelmate_request_users', 'requests_id')) {
                    $table->integer('requests_id');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
