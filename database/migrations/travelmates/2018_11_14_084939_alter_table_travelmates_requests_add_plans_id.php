<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTravelmatesRequestsAddPlansId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('travelmate_requests')) {
            Schema::table('travelmate_requests', function($table) {
                if (!Schema::hasColumn('travelmate_requests', 'plans_id')) {
                    $table->integer('plans_id')->nullable();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
