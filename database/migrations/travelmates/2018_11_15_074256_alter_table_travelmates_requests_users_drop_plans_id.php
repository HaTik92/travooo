<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTravelmatesRequestsUsersDropPlansId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('travelmate_requests_users')) {
            Schema::table('travelmate_requests_users', function($table) {
                if (Schema::hasColumn('travelmate_requests_users', 'plans_id')) {
                    $table->dropColumn('plans_id');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
