<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reviews', function(Blueprint $table) {
            $table->integer('embassies_id')->nullable()->change();
            $table->integer('medias_id')->nullable()->change();
            $table->integer('pages_id')->nullable()->change();
            $table->integer('places_id')->nullable()->change();
            $table->integer('restaurants_id')->nullable()->change();
            $table->integer('hotels_id')->nullable()->change();
            //$table->dropForeign('reviews_ibfk_8');
            $table->integer('by_users_id')->nullable()->change();
            $table->dropColumn('active');
            $table->renameColumn('description', 'text');
            $table->string('google_author_name')->nullable();
            $table->string('google_author_url')->nullable();
            $table->string('google_profile_photo_url')->nullable();
            $table->string('google_language')->nullable();
            $table->string('google_relative_time_description')->nullable();
            $table->string('google_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
