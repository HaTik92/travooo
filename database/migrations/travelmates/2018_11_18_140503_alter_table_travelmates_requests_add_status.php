<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTravelmatesRequestsAddStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('travelmate_requests')) {
            Schema::table('travelmate_requests', function($table) {
                if (!Schema::hasColumn('travelmate_requests', 'status')) {
                    $table->integer('status')->default('1');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
