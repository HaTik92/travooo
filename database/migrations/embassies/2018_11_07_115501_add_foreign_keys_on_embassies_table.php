<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysOnEmbassiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('embassies', function(Blueprint $table)
        {
            $table->foreign('origin_country_id')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('location_country_id')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
