<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEmbassiesTableCountriesColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('embassies', function(Blueprint $table) {
            $table->renameColumn('countries_id', 'location_country_id');
            $table->integer('origin_country_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('embassies', function(Blueprint $table) {
            $table->renameColumn('location_country_id', 'countries_id');
            $table->dropColumn('origin_country_id');
        });
    }
}
