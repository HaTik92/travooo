<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('reports_locations')) {
            Schema::create('reports_locations', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('reports_id')->unsigned();
                $table->string('location_type')->nullable()->comment('country, city');
                $table->integer('location_id')->unsigned()->nullable();
                $table->engine = 'InnoDB';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('reports_locations')) {
            Schema::dropIfExists('reports_locations');
        }
    }
}
