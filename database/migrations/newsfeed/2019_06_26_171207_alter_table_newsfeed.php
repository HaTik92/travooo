<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNewsfeed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('newsfeed')) {
            Schema::table('newsfeed', function (Blueprint $table) {
                $table->integer('countries_id')->nullable();
                $table->integer('cities_id')->nullable();
                $table->integer('places_id')->nullable();
                $table->integer('hotels_id')->nullable();
                $table->integer('restaurants_id')->nullable();
                $table->integer('embassies_id')->nullable();
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
