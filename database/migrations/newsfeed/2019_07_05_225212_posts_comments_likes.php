<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostsCommentsLikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_comments_likes', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('posts_comments_id')->unsigned();
            $table->integer('users_id')->unsigned();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->engine = 'InnoDB';

            $table->foreign('posts_comments_id')->references('id')->on('posts_comments')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('users_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
