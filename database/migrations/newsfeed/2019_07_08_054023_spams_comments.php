<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SpamsComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spams_comments', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('comments_id')->unsigned();
            $table->integer('users_id')->unsigned();
            $table->text('comment_type')->nullable();
            $table->text('report_type')->nullable();
            $table->text('report_text')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->engine = 'InnoDB';

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
