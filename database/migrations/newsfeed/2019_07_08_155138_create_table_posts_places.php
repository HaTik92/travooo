<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePostsPlaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('posts_places')) {
        Schema::create('posts_places', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('posts_id')->unsigned();
                        $table->integer('places_id')->unsigned();
                        $table->engine = 'InnoDB';
		});
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
