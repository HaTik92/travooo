<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateTableNewsfeed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('newsfeed')) {
            Schema::create('newsfeed', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('users_id');
                
                $table->integer('friends_id')->nullable();
                $table->integer('posts_id')->nullable();
                $table->integer('plans_id')->nullable();
                $table->integer('travelmates_id')->nullable();
                $table->integer('discussions_id')->nullable();
                $table->integer('reports_id')->nullable();
                $table->integer('medias_id')->nullable();
                $table->text('action')->nullable();
                
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->tinyInteger('scope')->default(0);
                
                $table->engine = 'InnoDB';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
