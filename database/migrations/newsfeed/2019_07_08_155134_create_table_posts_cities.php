<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePostsCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('posts_cities')) {
        Schema::create('posts_cities', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('posts_id')->unsigned();
                        $table->integer('cities_id')->unsigned();
                        $table->engine = 'InnoDB';
		});
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
