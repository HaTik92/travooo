<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePostsCountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('posts_countries')) {
        Schema::create('posts_countries', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('posts_id')->unsigned();
                        $table->integer('countries_id')->unsigned();
                        $table->engine = 'InnoDB';
		});
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
