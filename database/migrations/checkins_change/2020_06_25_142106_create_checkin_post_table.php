<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckinPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('posts_checkin')) {
			Schema::create('posts_checkin', function(Blueprint $table)
			{
                 $table->integer('checkin_id')->index('checkin_id');
                $table->integer('posts_id')->index('posts_id');
				$table->engine = 'InnoDB';
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
