<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPostsSharesAddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts_shares', function (Blueprint $table) {
            $table->index('type');
            $table->index('posts_type_id');
            $table->index('text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts_shares', function (Blueprint $table) {
            $table->dropIndex(['type']);
            $table->dropIndex(['posts_type_id']);
            $table->dropIndex(['text']);
        });
    }
}
