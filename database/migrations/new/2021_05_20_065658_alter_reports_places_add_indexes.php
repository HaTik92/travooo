<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReportsPlacesAddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_places', function (Blueprint $table) {
            $table->index('reports_id');
            $table->index('places_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_places', function (Blueprint $table) {
            $table->dropIndex(['reports_id']);
            $table->dropIndex(['places_id']);
        });
    }
}
