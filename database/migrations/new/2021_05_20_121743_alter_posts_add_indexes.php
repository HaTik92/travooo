<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPostsAddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->index('language_id');
            $table->index('users_id');
            $table->index('text');
            $table->index('permission');
            $table->index('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropIndex(['language_id']);
            $table->dropIndex(['users_id']);
            $table->dropIndex(['text']);
            $table->dropIndex(['permission']);
            $table->dropIndex(['date']);
        });
    }
}
