<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPostsCountriesAddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts_countries', function (Blueprint $table) {
            $table->index('posts_id');
            $table->index('countries_id');
            $table->index('cities_id');
            $table->index('places_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts_countries', function (Blueprint $table) {
            $table->dropIndex(['posts_id']);
            $table->dropIndex(['countries_id']);
            $table->dropIndex(['cities_id']);
            $table->dropIndex(['places_id']);
        });
    }
}
