<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityLogAddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_log', function (Blueprint $table) {
            $table->index('type');
            $table->index('action');
            $table->index('variable');
            $table->index('time');
            $table->index('permission');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_log', function (Blueprint $table) {
            $table->dropIndex(['type']);
            $table->dropIndex(['action']);
            $table->dropIndex(['variable']);
            $table->dropIndex(['time']);
            $table->dropIndex(['permission']);
        });
    }
}
