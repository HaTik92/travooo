<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPostsTagsAddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts_tags', function (Blueprint $table) {
            $table->index('posts_type');
            $table->index('posts_id');
            $table->index('destination');
            $table->index('destination_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts_tags', function (Blueprint $table) {
            $table->dropIndex(['posts_type']);
            $table->dropIndex(['posts_id']);
            $table->dropIndex(['destination']);
            $table->dropIndex(['destination_id']);
        });
    }
}
