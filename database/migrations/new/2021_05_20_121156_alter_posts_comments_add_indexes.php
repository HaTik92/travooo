<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPostsCommentsAddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts_comments', function (Blueprint $table) {
            $table->index('users_id');
            $table->index('text');
            $table->index('type');
            $table->index('parents_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts_comments', function (Blueprint $table) {
            $table->dropIndex(['users_id']);
            $table->dropIndex(['text']);
            $table->dropIndex(['type']);
            $table->dropIndex(['parents_id']);
        });
    }
}
