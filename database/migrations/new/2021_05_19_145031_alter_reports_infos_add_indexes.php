<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReportsInfosAddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_infos', function (Blueprint $table) {
            $table->index('var');
            $table->index('val');
            $table->index('reports_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_infos', function (Blueprint $table) {
            $table->dropIndex(['var']);
            $table->dropIndex(['val']);
            $table->dropIndex(['reports_id']);
        });
    }
}
