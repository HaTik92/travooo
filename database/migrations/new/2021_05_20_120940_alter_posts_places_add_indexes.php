<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPostsPlacesAddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts_places', function (Blueprint $table) {
            $table->index('posts_id');
            $table->index('places_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts_places', function (Blueprint $table) {
            $table->dropIndex(['posts_id']);
            $table->dropIndex(['places_id']);
        });
    }
}
