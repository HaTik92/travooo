<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CopyrightInfringementBlacklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('copyright_infringement_blacklist')) {
            Schema::create('copyright_infringement_blacklist', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('copyright_infringement_id');
                $table->integer('admin_id');
                $table->string('sender_email');
                $table->string('sender_name');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('copyright_infringement_blacklist');
    }
}
