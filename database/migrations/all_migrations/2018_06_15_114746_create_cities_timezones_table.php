<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTimezonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities_timezones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cities_id');
            $table->text('time_zone_id')->nullable();
            $table->text('time_zone_name')->nullable();
            $table->string('dst_offset', 255)->nullable();
            $table->string('raw_offset', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities_timezones');
    }
}
