<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFollowersFollowingToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            if (!Schema::hasColumn('users', 'followers')) {
                $table->integer('followers')->nullable()->default(0);
            }
            if (!Schema::hasColumn('users', 'following')) {
                $table->integer('following')->nullable()->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            if (Schema::hasColumn('users', 'followers')) {
                $table->dropColumn('followers');
            }
            if (Schema::hasColumn('users', 'following')) {
                $table->dropColumn('following');
            }
        });
    }
}
