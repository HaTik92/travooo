<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConfAccommodationsTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('conf_accommodations_trans', function(Blueprint $table)
		{
			$table->foreign('accommodations_id', 'conf_accommodations_trans_ibfk_1')->references('id')->on('conf_accommodations')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('languages_id', 'conf_accommodations_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conf_accommodations_trans', function(Blueprint $table)
		{
			$table->dropForeign('conf_accommodations_trans_ibfk_1');
			$table->dropForeign('conf_accommodations_trans_ibfk_2');
		});
	}

}
