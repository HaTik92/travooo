<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmbassiesSearchHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('embassies_search_history', function(Blueprint $table)
		{
			$table->foreign('admin_id', 'embassies_search_history_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('embassies_search_history', function(Blueprint $table)
		{
			$table->dropForeign('embassies_search_history_ibfk_1');
		});
	}

}
