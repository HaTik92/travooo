<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMediasLikesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('medias_likes', function(Blueprint $table)
		{
			$table->foreign('users_id', 'medias_likes_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('medias_id', 'medias_likes_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('medias_likes', function(Blueprint $table)
		{
			$table->dropForeign('medias_likes_ibfk_1');
			$table->dropForeign('medias_likes_ibfk_2');
		});
	}

}
