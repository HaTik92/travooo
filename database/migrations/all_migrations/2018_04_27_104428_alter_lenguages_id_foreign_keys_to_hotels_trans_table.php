<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLenguagesIdForeignKeysToHotelsTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotels_trans', function(Blueprint $table)
        {
            $table->dropForeign('hotels_trans_ibfk_2');
            $table->foreign('languages_id', 'hotels_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotels_trans', function(Blueprint $table) {
            $table->dropForeign('hotels_trans_ibfk_2');
            $table->foreign('languages_id', 'hotels_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }
}
