<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersHobbiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_hobbies', function(Blueprint $table)
		{
			$table->foreign('hobbies_id', 'users_hobbies_ibfk_1')->references('id')->on('conf_hobbies')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('users_id', 'users_hobbies_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_hobbies', function(Blueprint $table)
		{
			$table->dropForeign('users_hobbies_ibfk_1');
			$table->dropForeign('users_hobbies_ibfk_2');
		});
	}

}
