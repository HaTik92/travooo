<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersTravelHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_travel_history', function(Blueprint $table)
		{
			$table->foreign('countries_id', 'users_travel_history_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cities_id', 'users_travel_history_ibfk_2')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('places_id', 'users_travel_history_ibfk_3')->references('id')->on('places')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_travel_history', function(Blueprint $table)
		{
			$table->dropForeign('users_travel_history_ibfk_1');
			$table->dropForeign('users_travel_history_ibfk_2');
			$table->dropForeign('users_travel_history_ibfk_3');
		});
	}

}
