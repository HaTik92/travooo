<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConfLifestylesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('conf_lifestyles', function(Blueprint $table)
		{
			$table->foreign('cover_media_id', 'conf_lifestyles_ibfk_1')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conf_lifestyles', function(Blueprint $table)
		{
			$table->dropForeign('conf_lifestyles_ibfk_1');
		});
	}

}
