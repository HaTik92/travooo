<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTravelStylesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_travel_styles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('users_id')->unsigned()->index('users_id');
			$table->integer('conf_lifestyles_id')->index('conf_lifestyles_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_travel_styles');
	}

}
