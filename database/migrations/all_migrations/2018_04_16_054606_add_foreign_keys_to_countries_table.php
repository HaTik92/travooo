<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('countries', function(Blueprint $table)
		{
			$table->foreign('regions_id', 'countries_ibfk_1')->references('id')->on('conf_regions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('safety_degree_id', 'countries_ibfk_2')->references('id')->on('conf_safety_degrees')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cover_media_id', 'countries_ibfk_3')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('countries', function(Blueprint $table)
		{
			$table->dropForeign('countries_ibfk_1');
			$table->dropForeign('countries_ibfk_2');
			$table->dropForeign('countries_ibfk_3');
		});
	}

}
