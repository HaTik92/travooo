<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCountriesEmergencyNumbersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('countries_emergency_numbers', function(Blueprint $table)
		{
			$table->foreign('countries_id', 'countries_emergency_numbers_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('emergency_numbers_id', 'countries_emergency_numbers_ibfk_2')->references('id')->on('conf_emergency_numbers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('countries_emergency_numbers', function(Blueprint $table)
		{
			$table->dropForeign('countries_emergency_numbers_ibfk_1');
			$table->dropForeign('countries_emergency_numbers_ibfk_2');
		});
	}

}
