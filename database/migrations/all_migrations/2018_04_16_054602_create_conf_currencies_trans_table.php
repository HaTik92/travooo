<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfCurrenciesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conf_currencies_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('currencies_id')->index('currencies_id');
			$table->integer('languages_id')->index('languages_id');
			$table->string('title');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conf_currencies_trans');
	}

}
