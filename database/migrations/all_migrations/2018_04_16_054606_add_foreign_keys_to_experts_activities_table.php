<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExpertsActivitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('experts_activities', function(Blueprint $table)
		{
			$table->foreign('users_id', 'experts_activities_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('activities_id', 'experts_activities_ibfk_2')->references('id')->on('activities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('experts_activities', function(Blueprint $table)
		{
			$table->dropForeign('experts_activities_ibfk_1');
			$table->dropForeign('experts_activities_ibfk_2');
		});
	}

}
