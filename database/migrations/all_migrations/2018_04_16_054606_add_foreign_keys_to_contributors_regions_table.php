<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContributorsRegionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contributors_regions', function(Blueprint $table)
		{
			$table->foreign('users_id', 'contributors_regions_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('regions_id', 'contributors_regions_ibfk_2')->references('id')->on('conf_religions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contributors_regions', function(Blueprint $table)
		{
			$table->dropForeign('contributors_regions_ibfk_1');
			$table->dropForeign('contributors_regions_ibfk_2');
		});
	}

}
