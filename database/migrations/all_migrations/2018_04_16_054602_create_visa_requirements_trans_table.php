<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisaRequirementsTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visa_requirements_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('visa_requirements_ids')->index('visa_requirements_ids');
			$table->integer('languages_ids')->index('languages_ids');
			$table->string('title');
			$table->text('description', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visa_requirements_trans');
	}

}
