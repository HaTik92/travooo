<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRestaurantsTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('restaurants_trans', function(Blueprint $table)
		{
			$table->foreign('restaurants_id', 'restaurants_trans_ibfk_1')->references('id')->on('restaurants')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('languages_id', 'restaurants_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('restaurants_trans', function(Blueprint $table)
		{
			$table->dropForeign('restaurants_trans_ibfk_1');
			$table->dropForeign('restaurants_trans_ibfk_2');
		});
	}

}
