<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRestaurantsMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('restaurants_medias', function(Blueprint $table)
		{
			$table->foreign('restaurants_id', 'restaurants_medias_ibfk_1')->references('id')->on('restaurants')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('medias_id', 'restaurants_medias_ibfk_2')->references('id')->on('medias')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('restaurants_medias', function(Blueprint $table)
		{
			$table->dropForeign('restaurants_medias_ibfk_1');
			$table->dropForeign('restaurants_medias_ibfk_2');
		});
	}

}
