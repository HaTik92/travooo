<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMediasTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('medias_trans', function(Blueprint $table)
		{
			$table->foreign('medias_id', 'medias_trans_ibfk_1')->references('id')->on('medias')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('languages_id', 'medias_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('medias_trans', function(Blueprint $table)
		{
			$table->dropForeign('medias_trans_ibfk_1');
			$table->dropForeign('medias_trans_ibfk_2');
		});
	}

}
