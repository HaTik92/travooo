<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeyReferencesConfRegionsTransForCascadeDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('conf_regions_trans',function (Blueprint $table) {
            if (Schema::hasColumn('conf_regions_trans', 'conf_regions_trans_ibfk_1')) {
                $table->dropForeign('conf_regions_trans_ibfk_1');
            }
            $table->foreign('regions_id', 'conf_regions_trans_ibfk_1')->references('id')->on('regions')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('conf_regions_trans',function (Blueprint $table) {
            if (Schema::hasColumn('conf_regions_trans', 'conf_regions_trans_ibfk_1')) {
                $table->dropForeign('conf_regions_trans_ibfk_1');
            }
            $table->foreign('regions_id', 'conf_regions_trans_ibfk_1')->references('id')->on('regions')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
        Schema::enableForeignKeyConstraints();
    }
}
