<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersLevelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_levels', function(Blueprint $table)
		{
			$table->foreign('levels_id', 'users_levels_ibfk_1')->references('id')->on('conf_levels')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('users_id', 'users_levels_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_levels', function(Blueprint $table)
		{
			$table->dropForeign('users_levels_ibfk_1');
			$table->dropForeign('users_levels_ibfk_2');
		});
	}

}
