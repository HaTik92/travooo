<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activities', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('types_id')->index('types_id');
			$table->integer('countries_id')->index('countries_id');
			$table->integer('cities_id')->index('cities_id');
			$table->integer('places_id')->index('places_id');
			$table->decimal('lat', 10, 8);
			$table->decimal('lng', 11, 8);
			$table->integer('safety_degrees_id');
			$table->boolean('active');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activities');
	}

}
