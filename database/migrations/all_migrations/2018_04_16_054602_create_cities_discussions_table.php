<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesDiscussionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities_discussions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('thread_id');
			$table->integer('parent_id');
			$table->integer('cities_id');
			$table->integer('users_id');
			$table->text('text', 65535);
			$table->integer('upvotes');
			$table->integer('downvotes');
			$table->integer('replies');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities_discussions');
	}

}
