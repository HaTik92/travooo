<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeyReferencesHotelsTransForCascadeDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('hotels_trans',function (Blueprint $table) {
            $table->foreign('hotels_id', 'hotels_trans_ibfk_1')->references('id')->on('hotels')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('hotels_trans',function (Blueprint $table) {
            $table->foreign('hotels_id', 'hotels_trans_ibfk_1')->references('id')->on('hotels')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
        Schema::enableForeignKeyConstraints();
    }
}
