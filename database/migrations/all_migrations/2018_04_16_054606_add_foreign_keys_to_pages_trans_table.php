<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPagesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pages_trans', function(Blueprint $table)
		{
			$table->foreign('pages_id', 'pages_trans_ibfk_1')->references('id')->on('pages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('languages_id', 'pages_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pages_trans', function(Blueprint $table)
		{
			$table->dropForeign('pages_trans_ibfk_1');
			$table->dropForeign('pages_trans_ibfk_2');
		});
	}

}
