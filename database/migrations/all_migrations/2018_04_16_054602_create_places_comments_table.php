<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlacesCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('places_comments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('places_id')->index('places_id');
			$table->integer('users_id')->unsigned()->index('users_id');
			$table->text('text', 65535);
			$table->dateTime('time');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('places_comments');
	}

}
