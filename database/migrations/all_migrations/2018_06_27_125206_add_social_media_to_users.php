<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialMediaToUsers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users', function (Blueprint $table) {
            if (!Schema::hasColumn('users', 'website')) {
                $table->string('website')->after('display_name')->nullable()->default(null);
            }
            if (!Schema::hasColumn('users', 'facebook')) {
                $table->string('facebook')->after('website')->nullable()->default(null);
            }
            if (!Schema::hasColumn('users', 'twitter')) {
                $table->string('twitter')->after('facebook')->nullable()->default(null);
            }
            if (!Schema::hasColumn('users', 'instagram')) {
                $table->string('instagram')->after('twitter')->nullable()->default(null);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::table('users', function (Blueprint $table) {
            if (Schema::hasColumn('users', 'website')) {
                $table->dropColumn('website');
            }
            if (Schema::hasColumn('users', 'facebook')) {
                $table->dropColumn('facebook');
            }
            if (Schema::hasColumn('users', 'twitter')) {
                $table->dropColumn('twitter');
            }
            if (Schema::hasColumn('users', 'instagram')) {
                $table->dropColumn('instagram');
            }
        });
    }

}
