<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPagesAdminsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pages_admins', function(Blueprint $table)
		{
			$table->foreign('pages_id', 'pages_admins_ibfk_1')->references('id')->on('pages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('users_id', 'pages_admins_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pages_admins', function(Blueprint $table)
		{
			$table->dropForeign('pages_admins_ibfk_1');
			$table->dropForeign('pages_admins_ibfk_2');
		});
	}

}
