<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesDiscussionsDownvotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places_discussions_downvotes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('discussions_id');
            $table->integer('users_id');
            $table->string('ipaddress');
            $table->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places_discussions_downvotes');
    }
}
