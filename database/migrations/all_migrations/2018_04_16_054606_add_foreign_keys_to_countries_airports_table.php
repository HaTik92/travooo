<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCountriesAirportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('countries_airports', function(Blueprint $table)
		{
			$table->foreign('countries_id', 'countries_airports_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('places_id', 'countries_airports_ibfk_2')->references('id')->on('places')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('countries_airports', function(Blueprint $table)
		{
			$table->dropForeign('countries_airports_ibfk_1');
			$table->dropForeign('countries_airports_ibfk_2');
		});
	}

}
