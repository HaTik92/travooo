<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMediasSharesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('medias_shares', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('medias_id')->index('medias_id');
			$table->integer('users_id')->unsigned()->index('users_id');
			$table->integer('scope');
			$table->dateTime('created_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('medias_shares');
	}

}
