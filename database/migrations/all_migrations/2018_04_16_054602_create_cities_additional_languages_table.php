<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesAdditionalLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities_additional_languages', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cities_id')->nullable()->index('cities_id');
			$table->integer('languages_spoken_id')->nullable()->index('languages_spoken_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities_additional_languages');
	}

}
