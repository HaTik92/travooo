<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesDiscussionsUpvotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities_discussions_upvotes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('discussions_id');
			$table->integer('users_id');
			$table->string('ipaddress', 191);
			$table->dateTime('created_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities_discussions_upvotes');
	}

}
