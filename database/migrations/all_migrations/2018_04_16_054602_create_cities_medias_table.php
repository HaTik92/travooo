<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities_medias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cities_id')->index('cities_id');
			$table->integer('medias_id')->index('medias_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities_medias');
	}

}
