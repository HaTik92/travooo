<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHotelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hotels', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('countries_id')->index('countries_id');
			$table->integer('cities_id')->index('cities_id');
			$table->string('place_type')->nullable();
			$table->integer('places_id')->index('places_id');
			$table->decimal('lat', 10, 8);
			$table->decimal('lng', 11, 8);
			$table->string('provider_id')->nullable()->index('provider_id');
			$table->decimal('rating', 2, 1)->nullable();
			$table->boolean('active');
			$table->integer('media_done')->nullable();
			$table->integer('cover_media_id')->nullable()->index('cover_media_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hotels');
	}

}
