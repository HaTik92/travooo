<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPagesNotificationSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pages_notification_settings', function(Blueprint $table)
		{
			$table->foreign('pages_id', 'pages_notification_settings_ibfk_1')->references('id')->on('pages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pages_notification_settings', function(Blueprint $table)
		{
			$table->dropForeign('pages_notification_settings_ibfk_1');
		});
	}

}
