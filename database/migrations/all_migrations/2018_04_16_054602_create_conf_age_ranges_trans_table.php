<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfAgeRangesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conf_age_ranges_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('age_ranges_id')->index('age_ranges_id');
			$table->integer('languages_id')->index('languages_id');
			$table->string('title');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conf_age_ranges_trans');
	}

}
