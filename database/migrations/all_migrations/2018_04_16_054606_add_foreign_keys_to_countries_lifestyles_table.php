<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCountriesLifestylesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('countries_lifestyles', function(Blueprint $table)
		{
			$table->foreign('countries_id', 'countries_lifestyles_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('lifestyles_id', 'countries_lifestyles_ibfk_2')->references('id')->on('conf_lifestyles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('countries_lifestyles', function(Blueprint $table)
		{
			$table->dropForeign('countries_lifestyles_ibfk_1');
			$table->dropForeign('countries_lifestyles_ibfk_2');
		});
	}

}
