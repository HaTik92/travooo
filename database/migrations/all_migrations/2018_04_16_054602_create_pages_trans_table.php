<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pages_id')->index('pages_id');
			$table->integer('languages_id')->index('languages_id');
			$table->string('title');
			$table->text('description', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages_trans');
	}

}
