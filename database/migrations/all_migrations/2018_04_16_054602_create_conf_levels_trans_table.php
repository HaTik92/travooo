<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfLevelsTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conf_levels_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('levels_id')->index('levels_id');
			$table->integer('languages_id')->index('languages_id');
			$table->string('title');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conf_levels_trans');
	}

}
