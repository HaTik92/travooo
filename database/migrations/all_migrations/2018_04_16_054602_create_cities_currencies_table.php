<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesCurrenciesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities_currencies', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cities_id')->index('cities_id');
			$table->integer('currencies_id')->index('currencies_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities_currencies');
	}

}
