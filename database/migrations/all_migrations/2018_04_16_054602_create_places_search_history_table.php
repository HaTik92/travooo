<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlacesSearchHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('places_search_history', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('admin_id')->unsigned()->index('admin_id');
			$table->decimal('lat', 10, 8)->index('lat_2');
			$table->decimal('lng', 11, 8)->index('lng_2');
			$table->string('time');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('places_search_history');
	}

}
