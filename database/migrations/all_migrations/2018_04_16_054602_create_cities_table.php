<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('countries_id')->index('countries_id');
			$table->integer('code')->nullable();
			$table->decimal('lat', 10, 8);
			$table->decimal('lng', 11, 8);
			$table->boolean('is_capital');
			$table->integer('safety_degree_id')->nullable();
			$table->integer('level_of_living_id');
			$table->integer('active')->index('active');
			$table->integer('cover_media_id')->nullable()->index('cover_media_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities');
	}

}
