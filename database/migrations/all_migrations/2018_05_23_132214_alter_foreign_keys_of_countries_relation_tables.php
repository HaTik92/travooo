<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeysOfCountriesRelationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries_religions', function(Blueprint $table)
        {
            $table->dropForeign('countries_religions_ibfk_1');
            $table->dropForeign('countries_religions_ibfk_2');
            $table->foreign('countries_id', 'countries_religions_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('religions_id', 'countries_religions_ibfk_2')->references('id')->on('conf_religions')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('countries_capitals', function(Blueprint $table)
        {
            $table->dropForeign('countries_capitals_ibfk_1');
            $table->dropForeign('countries_capitals_ibfk_2');
            $table->foreign('countries_id', 'countries_capitals_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('cities_id', 'countries_capitals_ibfk_2')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('countries_currencies', function(Blueprint $table)
        {
            $table->dropForeign('countries_currencies_ibfk_1');
            $table->dropForeign('countries_currencies_ibfk_2');
            $table->foreign('countries_id', 'countries_currencies_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('currencies_id', 'countries_currencies_ibfk_2')->references('id')->on('conf_currencies')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('countries_emergency_numbers', function(Blueprint $table)
        {
            $table->dropForeign('countries_emergency_numbers_ibfk_1');
            $table->dropForeign('countries_emergency_numbers_ibfk_2');
            $table->foreign('countries_id', 'countries_emergency_numbers_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('emergency_numbers_id', 'countries_emergency_numbers_ibfk_2')->references('id')->on('conf_emergency_numbers')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('countries_holidays', function(Blueprint $table)
        {
            $table->dropForeign('countries_holidays_ibfk_1');
            $table->dropForeign('countries_holidays_ibfk_2');
            $table->foreign('countries_id', 'countries_holidays_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('holidays_id', 'countries_holidays_ibfk_2')->references('id')->on('conf_holidays')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('countries_langugaes_spoken', function(Blueprint $table)
        {
            $table->dropForeign('countries_langugaes_spoken_ibfk_1');
            $table->dropForeign('countries_langugaes_spoken_ibfk_2');
            $table->foreign('countries_id', 'countries_langugaes_spoken_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('languages_spoken_id', 'countries_langugaes_spoken_ibfk_2')->references('id')->on('conf_languages_spoken')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('countries_lifestyles', function(Blueprint $table)
        {
            $table->dropForeign('countries_lifestyles_ibfk_1');
            $table->dropForeign('countries_lifestyles_ibfk_2');
            $table->foreign('countries_id', 'countries_lifestyles_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('lifestyles_id', 'countries_lifestyles_ibfk_2')->references('id')->on('conf_lifestyles')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries_religions', function(Blueprint $table)
        {
            $table->dropForeign('countries_religions_ibfk_1');
            $table->dropForeign('countries_religions_ibfk_2');
            $table->foreign('countries_id', 'countries_religions_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('religions_id', 'countries_religions_ibfk_2')->references('id')->on('conf_religions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('countries_capitals', function(Blueprint $table)
        {
            $table->dropForeign('countries_capitals_ibfk_1');
            $table->dropForeign('countries_capitals_ibfk_2');
            $table->foreign('countries_id', 'countries_capitals_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('cities_id', 'countries_capitals_ibfk_2')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('countries_currencies', function(Blueprint $table)
        {
            $table->dropForeign('countries_currencies_ibfk_1');
            $table->dropForeign('countries_currencies_ibfk_2');
            $table->foreign('countries_id', 'countries_currencies_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('currencies_id', 'countries_currencies_ibfk_2')->references('id')->on('conf_currencies')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('countries_emergency_numbers', function(Blueprint $table)
        {
            $table->dropForeign('countries_emergency_numbers_ibfk_1');
            $table->dropForeign('countries_emergency_numbers_ibfk_2');
            $table->foreign('countries_id', 'countries_emergency_numbers_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('emergency_numbers_id', 'countries_emergency_numbers_ibfk_2')->references('id')->on('conf_emergency_numbers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('countries_holidays', function(Blueprint $table)
        {
            $table->dropForeign('countries_holidays_ibfk_1');
            $table->dropForeign('countries_holidays_ibfk_2');
            $table->foreign('countries_id', 'countries_holidays_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('holidays_id', 'countries_holidays_ibfk_2')->references('id')->on('conf_holidays')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('countries_langugaes_spoken', function(Blueprint $table)
        {
            $table->dropForeign('countries_langugaes_spoken_ibfk_1');
            $table->dropForeign('countries_langugaes_spoken_ibfk_2');
            $table->foreign('countries_id', 'countries_langugaes_spoken_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('languages_spoken_id', 'countries_langugaes_spoken_ibfk_2')->references('id')->on('conf_languages_spoken')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('countries_lifestyles', function(Blueprint $table)
        {
            $table->dropForeign('countries_lifestyles_ibfk_1');
            $table->dropForeign('countries_lifestyles_ibfk_2');
            $table->foreign('countries_id', 'countries_lifestyles_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('lifestyles_id', 'countries_lifestyles_ibfk_2')->references('id')->on('conf_lifestyles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }
}
