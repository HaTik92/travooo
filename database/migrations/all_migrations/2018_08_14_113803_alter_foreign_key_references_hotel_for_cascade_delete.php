<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeyReferencesHotelForCascadeDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('hotels',function (Blueprint $table) {
            if (Schema::hasColumn('hotels', 'hotels_ibfk_3')) {
                $table->dropForeign('hotels_ibfk_3');
            }
            $table->foreign('places_id', 'hotels_ibfk_3')->references('id')->on('places')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotels',function (Blueprint $table) {
            if (Schema::hasColumn('hotels', 'hotels_ibfk_3')) {
                $table->dropForeign('hotels_ibfk_3');
            }
            $table->foreign('places_id', 'hotels_ibfk_3')->references('id')->on('places')->onDelete('CASCADE');
        });
    }
}
