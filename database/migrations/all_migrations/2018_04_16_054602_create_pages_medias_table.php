<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages_medias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pages_ids')->index('pages_ids');
			$table->integer('medias_ids')->index('medias_ids');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages_medias');
	}

}
