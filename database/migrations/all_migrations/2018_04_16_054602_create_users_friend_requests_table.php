<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersFriendRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_friend_requests', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('from')->unsigned();
			$table->integer('to')->unsigned();
			$table->integer('status');
			$table->dateTime('created_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_friend_requests');
	}

}
