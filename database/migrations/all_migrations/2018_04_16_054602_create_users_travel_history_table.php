<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTravelHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_travel_history', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('countries_id')->nullable()->index('countries_id');
			$table->integer('cities_id')->nullable()->index('cities_id');
			$table->integer('places_id')->nullable()->index('places_id');
			$table->dateTime('time');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_travel_history');
	}

}
