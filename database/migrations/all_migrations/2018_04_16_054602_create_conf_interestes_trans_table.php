<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfInterestesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conf_interestes_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('interests_id')->index('interests_id');
			$table->integer('languages_id')->index('languages_id');
			$table->string('title');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conf_interestes_trans');
	}

}
