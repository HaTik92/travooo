<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCitiesReligionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cities_religions', function(Blueprint $table)
		{
			$table->foreign('cities_id', 'cities_religions_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('religions_id', 'cities_religions_ibfk_2')->references('id')->on('conf_religions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cities_religions', function(Blueprint $table)
		{
			$table->dropForeign('cities_religions_ibfk_1');
			$table->dropForeign('cities_religions_ibfk_2');
		});
	}

}
