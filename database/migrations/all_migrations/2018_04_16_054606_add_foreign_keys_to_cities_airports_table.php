<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCitiesAirportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cities_airports', function(Blueprint $table)
		{
			$table->foreign('cities_id', 'cities_airports_ibfk_3')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('places_id', 'cities_airports_ibfk_4')->references('id')->on('places')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cities_airports', function(Blueprint $table)
		{
			$table->dropForeign('cities_airports_ibfk_3');
			$table->dropForeign('cities_airports_ibfk_4');
		});
	}

}
