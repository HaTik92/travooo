<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersBlocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_blocks', function(Blueprint $table)
		{
			$table->foreign('users_id', 'users_blocks_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('blocks_id', 'users_blocks_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_blocks', function(Blueprint $table)
		{
			$table->dropForeign('users_blocks_ibfk_1');
			$table->dropForeign('users_blocks_ibfk_2');
		});
	}

}
