<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLenguagesIdForeignKeysToEmbassiesTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('embassies_trans', function(Blueprint $table)
        {
            $table->dropForeign('embassies_trans_ibfk_2');
            $table->foreign('languages_id', 'embassies_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('embassies_trans', function(Blueprint $table)
        {
            $table->dropForeign('embassies_trans_ibfk_2');
            $table->foreign('languages_id', 'embassies_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }
}
