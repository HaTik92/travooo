<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmbassiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('embassies', function(Blueprint $table)
		{
			$table->foreign('countries_id', 'embassies_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cover_media_id', 'embassies_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('embassies', function(Blueprint $table)
		{
			$table->dropForeign('embassies_ibfk_1');
			$table->dropForeign('embassies_ibfk_2');
		});
	}

}
