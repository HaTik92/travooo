<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTripsUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trips_users', function(Blueprint $table)
		{
			$table->foreign('trips_id', 'trips_users_ibfk_1')->references('id')->on('trips')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('users_id', 'trips_users_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trips_users', function(Blueprint $table)
		{
			$table->dropForeign('trips_users_ibfk_1');
			$table->dropForeign('trips_users_ibfk_2');
		});
	}

}
