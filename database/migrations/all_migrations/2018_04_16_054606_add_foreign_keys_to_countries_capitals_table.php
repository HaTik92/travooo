<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCountriesCapitalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('countries_capitals', function(Blueprint $table)
		{
			$table->foreign('countries_id', 'countries_capitals_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cities_id', 'countries_capitals_ibfk_2')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('countries_capitals', function(Blueprint $table)
		{
			$table->dropForeign('countries_capitals_ibfk_1');
			$table->dropForeign('countries_capitals_ibfk_2');
		});
	}

}
