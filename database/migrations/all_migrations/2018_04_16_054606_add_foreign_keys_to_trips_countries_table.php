<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTripsCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trips_countries', function(Blueprint $table)
		{
			$table->foreign('trips_id', 'trips_countries_ibfk_1')->references('id')->on('trips')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('countries_id', 'trips_countries_ibfk_2')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trips_countries', function(Blueprint $table)
		{
			$table->dropForeign('trips_countries_ibfk_1');
			$table->dropForeign('trips_countries_ibfk_2');
		});
	}

}
