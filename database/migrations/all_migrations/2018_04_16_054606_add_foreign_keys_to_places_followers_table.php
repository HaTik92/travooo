<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlacesFollowersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('places_followers', function(Blueprint $table)
		{
			$table->foreign('places_id', 'places_followers_ibfk_1')->references('id')->on('places')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('users_id', 'places_followers_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('places_followers', function(Blueprint $table)
		{
			$table->dropForeign('places_followers_ibfk_1');
			$table->dropForeign('places_followers_ibfk_2');
		});
	}

}
