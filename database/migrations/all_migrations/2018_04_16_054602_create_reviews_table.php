<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reviews', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cities_id')->index('cities_id');
			$table->integer('countries_id')->index('countries_id');
			$table->integer('embassies_id')->index('embassies_id');
			$table->integer('medias_id')->index('medias_id');
			$table->integer('pages_id')->index('pages_id');
			$table->integer('places_id')->index('places_id');
			$table->integer('users_id')->unsigned()->index('users_id');
			$table->integer('by_users_id')->unsigned()->index('by_users_id');
			$table->string('title');
			$table->text('description', 65535);
			$table->boolean('active');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reviews');
	}

}
