<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTripsCitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trips_cities', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('trips_id')->index('trips_id');
			$table->integer('cities_id')->index('cities_id');
			$table->date('date')->nullable();
			$table->integer('order')->nullable();
			$table->integer('active')->default(0);
			$table->text('transportation', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trips_cities');
	}

}
