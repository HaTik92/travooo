<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmbassiesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('embassies_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('embassies_id')->index('embassies_id');
			$table->integer('languages_id')->index('languages_id');
			$table->string('title');
			$table->string('description')->nullable();
			$table->text('working_days', 65535)->nullable();
			$table->string('phone')->nullable();
			$table->string('address')->nullable();
			$table->text('highlights', 65535)->nullable();
			$table->text('working_times', 65535)->nullable();
			$table->text('how_to_go', 65535)->nullable();
			$table->text('when_to_go', 65535)->nullable();
			$table->text('num_activities', 65535)->nullable();
			$table->text('popularity', 65535)->nullable();
			$table->text('conditions', 65535)->nullable();
			$table->text('price_level', 65535)->nullable();
			$table->text('num_checkins', 65535)->nullable();
			$table->text('history', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('embassies_trans');
	}

}
