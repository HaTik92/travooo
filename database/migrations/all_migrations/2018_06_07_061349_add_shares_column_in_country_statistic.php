<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSharesColumnInCountryStatistic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries_statistics', function (Blueprint $table) {
            if (!Schema::hasColumn('countries_statistics', 'shares')) {
                $table->integer('shares')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries_statistics', function (Blueprint $table) {
            if (Schema::hasColumn('countries_statistics', 'shares')) {
                $table->dropColumn('shares');
            }
        });
    }
}
