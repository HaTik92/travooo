<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConfLanguagesSpokenTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('conf_languages_spoken_trans', function(Blueprint $table)
		{
			$table->foreign('languages_spoken_id', 'conf_languages_spoken_trans_ibfk_1')->references('id')->on('conf_languages_spoken')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('languages_id', 'conf_languages_spoken_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conf_languages_spoken_trans', function(Blueprint $table)
		{
			$table->dropForeign('conf_languages_spoken_trans_ibfk_1');
			$table->dropForeign('conf_languages_spoken_trans_ibfk_2');
		});
	}

}
