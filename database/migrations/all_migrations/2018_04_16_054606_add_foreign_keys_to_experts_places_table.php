<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExpertsPlacesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('experts_places', function(Blueprint $table)
		{
			$table->foreign('places_id', 'experts_places_ibfk_1')->references('id')->on('places')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('users_id', 'experts_places_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('experts_places', function(Blueprint $table)
		{
			$table->dropForeign('experts_places_ibfk_1');
			$table->dropForeign('experts_places_ibfk_2');
		});
	}

}
