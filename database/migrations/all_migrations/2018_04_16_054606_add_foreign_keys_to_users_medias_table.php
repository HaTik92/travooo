<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_medias', function(Blueprint $table)
		{
			$table->foreign('users_id', 'users_medias_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('medias_id', 'users_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_medias', function(Blueprint $table)
		{
			$table->dropForeign('users_medias_ibfk_1');
			$table->dropForeign('users_medias_ibfk_2');
		});
	}

}
