<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCountriesLangugaesSpokenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('countries_langugaes_spoken', function(Blueprint $table)
		{
			$table->foreign('countries_id', 'countries_langugaes_spoken_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('languages_spoken_id', 'countries_langugaes_spoken_ibfk_2')->references('id')->on('conf_languages_spoken')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('countries_langugaes_spoken', function(Blueprint $table)
		{
			$table->dropForeign('countries_langugaes_spoken_ibfk_1');
			$table->dropForeign('countries_langugaes_spoken_ibfk_2');
		});
	}

}
