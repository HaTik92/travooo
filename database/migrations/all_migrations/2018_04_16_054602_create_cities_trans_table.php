<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cities_id')->index('cities_id');
			$table->integer('languages_id')->index('languages_id');
			$table->string('title');
			$table->text('description', 65535)->nullable();
			$table->text('best_place', 65535)->nullable();
			$table->text('best_time', 65535)->nullable();
			$table->text('cost_of_living', 65535)->nullable();
			$table->text('geo_stats', 65535)->nullable();
			$table->text('demographics', 65535)->nullable();
			$table->text('economy', 65535)->nullable();
			$table->text('suitable_for', 65535)->nullable();
			$table->text('metrics', 65535)->nullable();
			$table->text('health_notes', 65535)->nullable();
			$table->text('accommodation', 65535)->nullable();
			$table->text('potential_dangers', 65535)->nullable();
			$table->text('local_poisons', 65535)->nullable();
			$table->text('sockets', 65535)->nullable();
			$table->text('working_days', 65535)->nullable();
			$table->text('restrictions', 65535)->nullable();
			$table->text('planning_tips', 65535)->nullable();
			$table->text('other', 65535)->nullable();
			$table->text('internet', 65535)->nullable();
			$table->text('speed_limit', 65535)->nullable();
			$table->text('etiquette', 65535)->nullable();
			$table->text('pollution_index', 65535)->nullable();
			$table->text('transportation', 65535)->nullable();
			$table->text('highlights', 65535)->nullable();
			$table->string('population', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities_trans');
	}

}
