<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesNotificationSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages_notification_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pages_id')->index('pages_id');
			$table->string('var');
			$table->string('val');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages_notification_settings');
	}

}
