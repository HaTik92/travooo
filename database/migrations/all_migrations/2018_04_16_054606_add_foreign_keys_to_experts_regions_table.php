<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExpertsRegionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('experts_regions', function(Blueprint $table)
		{
			$table->foreign('users_id', 'experts_regions_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('regions_id', 'experts_regions_ibfk_2')->references('id')->on('conf_regions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('experts_regions', function(Blueprint $table)
		{
			$table->dropForeign('experts_regions_ibfk_1');
			$table->dropForeign('experts_regions_ibfk_2');
		});
	}

}
