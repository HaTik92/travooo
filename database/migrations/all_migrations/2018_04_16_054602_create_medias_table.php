<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('medias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('url');
			$table->integer('type')->nullable();
			$table->boolean('featured')->nullable();
			$table->string('title')->nullable();
			$table->string('author_name')->nullable();
			$table->string('author_url')->nullable();
			$table->string('license_name')->nullable();
			$table->string('license_url')->nullable();
			$table->string('md5')->nullable()->index('md5');
			$table->string('sha1')->nullable()->index('sha1');
			$table->integer('filesize')->nullable()->index('filesize');
			$table->text('html_attributions', 65535)->nullable();
			$table->string('source_url')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('medias');
	}

}
