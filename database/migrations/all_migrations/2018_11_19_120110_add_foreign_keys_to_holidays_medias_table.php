<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToHolidaysMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('holidays_medias', function(Blueprint $table)
        {
            $table->foreign('holidays_id')->references('id')->on('conf_holidays')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('medias_id')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('holidays_medias', function(Blueprint $table)
        {
            $table->dropForeign('holidays_id');
            $table->dropForeign('medias_id');
        });
    }
}
