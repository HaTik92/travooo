<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRegionsMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('regions_medias', function(Blueprint $table)
		{
			$table->foreign('regions_id', 'regions_medias_ibfk_1')->references('id')->on('conf_regions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('medias_id', 'regions_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('regions_medias', function(Blueprint $table)
		{
			$table->dropForeign('regions_medias_ibfk_1');
			$table->dropForeign('regions_medias_ibfk_2');
		});
	}

}
