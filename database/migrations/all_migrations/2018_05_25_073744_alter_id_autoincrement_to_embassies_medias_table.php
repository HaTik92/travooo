<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterIdAutoincrementToEmbassiesMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE embassies_media MODIFY id INT NOT NULL PRIMARY KEY AUTO_INCREMENT");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE embassies_media MODIFY id INT NOT NULL");
        DB::statement("ALTER TABLE embassies_media DROP PRIMARY KEY");
        DB::statement("ALTER TABLE embassies_media MODIFY id INT NULL");
    }
}
