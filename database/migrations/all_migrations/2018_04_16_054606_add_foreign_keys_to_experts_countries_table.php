<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExpertsCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('experts_countries', function(Blueprint $table)
		{
			$table->foreign('users_id', 'experts_countries_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('countries_id', 'experts_countries_ibfk_2')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('experts_countries', function(Blueprint $table)
		{
			$table->dropForeign('experts_countries_ibfk_1');
			$table->dropForeign('experts_countries_ibfk_2');
		});
	}

}
