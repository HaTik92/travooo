<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugColumnInConfHolidaysTrans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conf_holidays_trans', function (Blueprint $table) {
            if (!Schema::hasColumn('conf_holidays_trans', 'slug')) {
                $table->string('slug', 255)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('conf_holidays_trans', function (Blueprint $table) {
            if (Schema::hasColumn('conf_holidays_trans', 'slug')) {
                $table->dropColumn('slug');
            }
        });
    }
}
