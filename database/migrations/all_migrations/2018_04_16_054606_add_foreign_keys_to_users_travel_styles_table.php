<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersTravelStylesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_travel_styles', function(Blueprint $table)
		{
			$table->foreign('users_id', 'users_travel_styles_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('conf_lifestyles_id', 'users_travel_styles_ibfk_2')->references('id')->on('conf_lifestyles')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_travel_styles', function(Blueprint $table)
		{
			$table->dropForeign('users_travel_styles_ibfk_1');
			$table->dropForeign('users_travel_styles_ibfk_2');
		});
	}

}
