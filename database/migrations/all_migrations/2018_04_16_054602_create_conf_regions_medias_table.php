<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfRegionsMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conf_regions_medias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('regions_id')->index('regions_id');
			$table->integer('medias_id')->index('medias_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conf_regions_medias');
	}

}
