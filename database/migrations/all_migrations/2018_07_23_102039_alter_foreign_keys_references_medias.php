<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeysReferencesMedias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_medias',function (Blueprint $table) {
            $table->dropForeign('users_medias_ibfk_2');
            $table->foreign('medias_id', 'users_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
        Schema::table('trips_places',function (Blueprint $table) {
            $table->dropForeign('trips_places_ibfk_4');
            $table->foreign('places_id', 'trips_places_ibfk_4')->references('id')->on('places')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
        Schema::table('pages_medias',function (Blueprint $table) {
            $table->dropForeign('pages_medias_ibfk_2');
            $table->foreign('medias_ids', 'pages_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_medias', function (Blueprint $table) {
            $table->dropForeign('users_medias_ibfk_2');
            $table->foreign('medias_id', 'users_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
        Schema::table('trips_places',function (Blueprint $table) {
            $table->dropForeign('trips_places_ibfk_4');
            $table->foreign('places_id', 'trips_places_ibfk_4')->references('id')->on('places')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
        Schema::table('pages_medias',function (Blueprint $table) {
            $table->dropForeign('pages_medias_ibfk_2');
            $table->foreign('medias_ids', 'pages_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }
}
