<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reviews', function(Blueprint $table)
		{
			$table->foreign('cities_id', 'reviews_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('countries_id', 'reviews_ibfk_2')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('embassies_id', 'reviews_ibfk_3')->references('id')->on('embassies')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('medias_id', 'reviews_ibfk_4')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('pages_id', 'reviews_ibfk_5')->references('id')->on('pages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('places_id', 'reviews_ibfk_6')->references('id')->on('places')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('users_id', 'reviews_ibfk_7')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('by_users_id', 'reviews_ibfk_8')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reviews', function(Blueprint $table)
		{
			$table->dropForeign('reviews_ibfk_1');
			$table->dropForeign('reviews_ibfk_2');
			$table->dropForeign('reviews_ibfk_3');
			$table->dropForeign('reviews_ibfk_4');
			$table->dropForeign('reviews_ibfk_5');
			$table->dropForeign('reviews_ibfk_6');
			$table->dropForeign('reviews_ibfk_7');
			$table->dropForeign('reviews_ibfk_8');
		});
	}

}
