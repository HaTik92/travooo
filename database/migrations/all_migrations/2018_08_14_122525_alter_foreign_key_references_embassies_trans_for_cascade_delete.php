<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeyReferencesEmbassiesTransForCascadeDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('embassies_trans',function (Blueprint $table) {
            if (Schema::hasColumn('embassies_trans', 'embassies_trans_ibfk_1')) {
                $table->dropForeign('embassies_trans_ibfk_1');
            }
            $table->foreign('embassies_id', 'embassies_trans_ibfk_1')->references('id')->on('embassies')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('embassies_trans',function (Blueprint $table) {
            if (Schema::hasColumn('embassies_trans', 'embassies_trans_ibfk_1')) {
                $table->dropForeign('embassies_trans_ibfk_1');
            }
            $table->foreign('embassies_id', 'embassies_trans_ibfk_1')->references('id')->on('embassies')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
        Schema::enableForeignKeyConstraints();
    }
}
