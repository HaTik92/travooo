<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHotelsTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotels_trans', function(Blueprint $table)
		{
			$table->foreign('hotels_id', 'hotels_trans_ibfk_1')->references('id')->on('hotels')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('languages_id', 'hotels_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotels_trans', function(Blueprint $table)
		{
			$table->dropForeign('hotels_trans_ibfk_1');
			$table->dropForeign('hotels_trans_ibfk_2');
		});
	}

}
