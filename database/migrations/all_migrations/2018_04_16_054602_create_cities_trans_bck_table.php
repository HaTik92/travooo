<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesTransBckTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities_trans_bck', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cities_id')->index('cities_id');
			$table->integer('languages_id')->index('languages_id');
			$table->string('title');
			$table->text('description', 65535);
			$table->text('best_place', 65535);
			$table->text('best_time', 65535);
			$table->text('cost_of_living', 65535);
			$table->text('geo_stats', 65535);
			$table->text('demographics', 65535);
			$table->text('economy', 65535);
			$table->text('suitable_for', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities_trans_bck');
	}

}
