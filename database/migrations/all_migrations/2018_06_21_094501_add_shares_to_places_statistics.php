<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSharesToPlacesStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('places_statistics', function (Blueprint $table) {
            if (!Schema::hasColumn('places_statistics', 'shares')) {
                $table->integer('shares')->nullable()->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('places_statistics', function (Blueprint $table) {
            if (Schema::hasColumn('places_statistics', 'shares')) {
                $table->dropColumn('shares');
            }
        });
    }
}
