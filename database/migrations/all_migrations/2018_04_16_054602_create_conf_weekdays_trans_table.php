<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfWeekdaysTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conf_weekdays_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('weekdays_id')->index('weekdays_id');
			$table->integer('languages_id')->index('languages_id');
			$table->string('title');
			$table->text('description', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conf_weekdays_trans');
	}

}
