<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRestaurantsSearchHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('restaurants_search_history', function(Blueprint $table)
		{
			$table->foreign('admin_id', 'restaurants_search_history_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('restaurants_search_history', function(Blueprint $table)
		{
			$table->dropForeign('restaurants_search_history_ibfk_1');
		});
	}

}
