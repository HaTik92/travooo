<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCitiesFollowersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cities_followers', function(Blueprint $table)
		{
			$table->foreign('cities_id', 'cities_followers_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('users_id', 'cities_followers_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cities_followers', function(Blueprint $table)
		{
			$table->dropForeign('cities_followers_ibfk_1');
			$table->dropForeign('cities_followers_ibfk_2');
		});
	}

}
