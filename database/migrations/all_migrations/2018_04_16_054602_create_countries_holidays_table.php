<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesHolidaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries_holidays', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('countries_id')->index('countries_id');
			$table->integer('holidays_id')->index('holidays_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('countries_holidays');
	}

}
