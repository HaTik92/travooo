<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfLifestylesMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conf_lifestyles_medias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('lifestyles_id')->nullable()->index('lifestyles_id');
			$table->integer('medias_id')->nullable()->index('medias_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conf_lifestyles_medias');
	}

}
