<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConfSafetyDegreesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('conf_safety_degrees_trans', function(Blueprint $table)
		{
			$table->foreign('safety_degrees_id', 'conf_safety_degrees_trans_ibfk_1')->references('id')->on('conf_safety_degrees')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('languages_id', 'conf_safety_degrees_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conf_safety_degrees_trans', function(Blueprint $table)
		{
			$table->dropForeign('conf_safety_degrees_trans_ibfk_1');
			$table->dropForeign('conf_safety_degrees_trans_ibfk_2');
		});
	}

}
