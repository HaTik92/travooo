<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHotelsMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotels_medias', function(Blueprint $table)
		{
			$table->foreign('hotels_id', 'hotels_medias_ibfk_1')->references('id')->on('hotels')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('medias_id', 'hotels_medias_ibfk_2')->references('id')->on('medias')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotels_medias', function(Blueprint $table)
		{
			$table->dropForeign('hotels_medias_ibfk_1');
			$table->dropForeign('hotels_medias_ibfk_2');
		});
	}

}
