<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContributorsPlacesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contributors_places', function(Blueprint $table)
		{
			$table->foreign('users_id', 'contributors_places_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('places_id', 'contributors_places_ibfk_2')->references('id')->on('places')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contributors_places', function(Blueprint $table)
		{
			$table->dropForeign('contributors_places_ibfk_1');
			$table->dropForeign('contributors_places_ibfk_2');
		});
	}

}
