<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUploadedAtColumnToMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medias', function (Blueprint $table) {
            if (!Schema::hasColumn('medias', 'uploaded_at')) {
                $table->timestamp('uploaded_at')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medias', function (Blueprint $table) {
            if (Schema::hasColumn('medias', 'uploaded_at')) {
                $table->dropColumn('uploaded_at');
            }
        });
    }
}
