<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCountriesMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('countries_medias', function(Blueprint $table)
		{
			$table->foreign('countries_id', 'countries_medias_ibfk_1')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('medias_id', 'countries_medias_ibfk_2')->references('id')->on('medias')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('countries_medias', function(Blueprint $table)
		{
			$table->dropForeign('countries_medias_ibfk_1');
			$table->dropForeign('countries_medias_ibfk_2');
		});
	}

}
