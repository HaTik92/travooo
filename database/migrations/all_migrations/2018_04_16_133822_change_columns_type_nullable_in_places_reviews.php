<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsTypeNullableInPlacesReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('places_reviews', function (Blueprint $table) {
            $table->text('comment')->nullable()->change();
            $table->string('ip_address')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('places_reviews', function (Blueprint $table) {
            $table->text('comment')->nullable(false)->change();
            $table->string('ip_address')->nullable(false)->change();
        });
    }
}
