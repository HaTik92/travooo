<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191)->nullable();
			$table->string('username', 256)->nullable();
			$table->string('email', 191)->unique();
			$table->text('address')->nullable();
			$table->boolean('single')->nullable();
			$table->boolean('gender')->nullable();
			$table->integer('children')->nullable();
			$table->integer('age')->nullable();
			$table->string('profile_picture', 256)->nullable();
			$table->string('mobile', 256)->nullable();
			$table->boolean('status')->default(1);
			$table->string('nationality', 256)->nullable();
			$table->boolean('public_profile')->nullable();
			$table->boolean('notifications')->nullable();
			$table->boolean('messages')->nullable();
			$table->boolean('sms')->nullable();
			$table->string('password', 191);
			$table->string('confirmation_code', 191)->nullable();
			$table->boolean('confirmed')->nullable()->default(0);
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->dateTime('last_login')->nullable();
			$table->integer('travel_type')->nullable();
			$table->string('display_name', 256)->nullable();
			$table->text('password_reset_token', 65535)->nullable();
			$table->integer('login_type')->nullable()->default(1);
			$table->string('social_key', 256)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
