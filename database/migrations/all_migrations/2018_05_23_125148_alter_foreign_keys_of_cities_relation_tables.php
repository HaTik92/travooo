<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeysOfCitiesRelationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities_currencies', function(Blueprint $table)
        {
            $table->dropForeign('cities_currencies_ibfk_1');
            $table->dropForeign('cities_currencies_ibfk_2');
            $table->foreign('cities_id', 'cities_currencies_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('currencies_id', 'cities_currencies_ibfk_2')->references('id')->on('conf_currencies')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('cities_emergency_numbers', function(Blueprint $table)
        {
            $table->dropForeign('cities_emergency_numbers_ibfk_1');
            $table->dropForeign('cities_emergency_numbers_ibfk_2');
            $table->foreign('cities_id', 'cities_emergency_numbers_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('emergency_numbers_id', 'cities_emergency_numbers_ibfk_2')->references('id')->on('conf_emergency_numbers')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('cities_holidays', function(Blueprint $table)
        {
            $table->dropForeign('cities_holidays_ibfk_1');
            $table->dropForeign('cities_holidays_ibfk_2');
            $table->foreign('cities_id', 'cities_holidays_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('holidays_id', 'cities_holidays_ibfk_2')->references('id')->on('conf_holidays')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('cities_languages_spoken', function(Blueprint $table)
        {
            $table->dropForeign('cities_languages_spoken_ibfk_1');
            $table->dropForeign('cities_languages_spoken_ibfk_2');
            $table->foreign('cities_id', 'cities_languages_spoken_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('languages_spoken_id', 'cities_languages_spoken_ibfk_2')->references('id')->on('conf_languages_spoken')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('cities_lifestyles', function(Blueprint $table)
        {
            $table->dropForeign('cities_lifestyles_ibfk_1');
            $table->dropForeign('cities_lifestyles_ibfk_2');
            $table->foreign('cities_id', 'cities_lifestyles_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('lifestyles_id', 'cities_lifestyles_ibfk_2')->references('id')->on('conf_lifestyles')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('cities_religions', function(Blueprint $table)
        {
            $table->dropForeign('cities_religions_ibfk_1');
            $table->dropForeign('cities_religions_ibfk_2');
            $table->foreign('cities_id', 'cities_religions_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('religions_id', 'cities_religions_ibfk_2')->references('id')->on('conf_religions')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities_currencies', function(Blueprint $table)
        {
            $table->dropForeign('cities_currencies_ibfk_1');
            $table->dropForeign('cities_currencies_ibfk_2');
            $table->foreign('cities_id', 'cities_currencies_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('currencies_id', 'cities_currencies_ibfk_2')->references('id')->on('conf_currencies')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('cities_emergency_numbers', function(Blueprint $table)
        {
            $table->dropForeign('cities_emergency_numbers_ibfk_1');
            $table->dropForeign('cities_emergency_numbers_ibfk_2');
            $table->foreign('cities_id', 'cities_emergency_numbers_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('emergency_numbers_id', 'cities_emergency_numbers_ibfk_2')->references('id')->on('conf_emergency_numbers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('cities_holidays', function(Blueprint $table)
        {
            $table->dropForeign('cities_holidays_ibfk_1');
            $table->dropForeign('cities_holidays_ibfk_2');
            $table->foreign('cities_id', 'cities_holidays_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('holidays_id', 'cities_holidays_ibfk_2')->references('id')->on('conf_holidays')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('cities_languages_spoken', function(Blueprint $table)
        {
            $table->dropForeign('cities_languages_spoken_ibfk_1');
            $table->dropForeign('cities_languages_spoken_ibfk_2');
            $table->foreign('cities_id', 'cities_languages_spoken_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('languages_spoken_id', 'cities_languages_spoken_ibfk_2')->references('id')->on('conf_languages_spoken')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('cities_lifestyles', function(Blueprint $table)
        {
            $table->dropForeign('cities_lifestyles_ibfk_1');
            $table->dropForeign('cities_lifestyles_ibfk_2');
            $table->foreign('cities_id', 'cities_lifestyles_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('lifestyles_id', 'cities_lifestyles_ibfk_2')->references('id')->on('conf_lifestyles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::table('cities_religions', function(Blueprint $table)
        {
            $table->dropForeign('cities_religions_ibfk_1');
            $table->dropForeign('cities_religions_ibfk_2');
            $table->foreign('cities_id', 'cities_religions_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('religions_id', 'cities_religions_ibfk_2')->references('id')->on('conf_religions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }
}
