<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeyReferencesRestaurantsTransForCascadeDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('restaurants_trans',function (Blueprint $table) {
            if (Schema::hasColumn('restaurants_trans', 'restaurants_trans_ibfk_1')) {
                $table->dropForeign('restaurants_trans_ibfk_1');
            }
            $table->foreign('restaurants_id', 'restaurants_trans_ibfk_1')->references('id')->on('restaurants')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('restaurants_trans',function (Blueprint $table) {
            if (Schema::hasColumn('restaurants_trans', 'restaurants_trans_ibfk_1')) {
                $table->dropForeign('restaurants_trans_ibfk_1');
            }
            $table->foreign('restaurants_id', 'restaurants_trans_ibfk_1')->references('id')->on('restaurants')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
        Schema::enableForeignKeyConstraints();

    }
}
