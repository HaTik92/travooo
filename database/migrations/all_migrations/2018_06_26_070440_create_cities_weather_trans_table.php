<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesWeatherTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities_weather_trans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('weather_id');
            $table->integer('language_id');
            $table->text('daily_forecast');
            $table->text('upcoming_forecast');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cities_weather_trans');
    }
}
