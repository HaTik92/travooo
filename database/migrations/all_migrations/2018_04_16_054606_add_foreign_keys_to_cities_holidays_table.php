<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCitiesHolidaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cities_holidays', function(Blueprint $table)
		{
			$table->foreign('cities_id', 'cities_holidays_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('holidays_id', 'cities_holidays_ibfk_2')->references('id')->on('conf_holidays')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cities_holidays', function(Blueprint $table)
		{
			$table->dropForeign('cities_holidays_ibfk_1');
			$table->dropForeign('cities_holidays_ibfk_2');
		});
	}

}
