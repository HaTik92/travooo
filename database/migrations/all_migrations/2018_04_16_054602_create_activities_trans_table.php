<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivitiesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activities_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('activities_id')->index('activities_id');
			$table->integer('languages_id')->index('languages_id');
			$table->string('title');
			$table->text('description', 65535);
			$table->text('working_days', 65535);
			$table->text('working_times', 65535);
			$table->text('how_to_go', 65535);
			$table->text('when_to_go', 65535);
			$table->text('popularity', 65535);
			$table->text('conditions', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activities_trans');
	}

}
