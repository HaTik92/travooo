<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersNotificationSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_notification_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('users_id')->index('users_id');
			$table->string('var');
			$table->string('val');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_notification_settings');
	}

}
