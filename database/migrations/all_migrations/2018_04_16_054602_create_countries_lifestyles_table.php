<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesLifestylesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries_lifestyles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('countries_id')->index('countries_id');
			$table->integer('lifestyles_id')->index('lifestyles_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('countries_lifestyles');
	}

}
