<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeyReferencesRestaurantsForCascadeDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('restaurants',function (Blueprint $table) {
            if (Schema::hasColumn('restaurants', 'restaurants_ibfk_3')) {
                $table->dropForeign('restaurants_ibfk_3');
            }
            $table->foreign('places_id', 'restaurants_ibfk_3')->references('id')->on('places')->onDelete('CASCADE');
        });
        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('restaurants',function (Blueprint $table) {
            if (Schema::hasColumn('restaurants', 'restaurants_ibfk_3')) {
                $table->dropForeign('restaurants_ibfk_3');
            }
            $table->foreign('places_id', 'restaurants_ibfk_3')->references('id')->on('places')->onDelete('CASCADE');
        });
        Schema::enableForeignKeyConstraints();
    }
}
