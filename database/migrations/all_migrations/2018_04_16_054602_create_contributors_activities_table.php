<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContributorsActivitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contributors_activities', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('users_id')->unsigned()->index('users_id');
			$table->integer('activities_id')->index('activities_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contributors_activities');
	}

}
