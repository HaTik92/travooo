<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHotelsTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hotels_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('hotels_id')->index('hotels_id');
			$table->integer('languages_id')->index('languages_id');
			$table->string('title');
			$table->text('description', 65535)->nullable();
			$table->string('phone')->nullable();
			$table->string('address')->nullable();
			$table->text('working_days', 65535)->nullable();
			$table->text('working_times', 65535)->nullable();
			$table->text('how_to_go', 65535)->nullable();
			$table->text('when_to_go', 65535)->nullable();
			$table->text('num_activities', 65535)->nullable();
			$table->text('popularity', 65535)->nullable();
			$table->text('conditions', 65535)->nullable();
			$table->text('price_level', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hotels_trans');
	}

}
