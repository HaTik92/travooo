<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmbassiesSearchHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('embassies_search_history', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('admin_id')->unsigned()->index('admin_id');
			$table->decimal('lat', 10, 8);
			$table->decimal('lng', 11, 8);
			$table->string('time');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('embassies_search_history');
	}

}
