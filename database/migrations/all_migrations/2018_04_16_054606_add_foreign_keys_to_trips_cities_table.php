<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTripsCitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trips_cities', function(Blueprint $table)
		{
			$table->foreign('trips_id', 'trips_cities_ibfk_1')->references('id')->on('trips')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('cities_id', 'trips_cities_ibfk_2')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trips_cities', function(Blueprint $table)
		{
			$table->dropForeign('trips_cities_ibfk_1');
			$table->dropForeign('trips_cities_ibfk_2');
		});
	}

}
