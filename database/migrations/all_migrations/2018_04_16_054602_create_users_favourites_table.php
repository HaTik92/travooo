<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersFavouritesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_favourites', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('users_id')->unsigned()->index('users_id');
			$table->string('fav_type');
			$table->integer('fav_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_favourites');
	}

}
