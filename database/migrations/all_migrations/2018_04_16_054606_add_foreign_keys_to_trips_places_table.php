<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTripsPlacesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trips_places', function(Blueprint $table)
		{
			$table->foreign('trips_id', 'trips_places_ibfk_1')->references('id')->on('trips')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('countries_id', 'trips_places_ibfk_2')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cities_id', 'trips_places_ibfk_3')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('places_id', 'trips_places_ibfk_4')->references('id')->on('places')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trips_places', function(Blueprint $table)
		{
			$table->dropForeign('trips_places_ibfk_1');
			$table->dropForeign('trips_places_ibfk_2');
			$table->dropForeign('trips_places_ibfk_3');
			$table->dropForeign('trips_places_ibfk_4');
		});
	}

}
