<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeysOfHotelsRelationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotels_medias', function(Blueprint $table)
        {
            $table->dropForeign('hotels_medias_ibfk_1');
            $table->dropForeign('hotels_medias_ibfk_2');
            $table->foreign('hotels_id', 'hotels_medias_ibfk_1')->references('id')->on('hotels')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('medias_id', 'hotels_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });

        Schema::table('medias_shares', function(Blueprint $table)
        {
            $table->dropForeign('medias_shares_ibfk_1');
            $table->dropForeign('medias_shares_ibfk_2');
            $table->foreign('medias_id', 'medias_shares_ibfk_1')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('users_id', 'medias_shares_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotels_medias', function(Blueprint $table)
        {
            $table->dropForeign('hotels_medias_ibfk_1');
            $table->dropForeign('hotels_medias_ibfk_2');
            $table->foreign('hotels_id', 'hotels_medias_ibfk_1')->references('id')->on('hotels')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('medias_id', 'hotels_medias_ibfk_2')->references('id')->on('medias')->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::table('medias_shares', function(Blueprint $table)
        {
            $table->dropForeign('medias_shares_ibfk_1');
            $table->dropForeign('medias_shares_ibfk_2');
            $table->foreign('medias_id', 'medias_shares_ibfk_1')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('users_id', 'medias_shares_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }
}
