<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegionsMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('regions_medias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('regions_id')->index('regions_id');
			$table->integer('medias_id')->index('medias_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('regions_medias');
	}

}
