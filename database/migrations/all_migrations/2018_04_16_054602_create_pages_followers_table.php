<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesFollowersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages_followers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pages_id')->index('pages_id');
			$table->integer('users_id')->unsigned()->index('users_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages_followers');
	}

}
