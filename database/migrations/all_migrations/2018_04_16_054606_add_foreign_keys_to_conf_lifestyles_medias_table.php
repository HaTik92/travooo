<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConfLifestylesMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('conf_lifestyles_medias', function(Blueprint $table)
		{
			$table->foreign('lifestyles_id', 'conf_lifestyles_medias_ibfk_1')->references('id')->on('conf_lifestyles')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('medias_id', 'conf_lifestyles_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conf_lifestyles_medias', function(Blueprint $table)
		{
			$table->dropForeign('conf_lifestyles_medias_ibfk_1');
			$table->dropForeign('conf_lifestyles_medias_ibfk_2');
		});
	}

}
