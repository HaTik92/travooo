<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesStatisticsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities_statistics', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('cities_id')->index('cities_statistics_cities_id_foreign');
			$table->integer('medias')->default(0);
			$table->integer('followers')->default(0);
			$table->integer('trips')->default(0);
			$table->integer('places')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities_statistics');
	}

}
