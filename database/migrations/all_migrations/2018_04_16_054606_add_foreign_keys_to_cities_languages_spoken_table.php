<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCitiesLanguagesSpokenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cities_languages_spoken', function(Blueprint $table)
		{
			$table->foreign('cities_id', 'cities_languages_spoken_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('languages_spoken_id', 'cities_languages_spoken_ibfk_2')->references('id')->on('conf_languages_spoken')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cities_languages_spoken', function(Blueprint $table)
		{
			$table->dropForeign('cities_languages_spoken_ibfk_1');
			$table->dropForeign('cities_languages_spoken_ibfk_2');
		});
	}

}
