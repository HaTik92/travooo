<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmbassiesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('embassies_trans', function(Blueprint $table)
		{
			$table->foreign('embassies_id', 'embassies_trans_ibfk_1')->references('id')->on('embassies')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('languages_id', 'embassies_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('embassies_trans', function(Blueprint $table)
		{
			$table->dropForeign('embassies_trans_ibfk_1');
			$table->dropForeign('embassies_trans_ibfk_2');
		});
	}

}
