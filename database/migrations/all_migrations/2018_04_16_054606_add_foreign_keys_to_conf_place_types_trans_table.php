<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConfPlaceTypesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('conf_place_types_trans', function(Blueprint $table)
		{
			$table->foreign('place_types_ids', 'conf_place_types_trans_ibfk_1')->references('id')->on('conf_place_types')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('languages_ids', 'conf_place_types_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conf_place_types_trans', function(Blueprint $table)
		{
			$table->dropForeign('conf_place_types_trans_ibfk_1');
			$table->dropForeign('conf_place_types_trans_ibfk_2');
		});
	}

}
