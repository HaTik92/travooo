<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfTranslationsProgressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conf_translations_progress', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('language_title', 191);
			$table->string('language_code', 191);
			$table->string('event', 191);
			$table->integer('total_count')->default(0);
			$table->integer('done_count')->default(0);
			$table->dateTime('completed_at')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conf_translations_progress');
	}

}
