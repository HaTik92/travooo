<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConfRegionsMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('conf_regions_medias', function(Blueprint $table)
		{
			$table->foreign('regions_id', 'conf_regions_medias_ibfk_1')->references('id')->on('conf_regions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('medias_id', 'conf_regions_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conf_regions_medias', function(Blueprint $table)
		{
			$table->dropForeign('conf_regions_medias_ibfk_1');
			$table->dropForeign('conf_regions_medias_ibfk_2');
		});
	}

}
