<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlacesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('places', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('countries_id')->index('countries_id');
			$table->integer('cities_id')->index('cities_id');
			$table->string('place_type')->nullable()->index('place_type_ids');
			$table->string('provider_id')->index('provider_id');
			$table->decimal('lat', 10, 8);
			$table->decimal('lng', 11, 8);
			$table->decimal('rating', 2, 1)->nullable()->index('rating');
			$table->integer('safety_degrees_id')->nullable()->index('safety_degrees_id');
			$table->boolean('active')->index('active');
			$table->integer('media_done')->nullable()->index('media_done');
			$table->integer('media_count')->nullable()->default(0);
			$table->integer('cover_media_id')->nullable()->index('cover_media_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('places');
	}

}
