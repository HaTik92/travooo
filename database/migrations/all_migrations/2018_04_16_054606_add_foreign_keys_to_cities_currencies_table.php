<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCitiesCurrenciesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cities_currencies', function(Blueprint $table)
		{
			$table->foreign('cities_id', 'cities_currencies_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('currencies_id', 'cities_currencies_ibfk_2')->references('id')->on('conf_currencies')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cities_currencies', function(Blueprint $table)
		{
			$table->dropForeign('cities_currencies_ibfk_1');
			$table->dropForeign('cities_currencies_ibfk_2');
		});
	}

}
