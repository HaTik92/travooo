<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMediasReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('medias_reports', function(Blueprint $table)
		{
			$table->foreign('medias_id', 'medias_reports_ibfk_1')->references('id')->on('medias')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('users_id', 'medias_reports_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('medias_reports', function(Blueprint $table)
		{
			$table->dropForeign('medias_reports_ibfk_1');
			$table->dropForeign('medias_reports_ibfk_2');
		});
	}

}
