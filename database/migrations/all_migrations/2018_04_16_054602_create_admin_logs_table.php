<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_logs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('item_type', 25);
			$table->integer('item_id');
			$table->string('action');
			$table->text('query', 65535)->nullable();
			$table->string('time');
			$table->integer('admin_id')->unsigned()->index('admin_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_logs');
	}

}
