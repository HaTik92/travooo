<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCountriesFollowersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('countries_followers', function(Blueprint $table)
		{
			$table->foreign('countries_id', 'countries_followers_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('users_id', 'countries_followers_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('countries_followers', function(Blueprint $table)
		{
			$table->dropForeign('countries_followers_ibfk_1');
			$table->dropForeign('countries_followers_ibfk_2');
		});
	}

}
