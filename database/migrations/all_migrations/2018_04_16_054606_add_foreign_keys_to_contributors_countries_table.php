<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContributorsCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contributors_countries', function(Blueprint $table)
		{
			$table->foreign('users_id', 'contributors_countries_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('countries_id', 'contributors_countries_ibfk_2')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contributors_countries', function(Blueprint $table)
		{
			$table->dropForeign('contributors_countries_ibfk_1');
			$table->dropForeign('contributors_countries_ibfk_2');
		});
	}

}
