<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersHiddenContentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_hidden_content', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('users_id')->index('users_id');
			$table->integer('content_id');
			$table->integer('content_type');
			$table->dateTime('created_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_hidden_content');
	}

}
