<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaceStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('places_id');
            $table->integer('trips')->default(0);
            $table->integer('followers')->default(0);
            $table->integer('media')->default(0);
            $table->integer('likes')->default(0);
            $table->integer('reviews')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places_statistics');
    }
}
