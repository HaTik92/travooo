<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCitiesEmergencyNumbersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cities_emergency_numbers', function(Blueprint $table)
		{
			$table->foreign('cities_id', 'cities_emergency_numbers_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('emergency_numbers_id', 'cities_emergency_numbers_ibfk_2')->references('id')->on('conf_emergency_numbers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cities_emergency_numbers', function(Blueprint $table)
		{
			$table->dropForeign('cities_emergency_numbers_ibfk_1');
			$table->dropForeign('cities_emergency_numbers_ibfk_2');
		});
	}

}
