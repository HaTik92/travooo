<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVisaRequirementsTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('visa_requirements_trans', function(Blueprint $table)
		{
			$table->foreign('visa_requirements_ids', 'visa_requirements_trans_ibfk_1')->references('id')->on('visa_requirements')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('languages_ids', 'visa_requirements_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('visa_requirements_trans', function(Blueprint $table)
		{
			$table->dropForeign('visa_requirements_trans_ibfk_1');
			$table->dropForeign('visa_requirements_trans_ibfk_2');
		});
	}

}
