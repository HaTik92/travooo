<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVisaRequirementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('visa_requirements', function(Blueprint $table)
		{
			$table->foreign('from_countries_id', 'visa_requirements_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('to_countries_id', 'visa_requirements_ibfk_2')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('visa_requirements', function(Blueprint $table)
		{
			$table->dropForeign('visa_requirements_ibfk_1');
			$table->dropForeign('visa_requirements_ibfk_2');
		});
	}

}
