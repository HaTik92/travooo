<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesAdditionalLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries_additional_languages', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('countries_id')->nullable()->index('countries_id');
			$table->integer('languages_spoken_id')->nullable()->index('languages_spoken_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('countries_additional_languages');
	}

}
