<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCitiesMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cities_medias', function(Blueprint $table)
		{
			$table->foreign('cities_id', 'cities_medias_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('medias_id', 'cities_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cities_medias', function(Blueprint $table)
		{
			$table->dropForeign('cities_medias_ibfk_1');
			$table->dropForeign('cities_medias_ibfk_2');
		});
	}

}
