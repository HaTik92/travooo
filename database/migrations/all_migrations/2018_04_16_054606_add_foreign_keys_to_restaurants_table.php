<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRestaurantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('restaurants', function(Blueprint $table)
		{
			$table->foreign('countries_id', 'restaurants_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cities_id', 'restaurants_ibfk_2')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('places_id', 'restaurants_ibfk_3')->references('id')->on('places')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cover_media_id', 'restaurants_ibfk_4')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('restaurants', function(Blueprint $table)
		{
			$table->dropForeign('restaurants_ibfk_1');
			$table->dropForeign('restaurants_ibfk_2');
			$table->dropForeign('restaurants_ibfk_3');
			$table->dropForeign('restaurants_ibfk_4');
		});
	}

}
