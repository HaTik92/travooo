<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPagesMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pages_medias', function(Blueprint $table)
		{
			$table->foreign('pages_ids', 'pages_medias_ibfk_1')->references('id')->on('pages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('medias_ids', 'pages_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pages_medias', function(Blueprint $table)
		{
			$table->dropForeign('pages_medias_ibfk_1');
			$table->dropForeign('pages_medias_ibfk_2');
		});
	}

}
