<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmbassiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('embassies', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('countries_id')->index('countries_id');
			$table->integer('cities_id');
			$table->string('place_type')->nullable();
			$table->decimal('lat', 10, 8);
			$table->decimal('lng', 11, 8);
			$table->string('provider_id')->nullable();
			$table->decimal('rating', 2, 1)->nullable();
			$table->boolean('active');
			$table->integer('cover_media_id')->nullable()->index('cover_media_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('embassies');
	}

}
