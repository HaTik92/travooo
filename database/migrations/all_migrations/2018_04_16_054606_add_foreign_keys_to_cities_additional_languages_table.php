<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCitiesAdditionalLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cities_additional_languages', function(Blueprint $table)
		{
			$table->foreign('cities_id', 'cities_additional_languages_ibfk_1')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('languages_spoken_id', 'cities_additional_languages_ibfk_2')->references('id')->on('conf_languages_spoken')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cities_additional_languages', function(Blueprint $table)
		{
			$table->dropForeign('cities_additional_languages_ibfk_1');
			$table->dropForeign('cities_additional_languages_ibfk_2');
		});
	}

}
