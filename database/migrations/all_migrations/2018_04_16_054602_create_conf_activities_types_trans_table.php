<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfActivitiesTypesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conf_activities_types_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('activities_types_id')->index('activities_types_id');
			$table->integer('languages_id')->index('languages_id');
			$table->string('title');
			$table->text('description', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conf_activities_types_trans');
	}

}
