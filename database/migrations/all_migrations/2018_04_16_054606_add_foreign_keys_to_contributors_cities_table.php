<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContributorsCitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contributors_cities', function(Blueprint $table)
		{
			$table->foreign('users_id', 'contributors_cities_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cities_id', 'contributors_cities_ibfk_2')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contributors_cities', function(Blueprint $table)
		{
			$table->dropForeign('contributors_cities_ibfk_1');
			$table->dropForeign('contributors_cities_ibfk_2');
		});
	}

}
