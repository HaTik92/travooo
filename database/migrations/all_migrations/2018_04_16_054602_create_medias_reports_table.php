<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMediasReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('medias_reports', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('medias_id')->index('medias_id');
			$table->integer('users_id')->unsigned()->index('users_id');
			$table->integer('reason');
			$table->dateTime('created_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('medias_reports');
	}

}
