<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExpertsCitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('experts_cities', function(Blueprint $table)
		{
			$table->foreign('users_id', 'experts_cities_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cities_id', 'experts_cities_ibfk_2')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('experts_cities', function(Blueprint $table)
		{
			$table->dropForeign('experts_cities_ibfk_1');
			$table->dropForeign('experts_cities_ibfk_2');
		});
	}

}
