<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTripsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trips', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('users_id')->unsigned()->index('users_id');
			$table->string('title')->nullable();
			$table->integer('duration')->nullable();
			$table->integer('budget')->nullable();
			$table->integer('budget_currency')->nullable();
			$table->decimal('distance', 6)->nullable();
			$table->integer('num_places')->nullable();
			$table->integer('num_users')->nullable();
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->boolean('privacy')->nullable()->comment('1 public, 2 friends, 3 private, 4 draft');
			$table->dateTime('created_at')->nullable();
			$table->integer('active')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trips');
	}

}
