<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesHolidaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities_holidays', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cities_id')->index('cities_id');
			$table->integer('holidays_id')->index('holidays_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities_holidays');
	}

}
