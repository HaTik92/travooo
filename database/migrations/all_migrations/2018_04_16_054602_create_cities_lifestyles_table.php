<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesLifestylesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities_lifestyles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cities_id')->index('cities_id');
			$table->integer('lifestyles_id')->index('lifestyles_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities_lifestyles');
	}

}
