<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesStatisticsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries_statistics', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('medias')->default(0);
			$table->integer('countries_id')->index('countries_statistics_countries_id_foreign');
			$table->integer('followers')->default(0);
			$table->integer('trips')->default(0);
			$table->integer('places')->default(0);
			$table->integer('cities')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('countries_statistics');
	}

}
