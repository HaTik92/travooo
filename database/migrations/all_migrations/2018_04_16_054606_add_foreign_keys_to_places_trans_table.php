<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlacesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('places_trans', function(Blueprint $table)
		{
			$table->foreign('languages_id', 'places_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('places_id', 'places_trans_ibfk_3')->references('id')->on('places')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('places_trans', function(Blueprint $table)
		{
			$table->dropForeign('places_trans_ibfk_2');
			$table->dropForeign('places_trans_ibfk_3');
		});
	}

}
