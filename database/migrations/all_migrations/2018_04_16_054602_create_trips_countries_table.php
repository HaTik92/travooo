<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTripsCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trips_countries', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('trips_id')->index('trips_id');
			$table->integer('countries_id')->index('countries_id');
			$table->date('date');
			$table->integer('order');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trips_countries');
	}

}
