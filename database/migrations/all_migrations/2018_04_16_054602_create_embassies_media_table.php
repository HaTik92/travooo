<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmbassiesMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('embassies_media', function(Blueprint $table)
		{
			$table->integer('id')->nullable();
			$table->integer('embassies_id')->nullable()->index('embassies_id');
			$table->integer('medias_id')->nullable()->index('medias_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('embassies_media');
	}

}
