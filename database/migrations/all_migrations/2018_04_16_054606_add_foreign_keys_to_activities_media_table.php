<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivitiesMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activities_media', function(Blueprint $table)
		{
			$table->foreign('activities_id', 'activities_media_ibfk_1')->references('id')->on('activities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('medias_id', 'activities_media_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activities_media', function(Blueprint $table)
		{
			$table->dropForeign('activities_media_ibfk_1');
			$table->dropForeign('activities_media_ibfk_2');
		});
	}

}
