<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            if (!Schema::hasColumn('users', 'visited_locations')) {
                $table->integer('visited_locations')->nullable()->default(0);
            }
            if (!Schema::hasColumn('users', 'trip_plans')) {
                $table->integer('trip_plans')->nullable()->default(0);
            }
            if (!Schema::hasColumn('users', 'favorites')) {
                $table->integer('favorites')->nullable()->default(0);
            }
            if (!Schema::hasColumn('users', 'videos')) {
                $table->integer('videos')->nullable()->default(0);
            }
            if (!Schema::hasColumn('users', 'photos')) {
                $table->integer('photos')->nullable()->default(0);
            }
            if (!Schema::hasColumn('users', 'reviews')) {
                $table->integer('reviews')->nullable()->default(0);
            }
            if (!Schema::hasColumn('users', 'interests')) {
                $table->integer('interests')->nullable()->default(0);
            }
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            if (Schema::hasColumn('users', 'visited_locations')) {
                $table->dropColumn('visited_locations');
            }
            if (Schema::hasColumn('users', 'trip_plans')) {
                $table->dropColumn('trip_plans');
            }
            if (Schema::hasColumn('users', 'favorites')) {
                $table->dropColumn('favorites');
            }
            if (Schema::hasColumn('users', 'videos')) {
                $table->dropColumn('videos');
            }
            if (Schema::hasColumn('users', 'photos')) {
                $table->dropColumn('photos');
            }
            if (Schema::hasColumn('users', 'reviews')) {
                $table->dropColumn('reviews');
            }
            if (Schema::hasColumn('users', 'interests')) {
                $table->dropColumn('interests');
            }
            
        });
    }
}
