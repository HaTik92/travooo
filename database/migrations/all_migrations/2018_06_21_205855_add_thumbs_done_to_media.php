<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThumbsDoneToMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medias', function (Blueprint $table) {
            if (!Schema::hasColumn('medias', 'thumbs_done')) {
                $table->integer('thumbs_done')->nullable()->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medias', function (Blueprint $table) {
            if (Schema::hasColumn('medias', 'thumbs_done')) {
                $table->dropColumn('thumbs_done');
            }
        });
    }
}
