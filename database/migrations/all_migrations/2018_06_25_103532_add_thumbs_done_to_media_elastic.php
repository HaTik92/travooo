<?php

use App\Models\Place\Place;
use Illuminate\Database\Migrations\Migration;

class AddThumbsDoneToMediaElastic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $places = new Place();
        $places->addColumn('medias', 'thumbs_done', 0);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        $places = new Place();
//        $places->dropColumn('medias', 'thumbs_done');
    }
}
