<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCountriesAdditionalLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('countries_additional_languages', function(Blueprint $table)
		{
			$table->foreign('countries_id', 'countries_additional_languages_ibfk_1')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('languages_spoken_id', 'countries_additional_languages_ibfk_2')->references('id')->on('conf_languages_spoken')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('countries_additional_languages', function(Blueprint $table)
		{
			$table->dropForeign('countries_additional_languages_ibfk_1');
			$table->dropForeign('countries_additional_languages_ibfk_2');
		});
	}

}
