<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfAgeRangesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conf_age_ranges', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('active');
			$table->string('from', 20);
			$table->string('to', 20);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conf_age_ranges');
	}

}
