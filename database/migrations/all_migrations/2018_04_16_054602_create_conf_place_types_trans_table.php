<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfPlaceTypesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conf_place_types_trans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('place_types_ids')->index('place_types_ids');
			$table->integer('languages_ids')->index('languages_ids');
			$table->string('title');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conf_place_types_trans');
	}

}
