<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeCollationForMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE medias CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");
        Schema::table('medias', function (Blueprint $table) {
            $table->string('html_attributions')->charset('utf8mb4')->change();
            $table->string('html_attributions')->collation('utf8mb4_unicode_ci')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
