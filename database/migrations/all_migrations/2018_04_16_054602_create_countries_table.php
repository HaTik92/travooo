<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('regions_id')->index('regions_id');
			$table->string('code', 5);
			$table->string('iso_code', 2);
			$table->decimal('lat', 10, 8);
			$table->decimal('lng', 11, 8);
			$table->integer('safety_degree_id')->nullable()->index('safety_degree_id');
			$table->boolean('active')->index('active');
			$table->integer('cover_media_id')->nullable()->index('cover_media_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('countries');
	}

}
