<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersInterestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_interests', function(Blueprint $table)
		{
			$table->foreign('users_id', 'users_interests_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('interests_id', 'users_interests_ibfk_2')->references('id')->on('conf_interests')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_interests', function(Blueprint $table)
		{
			$table->dropForeign('users_interests_ibfk_1');
			$table->dropForeign('users_interests_ibfk_2');
		});
	}

}
