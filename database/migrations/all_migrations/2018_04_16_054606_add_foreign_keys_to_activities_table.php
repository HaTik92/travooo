<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activities', function(Blueprint $table)
		{
			$table->foreign('types_id', 'activities_ibfk_1')->references('id')->on('conf_activities_types')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('countries_id', 'activities_ibfk_2')->references('id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cities_id', 'activities_ibfk_3')->references('id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('places_id', 'activities_ibfk_4')->references('id')->on('places')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activities', function(Blueprint $table)
		{
			$table->dropForeign('activities_ibfk_1');
			$table->dropForeign('activities_ibfk_2');
			$table->dropForeign('activities_ibfk_3');
			$table->dropForeign('activities_ibfk_4');
		});
	}

}
