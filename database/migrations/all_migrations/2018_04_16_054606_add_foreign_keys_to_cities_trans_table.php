<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCitiesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *z
	 * @return void
	 */
	public function up()
	{
		Schema::table('cities_trans', function(Blueprint $table)
		{
			$table->foreign('languages_id', 'cities_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cities_trans', function(Blueprint $table)
		{
			$table->dropForeign('cities_trans_ibfk_2');
		});
	}

}
