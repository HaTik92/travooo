<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMediasIdForeignKeysToCitiesMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities_medias', function(Blueprint $table)
        {
            $table->dropForeign('cities_medias_ibfk_2');
            $table->foreign('medias_id', 'cities_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities_medias', function(Blueprint $table)
        {
            $table->dropForeign('cities_medias_ibfk_2');
            $table->foreign('medias_id', 'cities_medias_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }
}
