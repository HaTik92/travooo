<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlacesMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('places_medias', function(Blueprint $table)
		{
			$table->foreign('places_id', 'places_medias_ibfk_1')->references('id')->on('places')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('medias_id', 'places_medias_ibfk_2')->references('id')->on('medias')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('places_medias', function(Blueprint $table)
		{
			$table->dropForeign('places_medias_ibfk_1');
			$table->dropForeign('places_medias_ibfk_2');
		});
	}

}
