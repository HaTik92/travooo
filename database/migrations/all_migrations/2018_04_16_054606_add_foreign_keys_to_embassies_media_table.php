<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmbassiesMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('embassies_media', function(Blueprint $table)
		{
			$table->foreign('embassies_id', 'embassies_media_ibfk_1')->references('id')->on('embassies')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('medias_id', 'embassies_media_ibfk_2')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('embassies_media', function(Blueprint $table)
		{
			$table->dropForeign('embassies_media_ibfk_1');
			$table->dropForeign('embassies_media_ibfk_2');
		});
	}

}
