<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInCountriesTimezonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries_timezones', function (Blueprint $table) {
            if (!Schema::hasColumn('countries_timezones', 'dst_offset')) {
                $table->string('dst_offset', 255)->nullable();
            }
            if (!Schema::hasColumn('countries_timezones', 'raw_offset')) {
                $table->string('raw_offset', 255)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries_timezones', function (Blueprint $table) {
            if (Schema::hasColumn('countries_timezones', 'dst_offset')) {
                $table->dropColumn('dst_offset');
            }
        });
        Schema::table('countries_timezones', function (Blueprint $table) {
            if (Schema::hasColumn('countries_timezones', 'raw_offset')) {
                $table->dropColumn('raw_offset');
            }
        });
    }
}
