<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTripsPlacesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trips_places', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('trips_id')->index('trips_id');
			$table->integer('countries_id')->index('countries_id');
			$table->integer('cities_id')->index('cities_id');
			$table->integer('places_id')->index('places_id');
			$table->date('date')->nullable();
			$table->integer('order')->nullable();
			$table->dateTime('time')->nullable();
			$table->integer('duration')->nullable();
			$table->integer('budget')->nullable();
			$table->integer('weather')->nullable();
			$table->text('comment', 65535)->nullable();
			$table->integer('active')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trips_places');
	}

}
