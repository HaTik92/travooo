<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisaRequirementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visa_requirements', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('from_countries_id')->index('from_countries_id');
			$table->integer('to_countries_id')->index('to_countries_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visa_requirements');
	}

}
