<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConfActivitiesTypesTransTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('conf_activities_types_trans', function(Blueprint $table)
		{
			$table->foreign('activities_types_id', 'conf_activities_types_trans_ibfk_1')->references('id')->on('conf_activities_types')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('languages_id', 'conf_activities_types_trans_ibfk_2')->references('id')->on('conf_languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conf_activities_types_trans', function(Blueprint $table)
		{
			$table->dropForeign('conf_activities_types_trans_ibfk_1');
			$table->dropForeign('conf_activities_types_trans_ibfk_2');
		});
	}

}
