<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserContentLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_content_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id')->unsigned()->index('users_id');
             $table->integer('language_id');
            $table->timestamps();
            
            $table->foreign('users_id', 'user_content_languages_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_content_languages');
    }
}
