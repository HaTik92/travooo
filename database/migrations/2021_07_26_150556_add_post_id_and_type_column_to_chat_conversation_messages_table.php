<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostIdAndTypeColumnToChatConversationMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chat_conversation_messages', function (Blueprint $table) {
            $table->string('post_type', 20)->nullable()->index()->after('places_id');
            $table->bigInteger('post_id')->nullable()->index()->after('post_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chat_conversation_messages', function (Blueprint $table) {

            $table->dropColumn(['post_type', 'post_id']);
        });

    }
}
