<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsersTravelstyles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users_travelstyles')) {
            Schema::create('users_travelstyles', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('users_id')->unsigned();
                $table->integer('travelstyles_id')->unsigned();
                $table->engine = 'InnoDB';

                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
