<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsSuggestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips_suggestions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trips_id');
            $table->foreign('trips_id')->references('id')->on('trips');
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('type');
            $table->unsignedInteger('users_id');
            $table->foreign('users_id')->references('id')->on('users');
            $table->integer('active_trips_places_id')->nullable();
            $table->foreign('active_trips_places_id')->references('id')->on('trips_places');
            $table->integer('suggested_trips_places_id');
            $table->foreign('suggested_trips_places_id')->references('id')->on('trips_places');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips_suggestions');
    }
}
