<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsDraftsLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('reports_drafts_locations')) {
            Schema::create('reports_drafts_locations', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('reports_id')->unsigned();
                $table->integer('reports_draft_id')->unsigned();
                $table->string('location_type')->nullable()->comment('country, city');
                $table->integer('location_id')->unsigned()->nullable();
                $table->engine = 'InnoDB';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('reports_drafts_locations')) {
            Schema::dropIfExists('reports_drafts_locations');
        }
    }
}
