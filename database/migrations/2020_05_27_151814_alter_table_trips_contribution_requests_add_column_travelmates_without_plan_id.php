<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTripsContributionRequestsAddColumnTravelmatesWithoutPlanId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips_contribution_requests', function (Blueprint $table) {
            $table->integer('w_plans_id')->nullable()->after('role');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('trips_contribution_requests', function (Blueprint $table) {
            $table->dropColumn('w_plans_id');
        });
    }
}
