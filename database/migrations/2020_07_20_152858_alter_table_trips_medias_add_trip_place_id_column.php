<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTripsMediasAddTripPlaceIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips_medias', function (Blueprint $table) {
            $table->unsignedInteger('trip_place_id')->nullable()->after('medias_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips_medias', function (Blueprint $table) {
            $table->dropColumn(['trip_place_id']);
        });
    }
}
