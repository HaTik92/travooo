<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('trips_descriptions')) {
            Schema::create('trips_descriptions', function(Blueprint $table)
            {
                    $table->integer('id', true);
                    $table->integer('trips_id')->index('trips_id');
                    $table->integer('places_id')->index('places_id');
                    $table->integer('users_id')->unsigned()->index('users_id');
                    $table->text('description')->nullable();
                    $table->text('images')->nullable();
                    $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                    $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                    $table->engine = 'InnoDB';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
