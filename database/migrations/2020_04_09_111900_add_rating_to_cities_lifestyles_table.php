<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRatingToCitiesLifestylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities_lifestyles', function(Blueprint $table)
        {
            if (!Schema::hasColumn('cities_lifestyles', 'rating')) {
                $table->integer('rating')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities_lifestyles', function (Blueprint $table) {
            if (Schema::hasColumn('cities_lifestyles', 'rating')) {
                $table->dropColumn('rating');
            }
        });
    }
}
