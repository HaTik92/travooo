<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableInviteExpertLinksAddBadgeIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invite_expert_links', function (Blueprint $table) {
            $table->unsignedInteger('badge_id')->nullable()->after('code');
            $table->foreign('badge_id')->references('id')->on('ranking_badges');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invite_expert_links', function (Blueprint $table) {
            $table->dropColumn('badge_id');
        });
    }
}
