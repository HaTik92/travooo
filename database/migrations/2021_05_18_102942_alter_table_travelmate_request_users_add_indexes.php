<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTravelmateRequestUsersAddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('travelmate_request_users', function (Blueprint $table) {
            $table->index('users_id');
            $table->index('requests_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('travelmate_request_users', function (Blueprint $table) {
            $table->dropIndex(['users_id', 'requests_id']);
        });
    }
}
