<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediasCommentsVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('medias_comments_votes')) {
            Schema::create('medias_comments_votes', function(Blueprint $table)
            {
                    $table->integer('id', true);
                    $table->integer('comment_id')->index('comment_id');
                    $table->integer('users_id')->index('users_id');
                    $table->integer('vote_type')->nullable()->comment('1 Up Vote, 2 Down Vote');
                    $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                    $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                    $table->engine = 'InnoDB';
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
