<?php

use App\Models\ActivityLog\ActivityLog;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedToActivityLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_log', function (Blueprint $table) {
            $table->boolean('deleted')->default(false)->after('permission');
        });

        ActivityLog::where('action', 'delete')->chunk(100, function ($logs) {
            $condition_items = [];
            $where_condition = '';
            foreach ($logs as $log) {
                $condition_items[] = '(type = "' . $log['type'] . '" and variable = "' . $log['variable'] . '" and action != "delete")';
            }
            $where_condition .= '(' . implode(' or ', $condition_items) . ')';
            ActivityLog::query()->whereRaw($where_condition)->update(['deleted' => true]);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_log', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });
    }
}
