<?php

use App\Models\ActivityLog\ActivityLog;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeInActivityLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        ActivityLog::whereIn('type',['trips_places_shares', 'trips_medias_shares', 'trips_shares'])->chunk(100, function ($logs) {
            foreach ($logs as $log) {
                $log->type = 'share';
                $log->save();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
