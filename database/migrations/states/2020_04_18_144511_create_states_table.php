<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('states', function(Blueprint $table)
		{
            $table->integer('id', true);
			$table->integer('countries_id')->index('countries_id');
			$table->decimal('lat', 10, 8);
            $table->decimal('lng', 11, 8);
            $table->tinyInteger('is_province')->nullable();
			$table->integer('active')->index('active');
			$table->integer('cover_media_id')->nullable()->index('cover_media_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('states');
	}
}
