<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlanAndPlaceToChatConversationMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chat_conversation_messages', function (Blueprint $table) {
            $table->integer('places_id')->nullable()->after('is_read');
            $table->integer('plans_id')->nullable()->after('is_read');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chat_conversation_messages', function (Blueprint $table) {
            $table->dropColumn(['places_id', 'plans_id']);
        });
    }
}
