<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CopyrightInfringements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('copyright_infringements')) {
            Schema::create('copyright_infringements', function (Blueprint $table) {
                $table->integer('id', true);
                $table->integer('owner_id')->nullable();
                $table->string('sender_type');
                $table->string('owner_name');
                $table->string('sender_name');
                $table->string('company')->nullable();
                $table->string('job_title');
                $table->string('sender_email');
                $table->string('street');
                $table->string('city');
                $table->string('state');
                $table->integer('postal_code');
                $table->string('country');
                $table->string('phone')->nullable();
                $table->string('fax')->nullable();
                $table->text('description');
                $table->string('original_link')->nullable();
                $table->string('infringement_location');
                $table->string('reported_url');
                $table->text('infringement_description');
                $table->string('authority_act');
                $table->string('signature');

                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('copyright_infringements');
    }
}
