<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpamsRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spams_replies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reply_id');
            $table->unsignedInteger('users_id');
            $table->tinyInteger('report_type')->nullable();
            $table->text('report_text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spams_replies');
    }
}
