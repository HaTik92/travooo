<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableChangeSuggestionPlaceToNullableForSuggestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips_suggestions', function (Blueprint $table) {
            $table->integer('suggested_trips_places_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips_suggestions', function (Blueprint $table) {
            $table->integer('suggested_trips_places_id')->nullable(false)->change();
        });
    }
}
