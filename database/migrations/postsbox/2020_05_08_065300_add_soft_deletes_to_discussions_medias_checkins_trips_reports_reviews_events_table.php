<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeletesToDiscussionsMediasCheckinsTripsReportsReviewsEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discussions', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('medias', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('checkins', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('trips', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('reports', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('reviews', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('events', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discussions', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('medias', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('checkins', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('trips', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('reports', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('reviews', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('events', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
