<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckinsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (!Schema::hasTable('checkins')) {
            Schema::disableForeignKeyConstraints();
            Schema::create('checkins', function (Blueprint $table) {
                $table->increments('id');
                $table->text('location');
                $table->text('lat_lng');
                $table->engine = 'InnoDB';
            });
            Schema::enableForeignKeyConstraints();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('checkins');
    }

}
