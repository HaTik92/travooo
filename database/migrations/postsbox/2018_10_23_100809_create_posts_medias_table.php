<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsMediasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (!Schema::hasTable('posts_medias')) {
            Schema::enableForeignKeyConstraints();
            Schema::create('posts_medias', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('posts_id')->unsigned();
                $table->integer('medias_id');

                $table->foreign('posts_id')->references('id')->on('posts')->onUpdate('RESTRICT')->onDelete('RESTRICT');
                $table->foreign('medias_id')->references('id')->on('medias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            });
            Schema::disableForeignKeyConstraints();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('posts_medias');
    }

}
