<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostsComments extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (!Schema::hasTable('posts_comments')) {
            Schema::create('posts_comments', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('posts_id')->unsigned();
                $table->integer('users_id')->unsigned();
                $table->text('text')->nullable();
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->engine = 'InnoDB';

                $table->foreign('posts_id')->references('id')->on('posts')->onUpdate('RESTRICT')->onDelete('RESTRICT');
                $table->foreign('users_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('posts_comments');
    }

}
