<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlaceIdCityIdCountryIdInCheckinsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (!Schema::hasTable('checkins')) {
            Schema::table('checkins', function($table) {
                if (Schema::hasColumn('checkins', 'place_id')) {
                    $table->integer('place_id')->nullable();
                }
                if (Schema::hasColumn('checkins', 'city_id')) {
                    $table->integer('city_id')->nullable();
                }
                if (Schema::hasColumn('checkins', 'country_id')) {
                    $table->integer('country_id')->nullable();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('checkins', function($table) {
            $table->dropColumn('place_id');
            $table->dropColumn('city_id');
            $table->dropColumn('country_id');
        });
    }

}
