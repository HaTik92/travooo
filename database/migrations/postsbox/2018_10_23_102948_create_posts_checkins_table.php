<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsCheckinsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (!Schema::hasTable('posts_checkins')) {
            Schema::enableForeignKeyConstraints();

            Schema::create('posts_checkins', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('posts_id')->unsigned();
                $table->integer('checkins_id')->unsigned();
                $table->engine = 'InnoDB';

                $table->foreign('posts_id')->references('id')->on('posts')->onUpdate('RESTRICT')->onDelete('RESTRICT');
                $table->foreign('checkins_id')->references('id')->on('checkins')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            });
            Schema::disableForeignKeyConstraints();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('posts_checkins');
    }

}
