<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMusementCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('musement_cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cities_id')->comment('Travooo cities id');
            $table->integer('countries_id')->comment('Travooo cities id');
            $table->integer('tp_id')->comment('third party id referrence');
            $table->text('name')->nullable();
            $table->text('country')->nullable();
            $table->integer('tp_country_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('musement_cities');
    }
}
