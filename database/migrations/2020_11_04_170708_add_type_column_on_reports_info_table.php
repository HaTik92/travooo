<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeColumnOnReportsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reports_infos')) {
            Schema::table('reports_infos', function (Blueprint $table) {
                $table->integer('type')->default(0)->after('order')->comment('0 - draft, 1 - public');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_infos', function (Blueprint $table) {
                $table->dropColumn(['type']);
        });
    }
}
