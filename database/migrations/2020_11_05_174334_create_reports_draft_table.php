<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsDraftTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_drafts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id');
            $table->integer('reports_id')->nullable();
            $table->string('countries_id')->nullable();
            $table->string('cities_id')->nullable();
            $table->string('title');
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports_drafts');
    }
}
