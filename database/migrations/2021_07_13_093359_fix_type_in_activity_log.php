<?php

use App\Models\ActivityLog\ActivityLog;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Str;

class FixTypeInActivityLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ActivityLog::chunk(100, function ($logs) {
            foreach ($logs as $log) {
                if (Str::lower($log->type) == 'medias') {
                    $newType = 'media';
                } else {
                    $newType = snake_case($log->type);
                }
                $log->update(['type' => $newType]);
            }
        });
    }

    public function down()
    {

    }
}