<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToSpamManagerAndConfirmedReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('confirmed_reports')) {
            Schema::table('confirmed_reports', function (Blueprint $table) {
                $table->integer('owner_id')->after('id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('confirmed_reports', function (Blueprint $table) {
            if (Schema::hasColumn('confirmed_reports', 'owner_id')) {
                $table->dropColumn('owner_id');
            }
        });
    }
}
