<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSuggestionsAddReasonColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips_suggestions', function (Blueprint $table) {
            $table->string('reason')->nullable()->after('suggested_trips_places_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips_suggestions', function (Blueprint $table) {
            $table->dropColumn(['reason']);
        });
    }
}
