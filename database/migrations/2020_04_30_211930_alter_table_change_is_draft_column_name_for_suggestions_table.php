<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableChangeIsDraftColumnNameForSuggestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips_suggestions', function (Blueprint $table) {
            $table->renameColumn('is_for_draft', 'is_published');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips_suggestions', function (Blueprint $table) {
            $table->renameColumn('is_published', 'is_for_draft');
        });
    }
}
