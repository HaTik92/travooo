<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRankAndRank7Columns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table){
            $table->integer('rank' )->default(0)->index()->after('text');
            $table->integer('rank_7')->default(0)->index()->after('rank');
        });

        Schema::table('discussions', function (Blueprint $table) {
            $table->integer('rank' )->default(0)->index()->after('language_id');
            $table->integer('rank_7')->default(0)->index()->after('rank');
        });

        Schema::table('trips', function (Blueprint $table) {
            $table->integer('rank' )->default(0)->index()->after('language_id');
            $table->integer('rank_7')->default(0)->index()->after('rank');
        });

        Schema::table('reports', function (Blueprint $table) {
            $table->integer('rank' )->default(0)->index()->after('language_id');
            $table->integer('rank_7')->default(0)->index()->after('rank');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn(['rank', 'rank_7']);
        });

        Schema::table('discussions', function (Blueprint $table) {
            $table->dropColumn(['rank', 'rank_7']);
        });

        Schema::table('trips', function (Blueprint $table) {
            $table->dropColumn(['rank', 'rank_7']);
        });

        Schema::table('reports', function (Blueprint $table) {
            $table->dropColumn(['rank', 'rank_7']);
        });
    }
}
