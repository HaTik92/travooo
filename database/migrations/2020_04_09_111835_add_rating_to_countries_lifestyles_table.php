<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRatingToCountriesLifestylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries_lifestyles', function(Blueprint $table)
        {
            if (!Schema::hasColumn('countries_lifestyles', 'rating')) {
                $table->integer('rating')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries_lifestyles', function (Blueprint $table) {
            if (Schema::hasColumn('countries_lifestyles', 'rating')) {
                $table->dropColumn('rating');
            }
        });
    }
}

