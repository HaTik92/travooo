<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPrivacyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_privacy', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id')->unsigned()->index('users_id');
            $table->tinyInteger('see_my_following')->default(0);
            $table->tinyInteger('message_me')->default(0);
            $table->tinyInteger('follow_me')->default(0);
            $table->tinyInteger('find_me')->default(0);
            $table->timestamps();
            
            $table->foreign('users_id', 'users_privacy_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_privacy');
    }
}
