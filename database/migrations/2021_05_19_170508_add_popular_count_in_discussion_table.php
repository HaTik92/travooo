<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPopularCountInDiscussionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('discussions')) {
            Schema::table('discussions', function (Blueprint $table) {
                $table->integer('popular_count')->default(0)->after('description')->comment('count of replies, share, upvote and downvote');
                $table->dropColumn(['last_reply_by', 'last_reply_on']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discussions', function (Blueprint $table) {
            $table->dropColumn(['popular_count']);
            $table->integer('last_reply_by');
            $table->timestamp('last_reply_on');
        });
    }
}
