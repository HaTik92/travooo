<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlanActivityLogsAddSuggestionColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_activity_logs', function (Blueprint $table) {
            $table->unsignedInteger('suggestion_id')->nullable()->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_activity_logs', function (Blueprint $table) {
            $table->dropColumn('suggestion_id');
        });
    }
}
