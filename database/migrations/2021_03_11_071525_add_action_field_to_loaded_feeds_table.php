<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActionFieldToLoadedFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loaded_feeds', function (Blueprint $table) {
            $table->integer('users_id')->unsigned()->after('token');
            $table->integer('page')->nullable()->unsigned()->after('date');
            $table->string('action')->nullable()->after('type');
            $table->string('meta')->nullable()->after('action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loaded_feeds', function (Blueprint $table) {
            $table->dropColumn('users_id');
            $table->dropColumn('page');
            $table->dropColumn('action');
            $table->dropColumn('meta');
        });
    }
}
