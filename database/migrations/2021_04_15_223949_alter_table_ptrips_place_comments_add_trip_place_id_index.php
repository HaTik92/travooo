<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePtripsPlaceCommentsAddTripPlaceIdIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips_places_comments', function (Blueprint $table) {
            $table->index('trip_place_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips_places_comments', function (Blueprint $table) {
            $table->dropIndex(['trip_place_id']);
        });
    }
}
