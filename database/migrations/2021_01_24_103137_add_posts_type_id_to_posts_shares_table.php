<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostsTypeIdToPostsSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts_shares', function (Blueprint $table) {
            $table->integer('posts_type_id')->unsigned()->nullable()->after('type');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts_shares', function (Blueprint $table) {
            $table->dropColumn('posts_type_id');
        });
    }
}
