<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Artisan;

class CreateCommonPageTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('common_page_translations', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('common_page_id')->index('common_page_id');
            $table->integer('languages_id')->index('languages_id');
            $table->string('title')->nullable();
            $table->text('description', 65535)->nullable();
        });
        Artisan::call('add:legal-pages');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('common_page_translations');
    }
}
