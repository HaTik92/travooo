<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateChatMessagesChangeVarcharToText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chat_conversation_messages', function (Blueprint $table) {
            $table->text('file_path')->change();
            $table->text('file_name')->change();
            $table->text('file_type')->change();
            $table->text('file_size')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chat_conversation_messages', function (Blueprint $table) {
            $table->string('file_path')->change();
            $table->string('file_name')->change();
            $table->string('file_type')->change();
            $table->integer('file_size')->change();
        });
    }
}
