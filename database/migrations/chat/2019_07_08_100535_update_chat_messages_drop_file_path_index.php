<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateChatMessagesDropFilePathIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chat_conversation_messages', function (Blueprint $table) {
            $table->dropIndex('chat_conversation_messages_file_path_index')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chat_conversation_messages', function (Blueprint $table) {

            $table->index('file_path')->change();
        });
    }
}
