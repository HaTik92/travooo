<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HelpCenterContents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_center_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sub_menu_id')->unsigned();
            $table->foreign('sub_menu_id')->references('id')->on('help_center_sub_menues')->onUpdate('cascade')->onDelete('cascade');;
            $table->enum('type', ['content','device_faq','media']);
            $table->longText('content')->nullable(true);
            $table->longText('mobile_faq')->nullable(true);
            $table->longText('desktop_faq')->nullable(true);
            $table->text('media_url')->nullable(true);
            $table->integer('position')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_center_contents');
    }
}
