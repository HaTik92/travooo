<?php

use App\Models\ActivityLog\ActivityLog;
use App\Models\Posts\PostsShares;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HotfixTextTypeToPostTypeSomeTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // text to post
        ActivityLog::where('type', 'share')->where('action', 'text')->update([
            'action' => 'post'
        ]);

        // remove 's'
        ActivityLog::where('type', 'share')->where('action', 'discussions')->update([
            'action' => 'discussion'
        ]);

        // text to post
        PostsShares::where('type', 'text')->update([
            'type' => 'post'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
