<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMediasToDiscussionRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (Schema::hasTable('discussion_replies')) {
            Schema::table('discussion_replies', function (Blueprint $table) {
                $table->string('medias_url')->after('num_follows')->nullable();
                $table->string('medias_type')->after('num_follows')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discussion_replies', function (Blueprint $table) {
                $table->dropColumn(['medias_url', 'medias_type']);
        });
    }
}
