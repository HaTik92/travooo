<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePostsTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('posts_type', 20)->nullable()->comment("post, trip, trip place, trip comment");
            $table->integer('posts_id')->unsigned()->comment("posts_id, trips_id, trips_places_id, posts_comment_id");
            $table->string('destination', 20)->nullable()->comment("city, country, user, place etc..");
            $table->integer('destination_id')->unsigned()->comment("cities_id, users_id ");
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_tags');
    }
}
