<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities_abouts', function (Blueprint $table) {
            $table->integer('id', true);
			$table->integer('cities_id')->index('cities_id');
			$table->string('type');
			$table->string('title');
			$table->text('body', 65535)->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities_abouts');
    }
}
