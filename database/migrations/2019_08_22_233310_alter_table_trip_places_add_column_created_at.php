<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTripPlacesAddColumnCreatedAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('trips_places', function (Blueprint $table) {
              $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'))->after('versions_id');;
              $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->after('versions_id');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
