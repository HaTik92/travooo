<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDiscussionsRepliesAddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discussion_replies', function (Blueprint $table) {
            $table->index('discussions_id');
            $table->index('medias_type');
            $table->index('medias_url');
            $table->index('parents_id');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discussion_replies', function (Blueprint $table) {
            $table->dropIndex(['discussions_id']);
            $table->dropIndex(['medias_type']);
            $table->dropIndex(['medias_url']);
            $table->dropIndex(['parents_id']);
        });
    }
}
