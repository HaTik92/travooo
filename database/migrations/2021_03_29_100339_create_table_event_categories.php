<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEventCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tp_id')->comment('third party id referrence');
            $table->text('name')->nullable();
            $table->text('code')->nullable();
            $table->text('type')->nullable();
            $table->tinyInteger('level')->nullable()->comment('sub activity level');
            $table->integer('p_id')->nullable()->comment('parent level activity reference');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events_categories');
    }
}
