<?php

use App\Models\Country\Countries;
use App\Models\Country\CountriesTimezones;
use Illuminate\Database\Seeder;

class CountriesTimezonesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = Countries::get();

        foreach ($countries as $country => $value) {

            $latitude  = $value->lat;
            $longitude = $value->lng;
            $time      = time();
            $key       = env('GOOGLE-API-KEY');

            $url       = 'https://maps.googleapis.com/maps/api/timezone/json?location='.$latitude.','.$longitude.'&timestamp='.$time.'&key='.$key;
            $client    = new \GuzzleHttp\Client();
            $response  = $client->get($url);
            $result    = json_decode($response->getBody());

            if ($result->status == 'OK') {
                $checkTimezone = CountriesTimezones::where('countries_id', $value->id)->first();
                if (empty($checkTimezone)) {
                    $countryTimezone = new CountriesTimezones();
                    $countryTimezone->countries_id   = $value->id;
                    $countryTimezone->time_zone_id   = $result->timeZoneId;
                    $countryTimezone->time_zone_name = $result->timeZoneName;
                    $countryTimezone->dst_offset     = $result->dstOffset;
                    $countryTimezone->raw_offset     = $result->rawOffset;
                    $countryTimezone->save();
                }
            }
        }
    }
}
