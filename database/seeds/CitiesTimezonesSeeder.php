<?php

use App\Models\City\Cities;
use App\Models\City\CitiesTimezones;
use Illuminate\Database\Seeder;

class CitiesTimezonesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = Cities::get();

        foreach ($cities as $city => $value) {

            $latitude  = $value->lat;
            $longitude = $value->lng;
            $time      = time();
            $key       = env('GOOGLE-API-KEY');

            $url       = 'https://maps.googleapis.com/maps/api/timezone/json?location='.$latitude.','.$longitude.'&timestamp='.$time.'&key='.$key;
            $client    = new \GuzzleHttp\Client();
            $response  = $client->get($url);
            $result    = json_decode($response->getBody());

            if ($result->status == 'OK') {
                $checkTimezone = CitiesTimezones::where('cities_id', $value->id)->first();
                if (empty($checkTimezone)) {
                    $cityTimezone = new CitiesTimezones();
                    $cityTimezone->cities_id      = $value->id;
                    $cityTimezone->time_zone_id   = $result->timeZoneId;
                    $cityTimezone->time_zone_name = $result->timeZoneName;
                    $cityTimezone->dst_offset     = $result->dstOffset;
                    $cityTimezone->raw_offset     = $result->rawOffset;
                    $cityTimezone->save();
                }
            }
        }
    }
}
