<?php

use App\Models\Country\Countries;
use App\Models\Country\CountriesTranslations;
use Illuminate\Database\Seeder;
use Jcf\Geocode\Geocode;

class CountriesLatLngUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countriesTrans = CountriesTranslations::get();

        foreach ($countriesTrans as $country => $value) {

            $params = [
                'key' => env('GOOGLE-API-KEY'),
                'language'=> 'en'
            ];

            $response = Geocode::make()->address($value->title, $params);

            if ($response) {
                $countries = Countries::find($value->countries_id);
                $isoCode   = $response->response->address_components[0]->short_name;

                $countries->update([
                    'iso_code' => !empty($isoCode) && strlen($isoCode) < 3  ? $isoCode : '',
                    'lat'      => $response->latitude(),
                    'lng'      => $response->longitude()
                ]);
            }
        }
    }
}
