<?php

use App\Models\City\Cities;
use App\Models\City\CitiesTranslations;
use Illuminate\Database\Seeder;

class CitiesLatLngUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $citiesTrans = CitiesTranslations::get();

        foreach ($citiesTrans as $city => $value) {

            $params = [
                'key'     => env('GOOGLE-API-KEY'),
                'address' => $value->title
            ];

            $client   = new \GuzzleHttp\Client();
            $response = json_decode($client->get('https://maps.googleapis.com/maps/api/geocode/json', [
                'query' => $params
            ])->getBody());

            if ($response->status == 'OVER_QUERY_LIMIT') {
                sleep(2);
                $response = json_decode($client->get('https://maps.googleapis.com/maps/api/geocode/json', [
                    'query' => $params
                ])->getBody());
            }

            if ($response->status == 'OK') {
                $cities  = Cities::find($value->cities_id);
                if (!empty($cities)) {
                    $cities->update([
                        'lat' => $response->results[0]->geometry->location->lat,
                        'lng' => $response->results[0]->geometry->location->lng
                    ]);
                }
            }
        }
    }
}
