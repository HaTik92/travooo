<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Carbon\Carbon as Carbon;
use Database\DisableForeignKeys;
use Illuminate\Support\Facades\DB;

class HelpCenterSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Add the master administrator, user id of 1
        $categories = [
            [
                'name'              => 'Using Travooo',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Managing your account',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Safety and security',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Rules and policies',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'About Travooo',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
        ];

        DB::table("help_center_categories")->insert($categories);

    }
}
