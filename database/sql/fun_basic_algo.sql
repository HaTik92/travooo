DROP FUNCTION IF EXISTS fun_basic_algo;
CREATE FUNCTION fun_basic_algo(
    `var_id` VARCHAR(20),
    `var_type` VARCHAR(20)
) 
RETURNS INT
DETERMINISTIC
BEGIN
    DECLARE var_return INT DEFAULT 0;

    -- Check Module Wise
    IF var_type="post" THEN
        SELECT( 
            (SELECT COUNT(id) FROM `posts_comments_likes` WHERE `posts_comments_id`= var_id) + 
            (SELECT COUNT(id) FROM `posts_comments` WHERE `parents_id`= var_id) 
        ) INTO var_return;
    ELSEIF  var_type="trip" THEN
        SELECT( 
            (SELECT COUNT(id) FROM `trips_comments_likes` WHERE `trips_comments_id`= var_id) + 
            (SELECT COUNT(id) FROM `posts_comments` WHERE `parents_id`= var_id) 
        ) INTO var_return;
    ELSEIF  var_type="report" THEN
        SELECT( 
            (SELECT COUNT(id) FROM `reports_comments_likes` WHERE `comments_id`= var_id) + 
            (SELECT COUNT(id) FROM `posts_comments` WHERE `parents_id`= var_id) 
        ) INTO var_return;
    ELSEIF  var_type="trip_place" THEN
        SELECT( 
            (SELECT COUNT(id) FROM `trips_places_comments_likes` WHERE `trip_place_comment_id` in(SELECT id FROM `trips_places_comments` WHERE `trip_place_id`= var_id)) + 
            (SELECT COUNT(id) FROM `trips_places_comments` WHERE `reply_to`= var_id) 
        ) INTO var_return;
    ELSEIF  var_type="trip_place_media" THEN
        SELECT( 
            (SELECT COUNT(id) FROM `medias_comments_likes` WHERE `medias_comments_id` in(SELECT id FROM `medias_comments` WHERE `medias_id`= var_id)) + 
            (SELECT COUNT(id) FROM `medias_comments` WHERE `medias_id`= var_id and `reply_to`!=0) 
        ) INTO var_return;
    END IF; 

    RETURN var_return;
END
