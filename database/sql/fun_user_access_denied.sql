DROP FUNCTION IF EXISTS fun_user_access_denied;
CREATE FUNCTION fun_user_access_denied(
    `var_auth_user_id` int(20),
    `var_feed_users_id` int(20),
    `var_feed_type` varchar(255),
    `var_feed_action` varchar(255),
    `var_feed_id` varchar(255)
) 
RETURNS INT
DETERMINISTIC
BEGIN
    -- Response Code
    DECLARE var_success INT DEFAULT 200;
    DECLARE var_not_found INT DEFAULT 404;
    DECLARE var_unauthorized INT DEFAULT 401;
    DECLARE var_no_content INT DEFAULT 204;

    -- Other usefull members
    DECLARE var_return INT DEFAULT 0;
    DECLARE var_users_block INT DEFAULT 0;

    -- Check User Blocked Or Not
    IF var_feed_type <> '' THEN
        SELECT `id` into var_users_block FROM `setting_users_block` WHERE 
            (`users_id` = var_auth_user_id AND `blocked_users_id` = var_feed_users_id) OR 
            (`users_id` = var_feed_users_id AND `blocked_users_id` = var_auth_user_id) limit 1;
    ELSE
        SELECT `id` into var_users_block FROM `setting_users_block` WHERE `users_id` = var_feed_users_id AND `blocked_users_id` = var_auth_user_id limit 1;
    END IF; 

    IF var_users_block != 0 THEN
        SET var_return = var_unauthorized;
    ELSE
        SET var_return = var_success;
    END IF;    
      
    RETURN var_return;
END
