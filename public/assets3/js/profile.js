$(document).ready(function () {
    let el = {
        name: $(".name-block").find("h3").html(),
        about: $(".about-block").html(),
        nationality: $(".nationality-block").html(),
        email: $(".email-block").html(),
        phone: $(".phone-block").html(),
        interest: $(".interest-block").html(),
        interest_value: $(".interest-block").attr('uInterest'),
        expertise: $(".expertise-block").html(),
        expertise_value: $(".expertise-block").attr('expId'),
        travelstyle: $(".travelstyle-block").html(),
        travelstyle_value: $(".travelstyle-block").attr('styleId'),
        exp_content: '',
        trev_content: ''
    };

    $(document).on("click", ".edit-link", function (e) {
        e.preventDefault(),
        el.interest_value = $(".interest-block").attr('uInterest');
        el.phone = $(".phone-block").html();
        
        $(".editable").remove(), $(".empty-row").css('display', 'flex'), $(".phone-block, .email-block, .nationality-block, .expertise-block, .travelstyle-block, .about-block, .interest-block").addClass('editable-block'), $('.travelstyle-block, .expertise-block').addClass('flex-custom'), $(".name-block").find("h3").hide(), $(".name-block").find("h3").after($(".edit_inputs_name")[0].outerHTML), $(".edit_inputs_name:eq(1)").val($(".edit_inputs_name:eq(0)").val()), $(".name-block").find(".edit_inputs_name").after($(".edit_inputs_username")[0].outerHTML), $(".edit_inputs_username:eq(1)").val($(".edit_inputs_username:eq(0)").val()), $(".edit_inputs_username:eq(1)").addClass("editable"), $(".email-block").html($(".edit_contact_email")[0].outerHTML), $(".edit_contact_email:eq(1)").val($(".edit_contact_email:eq(0)").val()), $(".phone-block").html($(".inputs_phone_block")[0].outerHTML),  $(".edit_inputs_phone:eq(1)").val($.trim(el.phone)), $('.edit_inputs_phone').intlTelInput('destroy').intlTelInput({autoHideDialCode: false, formatOnDisplay: true, separateDialCode: true,initialCountry:'', utilsScript: $('.edit_inputs_phone').attr('data-utils')}), $(".about-block").html($(".edit_inputs_block")[0].outerHTML), $(".edit_inputs_about:eq(1)").val($.trim($(".edit_inputs_about:eq(0)").val())), $(".interest-block").html($(".edit_inputs_interests")[0].outerHTML), $(".interest-block").append('<ul class="search-selected-block p-8 interest-sel-block"></ul>'), $(".interest-block").find('.edit_inputs_interests').after('<label for="interests" class="error interest-error-block" style="color:red"></label>'), $(".social-block").after($(".edit_inputs_social")[0].outerHTML);
        el.interest_list = (el.interest_value != '') ? el.interest_value.split(',') : [];
        if (el.interest_list.length > 0) {
            $.each(el.interest_list, function (index, interest) {
                $('.interest-sel-block').append(getSelectedHtml(interest, 'close-interest-filter', 'interest-value', interest))
            });
        }
        if($('#profile-form').attr('data-expert-type') == 1){
        let t = $(".expertise-block");
        t.html(el.exp_content!=''?el.exp_content:$(".exp-block")[0].outerHTML);
        el.expertise_value = $(".expertise-block").attr('expId');
        el.expertise_list = el.expertise_value!=''?el.expertise_value.split('-:'):[];
        let a = $(".expertise_url").val();

        t.find('#exp_autocomplete').autocomplete({
            source: function (request, response) {
                jQuery.get(a, {
                    q: request.term
                }, function (data) {
                    var items = [];
                    var data = JSON.parse(data);

                    data.forEach(function (s_item, index) {
                        items.push({
                            id: s_item.id, value: s_item.text, image: s_item.image, type: s_item.type, query: s_item.query
                        });
                    });
                    response(items);
                });
            },
            classes: {"ui-autocomplete": "travel-mates-autocomplate"},
            focus: function (event, ui) {
                return false;
            },
            select: function (event, ui) {
                t.find('#exp_autocomplete').val('')
                if ($.inArray(ui.item.id, el.expertise_list) == -1) {
                    if(el.expertise_list.length < 5){
                        el.expertise_list.push(ui.item.id)
                        $('.expertise-sel-block').append(getSelectedHtml(ui.item.value, 'close-expertise-filter', 'expertise-id', ui.item.id));
                         el.exp_content = $('.expertise-block .exp-block')[0].outerHTML;
                        $('.expertise-error-block').html('')
                    }else{
                        $('.expertise-error-block').html('The maximum expertise count is 5.')
                    }
                }
                return false;
            },
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                    .append(getMarkupHtml(item))
                    .appendTo(ul);
        }, $(".expert-rigth-link").html('<a href="" class="cancel-edit" style="margin-right: 10px;">Cancel</a>  <button class="btn btn-light-primary btn-bordered save-profile">Save</button>'), $(".edit_inputs_about:eq(1)").addClass("editable"), $(".edit_inputs_name:eq(1)").addClass("editable"), $(".edit_inputs_social:eq(1)").addClass("editable"), $(".edit_inputs_nationality:eq(1)").addClass("editable"), $(".edit_inputs_age:eq(1)").addClass("editable"), $("body").bstooltip({
            selector: "[rel=tooltip]"
        })};
        let n = $(".nationality-block");
        n.html($(".edit_inputs_nationality")[0].outerHTML), n.find(".edit_inputs_nationality").attr("id", "nationalitys"), $(".edit_inputs_nationality:eq(1)").val($(".edit_inputs_nationality:eq(0)").val());
        $('#nationalitys').select2({
            debug: !0,
            placeholder: "Select nationality",
            containerCssClass: "select2-about-input",
        }), $(".expert-rigth-link").html('<a href="" class="cancel-edit" style="margin-right: 10px;">Cancel</a>  <button class="btn btn-light-primary btn-bordered save-profile">Save</button>'), $(".edit_inputs_about:eq(1)").addClass("editable"), $(".edit_inputs_name:eq(1)").addClass("editable"), $(".edit_inputs_social:eq(1)").addClass("editable"), $(".edit_inputs_nationality:eq(1)").addClass("editable"), $(".edit_inputs_age:eq(1)").addClass("editable"), $(".edit_inputs_gender:eq(1)").addClass("editable"), $("body").bstooltip({
            selector: "[rel=tooltip]"
        });
        
        let r = $(".travelstyle-block");
        r.html(el.trev_content!=''?el.trev_content:$(".trev-style-block")[0].outerHTML);
        let  ar = $(".travelstyle_url").val();
        el.travelstyle_value = $(".travelstyle-block").attr('styleId')
        el.travelstyle_list = el.travelstyle_value!=''?el.travelstyle_value.split(','):[];

        $(".expert-rigth-link").html('<a href="" class="cancel-edit" style="margin-right: 10px;">Cancel</a>  <button class="btn btn-light-primary btn-bordered save-profile">Save</button>'), $(".edit_inputs_about:eq(1)").addClass("editable"), $(".edit_inputs_name:eq(1)").addClass("editable"), $(".edit_inputs_social:eq(1)").addClass("editable"), $(".edit_inputs_nationality:eq(1)").addClass("editable"), $(".edit_inputs_age:eq(1)").addClass("editable"), $(".edit_inputs_gender:eq(1)").addClass("editable"), $("body").bstooltip({
            selector: "[rel=tooltip]"
        });
        $('#travelstyleSelect').on('select2:unselect', function (e) {
            var data = e.params;
        }).trigger('change');

        $( ".inputs_phone_block" ).click(function() {
            $(this).find('.phone_error.profile-error-text').html('');
        });
    });

    $(document).on("click", ".cancel-edit", function (t) {
        $('#username-error').hide();
        t.preventDefault(), $(".editable").remove(), $(".empty-row").css('display', 'none'), $(".phone-block, .nationality-block, .expertise-block, .travelstyle-block, .about-block, .interest-block").removeClass('editable-block'), $(".expert-rigth-link").html('<a href="" class="edit-link">Edit</a>'), $('.travelstyle-block, .expertise-block').removeClass('flex-custom'), $(".select2-container").remove(), $(".name-block").find("h3").show(), $(".name-block").find("h3").html(el.name), $(".about-block").html(el.about), $(".interest-block").html(el.interest), $(".nationality-block").html(el.nationality), $(".phone-block").html($.trim(el.phone)), $(".edit_inputs_phone").intlTelInput('destroy'), $(".expertise-block").html(el.expertise), $(".travelstyle-block").html(el.travelstyle), $(".email-block").html(el.email)
    });

    $(document).on("keyup", ".name-input", function (t) {
        $('#username-error').hide();
        t.preventDefault()
    });

    $(document).on("click", ".save-profile", function (t) {
        t.preventDefault();
        let i = $("#profile-form").serializeArray();
        var countryData = $('.edit_inputs_phone').intlTelInput('getSelectedCountryData');
        var tel = '';
        if ($("#profile-form").find('.edit_inputs_phone').val() != '')
            tel = '+' + countryData['dialCode'] + ' ' + $("#profile-form").find('.edit_inputs_phone').val();

        $("#profile-form").find('.edit_inputs_name').rules("add", {
            required: true,
            maxlength: 50,
            messages: {
                required: "Name field is required.",
                maxlength: jQuery.validator.format(" Name can be max 50 characters long.")
            }
        });

        $("#profile-form").find('.edit_inputs_username').rules("add", {
            required: true,
            maxlength: 15,
            messages: {
                required: "Username field is required.",
                maxlength: jQuery.validator.format("Username can be max 15 characters long.")
            }
        });

        if (!$('#profile-form').validate().form()) {
            return
        }
        if (tel != '' && !$('.edit_inputs_phone').intlTelInput('isValidNumber')) {
            $('.phone_error').html('Please enter valid number');
            return;
        }
        
        if (typeof el.expertise_list != 'undefined' && el.expertise_list.length > 5) {
             $('.expertise-error-block').show();
             $('.expertise-error-block').html('The maximum expertise count is 5.')
            return;
        }
        
        i.push({
            name: "full_mobile_number",
            value: tel
        }), i.push({
            name: "interest_list",
            value: (typeof el.interest_list != 'undefined')?el.interest_list.join(','):''
        }), i.push({
            name: "travelstyles",
            value: el.travelstyle_list
        }), i.push({
            name: "expertise",
            value: (typeof el.expertise_list != 'undefined')?el.expertise_list.join('-:'):''
        }), i.push({
            name: "is_ajax",
            value: "1"
        });
        let a = $("#profile-form").attr("action"),
                n = $(this);
        n.attr("disabled", "true"), n.find("i").length || n.append(' <i class="fa fa-spinner fa-spin"></i>'), $(".cancel-edit").hide(), $(".success-edited").remove(), $(".error-edited").remove(), $.ajax({
            type: "POST",
            url: a,
            data: i,
            dataType: "json",
            success: function (resp) {
                if (resp.data.success) {
                     if (resp.data.success) {
                    let t = "",
                        i = 0;
                    $(".expertise-block .expertise-sel-block li").each(function() {
                        t += i < 3 ? "<a href='javascript:;' class='form-link'>" + $(this).find('.search-selitem-title').html() + "</a>"+ (i !=$(".expertise-block .expertise-sel-block li").length-1? ',&nbsp;':'') +"" : 3 == i ? `<a href="javascript:;" class="form-link show-more" style="cursor:pointer;">+${$(".expertise-block .expertise-sel-block li").length-3} More</a>\n<a href="javascript:;" class="form-link more-links d-none">${$(this).find('.search-selitem-title').html()}</a>${(i != $(".expertise-block .expertise-sel-block li").length-1? '<span class="more-links d-none">,&nbsp;</span>':'')}` : `<a href="javascript:;" class="form-link more-links d-none">${$(this).find('.search-selitem-title').html()}</a>${(i !=$(".expertise-block .expertise-sel-block li").length-1? '<span class="more-links d-none">,&nbsp;</span>':'')}`, i++
                    });

                    let tr = "",
                        tr_cont = '',
                        ir = 0;
                    $(".travelstyle-block .styles-sel-block li").each(function() {
                        let tr_scope = '';
                        if(ir < $(".travelstyle-block .styles-sel-block li").length-2){
                            tr_scope = ' , ';
                        }
                        if(ir == $(".travelstyle-block .styles-sel-block li").length-2){
                             tr_scope = ' & ';
                        }
                        tr_cont += $(this).find('.search-selitem-title').html() + tr_scope;
                        
                        tr += ir < 3 ? "<a href='javascript:;' class='form-link'>" + $(this).find('.search-selitem-title').html() + "</a>"+ (ir !=$(".travelstyle-block .styles-sel-block li").length-1? ',&nbsp;':'') +"" : 3 == ir ? `<a href="javascript:;" class="form-link show-more" style="cursor:pointer;">+${$(".travelstyle-block .styles-sel-block li").length-3} More</a>\n<a href="javascript:;" class="form-link more-links d-none">${$(this).find('.search-selitem-title').html()}</a>${(ir != $(".travelstyle-block .styles-sel-block li").length-1? '<span class="more-links d-none">,&nbsp;</span>':'')}` : `<a href="javascript:;" class="form-link more-links d-none">${$(this).find('.search-selitem-title').html()}</a>${(ir !=$(".travelstyle-block .styles-sel-block li").length-1? '<span class="more-links d-none">,&nbsp;</span>':'')}`, ir++
                    });
                   
                    let a = $(".edit_inputs_about:eq(1)").val(),
                        n = $(".edit_inputs_name:eq(1)").val(),
                        l = $(".edit_inputs_nationality:eq(1) option:selected").html(),
                        s = $(".edit_inputs_nationality:eq(1)").val(),
                        un = $(".edit_inputs_username:eq(1)").val(),
                        eml = $(".edit_contact_email:eq(1)").val(),
                        ex = (typeof el.expertise_list != 'undefined')?el.expertise_list.join('-:'):'',
                        u = el.interest_list.join(','),
                        ts = el.travelstyle_list.join(',');
                
                        if(el.travelstyle_list && el.travelstyle_list.length > 0){
                            $('.profile-tstyle-content').removeClass('d-none');
                            $('.prof-tstyle-content').removeClass('empty-row');
                            $('.prof-exp-content').removeClass('bb-gray');
                        }else{
                            $('.profile-tstyle-content').addClass('d-none');
                            $('.prof-tstyle-content').addClass('empty-row');
                            $('.prof-exp-content').addClass('bb-gray');
                            
                            $('.prof-tstyle-content').hide();
                        }
                        
                        if(el.interest_list && el.interest_list.length > 0){
                            $('.prof-interest-content').removeClass('empty-row');
                            $('.prof-msince-content').addClass('bb-gray');
                        }else{
                            $('.prof-interest-content').addClass('empty-row');
                            $('.prof-msince-content').removeClass('bb-gray');
                            
                            $('.prof-interest-content').hide();
                        }

                        if(el.expertise_list && el.expertise_list.length > 0){
                            $('.profile-exp-content').removeClass('d-none');
                            $('.prof-exp-content').removeClass('empty-row');
                        }else{
                            $('.profile-exp-content').addClass('d-none');
                            $('.prof-exp-content').addClass('empty-row');
                            
                            $('.prof-exp-content').hide();
                        }
                        
                        if(eml == ''){
                            $('.prof-email-content').addClass('empty-row')
                            $('.prof-email-content').hide()
                        }else{
                            $('.prof-email-content').removeClass('empty-row')
                        }
                        
                        if(tel == ''){
                            $('.prof-phone-content').addClass('empty-row')
                            $('.prof-phone-content').hide()
                        }else{
                            $('.prof-phone-content').removeClass('empty-row')
                        }
   
                        $(".name-block").find("h3.side-ttls").show();
                        $(".name-block").find("h3.side-ttls").css('font-size', resp.data.about_name_size);
                        $(".name-block").find("h3.side-ttls").html($.trim(n)+' <span class="about-uername">(@'+un+')</span>'); 
                        $('.prof-username').html($.trim(n));
                        $(".about-block").html(`<p class="l-breack">${$.trim(a)||"n/a"}</p>`);
                        $(".nationality-block").html(l);
                        $(".email-block").html(eml);
                        $(".phone-block").html(tel);
                        $(".expertise-block").html(t);
                        $(".expertise-block").removeClass('editable-block');
                        $(".expertise-block").attr('expid', ex);
                        $(".expertise-block").removeClass('flex-custom');
                        
                        $(".travelstyle-block").html(tr);
                        $(".travelstyle-block").removeClass('editable-block');
                        $(".travelstyle-block").attr('styleid', ts);
                        $(".travelstyle-block").removeClass('flex-custom');

                        $(".interest-block").html(`<p>${u ||"n/a"}</p>`);
                        $(".interest-block").attr('uInterest', u);
                        $(".interest-block").removeClass('editable-block');

                        $(".edit_inputs_name:eq(0)").val(n);
                        $(".edit_inputs_username:eq(0)").val(un);
                        $(".edit_contact_email:eq(0)").val(eml);
                        $(".edit_inputs_about:eq(0)").val(a);
                        $(".edit_inputs_nationality:eq(0)").val(s);
                        $(".edit_inputs_phone").val(tel);
                        $(".edit_inputs_phone").intlTelInput('destroy')

                        $(".editable").remove(), $(".tooltip").remove(),$(".select2-container").remove(),
                        $(".expert-rigth-link").html('<a href="" class="edit-link">Edit</a>'),

                       $(".main-content-layer").prepend('<div class="alert alert-success success-edited" role="alert" style="position: absolute;top: -10px;left: 13px;">\nSuccessfully Edited!\n</div>'), setTimeout(function() {
                            $(".success-edited").remove()
                        }, 3e3)
                        
                        $('.profile-exp-block').html(resp.data.exp_content);
                        $('.profile-trstyle-block').html(tr_cont);
                        $('.profile-name-title').css('font-size', resp.data.name_size);
                        $('.profile-name-title').html(resp.data.country_iso + ' ' + n + ' <span class="username-wrap">(@'+un+')</span>');
                         $('#username-error').show();
                }
                }
            },
            error: function (t) {
                $('.error-block').find('label').each(function () {
                    $(this).html('');
                });

                if (t.responseJSON.errors.username) {
                    $('#username-error').html(t.responseJSON.errors.username[0]);
                    $('#username-error').show();
                    $('.cancel-edit').show();
                    n.removeAttr("disabled");
                    n.find('i.fa-spinner').remove()
                }
            }
        })
    })


    $(document).on('click', 'h3.edit-links span.save', function (e) {
        let website = ($('input[name=website].side').val() !='')?'https://' + $('input[name=website].side').val():'';
        let facebook = ($('input[name=facebook].side').val() !='')?'https://' + $('input[name=facebook].side').val():'';
        let instagram = ($('input[name=instagram].side').val() !='')?'https://' + $('input[name=instagram].side').val():'';
        let twitter = ($('input[name=twitter].side').val() !='')?'https://' + $('input[name=twitter].side').val():'';
        let youtube = ($('input[name=youtube].side').val() != '')?'https://' + $('input[name=youtube].side').val():'';
        let tumblr = ($('input[name=tumblr].side').val() !='')?'https://' + $('input[name=tumblr].side').val():'';
        let pinterest = ($('input[name=pinterest].side').val() != '')?'https://' + $('input[name=pinterest].side').val():'';

        if (!$('#social_form').validate().form()) {
            return;
        }
        $.ajax({
            type: "POST",
            url: '/settings/social',
            data: {
                'is_ajax': true,
                'website': website,
                'facebook': facebook,
                'instagram': instagram,
                'twitter': twitter,
                'youtube': youtube,
                'tumblr': tumblr,
                'pinterest': pinterest,
            },
            dataType: "json",
            success: function (t) {
                if (t.data.success) {
                    window.location.reload();
                }
            },
            error: function (e, t, i) {
                $(".main-content-layer").prepend('<div class="alert alert-danger error-edited" role="alert" style="position: absolute;top: -10px;left: 13px;">\n                                                      Something went wrong. Please try again later.\n                                                    </div>'), setTimeout(function () {
                    $(".error-edited").remove()
                }, 3e3)
            }
        })
    });

    $(document).on('click', 'h3.edit-about', function (e) {
        $(this).html('<span class="save-about">Save</span>');
        $('.profile-about-inner').addClass('d-none')
        $('.profile-bio-block').removeClass('d-none')
        textarea_auto_height($('#profile_about_text')[0]);
    });

    //travel style script
    $(document).on('focus', '#trev_style_autocomplete', function (e) {
                getTravelStyles();
    });
    
    
    $(document).on('keyup', '#trev_style_autocomplete', function (e) {
        var search_item = $(this).val()
        if(search_item != ''){
             searchTravelStyles(search_item);
        }
    });
    
     $(document).click(function (event) {
        $target = $(event.target);
        if (!$target.closest('.trev-style-block').length) {
            $('.travel-styles-ui').hide();
        }
    });
    
    //select travel styles
    $(document).on('click', '.item-wrapper', function (e) {
               var id = $(this).attr('itemId');
               var text = $(this).text();
               
             $('.travelstyle-block').find('#trev_style_autocomplete').val('');
             $('.travel-styles-ui').hide();
                if ($.inArray(id, el.travelstyle_list) == -1) {
                    el.travelstyle_list.push(id)
                    $('.styles-sel-block').append(getSelectedHtml(text, 'close-styles-filter', 'styles-id', id));
                    el.trev_content = $(".travelstyle-block .trev-style-block")[0].outerHTML;
                }
    });

    $(document).on('click', 'h3.edit-about span.save-about', function (e) {
        $("#profile_about_text").rules("add", {
            required: true,
            messages: {
                required: "Bio is required...",
            }
        })

        if (!$('#profile_about_form').validate().form()) {
            return;
        }

        let about = $('#profile_about_text').val();

        $.ajax({
            type: "POST",
            url: '/profile/update-bio',
            data: {
                'bio': about,
            },
            dataType: "json",
            success: function (t) {
                if (t.data.success) {
                    $('.edit-about').html('<span class="edit">Edit</span>');
                    $('.profile-about-inner p').html(about)
                    $('.profile-about-inner').removeClass('d-none')
                    $('.profile-bio-block').addClass('d-none')
                }
            }
        })
    });

    $(document).on('click', 'h3.edit-links span.edit', function (e) {
        $('div.links-show').hide();
        $('div.links-edit').show();
    });

    $('#profile-form').validate();
    $('#social_form').validate();
    $('#profile_about_form').validate();




    $(".edit_contact_email").rules("add", {
        email: true,
    })
    
    
    $("#facebook_url").rules("add", {
        facebook_url: true,
    })

    $("#website_url").rules("add", {
        website_url: true,
    })

    $("#twitter_url").rules("add", {
        twitter_url: true,
    })
    $("#instagram_url").rules("add", {
        instagram_url: true,
    })
    $("#youtube_url").rules("add", {
        youtube_url: true,
    })
    $("#tumblr_url").rules("add", {
        tumblr_url: true,
    })
    $("#pinterest_url").rules("add", {
        pinterest_url: true,
    })

    $.validator.addMethod("facebook_url", function (value, element) {
        return this.optional(element) || /(?:https?:\/\/)?(?:www\.)?facebook\.com\/.(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-\.]*)/i.test(value);
    }, "Please enter a valid facebook URL."
            );

    $.validator.addMethod("twitter_url", function (value, element) {
        return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)twitter\.com\/(#!\/)?[a-z0-9_]+$/i.test(value);
    }, "Please enter a valid twitter URL."
            );

    $.validator.addMethod("instagram_url", function (value, element) {
        return this.optional(element) || /(https?:\/\/(?:www\.)?instagram\.com\/([^/?#&]+)).*/i.test(value);
    }, "Please enter a valid instagram URL."
            );

    $.validator.addMethod("website_url", function (value, element) {
        return this.optional(element) || /^(?:(?:http?|https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/i.test(value);
    }, "Please enter a valid URL."
            );

    $.validator.addMethod("youtube_url", function (value, element) {
        return this.optional(element) || /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/i.test(value);
    }, "Please enter a valid youtube URL."
            );

    $.validator.addMethod("tumblr_url", function (value, element) {
        return this.optional(element) || /[^"\/www\."](?<!w{3})[A-Za-z0-9]*(?=\.tumblr\.com)|(?<=\.tumblr\.com\/blog\/).*/i.test(value);
    }, "Please enter a valid tumblr URL."
            );

    $.validator.addMethod("pinterest_url", function (value, element) {
        return this.optional(element) || /(?:(?:http|https):\/\/)?(?:www\.)?(?:pinterest\.com|instagr\.am)\/([A-Za-z0-9-_\.]+)/i.test(value);
    }, "Please enter a valid pinterest URL."
            );

    //interests script
    $(document).on("paste", ".edit_inputs_interests", function(e){
        var pastedData = e.originalEvent.clipboardData.getData('text');
        var inputVal = pastedData.replace(/,/g, '', pastedData);
        var pasteStr = inputVal.split(' ');
 
        $.each( pasteStr, function( key, value ) {
            if ($.inArray(value, el.interest_list) == -1) {
                if(el.interest_list.length < 10){
                    el.interest_list.push(value)
                    $('.interest-sel-block').append(getSelectedHtml(value, 'close-interest-filter', 'interest-value', value));
                    $('.interest-error-block').html('')
                }else{
                    $('.interest-error-block').html('The maximum tags count is 10')
                }
            }
              
        });
        var self = $(this);
          setTimeout(function(e) {
              self.val('');
          }, 100);
    } );
    $(document).on('keydown', '.edit_inputs_interests', function (e) {
        if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 || e.keyCode == 190) && !e.shiftKey) {
            e.preventDefault();
        }
    });

    $(document).on('keyup', '.edit_inputs_interests', function (e) {
        if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 188 || e.keyCode == 190) && !e.shiftKey) {
            var input_value = $(this).val().trim();
            if (input_value != '') {
                if ($.inArray(input_value, el.interest_list) == -1) {
                    if(el.interest_list.length < 10){
                        el.interest_list.push(input_value)
                        $('.interest-sel-block').append(getSelectedHtml(input_value, 'close-interest-filter', 'interest-value', input_value));
                        $('.interest-error-block').html('')
                    }else{
                        $('.interest-error-block').html('The maximum tags count is 10')
                    }
                }
                $(this).val('')
            }
        }
    });

    $(document).on('blur', '.edit_inputs_interests', function (e) {
        $(this).val('')
    });

    //Remove interest
    $(document).on('click', '.close-interest-filter', function () {
        var interst = $(this).attr('data-interest-value');
        el.interest_list = $.grep(el.interest_list, function (value) {
            return value != interst;
        });
        
         if(el.interest_list.length < 10){
               $('.interest-error-block').html('')
          }

        $(this).closest('li').remove()

    });

    //Remove expertise
    $(document).on('click', '.close-expertise-filter', function () {
        var exp = $(this).attr('data-expertise-id');
        el.expertise_list = $.grep(el.expertise_list, function (value) {
            return value != exp;
        });
        
        if(el.expertise_list.length <= 5){
            $('.expertise-error-block').html('')
        }

        $(this).closest('li').remove();

        el.exp_content = $('.expertise-block .exp-block')[0].outerHTML;

    });
    
    
    //Remove travel styles
    $(document).on('click', '.close-styles-filter', function () {
        var t_style = $(this).attr('data-styles-id');
        el.travelstyle_list = $.grep(el.travelstyle_list, function (value) {
            return value != t_style;
        });

        $(this).closest('li').remove()
        
        el.trev_content = $(".travelstyle-block .trev-style-block")[0].outerHTML;

    });
    
    
     $(document).on('keydown', '#profile-form .flex-input', function(e){
            if (e.keyCode == 13) {
                e.preventDefault();
            }
        })

});


function markLocetionMatch(text, term) {
    var regEx = new RegExp("(" + term + ")(?!([^<]+)?>)", "gi");
    var output = text.replace(regEx, "<span class='select2-locetion-rendered'>$1</span>");
    return output;
}

function textarea_auto_height(element) {
    element.style.height = "5px";
    element.style.paddingBottom = '30px';
    element.style.height = (element.scrollHeight) + "px";
    element.focus();
    element.setSelectionRange(element.value.length, element.value.length);
}


function getSelectedHtml(title, class_name, data_attr, id) {
    var html = '<li><span class="search-selitem-title">' + title + '</span>' +
            '<span class="close-search-item ' + class_name + '" data-' + data_attr + '="' + id + '"> x</span></li>'

    return html;
}

function getMarkupHtml(response) {
    if (response) {
        var markup = '<div class="search-country-block">';
        if (response.image) {
            markup += '<div class="search-country-imag"><img src="' + response.image + '" /></div>';
        }
        markup += '<div class="span10">' +
                '<div class="search-country-info">' +
                '<div class="search-country-text">' + markLocetionMatch(response.value, response.query) + '</div>';
        if (response.type != '') {
            markup += '<div class="search-country-type">City in ' + response.type + '</div>';
        } else {
            markup += '<div class="search-country-type">Country</div>';
        }
        markup += '</div></div>';

        return markup;
    }
}

function getTravelStyles() {
    $('.travel-styles-ui').html('')
    $('.travel-styles-ui').show()
    $.get('home/listTravelstylesForSelect', function (data) {
        var data = JSON.parse(data);

        data.results.forEach(function (s_item, index) {
           $('.travel-styles-ui').append('<li class="style-item"><div class="item-wrapper" itemId="'+ s_item.id +'">'+ s_item.text +'</div></li>')
        });
    });
}

function searchTravelStyles(item) {
    $('.travel-styles-ui').show()
    $.get('home/searchTravelstyles', {q:item}, function (data) {
        var data = JSON.parse(data);
       $('.travel-styles-ui').html('')
        data.results.forEach(function (s_item, index) {
           $('.travel-styles-ui').append('<li class="style-item"><div class="item-wrapper" itemId="'+ s_item.id +'">'+ s_item.text +'</div></li>')
        });
    });
}

