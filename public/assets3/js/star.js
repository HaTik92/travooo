window.onload = function () {
    // we select the necessary elements
    const LikeStar = document.querySelectorAll('.placehold > input');

    // iterate over all found elements and hang events on them
    [].forEach.call(LikeStar, function(element) {
        element.onclick = function(e) {

            document.querySelector('.slider > svg rect').style.opacity = '0';

        }
    });
}
