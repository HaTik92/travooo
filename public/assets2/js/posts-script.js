var Upload;
function dragStartHandler(ev){
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();

}

function dragEndHandler(ev){
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();

}

// Upload images/video drag and drop
function dropHandler(ev) {
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();

    if (ev.dataTransfer.items) {
        // Use DataTransferItemList interface to access the file(s)

        for (var i = 0; i < ev.dataTransfer.items.length; i++) {
            // If dropped items aren't files, reject them
            if (ev.dataTransfer.items[i].kind === 'file') {
                var file = ev.dataTransfer.items[i].getAsFile(); 
                uploadTmpFiles(file);

            }
        }


    } else {
        // Use DataTransfer interface to access the file(s)
        for (var i = 0; i < ev.dataTransfer.files.length; i++) {
            var file =  ev.dataTransfer.files[i];
            uploadTmpFiles(file);
        }

    }
}

function uploadTmpFiles(file){
    var upload = new Upload(file);
    var uid = Math.floor(Math.random()*1000000);
    if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
        var fRead = new FileReader();
        fRead.onload = (function(file){
            return function(e) {
                var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element
                var button = $('<span/>').addClass('close').addClass('removeFile').click(function(){     //create close button element
                    var filename = upload.getName();
                    postFileDelete(filename, this);
                }).append('<span>&times;</span>');
                var imgitem = $('<div>').attr('uid', uid).append(img); //create Image Wrapper element
                imgitem = $('<div/>').addClass('img-wrap-newsfeed').append(imgitem).append(button);
                $('.post-create-input .plus-icon').before(imgitem);
                upload.doUpload(uid, $("input[name=pair]").val());
                $('#sendPostForm').prop("disabled", false);
                $('.plus-icon').fadeIn();
            };
        })(file);
        fRead.readAsDataURL(file); //URL representing the file's data.
    } else if(/(\.|\/)(m4v|avi|mpg|mp4)$/i.test(file.type)){
        var fRead = new FileReader();
        fRead.onload = (function(file){
            return function(e) {
                var video = $('<video style="object-fit:cover;" width="151" height="130" controls>'+
                    '<source src='+e.target.result+' type="video/mp4">'+
                    '<source src="movie.ogg" type="video/ogg">'+
                    '</video>');
                var button = $('<span/>').addClass('close').addClass('removeFile').click(function(){     //create close button element
                    var filename = upload.getName();
                    postFileDelete(filename, this);
                }).append('<span>&times;</span>');
                var videoitem = $('<div/>').attr('attr', uid).append(video);
                videoitem = $('<div/>').addClass('img-wrap-newsfeed').append(videoitem).append(button);
                $('.post-create-input .plus-icon').before(videoitem);
                upload.doUpload(uid, $("input[name=pair]").val());
                $('#sendPostForm').prop("disabled", false);
                $('.plus-icon').fadeIn();
            };
        })(file);
        fRead.readAsDataURL(file); //URL representing the file's data.
    }
}
$(document).ready(function(){

    setTimeout(function() {
        $(".note-placeholder").css("display", "block");
    }, 6000);

    // for show success orerror messages

    setTimeout(function() {
        $("#postSavedSuccess").hide('blind', {}, 500)
    }, 3000);

    setTimeout(function() {
        $("#postSavedSuccess").hide('blind', {}, 500)
    }, 6000);


    $(document).on('click','i.trav-camera',function () {
        $("#createPosTxt").click();
        var target = $(this).attr('data-target');
        var target_el = $('#'+target);
        target_el.trigger('click');
    })

    // for show images in text area

        $('#sendPostForm').attr("disabled", true);

        $("#createPostTxt").keyup(function(){
            if(($('#createPostTxt').val().trim().length === 0) && ($('.form-horizontal').find('.medias').children().length == 1) && ($(".select2Class").val() == null)) {
                $('#sendPostForm').attr("disabled", true);
            } else {
                $('#sendPostForm').removeAttr("disabled");
            }
        });

        $("#file").change(function(){
            if(($('#createPostTxt').val().trim().length === 0) && ($('.form-horizontal').find('.medias').children().length == 1) && ($(".select2Class").val() == null)) {
                $('#sendPostForm').attr("disabled", true);
            } else {
                $('#sendPostForm').removeAttr("disabled");
            }
        });

        $(".select2Class").change(function(){
            if(($('#createPostTxt').val().trim().length === 0) && ($('.form-horizontal').find('.medias').children().length == 1) && ($(".select2Class").val() == null)) {
                $('#sendPostForm').attr("disabled", true);
            } else {
                $('#sendPostForm').removeAttr("disabled");
            }
        });
        
        $('#file').on('change', function(){

            if(($('.form-horizontal').find('.medias').children().length + $(this)[0].files.length) > 11)
            {
                if ($(this).data('page') !== 'home') {
                    alert("The maximum number of files to upload is 10.");
                }
                $(this).val("");
                return;
            }
            if (window.File && window.FileReader && window.FileList && window.Blob){ //check File API supported browser
                $('#createPostTxt').html('');
                
                var data = $(this)[0].files;
                $.each(data, function(index, file){ //loop though each file
                    uploadTmpFiles(file);
                });

                $(this).val("");

            }else{
                alert("Your browser doesn't support File API!"); //if File API is absent
            }

        });




    $('.select2Class').select2({
        width: '80%',
        placeholder: "Select a location",
        
        
        ajax: {
            url: './home/select2locations',
            dataType: 'json',
            delay: 500,
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }
    });

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $(document).on("click", ".location", function () {
            $('.locationSelect').show();
            $('.post-block.post-create-block .datepicker').show();
    });

    Upload = function (file) {
        this.file = file;
    };
    Upload.prototype.getType = function() {
        return this.file.type;
    };
    Upload.prototype.getSize = function() {
        return this.file.size;
    };
    Upload.prototype.getName = function() {
        return this.file.name;
    };
    Upload.prototype.doUpload = function (uid, pair) {
        var that = this;
        var formData = new FormData();
        var pathname = window.location.pathname;
        var path_params = pathname.split('/');
        if(path_params[1] == 'public'){
           var url = "/public/tmpupload";
        }else{
             var url = "/tmpupload";
        }
        // add assoc key values, this will be posts values
        formData.append("file", this.file, this.getName());
        formData.append("upload_file", true);
        formData.append("_token", $("input[name=_token]").val());
        formData.append("pair", pair);
        $("[attr="+ uid + "]").parent().hide();
        var spiner = $("[uid="+ uid + "]").closest('form').find('.spinner-border');
        var sendbtn = $("[attr="+ uid + "]").closest('form').find('button');
        
        sendbtn.hide();
        spiner.show();
        $.ajax({
            type: "POST",
            url: url,
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    // myXhr.upload.addEventListener('progress', that.progressHandling, false);
                }
                return myXhr;
            },
            success: function (data) {
                // your callback here
                $("[attr="+ uid + "]").parent().show();
                spiner.hide();
                sendbtn.show();
            },
            error: function (error) {
                spiner.hide();
                sendbtn.show();
                $("[attr="+ uid + "]").parent().remove();
                alert("The file can't be uploaded.");
                // handle error
            },
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000
        });
    };

    Upload.prototype.progressHandling = function (event) {
        var percent = 0;
        var position = event.loaded || event.position;
        var total = event.total;
        var progress_bar_id = "#progress-wrp";
        if (event.lengthComputable) {
            percent = Math.ceil(position / total * 100);
        }
        // update progressbars classes so it fits your code
        $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
        $(progress_bar_id + " .status").text(percent + "%");
    };
    
    /*Post text load more/less script*/
    $(document).on('click', '.post-more-link', function(){
        var parent = $(this).closest('.post-moreless-block');
        parent.find('.post-less-block').addClass('d-none');
        parent.find('.post-more-block').removeClass('d-none');
    })
    
    $(document).on('click', '.post-less-link', function(){
        var parent = $(this).closest('.post-moreless-block');
        parent.find('.post-less-block').removeClass('d-none');
        parent.find('.post-more-block').addClass('d-none');
    })
});


function postFileDelete(fileName, obj)
{
    $.ajax({
        method: 'POST',
        url: './tmpdelete',
        data: { pair: $('.form-horizontal').find("input[name=pair]").val(), fileName: fileName}
    })
    .done(function(){

        $(obj).parent().remove();
        if($('.form-horizontal').find('.medias').children().length == 1)
        {
            $('.plus-icon').fadeOut();
            $('#sendPostForm').attr("disabled", true);
        }
    });
}

function commentFileDelete(fileName, obj, pair)
{
    var pathname = window.location.pathname;
    var path_params = pathname.split('/');
    if(path_params[1] == 'public'){
       var url = "/public/tmpdelete";
    }else{
         var url = "/tmpdelete";
    }
         
    $.ajax({
        method: 'POST',
        url: url,
        data: { pair: pair, fileName: fileName}
    })
    .done(function(){

        $(obj).parent().remove();
        
    });
}
