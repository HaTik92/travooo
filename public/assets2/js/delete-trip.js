$(document).on('click','.confirm-trip-delete',function(e){
    let id = $(this).attr('data-id');
    let url = $(this).attr('data-url');
    let redirect_url = $(this).attr('data-redirect');
    $.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {"trip_id": id},
        success:function(res){
            if (res.status == 'success') {
                $('#deleteTripModal').modal('hide')
                $('.trip-' + id).fadeOut()
            } else {
                alert('@lang("profile.unknown_error_while_deleting_trip_plan")');
            }
        }
    })
});
$(document).on('click','.delete-trip',function(e){
    var id = $(this).attr('data-id');
    $('.confirm-trip-delete').attr('data-id', id);
})

// confirmation
$('.confirm-trip-deletion').on('click', function () {
    var trip_id = $(this).attr('data-id')
    $.confirm({
        title: '@lang("profile.are_you_sure")',
        content: '@lang("profile.if_you_canceled_this_trip_you_will_lose")',
        icon: 'fa fa-question-circle',
        animation: 'scale',
        closeAnimation: 'scale',
        opacity: 0.5,
        buttons: {
            'confirm': {
                text: 'Confirm',
                btnClass: 'btn-orange',
                action: function () {

                    $.ajax({
                        method: "POST",
                        url: "{{ route('trip.ajax_delete_trip')}}",
                        data: {"trip_id": trip_id}
                    })
                        .done(function (res) {
                            var result = JSON.parse(res);
                            if (result.status == 'success') {
                                //console.log(result.trip_city_id);
                                window.location.replace("{{ route('profile.plans') }}");
                            } else {
                                alert('@lang("profile.unknown_error_while_deleting_trip_plan")');
                            }


                        });
                }
            },
            cancel: function () {

            },

        }
    });
});
