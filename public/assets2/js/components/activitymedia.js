import BaseComponent from '../core/baseComponent';

export default class ActivityMediaComponent extends BaseComponent {
    _initProperty () {
        this.activityMediaTable = $('#activitymedia-table');
    }

    get url() {
        return this.activityMediaTable.data('url');
    }

    get config() {
        return this.activityMediaTable.data('config');
    }

    get delRoute() {
        return this.activityMediaTable.data('del');
    }

    _initBind () {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
        $(document).on('click','#select-all', this.selectAll.bind(this));
        $(document).on('click','#deselect-all', this.deselectAll.bind(this));
        $(document).on('click','#delete-all-selected', this.deleteAllSelected.bind(this));
    }

    _initPlugin() {
        super._initPlugin();
        this.activityMediaTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {
                    data: '',
                    name: '',
                },
                {
                    data: 'id',
                    name: this.config + '.id',
                    className: 'text-center'
                },
                {
                    data: 'transsingle',
                    name: 'transsingle.title',
                    className: 'text-center',
                    render: function (data) {
                        return data ? data.title : '-';
                    }
                },
                {
                    data: 'action',
                    name: 'action',
                    searchable: false,
                    sortable: false,
                    className: 'text-center',
                }
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });
    }

    selectAll(){
        if(this.activityMediaTable.find('tbody tr').length == 1){
            if(this.activityMediaTable.find('tbody tr td').hasClass('dataTables_empty')){
                return false;
            }
        }
        this.activityMediaTable.find('tbody tr').addClass('selected');
    }

    deselectAll(){
        this.activityMediaTable.find('tbody tr').removeClass('selected');
    }

    deleteAllSelected(){
        if(this.activityMediaTable.find('tbody tr.selected').length == 0){
            alert('Please select some rows first.');
            return false;
        }

        if(!confirm('Are you sure you want to delete the selected rows?')){
            return false;
        }
        var ids = '';
        var i = 0;
        this.activityMediaTable.find('tbody tr.selected').each(function(){
            if(i != 0){
                ids += ',';
            }
            ids += $(this).find('td:nth-child(2)').html();
            i++;
        });

        $.ajax({
            url: this.delRoute,
            type: 'post',
            data: {
                ids: ids
            },
            success: function(data){
                var result = JSON.parse(data);
                if(result.result == true){
                    document.location.reload();
                }
            }
        });
    }
}