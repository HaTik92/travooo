import Component from '../core/component';
import '../helpers/userPermissions';
export default class UserComponent extends Component {
    _initProperty () {
        this.userTable      = $('#users-table');
        this.status         = this.userTable.data('status') ? this.userTable.data('status') : false;
        this.trashed        = this.userTable.data('trashed') ? this.userTable.data('trashed') : false;
        this.utils          = $('#customer_phone').data('utils') ? $('#customer_phone').data('utils') : false;
        this.restore        = $('#restoreUser');
        this.deletePerm     = $('#deleteUserPerm');
    }

    get url() {
        return this.userTable.data('url');
    }

    get config() {
        return this.userTable.data('config');
    }

    get userDelRoute() {
        return this.userTable.data('del');
    }

    _initBind () {
        $(document).on('click','#select-all', this.selectAll.bind(this));
        $(document).on('click','#deselect-all', this.deselectAll.bind(this));
        $(document).on('click','#delete-all-selected', this.deleteAllSelected.bind(this));
        $(document).on('click','.submit_button', this.getNumber.bind(this));
        $(document).on('click',"a[name='delete_user_perm']", this.deleteUserPerm.bind(this));
        $(document).on('click',"a[name='restore_user']", this.restoreUser.bind(this));
    }

    _initPlugin () {
        this.userTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets: [0]
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: this.status, trashed: this.trashed}
            },
            columns: [
                {data: null, name: null, defaultContent: ''},
                {data: 'id', name: this.config + '.id'},
                {data: 'name', name: this.config + '.name'},
                {data: 'email', name: this.config + '.email'},
                {data: 'confirmed', name: this.config + '.confirmed'},
                {data: 'roles', name: 'roles.name', sortable: false},
                {data: 'created_at', name: this.config + '.created_at'},
                {data: 'updated_at', name: this.config + '.updated_at'},
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });

        $('.select2Class').select2();

        $("#customer_phone").intlTelInput({
            autoHideDialCode: false,
            formatOnDisplay: true,
            separateDialCode: true,
            utilsScript: this.utils
        });

        $('.datepicker').datepicker($.extend({
                inline: true,
                changeYear: true,
                changeMonth: true,
            },
            $.datepicker.regional['en']
        ));
    }

    selectAll(){
        if(this.userTable.find('tbody tr').length == 1){
            if(this.userTable.find('tbody tr td').hasClass('dataTables_empty')){
                return false;
            }
        }
        this.userTable.find('tbody tr').addClass('selected');
    }

    deselectAll(){
        this.userTable.find('tbody tr').removeClass('selected');
    }

    deleteAllSelected(){
        if(this.userTable.find('tbody tr.selected').length == 0){
            alert('Please select some rows first.');
            return false;
        }

        if(!confirm('Are you sure you want to delete the selected rows?')){
            return false;
        }
        var ids = '';
        var i = 0;
        this.userTable.find('tbody tr.selected').each(function(){
            if(i != 0){
                ids += ',';
            }
            ids += $(this).find('td:nth-child(2)').html();
            i++;
        });

        $.ajax({
            url: this.userDelRoute,
            type: 'post',
            data: {
                ids: ids
            },
            success: function(data){
                var result = JSON.parse(data);
                if(result.result == true){
                    document.location.reload();
                }
            }
        });
    }

    getNumber(e) {
        e.preventDefault();
        var countryData = $('#customer_phone').intlTelInput('getSelectedCountryData');
        var tel = '+'+countryData['dialCode']+' '+$('#customer_phone').val();
        $('#server_phone').val(tel);

        // *************** validate phone and fax before submit
        if($('#customer_phone').intlTelInput('isValidNumber') || $('#customer_phone').val() === '') {
            $('#User_form').submit();
        }else{
            var text = '';
            if($('#customer_phone').val() == ''){
                text = 'Phone number must not be empty.';
            }else{
                text = 'Phone number should be according to the given format';
            }
            var msg = '<div id="required-alert" class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> ' + text + '.</div>';

            $('.required_msg').html(msg);
            $("#required-alert").fadeTo(5000, 500).slideUp(500, function(){
                $("#required-alert").slideUp(500);
            });
        }
    }

    deleteUserPerm(e) {
        e.preventDefault();
        var elem = e.target.parentElement;
        var linkURL = elem.getAttribute('href');

        swal({
            title: this.restore.data('title'),
            text: this.restore.data('text'),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: this.restore.data('confirmButtonText'),
            cancelButtonText: this.restore.data('cancelButtonText'),
            closeOnConfirm: false
        }, function(isConfirmed){
            if (isConfirmed){
                window.location.href = linkURL;
            }
        });
    };

    restoreUser(e) {
        e.preventDefault();
        var elem = e.target.parentElement;
        var linkURL = elem.getAttribute('href');

        console.log(elem)
        console.log(linkURL)
        swal({
            title: this.deletePerm.data('title'),
            text: this.deletePerm.data('text'),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: this.deletePerm.data('confirmButtonText'),
            cancelButtonText: this.deletePerm.data('cancelButtonText'),
            closeOnConfirm: false
        }, function(isConfirmed){
            if (isConfirmed){
                window.location.href = linkURL;
            }
        });
    };
}