import BaseComponent from "../core/baseComponent";
import Map from '../helpers/map';

export default class DestinationComponent extends BaseComponent {

    _initProperty () {
        this.countriesByNationalityTable  = $('#CountriesByNationalityTable');
        this.citiesByNationalityTable  = $('#CitiesByNationalityTable');
        this.selectElements = [
            $('#nationalitySelect'),
            $('#countriesSelect'),
            $('#citiesSelect'),
        ];

    }

    get url() {
        return this.countriesByNationalityTable.data('url');
    }

    get citiesUrl() {
        return this.citiesByNationalityTable.data('url');
    }


    _initPlugin () {
        this.countriesByNationalityTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: null, name: null, defaultContent: ''},
                {data: 'title', name: 'transsingle.title'},
                {data: 'countries', name: 'countries'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });

        this.citiesByNationalityTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.citiesUrl,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: null, name: null, defaultContent: ''},
                {data: 'title', name: 'transsingle.title'},
                {data: 'cities', name: 'cities'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });


        $.each(this.selectElements, function( index, value ) {
            value.select2({
                placeholder: value.data('placeholder'),
                minimumInputLength: 2,
                quietMillis: 10,
                ajax: {
                    url: value.data('url'),
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term),
                            page: params.page || 1
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.data,
                            pagination: {
                                more: data.paginate
                            }
                        };
                    },
                    cache: true
                }
            });
        });

        $('[data-toggle="tooltip"]').tooltip();
        $('.select2Class').select2();
    }
}
