import BaseComponent from "../core/baseComponent";

export default class LevelsComponent extends BaseComponent {
    _initProperty() {
        this.levelsTable = $('#levels-table');
    }

    get url() {
        return this.levelsTable.data('url');
    }

    get config() {
        return this.levelsTable.data('config');
    }

    _initBind() {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin() {
        super._initPlugin();
        this.levelsTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }
}