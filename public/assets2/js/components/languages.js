import BaseComponent from "../core/baseComponent";
import '../helpers/roleScript';

export default class UserRolesComponent extends BaseComponent {
    _initProperty () {
        this.languagesTable     = $('#languages-table');
        this.languagesProgTable = $('#progress-table');
    }

    get url() {
        return this.languagesTable.data('url');
    }

    get config() {
        return this.languagesTable.data('config');
    }

    get progUrl() {
        return this.languagesProgTable.data('url');
    }

    _initBind () {
        $(document).on('click','.refresh-progress', this.refreshProgress.bind(this));
    }

    _initPlugin () {
        super._initPlugin();
        this.languagesTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'title', name: this.config + '.title'},
                {data: 'code', name: this.config + '.code'},
                {
                    name: this.config + '.active',
                    data: 'active',
                    sortable: false,
                    searchable: false,
                    render: function(data) {
                        if (data == 1) {
                            return '<label class="label label-success">Active</label>';
                        } else {
                            return '<label class="label label-danger">Deactive</label>';
                        }
                    }
                },
                {
                    name: 'action',
                    data: 'action',
                    searchable: false,
                    sortable: false,
                    render: function (data) {
                        let tmpElem = document.createElement('div');
                        tmpElem.innerHTML = data;
                        tmpElem.querySelector('.btn-info').classList.add('hidden');
                        return tmpElem.innerHTML;
                    }
                }
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });

        this.languagesProgTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.progUrl,
                type: 'get'
            },
            columns: [
                {data: 'language_title'},
                {data: 'language_code'},
                {data: 'event'},
                {data: 'total_count'},
                {data: 'done_count', className: 'progress-done'},
                {data: 'refresh', name:'refresh', orderable: false, searchable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }

    refreshProgress (e) {
        e.preventDefault();
        var button = $(e.target.parentElement);
        var id = button.data('id');
        button.find('.refresh-progress-icon').hide();
        button.find('.refresh-progress-load-icon').show();
        $.ajax({
            url: 'translation-progress/'+ id,
            method: 'GET',
            success: function(data) {
                button.closest('tr').find('td').eq(0).html(data.language_title);
                button.closest('tr').find('td').eq(1).html(data.language_code);
                button.closest('tr').find('td').eq(2).html(data.event);
                button.closest('tr').find('td').eq(3).html(data.total_count);
                button.closest('tr').find('td.progress-done').html(data.done_count);
                button.find('.refresh-progress-load-icon').hide();
                button.find('.refresh-progress-icon').show();
            },
            error: function(xhr, textStatus, errorThrown) {

            }
        });
    }
}