import BaseComponent from "../core/baseComponent";

export default class TimingsComponent extends BaseComponent {
    _initProperty() {
        this.timingsTable = $('#timings-table');
    }

    get url() {
        return this.timingsTable.data('url');
    }

    get config() {
        return this.timingsTable.data('config');
    }

    _initBind() {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin() {
        super._initPlugin();
        this.timingsTable.dataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        })
    }
}