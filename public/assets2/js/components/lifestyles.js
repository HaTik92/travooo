import BaseComponent from "../core/baseComponent";

export default class LifestylesComponent extends BaseComponent {

    _initProperty () {
        this.lifestyleTable = $('#lifestyle-table');
    }

    get url() {
        return this.lifestyleTable.data('url');
    }

    get config() {
        return this.lifestyleTable.data('config');
    }

    _initBind () {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
        $(document).on('click', '.delete-image', this.delImg.bind(this));
    }

    _initPlugin () {
        super._initPlugin();
        this.lifestyleTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }

    delImg(e) {
        var id = $(e.currentTarget).attr('data-id');
        $(e.currentTarget).parent().remove();

        var value = $('#delete-images').val();

        if(value == ''){
            $('#delete-images').val(id);
        }else{
            value += ',' + id;
            $('#delete-images').val(value);
        }
    };
}