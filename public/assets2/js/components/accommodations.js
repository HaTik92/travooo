import BaseComponent from '../core/baseComponent';

export default class AccommodationsComponent extends BaseComponent {
    _initProperty() {
        this.accommodationsTable = $('#accommodations-table');
    }

    get url() {
        return this.accommodationsTable.data('url');
    }

    get config() {
        return this.accommodationsTable.data('config');
    }

    _initBind() {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin() {
        super._initPlugin();
        this.accommodationsTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }
}
