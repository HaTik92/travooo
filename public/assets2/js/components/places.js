import BaseComponent from "../core/baseComponent";
import Map from '../helpers/map';

export default class PlacesComponent extends BaseComponent {

    _initProperty () {
        this.table;
        this.placesTable    = $('#place-table');
        this.selectElements = [$('#citiesSelect')];
        this.mediaIndex     = 1;
        global.initAutocomplete = window.initAutocomplete = this.initAutocomplete;
        global.draw_markers = window.draw_markers = this.draw_markers;
        global.searchRoute = window.searchRoute = this.searchRoute;
        if (location.pathname.substring(1) !== 'admin/location/place') {
            Map.bindApiKey();
        }
    }

    get url() {
        return this.placesTable.data('url');
    }

    get config() {
        return this.placesTable.data('config');
    }

    get placeDelRoute() {
        return this.placesTable.data('del');
    }

    _initBind () {
        $(document).on('click','#select-all', this.selectAll.bind(this));
        $(document).on('click','#deselect-all', this.deselectAll.bind(this));
        $(document).on('click','#delete-all-selected', this.deleteAllSelected.bind(this));
        $(document).on('change','#not-address-filter', this.notAddressFilter.bind(this));
        $(document).on('change','#address-filter', this.addressFilter.bind(this));
        $(document).on('change','#country-filter', this.countryFilter.bind(this));
        $(document).on('change','#city-filter', this.cityFilter.bind(this));
        $(document).on('change','#place-type-filter', this.placeTypeFilter.bind(this));
        $(document).on('change','#media-done-filter', this.mediaDoneFilter.bind(this));
        $('textarea[maxlength]').keypress(this.textareaType.bind(this));
        $(document).on('click','.submit_button', this.submitValidation.bind(this));
        $(document).on('change','.country-input', this.countryInput.bind(this));
        //add icon
        $(document).on('click','#add_icon', this.addRow.bind(this));
        //remove icon row
        $(document).on('click','.delete_row', this.deleteRow.bind(this));

        //import
        $(document).on('change', '#country_id', this.getJsonCities.bind(this));

        //select featured image in image table
        $(document).on('change', '.selectFeatured', this.selectHeaderImage);
        $(document).on('change', '#select_all', this.importSelectAll.bind(this));
        $(document).on('change', '.checkbox', this.importCheckbox.bind(this));
    }

    _initPlugin () {
        this.table = this.placesTable.DataTable({
            lengthMenu: [ 10, 25, 50, 100, 1000,10000 ],
            deferRender: true,
            columnDefs: [ {
                orderable: true,
                className: 'select-checkbox',
                targets:   0,
            },
                { "width": "20%", "targets" : 4 },
                { "width": "20%", "targets" : 5 },
                { "searchable": false, "targets": 0 }
            ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: function (request) {
                    var titleIndex;
                    request.status = 1;
                    request.trashed = false;
                    request.columns.forEach(function (item, index) {
                        if (item.data == "address") {
                            request.columns[index].search.value = {matchValue: $('#address-filter').val(), notMatchValue: $('#not-address-filter').val()};
                        }
                        if (item.data == "title") {
                            titleIndex = index;
                        }
                    });
                    if (request.search.value != "") {
                        request.columns[titleIndex].search.value = request.search.value;
                        request.columns[titleIndex].search.regex = "false";
                        request.search.value = null;
                    }
                }
            },
            columns: [
                {data: null, name: null, defaultContent: '', orderable: false},
                {data: 'id', name: this.config + '.id', visible: true},
                {data: 'title', name: 'title', visible: true},
                {data: 'address', name: 'address', visible: true},
                {data: 'country_title', name: 'country_title', visible: true},
                {data: 'city_title', name: 'city_title', visible: true},
                {data: 'place_id_title', name: this.config + '.place_type', visible: true},
                {data: 'media_done_new', name: this.config + '.media_done', visible: true},
                {data: 'media_count', name: this.config + '.media_count', visible: true},
                {
                    name: this.config + '.active',
                    data: 'active',
                    sortable: false,
                    searchable: false,
                    render: function(data) {
                        if (data == 1) {
                            return '<label class="label label-success">Active</label>';
                        } else {
                            return '<label class="label label-danger">Deactive</label>';
                        }
                    }
                , visible: true},
                {data: 'action', name: 'action', searchable: false, sortable: false, visible: true},
                {data: 'country_title', name: this.config + '.countries_id', visible: false},
                {data: 'city_title', name: this.config + '.cities_id', visible: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500,
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
                var cities = [];
                var place_types = [];
                $('#place-table tbody tr').each(function(){
                    var temp_text = $(this).find('td:nth-child(6)').html();
                    cities[temp_text] = temp_text;
                    temp_text = $(this).find('td:nth-child(7)').html();
                    place_types[temp_text] = temp_text;
                });
                $('#place-table thead').append('<tr><td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td></tr>');
                var count = 0;
                $('#place-table thead tr:nth-child(2) td').each(function () {

                    if (count == 3){
                        $(this).html(
                            '<input type="text" id="address-filter" placeholder="match" class="custom-filters form-control" style="width:150px; margin-bottom: 5px" />' +
                            '<input type="text" id="not-address-filter" placeholder="not match" class="custom-filters form-control" style="width:150px" />'
                        );
                    }

                    if (count == 4){
                        $(this).html('<select id="country-filter" class="custom-filters form-control"><option value="">Search Country</option></select>');
                    }

                    if (count == 5){
                        $(this).html('<select id="city-filter" class="custom-filters form-control"><option value="">Search City</option></select>');
                    }

                    if (count == 6){
                        $(this).html('<select id="place-type-filter" class="custom-filters"><option value="">Search Place Type</option></select>');
                    }
                    if (count == 7){
                        $(this).html('<select id="media-done-filter" class="custom-filters"><option value="">Search Media Done</option><option value="1">Yes</option><option value="0">No</option></select>');
                    }
                    count++;
                });

                $('#country-filter').select2({
                    width:'100%',
                    placeholder: 'Search Country',
                    ajax: {
                        url: $('#placeCountry').val(),
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    }
                });
                $('#city-filter').select2({
                    width:'100%',
                    placeholder: 'Search City',
                    minimumInputLength: 2,
                    quietMillis: 10,
                    ajax: {
                        url: $('#searchCity').val(),
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: $.trim(params.term),
                                page: params.page || 1
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data.data,
                                pagination: {
                                    more: data.paginate
                                }
                            };
                        },
                    cache: true
                    }
                });
                $('#place-type-filter').select2({
                    width:'100%',
                    placeholder: 'Search Place Types',
                    minimumInputLength: 2,
                    quietMillis: 10,
                    ajax: {
                        url: $('#PlaceType').val(),
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: $.trim(params.term),
                                page: params.page || 1
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data.data,
                                pagination: {
                                    more: data.paginate
                                }
                            };
                        },
                        cache: true
                    }
                });
                $('#media-done-filter').select2({
                    width:'100%',
                    placeholder: 'Yes/No',
                });
            }
        });

        $.each(this.selectElements, function( index, value ) {
            value.select2({
                placeholder: value.data('placeholder'),
                minimumInputLength: 2,
                quietMillis: 10,
                ajax: {
                    url: value.data('url'),
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term),
                            page: params.page || 1,
                            countryId: $('.country-input').val()
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.data,
                            pagination: {
                                more: data.paginate
                            }
                        };
                    },
                    cache: true
                }
            });
        });

        if (/admin\/location\/place\/\d+$/.test(location.pathname.substring(1)) == false) {
            this.registerSummernote('.description', 5000, function (max) {
                $('.textarea-notification').text('maxlength' + max);
                if (max == 0) {
                    var msg = '<div id="language-alert" class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong>Description should not be greater than 5000 characters.</div>';
                    var textareaMsg = $('.description').parent().siblings('.textarea-msg')
                    textareaMsg.html(msg)
                    $(".textarea-msg").fadeTo(5000, 500).slideUp(500, function () {
                        $(".textarea-msg").slideUp(500);
                    });
                }
            });
        }

        $('[data-toggle="tooltip"]').tooltip();
        $('.select2Class').select2();
    }

    initAutocomplete() {
        var map;
        var searchBox;

        var markers = [];

        if (location.pathname.substring(1) == 'admin/location/place/import') {
            map = window.map = new google.maps.Map(document.getElementById('gmap'), {
                center: {lat: -33.8688, lng: 151.2195},
                zoom: 13
            });
        }

        if ($('#countryInfo').val() != null) {
            var countriesLocation = JSON.parse($('#countryInfo').val())[$('.country-input').val()];
            map = window.map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: Number(countriesLocation.lat), lng: Number(countriesLocation.lng)},
                zoom: 7,
                mapTypeId: 'roadmap'
            });
        }

        if ($('#placeInfo').val() != null) {
            var placeInfo = JSON.parse($('#placeInfo').val());
            map = window.map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: Number(placeInfo.lat), lng: Number(placeInfo.lng)},
                zoom: 13,
                mapTypeId: 'roadmap'
            });

            var myLatLng = {lat: Number(placeInfo.lat), lng: Number(placeInfo.lng)};

            markers.push(new google.maps.Marker({
                map: map,
                position: myLatLng,
                draggable : true,
            }));

            google.maps.event.addListener(markers[0], 'dragend', function (evt) {
                var lat =  evt.latLng.lat().toFixed(3);
                var longit = evt.latLng.lng().toFixed(3);
                document.getElementById('lat-lng-input').setAttribute("value", lat + "," + longit );
                document.getElementById('lat-lng-input_show').setAttribute("value", lat + "," + lat );
            });
        }

        // Create the search box and link it to the UI element.
        if (document.getElementById('pac-input')) {
            var input = document.getElementById('pac-input');
            if (location.pathname.substring(1) !== 'admin/location/place/import') {
                var options = {
                    componentRestrictions: {country: countriesLocation.iso_code}
                };
                searchBox = window.searchBox = new google.maps.places.Autocomplete(input, options);

                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                searchBox.bindTo('bounds', map);
                searchBox.addListener('place_changed', function () {
                    var place = searchBox.getPlace();

                    if (!place) {
                        return;
                    }
                    var marker = new google.maps.Marker({
                        map: map,
                        icon: {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        },
                        title: place.name,
                        draggable: true,
                        position: place.geometry.location
                    });

                    var latitude = place.geometry.location.lat();
                    var longitude = place.geometry.location.lng();
                    $('#lat-lng-input').val(latitude + "," + longitude);
                    $('#lat-lng-input_show').val(latitude + "," + longitude);

                    google.maps.event.addListener(marker, 'dragend', function (evt) {
                        $('#lat-lng-input').val(evt.latLng.lat().toFixed(3) + "," + evt.latLng.lng().toFixed(3));
                    });

                    var bounds = new google.maps.LatLngBounds();

                    if (place.geometry.viewport) {
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }

                    map.fitBounds(bounds);
                });
            } else {
                searchBox = window.searchBox = new google.maps.places.SearchBox(input);
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                // Bias the SearchBox results towards current map's viewport.
                map.addListener('bounds_changed', function () {
                    searchBox.setBounds(map.getBounds());
                });

                var markers = [];
                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function () {
                    var places = searchBox.getPlaces();

                    if (places.length == 0) {
                        return;
                    }

                    // Clear out the old markers.
                    markers.forEach(function (marker) {
                        marker.setMap(null);
                    });
                    markers = [];

                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();
                    places.forEach(function (place) {
                        if (!place.geometry) {
                            console.log("Returned place contains no geometry");
                            return;
                        }
                        var icon = {
                            url: place.icon,
                            size: new google.maps.Size(71, 71),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(25, 25)
                        };

                        // Create a marker for each place.
                        markers.push(new google.maps.Marker({
                            map: map,
                            icon: icon,
                            title: place.name,
                            position: place.geometry.location
                        }));

                        if (place.geometry.viewport) {
                            // Only geocodes have viewport.
                            bounds.union(place.geometry.viewport);
                        } else {
                            bounds.extend(place.geometry.location);
                        }
                    });
                    map.fitBounds(bounds);
                });
                map.addListener('click', function (event) {
                    var loc = event.latLng.lat() + "," + event.latLng.lng();
                    document.getElementById('latlng').value = loc;
                    // Position = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());

                });
                map.addListener('idle', function (event) {
                    draw_markers(map);
                });
            }
        }
    }

    draw_markers(Map) {
        var bounds = Map.getBounds();
        var ne = bounds.getNorthEast(); // LatLng of the north-east corner
        var sw = bounds.getSouthWest(); // LatLng of the south-west corder
        var searchRoute = $('#gmap').data('search');
        var mrks = [];
        $.ajax({
            type: "GET",
            url: searchRoute,
            data: {ne_lat: ne.lat(), ne_lng: ne.lng(), sw_lat: sw.lat(), sw_lng: sw.lng()},
            success: function (data) {
                mrks = JSON.parse(data);
                for (var i = 0; i < mrks.length; i++) {
                    new google.maps.Marker({
                        position: new google.maps.LatLng(mrks[i].lat, mrks[i].lng),
                        map: Map
                    });
                }
            }
        });
    }

    countryInput() {
        let map = window.map;
        let searchBox = window.searchBox;
        let countriesLocation = JSON.parse($('#countryInfo').val())[$('.country-input').val()];
        map.setCenter({
            lat: Number(countriesLocation.lat),
            lng: Number(countriesLocation.lng)
        });
        searchBox.componentRestrictions.country = countriesLocation.iso_code;

        $('#citiesSelect').val(null).trigger("change");
    }

    selectAll(){
        if(this.placesTable.find('tbody tr').length == 1){
            if(this.placesTable.find('tbody tr td').hasClass('dataTables_empty')){
                return false;
            }
        }
        this.placesTable.find('tbody tr').addClass('selected');
    }

    deselectAll(){
        this.placesTable.find('tbody tr').removeClass('selected');
    }

    deleteAllSelected(){
        if(this.placesTable.find('tbody tr.selected').length == 0){
            alert('Please select some rows first.');
            return false;
        }

        if(!confirm('Are you sure you want to delete the selected rows?')){
            return false;
        }
        $('#deleteLoadingPlace').show();
        $('#deleteLoadingPlace .fa-spin').css("z-index","5");
        $('.table-responsive').css("opacity","0.2");
        $('.select-checkbox').click(function(e) {
            e.stopPropagation();
        });
        var ids = '';
        var i = 0;
        this.placesTable.find('tbody tr.selected').each(function(){
            if(i != 0){
                ids += ',';
            }
            ids += $(this).find('td:nth-child(2)').html();
            i++;
        });

        $.ajax({
            url: this.placeDelRoute,
            type: 'post',
            data: {
                ids: ids
            },
            success: function(data){
                var result = JSON.parse(data);
                if(result.result == true){
                    document.location.reload();
                }
            }
        });
    }

    notAddressFilter() {
        this.table.draw();
    }

    addressFilter() {
        this.table.draw();
    }

    countryFilter(e) {
        var val = $(e.currentTarget).val();
        var dataTable = this.table;
        if (val != ''){
            dataTable.settings().init().columns.forEach(function(item, index) {
                if(item.data == "country_title"){
                    if ( dataTable.columns(index).search() !== val ) {
                        dataTable.columns(index).search(val, false).draw();
                    }
                }
            });
        }
    }

    cityFilter(e) {
        var val = $(e.currentTarget).val();
        var dataTable = this.table;
        if (val != ''){
            dataTable.settings().init().columns.forEach(function(item, index) {
                if(item.data == "city_title"){
                    if ( dataTable.columns(index).search() !== val ) {
                        dataTable.columns(index).search(val, false).draw();
                    }
                }
            });
        }
    }

    placeTypeFilter(e) {
        var val = $(e.currentTarget).val();
        var dataTable = this.table;
        if (val != ''){
            dataTable.settings().init().columns.forEach(function(item, index) {
                if(item.data == "place_id_title"){
                    if ( dataTable.columns(index).search() !== val ) {
                        dataTable.columns(index).search(val, false).draw();
                    }
                }
            });
        }
    }

    mediaDoneFilter(e) {
        var val = $(e.currentTarget).val();
        var dataTable = this.table;
        if(val == '1' || val == 1){
            dataTable.settings().init().columns.forEach(function(item, index) {
                if(item.data == "media_done_new"){
                    dataTable.columns(index).search("1", false).draw();
                }
            });
        }else{
            dataTable.settings().init().columns.forEach(function(item, index) {
                if(item.data == "media_done_new"){
                    dataTable.columns(index).search("0", false).draw();
                }
            });
        }
    }

    registerSummernote(element, max, callbackMax) {
        $(element).summernote({
            height: 200,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    if (t.trim().length >= max) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    if (typeof callbackMax == 'function') {
                        callbackMax(max - t.trim().length);
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    document.execCommand('insertText', false, all.trim().substring(0, max));
                    if (typeof callbackMax == 'function') {
                        callbackMax(max - t.length);
                    }
                }
            }
        });
    }

    textareaType(event) {
        var key = event.which;
        var msg = '<div id="language-alert" class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong>Text should not be greater than 5000 characters.</div>';
        if(key != 8) {
            var maxLength = $(event.currentTarget).attr('maxlength');
            var length = event.currentTarget.value.length;
            if(length >= maxLength) {
                var textareaMsg = $(event.target).closest('.form-group').find('.textarea-msg')
                textareaMsg.html(msg)
                textareaMsg.fadeTo(5000, 500).slideUp(500, function () {
                    textareaMsg.slideUp(500);
                });
            }
        }
    }

    addRow(e) {
        var newRow = $("<tr></tr>"),
            tbCols = [];

        tbCols.push('<td class="set_img_col"><input type="file" name="license_images['+this.mediaIndex+'][image]"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][title]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][author_name]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][author_url]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][source_url]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][license_name]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][license_url]" class="form-control"></td>');

        tbCols.push('<td><a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a></td>');
        this.mediaIndex++;
        newRow.append(tbCols.join("\r\n"));

        if ($('.delete_row').length <= 9) {
            $("#addMoreRows").append(newRow);
        } else {
            $(".set_error_msg").html('Only 10 Images Rows can Added');
            $(".alert-warning").slideDown();
            setTimeout(function() {
                $(".alert-warning").slideUp();
            }, 2000);
            return false;
        }
    }

    deleteRow(e) {
        $(e.currentTarget).closest("tr").remove();
    }

    getJsonCities(e) {
        $.getJSON($('#getCities').val(), {
            countryID: parseInt($(e.currentTarget).val())
         },
        function (data) {
            var model = $('select#city_id');
            model.empty();
            model.append("<option value=''>Select City </option>");
            $.each(data, function (index, element) {
                model.append("<option value='" + element.id + "'>" + element.title + "</option>");
            });
        });
    }

    selectHeaderImage() {
        $('.selectFeatured').prop('checked', false).val(0);
        $(this).prop('checked', true).val(1);
    }

    importSelectAll(e) {
        $(".checkbox").prop('checked', $(e.currentTarget).prop("checked")); //change all ".checkbox" checked status
    }

    importCheckbox(e) {
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if (false == $(e.currentTarget).prop("checked")) { //if this item is unchecked
            $("#select_all").prop('checked', false); //change "select all" checked status to false
        }
        //check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length) {
            $("#select_all").prop('checked', true);
        }
    }
}