import BaseComponent from '../core/baseComponent';
import Map from '../helpers/map';

export default class ActivityComponent extends BaseComponent {
    _initProperty () {
        this.activityTable = $('#activities-table');
        global.initAutocomplete = window.initAutocomplete = this.initAutocomplete;
        if (location.pathname.substring(1) !== 'admin/activities/activity') {
            Map.bindApiKey();
        }
    }

    get url() {
        return this.activityTable.data('url');
    }

    get config() {
        return this.activityTable.data('config');
    }

    _initBind () {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin () {
        super._initPlugin();
        this.activityTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {data: 'lat', name: this.config + '.lat'},
                {data: 'lng', name: this.config + '.lng'},
                {
                    name: this.config + '.active',
                    data: 'active',
                    sortable: false,
                    searchable: false,
                    render: function(data) {
                        if (data == 1) {
                            return '<label class="label label-success">Active</label>';
                        } else {
                            return '<label class="label label-danger">Deactive</label>';
                        }
                    }
                },
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }

    initAutocomplete() {
        var placeLat = Number($('#map').data('lat'));
        var placeLng = Number($('#map').data('lng'));

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: placeLat, lng: placeLng},
            zoom: 13,
            mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        if (document.getElementById('pac-input')) {
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });
        }

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        if (location.pathname.substring(1) !== 'admin/activities/activity/create') {
            var myLatLng = {lat: placeLat, lng: placeLng};

            markers.push(new google.maps.Marker({
                map: map,
                position: myLatLng,
                draggable : true,
            }));

            google.maps.event.addListener(markers[0], 'dragend', function (evt) {
                var lat =  evt.latLng.lat().toFixed(3);
                var longit = evt.latLng.lng().toFixed(3);
                if (document.getElementById('lat-lng-input') && document.getElementById('lat-lng-input_show')) {
                    document.getElementById('lat-lng-input').setAttribute("value", lat + "," + longit );
                    document.getElementById('lat-lng-input_show').setAttribute("value", lat + "," + lat );
                }
            });
        }

        if (searchBox) {
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        draggable : true,
                        position: place.geometry.location
                    }));

                    var latitude = place.geometry.location.lat();
                    var longitude = place.geometry.location.lng();
                    document.getElementById('lat-lng-input').setAttribute("value", latitude + "," + longitude );
                    document.getElementById('lat-lng-input_show').setAttribute("value", latitude + "," + longitude );

                    google.maps.event.addListener(markers[0], 'dragend', function (evt) {
                        var lat =  evt.latLng.lat().toFixed(3);
                        var longit = evt.latLng.lng().toFixed(3);
                        document.getElementById('lat-lng-input').setAttribute("value", lat + "," + longit );
                        document.getElementById('lat-lng-input_show').setAttribute("value", lat + "," + longit );
                    });

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }
    }
}