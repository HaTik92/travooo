import BaseComponent from "../core/baseComponent";

export default class RegionsComponent extends BaseComponent {

    _initProperty () {
        this.regionsTable   = $('#regions-table');
    }

    get url() {
        return this.regionsTable.data('url');
    }

    get config() {
        return this.regionsTable.data('config');
    }

    get regionDelRoute() {
        return this.regionsTable.data('del');
    }

    _initBind () {
        $(document).on('click','#select-all', this.selectAll.bind(this));
        $(document).on('click','#deselect-all', this.deselectAll.bind(this));
        $(document).on('click','#delete-all-selected', this.deleteAllSelected.bind(this));
        $(document).on('click','.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin () {
        super._initPlugin();
        this.regionsTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1}
            },
            columns: [
                {data: null, name: null, defaultContent: ''},
                {data: 'id', name: this.config + '.id'},
                {data: 'title', name: 'transsingle.title'},
                {
                    name: this.config + '.active',
                    data: 'active',
                    sortable: false,
                    searchable: false,
                    render: function(data) {
                        if (data == 1) {
                            return '<label class="label label-success">Active</label>';
                        } else {
                            return '<label class="label label-danger">Deactive</label>';
                        }
                    }
                },
                {data: 'action', name: 'actions', searchable: false, sortable: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });
    }

    selectAll(){
        if(this.regionsTable.find('tbody tr').length == 1){
            if(this.regionsTable.find('tbody tr td').hasClass('dataTables_empty')){
                return false;
            }
        }
        this.regionsTable.find('tbody tr').addClass('selected');
    }

    deselectAll(){
        this.regionsTable.find('tbody tr').removeClass('selected');
    }

    deleteAllSelected(){
        if(this.regionsTable.find('tbody tr.selected').length == 0){
            alert('Please select some rows first.');
            return false;
        }

        if(!confirm('Are you sure you want to delete the selected rows?')){
            return false;
        }
        $('#deleteLoadingRegions').show();
        $('#deleteLoadingRegions .fa-spin').css("z-index","5");
        $('.table-responsive').css("opacity","0.2");
        $('.select-checkbox').click(function(e) {
            e.stopPropagation();
        });
        var ids = '';
        var i = 0;
        this.regionsTable.find('tbody tr.selected').each(function(){
            if(i != 0){
                ids += ',';
            }
            ids += $(this).find('td:nth-child(2)').html();
            i++;
        });

        $.ajax({
            url: this.regionDelRoute,
            type: 'post',
            data: {
                ids: ids
            },
            success: function(data){
                var result = JSON.parse(data);
                if(result.result == true){
                    document.location.reload();
                }
            }
        });
    }
}