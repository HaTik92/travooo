import BaseComponent from "../core/baseComponent";
import Map from '../helpers/map';

export default class CountriesComponent extends BaseComponent {

    _initProperty () {
        this.countriesTable  = $('#countries-table');
        this.mediaIndex      = 1;
        this.selectElements = [
            $('#currenciesSelect'),
            $('#citiesSelect'),
            $('#emergencyNumbersSelect'),
            $('#holidaysSelect'),
            $('#languagesSpokenSelect'),
            $('#additionalLanguagesSpokenSelect')
        ];
        global.initAutocomplete = window.initAutocomplete = this.initAutocomplete;
        if (location.pathname.substring(1) !== 'admin/location/country') {
            Map.bindApiKey();
        }
    }

    get url() {
        return this.countriesTable.data('url');
    }

    get config() {
        return this.countriesTable.data('config');
    }

    get countryDelRoute() {
        return this.countriesTable.data('del');
    }

    _initBind () {
        $(document).on('click','#select-all', this.selectAll.bind(this));
        $(document).on('click','#deselect-all', this.deselectAll.bind(this));
        $(document).on('click','#delete-all-selected', this.deleteAllSelected.bind(this));
        $('textarea[maxlength]').keypress(this.textareaType.bind(this));
        $(document).on('click','.submit_button', this.submitValidation.bind(this));
        //add icon
        $(document).on('click','#add_icon', this.addRow.bind(this));
        //remove icon row
        $(document).on('click','.delete_row', this.deleteRow.bind(this));
    }

    _initPlugin () {
        this.countriesTable.DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: null, name: null, defaultContent: ''},
                {data: 'id', name: this.config + '.id'},
                {data: 'title', name: 'transsingle.title'},
                {data: 'code', name: this.config + '.code'},
                {data: 'lat', name: this.config + '.lat'},
                {data: 'lng', name: this.config + '.lng'},
                {
                    name: this.config + '.active',
                    data: 'active',
                    sortable: false,
                    searchable: false,
                    render: function(data) {
                        if (data == 1) {
                            return '<label class="label label-success">Active</label>';
                        } else {
                            return '<label class="label label-danger">Deactive</label>';
                        }
                    }
                },
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });

        $.each(this.selectElements, function( index, value ) {
            value.select2({
                placeholder: value.data('placeholder'),
                minimumInputLength: 2,
                quietMillis: 10,
                ajax: {
                    url: value.data('url'),
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term),
                            page: params.page || 1
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.data,
                            pagination: {
                                more: data.paginate
                            }
                        };
                    },
                    cache: true
                }
            });
        });

        if (/admin\/location\/country\/\d+$/.test(location.pathname.substring(1)) == false) {
            this.registerSummernote('.description', 5000, function(max) {
                $('.textarea-notification').text('maxlength'+max)
                if (max == 0) {
                    var msg = '<div id="language-alert" class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong>Description should not be greater than 5000 characters.</div>';
                    var textareaMsg = $('.description').parent().siblings('.textarea-msg')
                    textareaMsg.html(msg)
                    $(".textarea-msg").fadeTo(5000, 500).slideUp(500, function () {
                        $(".textarea-msg").slideUp(500);
                    });
                }
            });
        }

        $('[data-toggle="tooltip"]').tooltip();
        $('.select2Class').select2();
    }

    initAutocomplete() {
        var placeLat = Number($('#map').data('lat'));
        var placeLng = Number($('#map').data('lng'));

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: placeLat, lng: placeLng},
            zoom: 9,
            mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        if (document.getElementById('pac-input')) {
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });
        }

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        if (location.pathname.substring(1) !== 'admin/location/country/create') {
            var myLatLng = {lat: placeLat, lng: placeLng};

            markers.push(new google.maps.Marker({
                map: map,
                position: myLatLng,
                draggable : true,
            }));

            google.maps.event.addListener(markers[0], 'dragend', function (evt) {
                var lat =  evt.latLng.lat().toFixed(3);
                var longit = evt.latLng.lng().toFixed(3);
                if (document.getElementById('lat-lng-input') && document.getElementById('lat-lng-input_show')) {
                    document.getElementById('lat-lng-input').setAttribute("value", lat + "," + longit );
                    document.getElementById('lat-lng-input_show').setAttribute("value", lat + "," + lat );
                }
            });
        }

        if (searchBox) {
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        draggable : true,
                        position: place.geometry.location
                    }));

                    var latitude = place.geometry.location.lat();
                    var longitude = place.geometry.location.lng();
                    document.getElementById('lat-lng-input').setAttribute("value", latitude + "," + longitude );
                    document.getElementById('lat-lng-input_show').setAttribute("value", latitude + "," + longitude );

                    google.maps.event.addListener(markers[0], 'dragend', function (evt) {
                        var lat =  evt.latLng.lat().toFixed(3);
                        var longit = evt.latLng.lng().toFixed(3);
                        document.getElementById('lat-lng-input').setAttribute("value", lat + "," + longit );
                        document.getElementById('lat-lng-input_show').setAttribute("value", lat + "," + longit );
                    });

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }
    }

    selectAll(){
        if(this.countriesTable.find('tbody tr').length == 1){
            if(this.countriesTable.find('tbody tr td').hasClass('dataTables_empty')){
                return false;
            }
        }
        this.countriesTable.find('tbody tr').addClass('selected');
    }

    deselectAll(){
        this.countriesTable.find('tbody tr').removeClass('selected');
    }

    deleteAllSelected(){
        if(this.countriesTable.find('tbody tr.selected').length == 0){
            alert('Please select some rows first.');
            return false;
        }

        if(!confirm('Are you sure you want to delete the selected rows?')){
            return false;
        }
        $('#deleteLoadingCountries').show();
        $('#deleteLoadingCountries .fa-spin').css("z-index","5");
        $('.table-responsive').css("opacity","0.2");
        $('.select-checkbox').click(function(e) {
            e.stopPropagation();
        });
        var ids = '';
        var i = 0;
        this.countriesTable.find('tbody tr.selected').each(function(){
            if(i != 0){
                ids += ',';
            }
            ids += $(this).find('td:nth-child(2)').html();
            i++;
        });

        $.ajax({
            url: this.countryDelRoute,
            type: 'post',
            data: {
                ids: ids
            },
            success: function(data){
                var result = JSON.parse(data);
                if(result.result == true){
                    document.location.reload();
                    var msg = '<div id="deleted-alert" class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Countries Deleted Successfully</div>';
                    $('.deleted_msg').html(msg);
                    $("#deleted-alert").fadeTo(5000, 500).slideUp(500, function(){
                        $("#deleted-alert").slideUp(500);
                    });
                }
            }
        });
    }

    registerSummernote(element, max, callbackMax) {
        $(element).summernote({
            height: 200,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    if (t.trim().length >= max) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    if (typeof callbackMax == 'function') {
                        callbackMax(max - t.trim().length);
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    document.execCommand('insertText', false, all.trim().substring(0, max));
                    if (typeof callbackMax == 'function') {
                        callbackMax(max - t.length);
                    }
                }
            }
        });
    }

    textareaType(event) {
        var key = event.which;
        var msg = '<div id="language-alert" class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong>Text should not be greater than 5000 characters.</div>';
        if(key != 8) {
            var maxLength = $(event.currentTarget).attr('maxlength');
            var length = event.currentTarget.value.length;
            if(length >= maxLength) {
                var textareaMsg = $(event.target).closest('.form-group').find('.textarea-msg')
                textareaMsg.html(msg)
                textareaMsg.fadeTo(5000, 500).slideUp(500, function () {
                    textareaMsg.slideUp(500);
                });
            }
        }
    }

    addRow(e) {
        var newRow = $("<tr></tr>"),
        tbCols = [];

        tbCols.push('<td class="set_img_col"><input type="file" name="license_images['+this.mediaIndex+'][image]"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][title]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][author_name]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][author_url]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][source_url]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][license_name]" class="form-control"></td>');
        tbCols.push('<td><input type="text" name="license_images['+this.mediaIndex+'][license_url]" class="form-control"></td>');

        tbCols.push('<td><a href="javascript:void(0);" class="btn btn-danger btn-xs add_icon delete_row">&dash;</a></td>');
        this.mediaIndex++;
        newRow.append(tbCols.join("\r\n"));

        if ($('.delete_row').length <= 9) {
            $("#addMoreRows").append(newRow);
        } else {
            $(".set_error_msg").html('Only 10 Images Rows can Added');
            $(".alert-warning").slideDown();
            setTimeout(function() {
                $(".alert-warning").slideUp();
            }, 2000);
            return false;
        }
    }

    deleteRow(e) {
        $(e.currentTarget).closest("tr").remove();
    }
}