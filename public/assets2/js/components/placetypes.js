import BaseComponent from "../core/baseComponent";

export default class PlacetypesComponent extends BaseComponent {
    _initProperty() {
        this.placetypesTable = $('#place-types-table');
    }

    get url() {
        return this.placetypesTable.data('url');
    }

    get config() {
        return this.placetypesTable.data('config');
    }

    _initBind() {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin() {
        super._initPlugin();
        this.placetypesTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }
}