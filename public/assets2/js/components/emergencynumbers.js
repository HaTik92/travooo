import BaseComponent from "../core/baseComponent";

export default class EmergencyComponent extends BaseComponent {

    _initProperty () {
        this.emergencyTable = $('#emergencynumbers-table');
    }

    get url() {
        return this.emergencyTable.data('url');
    }

    get config() {
        return this.emergencyTable.data('config');
    }

    _initBind () {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }
    _initPlugin () {
        super._initPlugin();
        this.emergencyTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1, trashed: false}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[1, "asc"]],
            searchDelay: 500
        });
    }
}