import BaseComponent from "../core/baseComponent";

export default class WeekdaysComponent extends BaseComponent {

    _initProperty () {
        this.weekdaysTable = $('#weekdays-table');
    }

    get url() {
        return this.weekdaysTable.data('url');
    }

    get config() {
        return this.weekdaysTable.data('config');
    }

    _initBind () {
        $(document).on('click', '.submit_button', this.submitValidation.bind(this));
    }

    _initPlugin () {
        super._initPlugin();
        this.weekdaysTable.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: this.url,
                type: 'post',
                data: {status: 1}
            },
            columns: [
                {data: 'id', name: this.config + '.id'},
                {data: 'transsingle.title', name: 'transsingle.title'},
                {data: 'action', name: 'action', searchable: false, sortable: false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    }
}