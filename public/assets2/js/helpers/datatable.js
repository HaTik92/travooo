module.exports = {
    hideDtColumn: function (table, index) {
        $(`thead tr th:nth-child(${index + 1})`, table).attr('style','display:none !important;');
        $(`thead tr th:nth-child(${index + 2})`, table).attr('style','display:none !important;');
        $(`thead tr:nth-child(2) td:nth-child(${index})`, table).attr('style','display:none !important;');
        $(`tbody tr td:nth-child(${index})`, table).attr('style','display:none !important;');
    }
}