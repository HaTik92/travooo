import $ from 'jquery';

export class Router {
    match (url) {
        switch (true) {

            // Dashboard
            case 'admin/dashboard' === url:
                require(['../components/dashboard.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Access Management
            case 'admin/access/user' === url:
            case 'admin/access/user/create' === url:
            case 'admin/access/user/deactivated' === url:
            case 'admin/access/user/non-confirmed' === url:
            case 'admin/access/user/suspended' === url:
            case 'admin/access/user/deleted' === url:
            case 'admin/access/user/logs' === url:
            case /admin\/access\/user\/\d+\/edit/.test(url):
                require(['../components/user.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Experts Management
            case 'admin/access/experts' === url:
                require(['../../../../resources/assets/js/components/experts.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Accommodations Management
            case 'admin/accommodations' === url:
            case 'admin/accommodations/create' === url:
            case /admin\/accommodations\/\d+\/edit/.test(url):
                require(['../components/accommodations.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Access Management -> Role Management
            case 'admin/access/role' === url:
            case 'admin/access/role/create' === url:
            case /admin\/access\/role\/\d+\/edit/.test(url):
                require(['../components/userRoles.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // AgeRanges Management
            case 'admin/ageranges' === url:
            case 'admin/ageranges/create' === url:
            case /admin\/ageranges\/\d+\/edit/.test(url):
                require(['../components/ageranges.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Activities Management
            case 'admin/activities/activity' === url:
            case 'admin/activities/activity/create' === url:
            case /admin\/activities\/activity\/\d+/.test(url):
            case /admin\/activities\/activity\/\d+\/edit/.test(url):
                require(['../components/activity.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Activity Types Management
            case 'admin/activities/activitytypes' === url:
            case 'admin/activities/activitytypes/create' === url:
            case /admin\/activities\/activitytypes\/\d+\/edit/.test(url):
                require(['../components/activitytypes.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Activity Media Management
            case 'admin/activitymedia' === url:
            case 'admin/activitymedia/create' === url:
            case /admin\/activitymedia\/\d+\/edit/.test(url):
                require(['../components/activitymedia.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Languages Manager
            case 'admin/access/languages' === url:
            case 'admin/access/languages/create' === url:
                require(['../components/languages.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Locations Manager -> Regions
            case 'admin/location/regions' === url:
            case 'admin/location/regions/create' === url:
            case /admin\/location\/regions\/\d+\/edit/.test(url):
                require(['../components/regions.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Locations Manager -> Countries
            case 'admin/location/country' === url:
            case 'admin/location/country/create' === url:
            case /admin\/location\/country\/\d+/.test(url):
            case /admin\/location\/country\/\d+\/edit/.test(url):
                require(['../components/countries.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Locations Manager -> Cities
            case 'admin/location/city' === url:
            case 'admin/location/city/create' === url:
            case /admin\/location\/city\/\d+/.test(url):
            case /admin\/location\/city\/\d+\/edit/.test(url):
                require(['../components/cities.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Locations Manager -> Places & Activities
            case 'admin/location/place' === url:
            case 'admin/location/place/create' === url:
            case /admin\/location\/place\/\d+/.test(url):
            case /admin\/location\/place\/\d+\/edit/.test(url):
            case 'admin/location/place/import' === url:
            case 'admin/location/place/search' === url:
                require(['../components/places.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Locations Manager -> Places & Activities
            case 'admin/location/placetypes' === url:
            case 'admin/location/placetypes/create' === url:
            case /admin\/location\/placetypes\/\d+/.test(url):
            case /admin\/location\/placetypes\/\d+\/edit/.test(url):
                require(['../components/placetypes.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Embassies Manager -> Embassies
            case 'admin/embassies' === url:
            case 'admin/embassies/create' === url:
            case /admin\/embassies\/\d+/.test(url):
            case /admin\/embassies\/\d+\/edit/.test(url):
            case 'admin/embassies/import' === url:
            case 'admin/embassies/search' === url:
                require(['../components/embassies.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Hobbies Manager -> Hobbies
            case 'admin/hobbies' === url:
            case 'admin/hobbies/create' === url:
            case /admin\/hobbies\/\d+/.test(url):
            case /admin\/hobbies\/\d+\/edit/.test(url):
                require(['../components/hobbies.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Hotels Manager -> Hotels
            case 'admin/hotels' === url:
            case 'admin/hotels/create' === url:
            case /admin\/hotels\/\d+/.test(url):
            case /admin\/hotels\/\d+\/edit/.test(url):
            case 'admin/hotels/import' === url:
            case 'admin/hotels/search' === url:
                require(['../components/hotels.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Interest Manager -> Interest
            case 'admin/interest' === url:
            case 'admin/interest/create' === url:
            case /admin\/interest\/\d+/.test(url):
            case /admin\/interest\/\d+\/edit/.test(url):
                require(['../components/interest.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // SafetyDegrees Manager -> SafetyDegrees
            case 'admin/safety-degrees/safety' === url:
            case 'admin/safety-degrees/safety/create' === url:
            case /admin\/safety-degrees\/safety\/\d+/.test(url):
            case /admin\/safety-degrees\/safety\/\d+\/edit/.test(url):
                require(['../components/safety-degrees.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Levels Manager -> Levels
            case 'admin/levels' === url:
            case 'admin/levels/create' === url:
            case /admin\/levels\/\d+/.test(url):
            case /admin\/levels\/\d+\/edit/.test(url):
                require(['../components/levels.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Restaurants Manager -> Restaurants
            case 'admin/restaurants' === url:
            case 'admin/restaurants/create' === url:
            case /admin\/restaurants\/\d+/.test(url):
            case /admin\/restaurants\/\d+\/edit/.test(url):
            case 'admin/restaurants/import' === url:
            case 'admin/restaurants/search' === url:
                require(['../components/restaurants.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Religions Manager -> Religions
            case 'admin/religion' === url:
            case 'admin/religion/create' === url:
            case /admin\/religion\/\d+\/edit/.test(url):
                require(['../components/religions.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Travel Styles -> Travel Styles
            case 'admin/lifestyle' === url:
            case 'admin/lifestyle/create' === url:
            case /admin\/lifestyle\/\d+\/edit/.test(url):
                require(['../components/lifestyles.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Languages Spoken Manager -> Languages Spoken
            case 'admin/languagesspoken' === url:
            case 'admin/languagesspoken/create' === url:
            case /admin\/languagesspoken\/\d+\/edit/.test(url):
                require(['../components/languagesSpoken.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Weekdays Manager -> Weekdays
            case 'admin/weekdays' === url:
            case 'admin/weekdays/create' === url:
            case /admin\/weekdays\/\d+\/edit/.test(url):
                require(['../components/weekdays.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Holidays Manager Manager -> Holidays
            case 'admin/holidays' === url:
            case 'admin/holidays/create' === url:
            case /admin\/holidays\/\d+\/edit/.test(url):
                require(['../components/holidays.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Emergency Numbers Manager -> Emergency Numbers
            case 'admin/emergencynumbers' === url:
            case 'admin/emergencynumbers/create' === url:
            case /admin\/emergencynumbers\/\d+\/edit/.test(url):
                require(['../components/emergencynumbers.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Currencies Manager -> Currencies
            case 'admin/currencies' === url:
            case 'admin/currencies/create' === url:
            case /admin\/currencies\/\d+\/edit/.test(url):
                require(['../components/currencies.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Timings Manager -> Timings
            case 'admin/timings' === url:
            case 'admin/timings/create' === url:
            case /admin\/timings\/\d+\/edit/.test(url):
                require(['../components/timings.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            // Cultures Manager -> Cultures
            case 'admin/cultures' === url:
            case 'admin/cultures/create' === url:
            case /admin\/cultures\/\d+\/edit/.test(url):
                require(['../components/cultures.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            //Pages Manager -> Pages
            case 'admin/pages' === url:
            case 'admin/pages/create' === url:
            case /admin\/pages\/\d+\/edit/.test(url):
            case 'admin/pages_categories' === url:
            case 'admin/pages_categories/create' === url:
            case /admin\/pages_categories\/\d+\/edit/.test(url):
                require(['../components/pages.js'], (module) => {
                    this._initComponent(module);
                });
                break;

            default:
                break;
        }
    }

    /**
     *
     * @param componentClass
     * @private
     */
    _initComponent (module) {
        $(document).ready(() => {
            let component = new module.default();
            component.init();
        });
    }
}
