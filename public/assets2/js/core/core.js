import $ from 'jquery';
import { Router } from './route';
import AdminLTE from './adminLTE';
import { dt } from 'datatables';
import 'datatables-select';
import 'select2';
import 'intlTelInput';
import 'datepicker';
import 'summernote';
import 'lodash';
import '../plugin/sweetalert/sweetalert.min';
import 'bootstrap-sass';
import 'axios';
import 'summernote';
import '../helpers/formHelper';

global.$ = global.jQuery = $;

let lte = new AdminLTE();
lte.init();

let routes = new Router();
routes.match(location.pathname.substring(1));

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'X-Requested-With': 'XMLHttpRequest'
    }
});