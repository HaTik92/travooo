var tripPlanner = tripPlanner || {};

tripPlanner.selectModule = (function () {
    var $dropdownSelector = $('[data-prop="dropdownSelect"]'),
        $toggleButton = $dropdownSelector.find('[data-toggle="dropdown"]'),
        $menu = $dropdownSelector.find('.dropdown-menu');

    init();

    function init() {
        showSelected();
        onChange();
    }

    function _getSelected() {
        var $input = $menu.find('[type="radio"]:checked');
        var ID = $input.attr('id');
        var $label = $menu.find('[for="' + ID + '"]');
        var labelName = $label.find('.card-name').text();
        var labelIconHTML = $label.find('.card-img-icon').html();

        return {$label, labelName, labelIconHTML};
    }

    function showSelected() {
        var {$label, labelName, labelIconHTML} = _getSelected();
        
        if (labelName === '') {
            labelName = 'Public'
        }

        $toggleButton.find('.text').text(labelName);
        $toggleButton.find('.card-img-icon').html(labelIconHTML);
    }

    function onChange() {
        var $input = $menu.find('[type="radio"]');

        $input.on('change', function () {
            showSelected();
        });
    }
})();

$(function () {
    tripPlanner.selectModule;
});