$(document).ready(function(){

    var isTouchDevice = "ontouchstart" in window || navigator.msMaxTouchPoints > 0;

    // Create post placeholder fix
    setTimeout(function(){
        $('#createPostBlock .note-placeholder').show();
    }, 4000);

    // Sticky left menu for desktop
    if(!isTouchDevice) {
        var leftMenu = $('#leftOutsideMenu');
        var mainHeader = $('.main-header');
        $(window).scroll(function() {
            var windowTop = $(window).scrollTop();
            if (windowTop > mainHeader.height()) {
                leftMenu.addClass('sticky');
            } else {
                leftMenu.removeClass('sticky');
            }
        });
    }

  $(function () {
    let tooltipElem = $('.overview-block [data-toggle="tooltip"]');
    if($(window).width() >= 1260){
      $(tooltipElem).tooltip({
        placement: 'top',
        trigger: 'hover'
      })
    } else if($(window).width() < 991){
      $(tooltipElem).tooltip({
        placement: 'left',
        trigger: 'click'
      })
    } else{
      $(tooltipElem).tooltip({
        placement: 'left'
      })
    }
  });

  $(function () {
    $('.side-toggler').on('click', function(){
      $(this).parents('.tab-pane').toggleClass('opened');
    })
    $('.inside-list li').on('click', function(){
      $(this).parents('.tab-pane').toggleClass('opened');
    })
  })
  $(function () {
    $('[data-toggle="tooltip"]').tooltip();
  })

  $(function () {
    $('.post-add-com-input textarea, .post-add-com-input input').on('keydown', function(){
      let textValue = this.value;
      if(textValue.length > 3){
        $(this).addClass("open");
        $(this).parents(".post-add-comment-block").find('.comment-foot-btn').show();
      } else{
        $(this).removeClass("open");
        $(this).parents(".post-add-comment-block").find('.comment-foot-btn').hide();
      }
    });
  });
  $(function () {
    // $('[data-toggle="popover"]').popover({
    //   trigger: 'focus'
    // });
    $('ul.topic-list li').popover({
      trigger: 'focus',
      container: 'body',
      placement: 'top'
    });
  })

  $('#createPostTxt').on('keyup', function(){
    let inputVal = $(this).val();
    let postBtn = $(this).parents('.post-create-block').find('.btn-light-primary');
      if (!$(this).hasClass('post-create')) {
        $('body').addClass('postWritted');
      }
      $(postBtn).removeClass('btn-disabled');
      $(this).parents('.post-create-block').addClass('activated');

  });

  $('#createPostTxt').on('click', function(){
    let inputVal = $(this).val();
    let postBtn = $(this).parents('.post-create-block').find('.btn-light-primary');
      if (!$(this).hasClass('post-create')) {
        $('body').addClass('postWritted');
      }
      $(postBtn).removeClass('btn-disabled');
      $(this).parents('.post-create-block').addClass('activated');

  });


  $('body').click(function(evt){
       if(evt.target.id == "createPostBlock")
          return;
       //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
       if($(evt.target).closest('#createPostBlock').length)
          return;

      //Do processing of click event here for every element except with id menu_content
      if($('body').hasClass('postWritted'))
      {
        if($('#createPostTxt').val().trim() != "" || $('.form-horizontal').find('.medias').children().length != 1)
        {

          if(!confirm('Do you want to discard your post?'))
          return;
          $('#createPostTxt').val('');
          $('.form-horizontal').find('.medias').find('.removeFile').each(function(){
            $(this).click();
          });
        }
        $('body').removeClass('postWritted');
        $('#sendPostForm').attr("disabled", true);
      }

});


  $('.check-block').click(function(){
    $(this).toggleClass('checked-block');
  });

  $('#filterToggler').on('click', function(e){
    e.preventDefault();
    $('#leftOutsideMenu').toggleClass('filter-open');
  });
  $('.leftOutsideMenuClose').on('click', function(){
    if($('#leftOutsideMenu').hasClass('filter-open')){
      $('#leftOutsideMenu').removeClass('filter-open');
    }
  });
  $('#sidebarToggler, #tripSidebarToggler').on('click', function(e){
    e.preventDefault();
    $(this).toggleClass('hidden');
    $('#sidebarLayer').toggleClass('sidebar-open');
  });
  $('#leftSidebarToggler').on('click', function(e){
    e.preventDefault();
    $(this).toggleClass('hidden');
    $('#sidebarSetting').toggleClass('sidebar-open');
  });
  $('#commentToggler').on('click', function(e){
    e.preventDefault();
    $('#galleryCommentWrap').toggleClass('comment-open');
  });

  // class changes binding
  (function( func ) {
    $.fn.addClass = function() { // replace the existing function on $.fn
      func.apply( this, arguments ); // invoke the original function
      this.trigger('classChanged'); // trigger the custom event
      return this; // retain jQuery chainability
    }
  })($.fn.addClass); // pass the original function as an argument

  (function( func ) {
    $.fn.removeClass = function() {
      func.apply( this, arguments );
      this.trigger('classChanged');
      return this;
    }
  })($.fn.removeClass);

  let mainHeaderHeight = $('.main-header').outerHeight();

  $('.btn-mobile-side').each(function(){
    let el = this;
    $(el).css('top', mainHeaderHeight);
  });
  $('.sidebar-layer').css('top', mainHeaderHeight);

  $('#sidebarSetting, #sidebarLayer, #leftOutsideMenu').on('classChanged', function(){
    if ($('#sidebarSetting').hasClass('sidebar-open') || $('#sidebarLayer').hasClass('sidebar-open') || $('#leftOutsideMenu').hasClass('filter-open')) {
      $('body').css('overflow','hidden');
    } else {
      $('body').removeAttr('style');
    }
  });

  $('header.main-header, .main-content-layer, .top-banner-wrap, .top-board-wrap, .full-width-block, .top-profile-layer, .trip-map').on('click', function(){
    if($('#leftOutsideMenu').hasClass('filter-open')){
      $('#leftOutsideMenu').removeClass('filter-open');
    }
    if($('#sidebarLayer, #sidebarSetting').hasClass('sidebar-open')){
      $('#sidebarLayer, #sidebarSetting').removeClass('sidebar-open');
      setTimeout(function(){
        $('#sidebarToggler, #leftSidebarToggler, #tripSidebarToggler').removeClass('hidden');
      }, 400);
    }
  });

  $(window).scroll(function() {
    let scroll = $(window).scrollTop();
    let scrollVar = mainHeaderHeight;
    let followHeaderHeigth = null;

    // let scrollVar = 50;
    // if($(window).width() >= 576){
    //   scrollVar = 70;
    // }
    if (scroll >= scrollVar && $(window).width() < 991) {
      $("#leftOutsideMenu, #sidebarLayer, #sidebarSetting").addClass('scrolled');
      $(".btn-mobile-side:not(.trip-sidebar-toggler)").css('top', 0);
      $("#sidebarSetting, #sidebarLayer:not(.trip-side), #leftOutsideMenu").css('top', 0);
      if($("#followHeader").length){
        followHeaderHeigth = $(followHeader).outerHeight();
        $("#filterToggler, #sidebarToggler, #leftSidebarToggler").css('top', followHeaderHeigth);
        $("#sidebarSetting, #sidebarLayer, #leftOutsideMenu").css('top', followHeaderHeigth);
      }
    } else{
      $("#leftOutsideMenu, #sidebarLayer, #sidebarSetting").removeClass('scrolled');
      $(".btn-mobile-side").removeAttr('style');
      // $("#sidebarSetting, #sidebarLayer, #leftOutsideMenu").css('top', mainHeaderHeight);
      // $("#filterToggler, #sidebarToggler, #leftSidebarToggler").removeAttr('style');
    }
    if (scroll >= scrollVar) {
      $('#headerTripPlan').addClass('scrolled');
      $('#headerTripPlan .head-trip-plan_trip-line').show();
    } else{
      $('#headerTripPlan').removeClass('scrolled');
      $('#headerTripPlan .head-trip-plan_trip-line').hide();
    }

    // page of places follow scroll
    if(scroll >= 80 && $('#followHeader')){
      $('#followHeader').addClass('scrolled');
    } else{
      $('#followHeader').removeClass('scrolled');
    }
  });

  // lightslider initialization
  var customSliderObject = {
    storySlider: $("#storySlider").lightSlider({
      item:1,
      pager: false,
      controls: false,
      loop: true
    }),
    trendingDescription: $("#trendingDescription").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true
    }),
    discoverNewDestination: $("#discoverNewDestination").lightSlider({
      autoWidth: true,
      pager: false,
      controls: false,
      slideMargin: 15,
      loop: true,
      responsive : [
        {
            breakpoint:767,
            settings: {
                slideMargin: 9,
            }
        }
     ],
    }),
    trendingActivity: $("#trendingActivity").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true
    }),
    trendingActivity2: $("#trendingActivity2").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true
    }),
    citiesSlider: $("#citiesSlider").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true
    }),
    placesSlider: $("#placesSlider").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true
    }),
    placeYouMightLike: $("#placeYouMightLike").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true
    }),
    placeYouMightLikePostCard: $("#placeYouMightLikePostCard").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true
    }),
    nationalHoliday: $("#nationalHoliday").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true
    }),
    emergencyNumbers: $("#emergencyNumbers").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true
    }),
    videoYouMightLikePostCard: $("#videoYouMightLikePostCard").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true
    }),
    placeInfoPostCard: $("#placeInfoPostCard").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true,
      responsive:[{
        breakpoint:575,
        settings: {
          item: 1,
          autoWidth: false
        }
      }]
    }),
    newPeopleDiscover: $("#newPeopleDiscover").lightSlider({
      autoWidth: true,
      pager: false,
      controls: false,
      slideMargin: 9,
      loop: true
    }),
    newTravelerDiscover: $("#newTravelerDiscover").lightSlider({
      autoWidth: true,
      pager: false,
      controls: false,
      slideMargin: 9,
      loop: true,
    }),
    trendingFlights: $("#trendingFlights").lightSlider({
      autoWidth:true,
      pager: false,
      controls: false,
      slideMargin: 20,
      loop: true,
      responsive:[{
        breakpoint:767,
        settings: {
          slideMargin: 9,
        }
      }]
    }),
    trendingTravelDescription: $("#trendingTravelDestinations").lightSlider({
      autoWidth:true,
      pager: false,
      slideMargin: 20,
      prevHtml: '<i class="trav-angle-left"></i>',
      nextHtml: '<i class="trav-angle-right"></i>',
      addClass: 'post-dest-slider-wrap',
      onBeforeStart: function (el) {
        el.removeAttr("style")
    },
    }),
    travelMateCountries: $(".travelMateCountries").each(function(i, e) {
        $(e).lightSlider({
            autoWidth: true,
            slideMargin: 0,
            pager: false,
            addClass: 'country',
            controls: false
        })
      }
    ),
  };

  $('.post-block .slide-link').on('click', function(e){
    e.preventDefault();
    if(!$(this).parents('.post-block').hasClass('modal-post-slider')){
      let slideBlockId = $(this).parents('.post-block').find('.post-slider').attr('id');
      if($(this).hasClass('slide-prev')){
        customSliderObject[slideBlockId].goToPrevSlide();
        console.log(slideBlockId);
      } else if($(this).hasClass('slide-next')){
        customSliderObject[slideBlockId].goToNextSlide();
      }
    }
  });

  $("#postProfileSlider").lightSlider({
    item:1,
    pager: false,
    slideMargin: 0,
    prevHtml: '<i class="trav-angle-left"></i>',
    nextHtml: '<i class="trav-angle-right"></i>',
    addClass: 'post-profile-block'
  });
  $("#postDestSlider, #postDestSliderInner1, #postDestSliderInner2, #postDestSliderInner3").lightSlider({
    pager: false,
    autoWidth:true,
    slideMargin: 8,
    prevHtml: '<i class="trav-angle-left"></i>',
    nextHtml: '<i class="trav-angle-right"></i>',
    addClass: 'post-dest-slider-wrap'
  });
  $("#tripDestSlider").lightSlider({
    pager: false,
    controls: false,
    autoWidth:true,
    slideMargin: 0,
    addClass: 'trip-destination-slider-block'
  });
  $("#weatherHourCarousel").lightSlider({
    pager: false,
    controls: true,
    autoWidth:true,
    slideMargin: 0,
    onSliderLoad: function(el) {
      $('.weather-carousel-control>a').on('click', function(e){
        e.preventDefault();
        if($(this).hasClass('slide-prev')){
          el.goToPrevSlide();
        }
        if($(this).hasClass('slide-next')){
          el.goToNextSlide();
        }
      })
    }

  });
  /* profile slider */
  $("#interestCountries, #interestCities").lightSlider({
    item:3,
    pager: false,
    slideMargin: 10,
    enableDrag: true,
    loop: true,
    prevHtml: '<i class="trav-angle-left"></i>',
    nextHtml: '<i class="trav-angle-right"></i>',
    addClass: 'post-profile-slider'
  });

  $("#postFollowSlider").lightSlider({
    item:1,
    pager: false,
    slideMargin: 20,
    enableDrag: false,
    prevHtml: '<i class="trav-angle-left"></i>',
    nextHtml: '<i class="trav-angle-right"></i>',
    addClass: 'post-follow-slider-wrap'
  });
  $("#tripLineSlider").lightSlider({
    autoWidth:true,
    pager: false,
    slideMargin: 10,
    prevHtml: '<i class="trav-angle-left"></i>',
    nextHtml: '<i class="trav-angle-right"></i>',
    addClass: 'trip-line-slider-wrap',
    onSliderLoad: function(){
      $('#headerTripPlan .head-trip-plan_trip-line').hide();
    }
  });
  // $("#postTabBlock").lightSlider({
  //   autoWidth: true,
  //   pager: false,
  //   controls: false,
  //   slideMargin: 0,
  //   addClass: 'post-tab-slider-wrap'
  //   // prevHtml: '<i class="trav-angle-left"></i>',
  //   // nextHtml: '<i class="trav-angle-right"></i>'
  // });

  $('.modal-child').on('hidden.bs.modal', function () {
    $('body').addClass('modal-open').css('padding-right', '15px');
  });

  $('#happenQuestionPopup').on('shown.bs.modal', function(){
    if(!$("#postFollowSlider2").hasClass('lightSlider')){
      $("#postFollowSlider2").lightSlider({
        item:1,
        pager: false,
        slideMargin: 20,
        enableDrag: false,
        prevHtml: '<i class="trav-angle-left"></i>',
        nextHtml: '<i class="trav-angle-right"></i>',
        addClass: 'post-follow-slider-wrap'
      });
    }
  });
  $('#generalQPhoto').on('shown.bs.modal', function(){
    // if(!$("#generalQPhotoSlider").hasClass('lightSlider')){
    //   $("#generalQPhotoSlider").lightSlider({
    //     item:1,
    //     pager: false,
    //     slideMargin: 0,
    //     enableDrag: false,
    //     prevHtml: '<i class="trav-angle-left"></i>',
    //     nextHtml: '<i class="trav-angle-right"></i>',
    //     addClass: 'post-modal-question-photo-slider'
    //   });
    // }

    let photoSlider = $('.question-photo-slider');
    photoSlider.owlCarousel({
      margin:0,
      center: true,
      item: 1,
      autoWidth:true,
      loop: true,
      nav: true,
      dots: false
    });
    $('.question-photo-slider-wrap .control-btn').click(function(e) {
      e.preventDefault();
      if($(this).hasClass('next-slider')){
        photoSlider.trigger('next.owl.carousel');
      }
      if($(this).hasClass('prev-slider')){
        photoSlider.trigger('prev.owl.carousel');
      }
    })
  });

  $('#mapModePopup').on('shown.bs.modal', function(){
    if(!$("#tripMapSlider").hasClass('lightSlider')){
      $("#tripMapSlider").lightSlider({
        item:1,
        pager: false,
        slideMargin: 20,
        enableDrag: false,
        prevHtml: '<i class="trav-angle-left"></i>',
        nextHtml: '<i class="trav-angle-right"></i>',
        addClass: 'post-modal-follow-slider-wrap'
      });
    }
    if(!$("#tripDestSlider1, #tripDestSlider2, #tripDestSlider3").hasClass('lightSlider')){
      $("#tripDestSlider1, #tripDestSlider2, #tripDestSlider3").lightSlider({
        pager: false,
        autoWidth:true,
        slideMargin: 0,
        prevHtml: '<i class="trav-angle-left"></i>',
        nextHtml: '<i class="trav-angle-right"></i>',
        addClass: 'trip-destination-slider-block',
        onSliderLoad: function(el){
          $(el).css('opacity', 1);
        }
      });
    }
  });

  $('.trip-map_wrapper .destination-point').on('click', function(){
    $(this).toggleClass('selected');
  })

  $('#mapPopup').on('shown.bs.modal', function(){
    let getTheDirect = function(elem){
      let sliderController = $(elem).find('.lslide.active .dest-slide-vector');
      let leftDirect, rightDirect;
      if(sliderController.data('direct') == 'left'){
        leftDirect = sliderController;
      }
      if(sliderController.data('direct') == 'right'){
        rightDirect = sliderController;
      }
      $(leftDirect).on('click', function(){
        elem.goToPrevSlide();
      });
      $(rightDirect).on('click', function(){
        elem.goToNextSlide();
      });
    }

    $("#modalMapSlider").lightSlider({
      item:1,
      pager: false,
      controls: false,
      enableDrag: false,
      addClass: 'post-map-slider',
      onSliderLoad: function(el){
        getTheDirect(el);
      },
      onAfterSlide: function(el){
        getTheDirect(el);
      }
    });
  });

  var inputFocusBlur = (id) => {
    $(id).on('focus', function(){
      $(this).parents('.search-block-wrap, .post-add-com-input').addClass('show');
      if(!$("#searchFilterList").hasClass('lightSlider')){
        $("#searchFilterList").lightSlider({
          autoWidth:true,
          pager: false,
          slideMargin: 0,
          prevHtml: '<i class="trav-angle-left"></i>',
          nextHtml: '<i class="trav-angle-right"></i>',
          addClass: 'filter-slider-wrap'
        });
      };
    });
    $(id).on('blur', function(){
      $(this).parents('.search-block-wrap, .post-add-com-input').removeClass('show');
    });
  };

  $('#addPlacePopup, #wouldVisitPlacePopup, #happenQuestionPopup').on('shown.bs.modal', function(){
    inputFocusBlur('#placeSearchInput');
    inputFocusBlur('#placeSearchInput2');
    inputFocusBlur('#searchInput');
  });

  // dropdown issue fix
  $('.dropdown-toggle, [data-toggle=dropdown]').dropdown();

  $('body').click(function(evt){
    if($(evt.target).hasClass('header-search-block'))
       return;
    //For descendants of header-search-block being clicked, remove this check if you do not want to put constraint on descendants.
    if($(evt.target).closest('.header-search-block').length)
       return;

   //Do processing of click event here for every element except with class header-search-block
   if($('.header-search-block').hasClass('show'))
   {
      $('.search-block .delete-search').click();
   }

  });

  $('.search-block .delete-search').on('click', function(e){
    $(e.target).parents('.header-search-block').removeClass('show').find('input').val(null);
    // $(e.target).parents('.search-block').find('input').val(null);
  })

  $('#inputCountry1, #inputCountry2').keyup(function(e){
    let value = $(e.target).val();
    if(value.length >= 3){
      $(this).parents('.input-wrap').addClass('show');
    } else{
      $(this).parents('.input-wrap').removeClass('show');
    }
  });

  $('#storyModePopup, #storiesModePopup').on('shown.bs.modal', function(){
    let placeNameList = $(this).find('.place-name-list');
    let currentItem = $(placeNameList).find('.current');
    $.each(currentItem, function(){
      let itemWidth = $(this).outerWidth()/2;
      let parentBlock = $(this).parent();
      let parentBlockWidth = $(this).parent().width();
      let itemOffsetToParent = $(this).offset().left - parentBlock.offset().left;

      let calcCount = itemWidth + itemOffsetToParent;

      $(parentBlock).css('left', -calcCount);
    });

    if(!$("#moreStorySlider").hasClass('lightSlider')){
      $("#moreStorySlider").lightSlider({
        autoWidth:true,
        pager: false,
        controls: false,
        slideMargin: 10,
        loop: true,
        addClass: 'more-story-slider',
        onSliderLoad: function(el){
          $(el).parents('.post-block').find('.slide-link').on('click', function(e){
            e.preventDefault();
            let slideBlockId = $(this).parents('.post-block').find('.post-slider').attr('id');
            if($(this).hasClass('slide-prev')){
              el.goToPrevSlide();
            } else if($(this).hasClass('slide-next')){
              el.goToNextSlide();
            }
          });
        }
      });
    }

    if(!$("#calendarSlider").hasClass('lightSlider')){
      $("#calendarSlider").lightSlider({
        item:3,
        pager: false,
        controls: false,
        loop:false,
        slideMove:1,
        slideMargin: 0,
        centerSlider: true,
        addClass: 'head-calendar-slider',
        onSliderLoad: function(el){
          $('.day-slider a.prevDay').on('click', function(e){
            e.preventDefault();
            el.goToPrevSlide();
          });
          $('.day-slider a.nextDay').on('click', function(e){
            e.preventDefault();
            el.goToNextSlide();
          });
        }
      });
    }

    if(!$("#tripLineSliderModal").hasClass('lightSlider')){
      $("#tripLineSliderModal").lightSlider({
        autoWidth:true,
        pager: false,
        slideMargin: 10,
        prevHtml: '<i class="trav-angle-left"></i>',
        nextHtml: '<i class="trav-angle-right"></i>',
        addClass: 'trip-line-slider-wrap',
        onSliderLoad: function(el){
          $(el).parents('#modalHeadTripPlan').hide();
        }
      });
    }

    $(this).scroll(function() {
      let scroll = $(this).scrollTop();
      let scrollVar = 50;
      if($(window).width() >= 576){
        scrollVar = 140;
      }
      if (scroll >= scrollVar) {
        $('#storyModePopup .modal-close').css('top', '17px');
        $('#modalHeadTripPlan').addClass('scrolled').show();
      } else{
        $('#storyModePopup .modal-close').css('top', '30px');
        $('#modalHeadTripPlan').removeClass('scrolled').hide();
      }
    })

  });

  if(!$("#createTripCalendarSlider").hasClass('lightSlider')){
    $("#createTripCalendarSlider").lightSlider({
      item:3,
      pager: false,
      controls: false,
      loop:false,
      slideMove:1,
      slideMargin: 0,
      centerSlider: true,
      addClass: 'head-calendar-slider',
      onSliderLoad: function(el){
        $('.day-slider a.prevDay').on('click', function(e){
          e.preventDefault();
          el.goToPrevSlide();
        });
        $('.day-slider a.nextDay').on('click', function(e){
          e.preventDefault();
          el.goToNextSlide();
        });
      }
    });
  };
  $('#storyModePopup').on('hidden.bs.modal', function(){
    $('#storyModePopup .modal-close').css('top', '30px');
    $('#modalHeadTripPlan').removeClass('scrolled').hide();
  });

  $('#storiesModePopup, #placeOneDayPopup, #tripPlansPopup').on('show.bs.modal', function(){
    $(this).find("#storiesModeSlider").lightSlider({
      autoWidth: true,
      pager: false,
      slideMargin: 15,
      prevHtml: '<i class="trav-angle-left"></i>',
      nextHtml: '<i class="trav-angle-right"></i>'
    });
    $(this).find("#postDestSliderInner4").lightSlider({
      pager: false,
      autoWidth:true,
      slideMargin: 8,
      prevHtml: '<i class="trav-angle-left"></i>',
      nextHtml: '<i class="trav-angle-right"></i>',
      addClass: 'post-dest-slider-wrap'
    });
  });

  $('.modal').on('shown.bs.modal', function(){
    let headComment = $(this).find('.post-comment-head');
    let headHeight = $(headComment).outerHeight();
    let scrollVar = 30;
    $(this).scroll(function() {
      let scroll = $(this).scrollTop();
      let headTop = scroll - scrollVar - 1;
      if (scroll >= scrollVar) {
        $(this).find('.post-modal-comment').addClass('head-fixed').css('padding-top',headHeight);
        $(headComment).css('top',headTop);
      } else{
        $(this).find('.post-modal-comment').removeClass('head-fixed').removeAttr('style');
        $(headComment).removeAttr('style');
      }
    });

    $('#requestPopup .trip-plan-row .trip-check-layer').on('click', function(){
      let tripPlan = $(this).parents('.trip-plan-inner').find('.trip-plan-row');
      $(tripPlan).removeClass('checked-plan');
      $(this).parents('.trip-plan-row').addClass('checked-plan');
    });

    $('#writeReviewLink').on('click', function(e){
      e.preventDefault();
      $(this).hide();
      $("#reviewAddCommentBlock").show();
    });

    $("#reviewAddCommentBlock .btn-clear").on('click', function(e){
      e.preventDefault();
      $(this).parents('#reviewAddCommentBlock').hide();
      $("#writeReviewLink").show();
    });


  });

  $('.message-chat').after().on('click', function(){
    $(this).parents('.message-block').addClass('hide-side');

  });
  $("#mobileSideToggler").on('click', function(){
    let messageBlock = $(this).parents('.message-block');
    $(messageBlock).toggleClass('hide-side');
  });

  $('#lightGalleryTrigger').on('click', function(){

    let $lg = $(this).lightGallery({
      dynamic: true,
      dynamicEl: [{
        "src": 'https://sachinchoolur.github.io/lightGallery/static/img/1-1600.jpg',
        'thumb': 'https://sachinchoolur.github.io/lightGallery/static/img/thumb-1.jpg',
        'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner'>
            <div class="top-gallery-content">
              <div class="top-info-layer">
                <div class="top-avatar-wrap">
                  <img src="http://placehold.it/50x50" alt="">
                </div>
                <div class="top-info-txt">
                  <div class="preview-txt">
                    <p class="dest-name">Rabat sale airport</p> 
                    <p class="dest-place">Airport in <span>Rabat-sale, Morocco</span></p>
                  </div>
                </div>
              </div>
              <div class="sub-post-info">
                <ul class="sub-list">
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-popularity-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">#4</div>
                      <div class="sub-txt">Popularity</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-safety-big-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">9/10</div>
                      <div class="sub-txt">Safety</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-user-rating-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">4.8/5</div>
                      <div class="sub-txt">User rating</div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="map-preview">
              <img src="./assets/image/map-preview.jpg" alt="map">
            </div>
            <div class='post-map-info-caption map-black'>
              <div class='map-avatar'>
                <img src='http://placehold.it/25x25'>
              </div>
              <div class='map-label-txt'><b>Suzanne</b> checked on <b>2 Sep</b> at <b>8:30 am</b> and stayed <b>30 min</b></div>
            </div>
          </div>
        </div>`
      }, {
        "src": 'https://sachinchoolur.github.io/lightGallery/static/img/2-1600.jpg',
        'thumb': 'https://sachinchoolur.github.io/lightGallery/static/img/thumb-2.jpg',
        'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner comment-block'>
            <div class="map-preview">
              <img src="./assets/image/map-preview.jpg" alt="map">
            </div>
            <div class='gallery-comment-wrap'>
              <div class='gallery-comment-inner mCustomScrollbar'>
                <div class="top-gallery-content gallery-comment-top">
                  <div class="top-info-layer">
                    <div class="top-avatar-wrap">
                      <img src="http://placehold.it/50x50" alt="">
                    </div>
                    <div class="top-info-txt">
                      <div class="preview-txt">
                        <p class="dest-name" href="#">Rabat sale airport</p> 
                        <p class="dest-place">Airport in <span>Rabat-sale, Morocco</span></p>
                      </div>
                    </div>
                  </div>
                  <div class="gallery-comment-top-line">
                    <p>
                      <a href="#" class="gallery-link">Photo</a>
                      by
                      <img src="http://placehold.it/22x22" alt="avatar">
                      <a href="#" class="gallery-link">Suzanne</a>
                      <span class="dot">·</span> 18 Jun 2017
                    </p>
                  </div>
                  <div class="gallery-comment-top-line">
                    <p>
                      Trip Plan
                      <a href="#" class="gallery-link">From Morocco to Japan in 7 Days</a>
                    </p>
                  </div>
                  <div class="gal-com-footer-info">
                    <div class="post-foot-block post-reaction">
                      <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                      <span><b>2</b> Reactions</span>
                    </div>
                  </div>
                </div>
                <div class="post-comment-layer">
                  <div class="post-comment-top-info">
                    <div class="comm-count-info">
                      5 Comments
                    </div>
                    <div class="comm-count-info">
                      3 / 20
                    </div>
                  </div>
                  <div class="post-comment-wrapper">
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-smile.png" alt="">
                            <span>21</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Amine</a>
                          <a href="#" class="comment-nickname">@ak0117</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-like.png" alt="">
                            <span>19</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-smile.png" alt="">
                            <span>15</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <a href="#" class="load-more-link">Load more...</a>
                  </div>
                </div>
              </div>
              <div class="post-add-comment-block">
                <div class="avatar-wrap">
                  <img src="http://placehold.it/45x45" alt="">
                </div>
                <div class="post-add-com-input">
                  <input type="text" placeholder="Write a comment">
                </div>
              </div>
            </div>
          </div>
        </div>`
      }, {
        "src": 'https://sachinchoolur.github.io/lightGallery/static/img/2-1600.jpg',
        'thumb': 'https://sachinchoolur.github.io/lightGallery/static/img/thumb-2.jpg',
        'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner comment-block'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-angle-left"></i>
                  </div>
                  <span>Back</span>
                </a>
              </li>
            </ul>
            <div class="top-gallery-content">
              <div class="sub-post-info">
                <ul class="sub-list">
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-popularity-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">#4</div>
                      <div class="sub-txt">Popularity</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-safety-big-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">9/10</div>
                      <div class="sub-txt">Safety</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-user-rating-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">4.8/5</div>
                      <div class="sub-txt">User rating</div>
                    </div>
                  </li>
                </ul>
                <div class="follow-btn-wrap">
                  <button type="button" class="btn btn-light-primary btn-bordered btn-icon-side btn-icon-right">
                    Interested
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                  </button>
                </div>
              </div>
            </div>
            <div class="map-preview">
              <img src="./assets/image/map-preview.jpg" alt="map">
            </div>
            <div class='gallery-comment-wrap'>
              <div class='gallery-comment-inner mCustomScrollbar'>
                <div class="top-gallery-content gallery-comment-top">
                  <div class="top-info-layer">
                    <div class="top-avatar-wrap">
                      <img src="http://placehold.it/50x50" alt="">
                    </div>
                    <div class="top-info-txt">
                      <div class="preview-txt">
                        <p class="dest-name" href="#">Grand Legacy At The Park</p> 
                        <p class="dest-place">Hotel - 6.27 km from Disneyland</p>
                      </div>
                    </div>
                  </div>
                  <div class="gal-com-footer-info">
                    <div class="post-foot-block post-reaction">
                      <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                      <span><b>2</b> Reactions</span>
                    </div>
                  </div>
                </div>
                <div class="post-comment-layer">
                  <div class="post-comment-top-info">
                    <div class="comm-count-info">
                      5 Comments
                    </div>
                    <div class="comm-count-info">
                      3 / 20
                    </div>
                  </div>
                  <div class="post-comment-wrapper">
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-smile.png" alt="">
                            <span>21</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Amine</a>
                          <a href="#" class="comment-nickname">@ak0117</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-like.png" alt="">
                            <span>19</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-smile.png" alt="">
                            <span>15</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <a href="#" class="load-more-link">Load more...</a>
                  </div>
                </div>
              </div>
              <div class="post-add-comment-block">
                <div class="avatar-wrap">
                  <img src="http://placehold.it/45x45" alt="">
                </div>
                <div class="post-add-com-input">
                  <input type="text" placeholder="Write a comment">
                </div>
              </div>
            </div>
          </div>
        </div>`
      }, {
        "src": 'https://sachinchoolur.github.io/lightGallery/static/img/2-1600.jpg',
        'thumb': 'https://sachinchoolur.github.io/lightGallery/static/img/thumb-2.jpg',
        'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner comment-block'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-angle-left"></i>
                  </div>
                  <span>Back</span>
                </a>
              </li>
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-flag-icon"></i>
                  </div>
                  <span>Report</span>
                </a>
              </li>
            </ul>
            <div class="top-gallery-content">
              <div class="sub-post-info">
                <ul class="sub-list">
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-popularity-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">#4</div>
                      <div class="sub-txt">Popularity</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-safety-big-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">9/10</div>
                      <div class="sub-txt">Safety</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-user-rating-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">4.8/5</div>
                      <div class="sub-txt">User rating</div>
                    </div>
                  </li>
                </ul>
                <div class="follow-btn-wrap">
                  <button type="button" class="btn btn-light-primary btn-bordered btn-icon-side btn-icon-right">
                    Interested
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                  </button>
                </div>
              </div>
            </div>
            <div class="map-preview">
              <img src="./assets/image/map-preview.jpg" alt="map">
            </div>
            <div class='gallery-comment-wrap'>
              <div class='gallery-comment-inner mCustomScrollbar'>
                <div class="top-gallery-content gallery-comment-top">
                  <div class="top-info-layer">
                    <div class="post-top-round-icon-wrap">
                      <i class="trav-event-icon"></i>
                    </div>
                    <div class="top-info-txt">
                      <div class="preview-txt">
                        <a class="post-name-link" href="#">Quick Chek New Jersey Festival of Ballooning</a>
                        <p>
                          <span>By</span>
                          <a href="#" class="post-name-link">Donec Ultrices Nunc</a>
                          <span class="dot">·</span>
                          <span>8 Days left</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="gal-com-footer-info">
                    <div class="post-foot-block post-reaction">
                      <img src="./assets/image/reaction-icon-smile-only.png" alt="smile">
                      <span><b>2</b> Reactions</span>
                    </div>
                    <div class="post-foot-block post-comment-place">
                      <i class="trav-location"></i>
                      <span class="place-name">New York City</span>
                    </div>
                  </div>
                </div>
                <div class="post-comment-layer">
                  <div class="post-comment-top-info">
                    <div class="comm-count-info">
                      5 Comments
                    </div>
                    <div class="comm-count-info">
                      3 / 20
                    </div>
                  </div>
                  <div class="post-comment-wrapper">
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-smile.png" alt="">
                            <span>21</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Amine</a>
                          <a href="#" class="comment-nickname">@ak0117</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-like.png" alt="">
                            <span>19</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-smile.png" alt="">
                            <span>15</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <a href="#" class="load-more-link">Load more...</a>
                  </div>
                </div>
              </div>
              <div class="post-add-comment-block">
                <div class="avatar-wrap">
                  <img src="http://placehold.it/45x45" alt="">
                </div>
                <div class="post-add-com-input">
                  <input type="text" placeholder="Write a comment">
                </div>
              </div>
            </div>
          </div>
        </div>`
      }, {
        "src": 'https://sachinchoolur.github.io/lightGallery/static/img/2-1600.jpg',
        'thumb': 'https://sachinchoolur.github.io/lightGallery/static/img/thumb-2.jpg',
        'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner comment-block'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-angle-left"></i>
                  </div>
                  <span>Back</span>
                </a>
              </li>
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-flag-icon"></i>
                  </div>
                  <span>Report</span>
                </a>
              </li>
            </ul>
            <div class="top-gallery-content">
              <div class="sub-post-info">
                <ul class="sub-list">
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-popularity-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">#4</div>
                      <div class="sub-txt">Popularity</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-safety-big-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">9/10</div>
                      <div class="sub-txt">Safety</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-user-rating-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">4.8/5</div>
                      <div class="sub-txt">User rating</div>
                    </div>
                  </li>
                </ul>
                <div class="follow-btn-wrap">
                  <button type="button" class="btn btn-light-primary btn-bordered btn-icon-side btn-icon-right">
                    Interested
                    <span class="icon-wrap"><i class="trav-view-plan-icon"></i></span>
                  </button>
                </div>
              </div>
            </div>
            <div class="map-preview">
              <img src="./assets/image/map-preview.jpg" alt="map">
            </div>
            <div class='gallery-comment-wrap'>
              <div class='gallery-comment-inner mCustomScrollbar'>
                <div class="top-gallery-content gallery-comment-top">
                  <div class="top-info-layer">
                    <div class="top-avatar-wrap">
                      <img src="http://placehold.it/50x50" alt="">
                    </div>
                    <div class="top-info-txt">
                      <div class="preview-txt">
                        <a class="dest-name" href="#">Julie</a> 
                        <p class="dest-place">uploaded a <b>photo</b> <span class="date">2 hours ago</span></p>
                      </div>
                    </div>
                  </div>
                  <div class="gallery-comment-txt">
                    <p>This is an amazing street to walk around and do some shopping</p>
                  </div>
                  <div class="gal-com-footer-info">
                    <div class="post-foot-block post-reaction">
                      <i class="trav-heart-fill-icon"></i>
                      <span><b>185</b></span>
                    </div>
                    <div class="post-foot-block post-comment-place">
                      <i class="trav-location"></i>
                      <span class="place-name">510 LaGuardia Pl, Paris, France</span>
                    </div>
                  </div>
                </div>
                <div class="post-comment-layer">
                  <div class="post-comment-top-info">
                    <div class="comm-count-info">
                      5 Comments
                    </div>
                    <div class="comm-count-info">
                      3 / 20
                    </div>
                  </div>
                  <div class="post-comment-wrapper">
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-smile.png" alt="">
                            <span>21</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <div class="post-comment-row happen-event">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Amine</a>
                          <a href="#" class="comment-nickname">@ak0117</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-like.png" alt="">
                            <span>19</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-smile.png" alt="">
                            <span>15</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <a href="#" class="load-more-link">Load more...</a>
                  </div>
                </div>
              </div>
              <div class="post-add-comment-block">
                <div class="avatar-wrap">
                  <img src="http://placehold.it/45x45" alt="">
                </div>
                <div class="post-add-com-input">
                  <input type="text" placeholder="Write a comment">
                </div>
              </div>
            </div>
          </div>
        </div>`
      }, {
        "src": 'https://sachinchoolur.github.io/lightGallery/static/img/13-1600.jpg',
        'thumb': 'https://sachinchoolur.github.io/lightGallery/static/img/thumb-13.jpg',
        'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner'>
            <div class="top-gallery-content">
              <div class="top-info-layer">
                <div class="top-avatar-wrap">
                  <img src="http://placehold.it/50x50" alt="">
                </div>
                <div class="top-info-txt">
                  <div class="preview-txt">
                    <p class="dest-name" href="#">Rabat sale airport</p> 
                    <p class="dest-place">Airport in <span>Rabat-sale, Morocco</span></p>
                  </div>
                </div>
              </div>
              <div class="sub-post-info">
                <ul class="sub-list">
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-popularity-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">#4</div>
                      <div class="sub-txt">Popularity</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-safety-big-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">9/10</div>
                      <div class="sub-txt">Safety</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-user-rating-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">4.8/5</div>
                      <div class="sub-txt">User rating</div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="map-preview">
              <img src="./assets/image/map-preview.jpg" alt="map">
            </div>
            <div class='post-map-info-caption map-black'>
              <div class='map-avatar'>
                <img src='http://placehold.it/25x25'>
              </div>
              <div class='map-label-txt'><b>Suzanne</b> checked on <b>2 Sep</b> at <b>8:30 am</b> and stayed <b>30 min</b></div>
            </div>
          </div>
        </div>`
      }, {
        "src": 'https://sachinchoolur.github.io/lightGallery/static/img/4-1600.jpg',
        'thumb': 'https://sachinchoolur.github.io/lightGallery/static/img/thumb-4.jpg',
        'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner'>
            <div class="top-gallery-content">
              <div class="top-info-layer">
                <div class="top-avatar-wrap">
                  <img src="http://placehold.it/50x50" alt="">
                </div>
                <div class="top-info-txt">
                  <div class="preview-txt">
                    <p class="dest-name" href="#">Rabat sale airport</p> 
                    <p class="dest-place">Airport in <span>Rabat-sale, Morocco</span></p>
                  </div>
                </div>
              </div>
              <div class="sub-post-info">
                <ul class="sub-list">
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-popularity-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">#4</div>
                      <div class="sub-txt">Popularity</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-safety-big-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">9/10</div>
                      <div class="sub-txt">Safety</div>
                    </div>
                  </li>
                  <li>
                    <div class="icon-wrap">
                      <i class="trav-user-rating-icon"></i>
                    </div>
                    <div class="ctxt">
                      <div class="top-txt">4.8/5</div>
                      <div class="sub-txt">User rating</div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="map-preview">
              <img src="./assets/image/map-preview.jpg" alt="map">
            </div>
            <div class='post-map-info-caption map-black'>
              <div class='map-avatar'>
                <img src='http://placehold.it/25x25'>
              </div>
              <div class='map-label-txt'><b>Suzanne</b> checked on <b>2 Sep</b> at <b>8:30 am</b> and stayed <b>30 min</b></div>
            </div>
          </div>
        </div>`
      }],

      addClass: 'main-gallery-block',
      pager: false,
      hideControlOnEnd: true,
      loop: false,
      slideEndAnimatoin : false,
      thumbnail:true,
      toogleThumb: false,
      thumbHeight: 100,
      thumbMargin: 20,
      thumbContHeight: 180,
      actualSize: false,
      zoom: false,
      autoplayControls: false,
      fullScreen: false,
      download: false,
      counter: false,
      mousewheel: false,
      appendSubHtmlTo: 'lg-item',
      prevHtml: '<i class="trav-angle-left"></i>',
      nextHtml: '<i class="trav-angle-right"></i>',
      hideBarsDelay: 100000000
    });

    $lg.on('onAfterOpen.lg',function(){
      $('body').css('overflow','hidden');
      let itemArr = [], thumbArr = [];
      let galleryBlock = $('.main-gallery-block');
      let galleryItem = $(galleryBlock).find('.lg-item');
      let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
      $.each(galleryItem, function(i, val){
        // itemArr.push(val);
      });
      $.each(galleryThumb, function(i, val){
        // thumbArr.push(val);
        let startCnt = `<div class="thumb-txt"><i class="trav-flag-icon"></i> start</div>`;
        let startCntEmpty = `<div class="thumb-txt">&nbsp;</div>`;
        let placetxt = 'rabar-sale airport'
        let placeName = `<div class="thumb-txt">${placetxt}</div>`;
        if(i == 0){
          $(val).addClass('place-thumb');
          $(val).append(placeName).prepend(startCnt);
        }
        if(i == 2){
          $(val).addClass('place-thumb');
          $(val).append(placeName).prepend(startCntEmpty);
        }
      });
    });
    $lg.on('onBeforeClose.lg',function(){
      $('body').removeAttr('style');
    });
    let setWidth = function(){
      let mainBlock = $('.main-gallery-block');
      let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
      let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

      let slide = $('.main-gallery-block .lg-item');
      let currentItem = $('.main-gallery-block .lg-current');
      let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
      let currentImg = $('.main-gallery-block .lg-current .lg-image');
      let currentCommentIs = $(subTtl).hasClass('comment-block');
      let currentImgPos = $(currentImg).position().top;
      setTimeout(function(){
        let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
        let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
        if(currentCommentIs){
          // console.log('yes');
          $(currentImgWrap).css('padding-right', commentWidth);
          $(subTtl).css('width', currentWidth + commentWidth);
        } else{
          $(currentImgWrap).removeAttr('style');
          $(subTtl).css('width', currentWidth);
        }
        $(subTtlWrp).show();
        $('.mCustomScrollbar').mCustomScrollbar();
      }, 500);
    }

    $lg.on('onSlideItemLoad.lg',function(e){

      setWidth();
      $(window).on('resize', function(){
        setWidth();
      })
    });
    $lg.on('onAfterSlide.lg',function(){
      setWidth();
    });
  });



  // profile gallery popup
  $('#profilePhotosTrigger').on('click', function(e){

    let $lg = $(this).lightGallery({
      dynamic: true,
      dynamicEl: [{
        "src": 'https://placehold.it/750x500',
        'thumb': 'https://sachinchoolur.github.io/lightGallery/static/img/thumb-2.jpg',
        'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-comment-icon"></i>
                    <span class="count">189</span>
                  </div>
                  <span>Comments</span>
                </a>
              </li>
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-flag-icon"></i>
                  </div>
                  <span>Report</span>
                </a>
              </li>
            </ul>
            <div class="top-gallery-content">
              <div class="top-info-layer photo-layer">
                <div class="top-info-txt">
                  <div class="preview-txt">
                    <p class="photo-name">5th Avenue, New York: The most expensive shopping street</p> 
                    <p class="photo-txt">
                      <span>June 2017</span>
                    </p>
                  </div>
                  <div class="reaction-wrap">
                    <img src="./assets/image/smile-heart.svg" alt="smile">
                    <span>185</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="photo-bottom">
              <div class="photo-caption">
                <div class="avatar-wrap">
                  <img src="http://placehold.it/25x25" alt="ava">
                </div>
                <div class="photo-label-txt">Where did you take this picture?</div>
              </div>
              <div class="photo-caption">
                <div class="avatar-wrap">
                  <img src="http://placehold.it/25x25" alt="ava">
                </div>
                <div class="photo-label-txt">I really love to walk in this beautiful street</div>
              </div>
            </div>
          </div>
        </div>`
      },{
        "src": 'https://sachinchoolur.github.io/lightGallery/static/img/2-1600.jpg',
        'thumb': 'https://sachinchoolur.github.io/lightGallery/static/img/thumb-2.jpg',
        'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner comment-block'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-angle-left"></i>
                  </div>
                  <span>Back</span>
                </a>
              </li>
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-flag-icon"></i>
                  </div>
                  <span>Report</span>
                </a>
              </li>
            </ul>
            <div class='gallery-comment-wrap'>
              <div class='gallery-comment-inner mCustomScrollbar'>
                <div class="top-gallery-content gallery-comment-top">
                  <div class="top-info-layer">
                    <div class="top-info-txt">
                      <div class="preview-txt">
                        <p class="dest-place">Added on <b>March 30, 2017</b></p>
                      </div>
                    </div>
                  </div>
                  <div class="gallery-comment-txt">
                    <p>This is an amazing street to walk around and do some shopping</p>
                  </div>
                  <div class="gal-com-footer-info">
                    <div class="post-foot-block post-reaction">
                      <i class="trav-heart-fill-icon"></i>
                      <span><b>185</b></span>
                    </div>
                    <div class="post-foot-block post-comment-place">
                      <i class="trav-location"></i>
                      <span class="place-name">510 LaGuardia Pl, Paris, France</span>
                    </div>
                  </div>
                </div>
                <div class="post-comment-layer">
                  <div class="post-comment-top-info">
                    <div class="comm-count-info">
                      5 Comments
                    </div>
                    <div class="comm-count-info">
                      3 / 20
                    </div>
                  </div>
                  <div class="post-comment-wrapper">
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-smile.png" alt="">
                            <span>21</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <div class="post-comment-row happen-event">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Amine</a>
                          <a href="#" class="comment-nickname">@ak0117</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-like.png" alt="">
                            <span>19</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-smile.png" alt="">
                            <span>15</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <a href="#" class="load-more-link">Load more...</a>
                  </div>
                </div>
              </div>
              <div class="post-add-comment-block">
                <div class="avatar-wrap">
                  <img src="http://placehold.it/45x45" alt="">
                </div>
                <div class="post-add-com-input">
                  <input type="text" placeholder="Write a comment">
                </div>
              </div>
            </div>
          </div>
        </div>`
      },{
        "src": 'https://placehold.it/750x500',
        'thumb': 'https://sachinchoolur.github.io/lightGallery/static/img/thumb-2.jpg',
        'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-comment-icon"></i>
                    <span class="count">189</span>
                  </div>
                  <span>Comments</span>
                </a>
              </li>
            </ul>
            <div class="top-gallery-content">
              <div class="top-info-layer photo-layer">
                <div class="top-info-txt">
                  <div class="preview-txt">
                    <p class="photo-name">Yellowstone National Park</p> 
                    <p class="photo-txt">
                      <i class="trav-set-location-icon"></i>
                      <span>National park in the United States of America</span>
                    </p>
                  </div>
                  <div class="reaction-wrap">
                    <img src="./assets/image/smile-heart.svg" alt="smile">
                    <span>17</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="photo-bottom">
              <div class="photo-caption">
                <div class="avatar-wrap">
                  <img src="http://placehold.it/25x25" alt="ava">
                </div>
                <div class="photo-label-txt">Where did you take this picture?</div>
              </div>
              <div class="photo-caption">
                <div class="avatar-wrap">
                  <img src="http://placehold.it/25x25" alt="ava">
                </div>
                <div class="photo-label-txt">I really love to walk in this beautiful street</div>
              </div>
            </div>
          </div>
        </div>`
      },{
        "src": 'https://sachinchoolur.github.io/lightGallery/static/img/2-1600.jpg',
        'thumb': 'https://sachinchoolur.github.io/lightGallery/static/img/thumb-2.jpg',
        'subHtml': `<div class='cover-block' style='display:none;'>
          <div class='cover-block-inner comment-block'>
            <ul class="modal-outside-link-list white-bg">
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-angle-left"></i>
                  </div>
                  <span>Back</span>
                </a>
              </li>
              <li class="outside-link">
                <a href="#">
                  <div class="round-icon">
                    <i class="trav-flag-icon"></i>
                  </div>
                  <span>Report</span>
                </a>
              </li>
            </ul>
            <div class='gallery-comment-wrap'>
              <div class='gallery-comment-inner mCustomScrollbar'>
                <div class="top-gallery-content gallery-comment-top">
                  <div class="top-info-layer">
                    <div class="top-info-txt">
                      <div class="preview-txt">
                        <a class="post-name-link" href="#">Yellowstone National Park</a> 
                        <p><span>Jun 2017</span></p>
                      </div>
                    </div>
                  </div>
                  <div class="gallery-comment-txt">
                    <p>Yellowstone National Park is a nearly 3,500-sq.-mile wilderness recreation... <a href="#" class="more-link">More</a></p>
                  </div>
                  <div class="gal-com-footer-info">
                    <div class="post-foot-block post-reaction">
                      <i class="trav-heart-fill-icon"></i>
                      <span><b>185</b></span>
                    </div>
                    <div class="post-foot-block post-comment-place">
                      <i class="trav-location"></i>
                      <span class="place-name">510 LaGuardia Pl, Paris, France</span>
                    </div>
                  </div>
                </div>
                <div class="post-comment-layer">
                  <div class="post-comment-top-info">
                    <div class="comm-count-info">
                      5 Comments
                    </div>
                    <div class="comm-count-info">
                      3 / 20
                    </div>
                  </div>
                  <div class="post-comment-wrapper">
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-smile.png" alt="">
                            <span>21</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <div class="post-comment-row happen-event">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Amine</a>
                          <a href="#" class="comment-nickname">@ak0117</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-like.png" alt="">
                            <span>19</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <div class="post-comment-row">
                      <div class="post-com-avatar-wrap">
                        <img src="http://placehold.it/45x45" alt="">
                      </div>
                      <div class="post-comment-text">
                        <div class="post-com-name-layer">
                          <a href="#" class="comment-name">Katherin</a>
                          <a href="#" class="comment-nickname">@katherin</a>
                        </div>
                        <div class="comment-txt">
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex doloribus labore tenetur vel. Neque molestiae repellat culpa qui odit delectus.</p>
                        </div>
                        <div class="comment-bottom-info">
                          <div class="com-reaction">
                            <img src="./assets/image/icon-smile.png" alt="">
                            <span>15</span>
                          </div>
                          <div class="com-time">6 hours ago</div>
                        </div>
                      </div>
                    </div>
                    <a href="#" class="load-more-link">Load more...</a>
                  </div>
                </div>
              </div>
              <div class="post-add-comment-block">
                <div class="avatar-wrap">
                  <img src="http://placehold.it/45x45" alt="">
                </div>
                <div class="post-add-com-input">
                  <input type="text" placeholder="Write a comment">
                </div>
              </div>
            </div>
          </div>
        </div>`
      }],
      addClass: 'main-gallery-block with-header',
      pager: false,
      hideControlOnEnd: true,
      loop: false,
      slideEndAnimatoin: false,
      thumbnail:true,
      toogleThumb: false,
      thumbHeight: 100,
      thumbMargin: 20,
      thumbContHeight: 160,
      actualSize: false,
      zoom: false,
      autoplayControls: false,
      fullScreen: false,
      download: false,
      counter: false,
      mousewheel: false,
      appendSubHtmlTo: 'lg-item',
      prevHtml: '<i class="trav-angle-left"></i>',
      nextHtml: '<i class="trav-angle-right"></i>',
      hideBarsDelay: 100000000
    });
    let headerOfGallery =
      `<div class="modal-custom-header">
        <div class="modal-header_avatar-info">
          <div class="head-ava">
            <img src="http://placehold.it/50x50" alt="Header avatar">
          </div>
          <div class="head-txt">
            <div class="head-name">Justin Baker</div>
            <div class="head-sub-txt">Photos <span>987</span></div>
          </div>
        </div>
        <div class="modal-header_close">
        </div>
      </div>`;

    $lg.on('onAfterOpen.lg',function(){
      $('.lg-backdrop').css('background','black');
      $('body').css('overflow','hidden');
      let itemArr = [], thumbArr = [];
      let galleryBlock = $('.main-gallery-block');
      if(!$('.modal-custom-header').length){
        $(galleryBlock).find('.lg').prepend(headerOfGallery);
      }
      let galleryItem = $(galleryBlock).find('.lg-item');
      let galleryThumb = $(galleryBlock).find('.lg-thumb-item');
      $.each(galleryItem, function(i, val){
        // itemArr.push(val);
      });
      $.each(galleryThumb, function(i, val){
        // thumbArr.push(val);
        // let startCnt = `<div class="thumb-txt"><i class="trav-flag-icon"></i> start</div>`;
        // let startCntEmpty = `<div class="thumb-txt">&nbsp;</div>`;
        let cityName = 'new york city'
        let cityThumb = `<div class="thumb-txt">${cityName}</div>`;
        // if(i == 0){
        //   $(val).addClass('place-thumb');
        //   $(val).append(placeName).prepend(startCnt);
        // }
        if(i == 4 || i == 5){
          $(val).addClass('city-thumb');
          $(val).append(cityThumb);
        }
      });
    });
    $lg.on('onBeforeClose.lg',function(){
      $('body').removeAttr('style');
    });
    let setWidth = function(){
      let mainBlock = $('.main-gallery-block');
      let subTtlWrp = $(mainBlock).find('.lg-current .cover-block');
      let subTtl = $(mainBlock).find('.lg-current .cover-block-inner');

      let slide = $('.main-gallery-block .lg-item');
      let currentItem = $('.main-gallery-block .lg-current');
      let currentImgWrap = $('.main-gallery-block .lg-current .lg-img-wrap');
      let currentImg = $('.main-gallery-block .lg-current .lg-image');
      let currentCommentIs = $(subTtl).hasClass('comment-block');
      let currentImgPos = $(currentImg).position().top;
      setTimeout(function(){
        let commentWidth = $('.main-gallery-block .lg-current .gallery-comment-wrap').width();
        let currentWidth = $(mainBlock).find('.lg-current .lg-object').width();
        if(currentCommentIs){
          // console.log('yes');
          $(currentImgWrap).css('padding-right', commentWidth);
          $(subTtl).css('width', currentWidth + commentWidth);
        } else{
          $(currentImgWrap).removeAttr('style');
          $(subTtl).css('width', currentWidth);
        }
        $(subTtlWrp).show();
        $('.mCustomScrollbar').mCustomScrollbar();
      }, 500);
    }

    $lg.on('onSlideItemLoad.lg',function(e){

      setWidth();
      $(window).on('resize', function(){
        setWidth();
      })
    });
    $lg.on('onAfterSlide.lg',function(){
      setWidth();
    });
  });

  // country gallery popup


    // visa gallery popup
  $('#visaPopupTrigger').on('click', function(){

    let $lg = $(this).lightGallery({
      dynamic: true,
      dynamicEl: [{
        "src": './assets/image/visa-slide-image.jpg',
        'thumb': './assets/image/visa-slide-image.jpg'
      },{
        "src": './assets/image/visa-slide-image.jpg',
        'thumb': './assets/image/visa-slide-image.jpg'
      }],
      addClass: 'main-gallery-block',
      pager: false,
      hideControlOnEnd: true,
      loop: false,
      slideEndAnimatoin : false,
      thumbnail:true,
      toogleThumb: false,
      thumbHeight: 100,
      thumbMargin: 20,
      thumbContHeight: 180,
      actualSize: false,
      zoom: false,
      autoplayControls: false,
      fullScreen: false,
      download: false,
      counter: false,
      mousewheel: false,
      appendSubHtmlTo: 'lg-item',
      prevHtml: '<i class="trav-angle-left"></i>',
      nextHtml: '<i class="trav-angle-right"></i>',
      hideBarsDelay: 100000000
    });
  });

  // $('.side-trip-tab .trip-tab-block').on('click', function(e){
  //   $('.side-trip-tab .trip-tab-block').removeClass('current-tab');
  //   $(this).addClass('current-tab');
  // });

  $('.post-top-map-tabs .map-tab').on('click', function(){
    let tabWrap = $(this).parents('.post-top-map-tabs');
    let mapAreaItem = $(this).parents('.post-map-block').find('.area-txt');
    let getTxtName = $(this).data('tab');

    $(tabWrap).find('.map-tab').removeClass('current');
    $(this).addClass('current');

    $(mapAreaItem).hide();
    $('#'+getTxtName).show();
  });

  // uinoslider

  let uiSliderFn = function($id, $curCountId, $totalId, $totalCount, $startCount, $divider){

    var softSlider1 = document.getElementById($id),
        paddingMin = document.getElementById($curCountId),
        paddingMax = document.getElementById($totalId);

    var totalCount = $totalCount;
    var divider = $divider;

    noUiSlider.create(softSlider1, {
      start: $startCount,
      connect: true,
      disabled: true,
      step: 10,
      range: {
        min: 0,
        max: $totalCount
      }
    });

    softSlider1.noUiSlider.on('update', function ( values, handle, clearVal ) {
      var count = values[handle]/divider;
      if(divider != 0){
        if ( handle ) {
          paddingMax.innerHTML = count;
        } else {
          paddingMax.innerHTML = $totalCount;
          paddingMin.innerHTML = count;
        }
      } else{
        if ( handle ) {
          paddingMax.innerHTML = clearVal;
        } else {
          paddingMax.innerHTML = $totalCount;
          paddingMin.innerHTML = clearVal;
        }
      }
    });
  };



  if($("#sliderPollution, #costOfLiving, #crimeRate, #qualityOfLife, #sliderPollution1, #costOfLiving1, #crimeRate1, #qualityOfLife1").length){
    uiSliderFn('sliderPollution', 'current', 'total', parseInt($('#total').text()), parseInt($('#current').text()), 1);
    uiSliderFn('costOfLiving', 'currentCost', 'totalCost', parseInt($('#totalCost').text()), parseInt($('#currentCost').text()), 1);
    uiSliderFn('crimeRate', 'currentRate', 'totalRate', parseInt($('#totalRate').text()), parseInt($('#currentRate').text()), 1);
    uiSliderFn('qualityOfLife', 'currentQuolity', 'totalQuolity', parseInt($('#totalQuolity').text()), parseInt($('#currentQuolity').text()), 1);

  }

  // if($("#sliderComments, #sliderLikes, #sliderTrips, #sliderReviews, #sliderVideos, #sliderPhotos").length){
  //   uiSliderFn('sliderComments', 'currentComments', 'totalComments', 500, 163, 0);
  //   uiSliderFn('sliderLikes', 'currentLikes', 'totalLikes', 200, 120, 0);
  //   uiSliderFn('sliderTrips', 'currentTrips', 'totalTrips', 200, 120, 0);
  //   uiSliderFn('sliderReviews', 'currentReviews', 'totalReviews', 200, 120, 0);
  //   uiSliderFn('sliderVideos', 'currentVideos', 'totalVideos', 200, 120, 0);
  //   uiSliderFn('sliderPhotos', 'currentPhotos', 'totalPhotos', 200, 120, 0);
  // }

  // location search layer

  var iIsOpen = false;

  $('body').bind('click',function (e) {
    // e.stopPropagation();
    var $clicked = $(e.target);
    if(!($clicked.is('#locationDrop') || $clicked.is('.loc-search-block') || $clicked.parents().is('.loc-search-block'))){
      if (iIsOpen == true) {
        $('.location-drop-wrap').removeClass('show-result');
        $('#locSearchInput').val('');
        iIsOpen = false;
      }
    }
  });

  $('#locationDrop').click(function (e) {
    e.preventDefault();
    $('.location-drop-wrap').addClass('show-result');
    $('#locSearchInput').focus();
    iIsOpen = true;
  });

  $('[data-toggle=modal]').on('click',function(e){
    e.preventDefault();
    let dataTarget = $(this).attr('data-target');
    $(dataTarget).modal();
  });

  $('[data-toggle=collapse]').on('click',function(e){
    e.preventDefault();
    let dataTarget = $(this).attr('data-target');
    $(dataTarget).collapse('toggle');
  });



   $('#flight-popup-carousel').owlCarousel({
  // // $('.hotel-popup-carousel').owlCarousel({
     margin:40,
     center: true,
     autoWidth:true,
     loop: true,
     nav: true,
     dots: false,
     navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>']
   });

   $('#hotel-popup-carousel').owlCarousel({
  // // $('.hotel-popup-carousel').owlCarousel({
     margin:40,
     center: true,
     autoWidth:true,
     loop: true,
     nav: true,
     dots: false,
     navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>']
   });
   // hotel popup

  // question popup
  // $('.question-photo-slider').owlCarousel({
  //   margin:0,
  //   // center: true,
  //   item: 1,
  //   autoWidth:true,
  //   loop: true,
  //   nav: true,
  //   // dots: false,
  //   navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>']
  // });



});


function strip(html)
{
   var tmp = document.createElement("DIV");
   tmp.innerHTML = html;
   return tmp.textContent || tmp.innerText || "";
}


$(document).ready(function() {
	var showChar = 200;
	var ellipsestext = "...";
	var moretext = "see more";
	var lesstext = "see less";
	$('.more').each(function() {
		var content = $(this).html();

		if(strip(content).length > showChar) {

			var c = content.substr(0, showChar);
			var h = content.substr(showChar-1, content.length - showChar);

			var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

			$(this).html(html);
		}

	});

	// $(".morelink").click(function(){
	// 	if($(this).hasClass("less")) {
	// 		$(this).removeClass("less");
	// 		$(this).html(moretext);
	// 	} else {
	// 		$(this).addClass("less");
	// 		$(this).html(lesstext);
	// 	}
	// 	$(this).parent().prev().toggle();
	// 	$(this).prev().toggle();
	// 	return false;
	// });
});

//Comment filter function
    function commentSort(type, obj, reload_obj){
        if($(obj).hasClass('active'))
            return;

        $(obj).parent().find('li').removeClass('active');
        $(obj).addClass('active');
        var list, i, switching, shouldSwitch, switchcount = 0;
        switching = true;
        parent_obj = $(obj).closest('.post-comment-layer').find('.post-comment-wrapper');
        while (switching) {
            switching = false;
            list = parent_obj.find('>div');
            shouldSwitch = false;
            for (i = 0; i < list.length - 1; i++) {
                shouldSwitch = false;
                if(type == "Top")
                {
                    if ($(list[i]).attr('topsort') < $(list[i + 1]).attr('topsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else if(type == "New")
                {
                    if ($(list[i]).attr('newsort') < $(list[i + 1]).attr('newsort')) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                $(list[i]).before(list[i + 1]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && list.length > 1 && i == 0) {
                    switching = true;
                }
            }
        }

        Comment_Show.reload(reload_obj);
    }

//Comment load more function
var Comment_Show = (()=>{
    var total_rows = 0;
    var display_unit = 3;
    function init(obj){
        var target = $(obj).find('.sortBody');
        if(target.attr('travooo-comment-rows'))
        {
            return;
        }
        else
        {
           if(target.find(">div").hasClass('comment-not-fund-info')){
               return;
           }else{
            target.find(">div").hide();
            get_totalrows(target, true)
        }
        }
    }

    function reload(obj)
    {
        var target = $(obj).find('.sortBody');
        target.find(">div").hide();
        target.find(">div").removeClass('displayed');

        var check_count = (target.find(">div").length == display_unit+1) ? true : false;

        get_totalrows(target, check_count);
    }

    function get_totalrows(obj, check_type)
    {
        this.total_rows = obj.find(">div").length;

        if(this.total_rows>display_unit && check_type && obj.parent().find('a.load-more-comment-link').length == 0){
            var btn = $('<a href="javascript:;"/>').addClass('load-more-comment-link').text('Load more...').click(function(){
               Comment_Show.inc(obj)
           });
            obj.after(btn);
       }

        obj.attr('travooo-comment-rows', this.total_rows);
        if(!obj.attr('travooo-current-page'))
            obj.attr('travooo-current-page', 1);
        show_rows(obj, obj.attr('travooo-current-page'));
    }

    function show_rows(obj, page)
    {
        this.total_rows = parseInt(obj.attr('travooo-comment-rows'));
        var showing_rows = parseInt(page) * display_unit;
        showing_rows = this.total_rows > showing_rows ? showing_rows : this.total_rows;
        for(var i = 0; i < showing_rows; i++)
        {
            obj.find(">div").eq(i).show().addClass("displayed");
        }

        var disnum = obj.find(".displayed").length;
        obj.parent().find('.comm-count-info').find('strong').html( disnum );
    }

    function increase_show(obj)
    {
        var current_page = parseInt(obj.attr('travooo-current-page'));
        obj.attr('travooo-current-page', (current_page + 1));
        get_totalrows(obj, false);
    }


    return  {
        init: init,
        inc: increase_show,
        reload: reload
    }
})();


function commentMediaUploadFile(_this, form_obj, id) {

        if ((form_obj.find('.medias').children().length + _this[0].files.length) > 10)
        {
            alert("The maximum number of files to upload is 10.");
            _this.val("");
            return;
        }
        if (window.File && window.FileReader && window.FileList && window.Blob) { //check File API supported browser

            var data = _this[0].files;
            $.each(data, function (index, file) { //loop though each file

                var upload = new Upload(file);
                var uid = Math.floor(Math.random() * 1000000);
                if (/(\.|\/)(gif|jpe?g|png|webp)$/i.test(file.type)) {
                  if(file.size > 10485760){
                    form_obj.find('.rep-media-error-content').removeClass('d-none')
                    form_obj.find('.rep-media-error-content').html('The maximum size of image 10MB.')
                    return false;
                  }else{
                    form_obj.find('.rep-media-error-content').addClass('d-none')
                    form_obj.find('.rep-media-error-content').html('')
                  }


                    var fRead = new FileReader();
                    fRead.onload = (function (file) {
                        return function (e) {

                            var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element

                            var button = $('<span/>').addClass('close').addClass('removeFile').click(function () {     //create close button element
                                var filename = upload.getName();
                                commentFileDelete(filename, this, form_obj.find('*[data-id="pair'+id+'"]').val());
                            }).append('<span>&times;</span>');

                            var imgitem = $('<div>').attr('uid', uid).append(img); //create Image Wrapper element
                            imgitem = $('<div/>').addClass('img-wrap-newsfeed').append(imgitem).append(button);
                            form_obj.find('.medias').append(imgitem);
                            upload.doUpload(uid, form_obj.find('*[data-id="pair'+id+'"]').val());

                        };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.

                }else{
                  form_obj.find('.rep-media-error-content').removeClass('d-none')
                  form_obj.find('.rep-media-error-content').html('You can upload only image file.')
                }
            });
            form_obj.find('textarea').focus();
            _this.val("");

        } else {
            alert("Your browser doesn't support File API!"); //if File API is absent
        }
    }



$('body').on('click', '.morecommentlink', function () {
    var moretext = "more";
    var lesstext = "less";
    if($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
    } else {
        $(this).addClass("less");
        $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
});

// show mmore function
var visibleY = function(el){
  var rect = el.getBoundingClientRect(), top = rect.top, height = rect.height,
    el = el.parentNode;
  do {
    rect = el.getBoundingClientRect();
    if (top <= rect.bottom === false) return false;
    if ((top + height) <= rect.top) return false
    el = el.parentNode;
  } while (el != document.body);
  return top <= document.documentElement.clientHeight;
};

$('body').on('click', '.button_follow_from_post_likes', function() {
    var followBtn = $(this);
    var followClass = 'btn-light-grey';
    var unfollowClass = 'btn-light-primary';

    if (followBtn.hasClass('js-followed')) {
        var action = "/unfollow";
    } else {
        action = "/follow";
    }

    $.ajax({
        method: "POST",
        url: baseUrl + "/profile/" + followBtn.attr('data-id') + action,
        data: {name: "test"}
    }).done(function (response) {
        if (response.success === true) {
            if (action === "/follow") {
                followBtn.html(profileUnfollowText);
                followBtn.addClass(followClass);
                followBtn.removeClass(unfollowClass);
                followBtn.addClass('js-followed')
            } else {
                followBtn.html(profileFollowText);
                followBtn.removeClass(followClass);
                followBtn.addClass(unfollowClass);
                followBtn.removeClass('js-followed')
            }
        }
    });
});


function getDestinationSearch(query, append_body, parrent_class, item_class, type = '') {
  num_location_result = 0;
  $(parrent_class).find('.dest-search-result-block').addClass('open')
  if (query.length >= 3) {
    $(parrent_class).find('.dest-search-result-block').html('<ul class="location-tab-list"><li class="active" data-tab_item="all">All <b class="s-result-all-count">0</b></li><li class="" data-tab_item="place">Places <b class="s-result-place-count">0</b></li><li class="" data-tab_item="city">Cities <b class="s-result-cities-count">0</b></li><li class="" data-tab_item="country">Countries <b class="s-result-countries-count">0</b></li></ul>'+
        '<ul class="select-result-options" id="destination_search_result"></ul>');

    $.ajax({
      method: "GET",
      url: "/home/searchAllPOIs",
      data: {
        q: query, lat: window.locationLat, lng: window.locationLng
      }
    })
        .done(function (res) {
          res = JSON.parse(res);

          if(res.length > 0 && res[0] != '[]') {
            num_location_result = num_location_result + res.length;
            $('.s-result-all-count').html(num_location_result)
            $('.s-result-place-count').html(res.length)

            res.forEach(function (element) {

              if (!element.image) {
                element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
              } else {
                element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.image;
              }

              if (type == 'map') {
                var id = element.id;
              } else {
                var id = element.id + ',place';
              }

              $('#destination_search_result').append('<li class="search-results__item place ' + item_class + '"  data-select-id="' + id + '" data-select-text="' + element.title + '" data-lat="' + element.lat + '" data-lng="' + element.lng + '" data-zoom="12">' +
                  '<div class="search-country-block" dir="auto"><div class="search-country-imag" dir="auto">' +
                  '<img src="' + element.image + '" dir="auto">' +
                  '</div><div class="span10" dir="auto">' +
                  '<div class="search-country-info" dir="auto">' +
                  '<div class="search-country-text" dir="auto">' +
                  markLocetionMatch(element.title, query) + '</div><div class="search-country-type">Place</div></div></div></div></li>');

            });
          }
        });

    // start searching countries
    $.ajax({
      method: "GET",
      url: "/home/searchCountries",
      data: {q: query, lat: window.locationLat, lng: window.locationLng}
    })
        .done(function (res) {
          res = JSON.parse(res);
          num_location_result = num_location_result + res.length;

          $('.s-result-all-count').html(num_location_result)
          $('.s-result-countries-count').html(res.length)

          res.forEach(function (element) {
            if (typeof element.get_medias[0] === 'undefined' || typeof element.get_medias[0].url === 'undefined' || ! element.get_medias[0].url) {
              element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
            } else {
              element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.get_medias[0].url;
            }

            if (type == 'map') {
              var id = element.lat + '-:' + element.lng + '-:' + element.transsingle.title + '-:5-:' + element.transsingle.title +'-:'+ element.iso_code + '-:' + (element.transsingle.population != null?element.transsingle.population.replace(/,/g, "."):'') + '-:' + element.id;
            } else {
              var id = element.id + ',country';
            }

            $('#destination_search_result').append('<li class="search-results__item  country '+ item_class +'"  data-select-id="'+ id +'" data-select-text="'+ element.transsingle.title +'" data-lat="'+ element.lat +'" data-lng="'+ element.lng +'" data-zoom="5">'+
                '<div class="search-country-block" dir="auto"><div class="search-country-imag" dir="auto">'+
                '<img src="'+ element.image +'" dir="auto">' +
                '</div><div class="span10" dir="auto">' +
                '<div class="search-country-info" dir="auto">' +
                '<div class="search-country-text" dir="auto">' +
                markLocetionMatch(element.transsingle.title, query) + '</div><div class="search-country-type">Country in ' + element.region.transsingle.title + '</div></div></div></div></li>');
          });
        });
    // start searching cities
    $.ajax({
      method: "GET",
      url: "/home/searchCities",
      data: {q: query, lat: window.locationLat, lng: window.locationLng}
    })
        .done(function (res) {
          res = JSON.parse(res);
          num_location_result = num_location_result + res.length;

          $('.s-result-all-count').html(num_location_result)
          $('.s-result-cities-count').html(res.length)

          res.forEach(function (element) {
            //console.log(element);
            if (typeof element.get_medias[0] === 'undefined' || typeof element.get_medias[0].url === 'undefined' || ! element.get_medias[0].url) {
              element.image = "https://www.travooo.com/assets2/image/placeholders/place.png"
            } else {
              element.image = 'https://s3.amazonaws.com/travooo-images2/' + element.get_medias[0].url;
            }

            if (type == 'map') {
              var id = element.lat + '-:' + element.lng + '-:' + element.transsingle.title + '-:8-:' + element.transsingle.title +'-:'+ element.country.iso_code + '-:' + (element.transsingle.population != null?element.transsingle.population.replace(/,/g, "."):'') + '-:' + element.country.id;
            } else {
              var id = element.id + ',city';
            }

            $('#destination_search_result').append('<li class="search-results__item city '+ item_class +'"  data-select-id="'+ id +'" data-select-text="'+ element.transsingle.title +'" data-lat="'+ element.lat +'" data-lng="'+ element.lng +'" data-zoom="8">'+
                '<div class="search-country-block" dir="auto"><div class="search-country-imag" dir="auto">'+
                '<img src="'+ element.image +'" dir="auto">' +
                '</div><div class="span10" dir="auto">' +
                '<div class="search-country-info" dir="auto">' +
                '<div class="search-country-text" dir="auto">' +
                markLocetionMatch(element.transsingle.title, query) + '</div><div class="search-country-type">City in ' + element.country.transsingle.title + '</div></div></div></div></li>');
          });
        });
  }else{
    $(parrent_class).find('.dest-search-result-block').html('<span class="dest-empty-char">Please enter '+ (3 - query.length) +' or more characters</span>')
  }
}

$(document).on('click', '.location-tab-list li', function(){
  var type = $(this).attr('data-tab_item')

  $('.location-tab-list li').removeClass('active')
  $(this).addClass('active')

  if(type != 'all'){
    $('ul.select-result-options li').addClass('d-none')
    $('ul.select-result-options li.' + type).removeClass('d-none')
  }else{
    $('ul.select-result-options li').removeClass('d-none')
  }

})
