$(document).ready(function () {
    $.fn.extend({
        placeCursorAtEnd: function () {
            // Places the cursor at the end of a contenteditable container (should also work for textarea / input)
            if (this.length === 0) {
                throw new Error("Cannot manipulate an element if there is no element!");
            }
            var el = this[0];
            var range = document.createRange();
            var sel = window.getSelection();
            var childLength = el.childNodes.length;
            if (childLength > 0) {
                var lastNode = el.childNodes[childLength - 1];
                var lastNodeChildren = lastNode.childNodes.length;
                range.setStart(lastNode, lastNodeChildren);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
            return this;
        }
    });
});

function TAGGING_APP(id, containerSelector, inputSelector, summernoteSelector,  options) {

    var defaults = {
        tooltipContainerEl: undefined,
        tooltipTop: 0
    };

    this.options = Object.assign({}, defaults, options);

    this.id = id;

    this.containerSelector = containerSelector;
    this.containerEl = $(this.containerSelector).get(0);
    if( ! this.containerEl )
    {
        console.warn('TAGGING_APP: container element not found');
        return;
    }

    this.inputSelector = inputSelector;
    this.inputEl = $(this.containerEl).find(this.inputSelector).get(0);
    if( ! this.inputEl )
    {
        console.warn('TAGGING_APP: input element not found');
        return;
    }

    this.summernoteSelector = summernoteSelector;
    this.summernoteEl = $(this.containerEl).find(this.summernoteSelector).get(0);
    if( ! this.summernoteEl )
    {
        console.warn('TAGGING_APP: summernote element not found');
        return;
    }

    this.$resultContainerEl = undefined;

    this.init();
};
TAGGING_APP.prototype = {
    init: function() {
        var self = this;

        this.$resultContainerEl = this.buildResultListContainer();

        $(this.options.tooltipContainerEl).append(this.$resultContainerEl);

        var timeoutId;
        $(this.containerEl).on("keyup", this.inputSelector, function(e) {
            if(
                // e.which == 32 || // Press space
                // e.which == 8 ||// Press backspace
                e.which == 27 //Press esc
            )
            {
                self.$resultContainerEl.hide();
                return;
            }

            // do nothing when CTRL key keyup event fires
            if(e.which == 16 || // Shift
                e.which == 17)  // Ctrl
                return;

            var el = this;
            var tagWord = self.getTagWordFromEl(this);

            if( ! tagWord){
                self.$resultContainerEl.hide();
                return;
            }

            self.clearAllTabs();

            if( timeoutId ){
                clearTimeout(timeoutId);
                timeoutId = null;
            }


            timeoutId = setTimeout(function(){ // to prevent ajax request on each keyup
                var currentTagWord = self.getTagWordFromEl(el);

                if(tagWord != currentTagWord){
                    return;
                }

                self.clearResultsForQueryString();

                var lat = window.locationLat;
                var lng =  window.locationLng;

                // var lat = 52.52; // lat Berlin
                // var lng = 13.40; // lng Berlin

                // start searching users
                $.ajax({
                    method: "GET",
                    url: "/home/search_pois_for_tagging",//"{{ route('home.search_pois_for_tagging') }}",
                    data: {
                        query: tagWord,
                        lat: lat,
                        lng: lng
                    }
                })
                    .done(function (res) {
                        // if nothing found
                        if( res.data.places.length <= 0 && res.data.users.length <= 0){
                            self.$resultContainerEl.hide();
                            return;
                        }

                        var currentTagWord = self.getTagWordFromEl(el);

                        if($.trim(res.meta.query) != $.trim(currentTagWord))
                            return;

                        // to prevent doubles
                        self.clearResultsForQueryString();

                        var pois = res.data.places;
                        pois.forEach(function(entry) {
                            self.$resultContainerEl
                                .find('#lc-places-'+self.id)
                                .append(self.buildTooltipPlaceItem(entry, res.meta.query));

                            if ( ! entry.image){
                                // get POI image
                                $.ajax({
                                    method: "GET",
                                    url: "/places/ajax_get_poi_media",
                                    data: {
                                        place_id: entry.id
                                    }
                                })
                                    .done(function (res) {
                                        if(res.length > 0) {
                                            self.$resultContainerEl.find('.place-image-'+entry.id).attr('src', "https://s3.amazonaws.com/travooo-images2/"+res)
                                        }
                                    });
                            }
                        });
                        self.$resultContainerEl.find('.num-results-places').html(pois.length);

                        var users = res.data.users;
                        users.forEach(function(user) {
                            self.$resultContainerEl
                                .find('#lc-people-'+self.id)
                                .append(self.buildTooltipUserItem(user, res.meta.query));
                        });
                        self.$resultContainerEl.find('.num-results-people').html(users.length);

                        self.setResultsForQueryString(res.meta.query);

                        self.$resultContainerEl
                            .show();
                    });
            }, 250);
        });

        // hide when click outside
        $(document).click(function(event) {
            $target = $(event.target);
            if( ! $target.closest(self.$resultContainerEl).length &&
                $(self.$resultContainerEl).is(":visible")) {
                $(self.$resultContainerEl).hide();
            }
        });

        $(this.$resultContainerEl).on('click', '.tagging-result-item', function(e){
            e.stopPropagation();
            var place = $(this).data('place');
            var user = $(this).data('user');
            var tag = $(this).data('tag');

            var href = '#';
            var anchor = '';
            if(place){
                href = 'https://www.travooo.com/place/'+place.id;
                anchor = place.title;
            }

            if(user){
                href = 'https://www.travooo.com/profile/'+user.id;
                anchor = user.name;
            }

            var $summernote = $(self.containerEl).find(self.summernoteSelector);

            var contents_temp = $(self.containerEl).find(self.inputSelector).html();
            contents_temp = contents_temp.replace('@'+tag, '<a href="'+href+'">'+anchor+'</a>');

            $summernote.summernote('code', contents_temp);
            $(self.containerEl).find('[contenteditable]').placeCursorAtEnd();
            self.$resultContainerEl.hide();
        });
    },

    buildTooltipUserItem: function(user, tag) {
        if ( ! user.profile_picture) {
            if (user.gender == 0 || user.gender == 1) {
                user.profile_picture = "https://www.travooo.com/assets2/image/placeholders/male.png";
            } else {
                user.profile_picture = "https://www.travooo.com/assets2/image/placeholders/female.png";
            }
        }

        var $html = $("<div class=\"tagging-result-item drop-row\" style=\"cursor:pointer;\">" +
            "<div style=\"flex: 0.2;\" class=\"img-wrap rounded place-img\" dir=\"auto\">" +
            "<img class=\"img-responsive\" src=\""+user.profile_picture+"\" dir=\"auto\">" +
            "</div>" +
            "<div style=\"flex: 0.8;\" class=\"drop-content place-content\" dir=\"auto\">" +
            "<h3 class=\"content-ttl\" dir=\"auto\" style=\"font-size:12px\">"+user.name+"</h3>" +
            "<p class=\"place-name\" dir=\"auto\">" +
            "<span class=\"follow-tag\" dir=\"auto\" style=\"font-size:10px\">"+user.display_name+"</span>" +
            "</p>" +
            "</div>" +
            "</div>");

        $html.data('user', user);
        $html.data('tag', tag);

        return $html;
    },

    buildTooltipPlaceItem: function(data, tag){

        var imgUrl = 'https://www.travooo.com/assets2/image/placeholders/place.png';
        if( data.image ){
            imgUrl = 'https://s3.amazonaws.com/travooo-images2/'+data.image;
        }


        var $html = $("<div class=\"tagging-result-item drop-row\" style=\"cursor:pointer;\">" +
            "<div style=\"flex: 0.2;\" class=\"img-wrap rounded place-img\" dir=\"auto\">" +
            "<img class=\"place-image-"+data.id+" img-responsive\" src=\""+imgUrl+"\" dir=\"auto\">" +
            "</div>" +
            "<div style=\"flex: 0.8;\" class=\"drop-content place-content\" dir=\"auto\">" +
            "<h3 class=\"content-ttl\" dir=\"auto\" style=\"font-size:12px\">"+data.title+"</h3>" +
            "<p class=\"place-name\" dir=\"auto\">" +
            "<span class=\"follow-tag\" dir=\"auto\" style=\"font-size:10px\">"+data.place_type+"</span>" +
            "<span dir=\"auto\" style=\"font-size:12px\">"+data.address+"</span>" +
            "</p>" +
            "</div>" +
            "</div>");

        $html.data('place', data);
        $html.data('tag', tag);

        return $html;
    },

    buildResultListContainer: function(){

        var $html = $("" +
            "<div class=\"search-box tag-tooltip\" style=\"display:none;position: absolute;top:"+this.options.tooltipTop+"px;z-index: 1000;background-color: white;width: 400px;max-height: 300px;overflow: auto;-webkit-box-shadow: 0px 0px 26px 0px rgba(0,0,0,0.27);-moz-box-shadow: 0px 0px 26px 0px rgba(0,0,0,0.27);box-shadow: 0px 0px 26px 0px rgba(0,0,0,0.27);border-radius: 3px;\">" +
            "<div style=\"font-style: italic; padding: 5px 0 0 5px\" class='results-for-container'><i>Results for</i>&nbsp;<b class='results-for-query'>query</b></div>" +
            "  <ul class=\"nav nav-tabs search-tabs\" role=\"tablist\">" +
            "    <li class=\"active nav-item\" role=\"presentation\">" +
            "           <a class=\"nav-link active\" href=\"#lc-places-"+this.id+"\" aria-controls=\"lc-places-"+this.id+"\" role=\"tab\" data-toggle=\"tab\">Places <span class=\"num-results-places\"></span></a>" +
            "    </li>" +
            "    <li class=\"nav-item\" role=\"presentation\">" +
            "           <a class=\"nav-link \" href=\"#lc-people-"+this.id+"\" aria-controls=\"lc-people-"+this.id+"\" role=\"tab\" data-toggle=\"tab\">People <span class=\"num-results-people\"></span></a>" +
            "    </li>" +
            "  </ul>" +

            "  <div class=\"tab-content\">\n" +
            "        <div role=\"tabpanel\" class=\"tab-pane drop-wrap active\" id=\"lc-places-"+this.id+"\"></div>\n" +
            "        <div role=\"tabpanel\" class=\"tab-pane drop-wrap\" id=\"lc-people-"+this.id+"\"></div>\n" +
            "  </div>" +

            "</div>" +
            "");
        return $html;
    },

    getTagWordFromEl: function(el){
        var text = $(el).text();

        var pattern = /\B@[\s\p{L}\d_-]+/ugi;
        var match = text.match(pattern);

        if( ! match )
            return null;

        var lastMatch = match.pop();
        var tagWord = lastMatch.replace("@", "");

        return tagWord;
    },

    setResultsForQueryString: function(val){
        this.$resultContainerEl.find('.results-for-query').text(val);
    },

    clearAllTabs: function(){
        this.clearPlaceTab();
        this.clearPeopleTab();
    },

    clearPlaceTab: function(){
        this.$resultContainerEl.find('#lc-places-'+this.id).empty();

        this.$resultContainerEl.find('.num-results-places').html('');
    },

    clearPeopleTab: function(){
        this.$resultContainerEl.find('#lc-people-'+this.id).empty();

        this.$resultContainerEl.find('.num-results-people').html('');
    },

    clearResultsForQueryString: function(){
        this.$resultContainerEl.find('.results-for-query').html('');
    },
}; // end TAGGING_APP.prototype

// Count digits
function digits_count(n) {
    var count = 0;
    if (n >= 1) ++count;

    while (n / 10 >= 1) {
        n /= 10;
        ++count;
    }

    return count;
}

// Format date to hours:minutes AM/PM
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function formatDate(val, type){
    if(val){
        // Change month and day positions
        let spl = val.split('/');
        let tmp = spl[0];
        spl[0] = spl[1];
        spl[1] = tmp;
        val = spl.join('/');

        let date = new Date(val+' UTC');
        let day = (digits_count(date.getDate()) == 1) ? "0"+date.getDate(): date.getDate();
        let monthIndex = (digits_count(date.getMonth())) ? "0"+(date.getMonth() + 1): date.getMonth() + 1;
        let year = date.getFullYear();
        let hours_minutes_ampm = formatAMPM(date);
        if(type == 'date'){
            return day+"/"+monthIndex+"/"+year+" "+hours_minutes_ampm;
        }else if(type == 'time'){
            return hours_minutes_ampm;
        }



    }
}

// Function that every 100 miliseconds checks if element with class name exists
function waitForElement(class_name, callback){
    var poops = setInterval(function(){
        if(document.getElementsByClassName(class_name).length){
            clearInterval(poops);
            callback();
        }
    }, 100);
}

// Get week name from date
function getWeekName(date){
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var d = new Date(date);
    return days[d.getDay()];
}

// Get month name (short example Apr)
function getMonthName(date){
    return new Date(date).toLocaleString('default', { month: 'short' });
}

// Change month and day positions
function changeMonthDayPositions(date){
    let spl = date.split('/');
    let tmp = spl[0];
    spl[0] = spl[1];
    spl[1] = tmp;
    return spl.join('/');
}

// Count the time since today left part
function timeSince(date) {

    date = changeMonthDayPositions(date);

    var seconds = Math.floor((new Date() - new Date(date)) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval >= 1) {
        return getMonthName(date) + " " + new Date(date).getDate() + ", " + new Date(date).getFullYear();
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return getMonthName(date) + " " + new Date(date).getDate();
    }
    interval = Math.floor(seconds / 86400);
    if (interval >= 1 && interval < 7) {
        return getWeekName(date);
    }else if(interval >= 7){
        return getMonthName(date) + " " + new Date(date).getDate();
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours ago";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes ago";
    }
    return Math.floor(seconds) + " seconds";
}
timeSinceMessage("16/07/2019 00:00:00");

// Count the time since today date divider
function timeSinceMessage(date) {
    date = changeMonthDayPositions(date);

    var seconds = Math.floor((new Date() - new Date(date)) / 1000);
    var bool = (new Date(date).toDateString() === new Date().toDateString());
    if(bool){
        return "Today "+formatAMPM(new Date(date));
    }

    var interval = Math.floor(seconds / 31536000);

    if (interval >= 1) {
        return getMonthName(date).toUpperCase() + new Date(date).getDate() + ", " + new Date(date).getFullYear() + ", " + formatAMPM(new Date(date));
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return getMonthName(date) + " " + new Date(date).getDate();
    }
    interval = Math.floor(seconds / 86400);
    if (interval >= 1 && interval < 7) {
        return getWeekName(date) + " " + formatAMPM(new Date(date));
    }else if(interval >= 7){
        return getMonthName(date).toUpperCase() + " " + new Date(date).getDate() + ", " + new Date(date).getFullYear() + ", " + formatAMPM(new Date(date));
    }
    return getWeekName(date) + " " + formatAMPM(new Date(date));
}


var lastConversationsShown = false;


function toggleLastMessages() {
    let container = $('#container-last-messages');

    if (lastConversationsShown === false) {

        container.empty();

        container.append('<p id="loading-last-messages" style="margin: 20px;">Loading...</p>');

        let loadingIndicator = $('#loading-last-messages');

        //$('#header-last-messages').fadeIn();

        var jqxhr = $.get("./../../../../../../../../../chat/last/10", function (data) {
            let conversations = data.data;

            if (conversations.length === 0) {
                loadingIndicator.html('No conversations found.');
            }
            else {
                loadingIndicator.hide();
                $.each(conversations, function (index, conversation) {
                    let fromName = conversation.participants_names;
                    let userId = conversation.user_id;
                    let participantNamesChunk = conversation.participant_names_chunk;
                    let participantNamesChunkBefore = conversation.participant_names_chunk_before;
                    let images = conversation.participant_images;
                    let messageText = conversation.conversation_title;
                    let messageTime = formatDate(conversation.last_updated_at, 'date');
                    let userThumb = conversation.user_thumb;
                    let url = conversation.conversation_url;
                    let fontWeight = 'normal';
                    let exp = '';
                    if(images){
                        exp = images.split(',');
                    }

                    if (conversation.has_unread_messages === true) {
                        fontWeight = 'bold';
                    }

                    let htmlCode = `<div class="conv-block" style="cursor: pointer;" onclick="window.location.href = '${url}';">`;
                    if(exp && exp.length > 1){
                        htmlCode += `<div class="img-wrap" ><div class="${exp.length >= 2 ? 'main-blk' : ''}" style="display: flex;flex-wrap: wrap;flex: 1 0 auto;width: 80px">`;
                if (exp.length == 2) {
                    htmlCode += `
                        <div class="avatar">
                            <img src="${exp[0]}">
                        </div> 
                        <div class="avatar">
                            <img src="${exp[1]}">
                        </div>    
                    `;
                } else  if (exp.length >= 3) {
                    htmlCode += `
                        <div class="avatar">
                            <img src="${exp[0]}">
                        </div> 
                        <div class="avatar">
                            <img src="${exp[1]}">
                        </div>
                        <div class="avatar">
                            <img src="${exp[2]}">
                        </div> 
                    `;
                }
                htmlCode += '</div></div>';

                    }else{
                        htmlCode += `
                            <div class="img-wrap">
                                <div style="width: 80px">
                                    <a href="/profile/${userId}">
                                        <img src="${userThumb}" alt="image" style='width:50px;height:50px;'>
                                    </a>
                                </div>
                            </div>
                        `;
                    }
                    let tooltip = '';
                    let unread_span = '';
                    if(fromName.split(',').length > 4){
                        tooltip = `data-toggle="tooltip" data-placement="bottom" data-animation="false" title="${participantNamesChunk}"`;
                    }
                    if(fromName.split(',').length > 4){
                        fromName = participantNamesChunkBefore;
                    }
                    if(conversation.unread_messages_count){
                        unread_span = `<span class="unread-count-${conversation.id}">(${conversation.unread_messages_count})</span>`;
                    }
                    htmlCode += `<div class="conv-txt">
                                    <div class="name">
                                    <a href="${url}" class="inner-link" ${tooltip}>${fromName} ${unread_span}</a>
                                    </div>
                                    <div class="msg-txt">
                                        <p style="font-weight: ${fontWeight};">${messageText}</p>
                                    </div>
                                </div>
                                <div class="conv-time">
                                    <span class="time">${messageTime}</span>
                                </div>
                            </div>`;

                    container.append(htmlCode);
                });

                $('[data-toggle="tooltip"]').tooltip();
            }
        }).fail(function () {
            loadingIndicator.html('Error loading.');
        });

        lastConversationsShown = true;
    }
    else {
        $('#header-last-messages').fadeOut();
        lastConversationsShown = false;
    }
}