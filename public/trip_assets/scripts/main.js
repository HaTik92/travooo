var swiperDays = new Swiper('.swiper-days', {
  slidesPerView: 3,
  spaceBetween: 10,
  slideToClickedSlide: true,
  navigation: {
    nextEl: '.swiper-days-control .swiper-next',
    prevEl: '.swiper-days-control .swiper-prev'
  }
});

var swiper = new Swiper('.swiper-media', {
  slidesPerView: 'auto',
  spaceBetween: 12,
  observer: true,
  observeParents: true,
  navigation: {
    nextEl: '.swiper-media .swiper-button-next',
    prevEl: '.swiper-media .swiper-button-prev'
  }
});


$(document).ready(function () {
  //$('.count').prop('disabled', true);
  $(document).on('click', '.plus', function () {
    var curentInput = $(this).closest('.counter').find('.count');
    var max = $(curentInput).attr('max');
    curentInput.val(parseInt(curentInput.val()) + 1);

    if (max && parseInt(curentInput.val()) > max) {
      curentInput.val(max);
    }

    curentInput.change();
  });
  $(document).on('click', '.minus', function () {
    var curentInput = $(this).closest('.counter').find('.count');
    var min = $(curentInput).attr('min');

    curentInput.val(parseInt(curentInput.val()) - 1);
    if (curentInput.val() == -1) {
      curentInput.val(0);
    }

    if (min && curentInput.val() < min) {
      curentInput.val(min);
    }

    curentInput.change();
  });
});

$(document).ready(function () {
  $('.upload-wrap input[type=file]').change(function () {
    var id = $(this).attr('id');
    var newimage = new FileReader();
    newimage.readAsDataURL(this.files[0]);
    newimage.onload = function (e) {
      $('.uploadpreview.' + id).css('background-image', 'url(' + e.target.result + ')');
    };
  });
});