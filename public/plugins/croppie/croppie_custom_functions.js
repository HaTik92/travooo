var $uploadCrop, cropping_url, rawCropImg, canvasImgUrl, inputFile, imageWidth;

function showCroppieModal(image_url, image_name, default_img, upload_text, url, loader, props) {
    const $croppieContainer = $('#upload-demo');
    let croppiePropsDefault = {
        viewport: {
            width: 207,
            height: 207,
            type: 'circle'
        },
        boundary: {width: "100%", height: "390"},
        maxZoomedCropWidth: 400,
        enforceBoundary: true,
        enableExif: false,
    };

    let croppieProps = Object.assign(croppiePropsDefault, props);

    $croppieContainer.attr('data-crop-type', croppieProps.viewport.type);

    $('.upload-photo-text').html(upload_text)
    if (image_url != '') {

        $('#delete_profile_photo').removeClass('d-none');
        $('#delete_profile_photo').attr('data-profile_image', image_name);
        $('.upload-demo').addClass('ready');
        $('#upload-demo').croppie('destroy');
        $("#original_file_id").val('');
        $("#original_file_id").val('update');
        cropping_url = url;

        $uploadCrop = $('#upload-demo').croppie(croppieProps);

        $('#cropImagePop').modal('show');
        $('#upload-demo .cr-image').attr('src', loader)
        $('#upload-demo .cr-image').css({'width': '40', 'height': '40', 'left': '50%', 'top': '50%', 'marginLeft': '-20px', 'marginTop': '-20px'})


        $('#cropImagePop').on('shown.bs.modal', function () {
             $(this).off('shown.bs.modal');
            $uploadCrop.croppie('bind', {
                url: image_url,
            }).then(function () {
                var image = new Image();
                image.src = image_url;
                image.onload = function () {
                    var zoomed = getMinZoom(this.width)
                    $('.cr-slider').attr({'min': zoomed, 'max': 1.5000});
                }

            });
        });
    } else {
        $('#default_img').html('<img src="' + default_img + '" height="400px">')
        $('#upload-demo').css('height', 0);
        $('#cropImagePop').modal('show');
    }
}


function showOriginalImageModal(image_url, image_name, url, loader) {
    if (image_url != '') {
        $('.upload-demo').addClass('ready');
        $('#show_image').croppie('destroy');
        $("#image_original_file_id").val('');
        $("#image_original_file_id").val('update');
        cropping_url = url;

        $uploadCrop = $('#show_image').croppie({
            viewport: {
                width: 200,
                height: 200,
                type: 'circle'
            },
            enforceBoundary: true,
            enableExif: false
        });

        $('#showProfileImagePop').modal('show');
        $('#show_image .cr-image').attr('src', loader)
        $('#show_image .cr-image').css({'width': '200', 'height': '200', 'left': '200px', 'top': '70px'})
        $('#showProfileImagePop').on('shown.bs.modal', function () {
            $uploadCrop.croppie('bind', {
                url: image_url,
            }).then(function () {

            });
        });
    }
}

function readCroppingFile(input_field_id, input, url) {
    $('#upload-demo').css('height', '400px');
    $('#default_img').html('');
    $("#preview_result_id").val('');
    $("#original_file_id").val('');
    $("#original_file_id").val('save');
    var preview_result_id = $("#" + input_field_id).attr('data-cropped-id');
    $("#preview_result_id").val(preview_result_id);
    cropping_url = url;
    if (input.files && input.files[0]) {
        inputFile = input.files[0];
       
        $("#original_filename").val(input.files[0]['name']);
        var reader = new FileReader();
        reader.onload = function (e) {
            var image = new Image();
            image.src = e.target.result;

            image.onload = function () {
                var zoom = getMinZoom(this.width);

                $('.upload-demo').addClass('ready');
                rawCropImg = e.target.result;
                $('#upload-demo').croppie('destroy');
                $uploadCrop = $('#upload-demo').croppie({
                    viewport: {
                        width: 346,
                        height: 346,
                        type: 'circle'
                    },
                    boundary: {width: "598", height: "349"},
                    enforceBoundary: true,
                    enableExif: false
                });

                $uploadCrop.croppie('bind', {
                    url: rawCropImg,
                }).then(function () {
                    $('.cr-slider').attr({'min': zoom});
                });
            };

        }
        reader.readAsDataURL(input.files[0]);
    }
}

$('#cropImageBtn').on('click', function (ev) {
    var original_fileid = $("#original_file_id").val();
    $uploadCrop.croppie('result', {
        type: 'base64',
        format: 'png',
        size: {width: 146}
    }).then(function (resp) {

        var preview_response = $('#preview_result_id').val();
        upload_cropped_images(preview_response, resp, original_fileid);
        $('#cropImagePop').modal('hide');
    });
});

$('#cropOriginalImageBtn').on('click', function (ev) {
    var original_fileid = $("#image_original_file_id").val();
    $uploadCrop.croppie('result', {
        type: 'canvas', 
        size: 'original', 
        format: 'png',  
        size: {width: 146}
    }).then(function (resp) {

        var preview_response = $('#preview_result_id').val();
        upload_cropped_images(preview_response, resp, original_fileid);
        $('#showProfileImagePop').modal('hide');
    });
});
function upload_cropped_images(preview_response_id, resp, type) {
    var formdata = new FormData();
    if (type == 'save') {
        formdata.append("photo", inputFile);
    }
    formdata.append("image", resp);
    formdata.append("type", type);

    $.ajax({
        url: cropping_url,
        type: "POST",
        data: formdata,
        processData: false,
        contentType: false,
        beforeSend: function ()
        {
            $("#coverPreloader").show();
        },
        success: function (data) {
            $("#coverPreloader").hide();
            location.reload();
        }
    });

}

function getMinZoom(width) {
    if (width > 3000) {
        return   0.1;
    } else if (width <= 3000 && width > 1500) {
        return   0.25;
    } else if (width <= 1500 && width > 750) {
        return   0.5;
    } else if (width <= 750 && width > 598) {
        return 1;
    } else if (width <= 300 && width > 150) {
        return  1.5;
    } else {
        return  2;
    }
}

