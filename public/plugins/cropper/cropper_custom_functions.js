var cropping_url, cropping_cover_url, inputFile, inputCoverFile;
var canvas  = $("#upload_demo"),
    context = canvas.get(0).getContext("2d"),
    cover_canvas = $("#upload_cover_demo"),
    cover_context = cover_canvas.get(0).getContext("2d");

const mediaQuery = window.matchMedia('(min-width: 768px)');
const cropperProps = {
    canvas: {
        props: {
            aspectRatio: 1 / 1,
            zoomable: true,
            viewMode: 3,
            dragMode: 'move',
            minCropBoxWidth: 140,
            minCropBoxHeight: 140,
            cropBoxMovable: false,
            cropBoxResizable: false,
            zoomOnTouch: false,
            zoomOnWheel: false,
            ready: function (e) {
                var cropper = this.cropper;
                var profileImageZoomSlider = new JqueryUISlider({
                    slide: zoomCropperImage
                });

                cropper.zoom(0);
                profileImageZoomSlider.init();
            }
        },
        mediaQueryProps: {
            aspectRatio: 1 / 1,
            zoomable: false,
            viewMode: 3,
            minCropBoxWidth: 140,
            minCropBoxHeight: 140,
        }
    },
    cover_canvas: {
        props: {
            aspectRatio: 16 / 9,
            zoomable: true,
            viewMode: 3,
            dragMode: 'move',
            minCropBoxWidth: 480,
            minCropBoxHeight: 304,
            cropBoxMovable: false,
            cropBoxResizable: false,
            zoomOnTouch: false,
            zoomOnWheel: false,
            ready: function (e) {
                var cropper = this.cropper;
                var profileImageZoomSlider = new JqueryUISlider({
                    slide: zoomCropperImage
                });

                cropper.zoom(0);
                profileImageZoomSlider.init();
            }
        },
        mediaQueryProps: {
            aspectRatio: 16 / 9,
            zoomable: false,
            viewMode: 3,
            minCropBoxWidth: 480,
            minCropBoxHeight: 304,
        }
    }
};

function handleTabletChange(e, element, options) {
    if (e.matches) {
        var cropper = element.cropper(options.mediaQueryProps);

        console.log('Media Query Matched!');
    } else {
        var cropper = element.cropper(options.props);
    }
}

//Show profile cropper modal
function showProfileCroppieModal(image_url, default_img, url, loader) {
    canvas.cropper('destroy')

    if (image_url != '') {
        $('#delete_profile_photo').removeClass('d-none');
        $('#delete_profile_photo').attr('data-profile_image', 'cropped');
        $('#upload_demo').css('height', '390px');
        $("#original_file_id").val('update');
        cropping_url = url;

        var img = new Image();
        img.crossOrigin="anonymous";
        img.onload = function() {
            context.canvas.height = img.height;
            context.canvas.width  = img.width;
            context.drawImage(img, 0, 0);
            handleTabletChange(mediaQuery, canvas, cropperProps.canvas);
        };
        img.src = image_url;
        $('#cropProfileImagePop').modal('show');
    } else {
        $('#default_img').html('<img src="' + default_img + '" height="390px">')
        $('#upload_demo').css('height', 0);
        $('#cropProfileImagePop').modal('show');
    }
}

//Show cover cropper modal
function showCoverCroppieModal(image_url, default_img, url, loader) {
    cover_canvas.cropper('destroy')
    console.log('image_url', image_url)
    if (image_url != '') {

        $('#delete_profile_cover').removeClass('d-none');
        $('#upload_cover_demo').css('height', '390px');
        $('#delete_profile_cover').attr('data-cover_image', 'cropped');
        $("#original_cover_file_id").val('update');
        cropping_cover_url = url;


        var img = new Image();
        img.crossOrigin="anonymous";
        img.onload = function() {
            cover_context.canvas.height = img.height;
            cover_context.canvas.width  = img.width;
            cover_context.drawImage(img, 0, 0);
            handleTabletChange(mediaQuery, cover_canvas, cropperProps.cover_canvas);
        };
        img.src = image_url;
        $('#cropCoverImagePop').modal('show');
    } else {
        $('#default_cover_img').html('<img src="' + default_img + '" height="390px" width="100%">')
        $('#upload_cover_demo').css('height', 0);
        $('#cropCoverImagePop').modal('show');
    }
}

function readProfileCroppingFile(input_field_id, input, url, crp_type) {

    if (input.files && input.files[0]) {
        $('.profile-image-error-block').html('');
        if(crp_type == 'photo'){
            $('#upload_demo').css('height', '390px');
            $('#default_img').html('');
            $("#preview_result_id").val('');
            $("#original_file_id").val('');
            $("#original_file_id").val('save');
            cropping_url = url;
            inputFile = input.files[0];
        }else{
            $('#upload_cover_demo').css('height', '390px');
            $('#default_cover_img').html('');
            $("#original_cover_file_id").val('');
            $("#original_cover_file_id").val('save');
            cropping_cover_url = url;
            inputCoverFile = input.files[0];
        }


        var reader = new FileReader();
        reader.onload = function(evt) {
            var img = new Image();
            img.onload = function() {
                if(crp_type == 'photo') {
                    context.canvas.height = img.height;
                    context.canvas.width = img.width;
                    context.drawImage(img, 0, 0);
                    canvas.cropper('destroy');
                    handleTabletChange(mediaQuery, canvas, cropperProps.canvas);
                }else{
                    cover_context.canvas.height = img.height;
                    cover_context.canvas.width  = img.width;
                    cover_context.drawImage(img, 0, 0);
                    cover_canvas.cropper('destroy');
                    handleTabletChange(mediaQuery, cover_canvas, cropperProps.cover_canvas);
                }

            };
            img.src = evt.target.result;
        };
        reader.readAsDataURL(input.files[0]);
    }
}

//Upload profile photo
$('#cropProfileImageBtn').on('click', function (ev) {
    var original_fileid = $("#original_file_id").val();
    if(original_fileid != ''){
        var profileCroppedImage = canvas.cropper('getCroppedCanvas').toDataURL("image/png");

        upload_profile_cropped_images(profileCroppedImage, original_fileid, inputFile, cropping_url, 'photo');
        $('#cropProfileImagePop').modal('hide');
    }else{
        $('.profile-image-error-block').html('<span>Upload photo image is required.</span>')
    }

});

//Upload profile cover
$('#cropCoverImageBtn').on('click', function (ev) {
    var cover_original_fileid = $("#original_cover_file_id").val();
    if(cover_original_fileid != ''){
        var coverCroppedImage = cover_canvas.cropper('getCroppedCanvas').toDataURL("image/png");

        upload_profile_cropped_images(coverCroppedImage, cover_original_fileid, inputCoverFile, cropping_cover_url, 'cover');
        $('#cropCoverImagePop').modal('hide');
    }else{
        $('.profile-image-error-block').html('<span>Upload cover image is required.</span>')
    }


});

function upload_profile_cropped_images(resp, type, input_file, upload_url, crp_type) {
    var formdata = new FormData();
    if (type == 'save') {
        formdata.append("photo", input_file);
    }

    formdata.append("image", resp);
    formdata.append("type", type);

    $.ajax({
        url: upload_url,
        type: "POST",
        data: formdata,
        processData: false,
        contentType: false,
        beforeSend: function ()
        {
            $("#coverPreloader").show();
        },
        success: function (res) {
            $("#coverPreloader").hide();

            if(res.status == 'success'){
                if(crp_type == 'cover'){
                    $('#coverImage').css('background-image', 'url(' + res.data.croped_file + ')')
                    $('#coverImage').attr('data-original_image',  res.data.original_file)
                    $('.ava-cover').attr('src',  res.data.croped_file)

                    $('.cover-img-action').find('li.del-action').remove()

                    $('.cover-img-action').append('<li class="flexed del-action"><span class="cover-btn-border"></span></li>'+
                                                  '<li class="del-action"><a type="button" class="btn btn-light button-cover-delete" id="button_cover_delete">Delete</a></li>')

                    $('.cover-img-action #button_cover').html('<i class="fa fa-camera click-target" aria-hidden="true"></i> Edit cover')
                }else{
                    location.reload();
                }
            }
        }
    });

}

function zoomCropperImage(event, ui) {
    let zoomValue = parseFloat(ui.value);
    const CROPPER_ID = this.dataset.zoomSlider;
    let $cropper = $(`#${CROPPER_ID}`);

    $cropper.cropper('zoomTo', zoomValue.toFixed(4));
}

//Zoom slider for croppers images
function JqueryUISlider(props) {
    this.default = {
        selector: '[data-zoom-slider]',
        orientation: "horizontal",
        range: "min",
        max: 1.5,
        min: 0.5,
        value: 1,
        step: 0.0001
    };
    this.options = Object.assign(this.default, props);
    this.init = function () {
        if(typeof $.fn.slider === 'function' && $(this.options.selector)) {
            $(this.options.selector).slider(this.options);
            $(".cr-slider-wrap").show();
        }
    }
}