(function(factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('jquery'));
    } else {
        factory(window.jQuery);
    }
}(function($) {
    $.fn.extend({
        placeCursorAtEnd: function() {
            if (this.length === 0) {
                throw new Error("Cannot manipulate an element!");
            }
            var el = this[0];
            var range = document.createRange();
            var sel = window.getSelection();
            var childLength = el.childNodes.length;
            if (childLength > 0) {
                var lastNode = el.childNodes[childLength - 1];
                var lastNodeChildren = lastNode.childNodes.length;
                range.setStart(lastNode, lastNodeChildren);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
            return this;
        }
    });

    $.extend($.summernote.plugins, {
        'textComplate': function(context) {
            var self = this,
                $note = context.layoutInfo.note,
                $editor = context.layoutInfo.editor,
                $editable = context.layoutInfo.editable;

            var addListener = function() {
                var setTimeOut;
                $($editable).on("keyup", delay(function(e) {
                    var tagged = getTag($editable);
                    if (e.which == 16 || e.which == 17)
                        return;

                    destroyTab();
                    if (!tagged) {
                        destroyTab();
                        return;
                    }

                    var currentTag = getTag($editable);

                    if (tagged != currentTag) {
                        return;
                    }

                    $.post(document.textcompleteUrl, { q: tagged })
                        .done(function(data) {
                            var res = JSON.parse(data);

                            // if (res.query != tagged)
                            //     return;

                            if (res.users_count <= 0 && res.places_count <= 0 && res.cities_count <= 0 && res.countries_count <= 0) {
                                destroyTab();
                                return;
                            }
                            $($editor).parent().find('.textcomplete-wrapper').css({ 'top': getCursorPosition($editable) + 17 })
                            $($editor).parent().find('.textcomplete-wrapper').html(buildTaggedItems(res, tagged));

                        });
                }, 500))

                $(document).click(function(event) {
                    $target = $(event.target);
                    if (!$target.closest('.textcomplete-wrapper').length) {
                        destroyTab();
                    }
                });


                $(document).on('click', '.drop-meta-item', function() {
                    var placeId = $(this).data('placeid');
                    var userId = $(this).data('userid');
                    var title = $(this).data('title');
                    var meta = $(this).data('meta');
                    var country_id = $(this).data('country_id');
                    var city_id = $(this).data('city_id');
                    var href = '#';
                    var idData = '';
                    var $appendTaggedDestination = '';
                    if (placeId) { //
                        $appendTaggedDestination = '<li class="tag">' + title + '<input type="hidden" name="taggedPlaces[]" value="' + placeId + '"><i class="trav-close-icon delete"></i></li>';
                        href = '/place/' + placeId;
                        idData = 'data-place_id="' + placeId + '"';
                    }

                    if (userId) {
                        href = '/profile/' + userId;
                        idData = 'data-user_id="' + userId + '"';
                    }
                    if (country_id) {
                        $appendTaggedDestination = '<li class="tag">' + title + ' <input type="hidden" name="taggedCountries[]" value="' + country_id + '"><i class="trav-close-icon delete"></i></li>';

                        href = '/country/' + country_id;
                        idData = 'data-place_id="' + country_id + '"';
                    }

                    if (city_id) {
                        $appendTaggedDestination = '<li class="tag">' + title + ' <input type="hidden" name="taggedCities[]" value="' + city_id + '"><i class="trav-close-icon delete"></i></li>';
                        href = '/city/' + city_id;
                        idData = 'data-place_id="' + city_id + '"';
                    }

                    var contents_temp = $($editable).html();
                    var content_text_temp = $($editable).text();
                    if ($($editable).closest('#createNewPostPopup').length > 0 && userId) {
                        if (content_text_temp.indexOf("@") >= 0)
                            contents_temp = contents_temp.replace('@' + meta, '<a href="' + href + '" ' + idData + ' class="summernote-selected-items post-text-tag-' + userId + '" data-toggle="popover" data-html="true" data-original-title="" title="">' + title + '</a>&nbsp;');
                        else {
                            contents_temp = contents_temp.replace(meta.trim() + '&nbsp;', '<a href="' + href + '" ' + idData + ' class="summernote-selected-items post-text-tag-' + userId + '" data-toggle="popover" data-html="true" data-original-title="" title="">' + title + '</a>&nbsp;');
                        }
                    } else {
                        if (content_text_temp.indexOf("@") >= 0)
                            contents_temp = contents_temp.replace('@' + meta, '<a href="' + href + '" ' + idData + ' class="summernote-selected-items">' + title + '</a>&nbsp;');
                        else {
                            contents_temp = contents_temp.replace(meta.trim() + '&nbsp;', '<a href="' + href + '" ' + idData + ' class="summernote-selected-items">' + title + '</a>&nbsp;');
                        }
                    }
                    $($note).summernote('code', contents_temp);
                    console.log($editable);
                    $($editable).placeCursorAtEnd();
                    $('.post-tags-list').removeClass('d-none');
                    $('body').find('.tagged-destinations-container').append($appendTaggedDestination);
                    destroyTab();
                });

            };

            var getTag = function(el) {
                var text = $(el).text();
                if (/\s+$/.test(text)) {
                    var n = text.split(" ");
                    word = n[n.length - 1];
                    return word;
                }

                var pattern = /\B@[\s\p{L}\d_-]+/ugi;
                var match = text.match(pattern);

                if (!match)
                    return null;


                var last = match.pop();
                var Word = last.replace("@", "");

                return Word;
            };




            this.events = {
                'summernote.init': function(we, e) {
                    var mainConent = mainTextcompleteContent();
                    $($editor).parent().append(mainConent);
                }
            };

            this.initialize = function() {
                addListener();
            };

            var mainTextcompleteContent = function() {
                var html = '<div class="textcomplete-wrapper"></div>';

                return html;
            }


            var buildTaggedItems = function(data, tag) {
                var appendHTML = '<div class="dropdown-menu text-complate-block"><div class="comment-search-box">' +
                    '<ul class="nav nav-tabs search-tabs" role="tablist">' +
                    '<li class="nav-item">' +
                    '<a class="nav-link active" href="#all" role="tab" data-toggle="tab">All <span>' + data.all.length + '</span></a>' +
                    '</li>' +
                    '<li class="nav-item">' +
                    '<a class="nav-link " href="#places" role="tab" data-toggle="tab">Places <span>' + data.places_count + '</span></a>' +
                    '</li>' +
                    '<li class="nav-item">' +
                    '<a class="nav-link" href="#peoples" role="tab" data-toggle="tab">People <span>' + data.users_count + '</span></a>' +
                    '</li>' +
                    '<li class="nav-item">' +
                    '<a class="nav-link" href="#cities_tub" role="tab" data-toggle="tab">Cities <span>' + data.cities_count + '</span></a>' +
                    '</li>' +
                    '<li class="nav-item">' +
                    '<a class="nav-link" href="#countries_tub" role="tab" data-toggle="tab">Countries <span>' + data.countries_count + '</span></a>' +
                    '</li>' +
                    '</ul>';

                appendHTML += '<div class="tab-content">';
                appendHTML += '<div role="tabpanel" class="tab-pane fade in active show" id="all"  aria-expanded="true">';
                if (data.all && data.all.length > 0) {
                    data.all.forEach(function(city) {
                        var title;
                        var data_attr;
                        if (city.type == 'User') {
                            title = city.name;
                            data_attr = ' data-userid="`' + city.id + '`"';
                        } else if (city.type == 'Place') {
                            title = city.title;
                            data_attr = ' data-placeid="`' + city.id + '`"';
                        } else if (city.type == 'City') {
                            data_attr = ' data-city_id="`' + city.id + '`"';
                            title = city.title;
                        } else if (city.type == 'Country') {
                            data_attr = ' data-country_id="`' + city.id + '`"';
                            title = city.title;
                        }
                        var type = city.type;
                        appendHTML += `<div class="drop-row drop-items drop-meta-item" ` + data_attr + ` data-title="` + title + `" data-meta="` + tag + `">
                                                                <div class="img-wrap rounded search-ppl-img">
                                                                    <img src="` + city.image + `">
                                                                </div>
                                                                <div class="drop-content search-place-content"><p class="search-place-name"><span class="item-name">` + markTagMatch(title, tag) + `</span></p></div></div>`;
                    })
                }
                appendHTML += '</div>';
                appendHTML += '<div role="tabpanel" class="tab-pane fade in" id="peoples"  aria-expanded="true">';
                if (data.users && data.users.length > 0) {
                    data.users.forEach(function(people) {
                        appendHTML += `<div class="drop-row drop-items drop-meta-item" data-userid="` + people.id + `" data-title="` + people.name + `" data-meta="` + tag + `">
                                                                <div class="img-wrap rounded search-ppl-img">
                                                                    <img src="` + people.image + `" >
                                                                </div>
                                                                <div class="drop-content search-people-content"><p class="search-ppl-name"><span class="item-name">` + markTagMatch(people.name, tag) + `</span></p></div></div>`;
                    })
                }
                appendHTML += '</div>';
                appendHTML += '<div role="tabpanel" class="tab-pane fade in" id="cities_tub"  aria-expanded="true">';
                if (data.cities && data.cities.length > 0) {
                    data.cities.forEach(function(city) {
                        appendHTML += `<div class="drop-row drop-items drop-meta-item" data-city_id="` + city.id + `" data-title="` + city.title + `" data-meta="` + tag + `">
                                                                <div class="img-wrap rounded search-ppl-img">
                                                                    <img src="` + city.image + `">
                                                                </div>
                                                                <div class="drop-content search-place-content"><p class="search-place-name"><span class="item-name">` + markTagMatch(city.title, tag) + `</span></p></div></div>`;
                    })
                }
                appendHTML += '</div>';
                appendHTML += '<div role="tabpanel" class="tab-pane fade in" id="countries_tub"  aria-expanded="true">';
                if (data.countries && data.countries.length > 0) {
                    data.countries.forEach(function(country) {
                        appendHTML += `<div class="drop-row drop-items drop-meta-item" data-country_id="` + country.id + `" data-title="` + country.title + `" data-meta="` + tag + `">
                                                                <div class="img-wrap rounded search-ppl-img">
                                                                    <img src="` + country.image + `">
                                                                </div>
                                                                <div class="drop-content search-place-content"><p class="search-place-name"><span class="item-name">` + markTagMatch(country.title, tag) + `</span></p></div></div>`;
                    })
                }
                appendHTML += '</div>' +
                    '<div role="tabpanel" class="tab-pane fade in" id="places">';
                if (data.places && data.places.length > 0) {

                    data.places.forEach(function(place) {
                        appendHTML += `<div class="drop-row drop-items drop-meta-item" data-placeid="` + place.id + `" data-title="` + place.title + `" data-meta="` + tag + `">
                                                                <div class="img-wrap rounded search-ppl-img">
                                                                    <img src="` + place.image + `">
                                                                </div>
                                                                <div class="drop-content search-place-content"><p class="search-place-name"><span class="item-name">` + markTagMatch(place.title, tag) + `</span></p><p class="search-place-desc"><span>` + place.address + `</span></p></div></div>`;
                    })
                }
                appendHTML += '</div>' +
                    '</div>';

                appendHTML += '</div>';

                return appendHTML;
            }

            var destroyTab = function() {
                $($editor).parent().find('.textcomplete-wrapper').html('');

            }

            var getCursorPosition = function(elem) {

                if ($(elem).find('div').length > 0) {
                    return $(elem).find('div').last().position().top;
                } else {
                    return 17;
                }
            }

            var markTagMatch = function(text, term) {
                var regEx = new RegExp("(" + term + ")(?!([^<]+)?>)", "gi");
                var output = text.replace(regEx, "<span class='tag-rendered'>$1</span>");
                return output;
            }

        }
    });
}));