var loader;
//Used to reset textbox height after sending or removing content
var textBoxDefaultHeight = 65;

//Used to determine where to insert date divider
var lastDate = '';

var messages_from_input = {};

window.Echo.private('chat-typing.' + user_id)
    .listenForWhisper('typing', (e) => {

        let conversationId = parseInt($('#chat_conversation_id').val());
        let receivedConvId = parseInt(e.conversation_id);

        if (receivedConvId == conversationId) {

            if ($('#chatContents .typing').length === 0) {
                $('#chatContents').append('<p class="typing" style="font-style: italic;">' + e.user_name + ' is typing...</p>')
                setTimeout(() => {
                    $('#chatContents .typing').remove();
                }, 2000)
            }
        }
    })



waitForElement('time',function(){
    $('.time').each(function(){
        let val = $.trim($(this).text());

         let last = timeSince(formatDate(val, 'date'));

        $(this).text(last);

    })
});

$(function () {


    loader = $('.loader');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    chooseConversation();

    //Send chat message on Enter in message-text field
    //Use Shift+Enter for new line
    $(document).on("keypress", "#message-text", function (event) {

        if (event.keyCode == 13) {
            var content = this.value;
            var caret = getCaret(this);
            if (event.shiftKey) {
                this.value = content.substring(0, caret) + "\n" + content.substring(caret, content.length);
                event.preventDefault();
            } else {
                this.value = content.substring(0, caret) + content.substring(caret, content.length);
                event.preventDefault();
                sendChatMessage();
            }
        }

        //Send typing indicators
        let conversationId = $('#chat_conversation_id').val();
        if (conversationId > 0) {
            let participants = $('#conversation-container-' + conversationId).attr('data-participants').split(';');

            $.each(participants, function (index, participant) {
                if (parseInt(user_id) !== parseInt(participant)) {
                    let channel = window.Echo.private('chat-typing.' + participant)

                    setTimeout(() => {
                        channel.whisper('typing', {
                            user: user_id,
                            user_name: user_name,
                            conversation_id: conversationId,
                            typing: true
                        })
                    }, 500)
                }
            })

        }


    });


    let textBox = $('#message-text');

    textBox.focus();

    //Auto grow text box
    textBox.keyup(function (e) {
        if ($(this).val() == '') {
            let uploadField = $('#upload');
            if(uploadField.val() == ''){
                $('.send-mess-btn').prop('disabled',true);
            }
            $(this).height(16);//textBoxDefaultHeight
        }else{
            $('.send-mess-btn').prop('disabled',false);
        }
    });

    //Trick to use <a> tag as file input
    $("#upload_link").on('click', function (e) {
        e.preventDefault();
        $("#upload:hidden").trigger('click');
    });

    //Detect which file is selected
    $('input[type="file"]').change(function (e) {

        var fileName = e.target.files[0].name;

        $('#file-name').html(fileName);
        $('.send-mess-btn').prop('disabled',false);

    });
    $('#chat-participants-new').select2({
        minimumInputLength: 2,
        width: "100%",
        placeholder: 'Type chat participants.',
        ajax: {
            url: './../../chat/dropdown/participants_image',
            dataType: 'json',
            type: "GET",
            data: function (term) {
                return {
                    term: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id,
                            profile_picture: item.profile_picture
                        }
                    })
                };
            },

        },
        templateResult: addUserPic,
        templateSelection: addUserPic,
    });
    function addUserPic (opt) {

        if (!opt.id) {
            return opt.text;
        }
        var optimage = opt.profile_picture;
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<span class="user-name" style="padding:5px;"><img width="25px" height="25px" style="padding: 2px;border-radius: 50%;" src="' + optimage + '" class="user-pic" /> ' + opt.text + '</span>'
            );
            return $opt;
        }
    };

    $('#chat-participants-new').on('select2:close', function (e) {
        let participans =  $(this).val();
        hasConversationId(participans);
    });


    //Load dropdown
    //populateDropdown('', 'chat-participants-new', './../../chat/dropdown/participants_image');

});
/* DROPZONE */

function textarea_auto_height(el, default_height = 'auto') {
    el.style.height = default_height;
    el.style.height = (el.scrollHeight + parseFloat($(el).css("borderTopWidth")) + parseFloat($(el).css("borderBottomWidth"))) + "px";
}


function dropHandler(ev) {

    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
    var form_data = new FormData();
    form_data.append('chat_conversation_id', $('#chat_conversation_id').val());
    form_data.append('message', '');
    let url = './../../chat/send';

    if (ev.dataTransfer.items) {
        // Use DataTransferItemList interface to access the file(s)

        //fileInput.files = ev.dataTransfer.items;
        for (var i = 0; i < ev.dataTransfer.items.length; i++) {
            // If dropped items aren't files, reject them
            if (ev.dataTransfer.items[i].kind === 'file') {
                var file = ev.dataTransfer.items[i].getAsFile();

                form_data.append('documents[]', file);

            }
        }


    } else {
        // Use DataTransfer interface to access the file(s)
        for (var i = 0; i < ev.dataTransfer.files.length; i++) {
            form_data.append('documents[]', ev.dataTransfer.files[i]);
        }

    }
    loader.show();
    $.ajax({
        url: url,
        data: form_data,
        cache: false,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (dataofconfirm) {

            loader.hide();
        }
    }).fail(
        function (data) {
            loader.hide()
            alert("Error sending. Error description: " + data.responseJSON.message);
        }
    );



}


function hasConversationId(participants_ids){
    if(participants_ids.length > 0) {
        let url = './../../chat/has-conversation-id';

        $.ajax({
            url: url,
            data: {participants: participants_ids},
            type: 'POST',
            success: function (data) {
                if(data.conversation_id != 0){
                    let newOne = 'conversation-container-' + data.conversation_id;
                    setChatConversationId(newOne, true, false);
                }else{
                    newChatConversation(false)
                }
            }
        }).fail(
            function (data) {
                alert("Error sending. Error description: " + data.responseJSON.message);
            }
        );
    }
}



function dragStartHandler(ev){
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();

}

function dragEndHandler(ev){
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();

}

function chooseConversation() {
    let conversations = $('[id^=conversation-container-]');

    //Set top conversation as active
    if (conversations.length > 0) {
        //If conversation id is not provided from server then set to first
        if (conversation_id_query_string == 0) {
            setChatConversationId(conversations[0].id);
        }
        else {
            setChatConversationId('conversation-container-' + conversation_id_query_string);
        }
    }
    else {
        newChatConversation(false);
    }
}

function getCaret(el) {
    if (el.selectionStart) {
        return el.selectionStart;
    } else if (document.selection) {
        el.focus();
        var r = document.selection.createRange();
        if (r == null) {
            return 0;
        }
        var re = el.createTextRange(), rc = re.duplicate();
        re.moveToBookmark(r.getBookmark());
        rc.setEndPoint('EndToStart', re);
        return rc.text.length;
    }
    return 0;
}

function newChatConversation(from_search = true) {
    //Hide title
    $('#chat-party-name').hide();

    //Show input field for new conversation
    $('#chat-party-to').show();

    //Unselect all
    let conversations = $('.current').removeClass('current');

    //Set id to 0, this will be used as flag that new conversation must be added.
    $('#chat_conversation_id').val(0);

    let messagesContainer = $('#chatContents');

    messagesContainer.empty();

    if(from_search){
        $("#chat-participants-new").val([]).trigger('change')
        $("#chat-participants-new").select2('focus')
    }

    //messagesContainer.append('<p id="no-messages">New conversation.</p>');
}

function appendNewConversation(conversation) {
    console.log(conversation);

    let conversationId = conversation.id;
    let userThumb = conversation.user_thumb;

    let participantNames = conversation.participants_names;
    let userId = conversation.user_id;
    let participantNamesChunk = conversation.participant_names_chunk;
    let participantNamesChunkBefore = conversation.participant_names_chunk_before;
    let images = conversation.participant_images;
    let conversationTitle = conversation.conversation_title;
    let conversationLastUpdatedAt = timeSince(formatDate(conversation.last_updated_at, 'date'));
    let participantsId = conversation.participants_id;
    let exp = '';
    if(images){
        exp = images.split(',');
    }


    let conversationContainer = `<div id="conversation-container-${conversationId}" class="conv-block"
             data-unread="0" data-participants="${participantsId}"
             onclick="selectConversation(this)">`;

            if(exp && exp.length > 1){

                conversationContainer += `<div class="img-wrap" ><div class="${exp.length >= 2 ? 'main-blk' : ''}" style="display: flex;flex-wrap: wrap;flex: 1 0 auto;width: 80px">`;
                // for(let i=0;i<exp.length;i++){
                //     if(exp.length >= 4){
                //         if(i == 3){
                //             conversationContainer += `<i class="fa fa-plus" style="width:25px;height:25px;text-align: center;flex: 1 0 auto;max-width: 25px;"></i>`;
                //             break;
                //         }else{
                //             conversationContainer += `<img src="${exp[i]}" style='width:25px;height:25px;flex: 1 0 auto;max-width: 25px;'>`;
                //         }
                //     }else{
                //         conversationContainer += `<img src="${exp[i]}" style='width:25px;height:25px;flex: 1 0 auto;max-width: 25px;'>`;
                //     }
                // }
                if (exp.length == 2) {
                    // conversationContainer += `
                    //     <div class="other-half-circle">
                    //         <img src="${exp[0]}">
                    //     </div> 
                    //     <div class="half-circle">
                    //         <img src="${exp[1]}">
                    //     </div>    
                    // `;
                    conversationContainer += `
                        <div class="avatar">
                            <img src="${exp[0]}">
                        </div> 
                        <div class="avatar">
                            <img src="${exp[1]}">
                        </div>    
                    `;
                } else  if (exp.length >= 3) {
                    // conversationContainer += `
                    //     <div class="other-half-circle">
                    //         <img src="${exp[0]}">
                    //     </div> 
                    //     <div class="half-circle" style="height:25px;">
                    //         <img src="${exp[1]}">
                    //     </div>
                    //     <div class="quarter-circle">
                    //         <img src="${exp[2]}">
                    //     </div> 
                    // `;
                    conversationContainer += `
                        <div class="avatar">
                            <img src="${exp[0]}">
                        </div> 
                        <div class="avatar">
                            <img src="${exp[1]}">
                        </div>
                        <div class="avatar">
                            <img src="${exp[2]}">
                        </div> 
                    `;
                }
                conversationContainer += '</div></div>';

            }else{
                conversationContainer += `
                <div class="img-wrap">
                    <div style="width: 80px">
                        <a href="/profile/${userId}">
                            <img src="${userThumb}" alt="image" style='width:50px;height:50px;'>
                        </a>
                    </div>
                </div>
              `;
            }


            conversationContainer += `<div class="conv-txt">
                <div class="name">
                    <a id="conversation-participants-${conversationId}" href="#" class="inner-link"`;
                    if(participantNames.split(',').length > 4){
                        conversationContainer += `data-toggle="tooltip" data-placement="bottom" data-animation="false" title="${participantNamesChunk}"`;
                    }

            conversationContainer += `>`;

                if(participantNames.split(',').length > 4){
                    conversationContainer += participantNamesChunkBefore;
                }else{
                    conversationContainer += participantNames;
                }

    conversationContainer += `</a>
                </div>

                <div class="msg-txt">
                    <p id="conversation-title-${conversationId}"
                       style="font-weight: normal;">${conversationTitle}</p>
                </div>
            </div>

            <div class="conv-time">
                <span id="conversation-updated-at-${conversationId}" class="time">${conversationLastUpdatedAt}</span>
                <a href="#" onclick="deleteConversation(${conversationId})" class="setting-link">
                    <i class="trav-cog"></i>
                </a>
            </div>
        </div>`;

    $('#conversations-container').append(conversationContainer);

    let partyName = $('#chat-party-name');
    addTooltip(partyName, participantNamesChunk);


    //If user is creator select this conversation
    if (conversation.created_by_id == user_id) {
        let newOne = 'conversation-container-' + conversationId;
        setChatConversationId(newOne, false);
        moveConversationToTop(conversationId);
    }
}

function deleteConversation(conversationId) {
    var r = confirm("Are you sure you want to remove this conversation?");
    if (r == true) {
        var form = document.createElement("form");

        form.method = "GET";
        form.action = "/chat/conversation/"+conversationId+"/delete";

        document.body.appendChild(form);

        form.submit();
        /*$.get("./../../chat/conversation/" + conversationId + '/delete', function (data) {
            $("#conversation-container-" + conversationId).remove();
            var container = $('#chatContents');
            container.empty();
            container.append('<p id="loading-message">Conversation removed.</p>');
            window.location.href = '/chat';

        }).fail(
            function (data) {
                alert("Error deleting. Error description: " + data.responseJSON.message);
            }
        );*/
    }
}

function findConversation(conversation) {
    if( $('#conversations-container').find('#conversation-container-' + conversation.id).length == 0 )
    {
        appendNewConversation(conversation);
    }
    else
    {
        $('#conversations-container').find("#conversation-title-" + conversation.id).html(conversation.conversation_title);
        $('#conversations-container').find("#conversation-updated-at-" + conversation.id).html(timeSince(formatDate(conversation.last_updated_at, 'date')));

        let newOne = 'conversation-container-' + conversation.id;
        setChatConversationId(newOne, false);
        moveConversationToTop(conversation.id);
    }
}

function sendChatMessage() {

    let url = './../../chat/send';
    let messageTextField = $('#message-text');
    let messageText = messageTextField.val();
    let conversationId = $('#chat_conversation_id').val();
    let uploadField = $('#upload');

    //Mark as read when message is sent
    if (conversationId > 0) {
        decreaseUnread(conversationId);
    }

    if ((messageText != '') || (uploadField.val() != '')) {

        let participants = $('#chat-participants-new').val();

        if (conversationId == 0 && participants.length < 1) {
            alert('Please select at least one participant.');
            return;
        }

        let form = document.getElementById("chat-message-form");
        let request = new FormData(form);
        request.append('participants', participants);



        loader.show();

        $.ajax({
            url: url,
            data: request,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (dataofconfirm) {
                messageTextField.val('');
                messageTextField.trigger('keyup');

                loader.hide();
                $('#file-name').html("Add attachment");
                uploadField.val('');


                findConversation(dataofconfirm.conversation);
                // appendMessage(dataofconfirm.message_new, true)
                $('.send-mess-btn').prop('disabled',true);
            }
        }).fail(
            function (data) {
                loader.hide()
                alert("Error sending. Error description: " + data.responseJSON.message);
            }
        );

    }else{
        $('.send-mess-btn').prop('disabled',true);
    }
}

function startChatWith(userId) {

    let url = './../../chat/start';

        $.ajax({
            url: url,
            data: { participants: userId },
            type: 'POST',
            success: function (dataofconfirm) {

                if(dataofconfirm.responseJSON.message=='ok') {
                    var conversation_id = dataofconfirm.responseJSON.conversation_id;
                    window.location.replace("https://www.travooo.com/chat"+"/"+conversation_id);
                }
                loader.hide();
            }
        }).fail(
            function (data) {
                loader.hide()
                console.log(data);
                alert("Error sending. Error description: " + data.responseJSON.message);
            }
        );

}

// It remembers message text for each conversation, and displays it
function rememberMessageText(conversationId){
    let id = $('.conv-block.current').attr('id');
    if(id){
        id = id.split('-')[2];
        messages_from_input[id] = $('#message-text').val();
    }
    conversationId = conversationId.split('-')[2];
    // Empty the current text
    $('#message-text').val('');
    if(messages_from_input.hasOwnProperty(conversationId)){
        $('#message-text').val(messages_from_input[conversationId]);
    }
}

function selectConversation(element) {

    let conversationId = element.id;

    rememberMessageText(conversationId)

    //Remove current class from previous
    let prevConversationId = $('#chat_conversation_id').val();

    if (parseInt(conversationId.replace('conversation-container-', '')) !== parseInt(prevConversationId)) {
        let prevContainer = $('#conversation-container-' + prevConversationId);
        prevContainer.removeClass('current');

        //Set new value to hidden input field
        setChatConversationId(conversationId);
    }

}

function setChatConversationId(convContainerId, reload = true, has_converstaion = true) {

    let container = $('#' + convContainerId);
    container.addClass('current');

    convContainerId = convContainerId.replace('conversation-container-', '')

    if(has_converstaion){
        $('#chat_conversation_id').val(convContainerId);

        //Set chat-party-name
        let partyName = $('#chat-party-name');


        //Hide input field for new conversation
        $('#chat-party-to').hide();

        //Reset selection in dropdown
        $("#chat-participants-new").val([]).trigger('chosen:updated')

        let new_conv = $('#conversation-participants-' + convContainerId);

        partyName.html(new_conv.html());
        if(new_conv.html().split(',').length > 2){

            addTooltip(partyName, new_conv.attr('data-original-title'));

        }

        partyName.show();
    }else{
        $('#conversations-container').find('.conv-block').removeClass('current')
        $('#conversations-container').find('#conversation-container-' + convContainerId).addClass('current')
    }

    if (reload == true) {
        loadMessages(convContainerId);
    }else{
        removeUnreadCountConversationMessage(convContainerId);
    }
}

function insertEmoticon(shortcode) {
    let messageTextField = $('#message-text');
    let messageText = messageTextField.val();

    messageTextField.val(messageText + shortcode);
    $('.send-mess-btn').prop('disabled',false);
}

function loadMessages(conversationId) {
    let messagesContainer = $('#chatContents');

    messagesContainer.empty();

    messagesContainer.append('<p id="loading-message">Loading...</p>');

    $.get("./../../chat/messages/" + conversationId, function (data) {

        $('#loading-message').hide();

        if (data.length > 0) {
            $.each(data, function (index, object) {
                appendMessage(object, false);
            });

            decreaseUnread(conversationId);
            removeUnreadCountConversationMessage(conversationId);
        }
        else {
            messagesContainer.append('<p id="no-messages">No messages found.</p>');
        }

    }).fail(
        function (data) {
            $('#loading-message').hide();
            alert("Error loading. Error description: " + data.responseJSON.message);
        }
    );
}

// Removes the count of unread messages from conversation participants name
function removeUnreadCountConversationMessage(conversationId){
    $('.unread-count-'+conversationId).remove();
}

// Set unread mesages count over conversation participants name
function setUnreadCountMessage(conversationId, count){

    if($('.unread-count-'+conversationId).length){
        $('.unread-count-'+conversationId).text("("+count+")");
    }else{
        $('#conversation-participants-'+conversationId).append(" <span class='unread-count-"+conversationId+"'>("+count+")</span>");
    }
}

// Add data attributes required for bootstrap tooltip
function addTooltip(el, title){
    el.attr('data-toggle','tooltip');
    el.attr('data-placement','bottom');
    el.attr('data-animation','false');

    el.attr('title',title);
    el.attr('data-original-title',title);

}

function decreaseUnread(conversationId) {
    let container = $('#conversation-container-' + conversationId);
    let unread = parseInt(container.attr('data-unread'));

    if (unread !== 0) {
        let convContainer = document.getElementById('conversation-title-' + conversationId);
        let countContainerSmall = document.getElementById("chat-unread-counter-small");
        let unreadCount = parseInt(countContainerSmall.innerHTML);
        unreadCount = unreadCount - unread;

        if (unreadCount < 0) {
            unreadCount = 0;
        }

        countContainerSmall.innerHTML = unreadCount;

        //Mark conversation as read
        convContainer.style.fontWeight = "normal";

        container.attr('data-unread', 0);

        setUnreadConversationCount();
    }
}

function incrementConversationUnread(conversationId) {
    //Increment data-unread for this conversation
    let container = $('#conversation-container-' + conversationId);
    let unread = parseInt(container.attr('data-unread'));
    unread++;
    container.attr('data-unread', unread);

    setUnreadConversationCount();
    setUnreadCountMessage(conversationId, unread);

}

function setUnreadConversationCount() {
    let conversations = $('[id^=conversation-container-]');
    let unreadConv = 0;

    $.each(conversations, function (index, object) {
        let unread = parseInt($(object).attr('data-unread'));
        if (unread > 0) {
            unreadConv++;
        }
    });

    let countContainer = $('#chat-unread-counter');

    countContainer.html(unreadConv);

    if (unreadConv > 0) {
        countContainer.show();
    }
    else {
        countContainer.hide();
    }
}

function notifyMe(title = 'Title', body = 'Hi') {
    if (!("Notification" in window)) {
        alert("This browser does not support system notifications");
    }
    else if (Notification.permission === "granted") {
        notify(title, body);
    }
    else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {
            if (permission === "granted") {
                notify(title, body);
            }
        });
    }

    function notify(title, body) {
        var notification = new Notification(title, {
            icon: window.location.origin + "/public/favicon.ico",
            body: body,
        });

        notification.onclick = function () {
            window.open(window.location.origin + "/chat");
        };
        setTimeout(notification.close.bind(notification), 10000);
    }

}

function appendMessage(message, newMessage = true) {

    let count_chat_party = $('#chat-party-name').html().split(",")
    
    let container = $('#chatContents');

    let container_class = "chat-group right";

    if (user_id != message.from_id) {

        container_class = "chat-group";

    }

    if (message.type !== null) {
        container_class = "chat-group system";
    }

    if (message.message_parsed !== null || message.file_link !== null) {

        let message_parsed = '';

        let user_thumb = '';

        let time_stamp = '';

        let date_stamp = '';

        let tsArr = [];

        tsArr = message.last_updated_at.split(" ");

        if (tsArr.length > 2) {

            time_stamp = formatDate("01/01/1970 "+tsArr[1] + " " + tsArr[2], 'time');
            date_stamp = tsArr[0];
        }

        let date_divider = '';

        if(date_stamp != lastDate){
            date_divider = `<div style="text-align: center; display: inline-block; width: 100%; font-size: 12px; margin-top: -5px; margin-bottom: -10px; padding-top: 10px;">${date_stamp}</div><hr>`;
            lastDate = date_stamp;
        }

        user_thumb = ($.isEmptyObject(message.from_user_thumb)) ? false : message.from_user_thumb;

        if (message.message_parsed !== null) {
            message_parsed += message.message_parsed;
        }

        if (message.file_link !== null) {

            if (message.message_parsed === null) {
                message_parsed += message.file_link;
            }
            else {
                message_parsed += '<br>' + message.file_link;
            }
        }
        let profile_id = user_id;
        if (user_id != message.from_id) {
            profile_id = message.from_id;
        }

        let message_detail = `<div class="${container_class}">
                                ${(count_chat_party.length > 1 && user_id != message.from_id) ?`<div>${message.from_user_name}</div>` : ''}
                                <div class="avatar-block">
                                <a href="/profile/${profile_id}">`;
                                    if(user_thumb && message.type === null){
                                        message_detail += `<img src="${user_thumb}" alt="avatar">`;
                                    }else{
                                        message_detail += `<img src="/assets2/image/placeholders/male.png" alt="avatar">`;
                                    }
                                message_detail +=
                                 `</a>
                                </div>
                                <div class="text-block" data-toggle="tooltip" data-placement="bottom" data-animation="false" title="${timeSinceMessage(formatDate(message.last_updated_at,'date'))}">
                                    ${message_parsed}
                                </div>
                                <div><p style="font-size:10px;">${timeSinceMessage(formatDate(message.last_updated_at,'date'))}</p></div>
                            </div>`
        let message_container = date_divider + message_detail;

        //Remove No messages found if present
        $('#no-messages').remove();

        container.append(message_container);

        //Scroll to bottom
        container.scrollTop(container.height() * 1000 * 1000 * 1000 * 1000 * 1000);

        //Update time
        if (newMessage == true) {
            $('#conversation-updated-at-' + message.chat_conversation_id).html(timeSince(formatDate(message.last_updated_at, 'date')));

            //Remove typing indicator to maintain message order
            $('#chatContents .typing').remove();

            //System browser notification
            if (user_id != message.from_id) {
                notifyMe(message.from_user_name, message.message_parsed.substring(0, 100));
            }
        }

        //$('[data-toggle="tooltip"]').tooltip();
    }
}

function moveConversationToTop(conversationId) {
    let convContainer = $('#conversation-container-' + conversationId);

    let conversations = $('[id^=conversation-container-]');

    //Move conversation to top
    if (conversations.length > 1) {
        let topConversation = conversations[0];
        if (topConversation.id !== convContainer.id) {
            convContainer.insertBefore(topConversation);
        }
    }
}

function populateDropdown(value, fieldID, url, isChosen = true, triggerChange = false) {
    var t = typeof url;

    var combo = $('#' + fieldID);
    if (t == 'string') {
        $.getJSON(url, function (json) {
            $.each(json, function (index, object) {
                var selected = '';
                if (index == value) {
                    selected = ' selected';
                }
                combo.append('<option data-img-src="'+ object['profile_picture'] +'" value="' + object['id'] + '"' + selected + '>' + object['name'] + '</option>');
            });

            if (isChosen == true) {
                combo.chosen({
                    width: '100%',
                    no_results_text: 'No users found.',
                    placeholder_text_multiple: 'Please select chat participants.',
                    placeholder_text_single: 'Please select chat participant.'
                });
            }

            if (triggerChange == true) {
                combo.trigger('change');
            }

        }).fail(function () {
            console.log('Populate ' + fieldID + ' failed.');
        });
    }
    else {
        $.each(url, function (index, object) {
            var selected = '';
            if (index == value) {
                selected = ' selected';
            }
            combo.append('<option value="' + index + '"' + selected + '>' + object + '</option>');
        });

        if (isChosen == true) {
            combo.chosen({
                width: '100%',
                no_results_text: 'No users found.',
                placeholder_text_multiple: 'Please select chat participants.',
                placeholder_text_single: 'Please select chat participant.'
            });
        }

        if (triggerChange == true) {
            combo.trigger('change');
        }
    }
}

function markCurrentAsRead() {
    let conversationId = parseInt($('#chat_conversation_id').val());

    if (conversationId !== 0) {
        decreaseUnread(conversationId);
    }
}

$(document).on('click','.appended-img', function() {
    $('body').css('overflow','hidden');
    $('#overlay-for-image')
        .css({backgroundImage: `url(${this.src})`})
        .addClass('open')
        .one('click', function() { $(this).removeClass('open');$('body').css('overflow',''); });
});

// Chat page mobile layout
var isTouchDevice = "ontouchstart" in window || navigator.msMaxTouchPoints > 0;
if(isTouchDevice) {
    $('#newChat, #conversations-container .conv-block').click(function(e) {
        $('.message-chat').show(); 
        $('.message-conversation').hide();
    })
    $('#back-to-messages').click(function(e) {
        $('.message-chat').hide(); 
        $('.message-conversation').show();
    });
}
