<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Binaries
    |--------------------------------------------------------------------------
    |
    | Paths to ffmpeg nad ffprobe binaries
    |
    */

    'binaries' => [
        'ffmpeg'  => env('FFMPEG_BINARIES', '/usr/bin/ffmpeg'),
        'ffprobe' => env('FFPROBE_BINARIES', '/usr/bin/ffprobe')
    ]
];
