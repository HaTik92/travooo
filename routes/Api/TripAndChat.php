<?php

use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['auth:api', 'api_request:trip'], 'prefix' => 'trip'], function () {
    // Chat
    Route::get('/{trip_id}/chat', 'TripController@__chat')->where(['trip_id' => '[0-9]+']);
    Route::get('/{trip_id}/place-chat', 'TripController@__placeChat')->where(['trip_id' => '[0-9]+']);
    /*
        Trip creation : [Memory / Upcoming] 
    */
    Route::post('/', 'TripController@createStepOne');
    // Route::put('/', 'TripController@changeStepOne');
    Route::post('/{trip_id}/activate', 'TripController@postActivateTrip')->where([
        'trip_id' => '[0-9]+',
    ]);
    Route::delete('/{trip_id}', 'TripController@postAjaxDeleteTrip')->where([
        'trip_id' => '[0-9]+',
    ]);

    // places
    Route::post('/{trip_id}/place', 'TripController@postAddPlaceToTrip')->where(['trip_id' => '[0-9]+']);;
    Route::delete('/{trip_id}/places/{trip_place_id}/', 'TripController@postRemovePlaceFromTrip')->where([
        'trip_id' => '[0-9]+',
        'trip_place_id' => '[0-9]+'
    ]);
    Route::put('/{trip_id}/places/{trip_place_id}/', 'TripController@postEditPlaceInTrip')->where([
        'trip_id' => '[0-9]+',
        'trip_place_id' => '[0-9]+'
    ]);
    Route::get('/{trip_id}/places/{trip_place_id}/', 'TripController@ajaxGetTripPlace')->where([
        'trip_id' => '[0-9]+',
        'trip_place_id' => '[0-9]+'
    ]);
    Route::get('/{trip_id}/places/{trip_place_id}/story', 'TripController@ajaxGetTripPlaceStory')->where([
        'trip_id' => '[0-9]+',
        'trip_place_id' => '[0-9]+'
    ]);
    Route::get('/{trip_id}/places/{trip_place_id}/media/{trip_place_media_id}', 'TripController@findTripPlaceMedia')->where([
        'trip_id' => '[0-9]+',
        'trip_place_id' => '[0-9]+',
        'trip_place_media_id' => '[0-9]+',
    ]);
    Route::post('/{trip_id}/places/{place_id}/publish', 'TripController@postAjaxPublishTripPlace')->where([
        'trip_id' => '[0-9]+',
        'place_id' => '[0-9]+'
    ]);

    //media upload & delete
    Route::post('/{trip_id}/media', 'TripController@postUploadMedia')->where(['trip_id' => '[0-9]+']);;
    Route::delete('/{trip_id}/media', 'TripController@deleteTripPlaceMedia')->where(['trip_id' => '[0-9]+']);;

    // other trips apis
    Route::get('/{trip_id}/activity-logs', 'TripController@getPlanActivityLog')->where(['trip_id' => '[0-9]+']);
    Route::get('/{trip_id}/summary', 'TripController@getBasicPlanContents')->where(['trip_id' => '[0-9]+']);;
    Route::get('/menu', 'TripController@getTripMenu');
    Route::get('/{trip_id}/map', 'TripController@tripMapPoints')->where(['trip_id' => '[0-9]+']);
    Route::post('/{id}/undo', 'TripController@undoChanges')->where(['id' => '[0-9]+']);
    // search
    Route::get('/search/cities', 'TripController@searchCitiesForSelect');
    // Route::get('/place/provider', 'TripController@ajaxGetTripPlaceByProviderId');
    Route::get('/trending-locations', 'TripController@getTrendingLocations');

    // suggestions --op
    Route::post('/{trip_id}/places/{trip_place_id}/suggestions', 'SuggestionsController@suggest')->where([
        'trip_id' => '[0-9]+',
        'trip_place_id' => '[0-9]+'
    ]);
    Route::get('/{trip_id}/places/{place_id}/suggestions', 'SuggestionsController@getSuggestions')->where([
        'trip_id' => '[0-9]+',
        'place_id' => '[0-9]+'
    ]);
    Route::post('{trip_id}/suggestions', 'TripController@sendSuggestions')->where(['trip_id' => '[0-9]+']);
    Route::get('/{trip_id}/suggestions', 'SuggestionsController@getReviewList')->where(['trip_id' => '[0-9]+']);
    Route::post('/suggestion/{suggestion_id}/approve', 'TripController@approveSuggestion')->where(['suggestion_id' => '[0-9]+']);
    Route::post('/suggestion/{suggestion_id}/disapprove', 'TripController@disapproveSuggestion')->where(['suggestion_id' => '[0-9]+']);
    Route::post('/{trip_id}/places/{place_id}/review', 'SuggestionsController@postReview')->where([
        'trip_id' => '[0-9]+',
        'place_id' => '[0-9]+'
    ]);
    Route::post('/{trip_id}/place/{place_id}/dismiss', 'TripController@postAjaxDismissPlaceFromTrip')->where([
        'trip_id' => '[0-9]+',
        'place_id' => '[0-9]+'
    ]);
    // delete any suggestion ( new add trip place / change in exiting [tags,duration,time,budget.....] ) by Editor  before sending to admin
    Route::post('/{trip_id}/place/{place_id}/discard', 'TripController@postAjaxDiscardPlaceFromTrip')->where([
        'trip_id' => '[0-9]+',
        'place_id' => '[0-9]+'
    ]);

    // invite people --op
    Route::post('/{trip_id}/invite', 'TripController@postInviteFriends')->where(['trip_id' => '[0-9]+']);
    Route::delete('/{trip_id}/invite', 'TripController@postCancelInvitation')->where(['trip_id' => '[0-9]+']);
    Route::put('/{trip_id}/invite/role', 'TripController@changeInvitationRequestRole')->where(['trip_id' => '[0-9]+']);
    Route::post('/invitation/{invitation_id}/accept', 'TripController@postAcceptInvitation')->where(['invitation_id' => '[0-9]+']);
    Route::post('/invitation/{invitation_id}/reject', 'TripController@postRejectInvitation')->where(['invitation_id' => '[0-9]+']);
    Route::get('/{trip_id}/leave', 'TripController@getLeaveList')->where(['trip_id' => '[0-9]+']);
    Route::post('/{trip_id}/leave', 'TripController@postLeavePlan')->where(['trip_id' => '[0-9]+']);
    Route::get('/{id}/people', 'TripController@getPeopleToInvite')->where(['id' => '[0-9]+']);
    Route::get('/{id}/invited-people', 'TripController@getInvitedPeople')->where(['id' => '[0-9]+']);
});


Route::group(['middleware' => ['auth:api', 'api_request:chat'], 'prefix' => 'chat'], function () {
    // Chat
    Route::get('/place/users', 'PlaceChatsController@ajaxGetPlaceUsersToChat');
    // Messages History
    Route::get('/{chat_conversation_id}/message/', 'ChatPrivateController@getMessages')->where(['chat_conversation_id' => '[0-9]+']);
    // Send Messages : Trip Group
    Route::post('/{chat_conversation_id}/message/', 'PlaceChatsController@ajaxCreatePlaceChatMessage')->where(['chat_conversation_id' => '[0-9]+']);

    // Get Trip Participants
    // Route::post('/{trip_id}', 'PlaceChatsController@ajaxPlanChats')->where(['trip_id' => '[0-9]+']);

    // Route::get('/place', 'PlaceChatsController@ajaxGetPlaceChatMessages');
    // Route::get('/place/users', 'PlaceChatsController@ajaxGetPlaceChatUsers');

    // Route::get('/place/plan-users-to-chat', 'PlaceChatsController@ajaxGetPlanUsersToChat');
    // Route::get('/place/PlanPlaceUsersToChat', 'PlaceChatsController@ajaxGetPlanPlaceUsersToChat');
    // Route::get('/place/actual-chats', 'PlaceChatsController@ajaxGetActualChats');

    // Route::get('conversations/', 'ChatPrivateController@conversations');
    Route::post('/send', 'ChatPrivateController@send');
    // Route::get('/conversations/{chat_conversation_id}', 'ChatPrivateController@getConversations');
    // Route::get('/dropdown/participants', 'ChatPrivateController@getParticipantsCombo');
    // Route::get('/dropdown/participants_image', 'ChatPrivateController@getParticipantsComboImage');
    // Route::get('/last/{number}', 'ChatPrivateController@getLastFive');
    // Route::get('/conversation/{conversation_id}/delete', 'ChatPrivateController@deleteConversation');
    // Route::get('/new/{user_id}', 'ChatPrivateController@createNewConversation');
});
