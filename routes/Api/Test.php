<?php

use App\Exceptions\GeneralException;
use App\Models\User\User;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Http\Constants\CommonConst;
use App\Http\Responses\ApiResponse;
use App\LoadedFeed;
use App\Models\ActivityLog\ActivityLog;
use App\Models\ActivityMedia\Media;
use App\Models\ApiRequest;
use App\Models\City\Cities;
use App\Models\Country\Countries;
use App\Models\Discussion\Discussion;
use App\Models\Place\Place;
use App\Models\Posts\Posts;
use App\Models\Reports\Reports;
use App\Models\Reports\ReportsInfos;
use App\Models\TripPlaces\TripPlaces;
use App\Models\TripPlans\TripPlans;
use App\Models\User\UsersContentLanguages;
use App\Models\User\UserSession;
use App\Models\User\UserWatchHistory;
use App\Models\UsersFollowers\UsersFollowers;
use App\Services\Api\CCPNewsfeed;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Services\Api\LoadedFeedService;
use App\Services\Reports\ReportsService;
use Illuminate\Support\Facades\Schema;

// -- Start Testing

Route::group(['prefix' => 'test'], function () {
    Route::get('/users/{email?}', 'TestingController@users');
    Route::get('/db', 'TestingController@script');
    // Route::get('/users/delete/{email}', 'TestingController@deleteUser');
    // Route::get('/event-fire', 'User\UserController@publicEventFire');
    // Route::get('/algo', 'TestingController@algoTest')->middleware('auth:api');

    // only for testing
    Route::get('aync-seed', 'TestingController@ayncSeed');
    Route::get('debug', 'TestingController@debug');
    Route::get('find-duplication/{module}', 'TestingController@findDuplication');
    Route::get('debug-user-mode/{id}', 'TestingController@debugUserMode');
    Route::get('last-api-requests/{num}', 'TestingController@lastApiRequests');
    Route::get('/flush-session', 'TestingController@flushSession');
    Route::get('/dump-data', 'TestingController@dumpingXData');

    Route::get('truncate-data', function () {
        Schema::disableForeignKeyConstraints();
        UserWatchHistory::truncate();
        ApiRequest::truncate();
        LoadedFeed::truncate();
        UserSession::truncate();
        Schema::enableForeignKeyConstraints();
        dd('done');
    });
});

Route::get('users/{user_id}/event-fire', 'UserController@eventFire');
// -- End Testing