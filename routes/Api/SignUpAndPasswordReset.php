<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'api', 'prefix' => 'oauth'], function () {
    Route::get('/{driver}', 'SocialAuthController@redirectToProvider');
    Route::get('/{driver}/callback', 'SocialAuthController@handleProviderCallback');
});

Route::group(['middleware' => 'api', 'prefix' => 'password'], function () {
    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
});

Route::get('verify_link/{email}/{code}', 'PasswordResetController@verifyDevice')->middleware(['handle_link']);

/* User management route */
Route::group(['prefix' => 'users', 'as' => 'user.', 'namespace' => 'User'], function () {

    //SIGN IN
    Route::post('/login', 'UserController@login');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/logout', 'UserController@logout');
    });

    //SIGN UP
    Route::post('registration', 'UserController@registrationStepOne');
    Route::group(['middleware' => 'registration_steps'], function () {

        Route::get('/registration/languages', 'UserController@getLanguages');

        // common for both
        Route::post('registration/confirmation', 'UserController@registrationStepConfirmationEmail');
        Route::post('registration/resend-confirmation-code', 'UserController@resendConfirmationCode');

        // Expert
        Route::post('registration/social-links', 'UserController@registrationStepFourExpert');

        // Regular
        Route::post('registration/places', 'UserController@registrationStepFour');

        // Both
        Route::post('registration/city-and-country', 'UserController@registrationStepThree');
        Route::post('registration/area-of-expertise', 'UserController@registrationStepFiveExpert');
        Route::post('registration/travel-styles', 'UserController@registrationStepSix');
        Route::post('registration/languages', 'UserController@registrationStepTen'); // final step
        Route::post('registration/{step_type}', 'UserController@registrationUpdateSteps'); // Step Updates [Skip | Go Back]
        // Route::get('registration/interests', 'UserController@registrationStepFiveFind');
        // Route::post('registration/interests', 'UserController@registrationStepFive');
    });

    // Other Search Api
    Route::get('registration/check-username', 'UserController@registrationCheckUsername');
    Route::get('registration/city-and-country', 'UserController@searchCitiesOrCountries');
    Route::get('registration/area-of-expertise', 'UserController@searchAreaExpertise');
    Route::get('registration/places', 'UserController@searchPlaces');
    Route::get('registration/travel-styles', 'UserController@searchTravelStyles');
});


Route::group(['middleware' => 'auth:api', 'prefix' => 'users'], function () {
    Route::get('/getUserList', 'UsersController@getUserList');
});

// facebook login & registration api
Route::post('facebook', 'User\SocialController@facebookLogin');
Route::post('registration/facebook', 'User\SocialController@facebookRegistration');

// twitter login & registration api
Route::post('twitter', 'User\SocialController@twitterLogin');
Route::post('registration/twitter', 'User\SocialController@twitterRegistration');

// google login & registration api
Route::post('google', 'User\SocialController@googleLogin');
Route::post('registration/google', 'User\SocialController@googleRegistration');

// apple login & registration api
Route::post('apple', 'User\SocialController@appleLogin');
Route::post('registration/apple', 'User\SocialController@appleRegistration');
