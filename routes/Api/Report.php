<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:api', 'api_request:reports'], 'prefix' => 'reports'], function () {

    // Report Creation 
    Route::post('/create', 'ReportsController@postCreateEditFirstStep'); // step 1
    Route::get('/search-location', 'ReportsController@searchLocation'); // step 1
    Route::get('/{report_id}/search-location-details', 'ReportsController@searchLocationWithFullDetails'); // step 2: travooo info : based on step 1
    Route::get('/search-plan', 'ReportsController@getMorePlan');
    Route::get('/search-place', 'ReportsController@searchPlace');
    Route::post('/{report_id}/upload-video', 'ReportsController@postUploadVideo');
    Route::post('/{report_id}/upload-image', 'ReportsController@postUploadImage');
    Route::post('/{report_id}/upload-cover', 'ReportsController@postUploadCover');
    Route::get('/search-map', 'ReportsController@searchMap');
    Route::get('/search-user', 'ReportsController@searchUser');

    Route::get('get-plan', 'ReportsController@getPlanByID');

    Route::delete('{report_id}', 'ReportsController@postDelete');
    Route::post('{report_id}/publish', 'ReportsController@postPublish');


    // other
    Route::get('getDraftByID', 'ReportsController@getDraftByID');
    Route::get('getInfoByID', 'ReportsController@getInfoByID');
    Route::get('get-country', 'ReportsController@getCountryByID');

    Route::get('get-place-info/{id}', 'ReportsController@getPlaceInfo');
    Route::get('get-city', 'ReportsController@getCityByID');
    Route::post('geocodeCountry', 'ReportsController@postGeocodeCountry');
    Route::post('geocodeCity', 'ReportsController@postGeocodeCity');
    Route::get('searchMentionUser', 'ReportsController@searchMentionUser');

    Route::get('get-location-info/{location_id}', 'ReportsController@getLocationInfo');
    Route::get('get-location-list/{report_id}', 'ReportsController@getLocationList');
    Route::get('searchCountry', 'ReportsController@searchCountry');
    Route::get('searchCity', 'ReportsController@searchCity');
    Route::get('get-user-info', 'ReportsController@getUserInfo');


    Route::get('getCreate', 'ReportsController@getCreate');

    Route::post('create-info', 'ReportsController@postCreateEditSecondStep');
    Route::post('create-infos', 'ReportsController@newPostCreateEditSecondStep');
    Route::post('comment', 'ReportsController@postComment');
    Route::post('share', 'ReportsController@postShare');
    Route::get('likeunlike', 'ReportsController@postLike');
    Route::post('commentdelete', 'ReportsController@postCommentDelete');
    Route::post('postcomment', 'ReportsController@postCommentForPlace');
    Route::post('commentlike', 'ReportsController@postCommentLike');
    Route::post('commentlikeusers', 'ReportsController@getCommentLikeUsers');
    Route::post('commentedit', 'ReportsController@postCommentEdit');
    Route::post('loadMoreComment', 'ReportsController@getMoreComment');
    Route::post('reportlikes4modal', 'ReportsController@postLikes4Modal');
    Route::post('reportcommentlikes4modal', 'ReportsController@postCommentLikes4Modal');
    Route::post('report_comment4modal', 'ReportsController@reportComments4Modal');
    Route::post('loadMoreCommentLikeUser', 'ReportsController@getMoreCommentLikeUsers');
    Route::post('load-more-like-user', 'ReportsController@getMoreReportLikeUsers');


    Route::get('search-report-plan', 'ReportsController@searchReportPlan');
});
