<?php

use Illuminate\Support\Facades\Route;

// Book Module
// Route::group(['middleware' => 'auth:api', 'prefix' => 'book'], function () {
//     Route::get('/hotel', 'BookController@getHotels');
//     Route::get('/search-hotel', 'BookController@searchHotels');
//     Route::get('/search-flight', 'BookController@searchFlights');
//     Route::get('/complete-search-hotel', 'BookController@completeSearchHotels');
//     Route::get('/complete-search-flight', 'BookController@completeSearchFlights');
//     Route::get('/flight', 'BookController@getFlights');
//     Route::get('/search-city', 'BookController@searchCity');
// });


// Hotel Module
// Route::group(['middleware' => 'auth:api', 'prefix' => 'hotel'], function () {
//     Route::get('/get_media', 'HotelController@getMedia');

//     Route::get('/{hotel_id}', 'HotelController@getIndex');
// Route::get('/{hotel_id}/weather', 'HotelController@getWeather');
// Route::get('/{hotel_id}/packing-tips', 'HotelController@getPackingTips');
// Route::get('/{hotel_id}/etiquette', 'HotelController@getEtiquette');
// Route::get('/{hotel_id}/health', 'HotelController@getHealth');
// Route::get('/{hotel_id}/visa-requirements', 'HotelController@getVisaRequirements');
// Route::get('/{hotel_id}/when-to-go', 'HotelController@getWhenToGo');
// Route::get('/{hotel_id}/caution', 'HotelController@getCaution');

//     Route::get('/{hotel_id}/check-follow', 'HotelController@postCheckFollow');
//     Route::get('/{hotel_id}/follow', 'HotelController@postFollow');
//     Route::get('/{hotel_id}/unfollow', 'HotelController@postUnFollow');
//     // Route::post('/{hotel_id}/visa-requirements', 'HotelController@postVisaRequirements');
//     // Route::post('/{hotel_id}/weather', 'HotelController@postWeather');
//     Route::get('/{hotel_id}/talking-about', 'HotelController@postTalkingAbout');
//     Route::get('/{hotel_id}/now-in-place', 'HotelController@postNowInPlace');
//     // Route::post('/{hotel_id}/contribute', 'HotelController@postContribute');
//     Route::post('/{hotel_id}/postReview', 'HotelController@postReview');
//     Route::get('/{hotel_id}/happeningToday', 'HotelController@happeningToday');
//     Route::get('/{hotel_id}/hotelRates', 'HotelController@hotelRates');
//     Route::post('/trackRef', 'HotelController@trackRef');
// });

// Restaurant Module
// Route::group(['middleware' => 'auth:api', 'prefix' => 'restaurant'], function () {
//     Route::get('get_media', 'RestaurantController@getMedia');
//     Route::get('{restaurant_id}', 'RestaurantController@getIndex');
//     // Route::get('/{restaurant_id}/weather', 'RestaurantController@getWeather');
//     // Route::get('/{restaurant_id}/packing-tips', 'RestaurantController@getPackingTips');
//     // Route::get('/{restaurant_id}/etiquette', 'RestaurantController@getEtiquette');
//     // Route::get('/{restaurant_id}/health', 'RestaurantController@getHealth');
//     // Route::get('/{restaurant_id}/visa-requirements', 'RestaurantController@getVisaRequirements');
//     // Route::get('/{restaurant_id}/when-to-go', 'RestaurantController@getWhenToGo');
//     // Route::get('/{restaurant_id}/caution', 'RestaurantController@getCaution');

//     Route::get('{restaurant_id}/check-follow', 'RestaurantController@postCheckFollow');
//     Route::get('{restaurant_id}/follow', 'RestaurantController@postFollow');
//     Route::get('/{restaurant_id}/unfollow', 'RestaurantController@postUnFollow');
//     // Route::post('/{restaurant_id}/visa-requirements', 'RestaurantController@postVisaRequirements');
//     // Route::post('/{restaurant_id}/weather', 'RestaurantController@postWeather');
//     Route::get('/{restaurant_id}/talking-about', 'RestaurantController@postTalkingAbout');
//     Route::get('/{restaurant_id}/now-in-place', 'RestaurantController@postNowInPlace');
//     // Route::post('/{restaurant_id}/contribute', 'RestaurantController@postContribute');
//     Route::post('/{restaurant_id}/postReview', 'RestaurantController@postReview');
//     Route::get('/{restaurant_id}/happeningToday', 'RestaurantController@happeningToday');
// });