<?php

use Illuminate\Support\Facades\Route;


Route::group(['middleware' => 'api_request:discussions'], function () {
    Route::get('discussions/', 'DiscussionController@searchDiscussion');
    Route::get('discussions/tabs/{tab_name}', 'DiscussionController@tabs');
    Route::group(['middleware' => 'auth:api', 'prefix' => 'discussions'], function () {

        Route::get('/{discussion_id}', 'DiscussionController@getView')->where(['discussion_id' => '[0-9]+']);;
        // Route::get('/ajaxSearchDestination', 'HomeController@getSelect2Location');

        Route::post('/{discussion_id}/updownvote', 'DiscussionController@postUpDownVotes');
        Route::post('/{discussion_id}/reply/{reply_id}/updownvote', 'DiscussionController@postUpDownReplyVotes');

        // Route::get('/ajaxSearchTrips', 'HomeController@getSelect2Trips');
        Route::get('/{discussion_id}/edit', 'DiscussionController@getEdit');
        Route::post('/{discussion_id}/edit', 'DiscussionController@postEdit');
        Route::post('/check-updownvote', 'DiscussionController@postCheckUpDownVote');
        Route::post('/reply/report/{reply?}', 'DiscussionController@reportSpam');
        Route::put('/edit-reply/{reply?}', 'DiscussionController@putEditReply');
        Route::delete('/delete-reply/{reply_id}', 'DiscussionController@deleteReply');
        Route::post('/post-view-count', 'DiscussionController@postDiscussionViewCount');
        Route::delete('/{discussion_id}/delete', 'DiscussionController@postDelete');

        // Route::post('/likes4modal', 'DiscussionController@postLikes4Modal');
        Route::post('/shareunshare', 'DiscussionController@postShareUnshare');

        // Route::post('/ajaxreply', 'DiscussionController@postAjaxReply');

        Route::get('/search/about', 'DiscussionController@getAboutDiscussion'); // search coutry, city, place wise
        Route::get('/search/experts', 'DiscussionController@getExperts'); // get-experts
        Route::post('/create', 'DiscussionController@postCreate');
        Route::get('/likeunlike', 'DiscussionController@postLikeUnlike');

        Route::post('/get-discussion-view', 'DiscussionController@getDiscussionView');
        Route::post('/get-more-helpful', 'DiscussionController@getMoreHelpful');
        Route::post('/get-trending-topic', 'DiscussionController@getTrendingTopic');
        Route::post('/get-more-replies', 'DiscussionController@getMoreReplies');
    });
});
