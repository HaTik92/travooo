<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:api', 'api_request:setting', 'api_setting'], 'prefix' => 'setting'], function () {
    Route::get('{user_id}/social-links', 'SettingsController@getSocial');
    Route::get('{user_id}/agent-info', 'SettingsController@agentInfo');
    Route::post('{user_id}/security', 'SettingsController@postSecurity');
    Route::put('{user_id}/social-links', 'SettingsController@postSocial');
    Route::get('{user_id}/my-account', 'SettingsController@getMyProfile');
    Route::put('{user_id}/my-account', 'SettingsController@postAccount');
    Route::get('{user_id}/block/users', 'SettingsController@getBlockUsers');
    Route::put('{user_id}/block-unblock/{blocking_user_id}', 'SettingsController@blockUnBlockUser')->where(['user_id' => '[0-9]+']);

    Route::post('{user_id}/account-delete/request', 'SettingsController@sendMailOfDeleteRequest');
    Route::delete('{user_id}/account-delete/{token}', 'SettingsController@finalDeleteAccount');

    Route::get('{user_id}/privacy', 'SettingsController@fetchPrivacy');
    Route::put('{user_id}/privacy', 'SettingsController@putPrivacy');
});
