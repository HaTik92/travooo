<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* ----------------------------------------------------------------------- */

/*
 * Api Routes
 * Namespaces indicate folder structure
 */
Route::group(['prefix' => 'api'], function () {

    Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
    Route::post('authenticate', 'AuthenticateController@authenticate');

    /*
     * These routes need view-api permission
     * (good if you want to allow more than one group in the api,
     * then limit the api features by different roles or permissions)
     *
     */
    // includeRouteFiles(__DIR__.'/Api/');

    /*
     * User Manager
     */
    Route::group([
        'prefix' => 'users',
        'as' => 'user.',
        'namespace' => 'User',
        ], function () {

        /*
         * All User Related Api's Routes Will Be Defined Here
         */
        /* Sign Up/Login - Facebook */
        Route::post('create/facebook', 'UserController@FacebookSocialLogin');
        /* Sign Up/Login - Facebook */
        Route::get('create/facebook', 'UserController@FacebookSocialLogin');

        Route::middleware(['sessionStart'])->group(function () {
            Route::get('create/twitter/callback', 'UserController@handleProviderCallback');
            Route::get('create/twitter/login', 'UserController@redirectToProvider');
        });

        /* Sign Up/Login - Twitter  */
        Route::post('create/twitter', 'UserController@TwitterSocialLogin');
        /* Sign Up/Login - Twitter  */
//        Route::get('create/twitter', 'UserController@TwitterSocialLogin');
        /* Sign Up/Login - Twitter  */
//        Route::get('create/twitter/login', 'UserController@TwitterSocialLoginPage');
        /* Twitter Acces Token APi */
//        Route::post('create/twitter', 'UserController@TwitterSocialLoginSend');
        /* Sign Up/Create New User Api - 1 */
        Route::post('/create', 'UserController@createStep1');
        /* Sign Up/Create New User Api - 2 */
        Route::post('/create/step2', 'UserController@createStep2');
        /* Sign Up/Create New User Api - 3 */
        Route::post('set/fav_countries', 'UserController@createStep3');
        /* Sign Up/Create New User Api - 4 */
        Route::post('set/fav_places', 'UserController@createStep4');
        /* Sign Up/Create New User Api - 5 */
        Route::post('set/travel_styles', 'UserController@createStep5');
        /* Login Api */
        Route::post('/login', 'UserController@login');
        /* Logout Api */
        Route::post('/logout', 'UserController@logout');
        /* Forgot Password Request Api */
        Route::post('/forgot', 'UserController@forgot');
        /* Activate User Api */
        Route::get('/activate/{token}', 'UserController@activate');
        /* Get User Information Api */
        Route::get('/info/{user_id}/{session_token}', 'UserController@information');

        Route::middleware(['jwt.refresh'])->group(function () {
            Route::post('/refresh-token', 'UserController@refreshJwtToken');
        });

        Route::middleware(['jwt.auth'])->group(function () {
            /* Reset Password Api */
            Route::post('/reset', 'UserController@reset');
            /* Change Password Api */
            Route::post('/password', 'UserController@change_password');
        });
        /* Change Fullname Api */
        Route::post('/fullname', 'UserController@update_fullname');
        /* Update Mobile Number Api */
        Route::post('/mobile', 'UserController@update_mobile');
        /* Update Address Api */
        Route::post('/address', 'UserController@update_address');
        /* Update Age Api */
        Route::post('/age', 'UserController@update_age');
        /* Update Nationality Api */
        Route::post('/nationality', 'UserController@update_nationality');
        /* Get All User's Friends APi */
        Route::get('/friends/{user_id}/{session_token}', 'UserController@friends');
        /* Delete User's Friend Api */
        Route::delete('/friends/{user_id}/{session_token}/{friends_id}', 'UserController@delete_friends');
        /* Change Profile Picture Api */
        Route::post('/profilepicture/{user_id}/{session_token}', 'UserController@update_profile_image');
        /* Block List Api */
        Route::get('/blocklist/{user_id}/{session_token}', 'UserController@block_list');
        /* UnBlock Friend List Api */
        Route::post('/unblock', 'UserController@unblock_friend');
        /* Show Hidden Content Api */
        Route::get('/hiddencontent/{user_id}/{session_token}', 'UserController@hidden_content');
        /* Update Online Status Api */
        Route::post('/onlinestatus', 'UserController@change_online_status');
        /* Unhide A Content Api */
        Route::post('/unhidecontent', 'UserController@unhide_content');
        /* Deactivate Account Api */
        Route::post('/deactivate', 'UserController@deactivate');
        /* Update Contact Privacy Api */
        Route::post('/contact_privacy', 'UserController@update_contact_privacy');
        /* Update Contact Privacy Api */
        Route::post('/notification_settings', 'UserController@update_notification_settings');
        /* Tagging Friends Api */
        Route::get('/tag/{user_id}/{session_token}/{query}', 'UserController@tag');
        /* Send Friend Request Api */
        Route::post('/friend_request', 'UserController@friend_request');
        /* Display Friend Request Api */
        Route::get('/my_friend_requests/{user_id}/{session_token}', 'UserController@my_friend_requests');
        /* Accept Friend Request Api */
        Route::post('/accept_friend_request', 'UserController@accept_friend_request');
        /* Block User Api */
        Route::post('/block', 'UserController@block_user');

        Route::post('/warn', 'UserController@warn_user')->name('user.warn');
        /* Show Profile Picture Api */
        Route::get('/profilepicture/{user_id}/{session_token}', 'UserController@show_profile_picture');
        /* Add to favourites Api */
        Route::post('/add_favourites', 'UserController@add_favourites');
        /* Remove Favourties Api */
        Route::post('/remove_favourites', 'UserController@remove_favourites');
        /* Show Favourties Api */
        Route::get('/favourites/{user_id}/{session_token}', 'UserController@show_favourites');
    });

    /*
     * Medias Manager
     */
    Route::group([
        'prefix' => 'medias',
        'as' => 'media.',
        'namespace' => 'Media',
            ], function () {

        Route::middleware(['jwt.auth'])->group(function () {
            /* Create Media Api */
            Route::post('/create', 'MediasController@create');
            /* Add Comment On Media Api */
            Route::post('/{media_id}/comment', 'MediasController@comment');
            /* Liking Media Api */
            Route::post('/{media_id}/like', 'MediasController@like');
            
            
            Route::post('/likeunlike', 'MediasController@likeunlike');
            
            /* Unliking Media Api */
            Route::post('/unlike', 'MediasController@unlike');
            /* Sharing Media Api */
            Route::post('/{media_id}/share', 'MediasController@share');
            /* Deleting Media Api */
            Route::post('/delete', 'MediasController@delete');
            /* Hiding Media Api */
            Route::post('/{media_id}/hide', 'MediasController@hide');
            /* Reporting Media Api */
            Route::post('/{media_id}/report', 'MediasController@report');
            /* Display Media Activities Api */
            Route::post('/activity', 'MediasController@activity');
            /* Updating Media Description Api */
            Route::post('/description', 'MediasController@update_description');
            /* List User's Media Api */
            Route::get('/listbyuser/{user_id}/{session_token}/{media_user_id}', 'MediasController@listbyuser');
            /* Display Media reactions */
            Route::post('/{media_id}/reactions', 'MediasController@getMediaReactions');
            /* Liking Media Comment Api */
            Route::post('/{media_id}/{comment_id}/like', 'MediasController@commentLike');
        });
    });

    /*
     * Medias Manager
     */
    Route::group([
        'prefix' => 'pages',
        'as' => 'page.',
        'namespace' => 'Page',
            ], function () {

        /* Create a Page Api */
        Route::post('/create', 'PagesController@create');
        /* Add Page Admin Api */
        Route::post('/add_admin', 'PagesController@add_admin');
        /* Remove Page Admin Api */
        Route::post('/remove_admin', 'PagesController@remove_admin');
        /* Deactivate Page Api */
        Route::post('/deactivate', 'PagesController@deactivate');
        /* Page Notification Settings Api */
        Route::post('/notification_settings', 'PagesController@notification_settings');
    });

    /*
     * Embassy Manager
     */
    Route::group([
        'prefix' => 'embassy',
        'as' => 'embassy.',
        'namespace' => 'Embassies',
            ], function () {

        /* Show Embassies */
        Route::get('/{user_id}/{session_token}/{country_id}/{embassy_id?}', 'EmbassyController@show_embassies');
    });

    /*
     * Country Manager
     */
    Route::group([
        'prefix' => 'countries',
        'as' => 'countries.',
        'namespace' => 'Country',
            ], function () {

        
        /* Show Country */
        Route::get('/', 'CountryController@show');
        /* Get All Countries */
        Route::post('/search', 'CountryController@get_countries');
        /* Show Country information */
        Route::get('/{country_id}', 'CountryController@getCountryInformation');
        /* Get Count of Followers */
        Route::get('/{country_id}/num_followers', 'CountryController@getCountryNumberFollowers');
        /* Get Country Statistics */
        Route::get('/{country_id}/stats', 'CountryController@getCountryStats');
        /* Get Country’s Cities */
        Route::get('/{country_id}/cities', 'CountryController@getCountryCities');
        /* Get Country’s Media */
        Route::get('/{country_id}/media', 'CountryController@getCountryMedia');
        /* Get Country’s TripPlans */
        Route::get('/{country_id}/plans', 'CountryController@getCountryTripPlans');
        /* Get Country’s Places */
        Route::get('/{country_id}/places', 'CountryController@getCountryPlaces');
        /* Get Country’s Discussions */
        Route::get('/{country_id}/discussions', 'CountryController@getCountryDiscussions');
        
        

        Route::middleware(['jwt.auth'])->group(function () {
            /* Follow country */
            Route::post('/{country_id}/follow', 'CountryController@postFollow');
            /* Unfollow country */
            Route::post('/{country_id}/unfollow', 'CountryController@postUnFollow');
            /* Checkfollow country */
            Route::post('/{country_id}/checkfollow', 'CountryController@checkFollow');
            /* Share country */
            Route::post('/{country_id}/share', 'CountryController@postShare');
        });
    });

    /*
    * Cities
    */
    Route::group([
        'prefix' => 'cities',
        'as' => 'cities.',
        'namespace' => 'City',
    ], function () {

        /* Show City */
        Route::get('/', 'CityController@show');
        /* Get All Cities */
        Route::post('/search', 'CityController@postSearch');
        /* Show City information */
        Route::get('/{city_id}', 'CityController@getCityInformation');
        /* Get Count of Followers */
        Route::get('/{city_id}/num_followers', 'CityController@getCityNumberFollowers');
        /* Get City’s Stats */
        Route::get('/{city_id}/stats', 'CityController@getCityStats');
        /* Get City’s Media */
        Route::get('/{city_id}/media', 'CityController@getCityMedia');
        /* Get City’s TripPlans */
        Route::get('/{city_id}/plans', 'CityController@getCityTripPlans');
        /* Get City’s Places */
        Route::get('/{city_id}/places', 'CityController@getCityPlaces');
        /* Get City’s Discussions */
        Route::get('/{city_id}/discussions', 'CityController@getCityDiscussions');
        /* Get City’s Weather */
        Route::get('/{city_id}/weather', 'CityController@getCityWeather');

        Route::middleware(['jwt.auth'])->group(function () {
            /* Follow city */
            Route::post('/{city_id}/follow', 'CityController@postFollow');
            /* Unfollow city */
            Route::post('/{city_id}/unfollow', 'CityController@postUnFollow');
            /* Checkfollow city */
            Route::post('/{city_id}/checkfollow', 'CityController@checkFollow');
            /* Share city */
            Route::post('/{city_id}/share', 'CityController@postShare');
        });

    });

    /*
     * Place Manager
     */
    Route::group([
        'prefix' => 'places',
        'as' => 'places.',
        'namespace' => 'Place',
            ], function () {

        /* Get All Places */
        Route::post('/search', 'PlaceController@get_places');
        /* Show Places information */
        Route::get('/{place_id}', 'PlaceController@showPlaceInformation');
        /* Get Count of Followers */
        Route::get('/{place_id}/num_followers', 'PlaceController@getNumFollowers');
        /* Get Places Stats */
        Route::get('/{place_id}/stats', 'PlaceController@getPlacesStatistics');
        /* Get Places Media */
        Route::get('/{place_id}/media', 'PlaceController@getPlacesMedia');
        /* Get Places TripPlans */
        Route::get('/{place_id}/plans', 'PlaceController@getPlacesPlans');
        /* Get Places Discussions */
        Route::get('/{place_id}/discussions', 'PlaceController@getPlacesDiscussions');
        /* Get Places Reviews */
        Route::get('/{place_id}/reviews', 'PlaceController@getReviews');
        /* Get Places Nearby */
        Route::post('/{place_id}/nearby', 'PlaceController@getNearby');
        /* Get Places Followers */
        Route::post('/{place_id}/followers', 'PlaceController@getFollowers');

        Route::middleware(['jwt.auth'])->group(function () {
            /* Follow Places */
            Route::post('/{place_id}/follow', 'PlaceController@followPlace');
            /* Unfollow Places */
            Route::post('/{place_id}/unfollow', 'PlaceController@postUnFollow');
            /* Checkfollow Places */
            Route::post('/{place_id}/checkfollow', 'PlaceController@checkFollow');
            /* Share Places */
            Route::post('/{place_id}/share', 'PlaceController@postShare');
            /* Review Places */
            Route::post('/{place_id}/review', 'PlaceController@postReview');
        });
    });

    /*
     * Lifestyle Manager
     */
    Route::group([
        'prefix' => 'travelstyles',
        'as' => 'travelstyles.',
        'namespace' => 'LifeStyle',
            ], function () {

        /* Get All Lifestyles */
        Route::post('/search', 'LifestyleController@get_lifestyles');
    });
    
    /*
     * Trip Planner
     */
    Route::group([
        'prefix' => 'trips',
        'as' => 'trips.',
        'namespace' => 'Trip',
            ], function () {
        Route::post('/{trip_id}/add_city', 'TripController@postAddCity'); //done
        Route::post('/{trip_id}/add_place', 'TripController@postAddPlace');  //done

        Route::middleware(['jwt.auth'])->group(function () {
            Route::post('/new', 'TripController@postNew');
            Route::get('/{trip_id}/cities', 'TripController@getCities');
            Route::get('/{trip_id}/places', 'TripController@getPlaces');
            Route::post('/{trip_id}/finish_place', 'TripController@postFinishPlace');
            Route::post('/{trip_id}/cancel', 'TripController@postCancel');
            Route::post('/{trip_id}/finish_city', 'TripController@postFinishCity');
            Route::post('/{trip_id}/publish', 'TripController@postPublish');
            Route::post('/{trip_id}/remove_place', 'TripController@postRemovePlace');
            Route::post('/{trip_id}/remove_city', 'TripController@postRemoveCity');
            Route::post('/{trip_id}/places_order', 'TripController@postPlaceOrder');
            Route::post('/{trip_id}/share', 'TripController@postShare');
        });
    });
});
