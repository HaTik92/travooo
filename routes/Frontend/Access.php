<?php

/**
 * Frontend Access Controllers
 * All route names are prefixed with 'frontend.auth'.
 */

Route::get('user/logout', '\App\Http\Controllers\Auth\LoginController@log_out')->name('logout');

Route::group(['namespace' => 'Auth', 'as' => 'auth.'], function () {

    /*
     * These routes require the user to be logged in
     */
    Route::group(['middleware' => 'guest:user'], function () {

        //For when admin is logged in as user from backend
        Route::get('logout-as', 'UserLoginController@logoutAs')->name('logout-as');

        // Change Password Routes
        Route::patch('password/change', 'ChangePasswordController@changePassword')->name('password.change');

    });

    //Add Posts Routes
    Route::post('new-post', '\App\Http\Controllers\Frontend\PostController@createPost')->name('new-post');
    Route::post('my-location', '\App\Http\Controllers\Frontend\PostController@location')->name('my-location');
    Route::post('tmpupload', '\App\Http\Controllers\Frontend\PostController@postTmpUpload')->name('tmpupload');
    Route::post('tmpdelete', '\App\Http\Controllers\Frontend\PostController@uploadTmpDelete')->name('tmpdelete');
    Route::delete('delete-uploaded-temp-files', '\App\Http\Controllers\Frontend\PostController@deleteUploadedTempFiles');


    //delete post
    Route::post('delete-post', '\App\Http\Controllers\Frontend\PostController@deletePost')->name('delete-post');
    //get shareable content
    Route::get('get-shareable-post', '\App\Http\Controllers\Frontend\PostController@getSharePost')->name('get-shareable-post');

    //share a post
    Route::post('share-post', '\App\Http\Controllers\Frontend\PostController@sharePost')->name('share-post');
    Route::get('create-dummy-post','\App\Http\Controllers\Frontend\PostController@createDummyPost');



    /*
     * These routes require no user to be logged in
     */
    Route::group(['middleware' => 'guest:user'], function () {
        // Authentication Routes
        Route::get('login', function() {
            return view('frontend.index');
        })->name('user.login');
        Route::get('login/login', function() {
            return view('frontend.index');
        });
        Route::post('login', '\App\Http\Controllers\Auth\LoginController@log_in');
        Route::post('create/account/', '\App\Http\Controllers\Auth\RegisterController@registerstep1');
        Route::post('create/account/step2', '\App\Http\Controllers\Auth\RegisterController@registerstep2');
        Route::post('login/login', 'LoginController@login')->name('login.post');

        // Socialite Routes
        //Route::get('login/{provider}', 'SocialLoginController@login')->name('social.login');

        Route::get('facebook/redirect', '\App\Http\Controllers\Auth\SocialAuthFacebookController@redirect');
        Route::get('facebook/callback', '\App\Http\Controllers\Auth\SocialAuthFacebookController@callback');

        Route::get('twitter/redirect', '\App\Http\Controllers\Auth\SocialAuthTwitterController@redirect');
        Route::get('twitter/callback', '\App\Http\Controllers\Auth\SocialAuthTwitterController@callback');

        Route::get('social/{provider}', '\App\Http\Controllers\Auth\SocialAuthGoogleController@redirectToProvider');
        Route::get('social/callback/{provider}', '\App\Http\Controllers\Auth\SocialAuthGoogleController@handleProviderCallback');

        // Registration Routes
        if (config('access.users.registration')) {
            Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
            Route::post('register', 'RegisterController@register')->name('register.post');
        }

        // Confirm Account Routes
        Route::get('account/confirm/{token}', 'ConfirmAccountController@confirm')->name('account.confirm');
        Route::get('account/confirm/resend/{user}', 'ConfirmAccountController@sendConfirmationEmail')->name('account.confirm.resend');

        // Password Reset Routes
        Route::get('password/email', '\App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
        Route::post('password/email', '\App\Http\Controllers\Auth\ForgotPasswordController@send_ResetLinkEmail')->name('password.email.post');

        Route::get('password/reset/{token}{email?}', '\App\Http\Controllers\Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
        Route::post('password/reset', '\App\Http\Controllers\Auth\ResetPasswordController@reset_pass')->name('password.update');


    });

    //SIGN UP
    Route::group(['middleware' => 'guest:user'], function () {
          Route::get('signup', function() {
            return view('frontend.signup');
        })->name('user.signup');
        Route::post('registration/step-1', '\App\Http\Controllers\Auth\RegisterController@registrationStepOne')->name('registration.step.1');
        Route::post('registration/check-username', '\App\Http\Controllers\Auth\RegisterController@registrationCheckUsername')->name('registration.check.username');
        Route::group(['middleware' => 'registration_steps'], function () {
            Route::post('registration/resend-confirmation-code', '\App\Http\Controllers\Auth\RegisterController@resendConfirmationCode')->name('registration.resend_confirmation_code');
            Route::post('registration/step-2', '\App\Http\Controllers\Auth\RegisterController@registrationStepTwo')->name('registration.step.2');
            Route::post('registration/step-confirm', '\App\Http\Controllers\Auth\RegisterController@registrationStepConfirmationEmail')->name('registration.step.confirmation');
            Route::post('registration/step-3', '\App\Http\Controllers\Auth\RegisterController@registrationStepThree')->name('registration.step.3');
            Route::post('registration/step-4', '\App\Http\Controllers\Auth\RegisterController@registrationStepFour')->name('registration.step.4');
            Route::post('registration/step-4-expert', '\App\Http\Controllers\Auth\RegisterController@registrationStepFourExpert')->name('registration.step.4.expert');
            Route::post('registration/step-5', '\App\Http\Controllers\Auth\RegisterController@registrationStepFive')->name('registration.step.5');
            Route::post('registration/step-5-expert', '\App\Http\Controllers\Auth\RegisterController@registrationStepFiveExpert')->name('registration.step.5.expert');
            Route::post('registration/step-6', '\App\Http\Controllers\Auth\RegisterController@registrationStepSix')->name('registration.step.6');
            Route::post('registration/step-6-expert', '\App\Http\Controllers\Auth\RegisterController@registrationStepSixExpert')->name('registration.step.6.expert');
            Route::post('registration/step-final', '\App\Http\Controllers\Auth\RegisterController@registrationStepFinal')->name('registration.step.final');
            Route::post('registration/step-8-expert', '\App\Http\Controllers\Auth\RegisterController@registrationStepEightExpert')->name('registration.step.8.expert');
            Route::post('registration/step-9', '\App\Http\Controllers\Auth\RegisterController@registrationStepNine')->name('registration.step.9');
            Route::post('registration/step-10', '\App\Http\Controllers\Auth\RegisterController@registrationStepTen')->name('registration.step.10');
            Route::post('registration/update-step', '\App\Http\Controllers\Auth\RegisterController@registrationUpdateSteps')->name('registration.update.step');
        });
    });

    Route::get('registration/step-3', '\App\Http\Controllers\Auth\RegisterController@searchCitiesOrCountries')->name('registration.step.get.3');
    Route::get('registration/step-4', '\App\Http\Controllers\Auth\RegisterController@searchPlaces')->name('registration.step.get.4');
    Route::get('registration/step-5', '\App\Http\Controllers\Auth\RegisterController@getInterestSuggestion')->name('registration.step.get.5');
    Route::get('registration/step-7', '\App\Http\Controllers\Auth\RegisterController@getLanguageSuggestion')->name('registration.step.get.7');
    Route::get('registration/current-languages', '\App\Http\Controllers\Auth\RegisterController@getCurrentLanguage')->name('registration.get.current.languages');
    Route::get('registration/step-5-expert', '\App\Http\Controllers\Auth\RegisterController@searchAreaExpertise')->name('registration.step.5.expert');
    Route::get('registration/step-6', '\App\Http\Controllers\Auth\RegisterController@searchTravelStyles')->name('registration.step.get.6');
});
