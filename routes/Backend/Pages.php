<?php
/*
 * Pages Manager
 **/
Route::group(['namespace' => 'Pages'], function () {
    /*
     * For DataTables
     */
    Route::post('pages/table', ['uses' => 'PagesController@table'])->name('pages.table');

    /*
     * get medias
     */
    Route::get('pages/medias', ['uses' => 'PagesController@medias'])->name('pages.medias');

    /*
     * Pages CRUD
     */
    Route::resource('pages', 'PagesController');

    /*
     * Enable/Disable Specific Page
     */
    Route::group(['prefix' => 'pages/{pages}'], function () {
        Route::get('mark/{status}', 'PagesController@mark')->name('pages.mark')->where(['status' => '[1,2]']);
    });
});

/*
 * Pages Categories Manager
 **/
Route::group(['namespace' => 'PagesCategories'], function () {
    /*
     * For DataTables
     */
    Route::post('pages_categories/table', ['uses' => 'PagesCategoriesController@table'])->name('pages_categories.table');

    /*
     * PagesCategories CRUD
     */
    Route::resource('pages_categories', 'PagesCategoriesController');

    /*
     * Enable/Disable Specific PageCategory
     */
    Route::group(['prefix' => 'pages_categories/{pages_categories}'], function () {
        Route::get('mark/{status}', 'PagesCategoriesController@mark')->name('pages_categories.mark')->where(['status' => '[1,2]']);
    });
});
