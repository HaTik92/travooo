<?php
 /*
 * Safety Degrees Manager
 */
Route::group([
    'prefix'     => 'safety-degrees',
    'as'         => 'safety-degrees.',
    'namespace'  => 'SafetyDegrees',
], function () {


	/*
     * For DataTables
     */
    Route::post('safety/table', ['uses' => 'SafetyDegreesController@table'])->name('safety.table');

    /*
     * Safety Degrees CRUD
     */
    Route::resource('safety', 'SafetyDegreesController');
});