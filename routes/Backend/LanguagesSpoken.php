<?php
	/*
     * Languages Spoken Manager
     **/
    Route::group(['namespace' => 'LanguagesSpoken'], function () {
        /*
         * For DataTables
         */
        Route::post('languagesspoken/table', ['uses' => 'LanguagesSpokenController@table'])->name('languagesspoken.table');

        /*
         * Languages Spoken CRUD
         */
        Route::resource('languagesspoken', 'LanguagesSpokenController');

        /*
         * Enable/Disable Specific Languages Spoken
         */
        Route::group(['prefix' => 'languagesspoken/{languagesspoken}'], function () {
            Route::get('mark/{status}', 'LanguagesSpokenController@mark')->name('languagesspoken.mark')->where(['status' => '[1,2]']);
        });
}); 