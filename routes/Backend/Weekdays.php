<?php
 /*
 * Weekdays Manager
 */
Route::group(['namespace' => 'Weekdays'], function () {
    /*
     * For DataTables
     */
    Route::post('weekdays/table', ['uses' => 'WeekdaysController@table'])->name('weekdays.table');

    /*
     * Weekdays CRUD
     */
    Route::resource('weekdays', 'WeekdaysController');
});