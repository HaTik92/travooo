<?php
/*
 * Interest Manager
 **/
Route::group(['namespace' => 'Interest'], function () {
    /*
     * For DataTables
     */
    Route::post('interest/table', ['uses' => 'InterestController@table'])->name('interest.table');

    /*
     * Interest CRUD
     */
    Route::resource('interest', 'InterestController');

    /*
     * Enable/Disable Specific Interest
     */
    Route::group(['prefix' => 'interest/{interest}'], function () {
        Route::get('mark/{status}', 'InterestController@mark')->name('interest.mark')->where(['status' => '[1,2]']);
    });
}); 