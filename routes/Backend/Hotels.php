<?php
/*
 * Hotels Manager
 **/
Route::group(['namespace' => 'Hotels'], function () {
    /*
     * For DataTables
     */
    Route::post('hotels/table', ['uses' => 'HotelsController@table'])->name('hotels.table');
    Route::get('hotels/import', 'HotelsController@import')->name('hotels.import');

    Route::get('hotels/countries/{term?}/{type?}/{q?}', 'HotelsController@getAddedCountries')->name('hotels.countries');
    Route::get('hotels/cities/{term?}/{type?}/{q?}', 'HotelsController@getAddedCities')->name('hotels.cities');
    Route::get('hotels/types/{term?}/{type?}/{q?}', 'HotelsController@getPlaceTypes')->name('hotels.types');

    Route::post('hotels/search', 'HotelsController@search')->name('hotels.search');
    Route::get('hotels/search/{admin_logs_id?}/{country_id?}/{city_id?}/{latlng?}', 'HotelsController@search')->name('hotels.search');
    Route::post('hotels/savesearch', 'HotelsController@savesearch')->name('hotels.savesearch');
    Route::get('hotels/return_search_history', 'HotelsController@return_search_history')
            ->name('hotels.searchhistory');


    Route::get('hotels/create-test', 'HotelsController@create')->name('hotels.test');

    /*
     * Hotels CRUD
     */
    Route::resource('hotels', 'HotelsController');

    Route::post('hotels/delete-ajax', 'HotelsController@delete_ajax')->name('hotels.delete_ajax');

     /*
     * Enable/Disable Specific Hotels
     */
    Route::group(['prefix' => 'hotels/{hotels}'], function () {
        Route::get('mark/{status}', 'HotelsController@mark')->name('hotels.mark')->where(['status' => '[1,2]']);
    });
});