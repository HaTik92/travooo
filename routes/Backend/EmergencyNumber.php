<?php
/*
* Emergency Numbers Manager
*/
Route::group(
    [
        'namespace' => 'EmergencyNumbers'
    ],
    function () {
        /*
         * For DataTables
         */
        Route::post('emergencynumbers/table', ['uses' => 'EmergencyNumbersController@table'])->name('emergencynumbers.table');

        /*
         * EmergencyNumbers CRUD
         */
        Route::resource('emergencynumbers', 'EmergencyNumbersController');

    }
);