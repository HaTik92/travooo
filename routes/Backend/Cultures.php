<?php
/*
 * Cultures Manager
 **/
Route::group(['namespace' => 'Cultures'], function () {
    /*
     * For DataTables
     */
    Route::post('cultures/table', ['uses' => 'CulturesController@table'])->name('cultures.table');

    /*
     * Cultures CRUD
     */
    Route::resource('cultures', 'CulturesController');
});
