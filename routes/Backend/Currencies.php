<?php
 /*
 * Currencies Manager
 **/
Route::group(['namespace' => 'Currencies'], function () {
    /*
     * For DataTables
     */
    Route::post('currencies/table', ['uses' => 'CurrenciesController@table'])->name('currencies.table');

    /*
     * Currencies CRUD
     */
    Route::resource('currencies', 'CurrenciesController');

     /*
     * Enable/Disable Specific Currency
     */
    Route::group(['prefix' => 'currencies/{currencies}'], function () {
        Route::get('mark/{status}', 'CurrenciesController@mark')->name('currencies.mark')->where(['status' => '[1,2]']);
    });
});
