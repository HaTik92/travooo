<?php
/*
 * Spam Manager
 **/
Route::group(['namespace' => 'TopPlaces', 'prefix' => 'top-places'], function () {
    /*
     * Top places -> Places
     * For DataTables
     */
    Route::post('places/table', ['uses' => 'TopPlacesController@table'])->name('top_places.table');
    Route::post('places/delete-ajax', 'TopPlacesController@unmarkSelectedTopPlace')->name('top_places.delete_ajax');
    Route::delete('places/unmark-top-place', 'TopPlacesController@unmarkTopPlace')->name('top_places.unmark_top_place');
    Route::get('places/places-by-city', 'TopPlacesController@getPlacesByCity')->name('top_places.get_places_by_city');
    Route::get('places/countries/{term?}/{type?}/{q?}', 'TopPlacesController@getAddedCountries')->name('top_places.countries');
    Route::get('places/cities/{term?}/{type?}/{q?}', 'TopPlacesController@getAddedCities')->name('top_places.cities');
    Route::get('places/types/{term?}/{type?}/{q?}', 'TopPlacesController@getPlaceTypes')->name('top_places.types');
    Route::post('/update-sort', 'TopPlacesController@updateSort')->name('top_places.update_sort');

    /*
     * For CRUD
     */
    Route::resource('places', 'TopPlacesController');


    /*
     * Top places -> Hotels
     * For DataTables
     */
    Route::post('hotels/table', ['uses' => 'TopHotelsController@table'])->name('top_hotels.table');
    Route::post('hotels/delete-ajax', 'TopHotelsController@unmarkSelectedTopHotels')->name('top_hotels.delete_ajax');
    Route::delete('hotels/unmark-top-hotel', 'TopHotelsController@unmarkTopHotel')->name('top_hotels.unmark_top_hotel');
    Route::get('hotels/hotel-by-city', 'TopHotelsController@getHotelsByCity')->name('top_hotels.get_hotels_by_city');
    Route::get('hotels/countries/{term?}/{type?}/{q?}', 'TopHotelsController@getAddedCountries')->name('top_hotels.countries');
    Route::get('hotels/cities/{term?}/{type?}/{q?}', 'TopHotelsController@getAddedCities')->name('top_hotels.cities');
    Route::post('/update-hotel-sort', 'TopHotelsController@updateSort')->name('top_hotels.update_sort');

    /*
     * For CRUD
     */
    Route::resource('hotels', 'TopHotelsController', ['names' => [
        'index' => 'top_hotels.index',
        'create' => 'top_hotels.create',
        'store' => 'top_hotels.store'
    ]]);



    /*
     * Top places -> Restaurants
     * For DataTables
     */
    Route::post('restaurants/table', ['uses' => 'TopRestaurantsController@table'])->name('top_restaurants.table');
    Route::post('restaurants/delete-ajax', 'TopRestaurantsController@unmarkSelectedTopRestaurants')->name('top_restaurants.delete_ajax');
    Route::delete('restaurants/unmark-top-restaurant', 'TopRestaurantsController@unmarkTopRestaurants')->name('top_restaurants.unmark_top_restaurant');
    Route::get('restaurants/restaurants-by-city', 'TopRestaurantsController@getRestaurantsByCity')->name('top_restaurants.get_restaurants_by_city');
    Route::get('restaurants/countries/{term?}/{type?}/{q?}', 'TopRestaurantsController@getAddedCountries')->name('top_restaurants.countries');
    Route::get('restaurants/cities', 'TopRestaurantsController@getAddedCities')->name('top_restaurants.cities');
    Route::post('/update-restaurant-sort', 'TopRestaurantsController@updateSort')->name('top_restaurants.update_sort');

    /*
     * For CRUD
     */
    Route::resource('restaurants', 'TopRestaurantsController');
});
