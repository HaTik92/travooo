<?php
/*
 * Accommodations Manager
 **/
Route::group(['namespace' => 'Accommodations'], function () {
    /*
     * For DataTables
     */
    Route::post('accommodations/table', ['uses' => 'AccommodationsController@table'])->name('accommodations.table');

    /*
     * Accommodations CRUD
     */
    Route::resource('accommodations', 'AccommodationsController');

    /*
     * Enable/Disable Specific Accommodation
     */
    Route::group(['prefix' => 'accommodations/{accommodation}'], function () {
        Route::get('mark/{status}', 'AccommodationsController@mark')->name('accommodations.mark')->where(['status' => '[1,2]']);
    });
}); 