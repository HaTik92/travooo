<?php
/*
 * Pages Manager
 **/
Route::group(['namespace' => 'HelpCenter'], function () {
    /*
     * For DataTables
     */
    Route::post('help-center/menues/table', ['uses' => 'HelpCenterController@menuesTable'])->name('help-center.menues.table');
    Route::post('help-center/subMenues/table', ['uses' => 'HelpCenterController@subMenuesTable'])->name('help-center.subMenues.table');

    /*
     * Pages CRUD
     */
    Route::get('help-center/menues', 'HelpCenterController@menuesView')->name("help-center.menues");
    Route::get('help-center/menu/{id}', 'HelpCenterController@menuEditView')->where(['id' => '[0-9]+'])->name('help-center.menu.edit');
    Route::post('help-center/menu/{id}/update', 'HelpCenterController@menuUpdate')->where(['id' => '[0-9]+'])->name('help-center.menu.update');
    
    Route::get('help-center/menu/{id}/delete', 'HelpCenterController@deleteMenu')->where(['id' => '[0-9]+'])->name('help-center.menu.delete');


    Route::get('help-center/sub-menues', 'HelpCenterController@subMenuesView')->name("help-center.subMenues");
    Route::get('help-center/sub-menu/{id}', 'HelpCenterController@subMenuEditView')->where(['id' => '[0-9]+'])->name('help-center.subMenu.edit');
    Route::post('help-center/sub-menu/{id}/update', 'HelpCenterController@subMenuUpdate')->where(['id' => '[0-9]+'])->name('help-center.subMenu.update');

    Route::get('help-center/sub-menu/{id}/delete', 'HelpCenterController@deleteSubMenu')->where(['id' => '[0-9]+'])->name('help-center.subMenu.delete');

    Route::get('help-center/sub-menu/new', 'HelpCenterController@newSubMenu')->name('help-center.subMenu.create');
    Route::post('help-center/sub-menu/new/{type}', 'HelpCenterController@newSubMenuContent')->name('help-center.subMenu.new.content');

    Route::delete('help-center/sub-menu/content/{id}/delete', 'HelpCenterController@deleteSubMenuContent')->where(['id' => '[0-9]+'])->name('help-center.subMenu.content.delete');
    
    Route::post('help-center/sub-menu/content/{id}/media_upload', 'HelpCenterController@mediaUploadSubMenuContent')->where(['id' => '[0-9]+'])->name('help-center.subMenu.content.mediaUpload');

});