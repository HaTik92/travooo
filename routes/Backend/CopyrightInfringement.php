<?php
/*
 * Spam Manager
 **/
Route::group(['namespace' => 'CopyrightInfringement'], function () {
    /*
     * For DataTables
     */
    Route::post('copyright-infringement/table', ['uses' => 'CopyrightInfringementController@table'])->name('copyright_infringement.table');
    Route::post('copyright-infringement/view', ['uses' => 'CopyrightInfringementController@view'])->name('copyright_infringement.view');
    Route::post('copyright-infringement/reply', ['uses' => 'CopyrightInfringementController@reply'])->name('copyright_infringement.reply');
    Route::post('copyright-infringement/delete', ['uses' => 'CopyrightInfringementController@delete'])->name('copyright_infringement.delete');
    Route::post('copyright-infringement/confirm', ['uses' => 'CopyrightInfringementController@confirm'])->name('copyright_infringement.confirm');
    Route::get('copyright-infringement/users', ['uses' => 'CopyrightInfringementController@getSuspendedAccounts'])->name('copyright_infringement.users');
    Route::post('copyright-infringement/users-table', ['uses' => 'CopyrightInfringementController@getSuspendedAccountsTable'])->name('copyright_infringement.users_table');
    Route::post('copyright-infringement/restore-post', ['uses' => 'CopyrightInfringementController@restorePost'])->name('copyright_infringement.restore-post');
    Route::get('copyright-infringement/history', ['uses' => 'CopyrightInfringementController@getHistory'])->name('copyright_infringement.history');
    Route::post('copyright-infringement/history-table', ['uses' => 'CopyrightInfringementController@getHistoryTable'])->name('copyright_infringement.history_table');

    /*
     * For CRUD
     */
    Route::resource('copyright-infringement', 'CopyrightInfringementController');
});