<?php
 /*
 * Activities Manager
 */
Route::group([
    'prefix'     => 'activities',
    'as'         => 'activities.',
], function () {

	/*
     * Activity Types Manager
     **/
    Route::group(['namespace' => 'ActivityTypes'], function () {
        /*
         * For DataTables
         */
        Route::post('activitytypes/table', ['uses' => 'ActivityTypesController@table'])->name('activitytypes.table');

        /*
         * Activity Types CRUD
         */
        Route::resource('activitytypes', 'ActivityTypesController');

        /*
         * Enable/Disable Specific Activity Types
         */
        Route::group(['prefix' => 'activitytypes/{activitytypes}'], function () {
            Route::get('mark/{status}', 'ActivityTypesController@mark')->name('activitytypes.mark')->where(['status' => '[1,2]']);
        });

    });

    /*
     * Activity Manager
     **/
    Route::group(['namespace' => 'Activity'], function () {
        /*
         * For DataTables
         */
        Route::post('activity/table', ['uses' => 'ActivityController@table'])->name('activity.table');

        /*
         * Activity CRUD
         */
        Route::resource('activity', 'ActivityController');

        /*
         * Enable/Disable Specific Activity
         */
        Route::group(['prefix' => 'activity/{activity}'], function () {
            Route::get('mark/{status}', 'ActivityController@mark')->name('activity.mark')->where(['status' => '[1,2]']);
        });

    });

}); 