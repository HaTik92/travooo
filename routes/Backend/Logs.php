<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Logs'], function () {
    Route::group(['prefix' => 'log-viewer'], function () {
        Route::get('rank-cron', ['uses' => 'LogsReaderController@index'])->name('logs.rank-cron');
        Route::post('table', ['uses' => 'LogsReaderController@table'])->name('logs.rank-cron-table');
        Route::post('table/truncate', ['uses' => 'LogsReaderController@truncateRankCronTable'])->name('logs.rank-cron-table-truncate');
    });
});
