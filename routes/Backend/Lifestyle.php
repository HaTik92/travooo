<?php
/*
 * Lifestyle Manager
 **/
Route::group(['namespace' => 'Lifestyle'], function () {
    /*
     * For DataTables
     */
    Route::post('lifestyle/table', ['uses' => 'LifestyleController@table'])->name('lifestyle.table');

    /*
     * Lifestyle CRUD
     */
    Route::resource('lifestyle', 'LifestyleController');
}); 