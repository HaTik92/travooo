<?php
/*
 * Pages Manager
 **/
Route::group(['namespace' => 'Submissions'], function () {
    /*
     * For DataTables
     */
    Route::post('submissions/table', ['uses' => 'SubmissionsController@table'])->name('submissions.table');

    /*
     * Pages CRUD
     */
    Route::get('submissions', 'SubmissionsController@index')->name("submissions.index");

    // Route::get('submissions/index', 'SubmissionsController@index');
    // Route::get('submissions/approved', 'SubmissionController@getApprovedSubmissions')->name("submissions.approved");
    // Route::get('submissions/disapproved', 'SubmissionController@getDisapprovedSubmissions')->name("submissions.disapproved");

    Route::get('submission/approve/{id}', 'SubmissionsController@approve')->name('submission.approve');
    Route::get('submission/disapprove/{id}', 'SubmissionsController@disapprove')->name('submission.disapprove');

    Route::group(['namespace' => 'Approved'], function () {
        Route::get('submissions/approved', 'ApprovedController@index')->name("submissions.approved");

        Route::post('approved/submissions/table', ['uses' => 'ApprovedController@table'])->name('submissions.approved.table');
    });

    Route::group(['namespace' => 'Disapproved'], function () {
        Route::get('submissions/disapproved', 'DisapprovedController@index')->name("submissions.disapproved");

        Route::post('disapproved/submissions/table', ['uses' => 'DisapprovedController@table'])->name('submissions.disapproved.table');
    });

});