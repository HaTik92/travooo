<?php

/**
 * All route names are prefixed with 'admin.access'.
 */
Route::group([
    'prefix'     => 'experts',
    'as'         => 'experts.',
    'namespace'  => 'Expert',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsRole:1',
    ], function () {
        Route::group([], function () {
            /*
             * For DataTables
             */
            Route::post('table', ['uses' => 'ExpertsController@table'])->name('table');
            Route::get('not-approved', 'ExpertsController@notApproved')->name('not-approved');
            Route::get('approved', 'ExpertsController@approved')->name('approved');
            Route::get('special-approved', 'ExpertsController@specialCaseExpertsapproved')->name('special_approved');
            Route::get('pending-approval', 'ExpertsController@pendingApproval')->name('pending-approval');
            Route::get('invited', 'ExpertsController@invited')->name('invited');
            Route::get('applied', 'ExpertsController@applied')->name('applied');
            Route::post('approve-experts', 'ExpertsController@approveExperts')->name('approve_experts');
            Route::post('disapprove-experts', 'ExpertsController@disapproveExperts')->name('disapprove_experts');
            Route::resource('/', 'ExpertsController')->parameters(['' => 'experts']);

            Route::post('approve/{expert}', ['uses' => 'ExpertsController@approve'])->name('approve');
            Route::post('disapprove/{expert}', ['uses' => 'ExpertsController@disapprove'])->name('disapprove');

            Route::post('apply/{expert}', ['uses' => 'ExpertsController@apply'])->name('apply');
            Route::post('disapply/{expert}', ['uses' => 'ExpertsController@disapply'])->name('disapply');

            Route::post('change-badge/{expert}', ['uses' => 'ExpertsController@changeBadge'])->name('change_badge');
        });
    });
});

Route::group([
    'prefix' => 'badges-invitations',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsRole:1',
    ], function () {
        Route::group([], function () {
            Route::post('invite-links/table', ['uses' => 'Expert\InviteLinksController@table'])->name('invite-links.table');
            Route::post('badges/table', ['uses' => 'Expert\BadgesController@table'])->name('badges.table');
            Route::resource('invite-links', 'Expert\InviteLinksController');
            Route::resource('badges', 'Expert\BadgesController');
        });
    });
});

