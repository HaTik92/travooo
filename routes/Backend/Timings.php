<?php
/*
 * Timings Manager
 **/
Route::group(['namespace' => 'Timings'], function () {
    /*
     * For DataTables
     */
    Route::post('timings/table', ['uses' => 'TimingsController@table'])->name('timings.table');

    /*
     * Timings CRUD
     */
    Route::resource('timings', 'TimingsController');
}); 