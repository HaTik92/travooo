<?php
/*
 * Hobbies Manager
 **/
Route::group(['namespace' => 'Hobbies'], function () {
    /*
     * For DataTables
     */
    Route::post('hobbies/table', ['uses' => 'HobbiesController@table'])->name('hobbies.table');

    /*
     * Hobbies CRUD
     */
    Route::resource('hobbies', 'HobbiesController');
}); 