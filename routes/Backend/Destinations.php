<?php
/*
 * Spam Manager
 **/
Route::group(['namespace' => 'Destinations', 'prefix' => 'destinations'], function () {
    /*
     * For DataTables
     */
    Route::post('countries-nationality-table', ['uses' => 'CountriesNationalityController@table'])->name('destinations.countries_by_nationality_table');
    Route::post('cities-nationality-table', ['uses' => 'CitiesNationalityController@table'])->name('destinations.cities_by_nationality_table');
    Route::post('generic-top-countries-table', ['uses' => 'GenericTopCountriesController@table'])->name('destinations.generic_top_countries_table');
    Route::post('generic-top-cities-table', ['uses' => 'GenericTopCitiesController@table'])->name('destinations.generic_top_cities_table');
    Route::post('generic-top-places-table', ['uses' => 'GenericTopPlacesController@table'])->name('destinations.generic_top_places_table');

    Route::post('generic-top-cities-by-country', ['uses' => 'GenericTopCitiesController@getCitiesByCountry'])->name('destinations.generic_top_cities_by_country');
    Route::post('generic-top-places-by-country', ['uses' => 'GenericTopPlacesController@getPlacesByCountry'])->name('destinations.generic_top_places_by_country');
    Route::post('generic-top-places-by-city', ['uses' => 'GenericTopPlacesController@getPlacesByCity'])->name('destinations.generic_top_places_by_city');

    /*
     * For CRUD
     */
    Route::resource('countries-by-nationality', 'CountriesNationalityController');
    Route::resource('cities-by-nationality', 'CitiesNationalityController');
    Route::resource('generic-top-countries', 'GenericTopCountriesController');
    Route::resource('generic-top-cities', 'GenericTopCitiesController');
    Route::resource('generic-top-places', 'GenericTopPlacesController');
});
