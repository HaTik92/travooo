<?php
/*
 * Religion Manager
 **/
Route::group(['namespace' => 'Religion'], function () {
    /*
     * For DataTables
     */
    Route::post('religion/table', 'ReligionController@table')->name('religion.table');

    /*
     * Religion CRUD
     */
    Route::resource('religion', 'ReligionController');

    /*
     * Enable/Disable Specific Religion
     */
    Route::group(['prefix' => 'religion/{religion}'], function () {
        Route::get('mark/{status}', 'ReligionController@mark')->name('religion.mark')->where(['status' => '[1,2]']);
    });

}); 