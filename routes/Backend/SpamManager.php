<?php
/*
 * Spam Manager
 **/
Route::group(['namespace' => 'SpamManager'], function () {
    /*
     * For DataTables
     */
    Route::post('spam_manager/table', ['uses' => 'SpamManagerController@table'])->name('spam_manager.table');
    Route::post('spam_manager/history-table', ['uses' => 'SpamManagerController@historyTable'])->name('spam_manager.history_table');
    Route::get('spam_manager/history', ['uses' => 'SpamManagerController@history'])->name('spam_manager.history');
    Route::get('spam_manager/api-reports', ['uses' => 'SpamManagerController@getApiReports'])->name('spam_manager.api_reports');
    Route::post('spam_manager/mark-as-safe', ['uses' => 'SpamManagerController@markAsSafe'])->name('spam_manager.mark_as_safe');
    Route::post('spam_manager/remove-post', ['uses' => 'SpamManagerController@removeReportPost'])->name('spam_manager.remove_post');
    Route::post('spam_manager/remove-post-and-block-user', ['uses' => 'SpamManagerController@removeReportPostAndBlockUser'])->name('spam_manager.remove_post_and_block_user');
    Route::post('spam_manager/restore-removed-post', ['uses' => 'SpamManagerController@restoreRemovedPost'])->name('spam_manager.restore_removed_post');
    Route::post('spam_manager/api-reports-table', ['uses' => 'SpamManagerController@apiReportsTable'])->name('spam_manager.api_reports_table');
    Route::post('spam_manager/restore-api-report-post', ['uses' => 'SpamManagerController@restoreApiReportPost'])->name('spam_manager.restore_api_report_post');

    /*
     * For CRUD
     */
    Route::resource('spam_manager', 'SpamManagerController');
});