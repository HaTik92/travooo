<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Posts', 'prefix' => 'posts'], function () {
    Route::get('view', ['uses' => 'PostsController@index'])->name('posts.view');
    Route::post('search', ['uses' => 'PostsController@search'])->name('posts.search');
});
