<?php
/*
 * AgeRanges Manager
 **/
Route::group(['namespace' => 'AgeRanges'], function () {
    /*
     * For DataTables
     */
    Route::post('ageranges/table', ['uses' => 'AgeRangesController@table'])->name('ageranges.table');

    /*
     * AgeRanges CRUD
     */
    Route::resource('ageranges', 'AgeRangesController');

     /*
     * Enable/Disable Specific Age Range
     */
    Route::group(['prefix' => 'ageranges/{ageRanges}'], function () {
        Route::get('mark/{status}', 'AgeRangesController@mark')->name('ageranges.mark')->where(['status' => '[1,2]']);
    });
});