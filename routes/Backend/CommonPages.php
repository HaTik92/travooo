<?php
/*
 * Pages Manager
 **/
Route::group(['namespace' => 'CommonPages'], function () {
    Route::resource('commonpages', 'CommonPagesController');
});

Route::get('/error-load-reader', 'ErrorLogReaderController@index');
Route::get('/log/download/{file_name}', 'ErrorLogReaderController@download');
