<?php
 /*
 * Holidays Manager
 */
Route::group(
    [
        'namespace' => 'Holidays'
    ],
    function () {
        /*
         * For DataTables
         */
        Route::post('holidays/table', ['uses' => 'HolidaysController@table'])->name('holidays.table');

        /*
         * Holidays CRUD
         */
        Route::resource('holidays', 'HolidaysController');
    }
);