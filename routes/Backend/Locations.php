<?php
 /*
 * Locations Manager
 */
Route::group([
    'prefix'     => 'location',
    'as'         => 'location.',
], function () {

	/*
     * Country Manager
     **/
    Route::group(['namespace' => 'Country'], function () {
        /*
         * For DataTables
         */
        Route::post('country/table', ['uses' => 'CountryController@table'])->name('country.table');
        Route::get('country/jsoncities', 'CountryController@jsoncities')->name('country.jsoncities');

        /*
         * Country CRUD
         */
        Route::resource('country', 'CountryController');

        Route::post('country/delete-ajax', 'CountryController@delete_ajax')->name('country.delete_ajax');

        /*
         * Enable/Disable Specific Country
         */
        Route::group(['prefix' => 'country/{country}'], function () {
            Route::get('mark/{status}', 'CountryController@mark')->name('country.mark')->where(['status' => '[1,2]']);
        });

        Route::delete('country/media/{country}/{media}', 'CountryController@removeCountryMedia')->name('country.media.delete');

    });

    /*
     * Regions Manager
     **/

    Route::group(['namespace' => 'Regions'], function () {
        /*
         * For DataTables
         */
        Route::post('regions/table', ['uses' => 'RegionsController@table'])->name('regions.table');

        /*
         * User CRUD
         */
        Route::resource('regions', 'RegionsController');

        Route::post('regions/delete-ajax', 'RegionsController@delete_ajax')->name('regions.delete_ajax');

        /*
         * Specific Region
         */
        Route::group(['prefix' => 'regions/{region}'], function () {
            Route::get('mark/{status}', 'RegionsController@mark')->name('regions.mark')->where(['status' => '[1,2]']);
        });
    });

    /*
     * City Manager
     **/
    Route::group(['namespace' => 'City'], function () {
        /*
         * For DataTables
         */
        Route::post('city/table', ['uses' => 'CityController@table'])->name('city.table');

        Route::get('city/countries/{term?}/{type?}/{q?}', 'CityController@getAddedCountries')->name('city.countries');
        /*
         * City CRUD
         */
        Route::resource('city', 'CityController');

        Route::post('city/delete-ajax', 'CityController@delete_ajax')->name('city.delete_ajax');


        /*
         * Enable/Disable Specific City
         */
        Route::group(['prefix' => 'city/{city}'], function () {
            Route::get('mark/{status}', 'CityController@mark')->name('city.mark')->where(['status' => '[1,2]']);
        });
    });

    /*
     * Place Type Manager
     **/
    Route::group(['namespace' => 'PlaceTypes'], function () {
        /*
         * For DataTables
         */
        Route::post('placetypes/table', ['uses' => 'PlacetypesController@table'])->name('placetypes.table');

        /*
         * PlaceTypes CRUD
         */
        Route::resource('placetypes', 'PlaceTypesController');
    });
    /**
     * Isolated Pace Manager
     */
    Route::group(['namespace' => 'IsolatedPlaces'], function () {
        /*
         * For DataTables
         */
        Route::post('isolated/table', ['uses' => 'IsolatedPlacesController@table'])->name('isolated.table');
       
        Route::resource('isolated', 'IsolatedPlacesController');
      
    });

    /*
     * Place Manager
     **/
    Route::group(['namespace' => 'Place'], function () {
        /*
         * For DataTables
         */
        Route::post('place/table', ['uses' => 'PlaceController@table'])->name('place.table');
        Route::get('place/get-all-cities', 'PlaceController@getAllCities')->name('place.get_all_cities');
        Route::get('place/get-all-countries', 'PlaceController@getAllCountries')->name('place.get_all_countries');
        Route::post('place/change-places-to-new-city', 'PlaceController@changePlacesToNewCity')->name('place.change_places_to_new_city');

        Route::get('place/mass', ['uses'=>'PlaceController@mass'])->name('place.mass');
        Route::post('place/mass/filter', ['uses'=>'PlaceController@applyMassFilter']);
        Route::post('place/action/moveto', ['uses'=>'PlaceController@applyMoveToAction']);

        Route::get('place/countries/{term?}/{type?}/{q?}', 'PlaceController@getAddedCountries')->name('place.countries');
        Route::get('place/cities/{term?}/{type?}/{q?}', 'PlaceController@getAddedCities')->name('place.cities');
        Route::get('place/sections/{term?}/{type?}/{q?}', 'PlaceController@getAddedSections')->name('place.sections');
        Route::get('place/types/{term?}/{type?}/{q?}', 'PlaceController@getPlaceTypes')->name('place.types');

        Route::get('place/import', 'PlaceController@import')->name('place.import');
        Route::post('place/search', 'PlaceController@search')->name('place.search');
        Route::get('place/search/{admin_logs_id?}/{country_id?}/{city_id?}/{latlng?}', 'PlaceController@search')->name('place.search');
        Route::post('place/savesearch', 'PlaceController@savesearch')->name('place.savesearch');
        Route::get('place/return_search_history', 'PlaceController@return_search_history')->name('place.searchhistory');
        Route::get('place/mark-top-place', 'PlaceController@markTopPlace')->name('place.mark_top_place');
        Route::get('place/unmark-top-place', 'PlaceController@unmarkTopPlace')->name('place.unmark_top_place');

        /*
         * Place CRUD
         */
        Route::resource('place', 'PlaceController');

        /*
         * Enable/Disable Specific Place
         */
        Route::group(['prefix' => 'place/{place}'], function () {
            Route::get('mark/{status}', 'PlaceController@mark')->name('place.mark')->where(['status' => '[1,2]']);
        });

        Route::post('place/delete-ajax', 'PlaceController@delete_ajax')->name('place.delete_ajax');
    });

    /*
     * Unapproved items Manager
     **/
    Route::group(['namespace' => 'UnapprovedItem'], function () {
        /*
         * For DataTables
         */
        Route::post('unapproved-items/table', ['uses' => 'UnapprovedItemController@table'])->name('unapproved_item.table');
        Route::get('unapproved-items/approve', 'UnapprovedItemController@approve')->name('unapproved_item.approve');
        Route::post('unapproved-items/mass-approve', 'UnapprovedItemController@massApprove')->name('unapproved_item.mass_approve');
        Route::get('unapproved-items/mark-top-place', 'UnapprovedItemController@markTopPlace')->name('unapproved_item.mark_top_place');
        Route::get('unapproved-items/unmark-top-place', 'UnapprovedItemController@unmarkTopPlace')->name('unapproved_item.unmark_top_place');
        /*
         * Place CRUD
         */
        Route::resource('unapproved-items', 'UnapprovedItemController');
    });

    /*
     * Spam reports Manager
     **/
    Route::group(['namespace' => 'SpamReport'], function () {
        /*
         * For DataTables
         */
        Route::post('spam-reports/table', 'SpamReportController@table')->name('spam_reports.table');
        Route::get('spam-reports/ignore/{spamPostId}', 'SpamReportController@ignore')->name('spam_reports.ignore');
        Route::get('spam-reports/resolve/{spamPostId}', 'SpamReportController@resolve')->name('spam_reports.resolve');
        /*
         * Place CRUD
         */
        Route::resource('spam-reports', 'SpamReportController');
    });
});