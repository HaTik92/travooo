<?php
/*
 * Levels Manager
 **/
Route::group(['namespace' => 'Levels'], function () {
    /*
     * For DataTables
     */
    Route::post('levels/table', ['uses' => 'LevelsController@table'])->name('levels.table');

    /*
     * Levels CRUD
     */
    Route::resource('levels', 'LevelsController');

    /*
     * Enable/Disable Specific Levels
     */
    Route::group(['prefix' => 'levels/{levels}'], function () {
        Route::get('mark/{status}', 'LevelsController@mark')->name('levels.mark')->where(['status' => '[1,2]']);
    });
}); 