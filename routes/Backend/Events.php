<?php
/*
 * Events Manager
 **/
Route::group(['namespace' => 'Events'], function () {
    /*
     * For DataTables
     */
    Route::post('events/table', ['uses' => 'EventsController@table'])->name('events.table');
    Route::post('events/api-table', ['uses' => 'EventsController@api_table'])->name('events.api_table');
    Route::get('events/api-index', 'EventsController@api_index')->name('events.api_index');

    /*
     * Events CRUD
     */
    Route::resource('events', 'EventsController');

    Route::post('events/delete-ajax', 'EventsController@delete_ajax')->name('events.delete_ajax');

});