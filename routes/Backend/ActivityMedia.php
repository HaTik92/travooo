<?php
/*
 * Activity Media Manager
 **/
Route::group(['namespace' => 'ActivityMedia'], function () {
    /*
     * For DataTables
     */
    Route::post('activitymedia/table', ['uses' => 'ActivityMediaController@table'])->name('activitymedia.table');

    /*
     * ActivityMedia CRUD
     */
    Route::resource('activitymedia', 'ActivityMediaController');

    Route::post('activitymedia/delete-ajax', 'ActivityMediaController@delete_ajax')->name('activitymedia.delete_ajax');
}); 