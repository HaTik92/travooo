<?php

/**
 * All route names are prefixed with 'admin.access'.
 */
Route::group([
    'prefix'     => 'access',
    'as'         => 'access.',
    'namespace'  => 'Access',
], function () {

    /*
     * User Management
     */
    Route::group([
        'middleware' => 'access.routeNeedsRole:1',
    ], function () {
        Route::group(['namespace' => 'User'], function () {
            /*
             * For DataTables
             */
            Route::post('user/table', ['uses' => 'UserController@table'])->name('user.table');
            Route::get('user/logs', 'UserController@getLogs')->name('user.logs');
            Route::post('user/logs', 'UserController@postLogs')->name('user.logs');

            Route::post('user/cities-countries', 'UserController@citiesCountries')->name('user.cities_countries');
            Route::post('user/places', 'UserController@places')->name('user.places');

            /*
             * User Status'
             */
            Route::get('user/deactivated', 'UserStatusController@getDeactivated')->name('user.deactivated');
            Route::get('user/non-confirmed', 'UserStatusController@getNonConfirmed')->name('user.non_confirmed');
            Route::get('user/suspended', 'UserStatusController@getSuspended')->name('user.suspended');
            Route::get('user/deleted', 'UserStatusController@getDeleted')->name('user.deleted');

            /*
             * User CRUD
             */
            Route::resource('user', 'UserController');

            Route::post('user/delete-ajax', 'UserController@delete_ajax')->name('user.delete_ajax');
            /*Block User*/
            Route::post('user/block', ['uses' => 'UserController@block'])->name('user.block');
            /*UnBlock User*/
            Route::post('user/unblock', ['uses' => 'UserController@unBlockUser'])->name('user.unblock');
            /*
             * Specific User
             */
            Route::group(['prefix' => 'user/{user}'], function () {
                // Account
                Route::get('account/confirm/resend', 'UserConfirmationController@sendConfirmationEmail')->name('user.account.confirm.resend');

                // Status
                Route::get('mark/{status}', 'UserStatusController@mark')->name('user.mark')->where(['status' => '[0,1]']);

                // Password
                Route::get('password/change', 'UserPasswordController@edit')->name('user.change-password');
                Route::patch('password/change', 'UserPasswordController@update')->name('user.change-password.post');

                // Access
                Route::get('login-as', 'UserAccessController@loginAs')->name('user.login-as');

                // Session
                Route::get('clear-session', 'UserSessionController@clearSession')->name('user.clear-session');
            });

            /*
             * Deleted User
             */
            Route::group(['prefix' => 'user/{deletedUser}'], function () {
                Route::get('delete', 'UserStatusController@delete')->name('user.delete-permanently');
                Route::get('restore', 'UserStatusController@restore')->name('user.restore');
            });
        });

        /*
        * Role Management
        */
        Route::group(['namespace' => 'Role'], function () {
            Route::resource('role', 'RoleController');

            //For DataTables
            Route::post('role/table', ['uses' => 'RoleController@table'])->name('role.table');
        });


        /*
         * Language Manager
         **/

        Route::group(['namespace' => 'Languages'], function () {
            /*
             * For DataTables
             */
            Route::post('languages/table', ['uses' => 'LanguagesController@table'])->name('languages.table');

            /*
             * User CRUD
             */
            Route::resource('languages', 'LanguagesController');

            Route::get('languages/deactivated', 'LanguagesController@getDeactivated')->name('languages.deactivated');

            /*
             * Specific Language
             */
            Route::group(['prefix' => 'languages/{language}'], function () {
                Route::get('mark/{status}', 'LanguagesController@mark')->name('languages.mark')->where(['status' => '[1,2]']);
            });

            Route::get('translation-progress', 'ConfTranslationsProgressController@getUncompleted')->name('translation.progress');
            Route::get('translation-progress/{id}', 'ConfTranslationsProgressController@get')->name('translation.progress.get');

        });

    });
});

