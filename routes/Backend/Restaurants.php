<?php
/*
 * Restaurants Manager
 **/
Route::group(['namespace' => 'Restaurants'], function () {

    /*
     * For DataTables
     */
    Route::post('restaurants/table', ['uses' => 'RestaurantsController@table'])->name('restaurants.table');
    Route::get('restaurants/import', 'RestaurantsController@import')->name('restaurants.import');

    Route::get('restaurants/countries/{term?}/{type?}/{q?}', 'RestaurantsController@getAddedCountries')->name('restaurants.countries');
    Route::get('restaurants/cities/{term?}/{type?}/{q?}', 'RestaurantsController@getAddedCities')->name('restaurants.cities');
    Route::get('restaurants/types/{term?}/{type?}/{q?}', 'RestaurantsController@getPlaceTypes')->name('restaurants.types');

    Route::post('restaurants/search', 'RestaurantsController@search')->name('restaurants.search');
    Route::get('restaurants/search/{admin_logs_id?}/{country_id?}/{city_id?}/{latlng?}', 'RestaurantsController@search')->name('restaurants.search');
    Route::post('restaurants/savesearch', 'RestaurantsController@savesearch')->name('restaurants.savesearch');
    Route::get('restaurants/return_search_history', 'RestaurantsController@return_search_history')
            ->name('restaurants.searchhistory');

    /*
     * Restaurants CRUD
     */
    Route::resource('restaurants', 'RestaurantsController');

    Route::post('restaurants/delete-ajax', 'RestaurantsController@delete_ajax')->name('restaurants.delete_ajax');

     /*
     * Enable/Disable Specific Restaurants
     */
    Route::group(['prefix' => 'restaurants/{restaurants}'], function () {
        Route::get('mark/{status}', 'RestaurantsController@mark')->name('restaurants.mark')->where(['status' => '[1,2]']);
    });
});