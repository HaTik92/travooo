<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Services\Api\PrimaryPostService;
use Illuminate\Support\Facades\DB;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::group(['prefix' => 'api'], function () {
    Route::group(['prefix' => 'v1'], function () {

        Route::get('/log/download/{file_name}', 'TestingController@downloadLog');

        includeRouteFiles(__DIR__ . '/Api/');

        // Common Api | without auth [+ search api]
        Route::group(['middleware' => ['api_request:search-and-common']], function () {
            // other
            Route::get('/countries', 'SearchController@getCountryList');
            Route::get('/languages', 'SearchController@getLanguages');
            Route::get('/city', 'TravelMatesController@getCities');
            Route::get('/country', 'TravelMatesController@getCountries');

            Route::get("/search/global/countries", 'SearchController@searchCountries');
            Route::get("/search/global/cities", 'SearchController@searchCities');
        });

        // Common Api | With auth
        Route::group(['middleware' => ['auth:api', 'api_request:search-and-common']], function () {
            // media api
            Route::post('/media', 'CommonApiController@postTmpUpload');
            Route::delete('/media', 'CommonApiController@uploadTmpDelete');
            Route::get('/account-delete-reasons', 'SettingsController@getAccontDeleteReason');

            // search api
            Route::get("/search/users", 'SearchController@getUsers');
            Route::get("/search/user-or-place", 'ReportsController@getSearchUsersPlaces');
            Route::get('/search/global', 'SearchController@globalSearch');
            Route::get('/search/tags', 'SearchController@tagsSearch');
            Route::get('/search/location', 'SearchController@getLocationSearchResult');
            Route::get('/place/{place_id}/visits', 'TripController@getPlaceVisits')->where(['place_id' => '[0-9]+']);
            Route::get('/place/{place_id}/visits-thumbs', 'TripController@getPlaceVisitslabels')->where(['place_id' => '[0-9]+']);
        });

        // Primary Post
        Route::group(['middleware' => ['auth:api', 'api_request:primary-posts']], function () {
            // Orignal Post
            Route::post('{type}/{post_id}/like-unlike', 'PrimaryPostController@postLike');
            Route::get('{type}/{post_id}/likes', 'PrimaryPostController@getUserByLikes');
            Route::post('{type}/{post_id}/privacy', 'PrimaryPostController@changePrivacy');
            Route::post('/share', 'PrimaryPostController@sharePost');

            // Comment
            Route::post('{type}/{post_id}/comment', 'PrimaryPostController@postComment');
            Route::delete('{type}/{post_id}/comment/{comment_id}', 'PrimaryPostController@deleteComment');
            Route::post('{type}/{post_id}/comment/{comment_id}/like-unlike', 'PrimaryPostController@postCommetLike');

            //Report Post
            Route::post('/{type}/{post_id}/report', 'PrimaryPostController@report');
        });

        Route::group(['middleware' => 'auth:api', 'prefix' => 'add-dummy-data'], function () {
            Route::post('/trip/{newsfeed_type?}', 'AutoGenerateController@tripGenerate');
        });

        // Post
        Route::group(['middleware' => ['auth:api', 'api_request:posts'], 'prefix' => 'posts'], function () {
            Route::post('/', 'PostController@createPost');
            Route::post('/delete', 'PostController@postDelete');
            Route::post('my-location', 'PostController@location');
            Route::post('/spam', 'PostController@reportSpam');
            Route::get('/likeunlike', 'PostController@postLikeUnlike');
            // Route::post('/privacy', 'PostController@setPrivacy');

            Route::post('/likes4modal', 'PostController@postLikes4Modal');
            Route::post('/shareunshare', 'PostController@postShareUnshare');

            Route::get('/types', 'PostController@getPostType');
            Route::get('/report-types', 'PostController@getReportType');

            // Comments

            // Route::post('/{post_id}/comment/{comment_id}/likeunlike', 'PostController@postCommentLikeUnlike');

            Route::post('/checkincomment', 'PostController@checkinComment');
            Route::post('/checkinlikeunlike', 'PostController@checkinLikeUnlike');
            Route::post('/comment/edit', 'PostController@postCommentEdit');
            Route::post('/checkincommentdelete', 'PostController@checkinCommentDelete');
            Route::post('/checkincommentlikeunlike', 'PostController@checkinCommentLikeUnlike');
            Route::post('/commentlikeusers', 'PostController@getCommentLikeUsers');
        });

        Route::group(['middleware' => ['api_request:global-trip'], 'prefix' => 'global-trip'], function () {
            Route::get('/', 'GlobalTripController@index');
            Route::get('/trending-trips', 'GlobalTripController@topTrips');
            Route::get('/trending-destinations', 'GlobalTripController@trendingDestinations');
            // Route::get('/trip-plans', 'GlobalTripController@getIndex');
            // Route::get('/load-more-trip-plans', 'GlobalTripController@getMoreTripPlan');
            // Route::get('/trip-plans-filter', 'GlobalTripController@getFilterTripPlan');
            // Route::get('/get-search-countries', 'GlobalTripController@getSearchCountries');
        });

        Route::get('/reports/get-updatet-list', 'ReportsController@getUpdatetList');

        // This module is out of scope
        // Route::group(['middleware' => 'auth:api', 'prefix' => 'travelmates'], function () {
        //     Route::get('/', 'TravelMatesController@getIndexNewFunc');

        //     Route::get('travelmates-new', 'TravelMatesController@getIndexNew');
        //     Route::get('/travelmates-newest', 'TravelMatesController@getIndexNewFunc');
        //     Route::post('/get-travelmates', 'TravelMatesController@getTravelMates');
        //     Route::get('/travelmates-get-filters', 'TravelMatesController@getFilters');
        //     Route::get('/travelmates-get-order-filter', 'TravelMatesController@getOrderFilter');
        //     Route::get('/travelmates-old', 'TravelMatesController@getNewest');
        //     Route::get('/travelmates-trending', 'TravelMatesController@getTrending');
        //     Route::get('/travelmates-soon', 'TravelMatesController@getSoon');
        //     Route::get('/travelmates-notifications-invitations', 'TravelMatesController@getNotificationsInvitations');
        //     Route::get('/travelmates-notifications-requests', 'TravelMatesController@getNotificationsRequests');
        //     Route::get('/join-with-plans-requests', 'TravelMatesController@joinWithPlansRequests');
        //     Route::get('/join-without-plans-request', 'TravelMatesController@joinWithoutPlansRequests');
        //     Route::get('/travelmates-notifications-approved', 'TravelMatesController@getApprovedRequests');
        //     Route::get('/travelmates-request', 'TravelMatesController@getRequest');
        //     Route::post('/travelmates-search', 'TravelMatesController@postSearch');
        //     Route::post('dorequest', 'TravelMatesController@postDoRequest');
        //     Route::post('join', 'TravelMatesController@postJoin');
        //     Route::post('travelmates_join', 'TravelMatesController@postTravelMatesJoin');
        //     Route::post('remove_mate', 'TravelMatesController@postRemoveMate');
        //     Route::post('cancel_invitation', 'TravelMatesController@postCancelInvitation');
        //     Route::post('close-request', 'TravelMatesController@postCloseRequest');
        //     Route::post('unpublish-request', 'TravelMatesController@postUnpublishRequest');
        //     Route::post('cancel-request', 'TravelMatesController@postCancelRequest');
        //     Route::post('approve-request', 'TravelMatesController@postApproveRequest');
        //     Route::post('disapprove-request', 'TravelMatesController@postDisapproveRequest');
        //     Route::post('leave-trip-plan', 'TravelMatesController@postLeaveTripPlan');
        //     Route::get('getSearchCity', 'TravelMatesController@getSearchCity');
        //     Route::get('getSearchCountry', 'TravelMatesController@getSearchCountry');
        //     Route::post('create-without-plan', 'TravelMatesController@createWithoutPlan');
        //     Route::get('/travelmates-notifications-my-invitation', 'TravelMatesController@joinWithPlansRequests');
        //     Route::get('/travelmates-my-requests', 'TravelMatesController@getMyRequests');
        //     Route::get('/travelmates-my-invitations', 'TravelMatesController@getMyInvitations');
        //     Route::post('/travelmates-cancel-all-invitations', 'TravelMatesController@cancelAllInvitations');
        // });
        Route::group(['middleware' => ['auth:api', 'api_request:profile'], 'prefix' => 'profile'], function () {

            Route::post('/{user_id}/follow', 'ProfileController@postFollow');
            Route::post('/{user_id}/unfollow', 'ProfileController@postUnFollow');

            Route::get('/', 'ProfileController@getAbout');
            Route::get('/{id}', 'ProfileController@getAbout')->where(['id' => '[0-9]+']);
            Route::get('/profile-about', 'ProfileController@getAbout');
            Route::get('/{id}/feed', 'ProfileController@getFeed');
            Route::get('/{id}/liked_feed', 'ProfileController@getLikedFeed');
            Route::get('/profile-travel-history', 'ProfileController@getTravelHistory');
            Route::get('/{id}/profile-travel-history', 'ProfileController@getTravelHistory');
            Route::get('/{id}/profile-map', 'ProfileController@getMap');
            Route::get('/plans', 'ProfileController@getPlans');
            Route::get('/{id}/plans', 'ProfileController@getPlans');
            Route::get('/plans-invited', 'ProfileController@getPlansInvited');
            Route::get('/{id}/plans-upcoming', 'ProfileController@getPlansUpcoming');
            Route::get('/{id}/plans-memory', 'ProfileController@getPlansMemory');
            Route::get('/plans-mydraft', 'ProfileController@getMyDraftPlans');
            Route::get('/{id}/plans-mydraft', 'ProfileController@getMyDraftPlans');
            // Route::get('/profile-plans-memory/{id}', 'ProfileController@getPlansMemory');
            Route::get('/profile-favourites', 'ProfileController@getFavourites');
            Route::get('/profile-favourites/{id}', 'ProfileController@getFavourites');
            Route::get('/{id}/photos', 'ProfileController@getPhotos');
            Route::get('/{id}/videos', 'ProfileController@getVideos');
            // Route::get('/profile-interests', 'ProfileController@getInterests');
            Route::get('/{id}/reviews', 'ProfileController@getReviews');
            Route::get('/{user_id}/check-follow', 'ProfileController@postCheckFollow');
            Route::get('/{user_id}/checkcontent', 'ProfileController@postCheckFollowContent');
            Route::post('/upload_cover', 'ProfileController@postUploadCover');
            Route::post('/upload_profile_image', 'ProfileController@postUploadProfileImage');
            Route::get('/deletecover', 'ProfileController@postDeleteCover');
            Route::post('/{user_id}/update_report', 'ProfileController@updateReport');
            Route::get('/remove-profile-image', 'ProfileController@deleteProfileImage');
            Route::post('/get_comments', 'ProfileController@getMediaComments');
            Route::post('/profileGetVisitedCities', 'ProfileController@getVisitedCities');
            Route::post('/update_feed', 'ProfileController@postUpdateFeed');
            Route::post('/getTimlineMedias', 'ProfileController@getTimlineMedias');
            Route::post('/update-bio', 'ProfileController@postUdateBio');
            Route::get('/update-all-plan', 'ProfileController@updateTripPlan');
            Route::get('/{id}/update-all-plan', 'ProfileController@updateTripPlan');
            Route::get('/{user_id}/getFollowers', 'ProfileController@getFollowers');
            Route::get('/{user_id}/getFollowing', 'ProfileController@getFollowing');
            Route::post('/postBecomeExpert', 'ProfileController@postBecomeExpert');
        });

        Route::group(['middleware' => ['auth:api', 'api_request:dashboard', 'expert_dashbaord'], 'prefix' => 'dashboard'], function () {
            Route::get('/engagement', 'DashboardController@getEngagement');
            Route::get('/followers', 'DashboardController@getFollowers');
            //----
            Route::get('/', 'DashboardController@getIndex');
            Route::get('/get-more-affiliate', 'DashboardController@getMoreAffiliate');
            Route::get('/overview', 'DashboardController@getOverview');
            Route::get('/interaction', 'DashboardController@getInteraction');
            Route::get('/get-interaction-filter', 'DashboardController@getInteractionFilter');
            Route::get('/get-posts', 'DashboardController@getPosts');
            Route::get('/get-reports', 'DashboardController@getReports');
            Route::get('/get-ranking', 'DashboardController@getRanking');
            Route::post('/apply-to-badge', 'DashboardController@ApplyToBadge');
            Route::get('/load-more-followers', 'DashboardController@getMoreFollowers');
            Route::get('/global-impact', 'DashboardController@getGlobalImpact');
            Route::get('/load-more-leader', 'DashboardController@getMoreLeader');
            Route::post('/get-engagement-diginfo', 'DashboardController@getEngagementDigInfo');
        });

        Route::get('/notifications', 'HomeController@postGetNotifications')->middleware('auth:api');

        Route::group(['middleware' => ['auth:api', 'api_request:home'], 'prefix' => 'home'], function () {
            Route::get('/search/poi', 'HomeController@getSearchPOIs'); // search place : trip creation
            // Route::get('/search/cities/poi', 'HomeController@getSearchCitiesPOIs');
            Route::get('/searchCountries', 'HomeController@getSearchCountries');
            Route::get('/searchCities', 'HomeController@getSearchCities');
            Route::get('/searchUsers', 'HomeController@getSearchUsers');
            Route::get('/search-countries-cities', 'HomeController@getSearchCountriesCities');
            Route::get('/suggest/top-places', 'HomeController@getSuggestTopPlaces'); // based on city : trip creation

            Route::get('/', 'HomeController@getIndex');
            Route::get('/top-places', 'HomeController@getTopPlaces');
            Route::get('/new-discover-destinations', 'HomeController@getDiscoverNewDestinations');
            Route::get('/new-people', 'HomeController@getNewPeople');
            Route::get('/get_countries', 'HomeController@getSelect2');
            Route::get('/searchLocation', 'HomeController@getSelect2Location');
            Route::get('/searchPlaces', 'HomeController@getSearchPlaces');

            Route::get('/searchHotels', 'HomeController@getSearchHotels');
            Route::get('/searchHotelsMention', 'HomeController@getSearchHotelsMention');
            Route::get('/searchRestaurants', 'HomeController@getSearchRestaurants');
            Route::get('/searchCountriesCities', 'HomeController@getSearchCountriesCities');
            Route::get('/searchTravelstyles', 'HomeController@getSearchTravelstyles');
            Route::get('/list_travelstyles', 'HomeController@getListTravelstylesForSelect');
            Route::post('/url_preview', 'HomeController@postURLPreview');

            Route::get('/newsfeed', 'HomeNewsfeedController@index');
            Route::get('/newsfeed/{type}', 'HomeNewsfeedController@getSecondaryPost');
            Route::get('/new-newsfeed', 'NewsfeedController@index');
            Route::get('/events', 'HomeController@showEvents');


            Route::get('/search_pois_for_tagging', 'HomeController@getSearchPOIsForTagging');

            Route::get('/tools', 'HomeController@tools');

            Route::get('/getMessages', 'HomeController@postGetMessages');
            Route::get('ref/{referral_id}', 'HomeController@getReferral');
            Route::post('track_ref', 'HomeController@trackRef');
            // Route::get('user-blocked', 'HomeController@userBlocked');
            // Route::get('citiesToUpdate', 'HomeController@getExcelCities');
            // Route::get('copyright-infringement', 'HomeController@copyright');
            // Route::get('countriesToUpdate', 'HomeController@getExvelCountries'); //remain
            // Route::get('updatePlacesCities', 'HomeController@updatePlacesCities');
            // Route::get('/auth', 'HomeController@postChatAuth');
            // Route::post('/auth', 'HomeController@postChatAuth');
            Route::get('/post-view/{postId}', 'HomeController@getPostView');
            Route::get('el', 'HomeController@getEl');
            Route::get('el2', 'HomeController@getEl2');
            Route::get('el3', 'HomeController@getEl3');
            Route::get('/searchDestination', 'HomeController@getSelect2Location');
            Route::get('/SearchTrips', 'HomeController@getSearchTrips');

            // Route::post('home/select2locations', 'HomeController@getSelect2Location');
            // Route::get('home/select2locations', 'HomeController@getSelect2Location');
        });

        Route::group(['middleware' => 'auth:api', 'prefix' => 'expert'], function () {
            Route::get('/', 'ExpertController@getIndex');
        });

        Route::group(['middleware' => 'auth:api', 'prefix' => 'medias'], function () {
            Route::post('/{medias_id}/comment', 'MediasController@postComment');
            Route::post('/commentreply', 'MediasController@postCommentReply');
            Route::post('/commentedit', 'MediasController@postCommentEdit');
            Route::post('/likeunlike', 'MediasController@postLikeUnlike');
            Route::post('/trip-place-likeunlike', 'MediasController@tripPlaceLikeUnlike');
            Route::post('/commentdelete', 'MediasController@postCommentDelete');
            Route::post('/commentlikeunlike', 'MediasController@postCommentLikeUnlike');
            // Route::post('/comment-updownvote', 'MediasController@postCommentVotes');
            Route::get('/{medias_id}/showlikes', 'MediasController@postShowLikes');
            Route::post('/commentlikeusers', 'MediasController@getCommentLikeUsers');

            Route::post('/{medias_id}/comment4place', 'MediasController@postComment4Place');
            Route::get('/{medias_id}/updateviews', 'MediasController@postUpdateViews');
        });

        $placeRoutes = ['place' => 'PlaceController2@', 'country' => "CountryController2@", 'city' => "CityController@"];
        foreach ($placeRoutes as $prefix => $controllerName) {
            Route::group(['middleware' => ['auth:api', 'api_request:ccp'], 'prefix' => $prefix], function () use ($controllerName) {

                // review only for place module 
                Route::post('/{place_id}/review', 'PlaceController2@postReview');
                //---

                // Route::get('/{place_id}/weather', 'CityController@getWeather');
                // Route::get('/{place_id}/packing-tips', 'CityController@getPackingTips');
                // Route::get('/{place_id}/etiquette', 'CityController@getEtiquette');
                // Route::get('/{place_id}/health', 'PlaceController2@getHealth');
                // Route::get('/{place_id}/visa-requirements', 'CityController@postVisaRequirements');
                // Route::get('/{place_id}/when-to-go', 'PlaceController2@getWhenToGo');
                // Route::get('/{place_id}/caution', 'PlaceController2@getCaution');
                // Route::get('/{place_id}/check-checkin', 'PlaceController2@postCheckCheckin');

                Route::post('/media', $controllerName . 'uploadMedia');
                Route::delete('/media', $controllerName . 'deleteMedia');
                // Route::post('/{place_id}/checkin', $controllerName . 'postCheckin');
                // Route::get('/{place_id}/checkout', $controllerName . 'postCheckout');

                // Route::get('city_holiday/{id}', 'CityController@city_holiday');
                // Route::get('country_holiday/{id}', 'CountryController2@country_holiday');


                // Route::post('/postcomment', 'PlaceController2@postComment');
                // Route::post('/postcommentedit', 'PlaceController2@postCommentEdit');
                // Route::post('/postcommentreply', 'PlaceController2@postCommentReply');

                Route::get('/{place_id}/check-follow', $controllerName . 'postCheckFollow');
                Route::get('/{place_id}/follow', $controllerName . 'postFollow');
                Route::get('/{place_id}/unfollow', $controllerName . 'postUnFollow');
                // Route::post('/{place_id}/visa-requirements', 'PlaceController2@postVisaRequirements');
                // Route::post('/{place_id}/weather', 'PlaceController2@postWeather');
                // Route::get('/{place_id}/talking-about', 'PlaceController2@postTalkingAbout');
                // Route::get('/{place_id}/now-in-place', 'PlaceController2@postNowInPlace');
                // Route::post('/{place_id}/contribute', 'PlaceController2@postContribute');
                Route::post('/post-review', $controllerName . 'postReview');

                // Route::post('/editReview/{id}', 'PlaceController2@editReview');
                // Route::get('/deleteReview/{id}', 'PlaceController2@deleteReview');

                // Route::get('/{place_id}/happeningToday', 'PlaceController2@happeningToday');
                // Route::get('/{place_id}/postMorePhoto', 'PlaceController2@postMorePhoto');
                // Route::post('/discussionloadmore', 'PlaceController2@discussionTipLoadmore');
                // Route::post('/mediaComments', 'PlaceController2@mediaComments');

                // Route::post('/reviews-updownvote', 'PlaceController2@postReviewUpDownVote');

                // Route::post('/{place_id}/update_post', 'PlaceController2@ajaxUpdatePosts');

                // Route::post('/save-review', 'PlaceController2@saveReview');
                // Route::get('{id}/share-review', 'PlaceController2@shareReview');

                // Route::post('/searchall', 'PlaceController2@searchAllDatas');
                Route::post('/get64image', 'PlaceController2@get64Image');

                // Route::post('/medialike', 'PlaceController2@z');
                // Route::get('/eventlike/{event_id}', 'PlaceController2@eventLikes');
                // Route::get('/eventshare/{event_id}', 'PlaceController2@eventShares');
                // Route::post('/eventcomment', 'PlaceController2@eventComments');
                // Route::post('/eventcommentreply', 'PlaceController2@eventCommentsReply');
                // Route::post('/eventcommentedit', 'PlaceController2@eventCommentsEdit');
                // Route::get('/eventcommentlikeunlike/{comment_id}', 'PlaceController2@eventCommentsLike');
                // Route::get('/eventcommentdelete/{comment_id}', 'PlaceController2@eventCommentsDelete');
                // Route::post('/event_comment4modal', 'PlaceController2@eventComments4Modal');
                // Route::get('/eventcommentlikeusers/{comment_id}', 'PlaceController2@getCommentLikeUsers');

                // Route::get('/check_event/{place_id}', $controllerName . 'checkEvent');

                // Route::get('/get_media', 'PlaceController2@getMedia');
                // Route::get('/get_poi_media', 'PlaceController2@getPOIMedia');
                // Route::get('/set_poi_media', 'PlaceController2@setPOIMedia');

                // Route::get('/getTripData/{id}', 'PlaceController2@getTripData');
                // Route::get('/{place_id}/discussion_replies_24hrs', 'PlaceController2@discussion_replies_24hrs');
                // Route::get('/{place_id}/newsfeed_show_all', $controllerName . 'newsfeed_show_all');
                // Route::get('/{place_id}/newsfeed_show_about', $controllerName . 'newsfeed_show_about');
                // Route::get('/{place_id}/newsfeed_show_discussions', $controllerName . 'newsfeed_show_discussions');
                // Route::get('/{place_id}/newsfeed_show_top', 'PlaceController2@newsfeed_show_top');
                // Route::get('/{place_id}/newsfeed_show_media', $controllerName . 'newsfeed_show_media');
                // Route::get('/{place_id}/newsfeed_show_tripplan', $controllerName . 'newsfeed_show_tripplan');
                // Route::get('/{place_id}/newsfeed_show_travelmates', $controllerName . 'newsfeed_show_travelmates');
                // Route::get('/{place_id}/newsfeed/events', $controllerName . 'newsfeed_show_events');
                // Route::get('/{place_id}/newsfeed_show_reports', 'PlaceController2@newsfeed_show_reports');
                // Route::get('/{place_id}/newsfeed_show_reviews', $controllerName . 'newsfeed_show_reviews');
                // Route::get('/{place_id}/get-place-about', 'PlaceController2@getPlaceAboutInfo');

                // Route::post('/mediaComments4Modal', 'PlaceController2@mediaComments4Modal');
                // Route::post('/tripComments4Modal', 'PlaceController2@tripComments4Modal');
                // Route::post('/get-post-detail', 'PlaceController2@getPostById');
            });
        }

        Route::group(['middleware' => 'auth:api', 'prefix' => 'trip-place-comments'], function () {
            Route::get('/getCommentsList', 'TripPlaceCommentsController@getList');
            Route::post('/postComment', 'TripPlaceCommentsController@create');
        });

        // Common Api | auth optional
        Route::prefix('/')->group(function () {
            // newsfeed
            Route::get('/newsfeed/{page_id}', 'NewsfeedPageController@index');
            // Comment
            Route::get('/{type}/{post_id}/comment', 'PrimaryPostController@loadMoreComments')->where(['id' => '[0-9]+', 'type' => '[a-z]+']);
            // report
            Route::group(['middleware' => ['api_request:reports'], 'prefix' => 'reports'], function () {
                Route::get('/', 'ReportsController@getList');
                Route::get('/top-places', 'ReportsController@topPlaces');
                Route::get('/{id}', 'ReportsController@getShow')->where(['id' => '[0-9]+']);
            });
            // Timeline
            Route::get('/trip/{id}', 'TripController@getPlanContents')->where(['id' => '[0-9]+']);

            // Country City, Place Module
            $placeRoutes = ['place' => 'PlaceController2@', 'country' => "CountryController2@", 'city' => "CityController@"];
            foreach ($placeRoutes as $prefix => $controllerName) {
                Route::group(['middleware' => ['api_request:ccp'], 'prefix' => $prefix], function () use ($controllerName) {
                    // Fetch Country City, Place
                    Route::get('/{id}', $controllerName . 'getIndex')->where(['id' => '[0-9]+']);
                    Route::get('/{id}/experts', $controllerName . 'getExpertsList')->where(['id' => '[0-9]+']);
                    Route::get('/{id}/trips', $controllerName . 'getTrips')->where(['id' => '[0-9]+']);
                    Route::get('/{id}/newsfeed', $controllerName . 'showNewsfeed')->where(['id' => '[0-9]+']);
                    if (in_array($controllerName, ["CountryController2@", "CityController@"])) {
                        Route::get('/{id}/about', $controllerName . 'aboutTab')->where(['id' => '[0-9]+']);
                    }

                    // review only for place module 
                    Route::get('/{place_id}/reviews', 'PlaceController2@loadReview');
                    //---
                });
            }
        });
    });
});
