<?php
//error_reporting(0);

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

/**
 * Global Routes
 * Routes that are used between both frontetravend and backend.
 */

Route::get('check-cron-status', 'HomeController@checkCronStatus');
/*
Route::get('logout', '\App\Http\Controllers\Backend\Login\Auth\LoginController@logout')->name('logout');
Route::get('admin/login', '\App\Http\Controllers\Backend\Login\Auth\LoginController@login')->name('admin.login');
Route::group(['middleware' => 'auth:user'], function () {
    Route::get('/home', 'HomeController@getIndex');
    Route::post('/ajaxGetNotifications', 'HomeController@postAjaxGetNotifications');
});
Route::get('/auth', 'HomeController@postChatAuth');
Route::post('/auth', 'HomeController@postChatAuth');

Route::get('el', 'HomeController@getEl');
Route::get('el2', 'HomeController@getEl2');
Route::get('el3', 'HomeController@getEl3');

Route::get('home/select2', 'HomeController@getSelect2');
Route::post('home/select2', 'HomeController@getSelect2');
Route::post('home/select2locations', 'HomeController@getSelect2Location');
Route::get('home/select2locations', 'HomeController@getSelect2Location');
Route::get('home/searchPlaces', 'HomeController@getSearchPlaces');
Route::get('home/searchHotels', 'HomeController@getSearchHotels');
Route::get('home/searchRestaurants', 'HomeController@getSearchRestaurants');
Route::get('home/searchCountries', 'HomeController@getSearchCountries');
Route::get('home/searchCities', 'HomeController@getSearchCities');
Route::get('home/searchUsers', 'HomeController@getSearchUsers');

Route::get('/book/hotel', 'BookController@getHotels');
Route::post('book/search-hotel', 'BookController@postSearchHotels');
Route::get('book/search-hotel', 'BookController@getSearchHotels');
Route::post('book/search-flight', 'BookController@postSearchFlights');
Route::post('book/complete-search-hotel', 'BookController@ajaxCompleteSearchHotels');
Route::post('book/complete-search-flight', 'BookController@ajaxCompleteSearchFlights');
Route::get('/book/flight', 'BookController@getFlights');
Route::get('/book/ajaxSearchCity', 'BookController@ajaxSearchCity');

Route::get('reports/list', 'ReportsController@getList');
Route::get('reports/{id}', 'ReportsController@getShow');
Route::get('reports/create', 'ReportsController@getCreate');
Route::post('reports/create', 'ReportsController@postCreate');
Route::post('reports/ajaxGeocodeCountry', 'ReportsController@postAjaxGeocodeCountry');
Route::post('reports/ajaxGeocodeCity', 'ReportsController@postAjaxGeocodeCity');


Route::get('/profile', 'ProfileController@getAbout');
Route::get('/profile-about', 'ProfileController@getAbout');
Route::get('/profile-posts', 'ProfileController@getPosts');
Route::get('/profile-travel-history', 'ProfileController@getTravelHistory');
Route::get('/profile-map', 'ProfileController@getMap');
Route::get('/profile-plans', 'ProfileController@getPlans');
Route::get('/profile-plans-invitations', 'ProfileController@getPlansInvitations');
Route::get('/profile-favourites', 'ProfileController@getFavourites');
Route::get('/profile-photos', 'ProfileController@getPhotos');
Route::get('/profile-videos', 'ProfileController@getVideos');
Route::get('/profile-interests', 'ProfileController@getInterests');
Route::get('/profile-reviews', 'ProfileController@getReviews');
Route::get('/profile-badges', 'ProfileController@getBadges');
Route::post('/profile/{user_id}/check-follow', 'ProfileController@postCheckFollow');
Route::post('/profile/{user_id}/follow', 'ProfileController@postFollow');
Route::post('/profile/{user_id}/unfollow', 'ProfileController@postUnFollow');



Route::get('/travelmates', 'TravelMatesController@getIndex');
Route::get('/travelmates-newest', 'TravelMatesController@getNewest');
Route::get('/travelmates-trending', 'TravelMatesController@getTrending');
Route::get('/travelmates-soon', 'TravelMatesController@getSoon');
Route::get('/travelmates-notifications-invitations', 'TravelMatesController@getNotificationsInvitations');
Route::get('/travelmates-notifications-requests', 'TravelMatesController@getNotificationsRequests');
Route::get('/travelmates-request', 'TravelMatesController@getRequest');
Route::post('/travelmates-search', 'TravelMatesController@postSearch');
Route::post('/travelmates/dorequest', 'TravelMatesController@postDoRequest');
Route::post('/travelmates/join', 'TravelMatesController@postJoin');
Route::post('/travelmates/ajax_remove_mate', 'TravelMatesController@postAjaxRemoveMate');
Route::post('/travelmates/ajax_cancel_invitation', 'TravelMatesController@postAjaxCancelInvitation');

Route::get('/trip/plan/{id}', 'TripController@getCreateTrip');
Route::get('/trip/plan/{id}/{city_id}', 'TripController@getCreateTrip');
Route::get('/trip/view/{id}', 'TripController@getIndex');
Route::post('/trip/version-respond', 'TripController@postVersionRespond');
Route::post('/trip/ajaxFirstStep', 'TripController@postAjaxFirstStep');
Route::post('/trip/ajaxSearchCities', 'TripController@postAjaxSearchCities');
Route::post('/trip/ajaxSearchPlaces', 'TripController@postAjaxSearchPlaces');
Route::post('/trip/ajaxAddCityToTrip', 'TripController@postAjaxAddCityToTrip');
Route::post('/trip/ajaxAddPlaceToTrip', 'TripController@postAjaxAddPlaceToTrip');
Route::post('/trip/ajaxRemovePlaceFromTrip', 'TripController@postajaxRemovePlaceFromTrip');
Route::post('/trip/ajaxActivateTripCity', 'TripController@postAjaxActivateTripCity');
Route::post('/trip/ajaxDeActivateTripCity', 'TripController@postAjaxDeActivateTripCity');
Route::post('/trip/ajaxActivateTripPlace', 'TripController@postAjaxActivateTripPlace');
Route::post('/trip/ajaxDeActivateTripPlace', 'TripController@postAjaxDeActivateTripPlace');
Route::post('/trip/ajaxDeleteTrip', 'TripController@postAjaxDeleteTrip');
Route::post('/trip/ajaxActivateTrip', 'TripController@postAjaxActivateTrip');
Route::post('/trip/ajaxInviteFriends', 'TripController@postAjaxInviteFriends');
Route::post('/trip/ajaxAcceptInvitation', 'TripController@postAjaxAcceptInvitation');
Route::post('/trip/ajaxRejectInvitation', 'TripController@postAjaxRejectInvitation');

Route::get('/settings', 'SettingsController@getIndex');
Route::post('/settings/account', 'SettingsController@postAccount');
Route::post('/settings/social', 'SettingsController@postSocial');

Route::get('/users/autocomplete', 'UsersController@getAutoComplete');

Route::get('/log', 'LogController@getIndex');
Route::get('/settings', 'SettingsController@getIndex');


Route::get('/dashboard', 'DashboardController@getIndex');

Route::get('/expert', 'ExpertController@getIndex');



Route::group(['middleware' => 'auth:user', 'prefix' => 'chat'], function () {
    Route::get('/{conversation_id?}', ['as' => 'chat.index', 'uses' => 'ChatPrivateController@index']);
    Route::post('/send', 'ChatPrivateController@send');
    //Route::post('/start', 'ChatPrivateController@start');
    Route::get('/messages/{chat_conversation_id}', 'ChatPrivateController@getMessages');
    Route::get('/dropdown/participants','ChatPrivateController@getParticipantsCombo');
    Route::get('/last/{number}', 'ChatPrivateController@getLastFive');
    Route::get('/conversation/{conversation_id}/delete', 'ChatPrivateController@deleteConversation');
    Route::get('/new/{user_id}', 'ChatPrivateController@createNewConversation')->name('chat.new');
});
*/

Route::group(['middleware' => ['access.routeNeedsRole:1']], function () {
    Route::get('clear-cache', function () {
        /* php artisan cache:clear */
        Artisan::call('cache:clear');
        dd("Cache is cleared");
    });
    Route::get('config-clear', function () {
        /* php artisan cache:clear */
        Artisan::call('config:clear');
        dd("Config is cleared");
    });
    Route::get('config-cache', function () {
        /* php artisan config:cache */
        Artisan::call('config:cache');
        dd("Config is cached");
    });
    Route::get('repair-medias-type', function () {
        /* php artisan config:cache */
        Artisan::call('repair:medias-type');
        dd("done");
    });


    Route::get('view-clear', function () {
        /* php artisan view:clear */
        Artisan::call('view:clear');
        dd("View is cleared");
    });

    Route::get('composer-dump-autoload', function () {
        shell_exec('cd ' . base_path() . ' && composer dump-autoload');
        dd('Generated optimized autoload files');
    })->name('dump');

    Route::get('db-seed/{class}', function ($class) {
        if (class_exists($class)) {
            Artisan::call('db:seed', ['--class' => $class]);
            dd($class . ' seeded');
        }
        dd("Invalid seeder class name (Please make sure you call the roue '/composer-dump-autoload' before running seeder)");
    });

    // Route::get('seed-generic-top', function () {
    //     Artisan::call('seed-generic-tops');
    //     dd("Generic top seeded");
    // });
});

Route::get('/log/download/{file_name}', 'Api\TestingController@downloadLog');


Route::get('/tv-link', 'Api\TestingController@checkLink');

Route::get('/web-test-link', function () {
    return view('test.test');
});

Route::get('/delete-account/{token}', 'SettingsController@finalDeleteAccount');
//random scripts started
Route::get('citiesToUpdate', 'HomeController@getExcelCities');
Route::post('copyright-infringement', 'Backend\CopyrightInfringement\CopyrightInfringementController@store');
Route::get('copyright-infringement', 'HomeController@copyright')->name('copyright_infringement');
Route::get('copyright-policy/{id}', 'Backend\CopyrightInfringement\CopyrightInfringementController@getCopyrightPolicy');
Route::get('countriesToUpdate', 'HomeController@getExvelCountries');
Route::get('updatePlacesCities', 'HomeController@updatePlacesCities');
Route::get('identifyStranfeCities', 'PlaceController2@getCitiesInfoFromGoogle');
Route::get('updateLanguageCodes', 'PlaceController2@updateLanguageCodes');
Route::get('set-city-lat-long', 'PlaceController2@updateCiteisLatLong');
Route::get('get-top-places', 'HomeController@getTopPlaces');
Route::get('extract-file', 'HomeController@extractFile');
Route::get('get-google-countries', 'HomeController@getCountries');
Route::get('design', 'FrontEndController@design');
Route::get('popups', 'FrontEndController@popups');
Route::get('pages', 'FrontEndController@pages');
Route::get('userboard', 'FrontEndController@userboard');
Route::get('getUsaCities', 'HomeController@getUsaCiies');
Route::get('getStates', 'HomeController@getStates');
Route::get('getStatesData', 'HomeController@getStatesData');
Route::get('updatePlacesInformationByStates', 'HomeController@updatePlacesInformationByStates');
Route::get('getTopCities', 'HomeController@getTopCities');
Route::get('getTranslate', 'HomeController@getTranslate');
Route::get('processCsv', 'HomeController@processCsv');
Route::get('getcities', 'HomeController@getCities');
Route::get('getTranslateGlobal', 'HomeController@getTranslateGlobal');
Route::get('getTopCitiesCountries', 'HomeController@getTopCitiesCountries');
Route::get('move-place-post-to-country-city', 'HomeController@movePlacePostToCitiesCountries');
Route::get('getStatesFromGoogle', 'HomeController@getStatesFromGoogle');
Route::get('getCountryRecords', 'HomeController@getCountryRecords');
Route::get('getJoinCsv', 'HomeController@joinCsv');
Route::get('get-empty-cities', 'HomeController@getEmptyCities');
Route::get('getWrongCities', 'HomeController@getWrongCities');
Route::get('dummyRecordsData', 'HomeController@dummyRecordsData');
Route::get('getRemainingCountriesData', 'HomeController@getRemainingCountriesData');
Route::get('getOldCountries', 'HomeController@getOldCountries');
Route::get('get95kPlace', 'HomeController@get95kPlace');
Route::get('getOriginalDummy', 'HomeController@getOriginalDummy');
Route::get('getDuplicateCity', 'HomeController@getDuplicateCity');
Route::get('getUniquePlacesList', 'HomeController@getUniquePlacesList');
Route::get('topPlacesScript', 'HomeController@topPlacesScript');
Route::get('setMislData', 'HomeController@setMislData');
Route::get('setLatLongCity', 'HomeController@setLatLongCity');
Route::get('deleteDuplicate', 'HomeController@deleteDuplicate');
Route::get('getDuplicateCountryWise', 'HomeController@getDuplicateCountryWise');
Route::get('getUSDuplicateCoordinate', 'HomeController@getUSDuplicateCoordinate');

Route::get('getCitiesTypes', 'HomeController@getCitiesTypes');

Route::get('getzerolatlongCities', 'HomeController@getzerolatlongCities');
Route::get('deleteEmptyPlaceCities', 'HomeController@deleteEmptyPlaceCities');
Route::get('updateTopPlaces', 'PlaceController2@updateTopPlaces');
Route::get('getCitiesList', 'HomeController@getCitiesList');
Route::get('updateToursCities', 'HomeController@ajaxCheckToursCities');
Route::get('updateToursCountries', 'HomeController@ajaxCheckToursCountries');
Route::get('updateActivitiesCities', 'HomeController@ajaxCheckActivitiesCities');
Route::get('updateActivitiesCountries', 'HomeController@ajaxCheckActivitiesCountries');
Route::get('deleteTripPlan', 'HomeController@deleteTripPlan');
Route::get('getAllTopPlaces', 'HomeController@getAllTopPlaces');
Route::get('get-categories', 'HomeController@fetchAllCategoriesFromMusement');
Route::get('get-musement-cities', 'HomeController@fetchAllCitiesFromMusemnt');
Route::get('get-musement-events', 'HomeController@fetchTestEvents');
Route::get('get-tags', 'HomeController@removeTags');
Route::get('set-lang-content', 'HomeController@setContentsLanguage');
Route::get('add-engagement', 'HomeController@addLikeEngagement');
Route::get('remove-junk', 'HomeController@removeJunkContent');
Route::get('home-query-testing', 'HomeController@checkTriplanEngagement');
Route::get('get-google-name', 'HomeController@getGoogleNamesFromCSV');
Route::get('update-topplace-sort', 'HomeController@updateTopPlaceSort');
Route::get('checkWatchhistory', 'HomeController@checkWatchhistory');
Route::get('set-place-languages', 'PlaceController2@setPlacesLanguages');

//Landing pages links
Route::get('experts', 'FrontEndController@getExperts');
Route::get('trip-planner', 'FrontEndController@getTripPlanner');


//random scripts ended

Route::get('/', 'HomeController@showHome')->name('show_home');
Route::get('/home', 'HomeController@getIndex')->name('home')->middleware(['session_count']);
Route::post('/ajaxGetNotifications', 'HomeController@postAjaxGetNotifications')->name('home.ajax_get_notifications');
Route::post('/ajaxGetMessages', 'HomeController@postAjaxGetMessages')->name('home.ajax_get_messages');

Route::get('ref{referral_id}', 'HomeController@getReferral');
Route::post('ajax_track_ref', 'HomeController@ajaxTrackRef');
Route::get('user-blocked', 'HomeController@userBlocked')->name('user_is_blocked');
Route::get('setBulkData', 'HomeController@bulkUpdateInstance');


Route::group(['as' => 'page.'], function () {
    Route::get('privacy-policy', 'CommonPagesController@privacyPolicyPage')->name('privacy_policy');
    Route::get('terms-of-service', 'CommonPagesController@termsOfServicePage')->name('terms_of_service');
});
// Below Links For React Native Application
Route::get('page/privacy-policy', 'CommonPagesController@privacyPolicyPageApp');
Route::get('page/terms-of-service', 'CommonPagesController@termsOfServicePageApp');

Route::group([
    'prefix'     => \App\Http\Middleware\FrontendLocaleMiddleware::getLocale(),
    'middleware' => ['frontend_locale', 'session_count']
], function () {
    Route::group(['middleware' => 'auth:user'], function () {
    });
    Route::get('/auth', 'HomeController@postChatAuth');
    Route::post('/auth', 'HomeController@postChatAuth');

    Route::get('el', 'HomeController@getEl');
    Route::get('eltop', 'HomeController@getElTop');
    Route::get('el2', 'HomeController@getEl2');
    Route::get('el3', 'HomeController@getEl3');
    Route::get('el4', 'HomeController@getEl4');

    Route::post('submissions/create', 'Backend\Submissions\SubmissionsController@create')->name('submissions.create');

    Route::get('home/taggingSearchResult', 'HomeController@getTaggingSearchResult');
    Route::post('home/taggingSearchResult', 'HomeController@getTaggingSearchResult');
    Route::get('home/locationSearchResult', 'HomeController@getLocationSearchResult');
    Route::post('home/locationSearchResult', 'HomeController@getLocationSearchResult');
    Route::get('home/select2', 'HomeController@getSelect2');
    Route::post('home/select2', 'HomeController@getSelect2');
    Route::post('home/select2locations', 'HomeController@getSelect2Location');
    Route::get('home/select2locations', 'HomeController@getSelect2Location');
    Route::get('home/searchPlaces', 'HomeController@getSearchPlaces');
    Route::get('home/searchAllPOIs', 'HomeController@getSearchPOIs');
    Route::get('home/suggestTopPlaces', 'HomeController@getSuggestTopPlaces');
    Route::get('home/searchHotels', 'HomeController@getSearchHotels');
    Route::get('home/searchHotelsMention', 'HomeController@getSearchHotelsMention');
    Route::get('home/searchRestaurants', 'HomeController@getSearchRestaurants');
    Route::get('home/searchCountries', 'HomeController@getSearchCountries');
    Route::get('home/searchCities', 'HomeController@getSearchCities');
    Route::get('home/searchUsers', 'HomeController@getSearchUsers');
    Route::get('home/searchCountriesCities', 'HomeController@getSearchCountriesCities');
    Route::get('home/searchTravelstyles', 'HomeController@getSearchTravelstyles');
    Route::get('home/listTravelstylesForSelect', 'HomeController@getListTravelstylesForSelect');
    Route::post('home/url_preview', 'HomeController@postURLPreview');
    Route::post('home/update_feed', 'HomeController@postUpdateFeed');
    Route::get('home/search_pois_for_tagging', 'HomeController@getSearchPOIsForTagging')->name('home.search_pois_for_tagging');
    Route::get('home/tools', 'HomeController@tools');
    Route::post('home/userlikes', 'HomeController@getUserByLikes')->name('home.userlikes');
    Route::get('home/get-friend-trips', 'HomeController@getFriendsTrips')->name('get-friend-trips');
    Route::get('fix-place-country', 'HomeController@fixTopPlaceCountry');
    Route::get('fix-all-place-country', 'HomeController@updatePlacesWithCountries');

    $url_prefixs = ['post', 'review', 'discussion', 'trip-plan', 'trip-media', 'event', 'external', 'share'];
    foreach ($url_prefixs as $url_prefix) {
        Route::group(['prefix' => $url_prefix, 'as' => $url_prefix . '.'], function () use ($url_prefix) {
            Route::get('{username}/{id}/{islog?}', 'NewsFeedController@getNewsFeed');
        });
    }


    Route::get('/book/hotel', 'BookController@getHotels')->name('book.hotel');
    Route::post('book/search-hotel', 'BookController@postSearchHotels');
    Route::get('book/search-hotel', 'BookController@getSearchHotels')->name('book.search_hotel');
    Route::post('book/search-flight', 'BookController@postSearchFlights')->name('book.search_flight');
    Route::post('book/complete-search-hotel', 'BookController@ajaxCompleteSearchHotels')->name('book.complete_search_hotel');
    Route::post('book/complete-search-flight', 'BookController@ajaxCompleteSearchFlights')->name('book.complete_search_flights');
    Route::get('/book/flight', 'BookController@getFlights')->name('book.flight');
    Route::get('/book/ajaxSearchCity', 'BookController@ajaxSearchCity')->name('book.ajax_search_city');

    Route::get('reports/ajaxGetDraftByID', 'ReportsController@ajaxGetDraftByID');
    Route::get('reports/ajaxGetInfoByID', 'ReportsController@ajaxGetInfoByID');
    Route::get('reports/ajaxGetPlanByID', 'ReportsController@ajaxGetPlanByID');
    Route::get('reports/ajaxGetCountryByID', 'ReportsController@ajaxGetCountryByID');
    Route::get('reports/ajaxGetCityByID', 'ReportsController@ajaxGetCityByID');
    Route::get('reports/ajaxSearchUser', 'ReportsController@ajaxSearchUser');
    Route::get('reports/ajaxSearchMentionUser', 'ReportsController@ajaxSearchMentionUser');
    Route::get('reports/ajaxSearchLocation', 'ReportsController@ajaxSearchLocation');
    Route::post('reports/ajaxGetLocationInfo', 'ReportsController@ajaxGetLocationInfo');
    Route::post('reports/ajaxGetLocationList', 'ReportsController@ajaxGetLocationList');
    Route::get('reports/ajaxSearchPlace', 'ReportsController@ajaxSearchPlace');
    Route::get('reports/ajaxGetUserInfo', 'ReportsController@ajaxGetUserInfo');
    Route::get('reports/list', 'ReportsController@getList')->name('report.list');
    Route::get('reports/updatelist', 'ReportsController@geUpdatetList')->name('report.updatelist');
    Route::get('reports/{id}', 'ReportsController@getShow')->name('report.show');
    Route::get('reports/embed/{id}/{user_id}', 'ReportsController@getEmbedShow')->name('report.embedshow');
    Route::post('reports/{id}/delete', 'ReportsController@postDelete')->name('report.delete');
    Route::get('reports/create', 'ReportsController@getCreate')->name('report.create');
    Route::post('reports/create', 'ReportsController@postCreateEditFirstStep');
    Route::post('reports/create-info', 'ReportsController@postCreateEditSecondStep');
    Route::post('reports/comment', 'ReportsController@postComment');
    Route::post('reports/share', 'ReportsController@postShare')->name('report.share');
    Route::post('reports/like', 'ReportsController@postLike')->name('report.like');
    Route::post('reports/commentdelete', 'ReportsController@postCommentDelete')->name('report.commentdelete');
    Route::post('reports/postcomment', 'ReportsController@postCommentForPlace')->name('report.postcomment');
    Route::post('reports/commentlike', 'ReportsController@postCommentLike')->name('report.commentlike');
    Route::post('/commentlikeusers', 'ReportsController@getCommentLikeUsers')->name('report.commentlikeusers');
    Route::post('/commentedit', 'ReportsController@postCommentEdit')->name('report.commentedit');
    Route::post('/loadMoreComment', 'ReportsController@getMoreComment')->name('report.load_more_comment');
    Route::post('/reportlikes4modal', 'ReportsController@postLikes4Modal')->name('reportlikes4modal');
    Route::post('/reportcommentlikes4modal', 'ReportsController@postCommentLikes4Modal')->name('reportcommentlikes4modal');
    Route::post('/ajax_report_comment4modal', 'ReportsController@ajaxReportComments4Modal')->name('report.ajax_report_comment4modal');
    Route::post('reports/get_search_info', 'ReportsController@getSearchUsersPlaces')->name('report.getSearchUsersPlace');
    Route::post('reports/ajaxGetPlaceInfo', 'ReportsController@ajaxGetPlaceInfo');
    Route::post('/loadMoreCommentLikeUser', 'ReportsController@getMoreCommentLikeUsers')->name('report.load_more_comment_like_users');
    Route::post('/load-more-like-user', 'ReportsController@getMoreReportLikeUsers')->name('report.load_more_report_like_users');
    Route::post('/load-more-plan', 'ReportsController@getMorePlan')->name('report.load_more_plan');
    Route::post('/reports/publish', 'ReportsController@postPublish')->name('report.publish');
    Route::post('/reports/upload-cover', 'ReportsController@postUploadCover')->name('report.upload.cover');
    Route::post('/reports/upload-image', 'ReportsController@postUploadImage')->name('report.upload.image');
    Route::post('/reports/upload-video', 'ReportsController@postUploadVideo')->name('report.upload.video');
    Route::post('/reports/search-report-plan', 'ReportsController@postSearchReportPlan')->name('report.search_report_plan');
    Route::post('/reports/get-filter', 'ReportsController@getReportFilter')->name('reports.get_filter');

    Route::group([
        'middleware' => 'user_blocked',
    ], function () {
        Route::get('/profile', 'ProfileController@getAbout')->name('profile');
        Route::get('/profile/{id}', 'ProfileController@getAbout')->name('profile.show');
        Route::get('/profile-about', 'ProfileController@getAbout')->name('profile.about');
        Route::get('/profile-posts', 'ProfileController@getPosts')->name('profile.posts');
        Route::get('/profile-posts/{id}', 'ProfileController@getPosts');
        Route::get('/profile-liked-posts', 'ProfileController@getLikedPosts')->name('profile.liked.posts');
        Route::get('/profile-liked-posts/{id}', 'ProfileController@getLikedPosts');
        Route::post('/profile/more-liked-posts', 'ProfileController@getMoreLikedPosts')->name('profile.more.liked.posts');
        Route::get('/profile-travel-history', 'ProfileController@getTravelHistory')->name('profile.travel_history');
        Route::get('/profile-travel-history/{id}', 'ProfileController@getTravelHistory');
        Route::get('/profile-map', 'ProfileController@getMap')->name('profile.map');
        Route::get('/profile-map/{id}', 'ProfileController@getMap');
        Route::get('/profile-plans', 'ProfileController@getPlans')->name('profile.plans');
        Route::get('/profile-plans/{id}', 'ProfileController@getPlans');
        Route::post('/profile-plans-invited', 'ProfileController@getPlansInvited')->name('profile.plans_invitations');
        Route::post('/profile-plans-upcoming', 'ProfileController@getPlansUpcoming')->name('profile.plans_upcoming');
        Route::post('/profile-plans-memory', 'ProfileController@getPlansMemory');
        Route::post('/profile-plans-mydraft', 'ProfileController@getMyDraftPlans')->name('profile.plans_my_draft');
        Route::get('/profile-plans-memory/{id}', 'ProfileController@getPlansMemory');
        Route::get('/profile-favourites', 'ProfileController@getFavourites')->name('profile.favourites');
        Route::get('/profile-photos', 'ProfileController@getPhotos')->name('profile.photos');
        Route::get('/profile-photos/{id}', 'ProfileController@getPhotos');
        Route::get('/profile-videos', 'ProfileController@getVideos')->name('profile.videos');
        Route::get('/profile-videos/{id}', 'ProfileController@getVideos');
        Route::get('/profile-interests', 'ProfileController@getInterests')->name('profile.interests');
        Route::get('/profile-reviews', 'ProfileController@getReviews')->name('profile.reviews');
        Route::get('/profile-reviews/{id}', 'ProfileController@getReviews');
        Route::post('/profile/get-more-review', 'ProfileController@getMoreReview');
        Route::post('/profile/{user_id}/check-follow', 'ProfileController@postCheckFollow')->name('profile.check-follow');
        Route::post('/profile/{user_id}/follow', 'ProfileController@postFollow')->name('profile.follow');
        Route::post('/profile/{user_id}/unfollow', 'ProfileController@postUnFollow')->name('profile.unfolllow');
        Route::post('/profile/{user_id}/checkcontent', 'ProfileController@postCheckFollowContent')->name('profile.checkcontent');
        Route::post('/profile/upload_cover', 'ProfileController@postUploadCover');
        Route::post('/profile/upload_profile_image', 'ProfileController@postUploadProfileImage');
        Route::post('/profile/deletecover', 'ProfileController@postDeleteCover');
        Route::post('/profile/{user_id}/update_report', 'ProfileController@updateReport')->name('profile.update_report');
        Route::post('/profile/remove-profile-image', 'ProfileController@deleteProfileImage')->name('remove_profile_image');
        Route::post('/profile/get_comments', 'ProfileController@getMediaComments')->name('profile.get_comments');
        Route::post('/profileGetVisitedCities', 'ProfileController@getVisitedCities')->name('profile.visited.cities');
        Route::post('/profile/update_feed', 'ProfileController@postUpdateFeed');
        Route::post('/profile/getTimlineMedias', 'ProfileController@getTimlineMedias')->name('profile.timeline.medias');
        Route::post('/profile/update-bio', 'ProfileController@postUdateBio');
        Route::post('/profile/update-all-plan', 'ProfileController@updateTripPlan')->name('profile.update.all_plan');
        Route::post('/profile/load-more-photo', 'ProfileController@getMorePhoto')->name('profile.load_more_photo');
        Route::post('/profile/load-more-video', 'ProfileController@getMoreVideos')->name('profile.load_more_video');
        Route::post('/profile/load-more-follow', 'ProfileController@getMoreFollow')->name('profile.load_more_follow');
        Route::post('/profile/load-more-following', 'ProfileController@getMoreFollowing')->name('profile.load_more_following');
        Route::post('/profile/get-review-media', 'ProfileController@getReviewMedia')->name('profile.get_review_media');
        Route::post('/profile/getExpertiseLocations', 'ProfileController@getExpertiseLocations');
        Route::post('/profile/postBecomeExpert', 'ProfileController@postBecomeExpert');
    });

    Route::get('/travelmates-new', 'TravelMatesController@getIndexNew')->name('travelmate.new');
    Route::get('/travelmates-newest', 'TravelMatesController@getIndexNewFunc')->name('travelmate.newest');
    Route::get('/travelmates-new-func-ajax', 'TravelMatesController@ajaxGetIndexNewFunc')->name('travelmate.new.func.ajax');
    Route::get('/travelmates-get-filters-ajax', 'TravelMatesController@ajaxGetFilters')->name('travelmate.filters.ajax');
    Route::get('/travelmates-get-order-filter-ajax', 'TravelMatesController@ajaxGetOrderFilter')->name('travelmate.new.order.filter');
    Route::get('/travelmates', 'TravelMatesController@getIndexNewFunc')->name('travelmate');
    Route::get('/travelmates-old', 'TravelMatesController@getNewest')->name('travelmate.old');
    Route::get('/travelmates-trending', 'TravelMatesController@getTrending')->name('travelmate.trending');
    Route::get('/travelmates-soon', 'TravelMatesController@getSoon')->name('travelmate.soon');
    Route::get('/travelmates-notifications-invitations', 'TravelMatesController@getNotificationsInvitations')->name('travelmate.notifications_invitations');
    Route::get('/travelmates-notifications-requests', 'TravelMatesController@getNotificationsRequests')->name('travelmate.notifications_requests');
    Route::get('/ajax-travelmates-notifications-requests', 'TravelMatesController@ajaxGetNotificationsRequests')->name('travelmate.ajax.notifications_requests');
    Route::get('/ajax-travelmates-notifications-invitations', 'TravelMatesController@ajaxGetNotificationsInvitations')->name('travelmate.ajax.notifications_invitations');
    Route::get('/travelmates-notifications-approved', 'TravelMatesController@getApprovedRequests')->name('travelmate.notifications_approved');
    Route::get('/travelmates-request', 'TravelMatesController@getRequest')->name('travelmate.request');
    Route::post('/travelmates-search', 'TravelMatesController@postSearch')->name('travelmate.search');
    Route::post('/travelmates/dorequest', 'TravelMatesController@postDoRequest')->name('travelmate.dorequest');
    Route::post('/travelmates/join', 'TravelMatesController@postJoin')->name('travelmate.join');
    Route::post('/travelmates/ajaxjoin', 'TravelMatesController@postAjaxJoin')->name('travelmate.ajaxjoin');
    Route::post('/travelmates/ajax_remove_mate', 'TravelMatesController@postAjaxRemoveMate')->name('travelmate.ajax_remove_mate');
    Route::post('/travelmates/ajax_cancel_invitation', 'TravelMatesController@postAjaxCancelInvitation')->name('travelmate.ajax_cancel_invitation');
    Route::post('/travelmates/ajax-close-request', 'TravelMatesController@postAjaxCloseRequest')->name('travelmate.ajax_close_requesr');
    Route::post('/travelmates/ajax-unpublish-request', 'TravelMatesController@postAjaxUnpublishRequest')->name('travelmate.ajax_unpublish_requesr');
    Route::post('/travelmates/ajax-cancel-request', 'TravelMatesController@postAjaxCancelRequest')->name('travelmate.ajax_cancel_requesr');
    Route::post('/travelmates/approve-request', 'TravelMatesController@postAjaxApproveRequest')->name('travelmate.approve_request');
    Route::post('/travelmates/disapprove-request', 'TravelMatesController@postAjaxDisapproveRequest')->name('travelmate.disapprove_request');
    Route::post('/travelmates/leave-trip-plan', 'TravelMatesController@postAjaxLeaveTripPlan')->name('travelmate.leave_request');
    Route::get('travelmates/ajaxGetSearchCity', 'TravelMatesController@ajaxGetSearchCity');
    Route::get('travelmates/ajaxGetSearchCountry', 'TravelMatesController@ajaxGetSearchCountry');
    Route::post('travelmates/create-without-plan', 'TravelMatesController@createWithoutPlan')->name('travelmate.create_without_plan');
    Route::get('/ajax-travelmates-notifications-my-invitation', 'TravelMatesController@ajaxGetNotificationsInvitations')->name('travelmate.ajax.notifications_my_invitations');
    Route::get('/ajax-travelmates-my-requests', 'TravelMatesController@ajaxGetMyRequests')->name('travelmate.ajax.my_requests');
    Route::get('/ajax-travelmates-my-invitations', 'TravelMatesController@ajaxGetMyInvitations')->name('travelmate.ajax.my_invitations');
    Route::post('/ajax-travelmates-cancel-all-invitations', 'TravelMatesController@ajaxCancelAllInvitations')->name('travelmate.ajax_cancel_all_invitation');


    Route::post('/trip/plan/{id}/media', 'TripController@postUploadMedia');
    Route::delete('/trip/plan/media', 'TripController@deleteTripPlaceMedia')->name('trip_place_media_delete');
    Route::get('/trip/plan/{id}', 'TripController@getCreateTrip')->name('trip.plan');
    Route::get('/trip/plan/{id}/{city_id}', 'TripController@getCreateTrip')->name('trip.plan_city');
    Route::get('/trip/view/{id}', 'TripController@getIndex')->name('trip.view');
    Route::post('/trip/version-respond', 'TripController@postVersionRespond')->name('trip.version_respond');
    Route::post('/trip/ajaxEditTrip', 'TripController@postAjaxEditTrip')->name('trip.ajax_edit_trip');
    Route::post('/trip/ajaxFirstStep', 'TripController@postAjaxFirstStep')->name('trip.ajax_first_step');
    Route::post('/trip/ajaxSearchCities', 'TripController@postAjaxSearchCities')->name('trip.ajax_search_cities');
    Route::get('/trip/ajaxSearchCitiesForSelect', 'TripController@ajaxSearchCitiesForSelect');
    Route::get('/trip/ajaxSearchPlaces', 'TripController@postAjaxSearchPlaces')->name('trip.ajax_search_places');
    Route::post('/trip/ajaxAddCityToTrip', 'TripController@postAjaxAddCityToTrip')->name('trip.ajax_add_city_to_trip');
    Route::post('/trip/ajaxAddPlaceToTrip', 'TripController@postAjaxAddPlaceToTrip')->name('trip.ajax_add_place_to_trip');
    //Route::get('/trip/ajaxAddPlaceToTrip', 'TripController@postAjaxAddPlaceToTrip')->name('trip.ajax_add_place_to_trip');
    Route::post('/trip/ajaxRemovePlaceFromTrip', 'TripController@postajaxRemovePlaceFromTrip')->name('trip.ajaxRemovePlaceFromTrip');
    Route::post('/trip/ajax-dismiss-place-from-trip', 'TripController@postAjaxDismissPlaceFromTrip')->name('trip.ajax_dismiss_place_from_trip');
    Route::post('/trip/ajaxActivateTripCity', 'TripController@postAjaxActivateTripCity')->name('trip.ajax_activate_trip_city');
    Route::post('/trip/ajaxDeActivateTripCity', 'TripController@postAjaxDeActivateTripCity')->name('trip.ajax_deactivate_trip_city');
    Route::post('/trip/ajaxActivateTripPlace', 'TripController@postAjaxActivateTripPlace')->name('trip.ajax_activate_trip_place');
    Route::post('/trip/ajaxDeActivateTripPlace', 'TripController@postAjaxDeActivateTripPlace')->name('trip.ajax_deactivate_trip_place');
    Route::post('/trip/ajaxDeleteTrip', 'TripController@postAjaxDeleteTrip')->name('trip.ajax_delete_trip');
    Route::post('/trip/ajaxActivateTrip', 'TripController@postAjaxActivateTrip')->name('trip.ajax_activate_trip');
    Route::post('/trip/publish-trip-place', 'TripController@postAjaxPublishTripPlace')->name('trip.ajax_publish_trip_place');
    Route::post('/trip/ajaxApproveSuggestion', 'TripController@ajaxApproveSuggestion')->name('trip.ajax_approve_suggestion');
    Route::post('/trip/ajaxDisapproveSuggestion', 'TripController@ajaxDisapproveSuggestion')->name('trip.ajax_disapprove_suggestion');
    Route::put('/trip/ajaxChangeInvitationRequestRole', 'TripController@ajaxChangeInvitationRequestRole')->name('trip.ajax_change_invitation_request_role');
    Route::post('/trip/ajaxInviteFriends', 'TripController@postAjaxInviteFriends')->name('trip.ajax_invite_friends');
    Route::get('/trip/ajaxPeopleToInvite', 'TripController@getAjaxPeopleToInvite')->name('trip.ajax_people_to_invite');
    Route::get('/trip/ajaxInvitedPeople', 'TripController@getAjaxInvitedPeople')->name('trip.ajax_invited_people');
    Route::post('/trip/ajaxAcceptInvitation', 'TripController@postAjaxAcceptInvitation')->name('trip.ajax_accept_invitation');
    Route::post('/trip/ajaxRemoveCityFromTrip', 'TripController@postAjaxRemoveCityFromTrip')->name('trip.ajax_remove_city_from_trip');
    Route::post('/trip/ajaxRejectInvitation', 'TripController@postAjaxRejectInvitation')->name('trip.ajax_reject_invitation');
    Route::post('/trip/ajaxCancelInvitation', 'TripController@postAjaxCancelInvitation')->name('trip.ajax_cancel_invitation');
    Route::post('/trip/ajaxLeavePlan', 'TripController@postAjaxLeavePlan')->name('trip.ajax_leave_plan');
    Route::post('/trip/likeunlike', 'TripController@postLikeUnLike')->name('trip.likeunlike');
    Route::post('/trip/comment', 'TripController@postComment')->name('trip.comment');
    Route::post('/trip/commentreply', 'TripController@postCommentReply')->name('trip.postcommentreply');
    Route::post('/trip/commentedit', 'TripController@postCommentEdit')->name('trip.commentedit');
    Route::post('/trip/commentlikeunlike', 'TripController@postCommentLikeUnlike')->name('trip.commentlikeunlike');
    Route::post('/trip/comment4tripbox', 'TripController@postComment4Tripbox')->name('trip.comment4tripbox');
    Route::post('/trip/commentdelete', 'TripController@postCommentDelete')->name('trip.commentdelete');
    Route::post('/trip/comment-updownvote', 'TripController@postCommentUpDownVotes')->name('trip.comment_updownvote');
    Route::post('/trip/shareunshare', 'TripController@postShareUnshare')->name('trip.shareunshare');
    Route::post('/trip/postTripDistance', 'TripController@postTripDistance')->name('trip.distance');
    Route::post('/trip/ajaxTripDescription', 'TripController@posAjaxTripDescription')->name('trip.ajax_get_description');
    Route::post('/trip/post-trip-description', 'TripController@postTripDescription')->name('trip.description');
    Route::post('/trip/postAjaxAddComment', 'TripController@postAjaxAddComment')->name('trip.add_comment');
    Route::get('/trip/ajaxGetPlanContents', 'TripController@ajaxGetPlanContents');
    Route::get('/trip/ajaxGetPlaceById/{place_id}', 'TripController@ajaxGetPlaceById');
    Route::get('/trip/ajax-get-plan-data/{plan_id}', 'TripController@ajaxGetPlanData');
    Route::get('/trip/ajaxGetTripPlaceByProviderId', 'TripController@ajaxGetTripPlaceByProviderId');
    Route::get('/trip/ajaxGetTripPlace', 'TripController@ajaxGetTripPlace');
    Route::post('/trip/ajaxEditPlaceInTrip', 'TripController@postAjaxEditPlaceInTrip');
    Route::post('/trip/commentlikeusers', 'TripController@getCommentLikeUsers')->name('trip.commentlikeusers');
    Route::post('/trip/save-draft', 'TripController@saveDraft')->name('trip.save_draft');
    Route::post('/trip/undo', 'TripController@undoChanges')->name('trip.undo');
    Route::post('/trip/cancel-changes', 'TripController@cancelChanges')->name('trip.cancel_changes');
    Route::post('/trip/send-suggestions', 'TripController@sendSuggestions')->name('trip.send_suggestions');
    Route::get('/trip/activity-logs', 'TripController@getPlanActivityLog')->name('trip.activity_logs');
    Route::get('/trip/getNearbyHotels', 'TripController@getNearbyHotels');
    Route::get('/trip/getNearbyPlaces', 'TripController@getNearbyPlaces');
    Route::get('/trip/getNearbyTopPlaces', 'TripController@getNearbyTopPlaces');
    Route::post('/trip/likes4modal', 'TripController@postLikes4Modal')->name('trip_likes4modal');

    Route::post('/trip/trending-locations', 'TripController@getTrendingLocations')->name('trip.trending_locations');

    Route::post('/place-chats/createPlaceChatMessage', 'PlaceChat\PlaceChatsController@ajaxCreatePlaceChatMessage')->name('place_chat_send_message');
    Route::post('/place-chats/readMessage', 'PlaceChat\PlaceChatsController@ajaxReadMessage')->name('plan_place_read_message');
    Route::get('/place-chats/getPlaceChatMessages', 'PlaceChat\PlaceChatsController@ajaxGetPlaceChatMessages')->name('place_chat_messages');
    Route::get('/place-chats/getPlaceChatUsers', 'PlaceChat\PlaceChatsController@ajaxGetPlaceChatUsers')->name('place_chat_users');
    Route::get('/place-chats/getPlaceUsersToChat', 'PlaceChat\PlaceChatsController@ajaxGetPlaceUsersToChat')->name('place_chat_users');
    Route::get('/place-chats/getPlanUsersToChat', 'PlaceChat\PlaceChatsController@ajaxGetPlanUsersToChat')->name('plan_chat_users');
    Route::get('/place-chats/getPlanPlaceUsersToChat', 'PlaceChat\PlaceChatsController@ajaxGetPlanPlaceUsersToChat')->name('plan_place_chat_users');
    Route::get('/place-chats/ajaxGetActualChats', 'PlaceChat\PlaceChatsController@ajaxGetActualChats')->name('plan_place_chat_actual_chats');


    Route::get('/checkins/place/{id}', 'CheckinsController@getPlaceCheckins')->name('checkins.get_place_checkins');
    Route::get('/visits/place/{id}', 'VisitsController@getPlaceVisits')->name('visits.get_place_visits');
    Route::get('/visits/count/{id}', 'VisitsController@getPlaceVisitsCount')->name('visits.get_place_visits_count');
    Route::get('/visits/thumbs/{id}', 'VisitsController@getVisitsThumbs')->name('visits.get_place_visits_thumbs');

    Route::get('/suggestions', 'SuggestionsController@getSuggestions')->name('suggestions');
    Route::post('/suggestions', 'SuggestionsController@suggest')->name('suggestions.suggest');
    Route::post('/suggestions/read', 'SuggestionsController@read')->name('suggestions.read');

    Route::post('/trip/share-plan', 'TripShareController@sharePlan')->name('trip.share.plan');
    Route::post('/trip/share-media', 'TripShareController@shareMedia')->name('trip.share.media');
    Route::post('/trip/share-place', 'TripShareController@sharePlace')->name('trip.share.place');

    //Global trip plans
    Route::get('/trip-plans', 'GlobalTripController@getIndex')->name('globaltrip.index');
    Route::get('/load-more-trip-plans', 'GlobalTripController@getMoreTripPlan')->name('globaltrip.get_more_plan');
    Route::get('/trip-plans-filter', 'GlobalTripController@getFilterTripPlan')->name('globaltrip.get_filter');
    Route::get('/get-search-countries', 'GlobalTripController@getSearchCountries')->name('globaltrip.get_search_countries');

    Route::get('/settings', 'SettingsController@getIndex')->name('setting');
    Route::post('/settings/account', 'SettingsController@postAccount')->name('setting.account');
    Route::post('/settings/social', 'SettingsController@postSocial')->name('setting.social');
    Route::post('/settings/delete-account', 'SettingsController@postDeleteAccount')->name('setting.delete_account');
    Route::post('/settings/security', 'SettingsController@postSecurity')->name('setting.security');
    Route::post('/settings/privacy', 'SettingsController@postPrivacy')->name('setting.privacy');
    Route::post('/settings/blocking', 'SettingsController@postBlockingUsers')->name('setting.blocking_users');
    Route::post('/settings/unBlocking', 'SettingsController@postUnBlockingUsers')->name('setting.un_blocking_users');
    Route::get('/settings/get-more-blocking-user', 'SettingsController@getMoreBlockingUser');
    Route::get('/settings/searchUsers', 'SettingsController@ajaxSearchUser');
    Route::get('/settings/search-regional-language', 'SettingsController@ajaxSearchRegionalLanguage');

    Route::get('access-denied', 'ErrorsHandlerController@getAccessDeniedPage');
    Route::get('private-plan', 'ErrorsHandlerController@getPrivateTripPlanPage');
    Route::get('plan-access-denied', 'ErrorsHandlerController@getPlanAccessDeniedPage');
    Route::get('report-access-denied', 'ErrorsHandlerController@getRepertAccessDeniedPage');
    Route::get('permission-denied', 'ErrorsHandlerController@getPermissionDeniedPage');
    Route::get('content-deleted', 'ErrorsHandlerController@getContentDeletedPage');
    Route::get('account-deleted', 'ErrorsHandlerController@getAccountDeletedPage');
    Route::fallback(function () {
        return response()->view('errors.404', [], 404);
    });
    Route::get('under-maintenance', function () {
        return view('errors.under-maintenance');
    });

    Route::get('/users/autocomplete', 'UsersController@getAutoComplete')->name('user.autocomplete');

    //Route::get('/log', 'LogController@getIndex');
    Route::get('/settings', 'SettingsController@getIndex')->name('setting');
    Route::get('/post-view/{postId}', 'HomeController@getPostView')->name('post-view');


    Route::get('/dashboard', 'DashboardController@getIndex')->name('dashboard');
    Route::get('/get-more-affiliate', 'DashboardController@ajaxGetMoreAffiliate')->name('dashboard.ajax_get_affiliate');
    Route::get('/dashboard/get-overview', 'DashboardController@getOverview')->name('dashboard.get_overview');
    Route::get('/dashboard/get-engagement', 'DashboardController@getEngagement')->name('dashboard.get_engagement');
    Route::get('/dashboard/get-engagement-filter', 'DashboardController@getEngagementFilter')->name('dashboard.get_engagement_filter');
    Route::get('/dashboard/get-ranking', 'DashboardController@getRanking')->name('dashboard.get_ranking');
    Route::post('/dashboard/ajax-apply-to-badge', 'DashboardController@ajaxApplyToBadge')->name('dashboard.apply_to_badge');
    Route::get('/dashboard/get-interaction', 'DashboardController@getInteraction')->name('dashboard.get_interactions');
    Route::get('/dashboard/get-interaction-filter', 'DashboardController@getInteractionFilter')->name('dashboard.get_interaction_filter');
    Route::get('/dashboard/get-posts', 'DashboardController@getPosts')->name('dashboard.get_posts');
    Route::get('/dashboard/get-reports', 'DashboardController@getReports')->name('dashboard.get_reports');
    Route::get('/dashboard/get-followers', 'DashboardController@getFollowers')->name('dashboard.get_followers');
    Route::get('/dashboard/get-follower-filter', 'DashboardController@getFollowersFilter')->name('dashboard.get_followers_filter');
    Route::get('/dashboard/load-more-followers', 'DashboardController@getMoreFollowers')->name('dashboard.load_more_followers');
    Route::get('/dashboard/get-globalImpact', 'DashboardController@getGlobalImpact')->name('dashboard.get_global_impact');
    Route::get('/dashboard/load-more-leader', 'DashboardController@getMoreLeader')->name('dashboard.load_more_leader');
    Route::post('/dashboard/get-engagement-diginfo', 'DashboardController@getEngagementDigInfo')->name('dashboard.get_engagement_diginfo');

    Route::get('/expert', 'ExpertController@getIndex')->name('expert');

    Route::group(['prefix' => 'help', 'as' => 'help.'], function () {
        Route::get('/', 'HelpCenterController@index')->name('index');
        Route::post('/help_content/{id?}/likedislike', "HelpCenterController@likeDislike")->name("likeDislike");
        Route::get('/search', 'HelpCenterController@helpSearch')->name('search');
    });

    Route::group(['middleware' => 'auth:user', 'prefix' => 'chat', 'as' => 'chat.'], function () {
        Route::get('/{conversation_id?}', ['uses' => 'ChatPrivateController@index'])->name('index');
        Route::post('/send', 'ChatPrivateController@send')->name('send');
        Route::get('/post-sent-users/{post_type}/{post_id}', 'ChatPrivateController@getPostSentUsers')->name('sent-posts');
        Route::post('/has-conversation-id', 'ChatPrivateController@hasConversationId')->name('has_conversation_id');
        //Route::post('/start', 'ChatPrivateController@start');
        Route::get('/messages/{chat_conversation_id}', 'ChatPrivateController@getMessages')->name('messages');
        Route::get('/dropdown/participants', 'ChatPrivateController@getParticipantsCombo')->name('participants');
        Route::get('/dropdown/participants_image', 'ChatPrivateController@getParticipantsComboImage')->name('participants_image');
        Route::get('/last/{number}', 'ChatPrivateController@getLastFive')->name('last');
        Route::get('/conversation/{conversation_id}/delete', 'ChatPrivateController@deleteConversation')->name('conversation_delete');
        Route::get('/new/{user_id}', 'ChatPrivateController@createNewConversation')->name('new');
    });

    // Route::group(['prefix' => 'country', 'as' => 'country.'], function () {
    //     Route::get('/ajax_get_media', 'CountryController2@ajaxGetMedia');
    //     Route::get('/ajax_get_poi_media', 'CountryController2@ajaxGetPOIMedia');

    //     Route::get('/{country_id}', 'CountryController2@getIndex')->name('index');
    //     Route::get('/{country_id}/weather', 'CountryController2@getWeather')->name('weather');
    //     Route::get('/{country_id}/packing-tips', 'CountryController2@getPackingTips')->name('packing_tips');
    //     Route::get('/{country_id}/etiquette', 'CountryController2@getEtiquette')->name('etiquette');
    //     Route::get('/{country_id}/health', 'CountryController2@getHealth')->name('health');
    //     Route::get('/{country_id}/visa-requirements', 'CountryController2@getVisaRequirements')->name('visa_requirements');
    //     Route::get('/{country_id}/when-to-go', 'CountryController2@getWhenToGo')->name('when_to_go');
    //     Route::get('/{country_id}/caution', 'CountryController2@getCaution')->name('caution');

    //     Route::post('/{country_id}/check-checkin', 'CountryController2@postCheckCheckin')->name('check_checkin');
    //     Route::post('/{country_id}/checkin', 'CountryController2@postCheckin')->name('checkin');
    //     Route::post('/{country_id}/checkout', 'CountryController2@postCheckout')->name('checkout');

    //     Route::post('/{country_id}/check-follow', 'CountryController2@postCheckFollow')->name('check_follow');
    //     Route::post('/{country_id}/follow', 'CountryController2@postFollow')->name('follow');
    //     Route::post('/{country_id}/unfollow', 'CountryController2@postUnFollow')->name('unfollow');
    //     Route::post('/{country_id}/visa-requirements', 'CountryController2@postVisaRequirements')->name('visa_requirements');
    //     Route::post('/{country_id}/weather', 'CountryController2@postWeather')->name('weather');
    //     Route::post('/{country_id}/talking-about', 'CountryController2@postTalkingAbout')->name('talking_about');
    //     Route::post('/{country_id}/now-in-place', 'CountryController2@postNowInPlace')->name('now_in_place');
    //     Route::post('/{country_id}/contribute', 'CountryController2@postContribute')->name('contribute');
    //     Route::post('/{country_id}/ajaxPostReview', 'CountryController2@ajaxPostReview')->name('ajax_post_review');
    //     Route::post('/{country_id}/ajaxHappeningToday', 'CountryController2@ajaxHappeningToday')->name('ajax_happening_today');
    //     Route::post('/{country_id}/ajaxPostMorePhoto', 'CountryController2@ajaxPostMorePhoto')->name('ajax_more_photo');
    //     Route::post('/ajaxDiscussionloadmore', 'CountryController2@ajaxDiscussionTipLoadmore')->name('ajax_discuss_loadmore');
    //     Route::post('/ajaxMediaComments', 'CountryController2@ajaxMediaComments')->name('ajax_media_comment');
    //     Route::post('/{country_id}/update_post', 'CountryController2@ajaxUpdatePosts')->name('update_post');
    //     Route::post('/save-review', 'CountryController2@saveReview')->name('save-review');
    //     Route::post('/share-review', 'CountryController2@shareReview')->name('share-review');
    // });

    // Route::group(['prefix' => 'city', 'as' => 'city.'], function () {
    //     Route::get('/{city_id}', 'CityController@getIndex')->name('index');
    //     Route::get('/{city_id}/weather', 'CityController@getWeather')->name('weather');
    //     Route::get('/{city_id}/packing-tips', 'CityController@getPackingTips')->name('packing_tips');
    //     Route::get('/{city_id}/etiquette', 'CityController@getEtiquette')->name('etiquette');
    //     Route::get('/{city_id}/health', 'CityController@getHealth')->name('health');
    //     Route::get('/{city_id}/visa-requirements', 'CityController@getVisaRequirements')->name('visa_requirements');
    //     Route::get('/{city_id}/when-to-go', 'CityController@getWhenToGo')->name('when_to_go');
    //     Route::get('/{city_id}/caution', 'CityController@getCaution')->name('caution');

    //     Route::post('/{city_id}/check-follow', 'CityController@postCheckFollow')->name('check_follow');
    //     Route::post('/{city_id}/follow', 'CityController@postFollow')->name('follow');
    //     Route::post('/{city_id}/unfollow', 'CityController@postUnFollow')->name('unfollow');
    //     Route::post('/{city_id}/visa-requirements', 'CityController@postVisaRequirements')->name('visa_requirements');
    //     Route::post('/{city_id}/weather', 'CityController@postWeather')->name('weather');
    //     Route::post('/{city_id}/talking-about', 'CityController@postTalkingAbout')->name('talking_about');
    //     Route::post('/{city_id}/now-in-city', 'CityController@postNowInCity')->name('now_in_city');
    //     Route::post('/{city_id}/contribute', 'CityController@postContribute')->name('contribute');
    // });

    $prefixs = ['place', 'country', 'city', 'places'];
    foreach ($prefixs as $prefix) {
        Route::group(['prefix' => $prefix, 'as' => $prefix . '.'], function () use ($prefix) {
            if ($prefix !== 'places') {
                Route::get('/{place_id}', 'PlaceController2@getIndex')->name('index');
            }

            Route::get('/{place_id}/weather', 'PlaceController2@getWeather')->name('weather');
            Route::get('/{place_id}/packing-tips', 'PlaceController2@getPackingTips')->name('packing_tips');
            Route::get('/{place_id}/etiquette', 'PlaceController2@getEtiquette')->name('etiquette');
            Route::get('/{place_id}/health', 'PlaceController2@getHealth')->name('health');
            Route::get('/{place_id}/visa-requirements', 'PlaceController2@getVisaRequirements')->name('visa_requirements');
            Route::get('/{place_id}/when-to-go', 'PlaceController2@getWhenToGo')->name('when_to_go');
            Route::get('/{place_id}/caution', 'PlaceController2@getCaution')->name('caution');

            Route::post('/{place_id}/check-checkin', 'PlaceController2@postCheckCheckin')->name('check_checkin');
            Route::post('/{place_id}/checkin', 'PlaceController2@postCheckin')->name('checkin');
            Route::post('/{place_id}/checkout', 'PlaceController2@postCheckout')->name('checkout');
            Route::post('/{place_id}/addpost', 'PlaceController2@postAddPost')->name('addpost');
            Route::post('/upload-media-content', 'PlaceController2@uploadMedia')->name('upload-media-content');
            Route::post('/{place_id}/share', 'PlaceController2@postShare')->name('share');
            Route::post('/postcomment', 'PlaceController2@postComment')->name('postcomment');
            Route::post('/postcommentedit', 'PlaceController2@postCommentEdit')->name('postcommentedit');
            Route::post('/postcommentreply', 'PlaceController2@postCommentReply')->name('postcommentreply');

            Route::post('/{place_id}/check-follow', 'PlaceController2@postCheckFollow')->name('check_follow');
            Route::post('/{place_id}/follow', 'PlaceController2@postFollow')->name('follow');
            Route::post('/{place_id}/unfollow', 'PlaceController2@postUnFollow')->name('unfollow');
            Route::post('/{place_id}/visa-requirements', 'PlaceController2@postVisaRequirements')->name('visa_requirements');
            Route::post('/{place_id}/weather', 'PlaceController2@postWeather')->name('weather');
            Route::post('/{place_id}/talking-about', 'PlaceController2@postTalkingAbout')->name('talking_about');
            Route::post('/{place_id}/now-in-place', 'PlaceController2@postNowInPlace')->name('now_in_place');
            Route::post('/{place_id}/contribute', 'PlaceController2@postContribute')->name('contribute');
            Route::post('/{place_id}/ajaxPostReview', 'PlaceController2@ajaxPostReview')->name('ajax_post_review');
            Route::post('/ajaxEditReview/{review?}', 'PlaceController2@ajaxEditReview')->name('ajax_edit_review');
            Route::post('/ajaxDeleteReview/{review?}', 'PlaceController2@ajaxDeleteReview')->name('ajax_delete_review');
            Route::post('/{place_id}/ajaxHappeningToday', 'PlaceController2@ajaxHappeningToday')->name('ajax_happening_today');
            Route::post('/{place_id}/ajaxPostMorePhoto', 'PlaceController2@ajaxPostMorePhoto')->name('ajax_more_photo');
            Route::post('/ajaxDiscussionloadmore', 'PlaceController2@ajaxDiscussionTipLoadmore')->name('ajax_discuss_loadmore');
            Route::post('/ajaxMediaComments', 'PlaceController2@ajaxMediaComments')->name('ajax_media_comment');
            Route::post('/revirews-updownvote', 'PlaceController2@postReviewUpDownVote')->name('revirews_updownvote');
            Route::post('/{place_id}/update_post', 'PlaceController2@ajaxUpdatePosts')->name('update_post');

            Route::post('/save-review', 'PlaceController2@saveReview')->name('save-review');
            Route::post('/share-review', 'PlaceController2@shareReview')->name('share-review');

            Route::post('/searchall', 'PlaceController2@searchAllDatas')->name('searchall');
            Route::post('/get64image', 'PlaceController2@get64Image')->name('get64image');

            Route::post('/medialike', 'PlaceController2@ajaxMediaLikes')->name('medialike');
            Route::post('/eventlike', 'PlaceController2@ajaxEventLikes')->name('eventlike');
            Route::post('/eventshare', 'PlaceController2@ajaxEventShares')->name('eventshare');
            Route::post('/eventcomment', 'PlaceController2@ajaxEventComments')->name('eventcomment');
            Route::post('/eventcommentreply', 'PlaceController2@ajaxEventCommentsReply')->name('eventcommentreply');
            Route::post('/eventcommentedit', 'PlaceController2@ajaxEventCommentsEdit')->name('eventcommentedit');
            Route::post('/eventcommentlikeunlike', 'PlaceController2@ajaxEventCommentsLike')->name('eventcommentlikeunlike');
            Route::post('/eventcommentdelete', 'PlaceController2@ajaxEventCommentsDelete')->name('eventcommentdelete');
            Route::post('/ajax_event_comment4modal', 'PlaceController2@ajaxEventComments4Modal')->name('ajax_event_comment4modal');
            Route::post('/eventcommentlikeusers', 'PlaceController2@getCommentLikeUsers')->name('commentlikeusers');

            Route::post('/{place_id}/ajax_check_event', 'PlaceController2@ajaxCheckEvent')->name('ajax_check_event');
        });
    }

    $prefixs = ['places', 'country', 'city'];
    foreach ($prefixs as $prefix) {
        Route::group(['prefix' => $prefix, 'as' => $prefix . '.'], function () {
            Route::get('/ajax_get_media', 'PlaceController2@ajaxGetMedia');
            Route::post('/ajax_get_poi_media', 'PlaceController2@ajaxGetPOIMedia');
            Route::get('/ajax_set_poi_media', 'PlaceController2@ajaxSetPOIMedia');

            Route::get('/{place_id}', 'PlaceController2@getIndex')->name('index');
            Route::get('/{place_id}/weather', 'PlaceController2@getWeather')->name('weather');
            Route::get('/{place_id}/packing-tips', 'PlaceController2@getPackingTips')->name('packing_tips');
            Route::get('/{place_id}/etiquette', 'PlaceController2@getEtiquette')->name('etiquette');
            Route::get('/{place_id}/health', 'PlaceController2@getHealth')->name('health');
            Route::get('/{place_id}/visa-requirements', 'PlaceController2@getVisaRequirements')->name('visa_requirements');
            Route::get('/{place_id}/when-to-go', 'PlaceController2@getWhenToGo')->name('when_to_go');
            Route::get('/{place_id}/caution', 'PlaceController2@getCaution')->name('caution');

            Route::post('/{place_id}/check-checkin', 'PlaceController2@postCheckCheckin')->name('check_checkin');
            Route::post('/{place_id}/checkin', 'PlaceController2@postCheckin')->name('checkin');
            Route::post('/{place_id}/checkout', 'PlaceController2@postCheckout')->name('checkout');

            Route::post('/{place_id}/check-follow', 'PlaceController2@postCheckFollow')->name('check_follow');
            Route::post('/{place_id}/follow', 'PlaceController2@postFollow')->name('follow');
            Route::post('/{place_id}/unfollow', 'PlaceController2@postUnFollow')->name('unfollow');
            Route::post('/{place_id}/visa-requirements', 'PlaceController2@postVisaRequirements')->name('visa_requirements');
            Route::post('/{place_id}/weather', 'PlaceController2@postWeather')->name('weather');
            Route::post('/{place_id}/talking-about', 'PlaceController2@postTalkingAbout')->name('talking_about');
            Route::post('/{place_id}/now-in-place', 'PlaceController2@postNowInPlace')->name('now_in_place');
            Route::post('/{place_id}/contribute', 'PlaceController2@postContribute')->name('contribute');
            Route::post('/{place_id}/ajaxPostReview', 'PlaceController2@ajaxPostReview')->name('ajax_post_review');
            Route::post('/{place_id}/ajaxHappeningToday', 'PlaceController2@ajaxHappeningToday')->name('ajax_happening_today');
            Route::post('/{place_id}/ajaxPostMorePhoto', 'PlaceController2@ajaxPostMorePhoto')->name('ajax_more_photo');
            Route::post('/ajaxDiscussionloadmore', 'PlaceController2@ajaxDiscussionTipLoadmore')->name('ajax_discuss_loadmore');
            Route::post('/ajaxMediaComments', 'PlaceController2@ajaxMediaComments')->name('ajax_media_comment');
            Route::post('/{place_id}/update_post', 'PlaceController2@ajaxUpdatePosts')->name('update_post');
            Route::post('/ajaxgettripdata', 'PlaceController2@ajaxGetTripData')->name('ajaxgettripdata');
            Route::get('/{place_id}/discussion_replies_24hrs', 'PlaceController2@discussion_replies_24hrs');
            Route::get('/{place_id}/newsfeed_show_all', 'PlaceController2@newsfeed_show_all');
            Route::get('/{place_id}/newsfeed_show_about', 'PlaceController2@newsfeed_show_about');
            Route::get('/{place_id}/newsfeed_show_discussions', 'PlaceController2@newsfeed_show_discussions');
            Route::get('/{place_id}/newsfeed_show_top', 'PlaceController2@newsfeed_show_top');
            Route::get('/{place_id}/newsfeed_show_media', 'PlaceController2@newsfeed_show_media');
            Route::get('/{place_id}/newsfeed_show_tripplan', 'PlaceController2@newsfeed_show_tripplan');
            Route::get('/{place_id}/newsfeed_show_travelmates', 'PlaceController2@newsfeed_show_travelmates');
            Route::get('/{place_id}/newsfeed_show_events', 'PlaceController2@newsfeed_show_events');
            Route::get('/{place_id}/newsfeed_show_reports', 'PlaceController2@newsfeed_show_reports');
            Route::get('/{place_id}/newsfeed_show_reviews', 'PlaceController2@newsfeed_show_reviews');
            Route::get('/{place_id}/get-place-about', 'PlaceController2@getPlaceAboutInfo');

            Route::post('/ajaxMediaComments4modal', 'PlaceController2@ajaxMediaComments4Modal')->name('ajax_media_comment4modal');
            Route::post('/ajaxTripComments4modal', 'PlaceController2@ajaxTripComments4Modal')->name('ajax_trip_comment4modal');
            Route::post('/get-post-detail', 'PlaceController2@ajaxGetPostById')->name('get-post-detail');
        });
    }

    Route::group(['prefix' => 'hotel', 'as' => 'hotel.'], function () {
        Route::get('/ajax_get_media', 'HotelController@ajaxGetMedia');

        Route::get('/{hotel_id}', 'HotelController@getIndex')->name('index');
        Route::get('/{hotel_id}/weather', 'HotelController@getWeather')->name('weather');
        Route::get('/{hotel_id}/packing-tips', 'HotelController@getPackingTips')->name('packing_tips');
        Route::get('/{hotel_id}/etiquette', 'HotelController@getEtiquette')->name('etiquette');
        Route::get('/{hotel_id}/health', 'HotelController@getHealth')->name('heath');
        Route::get('/{hotel_id}/visa-requirements', 'HotelController@getVisaRequirements')->name('visa_requirements');
        Route::get('/{hotel_id}/when-to-go', 'HotelController@getWhenToGo')->name('when_to_go');
        Route::get('/{hotel_id}/caution', 'HotelController@getCaution')->name('caution');

        Route::post('/{hotel_id}/check-follow', 'HotelController@postCheckFollow')->name('check_follow');
        Route::post('/{hotel_id}/follow', 'HotelController@postFollow')->name('follow');
        Route::post('/{hotel_id}/unfollow', 'HotelController@postUnFollow')->name('unfollow');
        Route::post('/{hotel_id}/visa-requirements', 'HotelController@postVisaRequirements')->name('visa_requirements');
        Route::post('/{hotel_id}/weather', 'HotelController@postWeather')->name('weather');
        Route::post('/{hotel_id}/talking-about', 'HotelController@postTalkingAbout')->name('talking_about');
        Route::post('/{hotel_id}/now-in-place', 'HotelController@postNowInPlace')->name('now_in_place');
        Route::post('/{hotel_id}/contribute', 'HotelController@postContribute')->name('contribute');
        Route::post('/{hotel_id}/ajaxPostReview', 'HotelController@ajaxPostReview')->name('ajax_post_review');
        Route::post('/{hotel_id}/ajaxHappeningToday', 'HotelController@ajaxHappeningToday')->name('ajax_happening_today');
        Route::post('/{hotel_id}/ajaxHotelRates', 'HotelController@ajaxHotelRates')->name('ajax_hotel_rates');
        Route::post('/{hotel_id}/ajax_track_ref', 'HotelController@ajaxTrackRef');
    });

    Route::group(['prefix' => 'restaurant', 'as' => 'restaurant.'], function () {
        Route::get('/ajax_get_media', 'RestaurantController@ajaxGetMedia');
        Route::get('/{restaurant_id}', 'RestaurantController@getIndex')->name('index');
        Route::get('/{restaurant_id}/weather', 'RestaurantController@getWeather')->name('weather');
        Route::get('/{restaurant_id}/packing-tips', 'RestaurantController@getPackingTips')->name('packing_tips');
        Route::get('/{restaurant_id}/etiquette', 'RestaurantController@getEtiquette')->name('etiquette');
        Route::get('/{restaurant_id}/health', 'RestaurantController@getHealth')->name('health');
        Route::get('/{restaurant_id}/visa-requirements', 'RestaurantController@getVisaRequirements')->name('visa_requirements');
        Route::get('/{restaurant_id}/when-to-go', 'RestaurantController@getWhenToGo')->name('when_to_go');
        Route::get('/{restaurant_id}/caution', 'RestaurantController@getCaution')->name('caution');

        Route::post('/{restaurant_id}/check-follow', 'RestaurantController@postCheckFollow')->name('check_follow');
        Route::post('/{restaurant_id}/follow', 'RestaurantController@postFollow')->name('follow');
        Route::post('/{restaurant_id}/unfollow', 'RestaurantController@postUnFollow')->name('un_follow');
        Route::post('/{restaurant_id}/visa-requirements', 'RestaurantController@postVisaRequirements')->name('visa_requirements');
        Route::post('/{restaurant_id}/weather', 'RestaurantController@postWeather')->name('weather');
        Route::post('/{restaurant_id}/talking-about', 'RestaurantController@postTalkingAbout')->name('talking_about');
        Route::post('/{restaurant_id}/now-in-place', 'RestaurantController@postNowInPlace')->name('now_in_place');
        Route::post('/{restaurant_id}/contribute', 'RestaurantController@postContribute')->name('contribute');
        Route::post('/{restaurant_id}/ajaxPostReview', 'RestaurantController@ajaxPostReview')->name('ajax_post_review');
        Route::post('/{restaurant_id}/ajaxHappeningToday', 'RestaurantController@ajaxHappeningToday')->name('ajax_happening_today');
    });

    Route::group(['prefix' => 'discussions', 'as' => 'discussion.'], function () {
        Route::get('/ajaxSearchDestination', 'HomeController@getSelect2Location')->name('ajax_search_destination');
        Route::get('/ajaxSearchDiscussion', 'DiscussionController@getSearchDiscussion')->name('ajax_search_discussion');
        Route::get('/{type?}', 'DiscussionController@getIndex')->name('index');

        Route::get('/ajaxSearchTrips', 'HomeController@getSelect2Trips')->name('ajax_search_trips');

        Route::get('/{discussion_id}', 'DiscussionController@getView')->name('view');
        Route::get('/{discussion_id}/edit', 'DiscussionController@getEdit')->name('edit');
        Route::post('/{discussion_id}/edit', 'DiscussionController@postEdit');
        Route::get('/create', 'DiscussionController@getCreate')->name('create');
        Route::post('/create', 'DiscussionController@postCreate');
        Route::post('/upvote', 'DiscussionController@postUpVoteReply')->name('upvote');
        Route::post('/downvote', 'DiscussionController@postDownVoteReply')->name('downvote');
        Route::post('/reply', 'DiscussionController@postReply')->name('reply');
        Route::post('/reply/report/{reply?}', 'DiscussionController@reportSpam')->name('reply.report_spam');
        Route::post('/edit-reply/{reply?}', 'DiscussionController@putEditReply')->name('reply.edit');
        Route::delete('/delete-reply/{reply?}', 'DiscussionController@deleteReply')->name('reply.delete');
        Route::post('/filter-answers', 'DiscussionController@getFilterAnswers')->name('filter_answers');
        Route::post('/post-view-count', 'DiscussionController@postDiscussionViewCount')->name('post_view_count');
        Route::get('/{discussion_id}/delete', 'DiscussionController@postDelete')->name('delete');

        Route::post('/likeunlike', 'DiscussionController@postLikeUnlike')->name('likeunlike');
        Route::post('/updownvote', 'DiscussionController@postUpDownVotes')->name('updownvote');
        Route::post('/likes4modal', 'DiscussionController@postLikes4Modal')->name('likes4modal');
        Route::post('/shareunshare', 'DiscussionController@postShareUnshare')->name('shareunshare');

        Route::post('/ajaxreply', 'DiscussionController@postAjaxReply')->name('ajaxreply');
        Route::post('/ajaxGetDiscussion', 'DiscussionController@ajaxGetDiscussion')->name('ajax_get_destination');
        Route::post('/get-experts', 'DiscussionController@getExperts')->name('get_experts');
        Route::post('/get-discussion-view', 'DiscussionController@getAjaxView')->name('get_discussion_view');
        Route::post('/get-more-helpful', 'DiscussionController@getMoreHelpful')->name('get_more_helpful');
        Route::post('/get-trending-topic', 'DiscussionController@getTrendingTopic')->name('get_trensing_topic_by_place');
        Route::post('/get-more-replies', 'DiscussionController@getMoreReplies')->name('get_more_replies');
        Route::post('/upload-replies-media', 'DiscussionController@uploadRepliesMedia')->name('upload_replies_media');
        Route::post('/likes', 'DiscussionController@postUpDownvoteDiscussion')->name('likes');
    });

    Route::group(['prefix' => 'medias', 'as' => 'media.'], function () {
        Route::post('/{medias_id}/comment', 'MediasController@postComment')->name('comment');
        Route::post('/commentreply', 'MediasController@postCommentReply')->name('commentreply');
        Route::post('/commentedit', 'MediasController@postCommentEdit')->name('commentedit');
        Route::post('/likeunlike', 'MediasController@postLikeUnlike')->name('likeunlike');
        Route::post('/trip-place-likeunlike', 'MediasController@tripPlaceLikeUnlike')->name('trip-place.likeunlike');
        Route::post('/commentdelete', 'MediasController@postCommentDelete')->name('commentdelete');
        Route::post('/commentlikeunlike', 'MediasController@postCommentLikeUnlike')->name('commentlikeunlike');
        Route::post('/load-more-comments', 'MediasController@getMoreComments')->name('load_more_comment');
        Route::post('/comment-updownvote', 'MediasController@postCommentVotes')->name('comment_updownvote');
        Route::get('/{medias_id}/showlikes', 'MediasController@postShowLikes')->name('showlikes');
        Route::post('/commentlikeusers', 'MediasController@getCommentLikeUsers')->name('commentlikeusers');

        Route::post('/{medias_id}/comment4place', 'MediasController@postComment4Place')->name('comment4place');
        Route::post('/{medias_id}/updateviews', 'MediasController@postUpdateViews');

        Route::get('/comments/{comment_id}', 'MediasController@getComment')->name('get_comment');
        Route::get('/likes/{media_id}', 'MediasController@getMediaLikesData')->name('likes');

        Route::get('/load/{tripMediaId}', 'MediasController@tripMediaContentLoad')->name('load_trip_media_data');
    });

    Route::group(['prefix' => 'posts', 'as' => 'post.'], function () {
        Route::post('/commentlikeunlike', 'Frontend\PostController@postCommentLikeUnlike')->name('commentlikeunlike');
        Route::post('/likeunlike', 'Frontend\PostController@postLikeUnlike')->name('likeunlike');
        Route::post('/likes4modal', 'Frontend\PostController@postLikes4Modal')->name('likes4modal');
        Route::post('/shareunshare', 'Frontend\PostController@postShareUnshare')->name('shareunshare');
        Route::post('/comment', 'Frontend\PostController@postComment')->name('comment');
        Route::post('/commentreply', 'Frontend\PostController@postCommentReply')->name('commentreply');
        Route::post('/commentedit', 'Frontend\PostController@postCommentEdit')->name('commentedit');
        Route::post('/commentdelete', 'Frontend\PostController@postCommentDelete')->name('commentdelete');
        Route::post('/delete', 'Frontend\PostController@postDelete')->name('delete');
        Route::post('/spam', 'Frontend\PostController@reportSpam')->name('spam');
        Route::post('/checkinlikeunlike', 'Frontend\PostController@checkinLikeUnlike')->name('checkinlikeunlike');
        Route::post('/checkincomment', 'Frontend\PostController@checkinComment')->name('checkincomment');
        Route::post('/checkincommentdelete', 'Frontend\PostController@checkinCommentDelete')->name('checkincommentdelete');
        Route::post('/checkincommentlikeunlike', 'Frontend\PostController@checkinCommentLikeUnlike')->name('checkincommentlikeunlike');
        Route::post('/commentlikeusers', 'Frontend\PostController@getCommentLikeUsers')->name('commentlikeusers');
        Route::post('/update-post-permission', 'Frontend\PostController@updatePostPermissionByPostId')->name('updatePostPermission');
        Route::post('/new-comment', 'Frontend\PostController@createNewPost')->name('new-comment');
        Route::post('/load-more-comment', 'Frontend\PostController@getMoreComment')->name('load-more-comment');
        Route::post('/get-post-view', 'Frontend\PostController@getAjaxView')->name('get_post_view');
    });

    Route::group(['prefix' => 'enlarged-views', 'as' => 'enlarged_views.'], function () {
        Route::get('/{url_key}', 'EnlargedViewsController@index')->name('index');
    });

    Route::group(['prefix' => 'trip-places', 'as' => 'trip_places.'], function () {
        Route::post('/likeunlike', 'TripController@postTripPlaceLikeUnlike')->name('likeunlike');
        Route::post('/{trip_place_id}/comment', 'TripPlaceCommentsController@postComment')->name('comment');
        Route::post('/commentreply', 'TripPlaceCommentsController@postCommentReply')->name('commentreply');
        Route::post('/commentedit', 'TripPlaceCommentsController@postCommentEdit')->name('commentedit');
        Route::post('/commentdelete', 'TripPlaceCommentsController@postCommentDelete')->name('commentdelete');
        Route::post('/commentlikeunlike', 'TripPlaceCommentsController@postCommentLikeUnlike')->name('commentlikeunlike');
        Route::post('/load-more-comments', 'TripPlaceCommentsController@getMoreComments')->name('load_more_comment');
        Route::post('/commentlikeusers', 'TripPlaceCommentsController@getCommentLikeUsers')->name('commentlikeusers');

        Route::get('/comments/{comment_id}', 'TripPlaceCommentsController@getComment')->name('get_comment');
        Route::get('/likes/{trip_place_id}', 'TripController@getTripPlaceLikesData')->name('likes');
    });


    //Route::post('auth', 'UsersController@getAutoComplete');

    /*
     * Frontend Routes
     * Namespaces indicate folder structure
     */
    Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
        includeRouteFiles(__DIR__ . '/Frontend/');
    });
});


//Route::post('auth', 'UsersController@getAutoComplete');

Route::get('/download/{folder_name}/{file_name}', 'FileDownloadController@download');


Route::get('logout', '\App\Http\Controllers\Backend\Login\Auth\LoginController@logout')->name('logout');
Route::get('admin/login', '\App\Http\Controllers\Backend\Login\Auth\LoginController@login')->name('admin.login');


// Switch between the included languages
Route::get('lang/{lang}', 'LanguageController@swap');

Route::get('crons2/places/createplacethumbs', 'CronsController@getCreatePlaceThumbs');
Route::get('crons2/places/pluscodes', 'CronsController@getPlusCodes');
Route::get('crons2/hotels/pluscodes', 'CronsController@getHotelsPlusCodes');
Route::get('crons2/restaurants/pluscodes', 'CronsController@getRestaurantsPlusCodes');
Route::get('crons2/embassies/pluscodes', 'CronsController@getEmbassiesPlusCodes');

Route::get('crons2/places/fixlostimages', 'CronsController@getFixLostImages');

Route::get('crons2/places/{page}', 'CronsController@getPlacesMedia')->where(['page' => '^media[1-9][0-9]?$|^media100$']);
Route::get('crons2/restaurants/{page}', 'CronsController@getRestaurantsMedia')->where(['page' => '^media[1-9][0-9]?$|^media10$']);
Route::get('crons2/hotels/{page}', 'CronsController@getHotelsMedia')->where(['page' => '^media[1-9][0-9]?$|^media10$']);

Route::get('crons2/places/details/{field}', 'CronsController@getPlacesDetails');
Route::get('crons2/hotels/details/{field}', 'CronsController@getHotelsDetails');
Route::get('crons2/restaurants/details/{field}', 'CronsController@getRestaurantsDetails');
Route::get('crons2/embassies/details/{field}', 'CronsController@getEmbassiesDetails');


Route::get('crons2/topplaces', 'CronsController@getTopPlaces');

Route::get('demo/{id}', function ($id) {
    echo 'demo check test' . $id;
});
Route::get('/admin/place/delete_place_img/{id}/{imageName}', 'Backend\Place\PlaceController@delete_place_img');

/* ----------------------------------------------------------------------- */


/* ----------------------------------------------------------------------- */

/*
 * Admin Login Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend\Login', 'as' => 'frontend.', 'prefix' => 'admin'], function () {
    includeRouteFiles(__DIR__ . '/AdminLogin/');
});

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */

Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     */
    includeRouteFiles(__DIR__ . '/Backend/');
});

// Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.'], function(){

// Route::group(['namespace' => 'Auth','as' => 'auth.'], function () {
// Route::get('logout-admin','LoginController@logoutAdmin')->name('logout-admin');
// });
/*
 * These routes require no user to be logged in
 */
// Route::group(['middleware' => 'guest','namespace' => 'Auth','as' => 'auth.'], function () {

// Authentication Routes
// Route::get('login', 'LoginController@showLoginForm')->name('login');
// Route::post('login', 'LoginController@loginAdmin')->name('login.post');
// });

// });

/*
 * Api Routes
 * Namespaces indicate folder structure
 */
// Route::group(['namespace' => 'Api', 'as' => 'api.', 'middleware' => 'api'], function () {
/*
 * These routes need view-api permission
 * (good if you want to allow more than one group in the api,
 * then limit the api features by different roles or permissions)
 *
 */
// includeRouteFiles(__DIR__.'/Api/');
// });
